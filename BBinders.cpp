#include <iostream>
#include <sstream>
#include "BBinders.h"
#include "Entity.h"

using namespace std;

//
BBinders::BBinders(const string& p_subject, double p_rate, const string& p_type, BinderState p_state) {
	subject = p_subject;
	rate = p_rate;
	type = p_type;
	state = p_state;
	in = out = value = 0;
}

//
BBinders::~BBinders() {}

//
BBinders::BBinders(const BBinders &right) {
	subject = right.subject;
	rate = right.rate;
	type = right.type;
	state = right.state;
	in = out = value = 0;
}

//
void BBinders::Print() {
	cout << subject << " " << in << " " << out << " " << value << " " << type << endl;
}


//
void BBinders::PrintFile(ofstream *fstr) 
{
	(*fstr) << "#";
	if (state == STATE_HIDDEN) 
		(*fstr) << "h";
	else if (state == STATE_BOUND) 
		(*fstr) << "c";
	if ( rate == HUGE_VAL )
		(*fstr) << "(" << subject << ":inf," << type << ")"; 
	else
		(*fstr) << "(" << subject << ":" << rate << "," << type << ")"; 
}

//binding of subject in binders to channel in pi-proc must be conserved
bool BBinders::IsJoinOk2(const list<Entity*>& sources, Entity* target)
{
   bool res = true;
   std::list< std::list<BBinders*>* >::iterator list_it;
   std::list<BBinders*>::iterator it;
   std::list<Entity*>::const_iterator ent_it;   

   std::list< std::list<BBinders*>* > sources_binder_list;

   for (ent_it = sources.begin(); ent_it != sources.end(); ++ent_it)
   {
      sources_binder_list.push_back((**ent_it).GetBindersList());
   }
   
   list<BBinders*>* target_binder_list = target->GetBindersList();
   // build subject -> type map
   hash_map<string, string> target_binder_map;
   for (it = target_binder_list->begin(); it != target_binder_list->end(); ++it)
   {
      target_binder_map[(**it).GetSubject()] = (**it).GetType();
   }

   // build subject set 
   set<string> target_bound_names;   
   for (it = target_binder_list->begin(); it != target_binder_list->end(); ++it)
   {
      target_bound_names.insert((**it).GetSubject());
   }
   
   //binding of subject in binders to channel in pi-proc must be conserved  
   for (ent_it = sources.begin(); ent_it != sources.end(); ++ent_it)
   {
      Entity* current_source = (*ent_it);
      const list<string>& source_free_names = current_source->GetFn();

      list<string>::const_iterator free_it;
      for (free_it = source_free_names.begin(); free_it != source_free_names.end(); ++free_it)
      {
         if (target_bound_names.find(*free_it) != target_bound_names.end())
         {
            std::stringstream ss;
            ss << "the name '" << *free_it << "' was free in '" <<  current_source->GetId()
               << "' but now is bound to '" << target_binder_map[*free_it] << "'" ;
            Error_Manager::PrintWarning(3, ss.str());
         }
      }      
   }

   return res;
}

//perform a deep analysis of join: 
//no new binders introduced (types and channel names conserved)
//no binders lost
bool BBinders::IsJoinOk1(const std::list< std::list<BBinders*>* >& sources_binder_list, 
                         list<BBinders*>* target_binder_list, 
                         bool showWarnings)
{
   bool res = true;
   std::list< std::list<BBinders*>* >::const_iterator list_it;
   std::list<BBinders*>::iterator it;
   
   // build subject -> type map
   hash_map<string, string> target_binder_map;
   for (it = target_binder_list->begin(); it != target_binder_list->end(); ++it)
   {
      target_binder_map[(**it).GetSubject()] = (**it).GetType();
   }

   // build subject set 
   set<string> target_subject_set;   
   for (it = target_binder_list->begin(); it != target_binder_list->end(); ++it)
   {
      target_subject_set.insert((**it).GetSubject());
   }
   
   // the union of all sources_binder_list, both subjects and types, must be
   // exactly equal to target_binder_list

   // no new binder lost (types and channel names conserved)
   for (list_it = sources_binder_list.begin(); list_it != sources_binder_list.end(); ++list_it)
   {
      std::list<BBinders*>* current_source_list = *list_it;    
      for (it = current_source_list->begin(); it != current_source_list->end(); ++it)
      {
         hash_map<string, string>::iterator map_it = target_binder_map.find((**it).GetSubject());
         if (map_it == target_binder_map.end())
         {
            if (showWarnings)
            {
               std::stringstream ss;            
               ss << "subject '" << (**it).GetSubject() << "' is present in one of the event sources, but not in the event target";
               Error_Manager::PrintWarning(3, ss.str());
            }
            res = false;
         }
         else
         {
            if (map_it->second != (**it).GetType())
            {
               //(**it).GetType() is the type of the source entity
               if (showWarnings)
               {
                  std::stringstream ss;            
                  ss << "subject '" << (**it).GetSubject() << "' in event target must be of type '"
                     << (**it).GetType() << "' instead of '" << map_it->second <<"'";
                  Error_Manager::PrintWarning(3, ss.str());
               }
               res = false;
            }
         }
      }      
   }

   //no binders added
   for (list_it = sources_binder_list.begin(); list_it != sources_binder_list.end(); ++list_it)
   {
      //remove all the binders in sources from the target
      std::list<BBinders*>* current_source_list = *list_it;    
      for (it = current_source_list->begin(); it != current_source_list->end(); ++it)
      {
         target_subject_set.erase((**it).GetSubject());
      }
   }
   //if some binder is still in the set, the target has new binders
   if (!target_subject_set.empty())
   {
      if (showWarnings)
      {
         std::stringstream ss;
         ss << "the subject(s): ";
         set<string>::iterator set_it;
      
         bool first = true;
         for (set_it = target_subject_set.begin(); set_it != target_subject_set.end(); ++set_it)
         {
            if (first)
               first = false;
            else
               ss << ", ";
            ss << *set_it;
         }
         ss << " is/are not in any of the event sources";
         Error_Manager::PrintWarning(3, ss.str());
      }
      res = false;
   }
 
   return res;
}

//binding of subject in binders to channel in pi-proc must be conserved
bool BBinders::IsSplitOk2(Entity* source, const vector<Entity*>& targets)
{
   bool res = true;
   std::list< std::list<BBinders*>* >::iterator list_it;
   std::list<BBinders*>::iterator it;

   //build lists
   std::list< std::list<BBinders*>* > targets_binder_list;

   std::vector<Entity*>::const_iterator ent_it;
   for (ent_it = targets.begin(); ent_it != targets.end(); ++ent_it)
   {
      targets_binder_list.push_back((**ent_it).GetBindersList());
   }
   
   list<BBinders*>* binder_list = source->GetBindersList();
   //end build lists

   // build subject -> type map
   hash_map<string, string> binder_map;
   for (it = binder_list->begin(); it != binder_list->end(); ++it)
   {
      binder_map[(**it).GetSubject()] = (**it).GetType();
   }

   // build subject set 
   set<string> bound_names;   
   for (it = binder_list->begin(); it != binder_list->end(); ++it)
   {
      bound_names.insert((**it).GetSubject());
   }

   //binding of subject in binders to channel in pi-proc must be conserved  
   for (ent_it = targets.begin(); ent_it != targets.end(); ++ent_it)
   {
      Entity* current_target = (*ent_it);
      const list<string>& target_free_names = current_target->GetFn();

      list<string>::const_iterator free_it;
      for (free_it = target_free_names.begin(); free_it != target_free_names.end(); ++free_it)
      {
         if (bound_names.find(*free_it) != bound_names.end())
         {
            std::stringstream ss;
            ss << "the name '" << *free_it << "' was bound to '" <<  binder_map[*free_it] << "' in '"
               << current_target->GetId()
               << "' but is now free";
            Error_Manager::PrintWarning(3, ss.str());
         }
      }      
   }

   return res;
}

//perform a deep analysis of split: 
//no new binders introduced (types and channel names conserved)
//no binders lost
bool BBinders::IsSplitOk1(list<BBinders*>* binder_list,
                          const std::list< std::list<BBinders*>* >& targets_binder_list,
                          bool showWarnings)                          
{
   bool res = true;
   std::list< std::list<BBinders*>* >::const_iterator list_it;
   std::list<BBinders*>::iterator it;

   // build subject -> type map
   hash_map<string, string> binder_map;
   for (it = binder_list->begin(); it != binder_list->end(); ++it)
   {
      binder_map[(**it).GetSubject()] = (**it).GetType();
   }

   // build subject set 
   set<string> subject_set;   
   for (it = binder_list->begin(); it != binder_list->end(); ++it)
   {
      subject_set.insert((**it).GetSubject());
   }
   
   // the union of all targets_binder_list, both subjects and types, must be
   // exactly equal to binder_list

   // no new binder introduced (types and channel names conserved)
   for (list_it = targets_binder_list.begin(); list_it != targets_binder_list.end(); ++list_it)
   {
      std::list<BBinders*>* current_target_list = *list_it;    
      for (it = current_target_list->begin(); it != current_target_list->end(); ++it)
      {
         hash_map<string, string>::iterator map_it = binder_map.find((**it).GetSubject());
         if (map_it == binder_map.end())
         {
            if (showWarnings)
            {
               std::stringstream ss;            
               ss << "subject '" << (**it).GetSubject() << "' in event target is not present in the event source";
               Error_Manager::PrintWarning(3, ss.str());
            }
            res = false;
         }
         else
         {
            if (map_it->second != (**it).GetType())
            {
               if (showWarnings)
               {
                  std::stringstream ss;            
                  ss << "subject '" << (**it).GetSubject() << "' in event target should be of type '"
                     << map_it->second << "' instead of '" << (**it).GetType() <<"'";
                  Error_Manager::PrintWarning(3, ss.str());
               }
               res = false;
            }
         }
      }      
   }

   //no binders lost
   for (list_it = targets_binder_list.begin(); list_it != targets_binder_list.end(); ++list_it)
   {
      std::list<BBinders*>* current_target_list = *list_it;    
      for (it = current_target_list->begin(); it != current_target_list->end(); ++it)
      {
         subject_set.erase((**it).GetSubject());
      }
   }
   if (!subject_set.empty())
   {
      if (showWarnings)
      {
         std::stringstream ss;
         ss << "the subject(s): ";
         set<string>::iterator set_it;

         bool first = true;
         for (set_it = subject_set.begin(); set_it != subject_set.end(); ++set_it)
         {
            if (first)
               first = false;
            else
               ss << ", ";
            ss << *set_it;
         }
         ss << " is/are not present in any of the event target";
         Error_Manager::PrintWarning(3, ss.str());
      }
      res = false;
   }

   return res;
}

 
//compute the split type
//SPLIT_SYMMETRIC the targets are symmetric and equal to the subject
//SPLIT_DIVIDED binders are divided between the targets
//SPLIT_ASYMMETRIC binders are divided between the targets, but some of them
//                 are duplicated
//SPLIT_WRONG IsSplitOk == false
SplitType BBinders::GetSplitType(Entity* source, const vector<Entity*>& targets)
{
   list<BBinders*>::iterator it;
   vector<Entity*>::const_iterator ent_it;


   if (!IsSplitOk2(source, targets))   
      return SPLIT_WRONG;
  

   //check SYMMETRIC
   bool sym = true;
   
   list<BBinders*>* source_binder_list = source->GetBindersList();      
   //we know binders are ok (just checked) so only check sizes
   for (ent_it = targets.begin(); ent_it != targets.end(); ++ent_it)
   {
      Entity* target = *ent_it;
      list<BBinders*>* target_binder_list = target->GetBindersList();      
      if (target_binder_list->size() != source_binder_list->size())
      {
         sym = false;
         break;
      }
   }
   if (sym)
      return SPLIT_SYMMETRIC;

   // check DIVIDED - ASYMMETRIC   
   set<string> unique_binders;

   for (ent_it = targets.begin(); ent_it != targets.end(); ++ent_it)
   {
      Entity* target = *ent_it;
      list<BBinders*>* binder_list = target->GetBindersList();      
      
      for (it = binder_list->begin(); it != binder_list->end(); ++it)
      {
         const string& binding_type = (**it).GetSubject();
         set<string>::iterator it2 = unique_binders.find(binding_type);
         if (it2 == unique_binders.end())         
            unique_binders.insert(binding_type);
         else
            //non unico
            return SPLIT_ASYMMETRIC;
      }
   }

   // if we arrived here, sum of binders is unique
   return SPLIT_DIVIDED;
}

//TODO: TEST
bool BBinders::IsOkToInheritSplit(const string& binding_type, Entity* source, const vector<Entity*>& targets)
{
   // check that this is a perfect split: no (x : A, y: B) -> (x : C) + (x : B).
   // In such a case we don't know what to do if, for example, A is bound
   // (splitting will lose a binding?)
   std::list< std::list<BBinders*>* > target_binders_list;
   vector<Entity*>::const_iterator it;
   for (it = targets.begin(); it != targets.end(); ++it)
   {
      /*
      std::list<BBinders*> binder_list = (*it)->GetBindersList();
      std::list<BBinders*> bound_binder_list;

      std::list<BBinders*>::const_iterator bit;   
      for (bit = binder_list.begin(); bit != bit = binder_list.end(); ++it)
      {
         if ((*bit)->GetState() == "B")
         {
            bound_binder_list.push_back(*bit);
         }
      }
      target_binders_list.push_back(bound_binder_list);
      */
      if (*it != NULL)
      target_binders_list.push_back((*it)->GetBindersList());
   }

   if (!IsSplitOk1(source->GetBindersList(), target_binders_list, false))
   {
      return false;
   }

   // (x : A, y: B) -> (x : A, y : B) + (x : B)
   // check if two targets have the same bound 
   // binder (B); if so, we cannot split them no more
   // (who will get the bound binder? and the other will 
   // have an unbound binder?
   BBinders* target_binder = NULL; 

   for (it = targets.begin(); it != targets.end(); ++it)
   {
      Entity* target = *it;

      if (target != NULL)
      {
      target_binder = target->GetBB(binding_type);

      if (target_binder != NULL)
      {
         // two targets has the binder I want to bind, 
         // better discard this event
         return false;
      }
   }
   }

   return true;
}

//TODO: TEST
bool BBinders::IsOkToInheritJoin(const string& binding_type, const vector<Entity*>& sources, Entity* target)
{
   // check that this is a perfect join: no -> (x : C) + (x : B) -> (x : A, y: B)
   // In such a case we don't know what to do 
   // - if A is bound (same sbj, different type)
   // - if B is bound (same type, different sbj) 
   // - what if x: C and x: B are both bound? Will lose one bound sbj (one of the x)
   //   on the way

   // The target is Nil
   if (target == NULL)
      return false;
  
   // Iterators:
   std::list< std::list<BBinders*>* >::const_iterator list_it;
   std::list<BBinders*>::iterator it;
   std::vector<Entity*>::const_iterator vit;

   // Binders lists
   std::list<BBinders*>* target_binder_list = target->GetBindersList();  
   std::list< std::list<BBinders*>* > sources_binder_list;

   // Build source list   
   for (vit = sources.begin(); vit != sources.end(); ++vit)
   {      
      sources_binder_list.push_back((*vit)->GetBindersList());
   }
   
   // build subject -> type map
   hash_map<string, string> target_binder_map;
   for (it = target_binder_list->begin(); it != target_binder_list->end(); ++it)
   {
      if ((*it)->GetState() == STATE_BOUND)
         target_binder_map[(**it).GetSubject()] = (**it).GetType();
   }
   
   // the union of all sources_binder_list, both subjects and types, must be
   // exactly equal to target_binder_list

   // no binder lost (types and channel names conserved)
   // all bound binders in sources have a correspondent binder in target
   for (list_it = sources_binder_list.begin(); list_it != sources_binder_list.end(); ++list_it)
   {
      std::list<BBinders*>* current_source_list = *list_it;    
      for (it = current_source_list->begin(); it != current_source_list->end(); ++it)
      {
         if ((*it)->GetState() == STATE_BOUND)
         {
            hash_map<string, string>::iterator map_it = target_binder_map.find((**it).GetSubject());
            if (map_it == target_binder_map.end())
            {            
               return false;
            }
            else
            {
               if (map_it->second != (**it).GetType())
               {
                  return false;
               }
            }
            // remove it: we already matched it, we don't want two source binders map
            // onto the same target
            target_binder_map.erase(map_it);
         }
      }      
   }

   return true;
}   

