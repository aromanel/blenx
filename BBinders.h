#ifndef BBINDERS_H
#define BBINDERS_H

#ifdef _MSC_VER
#include <hash_map>
using namespace stdext;
#else
#include <ext/hash_map>
#endif

#include <fstream>
#include <list>
#include "Define.h"

class Entity;

//////////////////////////////////////////////////////
// BBINDERS CLASS
//////////////////////////////////////////////////////

class BBinders {
private:
	string subject;
	int in;
	int out;
	int value;
	double rate;
	string type;
	BinderState state;
		
public:
	BBinders(const string& p_subject, double p_rate, const string& p_type, BinderState p_state);
	BBinders(const BBinders &right);
	~BBinders();

   //
   const string& GetType() { return type; }

   //
   int GetIn() { return in; }
   int GetOut() { return out; }
   int GetVal() { return value; }

   const string& GetSubject() { return subject; }
   void SetSubject(const string& p_subject) { subject = p_subject; }

   //
   BinderState GetState() { return state; }
   double GetRate() { return rate; }

   void AddIn(int val) { in += val; }
   void AddOut(int val) { out += val; }
   void DecIn(int val) { in -= val; }
   void DecOut(int val) { out -= val; }
   void SetHide() { value = 0; }
   void CalculateVal() { value = Utility::sg(in) + 2 * Utility::sg(out); }
   void SetState(BinderState p_state) { state = p_state; }
   void SetType(const string& p_type) { type = p_type; }
   void Print();
   void PrintFile(ofstream *fstr);


   static bool IsJoinOk2(const list<Entity*>& sources, Entity* target);
   static bool IsJoinOk1(const std::list< std::list<BBinders*>* >& sources_binder_list,
                         list<BBinders*>* target_binder_list,
                         bool showWarnings);

   static bool IsJoinOk1(const std::list< std::list<BBinders*>* >& sources_binder_list,
      list<BBinders*>* target_binder_list)
   {
      return IsJoinOk1(sources_binder_list, target_binder_list, true);
   }

   //perform a deep analysis of split: 
   //binding of subject in binders to channel in pi-proc must be conserved,
   //no new binders introduced (types and channel names conserved),
   //no new bindings introduced,
   //no binders lost
   static bool IsSplitOk2(Entity* source, const vector<Entity*>& targets);
   static bool IsSplitOk1(list<BBinders*>* source_binder_list,
                          const std::list< std::list<BBinders*>* >& targets_binder_list,
                          bool showWarnings);

   static bool IsSplitOk1(list<BBinders*>* source_binder_list,
                          const std::list< std::list<BBinders*>* >& targets_binder_list)
   {
      return IsSplitOk1(source_binder_list, targets_binder_list, true);
   }

   //static bool IsSplitOk(std::list<BBinders*>* binder_list, std::list< std::list<BBinders*>* > targets_binder_list);

   //compute the split type
   //SPLIT_SYMMETRIC the targets are symmetric and equal to the subject
   //SPLIT_DIVIDED binders are divided between the targets
   //SPLIT_ASYMMETRIC binders are divided between the targets, but some of them
   //                 are duplicated
   //SPLIT_WRONG IsSplitOk == false
   static SplitType GetSplitType(Entity* source, const vector<Entity*>& targets);

   //binding_type: the type that is going to be bound
   static bool IsOkToInheritSplit(const string& binding_type, Entity* source, const vector<Entity*>& targets);

   static bool IsOkToInheritJoin(const string& binding_type, const vector<Entity*>& sources, Entity* target);

   
};

#endif

