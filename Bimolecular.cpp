#include <iostream>
#include "Bimolecular.h"
#include "Environment.h"
#include "Define.h"

using namespace std;

//
Bimolecular::Bimolecular(Environment* p_env, Entity *p_beta1, Entity *p_beta2, FunBase *p_rate,
	BBinders *p_binder1, BBinders *p_binder2) : Element(p_env, BIM)
{
	beta1 = p_beta1;
	beta2 = p_beta2;
	rate = p_rate;
	binder1 = p_binder1;
	binder2 = p_binder2;
}

//
Bimolecular::~Bimolecular()
{
   if (!rate->IsManagedByMap())
	 delete rate;
}

//
Entity *Bimolecular::GetOutput()
{
	return beta1;
}

//
Entity *Bimolecular::GetInput()
{
	return beta2;
}

//
BBinders *Bimolecular::GetOutBinder()
{
	return binder1;
}

//
BBinders *Bimolecular::GetInBinder()
{
	return binder2;
}

//
double Bimolecular::GetRate(Iterator_Interface *c_iter)
{
	return this->rate->Rate(c_iter);
}


Expr* Bimolecular::GetExpr(Iterator_Interface *c_iter){
	CR_Two_Iterator *cr_two_it;
	int mol1, mol2;

	if (c_iter->GetIterator(2) == NULL){
		mol1 = beta1->GetId();
		mol2 = beta2->GetId();
	} else {

		assert(dynamic_cast<CR_Two_Iterator *>(c_iter->GetIterator(2)));
		cr_two_it = (CR_Two_Iterator *)c_iter->GetIterator(2);

		if (c_iter->GetIterator(2)->GetIterator(1) == NULL) {
			mol1 = beta2->GetId();
		} else {
			assert(dynamic_cast<CR_One_Iterator *>(cr_two_it->GetIterator(1)));
			mol1 = (*(((CR_One_Iterator *)cr_two_it->GetIterator(1))->GetCurrent()))->GetIdN();
		}

		if (c_iter->GetIterator(2)->GetIterator(2) == NULL) {
			mol2 = beta1->GetId();
		} else {
			assert(dynamic_cast<CR_One_Iterator *>(cr_two_it->GetIterator(2)));
			mol2 = (*(((CR_One_Iterator *)cr_two_it->GetIterator(2))->GetCurrent()))->GetIdN();
		}
	}

	return rate->GetExpr(mol1, mol2, c_iter);
}

double Bimolecular::GetActualRate()
{
	return ( rate->ActualRate() );
}

void Bimolecular::GetAllDependencies(std::vector<Map_Elem*>& elem_list)
{
   if (rate != NULL)
   {
      env->GetDependencies(rate->GetEntityList(), elem_list);
      env->GetDependencies(rate->GetStateVarList(), elem_list);
   }
}


bool Bimolecular::OutOfGas()
{
   if (beta1->GetCounter() < 1 || beta2->GetCounter() < 1)
      return true;
   return false;
}

//
double Bimolecular::Time()
{
	double actual_rate = rate->ActualRate();

	if ( actual_rate == 0.0 )
		return HUGE_VAL;

   //first, check if we have something!
   if (OutOfGas())
   {
      //not 0.0, and out of gas: warning
      Error_Manager::PrintWarningOnce(22, "The event tried to schedule itself (actual_rate != 0) even if some originating entities are = 0; check your rate function!");
      return HUGE_VAL;
   }

	if ( actual_rate == HUGE_VAL)
		return -1.0;

	double rn = env->Random0To1();

	if (rn == 0.0)
		return HUGE_VAL;

	return ( 1.0 / ( actual_rate ) ) * log( 1.0 / rn );
}

//
double Bimolecular::GetTotal()
{
	// Quando viene invocata nel caso di rate con funzione generica
	// ritorna positivo se le entitia' coinvolte hanno cardinalita'
	// maggiore di 0. Nel caso di rate stocastico invece ritorna
	// il numero totale di combinazioni.
	switch(rate->GetType()) {
		case(RATE_BIM):
			return (rate->ActionNumber());
			break;
		case(RATE_GENERIC):
			if (beta1->GetCounter()>0 && beta2->GetCounter()>0)
			{
				return(1);
			}
			return(0);
			break;
		default:
			return(0);
	}
}

//
void Bimolecular::Print() {
	cout << beta1->GetId() << " " << beta2->GetId() << " " << rate << " " <<
		binder1->GetSubject() << " " << binder2->GetSubject();
}

//
bool Bimolecular::Is(Element *elem) {
	return ((elem == beta1) || (elem == beta2));
}

//
Iterator_Interface *Bimolecular::GetIterator()
{
	Iterator_Interface *iter_out = this->beta1->GetIntraIteratorOut(this->binder1->GetSubject());
	Iterator_Interface *iter_in = this->beta2->GetIntraIteratorIn(this->binder2->GetSubject());
	return ( new Bimolecular_Iterator(iter_out,iter_in) );
}


////////////////////////////////////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////////////////////////////////////

Bimolecular_Iterator::Bimolecular_Iterator(Iterator_Interface *p_intra_out_iterator,
		Iterator_Interface *p_intra_in_iterator) : Iterator_Interface(IT_BIMOLECULAR,2)
{
	this->SetIterator(p_intra_out_iterator,1);
	this->SetIterator(p_intra_in_iterator,2);
}

Bimolecular_Iterator::~Bimolecular_Iterator()
{}

void Bimolecular_Iterator::IteratorReset()
{
	this->GetIterator(1)->IteratorReset();
	this->GetIterator(2)->IteratorReset();
}

bool Bimolecular_Iterator::IteratorIsEnd()
{
	return this->GetIterator(1)->IteratorIsEnd();
}

void Bimolecular_Iterator::IteratorNext()
{
	this->GetIterator(2)->IteratorNext();
	if ( this->GetIterator(2)->IteratorIsEnd() )
	{
		this->GetIterator(1)->IteratorNext();
		this->GetIterator(2)->IteratorReset();
	}
}

void Bimolecular::GetReactants(vector<Entity *> &reactants){
	reactants.push_back(beta1);
	reactants.push_back(beta2);
}


void Bimolecular::GetIterator(pair<Entity *, Entity *> *ent_pair, list<Complex *> *comp, pair<Iterator_Interface *, Iterator_Interface *> *it_pair){

	list<Complex *>::iterator comp_it;
	list<Complex *> *comp_list;
	Entity *e1;
	Entity *e2;
	bool inverted = false;

	//At least one entity has to be part of the complex
	assert(ent_pair->first != NULL);
	if(ent_pair->second != NULL){
		//Entities that take part in the reaction (this reaction) are both part
		//of the complex (and are different, the "else" is chosen also if
		//beta1 == beta2)
		e1 = ent_pair->first;
		e2 = ent_pair->second;
	} else {
		//Of the two entity that take part at the reaction we found which one is
		//also part of the complex
		if (ent_pair->first->GetId() == this->beta1->GetId()){
			//beta1 is part of the complex
			e1 = ent_pair->first;
			e2 = beta2;
		} else {
			//beta2 is part of the complex
			assert(ent_pair->first->GetId() == this->beta2->GetId());
			e1 = ent_pair->first;
			e2 = beta1;
		}
	}

	//The first iterator look through all the complexes which e2 is in.
	CR_Two_Iterator *cr2i = new CR_Two_Iterator(env, BIM_AND_MONO, e1 == e2);

	//Two cases to be considered:
	// 1) beta1 is in the complex we are analysing
	// 2) beta2 is in the complex we are analysing
	// In the first case we have to compare our complex with all the complexes
	// that contains beta2, in the secod case we have to compare all the
	// complexes that conteins beta1 with our complex
	if (inverted){
		cr2i->SetIterator(new CR_One_Iterator(comp->begin(),comp->size(), e1->GetId(), env),2);
		cr2i->SetIterator(env->GetComplexRel()->GetIteratorOne(e2->GetId()),1);
	} else {
		cr2i->SetIterator(new CR_One_Iterator(comp->begin(), comp->size(), e1->GetId(), env),1);
		cr2i->SetIterator(env->GetComplexRel()->GetIteratorOne(e2->GetId()),2);
	}

	it_pair->first = new Simple_Iterator(this->GetIterator(), cr2i);

	if(ent_pair->second != NULL){
	//If both entities are part of the complex we need two iterators
	//The second iterator looks through all the complexes which e1 is in except
	//for the complex that belongs to the comp list

		//Only one complex has to be present in the list
		assert(comp->size() == 1);
		comp_it = comp->begin();

		//Get all the complexes which e1 is in
		comp_list = env->GetComplexRel()->GetComplexesList(e1->GetId());
		//Remove the complex that belongs to the comp list (the one we are
		//analysing)
		comp_list->remove(*comp_it);
		//Add it to the front of the list
		comp_list->push_front(*comp_it);
		//Set the comp_it iterator in order to make it pointing after the complex
		//we have just added to the comp_list list
		comp_it = comp_list->begin();
		assert(comp_it != comp_list->end());
		comp_it++;

		//Create the iterator
		cr2i->SetIterator(new CR_One_Iterator(comp->begin(), comp->size(), e2->GetId(), env),1);
		cr2i->SetIterator(new CR_One_Iterator(comp_it, comp_list->size(), e1->GetId(), env), 2);

		//it_pair->second->SetIterator(this->GetIterator(), 1);
		//it_pair->second->SetIterator(cr2i, 2);
		it_pair->second = new Simple_Iterator(this->GetIterator(), cr2i);
	} else {
		//Second iterator is not needed
		it_pair->second = NULL;
	}
}

bool Bimolecular::IsOnlyMono(int reactants[2]){
	reactants[0] = this->beta1->GetId();
	reactants[1] = this->beta2->GetId();

	return false;
}
Iterator_Interface * Bimolecular::GetIterator(Entity *e1, Entity *e2, list<Complex *> *c1, list<Complex *> *c2, TwoIteratorKind iterator_kind){
	if (c1->empty() && c2->empty()){
		return new Simple_Iterator(this->GetIterator(), NULL);
	}

	CR_Two_Iterator *cr2i = new CR_Two_Iterator(env, iterator_kind, e1 == e2);

	if (c2->empty()){
		cr2i->SetIterator(NULL,1);
	} else {
		cr2i->SetIterator(new CR_One_Iterator(c2->begin(), c2->size(), e2->GetId(), env),1);
	}

	if (c1->empty()){
		cr2i->SetIterator(NULL,2);
	} else {
		cr2i->SetIterator(new CR_One_Iterator(c1->begin(), c1->size(), e1->GetId(), env),2);
	}


	return new Simple_Iterator(this->GetIterator(), cr2i);
}

