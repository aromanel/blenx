#ifndef BIMOLECULAR_H
#define BIMOLECULAR_H

#include <cstdlib>
#include <string>
#include <list>
#include <map>
#include <cmath>
#include "Define.h"
#include "Entity.h"
#include "FunStd.h"
#include "Map_Elem.h"
#include "Complex_Rel.h"


using namespace std;

class Environment;
class FunBim;


//////////////////////////////////////////////////////
// BIMOLECULAR ITERATOR CLASS
//////////////////////////////////////////////////////

class Bimolecular_Iterator : public Iterator_Interface
{
public:
	Bimolecular_Iterator(Iterator_Interface *p_intra_out_iterator,
			Iterator_Interface *p_intra_in_iterator);
	~Bimolecular_Iterator();
	void IteratorReset();
	bool IteratorIsEnd();
	void IteratorNext();
};


//////////////////////////////////////////////////////
// BIMOLECULAR CLASS
//////////////////////////////////////////////////////

class Bimolecular : public Element 
{
private:
	Entity *beta1;			// First entity
	Entity *beta2;			// Second entity
	FunBase *rate;			// Functions describing the rate function
	BBinders *binder1;		// binder of BETA1
	BBinders *binder2;		// binder of BETA2

public:
	Bimolecular(Environment* p_env, Entity *p_beta1, Entity *p_beta2, FunBase *p_rate, 
		BBinders *p_binder1, BBinders *p_binder2);
	~Bimolecular();
	void Print();
	double GetTotal();
	double Time();
	bool Is(Element *elem);
	BBinders *GetOutBinder();
	BBinders *GetInBinder();
	Entity *GetOutput();
	Entity *GetInput();

	double GetRate(Iterator_Interface *c_iter);
	double GetActualRate();
	double GetBasalRate(Iterator_Interface *c_iter){ return rate->GetConstant(); };
	Expr* GetExpr(Iterator_Interface *c_iter);

	void GetAllDependencies(std::vector<Map_Elem *>& elem_list);   

	bool NotZero();
	bool OutOfGas();

	Iterator_Interface *GetIterator();
	void GetReactants(vector<Entity *> &reactants);

	void GetIterator(pair<Entity *, Entity *> *ent_pair, list<Complex *> *comp, pair<Iterator_Interface *, Iterator_Interface *> *it_pair);
	Iterator_Interface * GetIterator(Entity *e1, Entity *e2, list<Complex *> *c1, list<Complex *> *c2, TwoIteratorKind iterator_kind);
	bool IsOnlyMono(int reactants[2]);
};


#endif

