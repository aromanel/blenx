#ifndef BIMOLECULAR_BIND_H
#define BIMOLECULAR_BIND_H

#include <cstdlib>
#include <string>
#include <list>
#include <map>
#include <cmath>
#include "Bimolecular.h"

using namespace std;

class Entity;
class Environment;


//////////////////////////////////////////////////////
// BIMOLECULAR_BIND CLASS
//////////////////////////////////////////////////////

class Bimolecular_Bind : public Element {
private:
	Entity *beta1;
	Entity *beta2;
	double rate;
	//double actual_rate;
	int comb;
	BBinders *binder1;		// binder of BETA1
	BBinders *binder2;		// binder of BETA2

public:
	Bimolecular_Bind(Environment* p_env, Entity *p_beta1, Entity *p_beta2, double p_rate, BBinders *p_binder1, BBinders *p_binder2,
		int p_comb );
	~Bimolecular_Bind();
	void Print();
	double GetTotal();
	double Time();
	bool Is(Element *elem);
	BBinders *GetOutBinder();
	BBinders *GetInBinder();
	Entity *GetOutput();
	Entity *GetInput();

    double GetRate(Iterator_Interface *c_iter);
	double GetActualRate();
	double GetBasalRate(Iterator_Interface *c_iter){ return rate; };
	Expr* GetExpr(Iterator_Interface *c_iter);

	Iterator_Interface *GetIterator();
	void GetReactants(vector<Entity *> &reactants);
		virtual void GetIterator(pair<Entity *, Entity *> *, list<Complex *> *comp, pair<Iterator_Interface *, Iterator_Interface *> *);
	Iterator_Interface * GetIterator(Entity *e1, Entity *e2, list<Complex *> *c1, list<Complex *> *c2, TwoIteratorKind iterator_kind);
	bool IsOnlyMono(int reactants[2]);
};


#endif
