#include <iostream>
#include "Bind.h"
#include "Environment.h"

using namespace std;


//////////////////////////////////////////////////////
// CLASS BIND - METHODS DEFINITION
//////////////////////////////////////////////////////


//
Bind::Bind(Environment* p_env, Entity *p_beta1, Entity *p_beta2, double p_rate, 
           BBinders *p_binder1, BBinders *p_binder2, int p_num) 
   : Element(env, BIND) 
{
   env = p_env;
	beta1 = p_beta1;
	beta2 = p_beta2;
	rate = p_rate;
	binder1 = p_binder1;
	binder2 = p_binder2;
	num = p_num;
}

//
double Bind::GetTotal()
{
	return (GetComb());
}

//
double Bind::GetRate(Iterator_Interface *c_iter)
{
	Complex *c1,*c2;
	c1 = c2 = NULL;

	if ( c_iter != NULL )
	{
		assert(c_iter->GetCategory() == IT_ACT_MAP);
		double c_rate = this->rate;
		if ( c_iter->GetIterator(2) == NULL )
		{
			// sono specie non complessate
			if ( this->beta1 == this->beta2 )
			{	// homodimerization
				c_rate *= ( 0.5 * (double)this->beta1->GetCounter() * 
					((double)this->beta2->GetCounter() - 1.0) );
			}
			else 
			{ 
				c_rate *=  ( ((double)this->beta1->GetCounter()) * ((double)this->beta2->GetCounter()) ); 
			}
		}
		else
		{
			assert(c_iter->GetIterator(2)->GetCategory() == IT_COMPLEX_REL_TWO);
			CR_Two_Iterator * c_iter_comp = (CR_Two_Iterator *)(c_iter->GetIterator(2));
			CR_One_Iterator * c_iter1 = (CR_One_Iterator*)(c_iter_comp->GetIterator(1));
			if (c_iter1 != NULL)
				c1 = (*((CR_One_Iterator *)c_iter1)->GetCurrent());
			CR_One_Iterator * c_iter2 = (CR_One_Iterator*)(c_iter_comp->GetIterator(2));
			if (c_iter2 != NULL)
				c2 = (*((CR_One_Iterator *)c_iter2)->GetCurrent());
			if ( c_iter_comp->IsWait() )
			{
				// For sure c1 and c2 are equal and different from NULL, because
				// otherwise IsWait() returns always false
				c_rate *= c1->GetNumber();;
			}
			else
			{
				if ( c_iter1 != NULL && c_iter2 != NULL && !c_iter1->IsEqual(c_iter2) )
				{
					c_rate *= 2;
				}

				if ( c_iter1 == NULL )
				{
					c_rate *= this->beta1->GetCounter();
				}
				else
				{
					c_rate *= c1->GetNumber();
				}

				if ( c_iter2 == NULL )
				{
					c_rate *= this->beta2->GetCounter();
				}
				else
				{
					if ( c1 == c2 )
					{
						// non devo contare lo stesso complesso
						c_rate *= c2->GetNumber() - 1;
						c_rate = c_rate / 2 ;
					}
					else
					{
						c_rate *= c2->GetNumber();
					}

				}
			}

			return c_rate;
		}
	}
	
	return this->rate;
}


Expr* Bind::GetExpr(Iterator_Interface *c_iter){
	if (rate == HUGE_VAL){
		return NULL;
	}
	
	Pos pos(0,0,"");
	Expr* expr1 = NULL;
   Expr* expr2 = NULL;
   Expr* expr = NULL;

	CR_Two_Iterator *cr_two_it;
	int mol1 = 0; 
   int mol2 = 0;

	cr_two_it = (CR_Two_Iterator*)c_iter->GetIterator(2);

	if (c_iter->GetIterator(2) == NULL)
   {
		mol1 = beta1->GetId();
		mol2 = beta2->GetId();
	} 
   else 
   {
		assert(dynamic_cast<CR_Two_Iterator *>(c_iter->GetIterator(2)) != NULL);
		cr_two_it = (CR_Two_Iterator *)c_iter->GetIterator(2);


		if (c_iter->GetIterator(2)->GetIterator(1) == NULL) {
			mol1 = beta1->GetId();
		} else {
			assert(dynamic_cast<CR_One_Iterator *>(cr_two_it->GetIterator(1)));
			mol1 = (*(((CR_One_Iterator *)cr_two_it->GetIterator(1))->GetCurrent()))->GetIdN();
		}

		if (c_iter->GetIterator(2)->GetIterator(2) == NULL) {
			mol2 = beta2->GetId();
		} else {
			assert(dynamic_cast<CR_One_Iterator *>(cr_two_it->GetIterator(2)));
			mol2 = (*(((CR_One_Iterator *)cr_two_it->GetIterator(2))->GetCurrent()))->GetIdN();
		}
	}

	string name;
	if (mol1 > 0){
		name = env->GetST()->GetEntNameFromID(mol1);
		expr1 = new ExprConcentration(pos, env->GetST(), name.substr(2));
	} else {
		name = env->GetST()->GetMolNameFromID(abs(mol1));
		expr1 = new ExprCompConcentration(pos, env->GetST(), name.substr(2));
	}

	if (cr_two_it == NULL || !cr_two_it->IsWait()){
		if (mol2 > 0)
      {
			name = env->GetST()->GetEntNameFromID(mol2);
			expr2 = new ExprConcentration(pos, env->GetST(), name.substr(2));
		} 
      else if (mol2 < 0) // note we explicitly avoid 0: it is an error and has to be caught
      {
			name = env->GetST()->GetMolNameFromID(abs(mol2));
			expr2 = new ExprCompConcentration(pos, env->GetST(), name.substr(2));
		}
	}

	if (cr_two_it != NULL && cr_two_it->IsWait())
   {
		//Monomolecular reactions
		expr = new ExprOp(pos, env->GetST(), expr1, new ExprNumber(pos, env->GetST(),rate), EXPR_TIMES);
	} 
   else 
   {
		if (mol1 == mol2)
      {
			//Homodimerization reaction
         assert(expr2 != NULL);
			expr2 = new ExprOp(pos, env->GetST(), expr2, new ExprNumber(pos, env->GetST(), 1), EXPR_MINUS);
			expr = new ExprOp(pos, env->GetST(), expr1, expr2, EXPR_TIMES);	
			expr = new ExprOp(pos, env->GetST(), expr, new ExprNumber(pos, env->GetST(), 2), EXPR_DIV);
		} else {
			//Bimoleculare Reaction
			expr = new ExprOp(pos, env->GetST(), expr1, expr2, EXPR_TIMES);
			expr = new ExprOp(pos, env->GetST(), expr, new ExprNumber(pos, env->GetST(),rate), EXPR_TIMES);
		}
	}
	
	return expr;
}

//
double Bind::GetComb() 
{
	if (beta1 == beta2)
		return (0.5 * (double)beta1->GetCounter() * ((double)beta1->GetCounter() - 1.0));
	else
		return ((double)beta1->GetCounter() * (double)beta2->GetCounter());
}

//
double Bind::GetActualRate() 
{
	return ( GetComb() * rate  );
}

//
double Bind::Time()
{
   double time = HUGE_VAL;
	double comb = GetComb();

	if (comb == 0.0)
		return time;

	if ( rate == HUGE_VAL ) return -1.0;

	double rn = env->Random0To1();
	if (rn == 0.0)
		return time;

	time = ( (double)(1) / (comb * rate) ) * log( (double)(1) / rn ); 
   
   //time != HUGE_VAL -> comb > 0
   assert (time == HUGE_VAL || comb > 0);
   return time;
}

//
void Bind::Print(){
	cout << this->GetCategory() << " (" << beta1->GetId() << "," << binder1->GetSubject() << " - " 
	 	 << beta2->GetId() << "," << binder2->GetSubject() << " - " << rate << ")";
}

void Bind::GetReactants(vector<Entity *> &reactants){
	reactants.push_back(beta1);
	reactants.push_back(beta2);
}

void Bind::GetIterator(pair<Entity *, Entity *> *ent_pair, list<Complex *> *comp, pair<Iterator_Interface *, Iterator_Interface *> *it_pair){

	list<Complex *>::iterator comp_it;
	list<Complex *> *comp_list;
	Entity *e1;
	Entity *e2;
	bool inverted = false;

	//At least one entity has to be part of the complex
	assert(ent_pair->first != NULL);
	if(ent_pair->second != NULL){
		//Entities that take part in the reaction (this reaction) are both part
		//of the complex
		e1 = ent_pair->first;
		e2 = ent_pair->second;
		assert ((e1->GetId() == this->beta1->GetId() && e2->GetId() == this->beta2->GetId()) ||
				  (e2->GetId() == this->beta1->GetId() && e1->GetId() == this->beta2->GetId()));
	} else {
		//Of the two entity that take part at the reaction we found which one is
		//also part of the complex
		if (ent_pair->first->GetId() == this->beta1->GetId()){
			//beta1 is part of the complex
			e1 = ent_pair->first;
			e2 = beta2;
		} else {
			//beta2 is part of the complex
			assert(ent_pair->first->GetId() == this->beta2->GetId());
			inverted = true;
			e1 = ent_pair->first;
			e2 = beta1;
		}
	}


	//The first iterator look through all the complexes which e2 is in.
	CR_Two_Iterator *cr2i = new CR_Two_Iterator(env, BIM_AND_MONO, e1 == e2);
	if (inverted) {
		cr2i->SetIterator(new CR_One_Iterator(comp->begin(),comp->size(),e1->GetId(),env),2); 
		cr2i->SetIterator(env->GetComplexRel()->GetIteratorOne(e2->GetId()),1);
	} else {
		cr2i->SetIterator(new CR_One_Iterator(comp->begin(),comp->size(),e1->GetId(),env),1); 
		cr2i->SetIterator(env->GetComplexRel()->GetIteratorOne(e2->GetId()),2);
	}

	it_pair->first = new Simple_Iterator(NULL, cr2i);


	if(ent_pair->second != NULL){
	//If both entities are part of the complex we need two iterators
	//The second iterator looks through all the complexes which e1 is in except
	//for the complex that belongs to the comp list

		//Only one complex has to be present in the list
		assert(comp->size() == 1);
		comp_it = comp->begin();	

		//Get all the complexes which e1 is in
		comp_list = env->GetComplexRel()->GetComplexesList(e1->GetId());
		//Remove the complex that belongs to the comp list
		comp_list->remove(*comp_it);
		//Add it to the front of the list
		comp_list->push_front(*comp_it);
		//Set the comp_it iterator in order to make it point after the complex we
		//have just added to the comp_list list
		comp_it = comp_list->begin();
		assert(comp_it != comp_list->end());
		comp_it++;

		//Create the second iterator
		/*if(inf_action_iterator){
			it_pair->second = new InfActMap_Iterator(env->GetInfActMap()->GetActList()->begin(), env->GetInfActMap()->GetActList()->end(), env);	

		} else {
			it_pair->second = new ActMap_Iterator(env->GetActMap()->GetActList()->begin(), env->GetActMap()->GetActList()->end(), env);		
		}*/

		//Set properly the second iterator
		cr2i = new CR_Two_Iterator(env, BIM_AND_MONO, e1 == e2);
		if (inverted){
			cr2i->SetIterator(new CR_One_Iterator(comp->begin(), comp->size(), e2->GetId(), env),2);
			cr2i->SetIterator(new CR_One_Iterator(comp_it, comp_list->size(), e1->GetId(), env), 1);
		} else {
			cr2i->SetIterator(new CR_One_Iterator(comp->begin(), comp->size(), e2->GetId(), env),1);
			cr2i->SetIterator(new CR_One_Iterator(comp_it, comp_list->size(), e1->GetId(), env), 2);
		}
		//it_pair->second->SetIterator(NULL, 1);
		//it_pair->second->SetIterator(cr2i, 2);
		it_pair->second = new Simple_Iterator(NULL, cr2i);
	} else {
		//Second iterator is not needed
		it_pair->second = NULL;
	}	  
}


bool Bind::IsOnlyMono(int reactants[2]){
	reactants[0] = this->beta1->GetId();
	reactants[1] = this->beta2->GetId();

	return false;
}
	
Iterator_Interface * Bind::GetIterator(Entity *e1, Entity *e2, list<Complex *> *c1, list<Complex *> *c2, TwoIteratorKind iterator_kind){
	if (c1->empty() && c2->empty()){
		return new Simple_Iterator(NULL, NULL);
	}

	CR_Two_Iterator *cr2i = new CR_Two_Iterator(env, iterator_kind, e1 == e2);
		
	if (c1->empty()){
		cr2i->SetIterator(NULL,1);
	} else {
		cr2i->SetIterator(new CR_One_Iterator(c1->begin(), c1->size(), e1->GetId(), env),1);
	}

	if (c2->empty()){
		cr2i->SetIterator(NULL,2);
	} else {
		cr2i->SetIterator(new CR_One_Iterator(c2->begin(), c2->size(), e2->GetId(), env),2);
	}

	return new Simple_Iterator(NULL, cr2i);
}
