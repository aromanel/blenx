#ifndef BIND_H_INCLUDED
#define BIND_H_INCLUDED

#include <cstdlib>
#include <string>
#include <list>
#include <map>
#include <cmath>
#include "Entity.h"
#include "Define.h"

using namespace std;

class Entity;

//////////////////////////////////////////////////////
// BIND CLASS
// This class represents an action of type BIND.
//////////////////////////////////////////////////////

class Bind : public Element {

private:
	Entity *beta1;			// pointer to an Entity
	Entity *beta2;			// pointer to an Entity
	double rate;			// apparent rate of the action
	BBinders *binder1;		// binder of BETA1
	BBinders *binder2;		// binder of BETA2
	int num;				// number of binding

public:
	// constructor
	Bind(Environment* p_env, Entity *p_beta1, Entity *p_beta2, double p_rate, 
        BBinders *p_binder1, BBinders *p_binder2,
		  int p_num);
	// empty destructor
	~Bind() {};
	// returns the number of actions of this type. This number is calculated as product
	// of the cardinalities of BETA1 and BETA2
	double GetTotal();
	// calculates and returns the actual time of the action. The calculation is carried out
	// considering the actual information of the involved entities.
	double Time();
	double GetActualRate();
	double GetComb(); 
	// print the information of the action
	void Print();
	BBinders *GetBinder1() { return binder1; };
	BBinders *GetBinder2() { return binder2; };
	Entity *GetEntity1() { return beta1; };
	Entity *GetEntity2() { return beta2; };
	int GetNum() { return num; }
	//
	bool Is(Element *elem) { return (elem == this); };

	double GetRate(Iterator_Interface *c_iter);
	double GetBasalRate (Iterator_Interface *c_iter) { return rate; };

	Expr* GetExpr(Iterator_Interface *c_iter);

	void GetReactants(vector<Entity *> &reactants);
	void GetIterator(pair<Entity *, Entity *> *ent_pair, list<Complex *> *comp, pair<Iterator_Interface *, Iterator_Interface *> *it_pair);
	Iterator_Interface * GetIterator(Entity *e1, Entity *e2, list<Complex *> *c1, list<Complex *> *c2, TwoIteratorKind iterator_kind);

	bool IsOnlyMono(int reactants[2]);
};

#endif 


