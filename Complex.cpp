#include <iostream>
#include "Complex.h"
#include "Symbol_Table.h"
#include "Environment.h"

using namespace std;

//
Complex::Complex() {
	complex = NULL;
	number = 0;
	countdown = 0;
}

//
Complex::Complex(Complex_Graph *p_complex) {
	number = 1;
	complex = p_complex;
	countdown = complex->GetEnv()->countdown;
}

//
Complex::~Complex() {
	delete complex;
}

//
int Complex::GetNumber() { 
	return number; 
}

//
int Complex::GetNumberBinding(Entity *e1, Entity *e2, const string& b1, const string& b2) {
	return ( number * complex->GetBinding(e1,e2,b1,b2) );
}

//
int Complex::GetBinding(Entity *e1, Entity *e2, const string& b1, const string& b2) {
	return complex->GetBinding(e1,e2,b1,b2);
}

//
int Complex::GetNumberEntities(Entity *e) { 
	return ((int)(complex->GetNumber(e) * number));
}

size_t Complex::GetEntities(Entity *e) {
	return complex->GetNumber(e);
}


// 
void Complex::Print(ofstream *fstr) {
   (*fstr) << complex->GetEnv()->GetST()->GetMolNameFromID(id);
	if ( complex->ContainsInfiniteActions() )
		(*fstr) << " [contains immediate actions]";
	(*fstr) << endl;
	complex->Print(fstr);
}

// 
void Complex::Print(FILE *fstr) {
	fprintf(fstr, "%s", (complex->GetEnv()->GetST()->GetMolNameFromID(id)).c_str());
	if ( complex->ContainsInfiniteActions() )
		fprintf(fstr, " [contains immediate actions]");
	fprintf(fstr, "\n");
	complex->Print(fstr);
}


//
void Complex::PrintSTD() {
	std::vector<string> edge_array;
	std::vector<string>::iterator i;
	cout << id << " " << number << " - [ ";
	this->complex->Key(&edge_array);
	this->complex->Clean();
	for( i = edge_array.begin(); i != edge_array.end(); i++ ) 
		cout << (*i) << " " ;
	cout << "]";
}

//
void Complex::PrintDOT(int counter) 
{
	string fileName = "C_" + Utility::i2s(counter) + ".dot";
	ofstream *fstr_complex = new ofstream(fileName.c_str(),ios::out);
    (*fstr_complex) << "digraph M_" << Utility::i2s(this->GetId()) << " {" << endl;
	complex->PrintDOT(fstr_complex);
	(*fstr_complex) << "}" << endl;
	(*fstr_complex).close();
}

bool Complex::ContainsInfiniteActions()
{ 
   return complex->ContainsInfiniteActions(); 
}


//
void Complex::Eliminate(Entity *ent, vector<Map_Elem *> *e_list) {
	std::vector<string> edge_array;
	number--;
	complex->Key(&edge_array);
	complex->Clean();
	for(size_t i = 0 ; i < edge_array.size() ; i++)
		complex->DecBindElemInBindMap(edge_array[i],e_list);
	complex->InitIT();
	Entity *current = complex->GetIT();
	while( current != NULL ) {
		if ( current != ent ) 
		{
			int num = (int)complex->GetNumberNodes(current);
			current->DecCounter(num);
			current->GetEnv()->GetDependencies(current,*e_list);
		}
		current = complex->GetIT();
	}
}


void Complex::Inc() 
{ 
	if ( number == 0 && complex->GetEnv()->FLAG_COMPLEXES_ELIMINATION)
	{
		countdown = complex->GetEnv()->countdown;		
	}
	number++; 
}

void Complex::AddCounter(int counter) 
{ 
	if ( number == 0 && complex->GetEnv()->FLAG_COMPLEXES_ELIMINATION)
	{
		countdown = complex->GetEnv()->countdown;		
	}	
	number += counter; 
}
