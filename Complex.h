#ifndef COMPLEX_H_INCLUDED
#define COMPLEX_H_INCLUDED

#include <vector>
#include <iostream>
#include <algorithm>
#include "Complex_Graph.h"

class Entity;
class Complex_Graph;

class Complex {
private:
	Complex_Graph *complex;			// Complex graph
	int number;						// Number of this kind of complexes.
	int id;							// Complex identifier
	int countdown;					// The number of simulation steps after which eliminate the complex
										// after its population reaches and is stable at 0

	// Iterator variables
	int iterator_id;

public:	
	Complex();
	Complex(Complex_Graph *p_complex);
	~Complex();
	void SetId(int p_id) { id = p_id; };
	int GetId() { return id; };
	int GetIdN() { return id * (-1); }
	int GetNumber();
	void SetComplexToNULL() { complex = NULL; }
	void SetNumber(int n) { number = n; }
	int GetNumberEntities(Entity *e);
	size_t GetEntities(Entity *e);
	int GetNumberBinding(Entity *e1, Entity *e2, const string& b1, const string& b2);
	int GetBinding(Entity *e1, Entity *e2, const string& b1, const string& b2);
	void Dec() 
	{ 
		assert(number != 0);
		number--; 
	}
	void Inc();
	void AddCounter(int counter);
	Complex_Graph *GetComplex() { return complex; }
	void Print(ofstream *fstr);
	void Print(FILE *fstr);
	void PrintSTD();
	void PrintDOT(int counter);
	void Eliminate(Entity *ent, vector<Map_Elem *> *e_list);

	bool ContainsInfiniteActions();

	bool IsToEliminate()
	{
		return (countdown == 0 && number == 0);
	}

	void DecCountdown() 
	{ 
		if (number == 0 && countdown > 0)
			countdown--; 
	}
};


#endif // COMPLEX_H_INCLUDED


