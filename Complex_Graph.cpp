#include "Complex_Graph.h"
#include "Environment.h"
#include "Unbind.h"
#include "Bind.h"

using namespace std;

/////////////////////////////////////////////
// CG_EDGE METHODS DEFINITION
/////////////////////////////////////////////

//
CG_Edge::CG_Edge(Complex_Graph* graph) {
   my_graph = graph;
	ref = NULL;
	state = 0;
}

//
CG_Edge::CG_Edge(Complex_Graph* graph, const string& p_sx_binder, const string& p_rx_binder, CG_Node *p_ref) {
    my_graph = graph;
	sx_binder = p_sx_binder;
	rx_binder = p_rx_binder;
	ref = p_ref;
	state = 0;
}

//
CG_Edge::~CG_Edge() {

}

//
bool CG_Edge::Equal(CG_Edge *e) {
	if (sx_binder != e->sx_binder) return false;
	if (rx_binder != e->rx_binder) return false;
	if (ref->GetRef() != e->ref->GetRef()) return false;
	return true;
}

//
bool CG_Edge::Equal(Entity *ent_ref, const string& bsx, const string& brx)
{
	if (sx_binder != bsx) return false;
	if (rx_binder != brx) return false;
	if (ref->GetRef() != ent_ref) return false;
	return true;
}

//
string CG_Edge::GetStringEdge(Entity *ent)
{
	return (my_graph->GetEnv()->GetST()->GetEntNameFromID(ent->GetId()) + sx_binder + "," +
		my_graph->GetEnv()->GetST()->GetEntNameFromID(ref->GetRef()->GetId()) + rx_binder);
}

//
string CG_Edge::GetString(Entity *ent) {
	string res;
	int cond;
	if ( ent->GetId() < ref->GetRef()->GetId() ) cond = 1;
	else if ( ent->GetId() > ref->GetRef()->GetId() ) cond = 2;
	else {
		if ( sx_binder < rx_binder ) cond = 1;
		else cond = 2;
	}
	if ( cond == 1 )
		res = my_graph->GetEnv()->GetST()->GetEntNameFromID(ent->GetId()) + sx_binder + "," +
		my_graph->GetEnv()->GetST()->GetEntNameFromID(ref->GetRef()->GetId()) + rx_binder ;
	else
		res = my_graph->GetEnv()->GetST()->GetEntNameFromID(ref->GetRef()->GetId()) + rx_binder + "," +
		my_graph->GetEnv()->GetST()->GetEntNameFromID(ent->GetId()) + sx_binder ;
	return res;
}

//
void CG_Edge::Print(const string& name, ofstream *fstr) {
	(*fstr) << "( " + name + "," + sx_binder + "," +
		ref->node_name + "," + rx_binder + " )";
}

//
void CG_Edge::Print(const string& name, FILE *fstr) {
	fprintf(fstr,"(%s,%s,%s,%s)",name.c_str(),sx_binder.c_str(),ref->node_name.c_str(),rx_binder.c_str());
}

//
void CG_Edge::PrintDOT(const string& name, Entity *ent, ofstream *fstr)
{
	(*fstr) << name << "->" << ref->node_name << " [ label = \" " << ent->GetBBType(sx_binder) << "," <<
		ref->GetRef()->GetBBType(rx_binder) << " \" ] " << endl;
}



/////////////////////////////////////////////
// CG_NODE METHODS DEFINITION
/////////////////////////////////////////////

//
CG_Node::CG_Node(Complex_Graph* graph, Entity *p_e_ref) {
	my_graph = graph;
	e_ref = p_e_ref;
	mark = NULL;
	DFS_mark = 0;
	node_name = "";
}

//
CG_Node::~CG_Node() {
	CG_Edge *tmp;
	while(!edges.empty()) {
		tmp = edges.front();
		edges.pop_front();
		delete tmp;
	}
}

// the edge is added only if not present
void CG_Node::AddEdge(CG_Edge *edge) {
	edges.push_back(edge);
}

//
void CG_Node::DecBindAct( CG_Node *node, vector<Map_Elem *> *e_list ) {
	UIBind_elem *ui_elem;
	std::list<CG_Edge *>::iterator i;
	Entity *e;
	string key;

	for( i = edges.begin() ; i != edges.end() ; i++ ) {
		if ( (*i)->GetRef() != node) {
			e = (*i)->GetRef()->e_ref;
			key = (*i)->GetString(e_ref);
			if ( ( ui_elem = my_graph->FindInBindMap(key) ) != NULL ) {
				ui_elem->Dec();
				my_graph->GetEnv()->GetDependencies(ui_elem, *e_list);
			}
			else
				assert(false);
		}
	}
}

//
void CG_Node::IncBindAct( CG_Node *node, vector<Map_Elem *> *e_list )
{
	Map_Elem *map_elem = NULL;
	Element *elem = NULL;

	pair<Map_Elem *,Map_Elem *> p_action;
	UIBind_elem *ui_elem;
	std::list<CG_Edge *>::iterator i;
	BBinders *a, *b;
	Entity *e;
	string key;

	for( i = edges.begin() ; i != edges.end() ; i++ )
	{
		elem = NULL;
		if ( !(*i)->IsVisible() )
			continue;
		if ( (*i)->GetRef() != node )
		{
			e = (*i)->GetRef()->e_ref;
			key = (*i)->GetString(e_ref);
			if ( ( ui_elem = my_graph->FindInBindMap(key) ) == NULL )
			{
				a = e_ref->GetBB((*i)->GetSxBinder());
				b = e->GetBB((*i)->GetRxBinder());
				AffRuleComplex *aff = (AffRuleComplex *)(my_graph->GetAffinity(a->GetType(),b->GetType()));
				// controllo che l'affinita' esista e che sia del tipo EXP_RATES
				// Altri tipi di affinit\'a infatti non valgono per interbind e unbind
				if ( aff != NULL && aff->GetCategory() == RULE_EXP_RATES)
				{
					p_action = e_ref->AmbientAction(e,a,b,INTER_BIND,aff);
					if ( aff->unbind > 0. ) {
						elem = new Unbind(my_graph->GetEnv(),e_ref,e,aff->unbind,a,b);
					}

					if (elem != NULL) {
						map_elem = new Map_Elem(elem);
						if (aff->unbind == HUGE_VAL)
							my_graph->InsertInInfActMap(e_ref,e,elem);
						else
							my_graph->InsertInActMap(e_ref,e,map_elem);
					}
				}
				// creao il nuovo elemento contenente le azioni
				ui_elem = new UIBind_elem(map_elem,p_action.first,p_action.second);
				// inserisco il nuovo elemento nella mappa
				my_graph->InsertInBindMap(key, ui_elem);
			}
			else {
				ui_elem->Inc();
			}
			// aggiungo alla lista tutte le azioni che ci sono in ui_elem (diverse da NULL)
			if (e_list != NULL)
			{
				my_graph->GetEnv()->GetDependencies(ui_elem, *e_list);
			}
		}
	}
}

//
CG_Edge *CG_Node::ContainsEdge(Entity *e2, const string& b1, const string& b2) {
	std::list<CG_Edge *>::iterator i;
	for( i = edges.begin() ; i != edges.end() ; i++ ) {
		if ( (*i)->Equal(e2,b1,b2) )
			return (*i);
	}
	return NULL;
}

//
CG_Edge *CG_Node::DeleteEdge(Entity *e2, const string& b1, const string& b2) {
	std::list<CG_Edge *>::iterator i;
	CG_Edge *tmp = NULL;
	for( i = edges.begin() ; i != edges.end() ; i++ ) {
		if ( (*i)->Equal(e2,b1,b2) )
		{
			tmp = *i;
			i = edges.erase(i);
			break;
		}
	}
	return tmp;
}

//
CG_Edge *CG_Node::ContainsEdge(CG_Edge *edge)
{
	std::list<CG_Edge *>::iterator i;
	for( i = edges.begin() ; i != edges.end() ; i++ ) {
		if ( (*i)->Equal(edge) )
			return (*i);
	}
	return NULL;
}

//
bool CG_Node::DFSMatching(CG_Node *n2) {
	if ( DFS_mark != n2->DFS_mark ) return false;
	if ( DFS_mark == 1 ) return true;
	this->DFS_mark = n2->DFS_mark = 1;
	CG_Edge *edge2;
	bool res;
	std::list<CG_Edge *>::iterator i;
	for( i = edges.begin() ; i != edges.end() ; i++ ) {
		//if ( ( edge2 = n2->FindEdge((*i)->GetStringEdge(e_ref)) ) != NULL ) {
		if ( ( edge2 = n2->ContainsEdge(*i) ) != NULL ) {
			if ( edge2->GetRef()->edges.size() == (*i)->GetRef()->edges.size() ) {
				 res = (*i)->GetRef()->DFSMatching(edge2->GetRef());
				 if (!res) return false;
			}
			else return false;
		}
		else return false;
	}
	return true;
}

//
CG_Edge *CG_Node::FindEdge(const string& s_edge) {
	std::list<CG_Edge *>::iterator i;
	for( i = edges.begin() ; i != edges.end() ; i++ )
		if ( (*i)->GetStringEdge(e_ref) == s_edge ) return (*i);
	return NULL;
}

//
void CG_Node::ChangeEdgesLabels( Mapping *ch_mapping) {
	std::list<CG_Edge *>::iterator i;
	std::list<CG_Edge *>::iterator j;
	string new_label;
	for(i=edges.begin();i!=edges.end();i++) {
		new_label = ch_mapping->GetMapping( (*i)->GetSxBinder() );
		if ( new_label != "" ) {
			(*i)->SetSxBinder(new_label);
			for(j = (*i)->GetRef()->edges.begin();j != (*i)->GetRef()->edges.end(); j++) {
				if ( (*j)->GetSxBinder() == (*i)->GetRxBinder() ) {
					(*j)->SetRxBinder(new_label);
					break;
				}
			}
		}
	}
}


// BFS search
int CG_Node::VisitFromNode()
{
	CG_Node *node;
	int nodes = 1;
	std::queue<CG_Node *> nodes_queue;
	nodes_queue.push(this);

	std::list<CG_Edge *>::iterator i;
	while(!nodes_queue.empty())
	{
		node = nodes_queue.front();
		for ( i = node->edges.begin() ; i != node->edges.end() ; i++ )
		{
			if ( (*i)->IsVisible() ) {
				if ( (*i)->GetRef()->DFS_mark == 0 )
				{
					(*i)->GetRef()->DFS_mark = 1;
					nodes_queue.push((*i)->GetRef());
					nodes++;
				}
			}
		}
		node->DFS_mark = 2;
		nodes_queue.pop();
	}
	return nodes;
}

/////////////////////////////////////////////
// COMPLEX_GRAPH METHODS DEFINITION
/////////////////////////////////////////////


//
CG_Node *Complex_Graph::Subs(Entity *old_e, Entity *new_e, Iterator_Interface *c_iter) {
	//if (old_e == new_e) return NULL;
	CG_Node *node = NULL;
	hash_map< int,list<CG_Node *> >::iterator i;
	list<CG_Node *>::iterator j;
	if ( ( i = node_hmap.find(old_e->GetId()) ) != node_hmap.end() ) {

		if ( c_iter != NULL )
		{
			assert(c_iter->GetCategory() == IT_COMPLEX_GRAPH);
			node = ((CG_Iterator *)c_iter)->GetNode();
		}
		else
		{
			j = (*i).second.begin();
			int rn = env->Random0UptoN((int)(*i).second.size());
			while( rn > 0 ) {
				rn--;
				j++;
			}
			node = *j;
		}

		//node->SetRef(new_e);
	}
	return node;
}

//
pair<CG_Node *,CG_Node *> Complex_Graph::SubsBind(Entity *e1, Entity *be1,Entity *e2, Entity *be2,
								const string& b1, const string& b2, Iterator_Interface *c_iter)
{
	CG_Edge *edge = NULL;
	hash_map< int,list<CG_Node *> >::iterator i;
	list<CG_Node *> marked_node;
	list<CG_Node *>::iterator j;
	pair<CG_Node *,CG_Node *> bind_pair;
	if ( ( i = node_hmap.find(e1->GetId()) ) != node_hmap.end() ) {

		if ( c_iter != NULL )
		{
			assert(c_iter->GetCategory() == IT_COMPLEX_GRAPH_BIND);
			bind_pair = ((CG_Bind_Iterator *)c_iter)->GetBindPair();
		}
		else
		{
			int bind_number = this->GetBinding(e1,e2,b1,b2);
			int rn = env->Random0UptoN(bind_number);
			for( j = (*i).second.begin() ; j != (*i).second.end() ; j++ ) {
				if ( ( edge = (*j)->ContainsEdge(e2,b1,b2) ) != NULL ) {
					if ( edge->GetRef()->DFS_mark == 0 ) {
						if ( rn == 0 ) break;
						rn--;
						// lo marco perche' il bind potrebbe essere una cosa del
						// tipo AbAb e quindi devo contarlo una sola volta
						edge->GetRef()->DFS_mark = 1;
						marked_node.push_back(edge->GetRef());
					}
				}
			}

			assert( j != (*i).second.end() && edge != NULL );

			bind_pair.first = *j;
			bind_pair.second = edge->GetRef();

			// ripulisco le marcature
			for( j = marked_node.begin() ; j != marked_node.end() ; j++ )
				(*j)->DFS_mark = 0;
		}

		//bind_pair.first->SetRef(be1);
		//bind_pair.second->SetRef(be2);

	}
	else {
		assert(false);
	}

	return bind_pair;
}

//
pair<CG_Node *,CG_Node *> Complex_Graph::Subs2(Entity *old_e1, Entity *new_e1,Entity *old_e2, Entity *new_e2,
											   Iterator_Interface *c_iter)
{
	pair<CG_Node *,CG_Node *> p;
	list<CG_Node *>::iterator j1;
	CG_Node *tmp;
	hash_map< int,list<CG_Node *> >::iterator i;
	if (old_e1 == old_e2) {
		if ( ( i = node_hmap.find(old_e1->GetId()) ) != node_hmap.end() ) {

			if ( c_iter != NULL )
			{
				assert(c_iter->GetCategory() == IT_COMPLEX_REL_TWO);
				CR_One_Iterator *i_one = (CR_One_Iterator *)c_iter->GetIterator(1);
				p.first = ((CG_Iterator *)i_one->GetIterator(1))->GetNode();
				CR_One_Iterator *i_two = (CR_One_Iterator *)c_iter->GetIterator(2);
				p.second = ((CG_Iterator *)i_two->GetIterator(1))->GetNode();
			}
			else
			{
				int rn = env->Random0UptoN((int)(*i).second.size());
				int rn1 = env->Random0UptoN((int)(*i).second.size() - 1);
				j1 = (*i).second.begin();
				while( rn > 0 ) {
					rn--; j1++;
				}
				tmp = (*j1);
				(*i).second.erase(j1);
				j1 = (*i).second.begin();
				while( rn1 > 0 ) {
					rn1--; j1++;
				}
				(*i).second.push_back(tmp);
				p.first = tmp;
				p.second = *j1;
			}

			//p.first->SetRef(new_e1);
			//p.second->SetRef(new_e2);
		}
	}
	else
	{
		Iterator_Interface* iter1 = NULL;
		Iterator_Interface* iter2 = NULL;
		if (c_iter != NULL)
		{
			iter1 = c_iter->GetIterator(1);
			iter2 = c_iter->GetIterator(2);
		}

		if (iter1 == NULL){
			p.first = Subs(old_e1, new_e1, NULL);
		} else {
			p.first = Subs(old_e1, new_e1, iter1->GetIterator(1));
		}

		if (iter2 == NULL){
			p.second = Subs(old_e2 ,new_e2, NULL);
		} else {
			p.second = Subs(old_e2 ,new_e2, iter2->GetIterator(1));
		}

	}
	return p;
}

//
Complex_Graph::Complex_Graph(Environment* p_env) {
	env = p_env;
	nodes = edges = 0;
	iter = node_hmap.begin();
}

//
Complex_Graph::Complex_Graph(Environment* p_env, Entity *rx, Entity *sx, const string& rx_n, const string& sx_n, vector<Map_Elem *> *e_list) {
	env = p_env;
	CG_Node *node_rx = new CG_Node(this, rx);
	CG_Node *node_sx = new CG_Node(this, sx);
	CG_Edge *edge_rx = new CG_Edge(this, rx_n,sx_n,node_sx);
	CG_Edge *edge_sx = new CG_Edge(this, sx_n,rx_n,node_rx);
	node_rx->AddEdge(edge_rx);
	node_sx->AddEdge(edge_sx);
	node_hmap[sx->GetId()].push_front(node_sx);
	node_hmap[rx->GetId()].push_front(node_rx);
	nodes = edges = 2;
	iter = node_hmap.begin();
}


//
Complex_Graph::~Complex_Graph() {
	hash_map< int,list<CG_Node *> >::iterator i;
	CG_Node *tmp;
	for( i = node_hmap.begin() ; i != node_hmap.end() ; i++ ) {
		while ( !(*i).second.empty() ) {
			tmp = (*i).second.front();
			(*i).second.pop_front();
			delete(tmp);
		}
	}
}

//
size_t Complex_Graph::GetNumber(Entity *e) {
	hash_map< int,list<CG_Node *> >::iterator i;
	if ( ( i = node_hmap.find(e->GetId()) ) == node_hmap.end() )
		return 0;
	return (*i).second.size();
}

//
int Complex_Graph::GetBinding(Entity *e1, Entity *e2, const string& b1, const string& b2) {
	//std::vector<string> v_edges;
	//std::vector<string>::iterator i;
	int counter = 0;
	/*Key(&v_edges);
	string key = e1->GetKey(e2,b1,b2);
	for( i = v_edges.begin() ; i!= v_edges.end() ; i++ ) {
		if ( key == (*i) ) counter++;
	}
	this->Clean();*/
	COMPLEX_GRAPH_HMAP::iterator iter;
	std::list<CG_Node *>::iterator iter_nodes_begin;
	std::list<CG_Node *>::iterator iter_nodes_end;
	CG_Edge *current_edge;
	if ( ( iter = this->node_hmap.find(e1->GetId()) ) != this->node_hmap.end())
	{
		// iteratori alla lista di nodi
		iter_nodes_begin = iter->second.begin();
		iter_nodes_end = iter->second.end();

		std::set< std::pair<CG_Node*, CG_Node*> > edge_set;

		while ( iter_nodes_begin != iter_nodes_end )
		{
			current_edge = (*iter_nodes_begin)->ContainsEdge(e2,b1,b2);
			if ( current_edge != NULL )
			{
				std::pair<CG_Node*, CG_Node*> edge_signature;
				CG_Node* node1 = *iter_nodes_begin;
				CG_Node* node2 = current_edge->GetRef();

				if (node1 < node2)
				{
					edge_signature.first = node1;
					edge_signature.second = node2;
				}
				else
				{
					edge_signature.first = node2;
					edge_signature.second = node1;
				}

				if (edge_set.find(edge_signature) == edge_set.end())
				{
					edge_set.insert(edge_signature);
					counter++;
				}
			}
			iter_nodes_begin++;
		}
	}
	return counter;
}

// BFS implementation
void Complex_Graph::DeleteSubgraph(CG_Node *node)
{

}

// BFS implementation
CG_Node *Complex_Graph::Copy(CG_Node *node, hash_map< int,list<CG_Node *> > *n_map, CG_Node **ref, Entity *new_e,
							 int *num_nodes, int *num_edges, std::vector<string> *key_v, CG_Node **ref_node, Complex_Graph *n_complex)
{
	std::queue<CG_Node *> nodes_queue;

	CG_Node *n_node = new CG_Node(n_complex, node->GetRef());
	(*n_map)[n_node->GetRef()->GetId()].push_front(n_node);

	if (ref_node != NULL)
		if ((*ref_node) == node)
			(*ref_node) = n_node;
	if ((*ref) == node)  {
		(*ref) = n_node;
		n_node->SetRef(new_e);
	}

	node->mark = n_node;
	node->DFS_mark = 1;
	nodes_queue.push(node);
	(*num_nodes)++;

	CG_Edge *n_edge;
	CG_Node *tmp;
	std::list<CG_Edge *>::iterator i;
	while(!nodes_queue.empty())
	{
		node = nodes_queue.front();
		for ( i = node->edges.begin() ; i != node->edges.end() ; i++ )
		{
			if ( (*i)->IsVisible() ) {
				if ( (*i)->GetRef()->DFS_mark == 0 )
				{
					n_node = new CG_Node(n_complex,(*i)->GetRef()->GetRef());
					(*n_map)[n_node->GetRef()->GetId()].push_front(n_node);

					if (ref_node != NULL)
						if ((*ref_node) == (*i)->GetRef())
							(*ref_node) = n_node;
					if ((*ref) == (*i)->GetRef())  {
						(*ref) = n_node;
						n_node->SetRef(new_e);
					}

					(*i)->GetRef()->mark = n_node;
					(*i)->GetRef()->DFS_mark = 1;
					nodes_queue.push((*i)->GetRef());
					tmp = n_node;
					(*num_nodes)++;
				}
				else if ( (*i)->GetRef()->DFS_mark > 0 )
				{
					tmp = (*i)->GetRef()->mark;
				}

				//if (!this->GetEnv()->FLAG_COMPLEXES_SIGNATURE)
					//key_v->push_back( (*i)->GetString( node->GetRef() ) );
				n_edge = new CG_Edge(n_complex,(*i)->GetSxBinder(),(*i)->GetRxBinder(),tmp);
				node->mark->AddEdge(n_edge);
				(*num_edges)++;
			}
		}
		node->DFS_mark = 2;
		nodes_queue.pop();
	}
	return NULL;
}

CG_Node *Complex_Graph::Copy(CG_Node *node, hash_map< int,list<CG_Node *> > *n_map, CG_Node **ref1, CG_Node **ref2,
							 Entity *new_e1, Entity *new_e2, int *num_nodes, int *num_edges, std::vector<string> *key_v,
							 Complex_Graph *n_complex)
{
	std::queue<CG_Node *> nodes_queue;

	CG_Node *n_node = new CG_Node(n_complex, node->GetRef());
	(*n_map)[n_node->GetRef()->GetId()].push_front(n_node);

	if ( (*ref1) == node) {
		(*ref1) = n_node;
		n_node->SetRef(new_e1);
	}
	else if ( (*ref2) == node) {
		(*ref2) = n_node;
		n_node->SetRef(new_e2);
	}

	node->mark = n_node;
	node->DFS_mark = 1;
	nodes_queue.push(node);
	(*num_nodes)++;

	CG_Edge *n_edge;
	CG_Node *tmp;
	std::list<CG_Edge *>::iterator i;
	while(!nodes_queue.empty())
	{
		node = nodes_queue.front();
		for ( i = node->edges.begin() ; i != node->edges.end() ; i++ )
		{
			if ( (*i)->IsVisible() ) {
				if ( (*i)->GetRef()->DFS_mark == 0 )
				{
					CG_Node *n_node = new CG_Node(n_complex,(*i)->GetRef()->GetRef());
					(*n_map)[n_node->GetRef()->GetId()].push_front(n_node);

					if ( (*ref1) == (*i)->GetRef()) {
						(*ref1) = n_node;
						n_node->SetRef(new_e1);
					}
					else if ( (*ref2) == (*i)->GetRef()) {
						(*ref2) = n_node;
						n_node->SetRef(new_e2);
					}

					(*i)->GetRef()->mark = n_node;
					(*i)->GetRef()->DFS_mark = 1;
					nodes_queue.push((*i)->GetRef());
					tmp = n_node;
					(*num_nodes)++;
				}
				else if ( (*i)->GetRef()->DFS_mark > 0 )
				{
					tmp = (*i)->GetRef()->mark;
				}

				//if (!this->GetEnv()->FLAG_COMPLEXES_SIGNATURE)
					//key_v->push_back( (*i)->GetString( node->GetRef() ) );
				n_edge = new CG_Edge(n_complex,(*i)->GetSxBinder(),(*i)->GetRxBinder(),tmp);
				node->mark->AddEdge(n_edge);
				(*num_edges)++;
			}
		}
		node->DFS_mark = 2;
		nodes_queue.pop();
	}
	return NULL;
}


// DFS
/*CG_Node *Complex_Graph::Copy(CG_Node *node, hash_map< int,list<CG_Node *> > *n_map, CG_Node **ref, Entity *new_e,
							 int *num_nodes, int *num_edges, std::vector<string> *key_v, CG_Node **ref_node, Complex_Graph *n_complex)
{
	if (node->mark != NULL) return node->mark;
	(*num_nodes)++;
	node->DFS_mark = 1;
	CG_Node *n_node = new CG_Node(n_complex, node->GetRef());
	// verifico se il nodo corrente e' il nodo ref_node
	// In caso affermatico imposto ref_node con il nuovo nodo corrispondente
	if (ref_node != NULL)
		if ((*ref_node) == node)
			(*ref_node) = n_node;
	if ( (*ref) == node)  {
		(*ref) = n_node;
		n_node->SetRef(new_e);
	}
	node->mark = n_node;
	CG_Edge *n_edge;
	(*n_map)[n_node->GetRef()->GetId()].push_front(n_node);
	std::list<CG_Edge *>::iterator i;
	for ( i = node->edges.begin() ; i != node->edges.end() ; i++ ) {
		if ( (*i)->IsVisible() ) {
			(*num_edges)++;
			if ( (*i)->GetRef()->DFS_mark == 1 && !this->GetEnv()->FLAG_COMPLEXES_SIGNATURE)
				key_v->push_back( (*i)->GetString( node->GetRef() ) );
			n_edge = new CG_Edge(n_complex, (*i)->GetSxBinder(),(*i)->GetRxBinder(),Copy((*i)->GetRef(),n_map,ref,
				new_e,num_nodes,num_edges,key_v,ref_node,n_complex));
			n_node->AddEdge(n_edge);
		}
	}
	node->DFS_mark = 2;
	return n_node;
}

CG_Node *Complex_Graph::Copy(CG_Node *node, hash_map< int,list<CG_Node *> > *n_map, CG_Node **ref1, CG_Node **ref2,
							 Entity *new_e1, Entity *new_e2, int *num_nodes, int *num_edges, std::vector<string> *key_v,
							 Complex_Graph *n_complex)
{
	if (node->mark != NULL) return node->mark;
	(*num_nodes)++;
	node->DFS_mark = 1;
	CG_Node *n_node = new CG_Node(n_complex, node->GetRef());
	if ( (*ref1) == node) {
		(*ref1) = n_node;
		n_node->SetRef(new_e1);
	}
	else if ( (*ref2) == node) {
		(*ref2) = n_node;
		n_node->SetRef(new_e2);
	}
	node->mark = n_node;
	CG_Edge *n_edge;
	(*n_map)[n_node->GetRef()->GetId()].push_front(n_node);
	std::list<CG_Edge *>::iterator i;
	for ( i = node->edges.begin() ; i != node->edges.end() ; i++ ) {
		if ( !(*i)->IsVisible() ) continue;
		(*num_edges)++;
		if ( (*i)->GetRef()->DFS_mark == 1 && !this->GetEnv()->FLAG_COMPLEXES_SIGNATURE)
			key_v->push_back( (*i)->GetString( node->GetRef() ) );
		n_edge = new CG_Edge(n_complex, (*i)->GetSxBinder(),(*i)->GetRxBinder(),Copy((*i)->GetRef(),n_map,ref1,ref2,new_e1,new_e2,
			num_nodes,num_edges,key_v,n_complex));
		n_node->AddEdge(n_edge);
	}
	node->DFS_mark = 2;
	return n_node;
}*/

//
Complex_Graph *Complex_Graph::CopySub(Entity *new_e, CG_Node **ref, std::vector<string> *key_v, CG_Node **ref_node) {
	Complex_Graph *n_complex = new Complex_Graph(env);
	int n_nodes,n_edges;
	n_nodes = n_edges = 0;
	Copy((*ref),&(n_complex->node_hmap),ref,new_e,&n_nodes,&n_edges,key_v,ref_node,n_complex);
	n_complex->nodes = n_nodes;
	n_complex->edges = n_edges;
	return n_complex;
}

//
Complex_Graph *Complex_Graph::CopySub(Entity *new_e1, CG_Node **ref1, Entity *new_e2, CG_Node **ref2,
									  std::vector<string> *key_v)
{
	Complex_Graph *n_complex = new Complex_Graph(env);
	int n_nodes,n_edges;
	n_nodes = n_edges = 0;
	n_nodes = n_edges = 0;
	Copy((*ref1),&(n_complex->node_hmap),ref1,ref2,new_e1,new_e2,&n_nodes,&n_edges,key_v,n_complex);
	n_complex->nodes = n_nodes;
	n_complex->edges = n_edges;
	return n_complex;
}

//
void Complex_Graph::Clean() {
	hash_map< int,list<CG_Node *> >::iterator i;
	std::list<CG_Node *>::iterator j;
	for( i = node_hmap.begin() ; i != node_hmap.end() ; i++ )
		for ( j = (*i).second.begin() ; j != (*i).second.end() ; j++ ) {
			(*j)->mark = NULL;
			(*j)->DFS_mark = 0;
		}
}

void Complex_Graph::Edges(CG_Node *node, std::vector<string> *key_v, int *n_node) {
	std::list<CG_Edge *>::iterator i;
	node->DFS_mark = 1;
	(*n_node)++;
	for( i = node->edges.begin() ; i != node->edges.end() ; i++ ) {
		if ( !(*i)->IsVisible() ) continue;
		if ( (*i)->GetRef()->DFS_mark == 1 )
			key_v->push_back( (*i)->GetString( node->GetRef() ) );
		else if ( (*i)->GetRef()->DFS_mark == 0 )
				Edges((*i)->GetRef(),key_v,n_node);
	}
	node->DFS_mark = 2;
}

//
void Complex_Graph::Key(std::vector<string> *key) {
	int n_node = 0;
	Edges(node_hmap.begin()->second.front(),key,&n_node);
	std::sort(key->begin(),key->end());
}

// li attacca insieme
void Complex_Graph::Merge(Complex_Graph *c_complex) {
	hash_map< int,list<CG_Node *> >::iterator i;
	list<CG_Edge *>::iterator k;
	std::list<CG_Node *>::iterator j;
	for( i = c_complex->node_hmap.begin() ; i != c_complex->node_hmap.end() ; i++ ) {
		while(!(*i).second.empty()) {
			node_hmap[(*i).second.front()->GetRef()->GetId()].push_back((*i).second.front());
			(*i).second.front()->my_graph = this;
			for( k = (*i).second.front()->edges.begin() ; k != (*i).second.front()->edges.end() ; k++ )
			{
				(*k)->SetMyGraph(this);
			}
			(*i).second.pop_front();
		}
	}
	nodes += c_complex->nodes;
	edges += c_complex->edges;
}

//
void Complex_Graph::AddEdge(const string& bind1, const string& bind2,
							CG_Node *node1, CG_Node *node2)
{
	CG_Edge *edge1 = new CG_Edge(this,bind1,bind2,node2);
	CG_Edge *edge2 = new CG_Edge(this,bind2,bind1,node1);
	node1->AddEdge(edge1);
	node2->AddEdge(edge2);
	edges += 2;
}

//
void Complex_Graph::AddEdgeNode(const string& bind1, const string& bind2,
								CG_Node *node1, CG_Node *node2) {
	node_hmap[node1->GetRef()->GetId()].push_back(node1);
	node_hmap[node2->GetRef()->GetId()].push_back(node2);
	CG_Edge *edge1 = new CG_Edge(this, bind1,bind2,node2);
	CG_Edge *edge2 = new CG_Edge(this, bind2,bind1,node1);
	node1->AddEdge(edge1);
	node2->AddEdge(edge2);
	nodes += 2;
	edges += 2;
}

//
void Complex_Graph::AddEdgeMaybeNode(const string& bind1, const string& bind2,
								CG_Node *node1, CG_Node *node2)
{
	hash_map< int, std::list<CG_Node *> >::iterator i;
	std::list<CG_Node *>::iterator j;
	i = node_hmap.find(node1->GetRef()->GetId());
	if (i == node_hmap.end()) {
		node_hmap[node1->GetRef()->GetId()].push_back(node1);
		nodes++;
	}
	else {
		for(j=(*i).second.begin();j!=(*i).second.end();j++)
			if((*j) == node1)
				break;
		if (j==(*i).second.end()) {
			node_hmap[node1->GetRef()->GetId()].push_back(node1);
			nodes++;
		}
	}
	i = node_hmap.find(node2->GetRef()->GetId());
	if (i == node_hmap.end() ) {
		node_hmap[node2->GetRef()->GetId()].push_back(node2);
		nodes++;
	}
	else {
		for(j=(*i).second.begin();j!=(*i).second.end();j++)
			if((*j) == node2)
				break;
		if (j==(*i).second.end()) {
			node_hmap[node2->GetRef()->GetId()].push_back(node2);
			nodes++;
		}
	}
	CG_Edge *edge1 = new CG_Edge(this, bind1,bind2,node2);
	CG_Edge *edge2 = new CG_Edge(this, bind2,bind1,node1);
	node1->AddEdge(edge1);
	node2->AddEdge(edge2);
	edges += 2;
}

//
void Complex_Graph::AddEdge(Entity *new_e, const string& bind1,const string& bind2,
							CG_Node *node1) {
	CG_Node *node2 = new CG_Node(this, new_e);
	CG_Edge *edge1 = new CG_Edge(this, bind1,bind2,node2);
	CG_Edge *edge2 = new CG_Edge(this, bind2,bind1,node1);
	node1->AddEdge(edge1);
	node2->AddEdge(edge2);
	node_hmap[new_e->GetId()].push_back(node2);
	nodes += 1;
	edges += 2;
}

//
Complex_Graph *Complex_Graph::CopyOne(Entity *old_e, Entity *new_e, CG_Node **old_node, CG_Node **new_node,
						std::vector<string> *key_v, Iterator_Interface *c_iter)
{
	Complex_Graph *cg;
	//(*new_node) = (*old_node) = Subs(old_e,new_e,c_iter);
	(*old_node)->SetRef(new_e);
	cg = CopySub(new_e,new_node,key_v,NULL);
	if ((*old_node) != NULL) (*old_node)->SetRef(old_e);
	//std::sort(key_v->begin(),key_v->end());
	return cg;
}


//
CG_Node *Complex_Graph::Key(Entity *old_e, Entity *new_e, std::vector<string> *v_key) {
	CG_Node *node = Subs(old_e,new_e,NULL);
	Key(v_key);
	if (node != NULL) node->SetRef(old_e);
	return node;
}

//
Complex_Graph *Complex_Graph::CopyTwo(Entity *old_e1, Entity *new_e1, CG_Node **old_node1, CG_Node **new_node1,
		Entity *old_e2, Entity *new_e2, CG_Node **old_node2, CG_Node **new_node2, std::vector<string> *key_v,
		Iterator_Interface *c_iter)
{
	Complex_Graph *cg;
	//pair<CG_Node *,CG_Node *> node_pair;
	//node_pair.first = node_pair.second = NULL;
	//node_pair = Subs2(old_e1,new_e1,old_e2,new_e2,c_iter);
	//(*new_node1) = (*old_node1) = node_pair.first;
	//(*new_node2) = (*old_node2) = node_pair.second;
	(*old_node1)->SetRef(new_e1);
	(*old_node2)->SetRef(new_e2);
	cg = CopySub(new_e1,new_node1,new_e2,new_node2,key_v);
    if (*old_node1 != NULL) (*old_node1)->SetRef(old_e1);
	if (*old_node2 != NULL) (*old_node2)->SetRef(old_e2);
	//std::sort(key_v->begin(),key_v->end());
	return cg;
}

//
pair<CG_Node *,CG_Node *> Complex_Graph::Key(Entity *old_e1, Entity *new_e1, Entity *old_e2,
											 Entity *new_e2, std::vector<string> *v_key)
{
	pair<CG_Node *,CG_Node *> p;
	p.first = p.second = NULL;
	p = Subs2(old_e1,new_e1,old_e2,new_e2,NULL);
	Key(v_key);
	if (p.first != NULL) p.first->SetRef(old_e1);
	if (p.second != NULL) p.second->SetRef(old_e2);
	return p;
}

//
Complex_Graph *Complex_Graph::CopyInterBind(Entity *old_e1, Entity *new_e1, const string& b1, CG_Node **old_node1,
		CG_Node **new_node1, Entity *old_e2, Entity *new_e2, const string& b2, CG_Node **old_node2,
		CG_Node **new_node2, std::vector<string> *key_v, Iterator_Interface *c_iter)
{
	Complex_Graph *cg;
	//pair<CG_Node *,CG_Node *> node_pair;
	//node_pair.first = node_pair.second = NULL;
	//node_pair= SubsBind(old_e1,new_e1,old_e2,new_e2,b1,b2,c_iter);
	//(*new_node1) = (*old_node1) = node_pair.first;
	//(*new_node2) = (*old_node2) = node_pair.second;
	(*old_node1)->SetRef(new_e1);
	(*old_node2)->SetRef(new_e2);
	cg = CopySub(new_e1,new_node1,new_e2,new_node2,key_v);
    if ((*old_node1) != NULL) (*old_node1)->SetRef(old_e1);
	if ((*old_node2) != NULL) (*old_node2)->SetRef(old_e2);
	//std::sort(key_v->begin(),key_v->end());
	return cg;
}

//
pair<CG_Node *,CG_Node *> Complex_Graph::Key(Entity *e1, Entity *be1, Entity *e2, Entity *be2, const string& b1,
											 const string& b2, std::vector<string> *v_key)
{
	pair<CG_Node *,CG_Node *> node_pair;
	node_pair.first = node_pair.second = NULL;
	node_pair= SubsBind(e1,be1,e2,be2,b1,b2,NULL);
	Key(v_key);
	if (node_pair.first != NULL) node_pair.first->SetRef(e1);
	if (node_pair.second != NULL) node_pair.second->SetRef(e2);
	return node_pair;
}

//
pair<Complex_Graph *,Complex_Graph *> Complex_Graph::CopyUnbind(Entity *old_e1, Entity *new_e1, const string& b1,
		CG_Node **old_node1, CG_Node **new_node1, Entity *old_e2, Entity *new_e2, const string& b2, CG_Node **old_node2,
		CG_Node **new_node2, std::vector<string> *key_v1, std::vector<string> *key_v2, Iterator_Interface *c_iter)
{
	pair<Complex_Graph *,Complex_Graph *> graph_pair;
	//pair<CG_Node *,CG_Node *> node_pair;
    pair<CG_Edge *,CG_Edge *> edge_pair;

	// initialization of all the pair structure
	//node_pair.first = node_pair.second = NULL;
	edge_pair.first = edge_pair.second = NULL;
	graph_pair.first = graph_pair.second = NULL;

	//node_pair = SubsBind(old_e1,new_e1,old_e2,new_e2,b1,b2,c_iter);
    //(*new_node1) = (*old_node1) = node_pair.first;
	//(*new_node2) = (*old_node2) = node_pair.second;
	(*old_node1)->SetRef(new_e1);
	(*old_node2)->SetRef(new_e2);

	// seleziono i due archi che uniscono i due nodi selezionati
	edge_pair.first = (*old_node1)->ContainsEdge(new_e2,b1,b2);
	edge_pair.second = (*old_node2)->ContainsEdge(new_e1,b2,b1);

	// setto il vertice selezionato come invisibile
	edge_pair.second->SetInvisible();
	edge_pair.first->SetInvisible();

	// faccio la DFS a partire da uno dei nodi del vertice selezionato
	graph_pair.first = CopySub(new_e1,new_node1,key_v1,new_node2);

	if ( graph_pair.first->GetNodes() != nodes ) {
		// significa che ho un'altra componente connessa, e che
		// esattamente essa contiene l'altro nodo dell'arco che ho reso invisibile
		graph_pair.second = CopySub(new_e2,new_node2,key_v2,NULL);
		//if ( key_v2->empty() ) {
		if ( graph_pair.second->GetEdges() == 0 ) {
			delete graph_pair.second;
			graph_pair.second = NULL;
			(*new_node2) = NULL;
		}
		else
		{
			//std::sort(key_v2->begin(),key_v2->end());
		}
	}

	//if ( key_v1->empty() ) {

	if ( graph_pair.first->GetEdges() == 0 ) {
		delete graph_pair.first;
		graph_pair.first = NULL;
		(*new_node1) = NULL;
	}
	else
	{
		//std::sort(key_v1->begin(),key_v1->end());
	}

	// setto il vertice selezionato come invisibile
	edge_pair.second->SetVisible();
	edge_pair.first->SetVisible();

	// reimposto i valori originali
	if ((*old_node1) != NULL) (*old_node1)->SetRef(old_e1);
	if ((*old_node2) != NULL) (*old_node2)->SetRef(old_e2);

	return graph_pair;
}

//
pair<CG_Edge *,CG_Edge *> Complex_Graph::Key(Entity *e1, Entity *be1, Entity *e2, Entity *be2, const string& b1,
											 const string& b2, std::vector<string> *v_key1, std::vector<string> *v_key2,
											 bool *split)
{
	pair<CG_Node *,CG_Node *> node_pair;
    pair<CG_Edge *,CG_Edge *> edge_pair;
	node_pair.first = node_pair.second = NULL;
	edge_pair.first = edge_pair.second = NULL;
	node_pair = SubsBind(e1,be1,e2,be2,b1,b2,NULL);

	// seleziono i due archi che uniscono i due nodi selezionati
	edge_pair.first = node_pair.first->ContainsEdge(be2,b1,b2);
	edge_pair.second = node_pair.second->ContainsEdge(be1,b2,b1);

	// setto il vertice selezionato come invisibile
	edge_pair.second->SetInvisible();
	edge_pair.first->SetInvisible();

	// faccio la DFS a partire da uno dei nodi del vertice selezionato
	int n_node = 0;
	Edges(node_pair.first,v_key1,&n_node);
	//std::sort(v_key1->begin(),v_key1->end());

	// se il numero di nodi ritornati e' uguale al numero di nodi del grafo
	// allora ho ancora un'unica componente connessa
	if ( n_node == nodes ) (*split) = false;
	else {
		// altrimenti significa che ho un'altra componente connessa, e che
		// esattamente essa contiene l'altro nodo dell'arco che ho reso invisibile
		Edges(node_pair.second,v_key2,&(n_node = 0));
		//std::sort(v_key2->begin(),v_key2->end());
	    (*split) = true;
	}

	// reimposto i valori originali
	if (node_pair.first != NULL) node_pair.first->SetRef(e1);
	if (node_pair.second != NULL) node_pair.second->SetRef(e2);

	return edge_pair;
}


//
bool Complex_Graph::Contains(Entity *ent) {
	return ( node_hmap.find(ent->GetId()) != node_hmap.end() );
}

//
bool Complex_Graph::Contains(Entity *e1, Entity *e2, const string& b1, const string& b2) {
	hash_map< int,list<CG_Node *> >::iterator i;
	if ( ( i = node_hmap.find(e1->GetId()) ) == node_hmap.end() )
		return false;
	list<CG_Node *>::iterator j;
	list<CG_Edge *>::iterator k;
	for( j = (*i).second.begin() ; j != (*i).second.end() ; j++ )
		for( k = (*j)->edges.begin() ; k != (*j)->edges.end() ; k++ )
			if ( ( (*k)->GetRef()->GetRef() == e2) && ( (*k)->GetSxBinder() == b1 ) && ( (*k)->GetRxBinder() == b2 ) )
				return true;
	return false;
}

//
void Complex_Graph::PrintEdges(CG_Node *node, int *counter, ofstream *fstr) {
	std::queue<CG_Node *> nodes_queue;
	std::list<CG_Edge *>::iterator i;

	node->DFS_mark = 1;
	node->node_name = "Box" + Utility::i2s((*counter)++);
	nodes_queue.push(node);

	while(!nodes_queue.empty())
	{
		node = nodes_queue.front();
		for( i = node->edges.begin() ; i != node->edges.end() ; i++ ) {
			if ( (*i)->IsVisible() )
			{
				if ( (*i)->GetRef()->DFS_mark == 0 )
				{
					(*i)->GetRef()->DFS_mark = 1;
					(*i)->GetRef()->node_name = "Box" + Utility::i2s((*counter)++);
					nodes_queue.push((*i)->GetRef());
				}
				else if ( (*i)->GetRef()->DFS_mark == 2 )
				{
					(*i)->Print(node->node_name,fstr);
				}
			}
		}
		node->DFS_mark = 2;
		nodes_queue.pop();
	}
}

//
void Complex_Graph::PrintEdges(CG_Node *node, int *counter, FILE *fstr) {
	std::queue<CG_Node *> nodes_queue;
	std::list<CG_Edge *>::iterator i;

	node->DFS_mark = 1;
	node->node_name = "Box" + Utility::i2s((*counter)++);
	nodes_queue.push(node);

	while(!nodes_queue.empty())
	{
		node = nodes_queue.front();
		for( i = node->edges.begin() ; i != node->edges.end() ; i++ ) {
			if ( (*i)->IsVisible() )
			{
				if ( (*i)->GetRef()->DFS_mark == 0 )
				{
					(*i)->GetRef()->DFS_mark = 1;
					(*i)->GetRef()->node_name = "Box" + Utility::i2s((*counter)++);
					nodes_queue.push((*i)->GetRef());
				}
				else if ( (*i)->GetRef()->DFS_mark == 2 )
				{
					(*i)->Print(node->node_name,fstr);
				}
			}
		}
		node->DFS_mark = 2;
		nodes_queue.pop();
	}
}


//
void Complex_Graph::PrintDOT(CG_Node *node, ofstream *fstr)
{
	std::list<CG_Edge *>::iterator i;
	(*fstr) << node->node_name << " [ label = \" " << node->GetRef()->GetId() << " \" ]" << endl;
	node->DFS_mark = 1;
	for( i = node->edges.begin() ; i != node->edges.end() ; i++ ) {
		if ( !(*i)->IsVisible() ) continue;
		if ( (*i)->GetRef()->DFS_mark == 1 )
			(*i)->PrintDOT(node->node_name,node->GetRef(),fstr);
		else if ( (*i)->GetRef()->DFS_mark == 0 )
				PrintDOT((*i)->GetRef(),fstr);
	}
	node->DFS_mark = 2;
}

//
void Complex_Graph::PrintDOT(ofstream *fstr)
{
	this->Clean();
	PrintDOT(node_hmap.begin()->second.front(),fstr);
}

//
size_t Complex_Graph::GetNumberNodes(Entity *ent)
{
	COMPLEX_GRAPH_HMAP::iterator i;
	if ( ( i = node_hmap.find(ent->GetId())) == node_hmap.end() )
	{
		return 0;
	}
	else
	{
		return (*i).second.size();
	}
}


//
void Complex_Graph::Print(ofstream *fstr) {
	// stampo prima tutti i nodi del grafo
	// (*fstr) << nodes << " " << edges << endl;
	int node_counter=0;
	(*fstr) << "{" << endl << "( ";
	PrintEdges(node_hmap.begin()->second.front(),&node_counter,fstr);
	(*fstr) << " );" << endl;
	// stampo tutti i nodi e le loro definizioni
	hash_map< int,list<CG_Node *> >::iterator i;
	for( i = node_hmap.begin() ; i != node_hmap.end() ; i++ ) {
		(*fstr) << (*i).second.front()->node_name << ":"
			<< env->GetST()->GetEntNameFromID((*i).second.front()->GetRef()->GetId()) << "="
			<< "(" << (*i).second.front()->GetRef()->GetBoundNames() << ");" << endl;
	}
	std::list<CG_Node *>::iterator j;
	string tmp;
	for( i = node_hmap.begin() ; i != node_hmap.end() ; i++ )
	{
		tmp = "";
		for( j = (*i).second.begin() ; j != (*i).second.end() ; j++ )
		{
			if ( !tmp.empty() )
				(*fstr) << (*j)->node_name << "=" << tmp << ";" << endl;
			tmp = (*j)->node_name;
		}
	}
	(*fstr) << "}";
}

void Complex_Graph::Print(FILE *fstr) {
	// stampo prima tutti i nodi del grafo
	// (*fstr) << nodes << " " << edges << endl;
	int node_counter=0;
	fprintf(fstr,"{\n(");
	PrintEdges(node_hmap.begin()->second.front(),&node_counter,fstr);
	fprintf(fstr,");");
	// stampo tutti i nodi e le loro definizioni
	hash_map< int,list<CG_Node *> >::iterator i;
	for( i = node_hmap.begin() ; i != node_hmap.end() ; i++ ) {
		fprintf(fstr,"%s:%s=(%s);\n",
			(*i).second.front()->node_name.c_str(),
			(env->GetST()->GetEntNameFromID((*i).second.front()->GetRef()->GetId())).c_str(),
			(*i).second.front()->GetRef()->GetBoundNames().c_str());
	}
	std::list<CG_Node *>::iterator j;
	string tmp;
	for( i = node_hmap.begin() ; i != node_hmap.end() ; i++ )
	{
		tmp = "";
		for( j = (*i).second.begin() ; j != (*i).second.end() ; j++ )
		{
			if ( !tmp.empty() )
				fprintf(fstr,"%s=%s;\n", (*j)->node_name.c_str(),tmp.c_str());
			tmp = (*j)->node_name;
		}
	}
	fprintf(fstr,"}");
}


//
void Complex_Graph::CleanPrint() {
	hash_map< int,list<CG_Node *> >::iterator i;
	std::list<CG_Node *>::iterator j;
	for( i = node_hmap.begin() ; i != node_hmap.end() ; i++ )
		for ( j = (*i).second.begin() ; j != (*i).second.end() ; j++ ) {
			(*j)->node_name = "";
			(*j)->DFS_mark = 0;
		}
}

//
bool Complex_Graph::ContainsInfiniteActions() {
	hash_map< int,list<CG_Node *> >::iterator i;
	for( i = node_hmap.begin() ; i != node_hmap.end() ; i++ )
		if ( !(*i).second.front()->GetRef()->InfListEmpty() ) return true;
	return false;
}

//
bool Complex_Graph::IsConnected() {
	int n_node = 0;
	std::vector<string> v_key;
	Edges(node_hmap.begin()->second.front(),&v_key,&n_node);
	Clean();
	if ( n_node == nodes ) return true;
	return false;
}

//
bool Complex_Graph::Isomorphism(Complex_Graph *graph2) {
	hash_map< int,list<CG_Node *> >::iterator i;
	std::list<CG_Node *>::iterator j;
	bool res;
	CG_Node *node = node_hmap.begin()->second.front();
	if ( ( i = graph2->node_hmap.find(node->GetRef()->GetId()) ) != graph2->node_hmap.end() ) {
		for( j = (*i).second.begin() ; j != (*i).second.end() ; j++ ) {
			res = node->DFSMatching(*j);
			if (res) return true;
			this->Clean();
			graph2->Clean();
		}
	}
	return false;
}

//
UIBind_elem* Complex_Graph::FindInBindMap(const string& key)
{
   return env->GetUIBind_map()->Find(key);
}

//
void Complex_Graph::InsertInBindMap(const string& key, UIBind_elem* ui_elem)
{
   env->GetUIBind_map()->Insert(key, ui_elem);
}

//
AffRuleBase *Complex_Graph::GetAffinity(const string& a_type, const string& b_type)
{
   return env->GetTypeAffinity()->GetAffinity(a_type, b_type);
}

//
void Complex_Graph::InsertInActMap(Entity* e1, Entity* e2, Map_Elem* map_elem)
{
   env->GetActMap()->Insert(e1, e2, map_elem);
}

void Complex_Graph::InsertInInfActMap(Entity* e1, Entity* e2, Element* elem)
{
   env->GetInfActMap()->Insert(e1, e2, elem);
}

void Complex_Graph::DecBindElemInBindMap(const string& key, vector<Map_Elem *> *e_list)
{
   env->GetUIBind_map()->DecBindElem(key, e_list);
}

//
CG_Iterator *Complex_Graph::GetIterator(int e_id)
{
	std::list<CG_Node *>::iterator iterator_begin;
	size_t iterator_size;
	COMPLEX_GRAPH_HMAP::iterator i;
	if ( ( i = this->node_hmap.find(e_id) ) == this->node_hmap.end() )
		return NULL;
	iterator_begin = (*i).second.begin();
	iterator_size = (*i).second.size();
	return ( new CG_Iterator(iterator_begin,iterator_size,e_id,this) );
}

//
CG_Bind_Iterator *Complex_Graph::GetBindIterator(Entity *e1,Entity *e2,const string &sub1, const string &sub2)
{
	std::list<CG_Node *>::iterator iterator_begin;
	size_t iterator_size;
	COMPLEX_GRAPH_HMAP::iterator i;
	if ( ( i = this->node_hmap.find(e1->GetId()) ) == this->node_hmap.end() )
		return NULL;
	iterator_begin = (*i).second.begin();
	iterator_size = (*i).second.size();
	CG_Bind_Iterator *iter = new CG_Bind_Iterator(iterator_begin,iterator_size,e1,e2,sub1,sub2,this);
	return iter;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
// CG_Iterator methods implementation

//
CG_Iterator::CG_Iterator(CG_NODE_LIST p_iterator_begin, size_t p_iterator_size, int p_e_id,
		Complex_Graph *p_my_graph) : Iterator<CG_NODE_LIST>(p_iterator_begin,p_iterator_size,IT_COMPLEX_GRAPH,0)
{
	e_id = p_e_id;
	my_graph = p_my_graph;
}

//
CG_Iterator::~CG_Iterator()
{}

//
CG_Node *CG_Iterator::GetNode()
{
	return (*this->iterator_current);
}

//
bool CG_Iterator::IsEqual(Iterator_Interface *element)
{
	if ( element->GetCategory() != IT_COMPLEX_GRAPH )
		return false;
	if ( (*iterator_current) == (*(((Iterator<CG_NODE_LIST> *)element)->GetCurrent())) )
		return true;
	return false;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
// CG_Bind_Iterator methods implementation

//
CG_Bind_Iterator::CG_Bind_Iterator(CG_NODE_LIST p_iterator_begin, size_t p_iterator_size,Entity *p_e1,Entity *p_e2,
		const string& p_sub1, const string p_sub2,Complex_Graph *p_my_graph)
		:Iterator<CG_NODE_LIST>(p_iterator_begin,p_iterator_size,IT_COMPLEX_GRAPH_BIND,0)
{
	e1 = p_e1;
	e2 = p_e2;
	sub1 = p_sub1;
	sub2 = p_sub2;
	my_graph = p_my_graph;
}

//
void CG_Bind_Iterator::IteratorNext()
{
	if ( size != current_size )
	{
		(*iterator_current)->TS_mark = 1;
		iterator_current++;
		current_size++;
		while ( size != current_size && !(*iterator_current)->ContainsEdge(e2,sub1,sub2) )
		{
			iterator_current++;
			current_size++;
		}
	}
}

//
CG_Bind_Iterator::~CG_Bind_Iterator()
{}

//
void CG_Bind_Iterator::IteratorReset()
{
	CG_Edge *edge;
	iterator_current = iterator_begin;
	current_size = 0;
	while ( current_size != size )
	{
		(*iterator_current++)->TS_mark = 0;
		current_size++;
	}
    iterator_current = iterator_begin;
	current_size = 0;
	while ( current_size != size && ( ( ( edge = (*iterator_current)->ContainsEdge(e2,sub1,sub2) ) == NULL ) ||
			( ( ( edge = (*iterator_current)->ContainsEdge(e2,sub1,sub2) ) == NULL ) && edge->GetRef()->TS_mark == 1 ) ) )
	{
		iterator_current++;
		current_size++;
	}
}

//
pair<CG_Node *,CG_Node *> CG_Bind_Iterator::GetBindPair()
{
	CG_Edge *edge;
	pair<CG_Node *,CG_Node *> bind_pair;
	bind_pair.first = bind_pair.second = NULL;
	if ( size != current_size )
	{
		bind_pair.first = (*iterator_current);
		// Questo e' sicuramente diverso da NULL, altrimenti non sarebbe mai
		// stato impostato come contatore corrente
		edge = (*iterator_current)->ContainsEdge(e2,sub1,sub2);
		bind_pair.second = edge->GetRef();
	}
	return bind_pair;
}


