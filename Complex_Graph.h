#ifndef COMPLEX_GRAPH_H_INCLUDED
#define COMPLEX_GRAPH_H_INCLUDED

#include "hash.h"
#include <vector>
#include <list>
#include <iostream>
#include <algorithm>
#include <queue>
#include "Define.h"
#include "Entity.h"
//#include "Symbol_Table.h"
#include "Mapping.h"
#include "Type_Affinity.h"


class CG_Node;
class Complex_Graph;
class Environment;

typedef std::list<CG_Node *>::iterator CG_NODE_LIST;

////////////////////////////////////////////////////////////////////////////////////////
// Implementation of a graph that represents complexes of bio-processes. A node of 
// the graph contains the ID of the entity representing the bio-process. An edge
// between two nodes represent a BIND of two binders. For example, if the bio-process
// B = (x:A)[P] with id N and the bio-process B'=(y:b)[P'] with id M binds toghether 
// through the binders x and y, the graph representing this complex is the graph
//
//     _______            _______
//    |       |    x,y   |       |
//    |       | -------> |       |
//    |   N   |          |   M   |
//    |       | <------- |       |
//    |_______|   y,x    |_______|
//
//
// The nodes and the edges are labelled.

// this class implements one edge of 
// the complex graph
class CG_Edge {
private:
	// In the edge from the node N to the node M, the sx binder is x
	// while the rx binder is y. Ref is the reference to the node M
	string sx_binder;	
	string rx_binder;
	CG_Node *ref;
	int state;
		// 0 = the edge is visible
		// 1 = the edge is not visible

   Complex_Graph* my_graph;

public:
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// Constructor
	CG_Edge(Complex_Graph* graph);

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// Constructor with parameters
	CG_Edge(Complex_Graph* graph, const string& p_sx_binder, const string& p_rx_binder, CG_Node *ref);
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Deconstructor
	~CG_Edge();
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// Returns true if the edge is equal to the passed edge
	bool Equal(CG_Edge *e);
	bool Equal(Entity *ent_ref, const string& bsx, const string& bdx);
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// Returns the sx binder
	const string& GetSxBinder() { return sx_binder; }
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// return the rx binder
	const string& GetRxBinder() { return rx_binder; }
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Set the edge visible
	void SetVisible() { state = 0; }
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Set the edge not visible
	void SetInvisible() { state = 1; }
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// Return true if the edge is visible
	bool IsVisible() { return ( state == 0 );}
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Returns the Node reference ref
	CG_Node *GetRef() { return ref; }
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Returns the string representing the edge
	string GetString(Entity *ent);
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Returns the string representing the edge without reordering it
	string GetStringEdge(Entity *ent);

	void SetSxBinder(string label) { sx_binder = label; }
	void SetRxBinder(string label) { rx_binder = label;}
	void SetMyGraph(Complex_Graph *c)
	{
		my_graph = c;
	}

	
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Prints the edge
	void Print(const string& name, ofstream *fstr); 
	void Print(const string& name, FILE *fstr); 
	void PrintDOT(const string& name, Entity *ent, ofstream *fstr); 
};

// this class implements one node of
// the complex graph
class CG_Node {
private:
	Entity *e_ref;										// riferimento all'entita'
public:
	CG_Node *mark;										// indica se il nodo e' marcato o meno
	int DFS_mark;										// marcatore per la DFS
	bool TS_mark;
	string node_name;									// nome del nodo serve nella stampa
	std::list<CG_Edge *> edges;	
	Complex_Graph* my_graph;

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// Constructor
	CG_Node(Complex_Graph* graph, Entity *p_e_ref);
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Deconstructor
	~CG_Node();
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Returns the entity reference e_ref
	Entity *GetRef() { return e_ref; }
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Set the entity reference e_ref
	void SetRef(Entity *p_ref) { e_ref = p_ref; }
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Adds a new edge to the edge list of the node
	void AddEdge(CG_Edge *edge);
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// Controls if the nodes contains an edge with sx binder equal to b1, with rx binder equal to b2
	// and with a reference to a node with entity e2 
	CG_Edge *ContainsEdge(Entity *e2, const string& b1, const string& b2);
	CG_Edge *ContainsEdge(CG_Edge *edge);
	size_t GetEdgesNumber()
	{
		return edges.size();
	}
	
	CG_Edge *DeleteEdge(Entity *e2, const string& b1, const string& b2);
	int VisitFromNode(); 
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
	void DecBindAct( CG_Node *node, vector<Map_Elem *> *e_list );
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
	void IncBindAct( CG_Node *node, vector<Map_Elem *> *e_list );
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
	bool DFSMatching(CG_Node *n2);
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
	CG_Edge *FindEdge(const string& s_edge);

	//////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	void ChangeEdgesLabels(Mapping *ch_mapping);
};



///////////////////////////////////////////////////////////////////////////////////////////////////////////
// CG_Iterator Definition
///////////////////////////////////////////////////////////////////////////////////////////////////////////


class Complex_Graph;

//
class CG_Iterator : public Iterator<CG_NODE_LIST>
{
protected:
	int e_id;
	Complex_Graph *my_graph;
public:
	CG_Iterator(CG_NODE_LIST p_iterator_begin,size_t p_iterator_size,int p_e_id,Complex_Graph *p_my_graph);
	~CG_Iterator();
	CG_Node *GetNode();
	bool IsEqual(Iterator_Interface *element);
};


///////////////////////////////////////////////////////////////////////////////////////////////////////////
// CG_Bind_Iterator Definition
///////////////////////////////////////////////////////////////////////////////////////////////////////////

//
class CG_Bind_Iterator : public Iterator<CG_NODE_LIST>
{
protected:
	Entity *e1;
	Entity *e2;
	string sub1;
	string sub2;
	Complex_Graph *my_graph;
public:
	CG_Bind_Iterator(CG_NODE_LIST p_iterator_begin, size_t p_iterator_size, Entity *p_e1, Entity *p_e2,
		const string& p_sub1, const string p_sub2,Complex_Graph *p_my_graph);
	~CG_Bind_Iterator();
	void IteratorNext();
	void IteratorReset();
	pair<CG_Node *,CG_Node *> GetBindPair();
};


///////////////////////////////////////////////////////////////////////////////////////////////////////////
// Complex_Graph Definition
///////////////////////////////////////////////////////////////////////////////////////////////////////////

//
class Complex_Graph {
private:
	int nodes;												// number of nodes of the graph
	int edges;												// number of edges of the graph
	typedef hash_map< int,std::list<CG_Node *> > COMPLEX_GRAPH_HMAP;
	COMPLEX_GRAPH_HMAP node_hmap;							// hash_map of nodes
	COMPLEX_GRAPH_HMAP::iterator iter;						// iterator for all nodes

	Environment* env;

	public:

	/////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Selects randomly a node with entity old_e, changes his entity and
	// returns the pointer to the node
	CG_Node *Subs(Entity *old_e, Entity *new_e, Iterator_Interface *c_iter);

	/////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Selects randomly two nodes with entities old_e1 and old_e1, changes the entities
	// of the nodes with respectively the entities new_e1 and new_e2 and return a pair
	// containing the pointers to the changed nodes 
	pair<CG_Node *,CG_Node *> Subs2(Entity *old_e1, Entity *new_e1,Entity *old_e2, Entity *new_e2, 
		Iterator_Interface *c_iter);

	/////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Selects two nodes with entities e1 and e2 and connected by ad edge trough the binders b1 and b2,
	// substitues the entities with the entities be1 and be2 and returns the pointer of the two nodes
	pair<CG_Node *,CG_Node *> SubsBind(Entity *e1, Entity *be1,Entity *e2, Entity *be2,
		const string& b1, const string& b2, Iterator_Interface *c_iter);

	////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Returns the Hash_Map containing a copy of the connected graph that contains the node, the new 
	// reference of the node ref(if != NULL), the number of nodes, the number of edges, and
	// the key of the new graph. In the copy the entity referenced by the node ref is substituted
	// with the entity new_e
	CG_Node *Copy(CG_Node *node, hash_map< int,list<CG_Node *> > *n_map, CG_Node **ref, Entity *new_e,
		int *num_nodes, int *num_edges, std::vector<string> *key_v, CG_Node **ref_node, Complex_Graph *n_complex);

	////////////////////////////////////////////////////////////////////////////////////////////////////////
	// The same as the previous but with the control of two reference nodes (ref1 and ref2).
	CG_Node *Copy(CG_Node *node, hash_map< int,list<CG_Node *> > *n_map, CG_Node **ref1, CG_Node **ref2,
		Entity *new_e1, Entity *new_e2, int *num_nodes, int *num_edges, std::vector<string> *key_v, Complex_Graph *n_complex);

	////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Returns the edges of the graph and the number of nodes
	void Edges(CG_Node *node, std::vector<string> *key_v, int *n_node);

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// Create a copy of the graph where the entity referenced by the node ref is substituted with
	// the entity new_e
	Complex_Graph *CopySub(Entity *new_e, CG_Node **ref, std::vector<string> *key_v, CG_Node **ref_node);

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// Create a copy of the graph where the entity referenced by the node ref1 is substituted with
	// the entity new_e1 and where the entity referenced by the node ref2 is substituted with
	// the entity new_e2
	Complex_Graph *CopySub(Entity *new_e1, CG_Node **ref1, Entity *new_e2, CG_Node **ref2,
		std::vector<string> *key_v);

	// Print DOT 
	void PrintDOT(CG_Node *node,ofstream *fstr);


	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// Constructor
	Complex_Graph(Environment* environment);

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// Constructor with parameters
	Complex_Graph(Environment* environment, Entity *rx, Entity *sx, const string& rx_n, const string& sx_n, 
		vector<Map_Elem *> *e_list);

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// Deconstructor
	~Complex_Graph();

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// Cleans the variable DFS_mark and mark of all the nodes of the graph
	void Clean();

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// Controls if the graph is connected or not
	bool IsConnected();

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// Controls if the graph is Isomorph to the passed graph
	bool Isomorphism(Complex_Graph *graph2);

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// Returns the number of nodes with entity e
	size_t GetNumber(Entity *e);

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// Returns the number of edge of the type e1,b1,e2,b2
	int GetBinding(Entity *e1, Entity *e2, const string& b1, const string& b2);

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// Iterator of the node hash map
	void InitIT() { iter = node_hmap.begin(); }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////
	// Return the Entity of the current node and increase the iterator of the nodes
	Entity *GetIT() {
		if ( iter == node_hmap.end() ) return NULL;
		return (*iter++).second.front()->GetRef();
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// Return the Key and increase the iterator of the nodes
	int GetITK() {
		if ( iter == node_hmap.end() ) return 0;
		return (*iter++).first;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// Return the current node and increase the iterator of the nodes
	CG_Node *GetITN() {
		if ( iter == node_hmap.end() ) return NULL;
		return (*iter++).second.front();
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// Returns the number of nodes with entity equal to the parameter
	size_t GetNumberNodes(Entity *ent);

   Environment* GetEnv() { return env; }

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// Returns true if a node with entity ent exists
	bool Contains(Entity *ent);

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// Returns true if an edge of the type e1,b1,e2,b2 exists
	bool Contains(Entity *e1, Entity *e2, const string& b1, const string& b2);

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// Returns the number of nodes
	int GetNodes() { return nodes; }

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// Returns the number of edges
	int GetEdges() { return edges; }

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// Returns the key of the graph (the Key is not unique)
	void Key(std::vector<string> *key);

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// Copy methods 
	Complex_Graph *CopyOne(Entity *old_e, Entity *new_e, CG_Node **old_node, CG_Node **new_node, 
		std::vector<string> *key_v, Iterator_Interface *c_iter);
	Complex_Graph *CopyTwo(Entity *old_e1, Entity *new_e1, CG_Node **old_node1, CG_Node **new_node1, 
		Entity *old_e2, Entity *new_e2, CG_Node **old_node2, CG_Node **new_node2, std::vector<string> *key_v,
		Iterator_Interface *c_iter);
	Complex_Graph *CopyInterBind(Entity *old_e1, Entity *new_e1, const string& b1, CG_Node **old_node1, 
		CG_Node **new_node1, Entity *old_e2, Entity *new_e2, const string& b2, CG_Node **old_node2, 
		CG_Node **new_node2, std::vector<string> *key_v, Iterator_Interface *c_iter);
	pair<Complex_Graph *,Complex_Graph *> CopyUnbind(Entity *old_e1, Entity *new_e1, const string& b1, 
		CG_Node **old_node1, CG_Node **new_node1, Entity *old_e2, Entity *new_e2, const string& b2, 
		CG_Node **old_node2, CG_Node **new_node2, std::vector<string> *key_v1, std::vector<string> *key_v2,
		Iterator_Interface *c_iter);

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// Merges two graphs. The resulting graph is pointed by the graph that invokes the method. At the
	// end the other graph is empty
	void Merge(Complex_Graph *c_complex); 

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// Adds an edge between two existing nodes
	void AddEdge(const string& bind1, const string& bind2, CG_Node *node1, CG_Node *node2);

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// Creates a new node with entity new_e and add an edge between the new node and the node node1
	void AddEdge(Entity *new_e, const string& bind1, const string& bind2, CG_Node *node1);

	void AddEdgeNode(const string& bind1, const string& bind2, CG_Node *node1, CG_Node *node2); 

	void AddEdgeMaybeNode(const string& bind1, const string& bind2,CG_Node *node1, CG_Node *node2); 

	///////////////////////////////////////////////////////////////////////////////////////////////////////
   // BindMap, Type_Affinity, ecc. proxy functions
   UIBind_elem* FindInBindMap(const string& key);
   void DecBindElemInBindMap(const string& key, vector<Map_Elem *> *e_list);
   void InsertInBindMap(const string& key, UIBind_elem* ui_elem);

   AffRuleBase *GetAffinity(const string& a_type, const string& b_type);

   void InsertInActMap(Entity* e1, Entity* e2, Map_Elem* map_elem);  
   void InsertInInfActMap(Entity* e1, Entity* e2, Element* elem);  

	/////////////////////////////////////////////////////////////////////////////////////////////////////
	// NOT USED FUNCTIONS
	CG_Node* Key(Entity *old_e, Entity *new_e, std::vector<string> *v_key);
	pair<CG_Node *,CG_Node *> Key(Entity *old_e1, Entity *new_e1, Entity *old_e2, 
		Entity *new_e2, std::vector<string> *v_key); 
	pair<CG_Node *,CG_Node *> Key(Entity *e1, Entity *be1, Entity *e2, Entity *be2, const string& b1,
		const string& b2, std::vector<string> *v_key); 
	pair<CG_Edge *,CG_Edge *> Key(Entity *e1, Entity *be1, Entity *e2, Entity *be2, const string& b1,
		const string& b2, std::vector<string> *v_key1, std::vector<string> *v_key2, bool *split); 
	/////////////////////////////////////////////////////////////////////////////////////////////////////

	
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// Returns true if contains a node with an entity that contains infinite actions
	bool ContainsInfiniteActions();

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// Cleans the variable used by the Print function
	void CleanPrint(); 

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// Prints the edges of the graph
	void PrintEdges(CG_Node *node, int *counter, ofstream *fstr);
	void PrintEdges(CG_Node *node, int *counter, FILE *fstr);

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// Prints all the graph informations
	void Print(ofstream *fstr);
	void Print(FILE *fstr);
	void PrintDOT(ofstream *fstr);

	//////////////////////////////////////////////////////////////////////////////////////////////////////
	// Returns a Complex_Graph_iterator for the entity e_id
	CG_Iterator *GetIterator(int e_id);
	CG_Bind_Iterator *GetBindIterator(Entity *e1,Entity *e2,const string &sub1, const string &sub2);


	void DeleteSubgraph(CG_Node *node);

	void DeleteNode(CG_Node *node, int old_entity)
	{
		list<CG_Node *>::iterator k;
		COMPLEX_GRAPH_HMAP::iterator i;

		// delete the old hmap entry
		if ( ( i = node_hmap.find(old_entity) ) != node_hmap.end() )
		{
			if ( (*i).second.size() == 1 && (*i).second.front() == node )
				node_hmap.erase(old_entity);
			else
			{
				for ( k = (*i).second.begin() ; k != (*i).second.end() ; k++ )
				{
					if ( (*k) == node )
					{
						(*i).second.erase(k);
						break;
					}
				}
			}
		}
	}

	void ChangeNode(CG_Node *node, int old_entity, int new_entity)
	{
		list<CG_Node *>::iterator k;
		COMPLEX_GRAPH_HMAP::iterator i;

		// delete the old hmap entry
		if ( ( i = node_hmap.find(old_entity) ) != node_hmap.end() )
		{
			if ( (*i).second.size() == 1 && (*i).second.front() == node )
				node_hmap.erase(old_entity);
			else
			{
				for ( k = (*i).second.begin() ; k != (*i).second.end() ; k++ )
				{
					if ( (*k) == node )
					{
						(*i).second.erase(k);
						break;
					}
				}
			}
		}
		// change the node position in the hmap
		node_hmap[new_entity].push_back(node);
	}

	void IncNodes(int val)
	{
		nodes+=val;
	}
	
	void IncEdges(int val)
	{
		edges+=val;
	}

	void DecNodes(int val)
	{
		assert(nodes >= val);
		nodes-=val;
	}
	
	void DecEdges(int val)
	{
		assert(edges >= val);
		edges-=val;
	}
};


#endif // COMPLEX_GRAPH_H_INCLUDED


