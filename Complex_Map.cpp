#include <iostream>
#include "Complex_Map.h"
#include "Environment.h"


//
Complex_Map::Complex_Map(Environment* p_env) {
   env = p_env;
}

//
Complex_Map::~Complex_Map() {
}

//
Complex *Complex_Map::Insert(Complex *comp, const string& key, int e_id) {
	hash_map< string,list<Complex *> >::iterator i;
	std::list<Complex *>::iterator j;
	bool val;
	if ( ( i = hmap.find(key) ) == hmap.end() ) {
		if (e_id == EMPTY_ID)
			comp->SetId(env->GetST()->NewMolID());
		else
			comp->SetId(e_id);
        env->GetComplexList()->push_back(comp);
		hmap[key].push_back(comp);

		return NULL;
	}
	else {
		for( j = (*i).second.begin() ; j != (*i).second.end() ; j++ ) {
			val = (*j)->GetComplex()->Isomorphism(comp->GetComplex());
			(*j)->GetComplex()->Clean();
			comp->GetComplex()->Clean();
			if ( val ) {
				(*j)->AddCounter(comp->GetNumber());
				return (*j);
			} 
		}
		if (e_id == EMPTY_ID)
         comp->SetId(env->GetST()->NewMolID());
		else
			comp->SetId(e_id);

		env->GetComplexList()->push_back(comp);
		(*i).second.push_back(comp);
		return NULL;
	}
}

//
void Complex_Map::Print(ofstream *fstr) {

}


//
void Complex_Map::PrintVal(FILE *file, double time) {

}

