#ifndef COMPLEX_MAP_H_INCLUDED
#define COMPLEX_MAP_H_INCLUDED

#ifdef _MSC_VER
#include <hash_map>
using namespace stdext;
#else
#include <ext/hash_map>
#endif

#include <vector>
#include <iostream>
#include <list>
#include <cstdlib>
#include "Complex.h"

class Complex;

using namespace std;

class Complex_Map
{
private:
	hash_map< string,list<Complex *> > hmap;

	// back-reference to my ambient
	Environment* env;

	// Itearator
	std::list<Complex *>::iterator iterator_one;
	std::list<Complex *>::iterator iterator_two;
public:
	Complex_Map(Environment* p_env);
	~Complex_Map();
	Complex *Insert(Complex *comp, const string& key, int e_id);
	void Print(ofstream *fstr);
	void PrintVal(FILE *file, double time);
	
	list<Complex *> *GetComplexesList(string id)
	{
		hash_map< string,list<Complex *> >::iterator it;
		if ( ( it = hmap.find(id) ) != hmap.end() )
		{
			return &((*it).second);
		}
		return NULL;
	}

	void DeleteKey(string key)
	{
		hash_map< string,list<Complex *> >::iterator it;
		if ( ( it = hmap.find(key) ) != hmap.end() )
		{
			if ( (*it).second.empty() )
			{
				hmap.erase(key);
			}
		}
	}

	void PrintKeys(vector<string> *vector) 
	{
		hash_map< string,list<Complex *> >::iterator it;
		int counter = 0;
		for(it=hmap.begin();it!=hmap.end();it++)
		{
			vector->push_back((*it).first);
			(*it).second.front()->PrintDOT(counter++);
		}
	}
};


#endif // COMPLEX_MAP_H_INCLUDED


