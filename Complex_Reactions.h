#ifndef COMPLEX_H_REACTIONS
#define COMPLEX_H_REACTIONS

#include <hash_map>
#include <vector>
#include "Complex.h"
#include "TS_Signature.h"

class Complex;
class CG_Node;
class Complex_Reactions;
class TS_Signature;

/////////////////////////////////////////////////////////////////////////////////////////
// Structure implementing the signature of a complexes reaction

struct CR_Signature {
	int type;			// 0=BIND,1=MONO,2=BIM,3=BIMBIND,4=UNBIND
	int complex1;		// First complex id
	int complex2;		// Second complex id
	int species11;		// First old species id
	int species12;		// First new species id
	int species21;		// Second old species id
	int species22;		// Second new species id
	CG_Node *node1;		// First node
	CG_Node *node2;		// Second node
	CR_Signature(int p_type, int p_complex1, int p_complex2, int p_species11, 
				 int p_species12, int p_species21, int p_species22, CG_Node *p_node1,
				 CG_Node *p_node2):
		type(p_type), 
		complex1(p_complex1),
		complex2(p_complex2),
		species11(p_species11),
		species12(p_species12),
		species21(p_species21),
		species22(p_species22),
		node1(p_node1),
		node2(p_node2)
	{}
};

/////////////////////////////////////////////////////////////////////////////////////////
// Class CR_reaction containing the signature

class CR_Reaction {
private:
	CR_Signature *signature;
	Complex *c1;
	Complex *c2;
	Complex_Reactions *map;
public:
	vector<Map_Elem *> e_list;
	CR_Reaction(CR_Signature *p_signature, Complex_Reactions *p_map)
	{ 
		signature = p_signature; 
		map = p_map;
	}
	~CR_Reaction()
	{ delete signature; }
	CR_Signature *GetSignature()
	{ return signature; }
	void Setc1(Complex *element)
	{ c1 = element; }
	void Setc2(Complex *element)
	{ c2 = element; }
	Complex *Getc1()
	{ return c1; }
	Complex *Getc2()
	{ return c2; }
};

/////////////////////////////////////////////////////////////////////////////////////////
// TS_Signature predicates and functions

#ifdef _MSC_VER

using stdext::hash_map;
typedef unsigned __int16 uint16_t;
typedef unsigned __int32 uint32_t;

#elif defined(__GNUC__)

#include <ext/hash_map>

using __gnu_cxx::hash_map;
namespace __gnu_cxx
{
   template<> struct hash< TS_Signature >
   {
      size_t operator()(const TS_Signature& s1) const
      {
         return Hash((const char *)&s1,(int)(7*sizeof(int) + 2*sizeof(CG_Node *)));
      }
   };
}

#endif

///////////////////////////////////////////////////////////////////////////////////////////
// Binary Predicate for the Transition System node hash map

inline bool operator==(const CR_Signature& s1, const CR_Signature& s2)
{
	if ( &s1 == &s2 ) return true;
	if ( s1.type != s2.type ) return false;
	if ( s1.complex1 != s2.complex1 ) return false;
	if ( s1.complex2 != s2.complex2 ) return false;
	if ( s1.species11 != s2.species11 ) return false;
	if ( s1.species12 != s2.species12 ) return false;
	if ( s1.species21 != s2.species21 ) return false;
	if ( s1.species22 != s2.species22 ) return false;
	if ( s1.node1 != s2.node1 ) return false;
	if ( s1.node2 != s2.node2 ) return false;
	return true;
}

struct CR_Signature_hash_compare
{
	static const size_t bucket_size = 4;
	static const size_t min_buckets = 8;

	inline size_t operator()(const CR_Signature& s1) const;

	bool operator()(const CR_Signature& s1, const CR_Signature& s2) const
	{
		return !(s1==s2);
	}
};

///////////////////////////////////////////////////////////////////////////////////////////


class Complex_Reactions {
private:
	typedef hash_map< CR_Signature, CR_Reaction*, CR_Signature_hash_compare > CR_MAP;
	CR_MAP cr_hmap; 									
public:
	Complex_Reactions(){};
	~Complex_Reactions(){};
	void AddReaction(CR_Signature *signature, Complex *p_c1, Complex *p_c2, vector<Map_Elem *> *p_e_list)
	{
		CR_Reaction *n_reaction;
		n_reaction = new CR_Reaction(signature,this);
		n_reaction->Setc1(p_c1);
		n_reaction->Setc2(p_c2);
		n_reaction->e_list = *p_e_list;
		cr_hmap[*signature] = n_reaction;
	}
	CR_Reaction *Find(CR_Signature *signature)
	{
		if (signature == NULL) 
			return NULL;
		CR_MAP::iterator i;
		if ( ( i = cr_hmap.find(*signature) ) == cr_hmap.end() )
			return NULL;
		return (*i).second;
	}
};

// Hash function
inline size_t CR_Signature_hash_compare::operator()(const CR_Signature& s) const
{ 
	return Hash((const char *)&s,(int)(7*sizeof(int) + 2*sizeof(CG_Node *)));
}


#endif // COMPLEX_H_REACTIONS

