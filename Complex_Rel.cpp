#include <iostream>
#include <cassert>
#include "Complex_Rel.h"
#include "Environment.h"

//
bool Complex_Predicate(Entity *left, Entity *right) {
	return (left->GetId() < right->GetId());
}

//
Complex *Complex_Rel::Select(Entity *e, std::list<Complex*> *p_list) {
	std::list<Complex*>::iterator i;
	std::list<Complex*> n_list;
	int number;
	int sum = 0;
	for(i = p_list->begin(); i != p_list->end() ; i++) {
		number = (*i)->GetNumberEntities(e);
		if ( number > 0 ) {
			n_list.push_front(*i);
			sum += number;
		}
	}

	assert( !n_list.empty() );

	int rn = env->Random0UptoN(sum);
	i = n_list.begin();
	while( rn >= ( number = (*i)->GetNumberEntities(e) ) ) {
		rn -= number;
		i++;
	}
	return (*i);
}


void Complex_Rel::FillInReactionKey(reaction_key *key, int p1, int p2, int r1, int r2, ReactionType et){
	if ( min(p1,p2) == p1 ) {
		key->data[0] = p1;
		key->data[1] = p2;
		key->data[2] = r1;
		key->data[3] = r2;
	}
	else {
		key->data[0] = p2;
		key->data[1] = p1;
		key->data[2] = r2;
		key->data[3] = r1;
	}

	key->data[4] = et;
}

//
pair<Complex *,Complex *> Complex_Rel::SelectTWO(Entity *e1, Entity *e2) 
{
	hash_map<int, std::list<Complex *>* >::iterator i;
	pair<Complex *,Complex *> pair_comp;
	pair_comp.first = pair_comp.second = NULL;
	if ( e1 != e2 ) 
   {
		// Se sono diversi li scelgo separatamente con due Select sulle
		// relative liste
		if ( ( i = c_hmap.find(e1->GetId()) ) != c_hmap.end() )
			pair_comp.first = Select(e1,(*i).second);
		if ( ( i = c_hmap.find(e2->GetId()) ) != c_hmap.end() )
			pair_comp.second = Select(e2,(*i).second);
	}
	else 
   {
		// Se sono uguali ottengo la lista di complessi e ne scelgo
		// uno usando la Select
		if ( ( i = c_hmap.find(e1->GetId()) ) != c_hmap.end() )
			pair_comp.first = Select(e1,(*i).second);
		if ( pair_comp.first != NULL ) 
      {	
			std::list<Complex*>::iterator j;
			std::list<Complex*> n_list;
			int number;
			int sum = 0;
			for( j = (*i).second->begin() ; j != (*i).second->end() ; j++ ) {
				number = (*j)->GetNumberEntities(e2);
				if ( pair_comp.first == (*j))
            {
               if ( (number - 1) > 0 ) 
               {
					   n_list.push_front(*j);
					   sum += ( number - 1 );
               }
				}
				else 
            {
               if ( number > 0 ) 
               {
				   	n_list.push_front(*j);
				   	sum += number;
               }
            }
			}

			assert( n_list.size() > 0 );

			int rn = env->Random0UptoN(sum);
			j = n_list.begin();
			if ( (*j) == pair_comp.first ) 
            number = (*j)->GetNumberEntities(e2) - 1;
			else  
            number = (*j)->GetNumberEntities(e2);
			while( rn >= number ) {
				j++;
				rn -= number;
				if ( (*j) == pair_comp.first ) number = (*j)->GetNumberEntities(e2) - 1;
				else  number = (*j)->GetNumberEntities(e2);
			}

			pair_comp.second = *j;
		}
		else {
			if ( ( i = c_hmap.find(e2->GetId()) ) != c_hmap.end() )
				pair_comp.second = Select(e2,(*i).second);
		}

	}

	return pair_comp;
}

//
void Complex_Rel::AddComplex(Complex *comp) {
	hash_map< int,std::list<Complex*> * >::iterator i;
	std::list<Complex*> *n_list;
	comp->GetComplex()->InitIT();
	Entity *current = comp->GetComplex()->GetIT();
	while( current != NULL ) {
		if ( ( i = c_hmap.find(current->GetId()) ) != c_hmap.end() )
			(*i).second->push_back(comp);
		else {
			n_list = new list<Complex *>();
			n_list->push_back(comp);
			c_hmap[current->GetId()] = n_list;	
		}
		current = comp->GetComplex()->GetIT();
	}
}

//
void Complex_Rel::AddComplexToEntityIfNotPresent(Entity *species, Complex *complex)
{
	std::list<Complex*> *n_list;
	hash_map< int,std::list<Complex*> * >::iterator i;
	std::list<Complex*>::iterator j;
	if ( ( i = c_hmap.find(species->GetId()) ) != c_hmap.end() )
	{
		for ( j = (*i).second->begin() ; j != (*i).second->end() ; j++ )
		{
			if ( (*j) == complex )
				return;
		}
		(*i).second->push_back(complex);
	}	
	else
	{
		n_list = new list<Complex *>();
		n_list->push_back(complex);
		c_hmap[species->GetId()] = n_list;	
	}
}

void Complex_Rel::DelComplexFromEntityIfSingle(Entity *species, Complex *complex)
{
	hash_map< int,std::list<Complex*> * >::iterator i;
	std::list<Complex*>::iterator j;
	if ( ( i = c_hmap.find(species->GetId()) ) != c_hmap.end() )
	{
		if ( (*i).second->size() == 1 && (*i).second->front()->GetEntities(species) == 1 && (*i).second->front() == complex )
		{
				delete((*i).second);
				c_hmap.erase(species->GetId());
		}
		else
		{
			for ( j = (*i).second->begin() ; j != (*i).second->end() ; j++ )
			{

				if ( (*j) == complex && (*j)->GetEntities(species) == 1 )
				{
					(*i).second->erase(j);
					return;
				}
			}
		}
	}
}

//
Complex_Rel::Complex_Rel(Environment* p_env) {
	env = p_env;
}

//
Complex_Rel::~Complex_Rel() 
{}

// 
void Complex_Rel::Print() {
    cout << "Entities Values" << endl;
	std::vector<Entity *>::iterator k;
	for(k = env->GetEntityList()->begin(); k != env->GetEntityList()->end(); k++) 
		cout << (*k)->GetId() << " " << (*k)->GetCounter() << endl;
	cout << "Complex Relations Map" << endl;
	hash_map<int, std::list<Complex*>*>::iterator i;
	std::list<Complex*>::iterator j;
	for (i = c_hmap.begin(); i != c_hmap.end(); i++ ) {
		cout << (*i).first << " " << (*i).second->size() << " [ ";
		for(j = (*i).second->begin(); j != (*i).second->end(); j++)
			cout << "( " << (*j)->GetId() << "," << (*j)->GetNumber() << " ) ";
		cout << "] " << endl;
	}
	cout << "----------------------------------------------" << endl;
}


//
void Complex_Rel::IncNodeBind(CG_Node *n1, CG_Node *n2, CG_Node *n3, CG_Node *n4, vector<Map_Elem *> *e_list) 
{
	// queste si riferiscono a nodi di molecole diverse
	if (n1 != NULL) n1->IncBindAct(NULL,e_list);
	if (n2 != NULL) n2->IncBindAct(NULL,e_list);
	// queste si riferiscono a nodi di molecole diverse
	if (n3 != NULL) n3->IncBindAct(NULL,e_list);
	if (n4 != NULL) n4->IncBindAct(n3,e_list);
}

//
void Complex_Rel::DecNodeBind(CG_Node *n1, CG_Node *n2, CG_Node *n3, CG_Node *n4, vector<Map_Elem *> *e_list) 
{
	// queste si riferiscono a nodi di molecole diverse
	if (n1 != NULL) n1->DecBindAct(NULL,e_list);
	if (n2 != NULL) n2->DecBindAct(NULL,e_list);
	// queste si riferiscono a nodi di molecole diverse
	if (n3 != NULL) n3->DecBindAct(NULL,e_list);
	if (n4 != NULL) n4->DecBindAct(n3,e_list);
}

//
void  Complex_Rel::Bind(reaction_key *res, Entity *e1, Entity *e2, Entity *be1, Entity *be2, const string& b1, const string& b2, 
						 vector<Map_Elem *> *e_list, Iterator_Interface *c_iter) 
{
	Complex *c1,*c2,*c3,*c;
	Complex_Graph *cg1,*cg2;
	std::vector<string> edge_array1,edge_array2;
	string key;
	CG_Node *old_node1,*old_node2,*new_node1,*new_node2;
	old_node1 = old_node2 = new_node1 = new_node2 = NULL;
	pair<Complex *,Complex *> comp_pair;
	int cond;
	bool dec_c2 = true;
	
    c3 = NULL;
	cg1 = cg2 = NULL;

	string n_edge = be1->GetKey(be2,b1,b2);

	Iterator_Interface *graph_iter1 = NULL;
	Iterator_Interface *graph_iter2 = NULL;

	if ( c_iter != NULL )
	{
		assert(c_iter->GetCategory() == IT_COMPLEX_REL_TWO);
		c1 = c2 = NULL;
		graph_iter1 = graph_iter2 = NULL;
		if ((CR_One_Iterator *)c_iter->GetIterator(1) != NULL){
			c1 = (*((CR_One_Iterator *)c_iter->GetIterator(1))->GetCurrent());
			graph_iter1 = ((CR_One_Iterator *)c_iter->GetIterator(1))->GetIterator(1);
		}

		if ((CR_One_Iterator *)c_iter->GetIterator(2) != NULL){
			c2 = (*((CR_One_Iterator *)c_iter->GetIterator(2))->GetCurrent());
			graph_iter2 = ((CR_One_Iterator *)c_iter->GetIterator(2))->GetIterator(1);

		}
	}
	else
	{
		comp_pair = SelectTWO(e1,e2);
		c1 = comp_pair.first;
		c2 = comp_pair.second;
	}

	if (c1 != NULL && c2 == NULL)
		cond = 1;
	else if (c2 != NULL && c1 == NULL)
		cond = 2;
	else if (c2 != NULL && c1 != NULL) { 
		if ( c1 != c2 ) 
			cond = 3;
		else {
			if ( c_iter != NULL )
			{
			  if ( ((CR_Two_Iterator *)c_iter)->IsWait() )
					{
						cond = 4;
					}
					else
					{
						cond = 3;
					}
			}
			else
			{
				if ( ( e1 == e2 ) && ( c1->GetEntities(e1) == 1 ) )
				{
					cond = 3;
				}
				else {
					int rn = env->Random0UptoN(c1->GetNumber());
					if ( rn == 0 ) 
						cond = 4;
					else 
						cond = 3;
				}
			}
		}
	}
	else {
		cond = 5;
		//key = n_edge + "-";
		IncBindElemInBindMap(be1,be2,b1,b2,n_edge,e_list);
	}

	// reaction signature
	CR_Signature *signature=NULL;
	pair<CG_Node *,CG_Node *> node_pair;
	node_pair.first = node_pair.second = NULL;

	// selecting the nodes
	switch(cond) {
	case 1:
		new_node1 = old_node1 = c1->GetComplex()->Subs(e1,be1,graph_iter1);
		if (env->FLAG_COMPLEXES_REACTION)
			signature = new CR_Signature(0,c1->GetId(),0,e1->GetId(),be1->GetId(),e2->GetId(),be2->GetId(),old_node1,NULL);
		break;
	case 2:
		new_node1 = old_node1 = c2->GetComplex()->Subs(e2,be2,graph_iter2);
		if (env->FLAG_COMPLEXES_REACTION)
			signature = new CR_Signature(0,0,c2->GetId(),e1->GetId(),be1->GetId(),e2->GetId(),be2->GetId(),NULL,old_node1);
		break;
	case 3:
		new_node1 = old_node1 = c1->GetComplex()->Subs(e1,be1,graph_iter1);
		new_node2 = old_node2 = c2->GetComplex()->Subs(e2,be2,graph_iter2);
		if (env->FLAG_COMPLEXES_REACTION)
			signature = new CR_Signature(0,c1->GetId(),c2->GetId(),e1->GetId(),be1->GetId(),e2->GetId(),be2->GetId(),old_node1,old_node2);
		break;
	case 4:
		dec_c2 = false;
		node_pair = c1->GetComplex()->Subs2(e1,be1,e2,be2,c_iter);
		new_node1 = old_node1 = node_pair.first;
		new_node2 = old_node2 = node_pair.second;
		if (env->FLAG_COMPLEXES_REACTION)
			signature = new CR_Signature(0,c1->GetId(),0,e1->GetId(),be1->GetId(),e2->GetId(),be2->GetId(),old_node1,old_node2);
		break;
	case 5:
		if (env->FLAG_COMPLEXES_REACTION)
			signature = new CR_Signature(0,0,0,e1->GetId(),be1->GetId(),e2->GetId(),be2->GetId(),NULL,NULL);
		break;
	}

	CR_Reaction *reaction = NULL;
	if (env->FLAG_COMPLEXES_REACTION && (reaction = history.Find(signature)) != NULL )
	{
		c3 = reaction->Getc1();
		c3->Inc();
		*e_list = reaction->e_list;
		vector<Map_Elem *>::iterator it;
		for(it = e_list->begin(); it != e_list->end(); it++)
			(*it)->simulation_step = env->GetCurrentStep();
		DecComplex(c1,true);
		DecComplex(c2,dec_c2);
	}
	else
	{

		switch(cond) {
		case 1: 
			cg1 = c1->GetComplex()->CopyOne(e1,be1,&old_node1,&new_node1,&edge_array1,graph_iter1);
			cg1->AddEdge(be2,b1,b2,new_node1);
			//if (!env->FLAG_COMPLEXES_SIGNATURE)
				//Utility::VSKey(edge_array1,n_edge,&key);
			new_node1->IncBindAct(NULL,e_list);
			old_node1->DecBindAct(NULL,e_list);
			break;
		case 2: 
			cg1 = c2->GetComplex()->CopyOne(e2,be2,&old_node1,&new_node1,&edge_array1,graph_iter2);
			cg1->AddEdge(be1,b2,b1,new_node1);
			//if (!env->FLAG_COMPLEXES_SIGNATURE)
				//Utility::VSKey(edge_array1,n_edge,&key);
			new_node1->IncBindAct(NULL,e_list);
			old_node1->DecBindAct(NULL,e_list);
			break;
		case 3: 
			cg1 = c1->GetComplex()->CopyOne(e1,be1,&old_node1,&new_node1,&edge_array1,graph_iter1);
			if ( c1 == c2 ) c1->GetComplex()->Clean();
			cg2 = c2->GetComplex()->CopyOne(e2,be2,&old_node2,&new_node2,&edge_array2,graph_iter2);
			cg1->Merge(cg2);
			cg1->AddEdge(b1,b2,new_node1,new_node2);
			delete(cg2);
			//if (!env->FLAG_COMPLEXES_SIGNATURE)
				//Utility::VVSKey(edge_array1,edge_array2,n_edge,&key);
			new_node1->IncBindAct(NULL,e_list);
			old_node1->DecBindAct(NULL,e_list);
			new_node2->IncBindAct(new_node1,e_list);
			old_node2->DecBindAct(NULL,e_list);
			break;
		case 4:
			cg1 = c1->GetComplex()->CopyTwo(e1,be1,&old_node1,&new_node1,e2,be2,&old_node2,&new_node2,
				&edge_array1,c_iter);
			cg1->AddEdge(b1,b2,new_node1,new_node2);
			//if (!env->FLAG_COMPLEXES_SIGNATURE)
				//Utility::VSKey(edge_array1,n_edge,&key);
			new_node1->IncBindAct(NULL,e_list);
			old_node1->DecBindAct(NULL,e_list);
			new_node2->IncBindAct(new_node1,e_list);
			old_node2->DecBindAct(old_node1,e_list);
			break;
		case 5:
			cg1 = new Complex_Graph(env,be1,be2,b1,b2,e_list);
			break;
		}

		DecComplex(c1,true);
		DecComplex(c2,dec_c2);

		c3 = new Complex(cg1);

		if (!env->FLAG_COMPLEXES_SIGNATURE)
		{
			//key.clear();
			//edge_array1.clear();
			cg1->Key(&edge_array1);
			cg1->Clean();
			Utility::VKey(edge_array1,&key);
		}

		// lo inserisco nella lista di complessi
		if ( ( c = env->InsertComplex(c3,key,EMPTY_ID) ) == NULL ) {
			AddComplex(c3);
		}
		else {
			delete c3;
			c3 = c;
		}


		if (env->FLAG_COMPLEXES_REACTION)
			history.AddReaction(signature,c3,NULL,e_list);

	}

	switch(cond) {
			case 1: 
				FillInReactionKey(res,c1->GetIdN(),e2->GetId(),c3->GetIdN(),0,R_BIND);
				break;
			case 2: 
				FillInReactionKey(res,c2->GetIdN(),e1->GetId(),c3->GetIdN(),0,R_BIND);
				break;
			case 3: 
				FillInReactionKey(res,c1->GetIdN(),c2->GetIdN(),c3->GetIdN(),0,R_BIND);
				break;
			case 4: 
				FillInReactionKey(res,c1->GetIdN(),0,c3->GetIdN(),0,R_BIND);
				break;
			case 5: 
				FillInReactionKey(res,e1->GetId(),e2->GetId(),c3->GetIdN(),0,R_BIND);
				break;
	}


	// qui ritorno la molecola creata ed eventualmente le molecole che la hanno prodotta
	// Posso avere quattro tipi di ritorno
	// 3 M1 + M2 -> M3 due complessi si attaccano
	// 1 S1 + M1 -> M2 un'entit� si attacca ad un complesso
	// 2 M1 + S1 -> M3 un'entit� si attacca ad un complesso
	// 5 S1 + S2 -> M1 due entit� formano un complesso
	// 4 M1 -> M2 il legame riguarda due entit� appartenenti allo stesso complesso
}

//
void Complex_Rel::Mono(reaction_key *res, Entity *e1, Entity *be1, vector<Map_Elem *> *e_list, Iterator_Interface *c_iter) 
{
	Complex *c1,*c2,*c;
	Complex_Graph *cg;
	std::vector<string> edge_array;
	string key;
	CG_Node *old_node,*new_node;
	old_node = new_node = NULL;

	// inizializzo a NULL 
	c1 = NULL;
	
	// estraggo un complesso di cui e1 fa parte e uno di cui e2 fa parte
	hash_map<int, std::list<Complex *>* >::iterator i;
	if ( ( i = c_hmap.find(e1->GetId()) ) != c_hmap.end() ) {

		Iterator_Interface *graph_iter = NULL;
		
		if ( c_iter != NULL )
		{
			assert(c_iter->GetCategory() == IT_COMPLEX_REL_ONE);
			c1 = *(((CR_One_Iterator *)c_iter)->GetCurrent());
			graph_iter = c_iter->GetIterator(1);
		}
		else
		{
			c1 = Select(e1,(*i).second);
		}

		if ( be1 == NULL ) {
			c1->Eliminate(e1,e_list);
			FillInReactionKey(res,c1->GetIdN(),0,0,0,R_MONO);
		}
		else
		{
			// select the node
			new_node = old_node = c1->GetComplex()->Subs(e1,be1,graph_iter);

			// compute the signature
			CR_Signature *signature = NULL;
			CR_Reaction *reaction = NULL;
			if (env->FLAG_COMPLEXES_REACTION)
				signature = new CR_Signature(1,c1->GetId(),0,e1->GetId(),be1->GetId(),0,0,old_node,NULL);

			if (env->FLAG_COMPLEXES_REACTION && (reaction = history.Find(signature)) != NULL)
			{
				c2 = reaction->Getc1();
				c2->Inc();
				*e_list = reaction->e_list;
				vector<Map_Elem *>::iterator it;
				for(it = e_list->begin(); it != e_list->end(); it++)
					(*it)->simulation_step = env->GetCurrentStep();
				DecComplex(c1,true);
			}
			else
			{
				cg = c1->GetComplex()->CopyOne(e1,be1,&old_node,&new_node,&edge_array,graph_iter);
				//if (!env->FLAG_COMPLEXES_SIGNATURE)
				//	Utility::VKey(edge_array,&key);
				old_node->DecBindAct(NULL,e_list);

				// devo estrarre subjects e tipi che sono stati cambiati
				if ( e1->GetCategory() == MONO_CHANGE || e1->GetCategory() == MONO_EXPOSE ) {
					Mapping* mapping;
					mapping = e1->GetActualMapping();
					if (! mapping->IsIdentity() ) {
						new_node->ChangeEdgesLabels(mapping);
						// ricalcolo la chiave 
						// questo si potrebbe ottimizzare facendo tutto nella CopyOne
						if (!env->FLAG_COMPLEXES_SIGNATURE)
						{
							key = EMPTY_STRING;
							edge_array.clear();
							cg->Key(&edge_array);
							Utility::VKey(edge_array,&key);
							cg->Clean();
						}
					}
				}

				new_node->IncBindAct(NULL,e_list);

				DecComplex(c1,true);

				c2 = new Complex(cg); 

				if (!env->FLAG_COMPLEXES_SIGNATURE)
				{
					//key.clear();
					//edge_array.clear();
					cg->Key(&edge_array);
					cg->Clean();
					Utility::VKey(edge_array,&key);
				}

				if ( ( c = env->InsertComplex(c2,key,EMPTY_ID) ) == NULL ) {
					AddComplex(c2);
				}
				else {
					delete c2;
					c2 = c;
				}

				if (env->FLAG_COMPLEXES_REACTION)
					history.AddReaction(signature,c2,NULL,e_list);

			}

			// imposto la stringa che rappresenta l'azione eseguita
			FillInReactionKey(res,c1->GetIdN(),0,c2->GetIdN(),0,R_MONO);
		}
	}
	else {
		if ( be1 == NULL )
			FillInReactionKey(res,e1->GetId(),0,0,0,R_MONO);
		else
			FillInReactionKey(res,e1->GetId(),0,be1->GetId(),0,R_MONO);
	}


	// le azioni che qui possono avvenire sono
	// S1 -> S2
	// M1 -> M2
}



void Complex_Rel::Event(reaction_key *res,Entity* e1, 
                          Entity* se1, 
                          Entity* se2,  
                          int n_se1, int n_se2,
                          VerbType eventType,
								  vector<Map_Elem *>* e_list) 
{	
	ReactionType rt = R_MONO;
   /*if (eventType == VERB_SPLIT)
   {
	   // estraggo un complesso di cui e1 fa parte
	   hash_map<int, std::list<Complex *>* >::iterator it;
	   if ( ( it = c_hmap.find(e1->GetId()) ) != c_hmap.end() ) 
	   {
		   //Complex* c1 = Select(e1, it->second);
		   res = "splitting a bound entity";     

		   // imposto la stringa che rappresenta l'azione eseguita
		   // res = env->GetST()->GetReactionName(c1->GetIdN(),0,c2->GetIdN(),0,EVENT);

	   }
   }*/

   int i1 = (e1 != NULL ? e1->GetId() : NIL_ID);
   int i2 = 0;
   int i3 = (se1 != NULL ? se1->GetId() : NIL_ID);
   int i4 = (se2 != NULL ? se2->GetId() : NIL_ID);

	switch(eventType){
		case VERB_SPLIT:
			rt = R_SPLIT;
			break;
		case VERB_NEW:
			rt = R_NEW;
			break;
		case VERB_DELETE:
			rt = R_DELETE;
			i3 = i4 = 0;
			break;
		case VERB_JOIN:
			rt = R_JOIN;
			break;
		case VERB_UPDATE:
			rt = R_UPDATE;
			break;
		default:
			assert(false);
	}

	FillInReactionKey(res, i1, i2, i3, i4,rt);

	// le azioni che qui possono avvenire sono
	// S1 -> S2 + S3
	// M1 -> M2 + S1
   // M1 -> M2 + M3
}

void Complex_Rel::Event(reaction_key *res, Entity* e1, 
                          Entity* e2,
                          Entity* se1, 
                          int n_se1,
                          VerbType eventType, vector<Map_Elem *>* e_list) 
{
   ReactionType rt = R_UNKNOWN;

   int i1 = (e1 != NULL ? e1->GetId() : NIL_ID);
   int i2 = (e2 != NULL ? e2->GetId() : 0);
   int i3 = (se1 != NULL ? se1->GetId() : NIL_ID);
   int i4 = 0;

	switch(eventType){
		case VERB_SPLIT:
			rt = R_SPLIT;
			break;
		case VERB_NEW:
			rt = R_NEW;
			break;
		case VERB_DELETE:
			rt = R_DELETE;
			i3 = 0;
			break;
		case VERB_JOIN:
			rt = R_JOIN;
			break;
		case VERB_UPDATE:
			rt = R_UPDATE;
			break;
		default:
			assert(false);
	}

	FillInReactionKey(res,i1, i2, i3, i4, rt);	
	// le azioni che qui possono avvenire sono
	// S1 -> S2
	// M1 -> S1
   // S1 S2 -> S3
   // M1 M2 -> M3 ??? TODO: join
   // la molecola origine non viene mai toccata,
   // il target sono sempre Entita' (la semantica non permette di immettere molecole...
   // per ora)
}

void Complex_Rel::Event(reaction_key *res, Entity* e1, 
                          VerbType eventType, vector<Map_Elem *>* e_list) 
{
	ReactionType rt = R_UNKNOWN;

   int id;
   if (e1 == NULL)
      id = 0;
   else
      id = e1->GetId();

	switch(eventType)
   {
		case VERB_SPLIT:
			rt = R_SPLIT;         
			break;
		case VERB_NEW:
			rt = R_NEW;
			break;
		case VERB_DELETE:
			rt = R_DELETE;
			break;
		case VERB_JOIN:
			rt = R_JOIN;
			break;
		case VERB_UPDATE:
			rt = R_UPDATE;
			break;
		default:
			assert(false);
	}


	FillInReactionKey(res, id, 0, 0, 0, rt);
  	// azioni senza target (delete)
}

//
void Complex_Rel::Bim(reaction_key *res, Entity *e1, Entity *e2, Entity *be1, Entity *be2, vector<Map_Elem *> *e_list, 
						Iterator_Interface *c_iter) 
{
	Complex *c1,*c2,*c3,*c4,*c;
	Complex_Graph *cg1,*cg2;
	std::vector<string> edge_array1,edge_array2;
	string key1,key2;
	CG_Node *old_node1,*old_node2,*new_node1,*new_node2;
	old_node1 = old_node2 = new_node1 = new_node2 = NULL;
	pair<Complex *,Complex *> comp_pair;
	int cond;
	bool dec_c2 = true;

	c3 = c4 = NULL;
	cg1 = cg2 = NULL;

	Iterator_Interface *graph_iter1 = NULL;
	Iterator_Interface *graph_iter2 = NULL;

	if ( c_iter != NULL  )
	{
		assert(c_iter->GetCategory() == IT_COMPLEX_REL_TWO);
		c1 = c2 = NULL;
		graph_iter1 = graph_iter2 = NULL;
		if ((CR_One_Iterator *)c_iter->GetIterator(1) != NULL){
			c1 = (*((CR_One_Iterator *)c_iter->GetIterator(1))->GetCurrent());
			graph_iter1 = ((CR_One_Iterator *)c_iter->GetIterator(1))->GetIterator(1);
		}

		if ((CR_One_Iterator *)c_iter->GetIterator(2) != NULL){
			c2 = (*((CR_One_Iterator *)c_iter->GetIterator(2))->GetCurrent());
			graph_iter2 = ((CR_One_Iterator *)c_iter->GetIterator(2))->GetIterator(1);

		}
	}
	else
	{
		comp_pair = SelectTWO(e1,e2);
		c1 = comp_pair.first;
		c2 = comp_pair.second;
	}

	if (c1 != NULL && c2 == NULL)
		cond = 1;
	else if (c2 != NULL && c1 == NULL)
		cond = 2;
	else if (c2 != NULL && c1 != NULL) { 
		if ( c1 != c2 ) 
			cond = 3;
		else {
			if ( c_iter != NULL )
			{
			  if ( ((CR_Two_Iterator *)c_iter)->IsWait() )
					{
						cond = 4;
					}
					else
					{
						cond = 3;
					}
			}
			else
			{
				if ( ( e1 == e2 ) && ( c1->GetEntities(e1) == 1 ) )
				{
					cond = 3;
				}
				else {
					int rn = env->Random0UptoN(c1->GetNumber());
					if ( rn == 0 ) 
						cond = 4;
					else 
						cond = 3;
				}
			}
		}
	}
	else 
		cond = 5;

	CR_Signature *signature = NULL;

	// selecting the nodes
	switch(cond) {
	case 1:
		new_node1 = old_node1 = c1->GetComplex()->Subs(e1,be1,graph_iter1);
		if (env->FLAG_COMPLEXES_REACTION)
			signature = new CR_Signature(2,c1->GetId(),0,e1->GetId(),be1->GetId(),e2->GetId(),be2->GetId(),old_node1,NULL);
		break;
	case 2:
		new_node1 = old_node1 = c2->GetComplex()->Subs(e2,be2,graph_iter2);
		if (env->FLAG_COMPLEXES_REACTION)
			signature = new CR_Signature(2,0,c2->GetId(),e1->GetId(),be1->GetId(),e2->GetId(),be2->GetId(),NULL,old_node1);
		break;
	case 3:
		new_node1 = old_node1 = c1->GetComplex()->Subs(e1,be1,graph_iter1);
		new_node2 = old_node2 = c2->GetComplex()->Subs(e2,be2,graph_iter2);
		if (env->FLAG_COMPLEXES_REACTION)
			signature = new CR_Signature(2,c1->GetId(),c2->GetId(),e1->GetId(),be1->GetId(),e2->GetId(),be2->GetId(),old_node1,old_node2);
		break;
	case 4:
		dec_c2 = false;
		pair<CG_Node *,CG_Node *> node_pair;
		node_pair.first = node_pair.second = NULL;
		node_pair = c1->GetComplex()->Subs2(e1,be1,e2,be2,c_iter);
		new_node1 = old_node1 = node_pair.first;
		new_node2 = old_node2 = node_pair.second;
		if (env->FLAG_COMPLEXES_REACTION)
			signature = new CR_Signature(2,c1->GetId(),0,e1->GetId(),be1->GetId(),e2->GetId(),be2->GetId(),old_node1,old_node2);
		break;
	}

	CR_Reaction *reaction = NULL;
	if (env->FLAG_COMPLEXES_REACTION && (reaction = history.Find(signature)) != NULL )
	{
		c3 = reaction->Getc1();
		c4 = reaction->Getc2();
		if (c3 != NULL) c3->Inc();
		if (c4 != NULL) c4->Inc();
		*e_list = reaction->e_list;
		vector<Map_Elem *>::iterator it;
		for(it = e_list->begin(); it != e_list->end(); it++)
			(*it)->simulation_step = env->GetCurrentStep();
		DecComplex(c1,true);
		DecComplex(c2,dec_c2);
	}
	else
	{
		switch(cond) {
		case 1: 
			cg1 = c1->GetComplex()->CopyOne(e1,be1,&old_node1,&new_node1,&edge_array1,graph_iter1);
			//if (!env->FLAG_COMPLEXES_SIGNATURE)
			//	Utility::VKey(edge_array1,&key1);
			new_node1->IncBindAct(NULL,e_list);
			old_node1->DecBindAct(NULL,e_list);
			break;
		case 2: 
			cg1 = c2->GetComplex()->CopyOne(e2,be2,&old_node1,&new_node1,&edge_array1,graph_iter2);
			//if (!env->FLAG_COMPLEXES_SIGNATURE)
			//	Utility::VKey(edge_array1,&key1);
			new_node1->IncBindAct(NULL,e_list);
			old_node1->DecBindAct(NULL,e_list);
			break;
		case 3: 
			cg1 = c1->GetComplex()->CopyOne(e1,be1,&old_node1,&new_node1,&edge_array1,graph_iter1);
			//if (!env->FLAG_COMPLEXES_SIGNATURE)
			//	Utility::VKey(edge_array1,&key1);
			if ( c1 == c2 ) c1->GetComplex()->Clean();
			cg2 = c2->GetComplex()->CopyOne(e2,be2,&old_node2,&new_node2,&edge_array2,graph_iter2);
			//if (!env->FLAG_COMPLEXES_SIGNATURE)
			//	Utility::VKey(edge_array2,&key2);
			new_node1->IncBindAct(NULL,e_list);
			old_node1->DecBindAct(NULL,e_list);
			new_node2->IncBindAct(NULL,e_list);
			old_node2->DecBindAct(NULL,e_list);
			break;
		case 4:
			cg1 = c1->GetComplex()->CopyTwo(e1,be1,&old_node1,&new_node1,e2,be2,&old_node2,&new_node2,
				&edge_array1,c_iter);
			//if (!env->FLAG_COMPLEXES_SIGNATURE)
			//	Utility::VKey(edge_array1,&key1);
			new_node1->IncBindAct(NULL,e_list);
			old_node1->DecBindAct(NULL,e_list);
			new_node2->IncBindAct(new_node1,e_list);
			old_node2->DecBindAct(old_node1,e_list);
			break;
		}

		DecComplex(c1,true);
		DecComplex(c2,dec_c2);

		if ( cg1 != NULL ) {
			c3 = new Complex(cg1);

			if (!env->FLAG_COMPLEXES_SIGNATURE)
			{
				//key1.clear();
				//edge_array1.clear();
				cg1->Key(&edge_array1);
				cg1->Clean();
				Utility::VKey(edge_array1,&key1);
			}

			// lo inserisco nella lista di complessi
			if ( ( c = env->InsertComplex(c3,key1,EMPTY_ID) ) == NULL ) {
				AddComplex(c3);
			}
			else {
				delete c3;
				c3 = c;
			}
		}

		if ( cg2 != NULL ) {
			c4 = new Complex(cg2);

			if (!env->FLAG_COMPLEXES_SIGNATURE)
			{
				//key2.clear();
				//edge_array2.clear();
				cg2->Key(&edge_array2);
				cg2->Clean();
				Utility::VKey(edge_array2,&key2);
			}

			// lo inserisco nella lista di complessi
			if ( ( c = env->InsertComplex(c4,key2,EMPTY_ID) ) == NULL ) {
				AddComplex(c4);
			}
			else {
				delete c4;
				c4 = c;
			}
		}

		if (env->FLAG_COMPLEXES_REACTION && cond != 5)
			history.AddReaction(signature,c3,c4,e_list);

	}

	switch(cond) {
			case 1: 
				FillInReactionKey(res,c1->GetIdN(),e2->GetId(),c3->GetIdN(),be2->GetId(),R_BIM);
			  	break;
			case 2:
			   FillInReactionKey(res,c2->GetIdN(),e1->GetId(),c3->GetIdN(),be1->GetId(),R_BIM);	
				break;
			case 3: 
				FillInReactionKey(res,c1->GetIdN(),c2->GetIdN(),c3->GetIdN(),c4->GetIdN(),R_BIM);
				break;
			case 4: 
				FillInReactionKey(res,c1->GetIdN(),0,c3->GetIdN(),0,R_BIM);
			  	break;
			case 5: 
				FillInReactionKey(res,e1->GetId(),e2->GetId(),be1->GetId(),be2->GetId(),R_BIM);
				break;
	}


	// le azioni che qui possono avvenire sono
	// S1 + S2 -> S3 + S4
	// S1 + M1 -> S2 + M2
	// M1 + S1 -> M2 + S2
	// M1 + M2 -> M3 + M4
	// M1 -> M2
}

//
void Complex_Rel::BimBind(reaction_key *res, Entity *e1, Entity *e2, Entity *be1, Entity *be2, const string& b1, const string& b2, 
							vector<Map_Elem *> *e_list, Iterator_Interface *c_iter) 
{
	string key;
	std::vector<string> edge_array;
	hash_map<int, std::list<Complex *>* >::iterator i;
	CG_Node *old_node1,*old_node2,*new_node1,*new_node2;
	old_node1 = old_node2 = new_node1 = new_node2 = NULL;
	std::list<Complex*>::iterator j;
	std::list<int>::iterator k;
	std::list<Complex*> c_list;
	std::list<int> c_list_number;
	Complex *c,*c1;
	Complex_Graph *cg;
	int counter = 0;
	int number;

	if ( ( i = c_hmap.find(e1->GetId()) ) != c_hmap.end() ) {

		Iterator_Interface *graph_iter = NULL;

		if ( c_iter != NULL )
		{
			assert(c_iter->GetCategory() == IT_COMPLEX_REL_BIND);
			j = ((CR_Bind_Iterator *)c_iter)->GetCurrent();
			graph_iter = c_iter->GetIterator(1);
		}
		else
		{
			// trovo tutti i complessi che attualmente contengono e1 e e2
			for(j = (*i).second->begin(); j != (*i).second->end() ; j++ ) {
				number = (*j)->GetNumberBinding(e1,e2,b1,b2);
				if ( number > 0 ) {
					c_list.push_back(*j);
					c_list_number.push_back(number);
					counter += number;
				}
			}

			assert(!c_list.empty());

			// seleziono con distribuzione uniforme una delle azioni che contengono e1 e e2
			j = c_list.begin();
			k = c_list_number.begin();
			int rn = env->Random0UptoN(counter);
			while( rn >= (*k) ) {
				rn -= (*k++);
				j++;
			}

		}

		// selecting the nodes
		pair<CG_Node *,CG_Node *> node_pair;
		node_pair.first = node_pair.second = NULL;
		node_pair= (*j)->GetComplex()->SubsBind(e1,be1,e2,be2,b1,b2,graph_iter);
		new_node1 = old_node1 = node_pair.first;
		new_node2 = old_node2 = node_pair.second;

		// create the signature
		CR_Signature *signature = NULL;
		if (env->FLAG_COMPLEXES_REACTION)
			signature = new CR_Signature(3,(*j)->GetId(),0,e1->GetId(),be1->GetId(),e2->GetId(),be2->GetId(),old_node1,old_node2);

		CR_Reaction *reaction = NULL;
		if ( env->FLAG_COMPLEXES_REACTION && ( reaction = history.Find(signature) ) != NULL )
		{
			c1 = reaction->Getc1();
			c1->Inc();
			*e_list = reaction->e_list;
			vector<Map_Elem *>::iterator it;
			for(it = e_list->begin(); it != e_list->end(); it++)
				(*it)->simulation_step = env->GetCurrentStep();
			DecComplex(*j,true);
		}
		else
		{
			cg = (*j)->GetComplex()->CopyInterBind(e1,be1,b1,&old_node1,&new_node1,e2,be2,b2,&old_node2,&new_node2,
				&edge_array,graph_iter);

			//if (!env->FLAG_COMPLEXES_SIGNATURE)
			//	Utility::VKey(edge_array,&key);

			new_node1->IncBindAct(NULL,e_list);
			new_node2->IncBindAct(new_node1,e_list);
			old_node1->DecBindAct(NULL,e_list);
			old_node2->DecBindAct(old_node1,e_list);

			DecComplex(*j,true);

			c1 = new Complex(cg);

			if (!env->FLAG_COMPLEXES_SIGNATURE)
			{
				//key.clear();
				//edge_array.clear();
				cg->Key(&edge_array);
				cg->Clean();
				Utility::VKey(edge_array,&key);
			}

			if ( ( c = env->InsertComplex(c1,key,EMPTY_ID) ) == NULL ) 
			{
				AddComplex(c1);
			}
			else {
				delete c1;
				c1 = c;
			}


			if (env->FLAG_COMPLEXES_REACTION)
				history.AddReaction(signature,c1,NULL,e_list);
		}

		FillInReactionKey(res,(*j)->GetIdN(),0,c1->GetIdN(),0,R_INTER_BIND);
	}
	else {
		assert(false);
	}

	// l'azione eseguita e'
	// M1 -> M2

}

// Parto dal presupposto che se A e B sono legati, tutte le molecole
// a cui fa parte A, fa parte anche B
void Complex_Rel::Unbind(reaction_key *res, Entity *e1, Entity *e2, Entity *be1, Entity *be2, const string& b1, const string& b2, 
						   vector<Map_Elem *> *e_list, Iterator_Interface *c_iter) 
{
	string key1,key2;
	std::vector<string> edge_array1;
	std::vector<string> edge_array2;
	hash_map<int, std::list<Complex *>* >::iterator i;
	CG_Node *old_node1,*old_node2,*new_node1,*new_node2;
	old_node1 = old_node2 = new_node1 = new_node2 = NULL;
	pair< Complex_Graph *,Complex_Graph *> graph_pair;
	std::list<Complex*>::iterator j;
	std::list<int>::iterator k;
	std::list<Complex*> c_list;
	std::list<int> c_list_number;
	Complex *c1,*c2,*c;
	int counter = 0;
	int number;

	c1 = c2 = NULL;

	if ( ( i = c_hmap.find(e1->GetId()) ) != c_hmap.end() ) {

		Iterator_Interface *graph_iter = NULL;

		if ( c_iter != NULL )
		{
			assert(c_iter->GetCategory() == IT_COMPLEX_REL_BIND);
			j = ((CR_Bind_Iterator *)c_iter)->GetCurrent();
			graph_iter = c_iter->GetIterator(1);
		}
		else
		{
			// trovo tutti i complessi che attualmente contengono il binding
			// e1,b1,e2,b2
			for(j = (*i).second->begin(); j != (*i).second->end() ; j++ ) {
				number = (*j)->GetNumberBinding(e1,e2,b1,b2);
				if ( number > 0 ) {
					c_list.push_back(*j);
					c_list_number.push_back(number);
					counter += number;
				}
			}

			assert(!c_list.empty());

			// seleziono con distribuzione uniforme una delle azioni che contengono il binding
			j = c_list.begin();
			k = c_list_number.begin();
			int rn = env->Random0UptoN(counter);
			while( rn >= (*k) ) {
				rn -= (*k++);
				j++;
			}

		}

		// selecting the nodes
		pair<CG_Node *,CG_Node *> node_pair;
		node_pair.first = node_pair.second = NULL;
		node_pair= (*j)->GetComplex()->SubsBind(e1,be1,e2,be2,b1,b2,graph_iter);
		new_node1 = old_node1 = node_pair.first;
		new_node2 = old_node2 = node_pair.second;

		// create the signature
		CR_Signature *signature = NULL;
		if (env->FLAG_COMPLEXES_REACTION)
			signature = new CR_Signature(4,(*j)->GetId(),0,e1->GetId(),be1->GetId(),e2->GetId(),be2->GetId(),old_node1,old_node2);

		CR_Reaction *reaction = NULL;
		if ( env->FLAG_COMPLEXES_REACTION && ( reaction = history.Find(signature) ) != NULL )
		{
			c1 = reaction->Getc1();
			if (c1 != NULL) c1->Inc();
			c2 = reaction->Getc2();
			if (c2 != NULL) c2->Inc();
			*e_list = reaction->e_list;
			vector<Map_Elem *>::iterator it;
			for(it = e_list->begin(); it != e_list->end(); it++)
				(*it)->simulation_step = env->GetCurrentStep();
			DecComplex(*j,true);
		}
		else
		{
			// ottengo i grafi ottenuti dall'unbinding
			graph_pair = (*j)->GetComplex()->CopyUnbind(e1,be1,b1,&old_node1,&new_node1,e2,be2,b2,&old_node2,&new_node2,
				&edge_array1,&edge_array2,graph_iter);

			if (new_node1 != NULL) new_node1->IncBindAct(NULL,e_list);
			if (new_node2 != NULL) new_node2->IncBindAct(new_node1,e_list);
			old_node1->DecBindAct(NULL,e_list);
			old_node2->DecBindAct(old_node1,e_list);

			DecComplex(*j,true);

			if ( graph_pair.first != NULL ) {
				//if (!env->FLAG_COMPLEXES_SIGNATURE)
				//	Utility::VKey(edge_array1,&key1);
				c1 = new Complex(graph_pair.first);

				if (!env->FLAG_COMPLEXES_SIGNATURE)
				{
					//key1.clear();
					//edge_array1.clear();
					graph_pair.first->Key(&edge_array1);
					graph_pair.first->Clean();
					Utility::VKey(edge_array1,&key1);
				}

				if ( ( c = env->InsertComplex(c1,key1,EMPTY_ID) ) == NULL ) {
					AddComplex(c1);
				}
				else {
					delete c1;
					c1 = c;
				}
			}

			if ( graph_pair.second != NULL ) {
				//if (!env->FLAG_COMPLEXES_SIGNATURE)
				//	Utility::VKey(edge_array2,&key2);
				c2 = new Complex(graph_pair.second);

				if (!env->FLAG_COMPLEXES_SIGNATURE)
				{
					//key2.clear();
					//edge_array2.clear();
					graph_pair.second->Key(&edge_array2);
					graph_pair.second->Clean();
					Utility::VKey(edge_array2,&key2);
				}

				if ( ( c = env->InsertComplex(c2,key2,EMPTY_ID) ) == NULL ) {
					AddComplex(c2);
				}
				else {
					delete c2;
					c2 = c;
				}
			}

			if (env->FLAG_COMPLEXES_REACTION)
				history.AddReaction(signature,c1,c2,e_list);

		}

		(*j)->GetComplex()->Clean();

		if ( c1 != NULL && c1->GetComplex()->GetNodes() == (*j)->GetComplex()->GetNodes() ) {
				FillInReactionKey(res,(*j)->GetIdN(),0,c1->GetIdN(),0,R_UNBIND);
		}
		else {
			if ( c1 != NULL && c2 != NULL )
				FillInReactionKey(res,(*j)->GetIdN(),0,c1->GetIdN(),c2->GetIdN(),R_UNBIND);
			else if ( c1 != NULL && c2 == NULL )	
				FillInReactionKey(res,(*j)->GetIdN(),0,be2->GetId(),c1->GetIdN(),R_UNBIND);
			else if ( c1 == NULL && c2 != NULL )
				FillInReactionKey(res,(*j)->GetIdN(),0,c2->GetIdN(),be1->GetId(),R_UNBIND);
			else
				FillInReactionKey(res,(*j)->GetIdN(),0,be1->GetId(),be2->GetId(),R_UNBIND);
		}
	}
	else {
		assert(false);
	}

		
	// qui ritorno la molecola creata ed eventualmente le molecole che la hanno prodotta
	// Posso avere quattro tipi di ritorno
	// 1 M1 -> M2 
	// 2 M1 -> M2 + M3 
	// 3 M1 -> S1 + M2 
	// 4 M1 -> M2 + S1
	// 5 M1 -> S1 + S2 
}


void Complex_Rel::DecComplex(Complex *c, bool dec)
{
	if ( c != NULL ) {
		if ( dec )
			c->Dec();
		c->GetComplex()->Clean();
	}
}

//
void Complex_Rel::IncBindElemInBindMap(Entity *e1, Entity *e2, const string& b1, const string& b2, const string& key, 
                                       vector<Map_Elem *> *e_list)
{
   env->GetUIBind_map()->IncBindElem(e1, e2, b1, b2, key, e_list);
}

// TODO
int Complex_Rel::GetActiveBindings(Entity *e1, Entity *e2, const string &sub1, const string &sub2)
{
	COMPLEX_HMAP_ITERATOR i;
	if ( ( i = this->c_hmap.find(e1->GetId()) ) == this->c_hmap.end() )
		return 0;
	
	int res = 0;
	std::list<Complex *>::iterator comp_i;

	for ( comp_i = (*i).second->begin() ; comp_i != (*i).second->end() ; comp_i++ )
	{
		if ( (*comp_i)->GetNumber() > 0 )
		{
			res += (*comp_i)->GetNumberBinding(e1,e2,sub1,sub2);
		}
	}
	return res;
}

//
CR_One_Iterator *Complex_Rel::GetIteratorOne(int e_id)
{
	COMPLEX_HMAP_ITERATOR i;
	if ( ( i = this->c_hmap.find(e_id) ) == this->c_hmap.end() )
		return NULL;
	return ( new CR_One_Iterator((*i).second->begin(),(*i).second->size(),e_id,env) );
}

//
CR_Two_Iterator *Complex_Rel::GetIteratorTwo(int e_id1,int e_id2)
{
	//bool cond = false;
	CR_Two_Iterator *two_iter;
	CR_One_Iterator *one_iter1,*one_iter2;
	one_iter1 = one_iter2 = NULL;
	COMPLEX_HMAP_ITERATOR i;
	
	if ( ( i = this->c_hmap.find(e_id1) ) != this->c_hmap.end() )
	{
		one_iter1 = new CR_One_Iterator((*i).second->begin(),(*i).second->size(),e_id1,env);
	}

	if ( ( i = this->c_hmap.find(e_id2) ) != this->c_hmap.end() )
	{
		one_iter2 = new CR_One_Iterator((*i).second->begin(),(*i).second->size(),e_id2,env);
	}
	
	if ( one_iter1 != NULL || one_iter2 != NULL)
	{
		two_iter = new CR_Two_Iterator(env, BIM_AND_MONO, e_id1 == e_id2);
		two_iter->SetIterator(one_iter1,1);
		two_iter->SetIterator(one_iter2,2);
		return two_iter;
	}
	
	return NULL;
}

//
CR_Bind_Iterator *Complex_Rel::GetIteratorBind(Entity *p_e1,Entity *p_e2,const string &p_sub1,const string &p_sub2)
{
	COMPLEX_HMAP_ITERATOR i;
	if ( ( i = this->c_hmap.find(p_e1->GetId()) ) != this->c_hmap.end() )
		return ( new CR_Bind_Iterator((*i).second->begin(),(*i).second->size(),p_e1,p_e2,p_sub1,p_sub2,env) );
	return NULL;
}


///////////////////////////////////////////////////////////////////////////////////////////////////////
// CR_One_Iterator methods implementation

//
CR_One_Iterator::CR_One_Iterator(COMPLEX_LIST_ITERATOR p_iterator_begin, size_t p_iterator_size,
	int p_e_id,Environment *p_env)
	: Iterator<COMPLEX_LIST_ITERATOR>(p_iterator_begin,p_iterator_size,IT_COMPLEX_REL_ONE,1)
{
	e_id = p_e_id;
	env = p_env;
}

//
CR_One_Iterator::~CR_One_Iterator()
{
}

//
void CR_One_Iterator::IteratorReset()
{
	iterator_current = iterator_begin;
	current_size = 0;
	while ( current_size != size && (*iterator_current)->GetNumber() == 0 )
	{
		current_size++;
		iterator_current++;
	}
	if ( current_size != size )
	{
		this->SetIterator((*iterator_current)->GetComplex()->GetIterator(e_id),1);
	}
}

//
void CR_One_Iterator::IteratorNext()
{
	if ( current_size != size )
	{
		this->GetIterator(1)->IteratorNext();
		if ( this->GetIterator(1)->IteratorIsEnd() )
		{
			iterator_current++;
			current_size++;
			while ( current_size != size && (*iterator_current)->GetNumber() == 0 )
			{
				current_size++;
				iterator_current++;
			}
			if ( current_size != size )
			{
				this->SetIterator((*iterator_current)->GetComplex()->GetIterator(e_id),1);
			}
		}
	}
}

//
bool CR_One_Iterator::IsEqual(Iterator_Interface *element) 
{
	if ( element->GetCategory() != IT_COMPLEX_REL_ONE )
		return false;
	if ( this->GetIterator(1) == NULL || element->GetIterator(1) == NULL )
		return false;
	if ( !((CG_Iterator *)this->GetIterator(1))->IsEqual( element->GetIterator(1) ) )
		return false;
	return true;
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CR_Two_Iterator methods implementation

//
/*CR_Two_Iterator::CR_Two_Iterator(Environment *p_env) : Iterator_Interface(IT_COMPLEX_REL_TWO,2)
{
	env = p_env;
	wait_iter = true;
}

//
CR_Two_Iterator::~CR_Two_Iterator()
{}

//
void CR_Two_Iterator::IteratorReset()
{
	Complex *c1 = NULL;
	Complex *c2 = NULL;
	Iterator_Interface *iter;
	iter = this->GetIterator(1);
	if ( iter != NULL )
	{
		c1 = *(((CR_One_Iterator *)iter)->GetCurrent());
		iter->IteratorReset();
	}
	iter = this->GetIterator(2);
	if ( iter != NULL )
	{
		c2 = *(((CR_One_Iterator *)iter)->GetCurrent());
		iter->IteratorReset();
	}
	
	if ( c1 == c2 && c1 != NULL ) 
	{
		wait_iter = true;
	}
	else
	{
		wait_iter = false;
	}
}

//
void CR_Two_Iterator::IteratorNext()
{
	if ( this->GetIterator(1) != NULL && this->GetIterator(2) != NULL )
	{
		if ( wait_iter ) 
		{
			wait_iter = false;
		}
		else
		{
			this->GetIterator(2)->IteratorNext();
			if ( ((CR_One_Iterator *)this->GetIterator(1))->IsEqual( this->GetIterator(2) ) )
				this->GetIterator(2)->IteratorNext();
			if ( this->GetIterator(2)->IteratorIsEnd() )
			{
				this->GetIterator(1)->IteratorNext();
				this->GetIterator(2)->IteratorReset();
			}

			if ( !this->GetIterator(1)->IteratorIsEnd() )
			{ 
				Complex *c1 = *(((CR_One_Iterator *)(this->GetIterator(1)))->GetCurrent());
				Complex *c2 = *(((CR_One_Iterator *)(this->GetIterator(2)))->GetCurrent());
				if ( c1 == c2 ) 
				{
					wait_iter = true;
				}
				else
				{
					wait_iter = false;
				}
			}
		}
	}
	else if ( this->GetIterator(1) != NULL )
	{
		this->GetIterator(1)->IteratorNext();
	}
	else if ( this->GetIterator(2) != NULL )
	{
		this->GetIterator(2)->IteratorNext();
	}
}

//
bool CR_Two_Iterator::IteratorIsEnd()
{
	if ( this->GetIterator(1) != NULL && this->GetIterator(2) != NULL )
		return ( this->GetIterator(1)->IteratorIsEnd() );
	else if ( this->GetIterator(1) != NULL )
		return ( this->GetIterator(1)->IteratorIsEnd() );
	else if ( this->GetIterator(2) != NULL )
		return ( this->GetIterator(2)->IteratorIsEnd() );
	return true;
}*/

//
CR_Two_Iterator::CR_Two_Iterator(Environment *p_env, TwoIteratorKind p_kind, bool p_areEntitiesEqual) : Iterator_Interface(IT_COMPLEX_REL_TWO,2)
{
	env = p_env;
	wait_iter = false;
	areEntitiesEqual = p_areEntitiesEqual;
	kind = p_kind;
}

//
CR_Two_Iterator::~CR_Two_Iterator()
{}


//
void CR_Two_Iterator::IteratorReset()
{	
	Iterator_Interface *iter;
	Complex *c1 = NULL;
	Complex *c2 = NULL;
	iter = this->GetIterator(1);
	if ( iter != NULL )
	{
		iter->IteratorReset();
		c1 = *(((CR_One_Iterator *)(this->GetIterator(1)))->GetCurrent());
	}
	iter = this->GetIterator(2);
	if ( iter != NULL )
	{
		iter->IteratorReset();
		c2 = *(((CR_One_Iterator *)(this->GetIterator(2)))->GetCurrent());
	}
	
	// Se le due entita' sono uguale le rispettive liste di complessi sono uguali.
	// Dunque, all'inizio gli iteratori puntano allo stesso complesso. 
	// Se le entita' sono diverse cerco la prima coppia di complessi uguali
	if ( c1 != NULL && c2 != NULL )
	{
		if (kind == ONLY_MONO && !areEntitiesEqual){
			while (!GetIterator(1)->IteratorIsEnd() && c1 != c2){
				GetIterator(2)->IteratorNext();
				if (this->GetIterator(2)->IteratorIsEnd()){
					this->GetIterator(2)->IteratorReset();
					GetIterator(1)->IteratorNext();
					if ( !GetIterator(1)->IteratorIsEnd() )
						c1 = *(((CR_One_Iterator *)(this->GetIterator(1)))->GetCurrent());
				}
				c2 = *(((CR_One_Iterator *)(this->GetIterator(2)))->GetCurrent());
			}

		}

		if ( !GetIterator(1)->IteratorIsEnd() )
		{
			//rivedi bene come impostare il wait_iter, cos� non mi sembra giusto
			if ( c1 == c2 && kind != ONLY_BIM && !((CR_One_Iterator*)GetIterator(1))->IsEqual(GetIterator(2))) 
			{
				wait_iter = true;
			}
			else
			{
				wait_iter = false;
				if ( ( c1 == c2 && c1->GetNumber() == 1 ) || ( kind == ONLY_MONO ) )
					this->IteratorNext();
			}
		}
	}
	else if (kind == ONLY_MONO )
	{
		while ( !iter->IteratorIsEnd() )
			iter->IteratorNext();
	}
}


//
void CR_Two_Iterator::IteratorNext()
{
	if ( this->GetIterator(1) != NULL && this->GetIterator(2) != NULL )
	{
		if ( wait_iter && kind != ONLY_MONO ) 
		{
			Complex *c1 = *(((CR_One_Iterator *)(this->GetIterator(1)))->GetCurrent());
			Complex *c2 = *(((CR_One_Iterator *)(this->GetIterator(2)))->GetCurrent());
			wait_iter = false;
			if ( c1 == c2 && c1->GetNumber() == 1 )
				this->IteratorNext();
		}
		else
		{
			this->GetIterator(2)->IteratorNext();
			if ( this->GetIterator(2)->IteratorIsEnd() )
			{
				this->GetIterator(1)->IteratorNext();
				this->GetIterator(2)->IteratorReset();
				if ( !this->GetIterator(1)->IteratorIsEnd() && areEntitiesEqual){
					while (!((CR_One_Iterator*)GetIterator(1))->IsEqual(GetIterator(2))){
						GetIterator(2)->IteratorNext();
					}
				} 
			}

			if ( !this->GetIterator(1)->IteratorIsEnd() )
			{ 
				Complex *c1 = *(((CR_One_Iterator *)(this->GetIterator(1)))->GetCurrent());
				Complex *c2 = *(((CR_One_Iterator *)(this->GetIterator(2)))->GetCurrent());

				if (kind == ONLY_MONO && !areEntitiesEqual) {
					while (!GetIterator(1)->IteratorIsEnd() && c1 != c2){
						/*if (areEntitiesEqual){
						GetIterator(1)->IteratorNext();
						if(!((CR_One_Iterator*)GetIterator(1))->IsEqual(GetIterator(2))){
						GetIterator(2)->IteratorReset();
						while (!((CR_One_Iterator*)GetIterator(1))->IsEqual(GetIterator(2))){
						GetIterator(2)->IteratorNext();
						}
						}
						} else {*/
						GetIterator(2)->IteratorNext();
						if (this->GetIterator(2)->IteratorIsEnd()){
							this->GetIterator(2)->IteratorReset();
							GetIterator(1)->IteratorNext();
							if ( !GetIterator(1)->IteratorIsEnd() )
								c1 = *(((CR_One_Iterator *)(this->GetIterator(1)))->GetCurrent());
						}
						// }
						c2 = *(((CR_One_Iterator *)(this->GetIterator(2)))->GetCurrent());
					}
				}

				if ( c1 == c2 && kind != ONLY_BIM && !((CR_One_Iterator*)GetIterator(1))->IsEqual(GetIterator(2))) {
					wait_iter = true;
				}
				else {
					wait_iter = false;
					if ( ( c1 == c2 && c1->GetNumber() == 1 ) || ( kind == ONLY_MONO )  )
						this->IteratorNext();
				}
			}
		}
	}
	else if ( this->GetIterator(1) != NULL )
	{
		this->GetIterator(1)->IteratorNext();
	}
	else if ( this->GetIterator(2) != NULL )
	{
		this->GetIterator(2)->IteratorNext();
	}
}

//
bool CR_Two_Iterator::IteratorIsEnd()
{
	if ( this->GetIterator(1) != NULL && this->GetIterator(2) != NULL )
		return ( this->GetIterator(1)->IteratorIsEnd() );
	else if ( this->GetIterator(1) != NULL )
		return ( this->GetIterator(1)->IteratorIsEnd() );
	else if ( this->GetIterator(2) != NULL )
		return ( this->GetIterator(2)->IteratorIsEnd() );
	return true;
}




//////////////////////////////////////////////////////////////////////////////////////////////////////////
// CR_Bind_Iterator methods implementation

//
CR_Bind_Iterator::CR_Bind_Iterator(COMPLEX_LIST_ITERATOR p_iterator_begin,size_t p_iterator_size,
	Entity *p_e1,Entity *p_e2,const string &p_sub1,const string &p_sub2,Environment *p_env) 
	: Iterator<COMPLEX_LIST_ITERATOR>(p_iterator_begin,p_iterator_size,IT_COMPLEX_REL_BIND,1)
{
	e1 = p_e1;
	e2 = p_e2;
	sub1 = p_sub1;
	sub2 = p_sub2;
	env = p_env;
}

//
CR_Bind_Iterator::~CR_Bind_Iterator()
{}

//
void CR_Bind_Iterator::IteratorReset()
{
	iterator_current = iterator_begin;	
	current_size = 0;
	while ( current_size != size && ( (*iterator_current)->GetBinding(e1,e2,sub1,sub2) == 0 
		    || (*iterator_current)->GetNumber() == 0 ) )
	{
		current_size++;
		iterator_current++;
	}
	if ( current_size != size )
	{
		this->SetIterator((*iterator_current)->GetComplex()->GetBindIterator(e1,e2,sub1,sub2),1);
	}
}

//
void CR_Bind_Iterator::IteratorNext()
{
	if ( current_size != size )
	{
		this->GetIterator(1)->IteratorNext();
		if ( this->GetIterator(1)->IteratorIsEnd() )
		{
			iterator_current++;
			current_size++;
			while ( current_size != size && ( (*iterator_current)->GetBinding(e1,e2,sub1,sub2) == 0 
				    || (*iterator_current)->GetNumber() == 0 ) )
			{
				current_size++;
				iterator_current++;
			}
			if ( current_size != size )
			{
				this->SetIterator((*iterator_current)->GetComplex()->GetBindIterator(e1,e2,sub1,sub2),1);
			}
		}
	}
}

Complex *CR_Bind_Iterator::GetCurrentComplex(){
	return (*iterator_current);
}


list<Complex *> * Complex_Rel::GetComplexesList(int e_id){
	if (c_hmap.find(e_id) != c_hmap.end()){
		return c_hmap[e_id];
	}
	return NULL;
}


