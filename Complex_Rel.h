#ifndef COMPLEX_REL_H_INCLUDED
#define COMPLEX_REL_H_INCLUDED

#ifdef _MSC_VER
#include <hash_map>
using namespace stdext;
#else
#include <ext/hash_map>
#endif

#include <vector>
#include <list>
#include <algorithm>
#include "Complex.h"
#include "Complex_Reactions.h"
//#include "Complex_Graph.h"
//#include "Complex_Map.h"
//#include "Define.h"
//#include "Symbol_Table.h"

using namespace std;

//
typedef std::list<Complex *>::iterator COMPLEX_LIST_ITERATOR;
class CG_Node;
class Complex;


/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Iterators for the Complex_Rel Class
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

// 
class CR_One_Iterator : public Iterator<COMPLEX_LIST_ITERATOR>
{
private:
	int e_id;
	Environment *env;
public:
	CR_One_Iterator(COMPLEX_LIST_ITERATOR p_iterator_begin,size_t p_iterator_size,
		int p_e_id,Environment *p_env);
	~CR_One_Iterator();
	void IteratorReset();
	void IteratorNext();
	// This methods return yes if the two iterators has the same e_id and they
	// point to an equal complex with molteplicity equal to one. Returns false otherwise
	bool IsEqual(Iterator_Interface *element);
};

//
/*class CR_Two_Iterator : public Iterator_Interface
{
private:
	Environment *env;
	bool wait_iter;
public:
	CR_Two_Iterator(Environment *p_env);
	~CR_Two_Iterator();
	void IteratorReset();
	void IteratorNext();
	bool IteratorIsEnd();
	void TWait() { wait_iter = true; }
	void FWait() { wait_iter = false; }
	bool IsWait() { return wait_iter; } 
};*/


class CR_Two_Iterator : public Iterator_Interface
{
private:
	Environment *env;
	bool wait_iter;
	bool areEntitiesEqual;
	TwoIteratorKind kind;
	
public:
	CR_Two_Iterator(Environment *p_env, TwoIteratorKind p_kind, bool p_areEntitiesEqual);
	~CR_Two_Iterator();
	void IteratorReset();
	void IteratorNext();
	bool IteratorIsEnd();
	void TWait() { wait_iter = true; }
	void FWait() { wait_iter = false; }
	bool IsWait() { return wait_iter; } 
};

//
class CR_Bind_Iterator : public Iterator<COMPLEX_LIST_ITERATOR>
{
private:
	Entity *e1;
	Entity *e2;
	string sub1;
	string sub2;
	Environment *env;
public:
	CR_Bind_Iterator(COMPLEX_LIST_ITERATOR p_iterator_begin,size_t p_iterator_size,
		Entity *p_e1,Entity *p_e2,const string &p_sub1,const string &p_sub2,Environment *p_env);
	~CR_Bind_Iterator();
	void IteratorReset();
	void IteratorNext();
	Complex *GetCurrentComplex();
};


//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// The complex map contains all the relations Entity <--> Complexes. From an entity identifier (ID) we 
// obtain all the complexes in which the entity ID is present.
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

class Complex_Rel
{
private:
	// mantains the number of the created complexes
	hash_map<int, std::list<Complex*> * > c_hmap;

	// mantains reactions of complexes
	Complex_Reactions history;

	// select randomly one relation
	Complex *Select(Entity *e, std::list<Complex*>* list);
	pair<Complex *, Complex *> SelectTWO(Entity *e1, Entity *e2);
	void IncNodeBind(CG_Node *n1, CG_Node *n2, CG_Node *n3, CG_Node *n4, vector<Map_Elem *> *e_list);
	void DecNodeBind(CG_Node *n1, CG_Node *n2, CG_Node *n3, CG_Node *n4, vector<Map_Elem *> *e_list);

	void FillInReactionKey(reaction_key * key, int p1, int p2, int r1, int r2, ReactionType et);
	// back-reference to my ambient
	Environment* env;

	// iterators variables
	typedef hash_map< int, std::list<Complex *>* >::iterator COMPLEX_HMAP_ITERATOR;

public:
	Complex_Rel(Environment* p_env);
	~Complex_Rel();
	void AddComplex(Complex *comp);
	void AddComplexToEntityIfNotPresent(Entity *species, Complex *complex);
	void DelComplexFromEntityIfSingle(Entity *species, Complex *complex);
	void Mono(reaction_key* res, Entity *e1, Entity *e2, vector<Map_Elem *> *e_list, Iterator_Interface *c_iter);
	void Bim(reaction_key* res, Entity *e1, Entity *e2, Entity *be1, Entity *be2, vector<Map_Elem *> *e_list, Iterator_Interface *c_iter);
	void BimBind(reaction_key* res, Entity *e1, Entity *e2, Entity *be1, Entity *be2, const string& b1, const string& b2, vector<Map_Elem *> *e_list, Iterator_Interface *c_iter);
	void Bind(reaction_key* res, Entity *e1, Entity *e2, Entity *be1, Entity *be2, const string& b1, const string& b2, vector<Map_Elem *> *e_list, Iterator_Interface *c_iter);
	void Unbind(reaction_key* res, Entity *e1, Entity *e2, Entity *be1, Entity *be2, const string& b1, const string& b2, vector<Map_Elem *> *e_list, Iterator_Interface *c_iter);
	void Event(reaction_key* res, Entity* e1, Entity* se1, Entity* se2, int n_se1, int n_se2, VerbType eventType, vector<Map_Elem *>* e_list);
	void Event(reaction_key* res, Entity* e1, Entity* e2,  Entity* se1, int n_se1, VerbType eventType, vector<Map_Elem *>* e_list);
	void Event(reaction_key* res, Entity* e1, VerbType eventType, vector<Map_Elem *>* e_list);

	int GetActiveBindings(Entity *e1, Entity *e2, const string &sub1, const string &sub2);

	void Print();

	// Proxy functions
	void IncBindElemInBindMap(Entity *e1, Entity *e2, const string& b1, const string& b2, const string& key, vector<Map_Elem *> *e_list); 

	// Complex_Rel Iterators
	CR_One_Iterator *GetIteratorOne(int e_id);
	CR_Two_Iterator *GetIteratorTwo(int e_id1,int e_id2);
	CR_Bind_Iterator *GetIteratorBind(Entity *p_e1,Entity *p_e2,const string &p_sub1,const string &p_sub2);

	//Return the list of complexes which contains the entity with ID e_id
	list<Complex *> * GetComplexesList(int e_id);
	void DeleteKey(int key)
	{
		COMPLEX_HMAP_ITERATOR it;
		if ( ( it = c_hmap.find(key) ) != c_hmap.end() )
		{
			if ( (*it).second->empty() )
			{
				delete((*it).second);
			    c_hmap.erase(key);	
			}
		}
	}

	void DecComplex(Complex *c, bool dec);
};

#endif // COMPLEX_REL_H_INCLUDED

