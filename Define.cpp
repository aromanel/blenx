#include "Define.h"

//
// Statics initialization 
//
Pos Pos::Empty(0, 0, "FIXED");
RunningMode theRunningMode;

//
// ToString functions implementation
//
std::string ToString(TempType type)
{
	switch (type)
	{
		case TEMP_NAME:
			return "name";  
		case TEMP_TYPE:
			return "type";
		case TEMP_PPROC:
			return "pproc";
		case TEMP_SEQ:
			return "prefix";
      case TEMP_RATE:
         return "rate";
	}
	return "ERR";
}

std::string ToString(ElementType type)
{
	switch (type)
	{
		case MONO:
			return "mono";      
		case BIM:
			return "bim";
		case BIND:
			return "bind";
		case UNBIND:
			return "unbind";
		case INTER_BIND:
			return "bimbind";
		case EVENT:
			return "event";
		case STATEVAR:
			return "statevar";
	}
	return "ERR";
}

std::string ToString(ReactionType type)
{
	switch (type)
	{
		case R_MONO:
			return "mono";      
		case R_BIM:
			return "bim";
		case R_BIND:
			return "bind";
		case R_UNBIND:
			return "unbind";
		case R_INTER_BIND:
			return "bimbind";
		case R_STATEVAR:
			return "statevar";
		case R_SPLIT:
			return "split";
		case R_NEW:
			return "new";
		case R_DELETE:
			return "delete";
		case R_JOIN:
			return "join";
		case R_UPDATE:
			return "update";
		case R_UNKNOWN:
			return "unknown reaction";
	}

	return "ERR";
}



std::string ToString(ExpressionType type)
{
	switch (type)
	{
		case NOT:
			return "not";  
		case OR:
			return "or";
		case AND:
			return "and";
	}
	return "ERR";
}

std::string ToString(BinderState type)
{
	switch (type)
	{
		case STATE_HIDDEN:
			return "H";  
		case STATE_UNHIDDEN:
			return "U";
		case STATE_BOUND:
			return "B";
		case STATE_NOT_SPECIFIED:
			return "not specified";
	}
	return "ERR";
}

std::string ToStringC(BinderState type) 
{
	switch (type)
	{
		case STATE_HIDDEN:
			return "hidden";  
		case STATE_UNHIDDEN:
			return "unhidden";
		case STATE_BOUND:
			return "bound";
		case STATE_NOT_SPECIFIED:
			return "not specified";
	}
	return "ERR";
}

std::string ToString(ASTNodeType type)
{
	switch (type)
	{
		case PROGRAM: return "program";
		case INFO: return "info";
		case RATE: return "rate";
		case RATE_PAIR: return "rate_pair";
		case DEC: return "dec";
		case DEC_PAIR: return "dec_pair";
		case NAME_LIST: return "name_list";
		case NAME_LIST_PAIR: return "name_list_pair";

		case NIL: return "nil";
		case ID: return "id";
		case ACTION: return "action";
		case REPLICATION: return "replication";
		case CHOICE: return "choice";
		case PARALLEL: return "parallel";

		case IO: return "io";
		case EXPOSE: return "expose";
		case EXPOSE_NL: return "expose_nl";
		case HIDEUNHIDE: return "uh";
		case CHANGE: return "ch";
		case TAU: return "tau";
		case DIE: return "die";

		case BETA: return "beta";	
		case BINDER: return "binder";
		case BINDER_PAIR: return "binder_pair";
		case BINDER_NL: return "binder_nl";
		case BP: return "bp";
		case BP_PAIR: return "bp_pair";

		case MOLECULE: return "mol";
		case MOL_EDGE: return "mod_edge";
		case MOL_EDGE_LIST: return "mol_edge_list";
		case MOL_NODE: return "mol_node";
		case MOL_NODE_LIST: return "mol_node_list";
		case MOL_BINDER: return "mol_binder";
		case MOL_BINDER_LIST: return "mol_binder_list";

		case EVENT_EVENT: return "event";
		case EVENT_VERB: return "event_verb";
		case EVENT_COND: return "event_cond";

		case ATOM_IFTHEN: return "atom_ifthen";
		case NOT_IFTHEN: return "not_ifthen";
		case AND_IFTHEN: return "and_ifthen";
		case OR_IFTHEN: return "or_ifthen";
		case IFTHEN: return "if_then";

		case ACTION_LIST: return "action_list";
		case EVENT_COND_STATE: return "state";
		case EVENT_COND_STATELIST: return "statelist";
		case EVENT_ENTITY: return "entity";
		case EVENT_ENTITY_LIST: return "entitylist";
		case TEMPLATE: return "template";
		case INV_TEMPLATE: return "template_instantiation";
		case DEC_TEMP_ELEM: return "dec_temp_elem";
		case DEC_TEMP_LIST: return "dec_temp_list";
		case INV_TEMP_ELEM: return "inv_temp_elem";
		case INV_TEMP_LIST: return "inv_temp_list";
      case HYPEXP_PARAM: return "hyperexp_param";

      default: 
         assert(false);
	}
	return "ERR";
}

std::string ToString(EntityType type)
{
	switch (type)
	{
		case PPROC: return "pproc";
		case BPROC: return "bproc";
		case MOL: return "mol";
		case SEQUENCE: return "sequence";
		case STATE_VAR: return "statevar";
		case EVENT_DEF: return "event";
		case PPROC_TEMPLATE: return "pproc(T)";
		case BPROC_TEMPLATE: return "bproc(T)";
		case PPROC_TEMPLATE_REF: return "pproc<<>>";
		case TYPE_TEMPLATE_REF: return "binder<<>>";
		case NAME_TEMPLATE_REF: return "name<<>>";
		case SEQ_TEMPLATE_REF: return "prefix<<>>";
      case RATE_TEMPLATE_REF: return "rate<<>>";
	}
	return "ERR";
}

std::string ToString(NodeType type)
{
	switch (type)
	{
		case PP_ROOT: return "_root";
		case PP_NIL: return "_nil";
		case PP_ID: return "_id";
		case PP_SEQ: return "_seq";
		case PP_REPLICATION: return "_replication";
		case PP_PARALLEL: return "_parallel";
		case PP_CHOICE: return "_choice";
		case PP_CHANGE: return "_change";
		case PP_TAU: return "_tau";
		case PP_INPUT: return "_input";
		case PP_OUTPUT: return "_output";
		case PP_EXPOSE: return "_expose";
		case PP_HIDE: return "_hide";
		case PP_UNHIDE: return "_unhide";
		case PP_DIE: return "_die";
		case PP_ATOM: return "_atom";
		case PP_EXPRESSION: return "_expression";
		case PP_IFTHEN: return "_ifthen";
		case PP_TEMPLATE_NAME: return "_templ";
	}
	return "ERR";
}

std::string ToString(MonoType type)
{
	switch (type)
	{
		case MONO_TAU: return "TAU";
		case MONO_CHANGE: return "CHANGE";
		case MONO_INTRA: return "INTRA";
		case MONO_EXPOSE: return "EXPOSE";
		case MONO_HIDE: return "HIDE";
		case MONO_UNHIDE: return "UNHIDE";
		case MONO_DIE: return "DIE";
		case MONO_EMPTY: return "EMPTY";
	}
	return "ERR";
}

std::string ToString(VerbType verbType)
{
   switch (verbType)
   {
   case VERB_DELETE : return "DELETE";
   case VERB_SPLIT : return "SPLIT";
	case VERB_NEW : return "NEW";
	case VERB_JOIN : return "JOIN";
	case VERB_UPDATE : return "UPDATE";
   case VERB_NOTHING : return "NOTHING(ERR?)";
   }
   return "ERR";
}

std::string ToString(ExprOpType type)
{
	switch (type)
	{
		case EXPR_PLUS:
			return "+";  
		case EXPR_MINUS:
			return "-";
		case EXPR_TIMES:
			return "*";
		case EXPR_DIV:
			return "/";
	}
	return "ERR";
}


