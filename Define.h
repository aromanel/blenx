#ifndef DEFINE_H
#define DEFINE_H

#include <vector>
#include <string>
#include <list>
#include <cstdlib>
#include <cassert>
#include <sstream>

#include "Singleton.h"

//TODO: AARGH! REMOVE!!!
class Environment;
class Entity;
class Complex;
class Expr;

using namespace std;

// Visual studio specific directives
#ifdef _MSC_VER

//unreferenced formal parameter
#pragma warning (disable: 4100)
//unreferenced local function removed (bison)
#pragma warning (disable: 4505)
//decorated name length exceeded, name was truncated (STL, release builds)
#pragma warning (disable: 4503)
//assignment operator could not be generated
#pragma warning (disable: 4512)

#endif

//TIMING functions (for seeds)

#ifdef _MSC_VER

#include <sys/timeb.h>
#include <sys/types.h>

inline unsigned int GetTime()
{
   struct _timeb timebuffer;   
   _ftime( &timebuffer );

   unsigned int res = (unsigned int)((timebuffer.time * 1000) + timebuffer.millitm);

   return res;
}


#else //posix

#include <sys/timeb.h>
#include <sys/types.h>

inline unsigned int GetTime()
{
   struct timeb timebuffer;   
   ftime( &timebuffer );

   unsigned int res = (unsigned int)((timebuffer.time * 1000) + timebuffer.millitm);

   return res;
}

#endif

struct Pos
{
   size_t Line;
   size_t Col;
   std::string Filename;

   Pos(size_t line,  size_t col, const std::string& filename)
      : Line(line), Col(col), Filename(filename) { }

   static Pos Empty;
};

//
//
//
template <class T, class U, class V>
struct triple
{
   T first_elem;
   U second_elem;
   V third_elem;

   triple(const T& t, const U& u, const V& v)
      : first_elem(t), second_elem(u), third_elem(v) { }

   triple() { }

   const T& first() const
   {
      return first_elem;
   }

   const U& second() const
   {
      return second_elem;
   }

   const V& third() const
   {
      return third_elem;
   }

};


//////////////////////////////////////////////////////
// FREE NAMES STRUCT
//////////////////////////////////////////////////////


enum FreeNameType
{
   FN_BINDER_SUB,
   FN_GENERAL
};

struct Free_Name {
   string name;
   FreeNameType type;

   Free_Name() {}

   Free_Name(const string& p_name, const FreeNameType& p_type) {
      name = p_name;
      type = p_type;
   }

   Free_Name(const Free_Name& element) {
      name = element.name;
      type = element.type;
   }

};

//////////////////////////////////////////////////////
// FREE NAMES STRUCT
//////////////////////////////////////////////////////

enum SimMode 
{
   M_STEP,
   M_STEP_DELTA,
   M_TIME,
   M_NONE
};

enum BoundNameType
{
   BN_EXPOSE,
   BN_INPUT,
   BN_BINDER
};


struct Bound_Name {
   string name;
   BoundNameType type;

   Bound_Name() {}

   Bound_Name(const string& p_name, const BoundNameType& p_type) {
      name = p_name;
      type = p_type;
   }

   Bound_Name(const Bound_Name& element) {
      name = element.name;
      type = element.type;
   }
};


//////////////////////////////////////////////
// ITERATOR CLASSES
//////////////////////////////////////////////

//
enum IteratorCategory
{
   IT_MONO_INTRA,
   IT_MONO_INTRA_IN,
   IT_MONO_INTRA_OUT,
   IT_MONO_TAU,
   IT_MONO_DIE,
   IT_MONO_CHANGE,
   IT_MONO_EXPOSE,
   IT_MONO_HIDE,
   IT_MONO_UNHIDE,
   IT_ENTITY,
   IT_ENTITY_INF,
   IT_ENTITY_INTRA_MAP,
   IT_ENTITY_TAU_MAP,
   IT_ENTITY_DIE_MAP,
   IT_ENTITY_CHANGE_MAP,
   IT_ENTITY_EXPOSE_MAP,
   IT_ENTITY_HIDE_MAP,
   IT_ENTITY_UNHIDE_MAP,
   IT_BIMOLECULAR,
   IT_COMPLEX_REL_ONE,
   IT_COMPLEX_REL_TWO,
   IT_COMPLEX_REL_BIND,
   IT_COMPLEX_GRAPH,
   IT_COMPLEX_GRAPH_NODES,
   IT_COMPLEX_GRAPH_BIND,
   IT_ACT_MAP,
   IT_INF_ACT_MAP,
   IT_MONO_ACT,
   IT_SIMPLE,
   IT_CONSTANTS
};

enum TwoIteratorKind{
   ONLY_MONO,
   ONLY_BIM,
   BIM_AND_MONO
};


//
class Iterator_Interface 
{
private:
   Iterator_Interface **iter_array;
   int iter_array_length;
protected:
   IteratorCategory category;
public:
   Iterator_Interface(IteratorCategory p_category, int iter_num) 
   {
      category = p_category;
      if ( iter_num <= 0 )
      {
         iter_array = NULL;
         iter_array_length = 0;
      }
      else
      {
         iter_array = new Iterator_Interface *[iter_num];
         for(int i=0; i<iter_num; i++)
            iter_array[i] = NULL;
         iter_array_length = iter_num;
      }
   }
   virtual ~Iterator_Interface() {
      if ( iter_array != NULL )
      {
         for(int i=0; i<iter_array_length; i++)
            delete iter_array[i];

         delete []iter_array;
      }

   }
   virtual void IteratorReset() = 0;
   virtual void IteratorNext() = 0;
   virtual bool IteratorIsEnd() = 0;
   // la numerazione della posizione parte da 1
   void SetIterator(Iterator_Interface *iter, int position)
   {
      if ( position <= 0 || position > iter_array_length )
         return;

      if ( iter_array[position-1] != NULL )
         delete iter_array[position-1];

      iter_array[position-1] = iter;

      // Se l'iteratore che passo e' diverso da NULL
      // lo Resetto 
      if ( iter != NULL )
         iter->IteratorReset();
   }
   Iterator_Interface *GetIterator(int position)
   {
      if ( position <= 0 || position > iter_array_length )
         return NULL;

      return iter_array[position-1];
   }

   IteratorCategory GetCategory() { return category; }

   int GetLength()
   {
      return iter_array_length;
   }
};

//
template <class T>
class Iterator : public Iterator_Interface
{
protected:
   T iterator_begin;		
   // T iterator_end;
   T iterator_current;
   size_t size;
   size_t current_size;
public:

   Iterator(T p_iterator_begin, size_t p_size, IteratorCategory p_category, int iter_num) 
      : Iterator_Interface(p_category,iter_num) 
   {
      iterator_begin = p_iterator_begin;
      //iterator_end = p_iterator_end;
      size = p_size;
      current_size = 0;
   }

   Iterator(Iterator &element)
   {
      iterator_begin = element.iterator_begin;
      //iterator_end = element.iterator_end;
      size = element.size;
      current_size = element.current_size;
      iterator_current = element.iterator_current;
   }

   virtual ~Iterator() {}

   // this methods iterates on the nodes with id equal to e_id
   virtual void IteratorReset()
   {
      iterator_current = iterator_begin;
      current_size = 0;
   }

   virtual bool IteratorIsEnd()
   {
      // return  ( iterator_current == iterator_end );
      return ( size == current_size );
   }

   virtual void IteratorNext()
   {
      /* if ( iterator_current != iterator_end )
      iterator_current++; */
      if ( current_size != size )
      {
         current_size++;
         iterator_current++;
      }
   }

   virtual T GetCurrent()
   {
      return iterator_current;
   }
};


//////////////////////////////////////////////////////
// ELEMENT CLASS
//////////////////////////////////////////////////////

enum ElementType
{
   MONO,	
   BIM,	
   BIND,		
   UNBIND,	
   INTER_BIND,	
   STATEVAR,
   EVENT
};

enum ReactionType
{
   R_MONO,	
   R_BIM,	
   R_BIND,		
   R_UNBIND,	
   R_INTER_BIND,	
   R_STATEVAR,
   R_SPLIT,
   R_NEW,
   R_DELETE,
   R_JOIN,
   R_UPDATE,
   R_UNKNOWN
};

enum StateVarType
{
   STATEVAR_CONTINUOUS,
   STAREVAR_DISCRETE
};

class Element 
{
protected:
   ElementType category;
   Environment* env;

public:
   Element(Environment* p_env, ElementType p_category) 
   {
      env = p_env;
      category = p_category;
   }

   virtual ~Element() {}
   ElementType GetCategory() { return category; }
   virtual double GetTotal() { return 0.0; }
   virtual bool IsDiscrete() 
   { 
      double res =  this->GetTotal(); 
      return ( res > 0 ); 
   }
   virtual void Rate() {}
   // Ritorna il rate dell'azione a cui si riferisce l'iteratore
   virtual double GetRate(Iterator_Interface *c_iter) { return 0.0; }
   // this method is no more used by any function
   virtual double GetRate() { return 0.0; }
   // Ritorna il rate attuale associato all'Element
   virtual double GetActualRate() { return 0.0; }
   virtual double GetBasalRate(Iterator_Interface *c_iter) { return 0.0; };

   virtual Expr* GetExpr(Iterator_Interface *c_iter) = 0;

   virtual double Time() = 0;
   virtual bool Is(Element *elem) = 0;
   virtual void Print() {}
   //virtual Iterator_Interface * GetIterator(Complex * comp) = 0;
   virtual void GetReactants(vector<Entity *> &reactants) {};
   virtual void GetIterator(pair<Entity *, Entity *> *, list<Complex *> *comp, pair<Iterator_Interface *, Iterator_Interface *> *) = 0;
   virtual Iterator_Interface * GetIterator(Entity* e1, Entity* e2, list<Complex *> *c1, list<Complex *> *c2, TwoIteratorKind iterator_kind) = 0;

   //Return true if the Element represents a monomolecular reaction. The
   //vector reactants is used by the function to store the reactants of the
   //Element; in the case of one reactants the second element of the vector
   //is a -1. Remember that having two reactants doesn't mean that we have a
   //bimolecular reaction, it is the case of the Unbind element 
   virtual bool IsOnlyMono(int reactants[2]) = 0;

   Environment* GetEnv() { return env; }
};


enum MonoType
{
   MONO_TAU,
   MONO_CHANGE,
   MONO_INTRA,
   MONO_EXPOSE,
   MONO_HIDE,
   MONO_UNHIDE,
   MONO_DIE,
   MONO_EMPTY
};

//
class ElementMONO {
protected:
   MonoType category;
   Element* my_elem;
public:
   ElementMONO(Element* p_elem, MonoType p_category) 
   {
      my_elem = p_elem;
      category = p_category;
   }
   virtual ~ElementMONO() { }
   MonoType GetCategory() { return category; }
   virtual int GetNumber() = 0;
   virtual double EvalRate() = 0;
   virtual double GetActualRate() = 0;
   virtual double GetRate() = 0;
   virtual bool ARateZero() = 0;
   virtual void Print() {};
   virtual Iterator_Interface *GetIterator() { return NULL; }

   Element* GetParent() { return my_elem; }
};


//////////////////////////////////////////////////////
// Base value for the nameless procedure invocation
//////////////////////////////////////////////////////

#define NAMELESS 10


//////////////////////////////////////////////////////
// Varibles definition for the class Error_Manager
//////////////////////////////////////////////////////

const std::string ERROR("ERROR");
const std::string VALID	("");
const std::string EMPTY_STRING("");
const std::string NULL_STRING("NULL");
const std::string EVENT_STRING("Event");
const int EMPTY_ID = -1;
const std::string NO_PROCESS("$X");
const std::string NIL_BPROC("$NIL");
const std::string BPROC1("$B1");
const std::string BPROC2("$B2");

const int NIL_ID = 0;

//////////////////////////////////////////////////////
// Variables definition for the Parser
//////////////////////////////////////////////////////

// Action type
enum ActionType
{
   ACTION_INPUT,
   ACTION_OUTPUT,
   ACTION_HIDE,
   ACTION_UNHIDE
};

enum BinderState
{
   STATE_HIDDEN,			// H = hide
   STATE_UNHIDDEN,			// U = unhide
   STATE_BOUND,			// B = legato
   STATE_NOT_SPECIFIED
};

enum ExpressionType
{
   NOT,
   AND,
   OR
};

// AST node types
enum ASTNodeType
{
   PROGRAM,
   INFO,
   RATE,
   RATE_PAIR,
   DEC,
   DEC_PAIR,
   NAME_LIST,
   NAME_LIST_PAIR,

   NIL,
   ID,
   ACTION,
   REPLICATION,
   CHOICE,
   PARALLEL,
   ACTION_LIST,

   IO,
   EXPOSE,
   EXPOSE_NL,
   HIDEUNHIDE,
   CHANGE,
   TAU,
   DIE,

   BETA,	
   BINDER,
   BINDER_PAIR,
   BINDER_NL,
   BP,
   BP_PAIR,

   MOLECULE,
   MOL_EDGE,
   MOL_EDGE_LIST,
   MOL_NODE,
   MOL_NODE_LIST,
   MOL_BINDER,
   MOL_BINDER_LIST,

   EVENT_EVENT,
   EVENT_VERB,
   EVENT_COND,
   EVENT_COND_STATE,
   EVENT_COND_STATELIST,
   EVENT_ENTITY,
   EVENT_ENTITY_LIST,
   TEMPLATE,
   INV_TEMPLATE,
   DEC_TEMP_ELEM,
   DEC_TEMP_LIST,
   INV_TEMP_ELEM,
   INV_TEMP_LIST,

   ATOM_IFTHEN,
   NOT_IFTHEN,
   AND_IFTHEN,
   OR_IFTHEN,
   IFTHEN,

   HYPEXP_PARAM
};

//////////////////////////////////////////////////////
// Variables definition for the Symbol Table
//////////////////////////////////////////////////////

//NB! Numbers MUST correspond to the numbers of EntityType below!!
enum TempType
{
   TEMP_PPROC = 10,
   TEMP_TYPE = 11,
   TEMP_NAME = 12,
   TEMP_SEQ = 13,
   TEMP_RATE = 14
};

// Entity types
enum EntityType
{
   PPROC,
   BPROC,
   MOL,
   SEQUENCE,
   STATE_VAR,
   EVENT_DEF,
   PPROC_TEMPLATE,
   BPROC_TEMPLATE,
   PPROC_TEMPLATE_REF = 10,
   TYPE_TEMPLATE_REF = 11,
   NAME_TEMPLATE_REF = 12,
   SEQ_TEMPLATE_REF = 13,
   RATE_TEMPLATE_REF = 14
};



//////////////////////////////////////////////////////
// Variables definition for the Pi_Process class
//////////////////////////////////////////////////////

// Node type

enum NodeType
{
   PP_ROOT,
   PP_NIL,				
   PP_ID,
   PP_SEQ,
   PP_REPLICATION,
   PP_PARALLEL,
   PP_CHOICE,
   PP_CHANGE,
   PP_TAU,
   PP_INPUT,
   PP_OUTPUT,
   PP_EXPOSE,
   PP_HIDE,
   PP_UNHIDE,
   PP_DIE,
   PP_ATOM,
   PP_EXPRESSION,
   PP_IFTHEN,
   PP_TEMPLATE_NAME
};


// Names types
#define BN					"$bn"
#define BIND_N				"$bbn"
#define TMP_N				"$tmp"
#define EMPTY_OUTPUT		"$emptout"


//////////////////////////////////////////////////////
// Variables definition for the Logger
//////////////////////////////////////////////////////

#define HTML				true
#define TXT					false


//////////////////////////////////////////////////////
// General definitions
//////////////////////////////////////////////////////

#define INF_RATE			"inf"


//////////////////////////////////////////////////////
// Variables definition for the Priority Queue
//////////////////////////////////////////////////////

enum CondType
{
   COND_RATE,
   COND_COUNT_EQUAL,
   COND_COUNT_GREATER,
   COND_COUNT_LESS,
   COND_COUNT_NEQUAL,
   COND_COUNT_CHANGED,
   COND_TIME_EQUAL,
   COND_STEP_EQUAL,
   COND_RATE_FUN,
   COND_STATELIST,
   COND_EXPRESSION,
   COND_RATE_IMMEDIATE,
   COND_STATES,
   COND_RATE_NORMAL,
   COND_RATE_GAMMA,
   COND_RATE_HYPEXP
};

enum VerbType
{ 
   VERB_NOTHING = 0,
   VERB_SPLIT,
   VERB_NEW,
   VERB_DELETE,
   VERB_JOIN,
   VERB_UPDATE
};

enum StateType
{
   STATE_UP,
   STATE_DOWN
};

enum SplitType
{
   SPLIT_SYMMETRIC,  // the targets are symmetric and equal to the subject
   SPLIT_DIVIDED,    // binders are divided between the targets
   SPLIT_ASYMMETRIC, // binders are divided between the targets, but some of them are duplicated
   SPLIT_WRONG,      // IsSplitOk == false
   SPLIT_UNDEF
};

//////////////////////////////////////////////////////
// Running modes
/////////////////////////////////////////////////////

class RunningMode : public Singleton<RunningMode>
{
public:
   std::vector<std::string> stringModes;

   enum RunningModeType
   {
      GILLESPIE,
      TS,
      //TAULEAP,
      SBML,
      //PARAMETERS,
      NONE
   };

   RunningMode()
   {
      stringModes.push_back("GILLESPIE");
      stringModes.push_back("TS");
      //stringModes.push_back("TAULEAP");
      stringModes.push_back("SBML");
      //stringModes.push_back("PARAMETERS");
      stringModes.push_back("NONE");
   }

   std::string& ToString(RunningModeType runningMode)
   {
      int idx = (int)runningMode;
      if (idx >= 0 && idx < (int)NONE)      
         return stringModes[idx];      
      else
         return stringModes[(int)NONE];
   }   

   RunningModeType FromString(const std::string& str)
   {
      for (int i = 0; i < (int)NONE; ++i)
      {
         if (stringModes[i] == str)
            return (RunningModeType)i;
      }
      return NONE;
   }
};


//////////////////////////////////////////////////////
// Expression operation types
/////////////////////////////////////////////////////

enum ExprOpType
{
   EXPR_PLUS, 
   EXPR_MINUS,
   EXPR_TIMES,
   EXPR_DIV
};

/////////////////////////////////////////////////////

std::string ToString(VerbType type);
std::string ToString(ElementType type);
std::string ToString(ASTNodeType type);
std::string ToString(EntityType type);
std::string ToString(NodeType type);
std::string ToString(MonoType type);
std::string ToString(TempType type);
std::string ToString(BinderState type);
std::string ToString(ExpressionType type);
std::string ToString(ReactionType type);
std::string ToString(RunningMode mode);
std::string ToString(ExprOpType type);

// this function returns complete strings for beta binder status
// instead of characters
std::string ToStringC(BinderState type);


//////////////////////////////////////////////////////
// Function Rate Types
/////////////////////////////////////////////////////

enum FunctionRateType
{
   RATE_MONO,
   RATE_BIM,
   RATE_GENERIC
}; 


//////////////////////////////////////////////////////
// Structures and Utility classes
//////////////////////////////////////////////////////

//
struct Sub {
   string old_v;
   string new_v;
   Sub() {}
   Sub(const string& p_old_v, const string& p_new_v) {
      old_v = p_old_v;
      new_v = p_new_v;
   }
};

//
class Utility {
public:
   static bool IsNumeric(const string& test)
   {
      std::istringstream inpStream(test);
      double inpValue = 0.0;
      if (inpStream >> inpValue)
         return true;
      else
         return false;
   }

   template<class T>
   static string i2s(T number) 
   {
      std::stringstream ss;
      ss << number;
      return ss.str();
   }

   static bool sg(int number) 
   {
      if (number > 0) return 1;
      return 0;
   }

   static void Tokenize(const string& str,
      vector<string>& tokens,
      const string& delimiters = " ")
   {
      // Skip delimiters at beginning.
      string::size_type lastPos = str.find_first_not_of(delimiters, 0);
      // Find first "non-delimiter".
      string::size_type pos = str.find_first_of(delimiters, lastPos);

      while (string::npos != pos || string::npos != lastPos)
      {
         // Found a token, add it to the vector.
         tokens.push_back(str.substr(lastPos, pos - lastPos));
         // Skip delimiters.  Note the "not_of"
         lastPos = str.find_first_not_of(delimiters, pos);
         // Find next "non-delimiter"
         pos = str.find_first_of(delimiters, lastPos);
      }
   }

   static void VVSKey(const std::vector<string>& data1, const std::vector<string>& data2, 
      const string& edge, string *s_merge) 
   {
      bool inserted = false;
      std::vector<string>::const_iterator i = data1.begin();
      std::vector<string>::const_iterator j = data2.begin();

      while( i != data1.end() || j != data2.end() ) {
         if ( j == data2.end() || ( i != data1.end() && (*i) < (*j) ) )
            if ( !inserted && edge < (*i) ) {
               (*s_merge) += edge + "-";
               inserted = true;
            }
            else
               (*s_merge) += (*i++) + "-";
         else 
            if ( !inserted && edge < (*j) ) {
               (*s_merge) += edge + "-";
               inserted = true;
            }
            else
               (*s_merge) += (*j++) + "-";
      }

      if (!inserted) (*s_merge) += edge + "-";
   }

   static void VSKey(const std::vector<string>& data, const string& edge, string *s_merge) 
   {
      bool inserted = false;
      std::vector<string>::const_iterator i = data.begin();
      while( i != data.end() ) {
         if ( edge < (*i) && !inserted) {
            (*s_merge) += edge + "-";
            inserted = true;
         }
         else (*s_merge) += (*i++) + "-";
      }
      if (!inserted) (*s_merge) += edge + "-";
   }

   static void SSKey(const string& edge1, const string& edge2, string *s_merge) 
   {
      if ( edge1 < edge2 ) (*s_merge) = edge1 + "-" + edge2 + "-";
      else (*s_merge) = edge2 + "-" + edge1 + "-";
   }

   static void VKey(const std::vector<string>& data, string *s_merge) {
      std::vector<string>::const_iterator i;
      for( i = data.begin() ; i != data.end() ; i++ ) 
         (*s_merge) += (*i) + "-";
   }   
};


// Struttura per ricordarmi valore iniziale delle costanti e il valore 
// attuale modificato
struct paramValues {
   double init_value;
   double current_value;
};


#endif
