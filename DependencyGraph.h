
#ifndef DEPENDECY_GRAPH_H_INCLUDED
#define DEPENDECY_GRAPH_H_INCLUDED

#include <vector>
#include "hash.h"

template <class T>
class DependencyGraphNode
{
public:
   T token;
   std::vector<T> depend_list;
};


template <class T>
class DependencyGraph
{
private:
   hash_map< int, DependencyGraphNode<T> > nodeMap;
public:
   void Add(T token, const std::vector<T> list_of_tokens)
   {
      int id = token->GetId();
      DependencyGraphNode<T>& node = nodeMap[id];
      node.token = token;
      node.depend_list.insert(node.depend_list.end(), list_of_tokens.begin(), list_of_tokens.end());
   }


   void GetAllDependencies(int id, std::vector<T>& dependencies)
   {
      dependencies.clear();
      DependencyGraphNode<T>& node = nodeMap[id];
      GetAllDependenciesHelper(node);
   }

private:
   typedef typename std::vector<T>::iterator DEP_IT;

   void GetAllDependenciesHelper(const DependencyGraphNode<T>& node, std::vector<T>& dependencies)
   {      
      dependencies.insert(dependencies.end(), node.depend_list.begin(), node.depend_list.end());

      DEP_IT it;
      for (it = node.depend_list.begin(); it != node.depend_list.end(); ++it)
      {
         DependencyGraphNode<T>& next_node = nodeMap[(*it)->GetId()];
         GetAllDependenciesHelper(next_node, dependencies);
      }
   }
};

#endif //DEPENDECY_GRAPH_H_INCLUDED

