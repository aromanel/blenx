
#ifndef DEPENDENCY_MAP_H_INCLUDED
#define DEPENDENCY_MAP_H_INCLUDED

#include <vector>
#include <list>
#include "Map_Elem.h"

//The dependency map is a sort of hash_map: from an Entity/Variable identifier (ID)
//Obtains all elements in a ActMap that contains the identifier (as a source or
//target of a bimolecular action, or a mono action)

//Helper structure for the list of actions; a pointer to the correct map_elem
//and the simulation step (to not update twice)

template <class T>
class DependencyMap 
{
private:
   std::vector<std::list<Map_Elem*>*> h_vector;
   typedef typename std::vector<T>::const_iterator DEP_IT;
public:

   //Helper to get a list of elements that have an index like i
   inline list<Map_Elem*>* GetNodeList(int i) { return h_vector[i]; }
   void AddEntry(T key, Map_Elem* elem)
   {
      int e_id = key->GetId();

      assert (e_id < (int)h_vector.size());

      h_vector[e_id]->push_back(elem);
   }

   void ReserveEntry(T key)
   {
      int e_id = key->GetId();
      if ((int)h_vector.size() <= e_id)
      {
         size_t oldsize = h_vector.size();
         h_vector.resize(e_id + 1);
         for (size_t i = oldsize; i < h_vector.size(); ++i)
            h_vector[i] = new list<Map_Elem*>();
      }
   }

   void ReserveEntry(const vector<T>& keys)
   {
      DEP_IT it;   
      for (it = keys.begin(); it != keys.end(); ++it)
      {
         if (*it != NULL)
         ReserveEntry(*it);
      }
   }
};

#endif //DEPENDENCY_MAP_H_INCLUDED


