#include <iostream>
#include <cassert>
#include <limits>
#include "Entity.h"
#include "Event.h"
#include "Environment.h"
#include "Bind.h"
#include "FunStd.h"

//#include "Symbol_Table.h"

using namespace std;


bool BB_Predicate(BBinders *left,BBinders *right) {
	return (left->GetType() < right->GetType());
}

//
Entity::Entity(Environment* p_env, int p_id,list<BBinders *> *p_bb_list, PP_Node *p_pproc)
  : Element(p_env, MONO) 
{
	bb_list = p_bb_list;
	pproc = p_pproc;
	counter = n_actions = 0;
	id = p_id;
	sum = 0.;
}

//
Entity::~Entity() {
	ElementMONO *tmp;
	list<ElementMONO *> remove_list;

	if (bb_list != NULL) {
		BBinders *tmp_bb;
		while(!bb_list->empty()) {
			tmp_bb = bb_list->front();
			bb_list->pop_front();
			delete(tmp_bb);
		}
		delete(bb_list);
	}
	if (pproc != NULL) delete pproc;

	map<string,Tau *>::iterator i0;
	for(i0=tau_map.begin();i0!=tau_map.end();i0++)
		remove_list.push_front((*i0).second);
	map<string,Intra *>::iterator i1;
	for(i1=intra_map.begin();i1!=intra_map.end();i1++)
		remove_list.push_front((*i1).second);
	map<string,Expose *>::iterator i2;
	for(i2=expose_map.begin();i2!=expose_map.end();i2++)
		remove_list.push_front((*i2).second);
	map<string,Hide *>::iterator i3;
	for(i3=hide_map.begin();i3!=hide_map.end();i3++)
		remove_list.push_front((*i3).second);
	map<string,Unhide *>::iterator i4;
	for(i4=unhide_map.begin();i4!=unhide_map.end();i4++)
		remove_list.push_front((*i4).second);
	map<string,Change *>::iterator i5;
	for(i5=change_map.begin();i5!=change_map.end();i5++)
		remove_list.push_front((*i5).second);
	map<string,Die *>::iterator i6;
	for(i6=die_map.begin();i6!=die_map.end();i6++)
		remove_list.push_front((*i6).second);
	
	while(!remove_list.empty()) {
		tmp = remove_list.front();
		remove_list.pop_front();
		delete(tmp);
	}
}

//
bool Entity::Equal(list<BBinders *> *bb, PP_Node *p) {
	if (bb_list->size() != bb->size()) return false;
	list<BBinders *>::iterator i = bb_list->begin();
	list<BBinders *>::iterator j = bb->begin();
	while(i!=bb_list->end() && j!=bb->end()) {
		if ((*i)->GetType() != (*j)->GetType()) return false;
		if ((*i)->GetState() != (*j)->GetState()) return false;
		if ((*i++)->GetRate() != (*j++)->GetRate()) return false;
	}
	return pproc->Equal(p);
}

//
void Entity::EvalActions() {
	map<string,Intra *>::iterator i1;
	for(i1=intra_map.begin();i1!=intra_map.end();i1++)
		(*i1).second->EvalActions();
}

//
void Entity::PE() {
	map<string,Intra *>::iterator i;
	for(i=intra_map.begin();i!=intra_map.end();i++)
		(*i).second->PE();
}

//
void Entity::Rate() {
	double r;
	sum = 0.;

	map<string,Intra *>::iterator i0;
	for(i0=intra_map.begin();i0!=intra_map.end();i0++)
		if ((r = (*i0).second->EvalRate()) == HUGE_VAL) {
			inf_list.push_front((*i0).second);
			n_actions += (*i0).second->GetNumber();
		}
		else sum += r;
	map<string,Expose *>::iterator i1;
	for(i1=expose_map.begin();i1!=expose_map.end();i1++) 
		if ((r = (*i1).second->EvalRate()) == HUGE_VAL) {
			inf_list.push_front((*i1).second);
			n_actions += (*i1).second->GetNumber();;
		}
		else sum += r;
	map<string,Hide *>::iterator i2;
	for(i2=hide_map.begin();i2!=hide_map.end();i2++) 
		if ((r = (*i2).second->EvalRate()) == HUGE_VAL) {
			inf_list.push_front((*i2).second);
			n_actions += (*i2).second->GetNumber();
		}
		else sum += r;
	map<string,Unhide *>::iterator i3;
	for(i3=unhide_map.begin();i3!=unhide_map.end();i3++) 
		if ((r = (*i3).second->EvalRate()) == HUGE_VAL) { 
			inf_list.push_front((*i3).second); 
			n_actions += (*i3).second->GetNumber();
		}
		else sum += r;
	map<string,Tau *>::iterator i4;
	for(i4=tau_map.begin();i4!=tau_map.end();i4++) 
		if ((r = (*i4).second->EvalRate()) == HUGE_VAL) {
			inf_list.push_front((*i4).second);
			n_actions += (*i4).second->GetNumber();
		}
		else sum += r;
	map<string,Change *>::iterator i5;
	for(i5=change_map.begin();i5!=change_map.end();i5++) 
		if ((r = (*i5).second->EvalRate()) == HUGE_VAL) {
			inf_list.push_front((*i5).second);
			n_actions += (*i5).second->GetNumber();
		}
		else sum += r;
	map<string,Die *>::iterator i6;
	for(i6=die_map.begin();i6!=die_map.end();i6++) 
		if ((r = (*i6).second->EvalRate()) == HUGE_VAL) {
			inf_list.push_front((*i6).second);
			n_actions += (*i6).second->GetNumber();
		}
		else sum += r;
}

//
double Entity::GetActualRate() 
{
   if ( c_act.first == NULL )
      return 0.0;
	double val = c_act.first->GetActualRate();
	if ( val == HUGE_VAL ) 
		return HUGE_VAL;
	return ( val * this->counter ); 
}


//
double Entity::Time() 
{
	if (counter == 0) 
		return HUGE_VAL;

	if (!inf_list.empty()) 
	{
		int rn = env->Random0UptoN(n_actions);
		list<ElementMONO *>::iterator i = inf_list.begin();
		while(rn >= (*i)->GetNumber()) 
			rn -= (*i++)->GetNumber();
		c_act.first = *i;
		c_act.second = 0.0;
		return -1.0;
	}

	if ( sum == 0. ) return HUGE_VAL;

	double min_val = HUGE_VAL;
	double s_act;
	double rn_1 = env->Random0To1();
	double rn_2 = env->Random0To1();
	if ( rn_1 == 0. ) min_val = HUGE_VAL;
	else min_val = ( (double)1 / ( (double)counter * sum ) ) * log( (double)1 / rn_1 ); 
	s_act = rn_2 * sum;

	map<string,Intra *>::iterator i0;
	for(i0=intra_map.begin();i0!=intra_map.end();i0++) {
		if ((*i0).second->ARateZero()) continue;
		if (s_act <= (*i0).second->GetActualRate()) {
			c_act.second = min_val;
			c_act.first = (*i0).second;
			return min_val;
		}
		else s_act -= (*i0).second->GetActualRate();
	}
	map<string,Expose *>::iterator i1;
	for(i1=expose_map.begin();i1!=expose_map.end();i1++) {
		if ((*i1).second->ARateZero()) continue;
		if (s_act <= (*i1).second->GetActualRate()) {
			c_act.second = min_val;
			c_act.first = (*i1).second;
			return min_val;
		}
		else s_act -= (*i1).second->GetActualRate();
	}
	map<string,Hide *>::iterator i2;
	for(i2=hide_map.begin();i2!=hide_map.end();i2++) {
		if ((*i2).second->ARateZero()) continue;
		if (s_act <= (*i2).second->GetActualRate()) {
			c_act.second = min_val;
			c_act.first = (*i2).second;
			return min_val;
		}
		else s_act -= (*i2).second->GetActualRate();
	}
	map<string,Unhide *>::iterator i3;
	for(i3=unhide_map.begin();i3!=unhide_map.end();i3++) {
		if ((*i3).second->ARateZero()) continue;
		if (s_act <= (*i3).second->GetActualRate()) {
			c_act.second = min_val;
			c_act.first = (*i3).second;
			return min_val;
		}
		else s_act -= (*i3).second->GetActualRate();
	}
	map<string,Tau *>::iterator i4;
	for(i4=tau_map.begin();i4!=tau_map.end();i4++) {
		if ((*i4).second->ARateZero()) continue;
		if (s_act <= (*i4).second->GetActualRate()) {
			c_act.second = min_val;
			c_act.first = (*i4).second;
			return min_val;
		}
		else s_act -= (*i4).second->GetActualRate();
	}
    map<string,Change *>::iterator i5;
	for(i5=change_map.begin();i5!=change_map.end();i5++) {
		if ((*i5).second->ARateZero()) continue;
		if (s_act <= (*i5).second->GetActualRate()) {
			c_act.second = min_val;
			c_act.first = (*i5).second;
			return min_val;
		}
		else s_act -= (*i5).second->GetActualRate();
	}
	map<string,Die *>::iterator i6;
	for(i6=die_map.begin();i6!=die_map.end();i6++) {
		if ((*i6).second->ARateZero()) continue;
		if (s_act <= (*i6).second->GetActualRate()) {
			c_act.second = min_val;
			c_act.first = (*i6).second;
			return min_val;
		}
		else s_act -= (*i6).second->GetActualRate();
	}

	return min_val;
}

//
Entity *Entity::RedMono(Iterator_Interface *c_iter) 
{
		Entity *new_entity;
		PP_Node *in,*out;
		PP_Node *n_in,*n_out;
		PP_Node *proc;
		list<BBinders *>* n_bb_list = NULL;
		n_in = n_out = NULL;
		bool is_sum1,is_sum2;
		string name;
		string label;
		map<string,Entity *>::iterator i;
		map<string,Mapping *>::iterator cj;

		Iterator_Interface *act_iter = NULL;

		// Se l'iteratore � diverso da NULL imposto l'azione
		// attuale con quella puntata dall'iteratore
		if ( c_iter != NULL )
		{
			if (  c_iter->GetCategory() == IT_ENTITY )
			{
				c_act.first = ((Entity_Iterator *)c_iter)->GetCurrentAction();
				act_iter = ((Entity_Iterator *)c_iter)->GetCurrentIterator();
			}
			else if ( c_iter->GetCategory() == IT_ENTITY_INF )
			{
				c_act.first = (*((Entity_Iterator_Inf *)c_iter)->GetCurrent());
				act_iter = ((Entity_Iterator_Inf *)c_iter)->GetIterator(1);
			}
		}

		switch (c_act.first->GetCategory())
		{
		case MONO_TAU:
			{
				label = ((Tau *)c_act.first)->GetAction(&in,&is_sum1);
				if (( i = entity_ref.find(label)) == entity_ref.end()) {
					proc = pproc->GetCopyRed(in,NULL,&n_in,&n_out);
					n_in->RedProcess(&proc,is_sum1,&name);
					n_bb_list = this->GetCopyBB();
				}         
			}
			break;

		case MONO_CHANGE:
			{
				int binder_counter = 0;
				label = ((Change *)c_act.first)->GetAction(&in,&is_sum1,act_iter);
				if (( i = entity_ref.find(label)) == entity_ref.end()) {
					proc = pproc->GetCopyRed(in,NULL,&n_in,&n_out);
					n_in->RedProcess(&proc,is_sum1,&name);
					n_bb_list = this->GetCopyBBChange(((PP_Change *)in)->GetSubject(),((PP_Change *)in)->GetType());
					n_bb_list->sort(BB_Predicate);

					if (( cj = ch_ref.find(label)) == ch_ref.end()) {
						list<BBinders*>::iterator i;
						list<BBinders*>::iterator j;
						Mapping *mapping = new Mapping();
						int binder_counter = 0;
						string bb_name;
						mapping->SetIdentityT();
						for(i=n_bb_list->begin();i!=n_bb_list->end();i++) {
							bb_name = BIND_N + Utility::i2s(binder_counter++);
							mapping->InsertMapping((*i)->GetSubject(),bb_name);
							if ( mapping->IsIdentity() && bb_name != (*i)->GetSubject() )
								mapping->SetIdentityF();
						}
						ch_ref[label] = mapping;
					}

					// faccio sostituzioni con nomi intermedi per evitare collisioni
					list<BBinders *>::iterator i;
					for(i=n_bb_list->begin();i!=n_bb_list->end();i++) 
						proc->Renaming((*i)->GetSubject(),TMP_N + Utility::i2s(binder_counter++));
					binder_counter = 0;
					// reimposto i nomi legati dei binder
					for(i=n_bb_list->begin();i!=n_bb_list->end();i++) {
						name = BIND_N + Utility::i2s(binder_counter);
						(*i)->SetSubject(name);
						proc->Renaming(TMP_N + Utility::i2s(binder_counter++),name);
					}
				}
			}
			break;

		case MONO_INTRA:
			{
				label = ((Intra *)c_act.first)->GetAction(&in,&out,&is_sum1,&is_sum2,act_iter);
				if (( i = entity_ref.find(label)) == entity_ref.end()) {
					proc = pproc->GetCopyRed(in,out,&n_in,&n_out);

               //std::cout << "---------" <<std::endl;
               //proc->Print(0);

					n_out->RedProcess(&proc,is_sum2,&name);
					n_in->RedProcess(&proc,is_sum1,&name);
					n_bb_list = this->GetCopyBB();
				}
			}
			break;

		case MONO_EXPOSE:
			{
				double v_rate;
				int binder_counter = 0;
				string name = BIND_N + Utility::i2s((int)bb_list->size());
				label = ((Expose *)c_act.first)->GetAction(&in,&is_sum1);
				if (( i = entity_ref.find(label)) == entity_ref.end()) {
					proc = pproc->GetCopyRed(in,NULL,&n_in,&n_out);
					n_in->RedProcess(&proc,is_sum1,&name);
					n_bb_list = this->GetCopyBB();
					PP_Expose *exp = (PP_Expose *)in;
					if (exp->GetObjectRate() == INF_RATE) v_rate = HUGE_VAL;
					else v_rate = atof(exp->GetObjectRate().c_str());
					n_bb_list->push_back(new BBinders(exp->GetObject(),v_rate,exp->GetType(),STATE_UNHIDDEN));
					n_bb_list->sort(BB_Predicate);

					if (( cj = ch_ref.find(label)) == ch_ref.end()) {
						list<BBinders*>::iterator i;
						list<BBinders*>::iterator j;
						Mapping *mapping = new Mapping();
						int binder_counter = 0;
						string bb_name;
						mapping->SetIdentityT();
						for(i=n_bb_list->begin();i!=n_bb_list->end();i++) {
							bb_name = BIND_N + Utility::i2s(binder_counter++);
							mapping->InsertMapping((*i)->GetSubject(),bb_name);
							if ( mapping->IsIdentity() && bb_name != (*i)->GetSubject() )
								mapping->SetIdentityF();
						}
						ch_ref[label] = mapping;
					}

					// faccio sostituzioni con nomi intermedi per evitare collisioni
					list<BBinders *>::iterator i;
					for(i=n_bb_list->begin();i!=n_bb_list->end();i++) 
						proc->Renaming((*i)->GetSubject(),TMP_N + Utility::i2s(binder_counter++));
					binder_counter = 0;
					// reimposto i nomi legati dei binder
					for(i=n_bb_list->begin();i!=n_bb_list->end();i++) {
						name = BIND_N + Utility::i2s(binder_counter);
						(*i)->SetSubject(name);
						proc->Renaming(TMP_N + Utility::i2s(binder_counter++),name);
					}
				}
			}
			break;

		case MONO_HIDE:
			{
				label = ((Hide *)c_act.first)->GetAction(&in,&is_sum1,act_iter);
				if (( i = entity_ref.find(label)) == entity_ref.end()) {
					proc = pproc->GetCopyRed(in,NULL,&n_in,&n_out);
					n_in->RedProcess(&proc,is_sum1,&name);
					n_bb_list = this->GetCopyBBMode(((PP_Hide *)in)->GetObject(), STATE_HIDDEN);
				}
			}
			break;

		case MONO_UNHIDE:
			{
				label = ((Unhide *)c_act.first)->GetAction(&in,&is_sum1,act_iter);
            //pproc->Print(0);
				if (( i = entity_ref.find(label)) == entity_ref.end()) {
					proc = pproc->GetCopyRed(in,NULL,&n_in,&n_out);
					n_in->RedProcess(&proc,is_sum1,&name);
					n_bb_list = this->GetCopyBBMode(((PP_Unhide *)in)->GetObject(), STATE_UNHIDDEN);
				}
			}
			break;

		case MONO_DIE:
			{
            DecCounter();
				action_label = label;
				return NULL;
			}
			break;
			
		case MONO_EMPTY:
			break;
		}

      DecCounter();
		action_label = label;

		if (i == entity_ref.end()) 
      {
			EliminateNil(&proc);
			Order(&proc);
			new_entity = env->AddEntity(n_bb_list,proc,1,EMPTY_ID);
			entity_ref[label] = new_entity;
			return new_entity;
		}
		else 
      {
			(*i).second->AddCounter(1);
			return (*i).second;
		}

}

//
void Entity::AddCounter(int n) 
{
	if (numeric_limits<int>::max() - counter >= n){
		counter+=n;
	} else {
		counter = numeric_limits<int>::max();
	}
	assert(counter >= 0);
}

//
int Entity::DecCounter(int n) 
{
   int retval = n;
   if (counter >= n)
      counter -= n;
   else
   {
      retval = counter;
      counter = 0;
   }
   assert(counter >= 0);
   return retval;
}

//
double Entity::GetRate(Iterator_Interface *c_iter) 
{
	if ( c_iter != NULL )
	{
		assert(c_iter->GetCategory() == IT_MONO_ACT);
		double c_rate = ((Entity_Iterator *)(c_iter->GetIterator(1)))->GetCurrentAction()->GetRate();
		
		if ( c_iter->GetIterator(2) == NULL )
		{
			c_rate *= counter;
		}
		else
		{
			c_rate *= (*((CR_One_Iterator *)c_iter->GetIterator(2))->GetCurrent())->GetNumber();
		}

		return c_rate;
	}
	else if ( c_act.first != NULL )
	{
		return ( c_act.first->GetRate() );
	}
	else
	{
	return 0.0;
}
}

double Entity::GetBasalRate(Iterator_Interface *c_iter)
{
	if (c_iter->GetIterator(1)->GetCategory() == IT_ENTITY) {
		return ((Entity_Iterator *)(c_iter->GetIterator(1)))->GetCurrentAction()->GetRate();
	} else {
		return HUGE_VAL;
	}
}

Expr* Entity::GetExpr(Iterator_Interface *c_iter){
	if (c_iter->GetIterator(1)->GetCategory() == IT_ENTITY){
		Pos pos(0,0,"");
		string name;
		Expr *expr;
		int mol;
		CR_One_Iterator *cr_one_it = (CR_One_Iterator*)c_iter->GetIterator(2);
		assert(dynamic_cast<Entity_Iterator *>(c_iter->GetIterator(1)));
		double rate = ((Entity_Iterator *)(c_iter->GetIterator(1)))->GetCurrentAction()->GetRate();

		if (cr_one_it != NULL){
			assert(dynamic_cast<CR_One_Iterator *>(c_iter->GetIterator(2)));
			mol = (*(cr_one_it->GetCurrent()))->GetIdN();
		} else {
			mol = id; 
		}

		if (mol > 0){
			name = env->GetST()->GetEntNameFromID(mol);
			expr = new ExprConcentration(pos, env->GetST(), name.substr(2));
		} else {
			name = env->GetST()->GetMolNameFromID(abs(mol));
			expr = new ExprCompConcentration(pos, env->GetST(), name.substr(2));
		}
		return new ExprOp(pos, env->GetST(), expr, new ExprNumber(pos, env->GetST(), rate), EXPR_TIMES);
	} else {
		return NULL;
	}
}

//
double Entity::GetSubjectRate(string name) {
	list<BBinders *>::iterator k;
	for(k=bb_list->begin();k!=bb_list->end();k++)
		if ((*k)->GetSubject() == name) return (*k)->GetRate();
	return -1;
}

//
bool Entity::BBHide(string subject) {
	list<BBinders *>::iterator k;
	for(k=bb_list->begin();k!=bb_list->end();k++)
		if ((*k)->GetSubject() == subject && (*k)->GetState() == STATE_HIDDEN)
			return true;
	return false;
}

//
bool Entity::BBUnhide(string subject) {
	list<BBinders *>::iterator k;
	for(k=bb_list->begin();k!=bb_list->end();k++)
		if ((*k)->GetSubject() == subject && (*k)->GetState() == STATE_UNHIDDEN)
			return true;
	return false;
}

//
bool Entity::BBIs(string subject) {
	list<BBinders *>::iterator k;
	for(k=bb_list->begin();k!=bb_list->end();k++)
		if ((*k)->GetSubject() == subject)
			return true;
	return false;
}


//
bool Entity::IsBound(){
	list<BBinders *>::iterator k;
	for(k=bb_list->begin();k!=bb_list->end();k++)
		if ((*k)->GetState() == STATE_BOUND)
			return true;
	return false;
}

//
BBinders *Entity::GetBB(const string& type) {
	list<BBinders *>::iterator k;
	for(k=bb_list->begin();k!=bb_list->end();k++)
		if ((*k)->GetSubject() == type)
			return (*k);
	return NULL;
}

//
string Entity::GetBBType(const string& subject)
{
	list<BBinders *>::iterator k;
	for(k=bb_list->begin();k!=bb_list->end();k++)
		if ((*k)->GetSubject() == subject)
			return (*k)->GetType();
	return "";
}

//
void Entity::CodifyBB() {
	map<string,Intra *>::iterator i;
	list<BBinders *>::iterator k;
	for(k=bb_list->begin();k!=bb_list->end();k++) {
		if ((*k)->GetState() == STATE_HIDDEN) 
			(*k)->SetHide();
		else 
			(*k)->CalculateVal();
	}
}

//
void Entity::BBAddIn(string subject) {
	list<BBinders *>::iterator k;
	for(k=bb_list->begin();k!=bb_list->end();k++)
		if ((*k)->GetSubject() == subject) {
			(*k)->AddIn(1);
			return;
		}
}

//
void Entity::BBAddOut(string subject, string object) {
	BBinders *tmp = NULL;
	bool control = false;
	list<BBinders *>::iterator k;
	for(k=bb_list->begin();k!=bb_list->end();k++) {
		if ((*k)->GetSubject() == subject) tmp = (*k);
		if ((*k)->GetSubject() == object) control = true;
	}
	if (!control && tmp != NULL)
		tmp->AddOut(1);
}


//
map<string,Tau *> *Entity::GetTauMap() {
	return &tau_map;
}

//
map<string,Change *> *Entity::GetChangeMap() {
	return &change_map;
}

//
map<string,Intra *>	*Entity::GetIntraMap() { 
	return &intra_map; 
}

//
map<string,Expose *> *Entity::GetExposeMap() { 
	return &expose_map; 
}

//
map<string,Hide *> *Entity::GetHideMap() { 
	return &hide_map; 
}

//
map<string,Unhide *> *Entity::GetUnhideMap() { 
	return &unhide_map; 
}

//
map<string,Die *> *Entity::GetDieMap() {
	return &die_map;
}

// 
void Entity::PrintFile(ofstream *fstr) 
{
	if (inf_list.empty())
		(*fstr) << env->GetST()->GetEntNameFromID(id) << endl;
	else (*fstr) << env->GetST()->GetEntNameFromID(id) <<  " [contains immediate actions]" << endl;
	list<BBinders *>::iterator i=bb_list->begin();
	while(i!=bb_list->end()) { 
		(*i++)->PrintFile(fstr);
		if (i!=bb_list->end()) (*fstr) << ",";
	}
	(*fstr) << "[ ";
	pproc->PrintProc(fstr);
	(*fstr) << " ]";
	(*fstr) << endl << endl;
}

//
void Entity::Print() {
	cout << "ENTITY" << " " << id << endl;
	list<BBinders *>::iterator i;
	for(i=bb_list->begin();i!=bb_list->end();i++) 
		(*i)->Print();
	cout << counter << endl;
	
	list<string>::iterator j;
	for(j=fn.begin();j!=fn.end();j++)
		cout << *j << " ";
	cout << endl;
	
	pproc->Print(0);
	cout << endl;

	cout << "TAU" << endl;
	map<string,Tau *>::iterator p;
	for(p=tau_map.begin();p!=tau_map.end();p++) {
		cout << (*p).first << " -> ";
		(*p).second->Print();
		cout << endl;
	}
	cout << "INTRA" << endl;
	map<string,Intra *>::iterator k;
	for(k=intra_map.begin();k!=intra_map.end();k++) {
		cout << (*k).first << " -> ";
		(*k).second->Print();
		cout << endl;
	}
	cout << "EXPOSE" << endl;
	map<string,Expose *>::iterator z;
	for(z=expose_map.begin();z!=expose_map.end();z++) {
		cout << (*z).first << " -> ";
		(*z).second->Print();
		cout << endl;
	}
	cout << "HIDE" << endl;
	map<string,Hide *>::iterator q;
	for(q=hide_map.begin();q!=hide_map.end();q++) {
		cout << (*q).first << " -> ";
		(*q).second->Print();
		cout << endl;
	}
	cout << "UNHIDE" << endl;
	map<string,Unhide *>::iterator w;
	for(w=unhide_map.begin();w!=unhide_map.end();w++) {
		cout << (*w).first << " -> ";
		(*w).second->Print();
		cout << endl;
	}

	cout << "TIMES" << endl;
	list<ElementMONO *>::iterator u;
	for(u=inf_list.begin();u!=inf_list.end();u++)
		(*u)->Print();
}

//
list<BBinders *> *Entity::GetCopyBB() {
	list<BBinders *> *new_bb_list = new list<BBinders *>();
	list<BBinders *>::iterator k;
	for(k=bb_list->begin();k!=bb_list->end();k++)
		new_bb_list->push_back(new BBinders(*(*k)));
	return new_bb_list;
}

//
list<BBinders *> *Entity::DecBB(string subject, int num_in, int num_out) {
	list<BBinders *> *new_bb_list = new list<BBinders *>();
	list<BBinders *>::iterator k;
	for(k=bb_list->begin();k!=bb_list->end();k++)
		if (subject == (*k)->GetSubject()) {
			(*k)->DecIn(num_in);
			(*k)->DecOut(num_out);
			(*k)->CalculateVal();
		}
		return new_bb_list;
}

// 
list<BBinders *> *Entity::GetCopyBBMode(string subject, BinderState state) {
	list<BBinders *> *new_bb_list = new list<BBinders *>();
	list<BBinders *>::iterator k;
	BBinders *current;
	for(k=bb_list->begin();k!=bb_list->end();k++) {
		new_bb_list->push_back((current = new BBinders(*(*k))));
		if (current->GetSubject() == subject)
			current->SetState(state);
	}
	return new_bb_list;
}

// 
list<BBinders *> *Entity::GetCopyBBChange(string subject, const string& type) {
	list<BBinders *> *new_bb_list = new list<BBinders *>();
	list<BBinders *>::iterator k;
	BBinders *current;
	for(k=bb_list->begin();k!=bb_list->end();k++) {
		new_bb_list->push_back((current = new BBinders(*(*k))));
		if (current->GetSubject() == subject)
			current->SetType(type);
	}
	return new_bb_list;
}

//
bool Entity::IsTypeBB(string p_type,const string& p_subject) {
	list<BBinders *>::iterator k;
	for(k=bb_list->begin();k!=bb_list->end();k++)
		if ((*k)->GetType() == p_type && (*k)->GetSubject() != p_subject) return true;
	return false;
}

//
bool Entity::IsTypeBB(string p_type) {
	list<BBinders *>::iterator k;
	for(k=bb_list->begin();k!=bb_list->end();k++)
		if ((*k)->GetType() == p_type) return true;
	return false;
}

//
Element *Entity::GenerateAction(Environment *env, Entity *e1, Entity *e2, BBinders *bb1, BBinders *bb2, int comb,
						ElementType inter, AffRuleBase *aff, double *rate)
{
	FunBase* rateFun = NULL;
	AffRuleComplex *aff1;
	AffRuleExpRate *aff2;
	bool bool_tmp;

	// Get a copy from the map
	SymbolTable* st = this->GetEnv()->GetST();

	switch( aff->GetCategory() )
	{
		case RULE_EXP_RATES:
			aff1 = (AffRuleComplex *)aff;
			
			if (aff1->inter == HUGE_VAL) 
			{
				(*rate) = HUGE_VAL;
			}
			else 
			{
				(*rate) = aff1->inter;
			}

			if ( inter == INTER_BIND )
			{
				return (new Bimolecular_Bind(env,e1,e2,(*rate),bb1,bb2,comb));
			}
			else
			{
				// if e1 is equal to e2 and the binders are different i have to multiply
				// the combination by 2 because 
				if ( (e1 == e2) && (bb1 != bb2) ) 
				{
					comb = comb * 2;	
				}

				rateFun = new FunBim(env,(*rate),e1,e2,comb);
				return (new Bimolecular(env,e1,e2,rateFun,bb1,bb2));
			}
		case RULE_EXP_FUN:
			aff2 = (AffRuleExpRate *)aff;

			rateFun = aff2->rate_function->Clone();
			rateFun->ReplaceEntities(e1, e2);

			bool_tmp = st->AddClonedFunction(rateFun->GetId(),rateFun);
			assert(bool_tmp);

			return (new Bimolecular(env,e1,e2,rateFun,bb1,bb2));		
		case RULE_NORMAL:
			rateFun = new FunBim(env,0,e1,e2,comb);
			return (new Bimolecular(env,e1,e2,rateFun,bb1,bb2));
		case RULE_GAMMA:
			rateFun = new FunBim(env,0,e1,e2,comb);
			return (new Bimolecular(env,e1,e2,rateFun,bb1,bb2));
		case RULE_HYPEXP:
			rateFun = new FunBim(env,0,e1,e2,comb);
			return (new Bimolecular(env,e1,e2,rateFun,bb1,bb2));
		default:
			return NULL;
	}

}

//
pair<Map_Elem *,Map_Elem *> Entity::AmbientAction(Entity *i, BBinders *a, BBinders *b, ElementType inter, AffRuleBase *aff)
{
	pair<Map_Elem *,Map_Elem *> p_action;

	Element* elem1 = NULL; 
	Element* elem2 = NULL;

	int comb, val1, val2;
	double rate = 0.;
	double rate1 = 0.;
	val1 = a->GetVal();
	val2 = b->GetVal();


	if ( (val1==3 && val2==1) || (val1==2 && val2==1) || (val1==2 && val2==3) ) 
	{
		comb = a->GetOut() * b->GetIn();
		if (comb > 0) 
		{
			elem1 = this->GenerateAction(env,this,i,a,b,comb,inter,aff,&rate);
		}
	}
	else if ( (val1==1 && val2==3) || (val1==3 && val2==2) || (val1==1 && val2==2) ) 
	{
		comb = a->GetIn() * b->GetOut();
		if (comb > 0)
		{
			elem1 = this->GenerateAction(env,i,this,b,a,comb,inter,aff,&rate);
		}
	}
	else if (val1==3 && val2==3) 
	{
		comb = a->GetOut() * b->GetIn();
		if ( comb > 0 ) 
		{
			elem1 = this->GenerateAction(env,this,i,a,b,comb,inter,aff,&rate);
		}
		comb = a->GetIn() * b->GetOut();
		if ( comb > 0 ) 
		{
			elem2 = this->GenerateAction(env,i,this,b,a,comb,inter,aff,&rate1);
		}
	}

	Map_Elem* map_elem;

	if (elem1 != NULL) 
	{		
		if (rate == HUGE_VAL)
			env->GetInfActMap()->Insert(this,i, elem1);
		else 
		{
			map_elem = new Map_Elem(elem1);
			InsertAction(env,map_elem,aff,this,i);
			p_action.first = map_elem;
		}
	} 

	if (elem2 != NULL) 
	{
		if (rate1 == HUGE_VAL)
			env->GetInfActMap()->Insert(this,i,elem2);
		else 
		{
			map_elem = new Map_Elem(elem2);	
			InsertAction(env,map_elem,aff,this,i); 
			p_action.second = map_elem;
		}
	}

	return p_action;
}

// 
bool Entity::InsertAction(Environment *env, Map_Elem *element, AffRuleBase *aff,
						  Entity *e1,Entity *e2)
{
	AffRuleExpRate *aff1;

	switch( aff->GetCategory() )
	{
		case RULE_EXP_RATES:
			env->GetActMap()->Insert(e1,e2,element);
			return true;
		case RULE_EXP_FUN:
			env->GetActMap()->Insert(e1,e2,element);
			aff1 = (AffRuleExpRate *)aff;
			env->GetActMap()->AddDependencies(aff1->rate_function->GetStateVarList(),
												aff1->rate_function->GetEntityList(), element);
			return true;
		case RULE_NORMAL:
      case RULE_GAMMA:
      case RULE_HYPEXP:
			env->GetActMap()->InsertGD(e1,e2,element,aff);
			return true;

      default:
			return false;
	}
}

//
void Entity::AddAmbient() 
{
	Map_Elem *map_elem;
	Element *elem = NULL;
	double rate = 0.;
	// aggiungo una voce alla tabella di lookup veloce
	env->GetEntityMap()->ReserveEntry(this);

	// aggiungo l'azione monomoleculare
	map_elem = new Map_Elem(this);
	if (InfListLength() > 0)
		env->GetInfActMap()->Insert(this, this);
	else 
		env->GetActMap()->Insert(this, map_elem);

	// aggiungo le reazioni bimolecolari
	vector<Entity *>::iterator i;
	list<BBinders *>::iterator a;
	list<BBinders *>::iterator b;
	AffRuleBase *aff;
	for(i = env->GetEntityList()->begin(); i != env->GetEntityList()->end(); ++i)
	{
		for(a=bb_list->begin();a!=bb_list->end();a++)
			for(b=(*i)->bb_list->begin();b!=(*i)->bb_list->end();b++) {
				elem = NULL;
				aff = env->GetTypeAffinity()->GetAffinity((*a)->GetType(),(*b)->GetType());
				// If an affinity exists and the binders are unhidden
				if ( aff != NULL && (*a)->GetState() == STATE_UNHIDDEN && (*b)->GetState() == STATE_UNHIDDEN ) {
					// Two different cases
					switch ( aff->GetCategory() ) {
						case RULE_EXP_RATES:
							if ( ((AffRuleComplex*)aff)->bind > 0. ) {
								rate = ((AffRuleComplex*)aff)->bind;
								elem = new Bind(env, this, *i,rate,(*a),(*b), env->GetNextBindNumber());
							}
							else 
							{
								if( ( ((AffRuleComplex*)aff)->inter > 0.0 && ((AffRuleComplex*)aff)->unbind == 0.) )
									AmbientAction(*i,*a,*b,BIM,aff);
							}
							break;
						default:
							AmbientAction(*i,*a,*b,BIM,aff);
							break;
					}
				}
				if (elem != NULL) {
					map_elem = new Map_Elem(elem);
					if (rate == HUGE_VAL)
						env->GetInfActMap()->Insert(this, *i, elem);
					else
						env->GetActMap()->Insert(this, *i, map_elem);                     
				}
			}
	}
}

//
Entity *Entity::Binding(const string& type) 
{
	PP_Node *proc;
	Entity *new_entity;
	list<BBinders *>* n_bb_list = NULL;
	map<string,Entity *>::iterator i;
	string label = ToString(BIND) + type;
	if (( i = entity_ref.find(label)) == entity_ref.end()) {
		proc = pproc->GetCopy();
		n_bb_list = this->GetCopyBBMode(type, STATE_BOUND);
	}

   DecCounter();
	action_label = label;
		
	if (i == entity_ref.end()) 
	{
		EliminateNil(&proc);
		Order(&proc);
		bool is_new = false;
		new_entity = env->AddEntity(n_bb_list, proc, 1, EMPTY_ID, &is_new);
		if (is_new == true)
		{
			//copy the events that are compatible with this n_bb_list
			list<EventReaction*> events = env->GetEventList(this->GetId());
			list<EventReaction*>::iterator it;
			for (it = events.begin(); it != events.end(); ++it)
			{   
            bool isOriginating = false;

            std::vector<Entity*>::iterator e_it;
            for  (e_it = (*it)->OriginatingEntities.begin(); e_it != (*it)->OriginatingEntities.end(); ++e_it)
            {
               if ((**e_it).GetId() == this->GetId())
               {
                  isOriginating = true;
                  break;
               }
            }

            if (isOriginating)
            {
				   if ((*it)->IsInheritable(type))
				   {
					   //copy the event and add it directly
					   EventReaction* new_event = new EventReaction(**it);
					   env->AddEvent(new_event);
				   }
				   else
				   {
					   string message = "the entity '" + env->GetST()->GetEntNameFromID(new_entity->GetId()) + "' " +
						   "cannot inherit event '" + env->GetST()->GetStringVerb((**it).GetEventVerbType()) + "' from '" + env->GetST()->GetEntNameFromID(this->GetId()) + "'";
					   Error_Manager::PrintWarning(3, message);
				   }
            }
			}
		}
		entity_ref[label] = new_entity;
		return new_entity;
	}
	else {
		(*i).second->AddCounter(1);
		return (*i).second;
	}
}

//
Entity *Entity::Unbinding(const string& type) 
{
	PP_Node *proc;
	Entity *new_entity;
	list<BBinders *> *n_bb_list = NULL;
	map<string,Entity *>::iterator i;
	string label = ToString(UNBIND) + type;
	if (( i = entity_ref.find(label)) == entity_ref.end()) {
		proc = pproc->GetCopy();
		n_bb_list = this->GetCopyBBMode(type, STATE_UNHIDDEN);
	}

   DecCounter();  
   action_label = label;
		
	if (i == entity_ref.end()) {
		EliminateNil(&proc);
		Order(&proc);
		new_entity = env->AddEntity(n_bb_list,proc,1,EMPTY_ID);
		entity_ref[label] = new_entity;
		return new_entity;
	}
	else {
		(*i).second->AddCounter(1);
		return (*i).second;
	}
}

//
Entity *Entity::GetAction(string subject, string *name, string mod, Iterator_Interface *c_iter) 
{
	PP_Node *ref,*n_ref,*proc;
	Entity *new_entity;
	list<BBinders *> *bb_list_n = NULL;
	map<string,Entity *>::iterator i;
	string label;
	bool is_sum;
	if (mod == "OUTPUT") {
		label = intra_map[subject]->GetActionOut(bb_list,&ref,&is_sum,c_iter);
	}
	else {
		label = intra_map[subject]->GetActionIn(&ref,&is_sum,c_iter);
		// devo ricordarmi anche il nome ricevuto
		label = label + *name;
	}
	if (( i = entity_ref.find(label)) == entity_ref.end()) {
		proc = pproc->GetCopyRed(ref,NULL,&n_ref,NULL);
		n_ref->RedProcess(&proc,is_sum,name);
		bb_list_n = this->GetCopyBB();
	}
	else if (mod == "OUTPUT") {
		// in questo caso il nome di output devo ricavarlo 
		// direttamente dalla referenza perche' il nuovo pi-processo
		// non viene creato e il nome altrimenti non viene impostato
		*name = ((PP_Output *)ref)->GetObject(); 
	}


   DecCounter();   
   action_label = label;

	if (i == entity_ref.end()) 
   {
		EliminateNil(&proc);
		Order(&proc);
		new_entity = env->AddEntity(bb_list_n,proc,1,EMPTY_ID);
		entity_ref[label] = new_entity;
		return new_entity;
	}
	else {
		(*i).second->AddCounter(1);
		return (*i).second;
	}
}

//
bool Entity::Is(Element *elem) {
	return (elem == this);
}

//
string Entity::GetBoundNames() {
	string res;
	list<BBinders *>::iterator k = bb_list->begin();
	while( k!=bb_list->end()) {
		if ( (*k)->GetState() == STATE_BOUND ) {
			res += (*k++)->GetSubject();
			break;
		}
		else k++;
	}
	while(k!=bb_list->end()) {
		if ( (*k)->GetState() == STATE_BOUND ) 
			res += ";" + (*k)->GetSubject();
		k++;
	}
	return res;	
}

set<string> Entity::GetBoundNamesSet() 
{
	set<string> res;
	list<BBinders *>::iterator k = bb_list->begin();
	while( k!=bb_list->end()) 
	{
		if ( (*k)->GetState() == STATE_BOUND ) 
		{
			res.insert((*k++)->GetSubject());
		}
		k++;
	}
	return res;	
}

//
string Entity::GetKey(Entity *e2, const string& b1, const string& b2) 
{
	string key;
	int cond;

   if ( id < e2->GetId() ) 
      cond = 1;
	else if ( id > e2->GetId() ) 
      cond = 2;
	else //if ( id == e2->GetId() ) 
   {
		if ( b1 < b2 ) 
         cond = 1;
		else 
         cond =2;
	}

	if ( cond == 1 ) 
   {
		key = env->GetST()->GetEntNameFromID(id) + b1 + "," +
			env->GetST()->GetEntNameFromID(e2->GetId()) + b2;
	}
	else 
   {
		key = env->GetST()->GetEntNameFromID(e2->GetId()) + b2 + "," +
			env->GetST()->GetEntNameFromID(id) + b1;
	}	
	return key;
}

std::list<BBinders*>* Entity::GetBindersList()
{
   return bb_list;
}

//
void Entity::ImplodePiProcess()
{
	Implosion(&(this->pproc));
}

//
Iterator_Interface *Entity::GetIterator() 
{
	return ( new Entity_Iterator(GetIntraMapIterator(),GetTauMapIterator(),GetDieMapIterator(),
		GetChangeMapIterator(),GetExposeMapIterator(),GetHideMapIterator(),GetUnhideMapIterator() ) );
}

Iterator_Interface *Entity::GetInfIterator() 
{
	return ( new Entity_Iterator_Inf(inf_list.begin(),inf_list.size()) );
}

Iterator_Interface *Entity::GetIntraMapIterator() 
{
	return ( new Intra_Map_Iterator(intra_map.begin(),intra_map.size()) );
}

Iterator_Interface *Entity::GetIntraIteratorIn(string const& subject) 
{
	map<string,Intra *>::iterator i;
	if ( ( i = intra_map.find(subject) ) != intra_map.end() )
		return (*i).second->GetIteratorIn();
	return NULL;
}

Iterator_Interface *Entity::GetIntraIteratorOut(string const& subject)
{
	map<string,Intra *>::iterator i;
	if ( ( i = intra_map.find(subject) ) != intra_map.end() )
		return (*i).second->GetIteratorOut();
	return NULL;
}

Iterator_Interface *Entity::GetTauMapIterator() 
{
	return ( new Tau_Map_Iterator(tau_map.begin(),tau_map.size()) );
}

Iterator_Interface *Entity::GetDieMapIterator() 
{
	return ( new Die_Map_Iterator(die_map.begin(),die_map.size()) );
}

Iterator_Interface *Entity::GetChangeMapIterator() 
{
	return ( new Change_Map_Iterator(change_map.begin(),change_map.size()) );
}

Iterator_Interface *Entity::GetExposeMapIterator() 
{
	return ( new Expose_Map_Iterator(expose_map.begin(),expose_map.size()) );
}

Iterator_Interface *Entity::GetHideMapIterator() 
{
	return ( new Hide_Map_Iterator(hide_map.begin(),hide_map.size()) );
}

Iterator_Interface *Entity::GetUnhideMapIterator() 
{
	return ( new Unhide_Map_Iterator(unhide_map.begin(),unhide_map.size()) );
}


/////////////////////////////////////////////////////////////////////////////////////////////////////
// ITERATORS METHODS DEFINITION
/////////////////////////////////////////////////////////////////////////////////////////////////////


Entity_Iterator::Entity_Iterator(Iterator_Interface *intra,Iterator_Interface *tau,Iterator_Interface *die,
		Iterator_Interface *change,Iterator_Interface *expose,Iterator_Interface *hide,Iterator_Interface *unhide)
		: Iterator_Interface(IT_ENTITY,7)
{
	this->SetIterator(tau,1);
	this->SetIterator(change,2);
	this->SetIterator(intra,3);
	this->SetIterator(expose,4);
	this->SetIterator(hide,5);
	this->SetIterator(unhide,6);
	this->SetIterator(die,7);
	actual_iterator = 1;
}

Entity_Iterator::~Entity_Iterator()
{}

void Entity_Iterator::IteratorReset()
{
	for ( int i=1; i<8; i++ )
		this->GetIterator(i)->IteratorReset();
	actual_iterator = 1;
	while ( !IteratorIsEnd() && this->GetIterator(actual_iterator)->IteratorIsEnd() )
		actual_iterator++;
}

void Entity_Iterator::IteratorNext() 
{
	bool control;
	if ( !IteratorIsEnd() )
	{
		this->GetIterator(actual_iterator)->IteratorNext();
		control = this->GetIterator(actual_iterator)->IteratorIsEnd();
		if ( this->GetIterator(actual_iterator)->IteratorIsEnd() )
		{
			actual_iterator++;
			while ( !IteratorIsEnd() && this->GetIterator(actual_iterator)->IteratorIsEnd() )
				actual_iterator++;
		}
	}
}

bool Entity_Iterator::IteratorIsEnd()
{ 
	return ( actual_iterator == 8 );
}

ElementMONO *Entity_Iterator::GetCurrentAction()
{
	switch ( GetIterator(actual_iterator)->GetCategory() )
	{
	case IT_ENTITY_INTRA_MAP:
		return (*((Intra_Map_Iterator *)GetIterator(actual_iterator))->GetCurrent()).second;
		break;
	case IT_ENTITY_TAU_MAP:
		return (*((Tau_Map_Iterator *)GetIterator(actual_iterator))->GetCurrent()).second;
		break;
	case IT_ENTITY_DIE_MAP:
		return (*((Die_Map_Iterator *)GetIterator(actual_iterator))->GetCurrent()).second;
		break;
	case IT_ENTITY_CHANGE_MAP:
		return (*((Change_Map_Iterator *)GetIterator(actual_iterator))->GetCurrent()).second;
		break;
	case IT_ENTITY_EXPOSE_MAP:
		return (*((Expose_Map_Iterator *)GetIterator(actual_iterator))->GetCurrent()).second;
		break;
	case IT_ENTITY_HIDE_MAP:
		return (*((Hide_Map_Iterator *)GetIterator(actual_iterator))->GetCurrent()).second;
		break;
	case IT_ENTITY_UNHIDE_MAP:
		return (*((Unhide_Map_Iterator *)GetIterator(actual_iterator))->GetCurrent()).second;
		break;
	default:
		return NULL;
	}
}

Iterator_Interface *Entity_Iterator::GetCurrentIterator()
{
	if ( this->GetIterator(this->actual_iterator)->GetLength() == 0 )
		return NULL;
	return this->GetIterator(this->actual_iterator)->GetIterator(1);
}


Entity_Iterator_Inf::Entity_Iterator_Inf( list<ElementMONO *>::iterator p_iterator_begin, 
		size_t p_iterator_size ) 
   : Iterator<list<ElementMONO *>::iterator>(p_iterator_begin,p_iterator_size,IT_ENTITY_INF,1)
{}

Entity_Iterator_Inf::~Entity_Iterator_Inf() 
{}

void Entity_Iterator_Inf::IteratorReset() 
{
	iterator_current = iterator_begin;
	current_size = 0;
	if ( current_size != size )
	{
		this->SetIterator((*iterator_current)->GetIterator(),1);
	}
}

void Entity_Iterator_Inf::IteratorNext() 
{
	if ( current_size != size )
	{
		if ( this->GetIterator(1) == NULL )
		{
			current_size++;
			iterator_current++;
		}
		else
		{
			this->GetIterator(1)->IteratorNext();
			if ( this->GetIterator(1)->IteratorIsEnd() )
			{
				iterator_current++;
				current_size++;
				if ( current_size != size )
				{
					this->SetIterator((*iterator_current)->GetIterator(),1);
				}
			}
		}
	}
}

Intra_Map_Iterator::Intra_Map_Iterator( map<string,Intra *>::iterator p_iterator_begin, 
		size_t p_iterator_size ) 
   : Iterator<map<string,Intra *>::iterator>(p_iterator_begin,p_iterator_size,IT_ENTITY_INTRA_MAP,1)
{}

Intra_Map_Iterator::~Intra_Map_Iterator() 
{}

void Intra_Map_Iterator::IteratorReset() 
{
	iterator_current = iterator_begin;
	current_size = 0;
	while ( current_size != size && (*iterator_current).second->GetActualRate() == 0 ) 
		//( (*iterator_current).second->GetActualRate() == 0 ||
		//(*iterator_current).second->EvalRate() == HUGE_VAL ) )
	{
		current_size++;
		iterator_current++;
	}
	if ( current_size != size )
	{
		this->SetIterator((*iterator_current).second->GetIterator(),1);
	}
}

void Intra_Map_Iterator::IteratorNext() 
{
	if ( current_size != size )
	{
		this->GetIterator(1)->IteratorNext();
		if (  this->GetIterator(1)->IteratorIsEnd() )
		{
			iterator_current++;
			current_size++;
			while ( current_size != size && (*iterator_current).second->GetActualRate() == 0 ) 
				//( (*iterator_current).second->GetActualRate() == 0 ||
				//(*iterator_current).second->EvalRate() == HUGE_VAL ) )
			{
				current_size++;
				iterator_current++;
			}
			if ( current_size != size )
			{
				this->SetIterator((*iterator_current).second->GetIterator(),1);
			}
		}
	}
}

Tau_Map_Iterator::Tau_Map_Iterator( map<string,Tau *>::iterator p_iterator_begin, 
		size_t p_iterator_size ) 
	: Iterator<map<string,Tau *>::iterator>(p_iterator_begin,p_iterator_size,IT_ENTITY_TAU_MAP,0)
{}

Tau_Map_Iterator::~Tau_Map_Iterator() 
{}

/*void Tau_Map_Iterator::IteratorReset() 
{
	iterator_current = iterator_begin;
	current_size = 0;
	while ( current_size != size && (*iterator_current).second->EvalRate() == HUGE_VAL )
	{
		current_size++;
		iterator_current++;
	}
}

void Tau_Map_Iterator::IteratorNext() 
{
	if ( current_size != size )
	{
		iterator_current++;
		current_size++;
		while ( current_size != size && (*iterator_current).second->EvalRate() == HUGE_VAL )
		{
			current_size++;
			iterator_current++;
		}
	}
}*/

Die_Map_Iterator::Die_Map_Iterator( map<string,Die *>::iterator p_iterator_begin, 
		size_t p_iterator_size ) 
	: Iterator<map<string,Die *>::iterator>(p_iterator_begin,p_iterator_size,IT_ENTITY_DIE_MAP,0)
{}

Die_Map_Iterator::~Die_Map_Iterator() 
{}

/*void Die_Map_Iterator::IteratorReset() 
{
	iterator_current = iterator_begin;
	current_size = 0;
	while ( current_size != size && (*iterator_current).second->EvalRate() == HUGE_VAL )
	{
		current_size++;
		iterator_current++;
	}
}

void Die_Map_Iterator::IteratorNext() 
{
	if ( current_size != size )
	{
		iterator_current++;
		current_size++;
		while ( current_size != size && (*iterator_current).second->EvalRate() == HUGE_VAL )
		{
			current_size++;
			iterator_current++;
		}
	}
}*/

Change_Map_Iterator::Change_Map_Iterator( map<string,Change *>::iterator p_iterator_begin, 
		size_t p_iterator_size ) 
	: Iterator<map<string,Change *>::iterator>(p_iterator_begin,p_iterator_size,IT_ENTITY_CHANGE_MAP,1)
{}

Change_Map_Iterator::~Change_Map_Iterator() 
{}

void Change_Map_Iterator::IteratorReset() 
{
	iterator_current = iterator_begin;
	current_size = 0;
	/*while ( current_size != size && (*iterator_current).second->EvalRate() == HUGE_VAL )
	{
		current_size++;
		iterator_current++;
	}*/
	if ( current_size != size )
	{
		this->SetIterator((*iterator_current).second->GetIterator(),1);
	}
}

void Change_Map_Iterator::IteratorNext() 
{
	if ( current_size != size )
	{
		this->GetIterator(1)->IteratorNext();
		if ( this->GetIterator(1)->IteratorIsEnd() )
		{
			iterator_current++;
			current_size++;
			/*while ( current_size != size && (*iterator_current).second->EvalRate() == HUGE_VAL )
			{
				current_size++;
				iterator_current++;
			}*/
			if ( current_size != size )
			{
				this->SetIterator((*iterator_current).second->GetIterator(),1);
			}
		}
	}
}

Expose_Map_Iterator::Expose_Map_Iterator( map<string,Expose *>::iterator p_iterator_begin, 
		size_t p_iterator_size ) 
	: Iterator<map<string,Expose *>::iterator>(p_iterator_begin,p_iterator_size,IT_ENTITY_EXPOSE_MAP,0)
{}

Expose_Map_Iterator::~Expose_Map_Iterator() 
{}

/*void Expose_Map_Iterator::IteratorReset() 
{
	iterator_current = iterator_begin;
	current_size = 0;
	while ( current_size != size && (*iterator_current).second->EvalRate() == HUGE_VAL )
	{
		current_size++;
		iterator_current++;
	}
}

void Expose_Map_Iterator::IteratorNext() 
{
	if ( current_size != size )
	{
		iterator_current++;
		current_size++;
		while ( current_size != size && (*iterator_current).second->EvalRate() == HUGE_VAL )
		{
			current_size++;
			iterator_current++;
		}
	}
}*/

Hide_Map_Iterator::Hide_Map_Iterator( map<string,Hide *>::iterator p_iterator_begin, 
		size_t p_iterator_size ) 
   : Iterator<map<string,Hide *>::iterator>(p_iterator_begin,p_iterator_size,IT_ENTITY_HIDE_MAP,1)
{}

Hide_Map_Iterator::~Hide_Map_Iterator() 
{}

void Hide_Map_Iterator::IteratorReset() 
{
	iterator_current = iterator_begin;
	current_size = 0;
	/*while ( current_size != size && (*iterator_current).second->EvalRate() == HUGE_VAL )
	{
		current_size++;
		iterator_current++;
	}*/
	if ( current_size != size ) 
	{
		this->SetIterator((*iterator_current).second->GetIterator(),1);
	}
}

void Hide_Map_Iterator::IteratorNext() 
{
	if ( current_size != size )
	{
		this->GetIterator(1)->IteratorNext();
		if ( this->GetIterator(1)->IteratorIsEnd() )
		{
			iterator_current++;
			current_size++;
			/*while ( current_size != size && (*iterator_current).second->EvalRate() == HUGE_VAL )
			{
				current_size++;
				iterator_current++;
			}*/
			if ( current_size != size ) 
			{
				this->SetIterator((*iterator_current).second->GetIterator(),1);
			}
		}
	}
}

Unhide_Map_Iterator::Unhide_Map_Iterator( map<string,Unhide *>::iterator p_iterator_begin, 
		size_t p_iterator_size ) 
   : Iterator<map<string,Unhide *>::iterator>(p_iterator_begin,p_iterator_size,IT_ENTITY_UNHIDE_MAP,1)
{}

Unhide_Map_Iterator::~Unhide_Map_Iterator() 
{}

void Unhide_Map_Iterator::IteratorReset() 
{
	iterator_current = iterator_begin;
	current_size = 0;
	/*while ( current_size != size && (*iterator_current).second->EvalRate() == HUGE_VAL )
	{
		current_size++;
		iterator_current++;
	}*/
	if ( current_size != size )
	{
		this->SetIterator((*iterator_current).second->GetIterator(),1);
	}
}

void Unhide_Map_Iterator::IteratorNext() 
{
	if ( current_size != size )
	{
		this->GetIterator(1)->IteratorNext();
		if ( this->GetIterator(1)->IteratorIsEnd() )
		{
			iterator_current++;
			current_size++;
			/*while ( current_size != size && (*iterator_current).second->EvalRate() == HUGE_VAL )
			{
				current_size++;
				iterator_current++;
			}*/
			if ( current_size != size )
			{
				this->SetIterator((*iterator_current).second->GetIterator(),1);
			}
		}
	}
}

void Entity::GetReactants(vector<Entity *> &reactants){
	reactants.push_back(this);
}

void Entity::GetIterator(pair<Entity *, Entity *> *ent_pair, list<Complex *> *comp, pair<Iterator_Interface *, Iterator_Interface *> *it_pair){
	//An Element of type MONO can't have more then one reactant
	assert(ent_pair->second == NULL);
	it_pair->first = new Mono_Act_Iterator(new CR_One_Iterator(comp->begin(), comp->size(), ent_pair->first->GetId(), env), this->GetIterator());
}

bool Entity::IsOnlyMono(int reactants[2]){
	reactants[0] = this->GetId();
	reactants[1] = -1;

	return true;
}


Iterator_Interface * Entity::GetIterator(Entity *e1, Entity *e2, list<Complex *> *c1, list<Complex *> *c2, TwoIteratorKind iterator_kind){
	assert(c2 == NULL || c2->empty());
	return new Mono_Act_Iterator(new CR_One_Iterator(c1->begin(), c1->size(), e1->GetId(), env), this->GetIterator());
}


Iterator_Interface * Entity::GetInfIterator(Entity *e1, Entity *e2, list<Complex *> *c1, list<Complex *> *c2, TwoIteratorKind iterator_kind){
	assert(c2 == NULL || c2->empty());
	return new Mono_Act_Iterator(new CR_One_Iterator(c1->begin(), c1->size(), e1->GetId(), env), this->GetInfIterator());
}

ChainEntry* Entity::GetEntityChain()
{
   ChainEntry* root = new ChainEntry();
   root->id = this->GetId();
   set<int> addedEntities;
   //addedEntities.insert(root->id);
   this->GetEntityChainInternal(root, addedEntities);
   return root;
}

void Entity::GetEntityChainInternal(ChainEntry* currentEntry, set<int>& addedEntities)
{
   map<string,Entity *>::iterator it;

   for (it = entity_ref.begin(); it != entity_ref.end(); ++it)
   {
      int it_id = it->second->GetId();   
   
      if (addedEntities.find(it_id) == addedEntities.end())
      {
         addedEntities.insert(it_id);

         ChainEntry* child = new ChainEntry();
         child->id = this->GetId();
         currentEntry->childs.push_back(child);
      
         it->second->GetEntityChainInternal(child, addedEntities);
      }
   }
}
