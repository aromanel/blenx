#ifndef ENTITY_H
#define ENTITY_H

#include <cstdlib>
#include <string>
#include <list>
#include <map>
#include <vector>
#include <algorithm>
#include <cmath>
#include <ctime>
#include <cassert>
#include <set>

#ifdef _MSC_VER
#include <hash_map>
using namespace stdext;
#else
#include <ext/hash_map>
#endif

#include "BBinders.h"
#include "Define.h"
#include "Pi_Process.h"
#include "Monomolecular.h"
#include "Bimolecular.h"
#include "Bimolecular_Bind.h"
#include "Type_Affinity.h"
#include "Map_Elem.h"
#include "Mapping.h"

class PP_Node;
class Intra;
class Expose;
class Hide;
class Unhide;
class Tau;
class Change;
class EventReaction;
class Die;
class Environment;

class Intra_Iterator;
class Intra_Iterator_In;
class Intra_Iterator_Out;
class Tau_Iterator;
class Die_Iterator;
class Change_Iterator;
class Hide_Iterator;
class Unhide_Iterator;
class Expose_Iterator;

using namespace std;



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ENTITY ITERATORS CLASSES
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//
class Entity_Iterator : public Iterator_Interface
{
private:
	int actual_iterator;
public:
	Entity_Iterator(Iterator_Interface *intra,Iterator_Interface *tau,Iterator_Interface *die,
		Iterator_Interface *change,Iterator_Interface *expose,Iterator_Interface *hide,Iterator_Interface *unhide);
	~Entity_Iterator();
	void IteratorReset();
	void IteratorNext();
	bool IteratorIsEnd();
	ElementMONO *GetCurrentAction();
	Iterator_Interface *GetCurrentIterator();
};

//
class Entity_Iterator_Inf : public Iterator< list<ElementMONO *>::iterator >
{
public:
	Entity_Iterator_Inf( list<ElementMONO *>::iterator p_iterator_begin, 
		size_t p_iterator_size );
	~Entity_Iterator_Inf();
	void IteratorReset();
	void IteratorNext();
};

//
class Intra_Map_Iterator : public Iterator< map<string,Intra *>::iterator >
{
public:
	Intra_Map_Iterator( map<string,Intra *>::iterator p_iterator_begin, 
		size_t p_iterator_size );
	~Intra_Map_Iterator();
	void IteratorReset();
	void IteratorNext();
};

//
class Tau_Map_Iterator : public Iterator< map<string,Tau *>::iterator >
{
public:
	Tau_Map_Iterator( map<string,Tau *>::iterator p_iterator_begin, 
		size_t p_iterator_size );
	~Tau_Map_Iterator();
	//void IteratorReset();
	//void IteratorNext();
};

//
class Die_Map_Iterator : public Iterator< map<string,Die *>::iterator >
{
public:
	Die_Map_Iterator( map<string,Die *>::iterator p_iterator_begin, 
		size_t p_iterator_size );
	~Die_Map_Iterator();
	//void IteratorReset();
	//void IteratorNext();
};

//
class Change_Map_Iterator : public Iterator< map<string,Change *>::iterator >
{
private:
	Iterator_Interface *change_iterator;
public:
	Change_Map_Iterator( map<string,Change *>::iterator p_iterator_begin, 
		size_t p_iterator_size );
	~Change_Map_Iterator();
	void IteratorReset();
	void IteratorNext();
};

//
class Expose_Map_Iterator : public Iterator< map<string,Expose *>::iterator >
{
public:
	Expose_Map_Iterator( map<string,Expose *>::iterator p_iterator_begin, 
		size_t p_iterator_size );
	~Expose_Map_Iterator();
	//void IteratorReset();
	//void IteratorNext();
};

//
class Hide_Map_Iterator : public Iterator< map<string,Hide *>::iterator >
{
private:
	Iterator_Interface *hide_iterator;
public:
	Hide_Map_Iterator( map<string,Hide *>::iterator p_iterator_begin, 
		size_t p_iterator_size );
	~Hide_Map_Iterator();
	void IteratorReset();
	void IteratorNext();
};

//
class Unhide_Map_Iterator : public Iterator< map<string,Unhide *>::iterator >
{
private:
	Iterator_Interface *unhide_iterator;
public:
	Unhide_Map_Iterator( map<string,Unhide *>::iterator p_iterator_begin, 
		size_t p_iterator_size );
	~Unhide_Map_Iterator();
	void IteratorReset();
	void IteratorNext();
};


class ChainEntry
{
public:
   std::list<ChainEntry*> childs;
   int id;
   std::string groupLabel;

   ~ChainEntry() 
   {
      list<ChainEntry*>::iterator ent_it;
      for (ent_it = childs.begin(); ent_it != childs.end(); ++ent_it)
      {
         delete *ent_it;
      }
   }
};



//////////////////////////////////////////////////////
// ENTITY CLASS
//////////////////////////////////////////////////////

class Entity : public Element {
private:
	int id;
	int counter;
	double sum;
	string action_label;
	int n_actions;
	pair<ElementMONO *,double> c_act;
	list<BBinders *> *bb_list;
	list<string> fn;
	list<ElementMONO *> inf_list;
	PP_Node *pproc;
	map<string,Entity *> entity_ref;
	map<string,Mapping *> ch_ref;

	map<string,Tau *> tau_map;
	map<string,Change *> change_map;
	map<string,Intra *>	intra_map;
	map<string,Expose *> expose_map;
	map<string,Hide *> hide_map;
	map<string,Unhide *> unhide_map;
	map<string,Die *> die_map;

public:
	Entity(Environment* p_env, int p_id,list<BBinders *> *p_bb_list, PP_Node *p_pproc);
	~Entity();

	// questo metodo ritorna il pi-processo
	PP_Node * _GetPProc() {
		return pproc;
	}
    // questo ritorna la lista di binders
	list<BBinders *> *_GetBBList() {
		return bb_list;
	}

	/////////////////////////////////////////////////////////////////////////////////////
	// Methods for implementing the actions iterators of immediate and 
	// non-immediate actions
	
	Iterator_Interface *GetIterator();
	Iterator_Interface *GetInfIterator();
	Iterator_Interface *GetIntraMapIterator();
	Iterator_Interface *GetIntraIteratorIn(string const& subject);
	Iterator_Interface *GetIntraIteratorOut(string const& subject);
	Iterator_Interface *GetTauMapIterator();
	Iterator_Interface *GetDieMapIterator();
	Iterator_Interface *GetChangeMapIterator();
	Iterator_Interface *GetExposeMapIterator();
	Iterator_Interface *GetHideMapIterator();
	Iterator_Interface *GetUnhideMapIterator();

	/////////////////////////////////////////////////////////////////////////////////////

	void PE();
	//void IncCounter() { counter++; }
    void DecCounter() { 
		assert(counter > 0); 
		counter--; 
	}
    int DecCounter(int n);
	void EvalActions();
	void Rate();
	void AddCounter(int n);
	void SetCounter(int n) { counter = n; }
	void Print();
	void PrintFile(ofstream *fstr);
	void AddAmbient();
	void CodifyBB();
	void BBAddIn(string subject);
	void BBAddOut(string subject, string object);

	pair<Map_Elem *,Map_Elem *> AmbientAction(Entity *i, BBinders *a, BBinders *b, 
		ElementType inter, AffRuleBase *aff);
	Element *GenerateAction(Environment *env, Entity *e1, Entity *e2, BBinders *bb1, BBinders *bb2, int comb,
						ElementType inter, AffRuleBase *aff,double *rate);
	bool InsertAction(Environment *env, Map_Elem *element, AffRuleBase *aff,
						  Entity *e1,Entity *e2);

	int GetId() { return id; }
	int GetCounter() { return counter; }
	string GetActionLabel() { return action_label; }     
	int InfListLength() { return (int)inf_list.size(); }
	bool InfListEmpty() { return inf_list.empty(); }

	double GetSubjectRate(string name);
	double GetRate(Iterator_Interface *c_iter);
	double GetBasalRate(Iterator_Interface *c_iter);
	Expr* GetExpr(Iterator_Interface *c_iter);
	double Time();
	double GetActualRate();
	bool Is(Element *elem);
	MonoType GetCategory() { 
		if ( c_act.first != NULL )
			return c_act.first->GetCategory();
		return MONO_EMPTY;
	}
	// ritorno il puntatore per velocizzare
	Mapping* GetActualMapping() {
		map<string,Mapping *>::iterator cj;
		if (( cj = ch_ref.find(this->action_label)) != ch_ref.end()) 
			return ((*cj).second);
		 return NULL;
	}

	bool IsTypeBB(string p_type,const string& p_subject);
	bool IsTypeBB(string p_type);
	bool Equal(list<BBinders *> *bb, PP_Node *p);
	bool BBHide(string subject);
	bool BBUnhide(string subject);
	bool BBIs(string subject);
	bool IsBound();
	BBinders *GetBB(const string& subject);
	string GetBBType(const string& subject);
	
	Entity *RedMono(Iterator_Interface *c_iter);
	Entity *GetAction(string subject, string *name, string mod, Iterator_Interface *c_iter);
	Entity *Binding(const string& type);
	Entity *Unbinding(const string& type);
	
	map<string,Tau *> *GetTauMap();
	map<string,Change *> *GetChangeMap();
	map<string,Intra *>	*GetIntraMap();
	map<string,Expose *> *GetExposeMap();
	map<string,Hide *> *GetHideMap();
	map<string,Unhide *> *GetUnhideMap();
	map<string,Die *> *GetDieMap();

	list<BBinders *> *GetBindersList();
	list<BBinders *> *GetCopyBB();
	list<BBinders *> *DecBB(string subject, int num_in, int num_out);
	list<BBinders *> *GetCopyBBMode(string subject, BinderState state);
	list<BBinders *> *GetCopyBBChange(string subject, const string& type);
	string GetBoundNames();
	set<string> GetBoundNamesSet();
	string GetKey(Entity *e2, const string& b1, const string& b2);

	double GetTotal() { return ((double)( GetCounter() * n_actions )); }	
	const list<string>& GetFn() const { return fn; }

	void ImplodePiProcess();
	void GetReactants(vector<Entity *> &reactants);

	void GetIterator(pair<Entity *, Entity *> *ent_pair, list<Complex *> *comp, pair<Iterator_Interface *, Iterator_Interface *> *it_pair);
	Iterator_Interface * GetIterator(Entity *e1, Entity *e2, list<Complex *> *c1, list<Complex *> *c2, TwoIteratorKind iterator_kind);
	Iterator_Interface * GetInfIterator(Entity *e1, Entity *e2, list<Complex *> *c1, list<Complex *> *c2, TwoIteratorKind iterator_kind);
	bool IsOnlyMono(int reactants[2]);

   ChainEntry* GetEntityChain();
   void GetEntityChainInternal(ChainEntry* currentEntry, set<int>& addedEntities);
};

bool BB_Predicate(BBinders *left,BBinders *right);

#endif

