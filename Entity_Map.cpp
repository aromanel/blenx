
#include <cassert>
#include "Define.h"
#include "Entity.h"
#include "Entity_Map.h"

using namespace std;

void EntityMap::AddEntity(Entity* key, Map_Elem* elem)
{
   int e_id = key->GetId();
   
   assert (e_id < (int)h_vector.size());

   h_vector[e_id]->push_back(elem);
}

void EntityMap::ReserveEntity(Entity* key)
{
   int e_id = key->GetId();
   if ((int)h_vector.size() <= e_id)
   {
      size_t oldsize = h_vector.size();
      h_vector.resize(e_id + 1);
      for (size_t i = oldsize; i < h_vector.size(); ++i)
         h_vector[i] = new list<Map_Elem*>();
   }
}
   
void EntityMap::ReserveEntity(const vector<Entity*>& keys)
{
   vector<Entity*>::const_iterator it;   
   for (it = keys.begin(); it != keys.end(); ++it)
   {
      ReserveEntity(*it);
   }
}
