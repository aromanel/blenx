#ifndef ENTITY_MAP_H_INCLUDED
#define ENTITY_MAP_H_INCLUDED

#include <vector>
#include <list>
#include "Map_Elem.h"
#include "Entity.h"

//The entity map is a sort of hash_map: from an Entity identifier (ID)
//Obtains all elements in a ActMap that contains the identifier (as a source or
//target of a bimolecular action, or a mono action)

//Helper structure for the list of actions; a pointer to the correct map_elem
//and the simulation step (to not update twice)


class EntityMap 
{
private:
   std::vector<std::list<Map_Elem*>*> h_vector;
public:
   
   //Helper to get a list of elements that have an index like i
   inline list<Map_Elem*>* GetNodeList(int i) { return h_vector[i]; }
   void AddEntity(Entity* key, Map_Elem* elem);
   void ReserveEntity(Entity* key);
   void ReserveEntity(const vector<Entity*>& keys);
};

#endif //ENTITY_MAP_H_INCLUDED
