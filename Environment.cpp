#include <iostream>
#include <ctime>
#include <cassert>
#include <limits>

#include <set>

#include "Environment.h"
#include "Pi_Process.h"
#include "Symbol_Table.h"
#include "DependencyMap.h"
#include "Complex_Map.h"
#include "Complex_Rel.h"
#include "Type_Affinity.h"

//The various Elements for action execution
#include "Event.h"
#include "Unbind.h"
#include "Bind.h"
#include "Define.h"

using namespace std;


/////////////////////////////////////////////////////////////////
// PREDICATES
/////////////////////////////////////////////////////////////////


//
bool Entity_Predicate(Entity *left, Entity *right) {
	return (left->GetId() < right->GetId());
}

//
bool PQ_Predicate(PQ_Elem *left, PQ_Elem *right) {
	return (left->GetTime() < right->GetTime());
}


/////////////////////////////////////////////////////////////////
// CLASS UIBIND_MAP METHODS DEFINITION
/////////////////////////////////////////////////////////////////

//
UIBind_elem *UIBind_map::Find(Entity *e1, const string& b1, Entity *e2, const string& b2) {
	string key;
	hash_map<string,UIBind_elem *>::iterator i;
	key = e1->GetKey(e2,b1,b2);
	i = ui_map.find(key);
	if ( i == ui_map.end() ) return NULL;
	return (*i).second;
}

//
void UIBind_map::DecBindElem(const string& key, vector<Map_Elem *> *e_list) 
{
	UIBind_elem *ui_elem;
	if ( ( ui_elem = Find(key) ) != NULL ) 
	{
		//cout << "DEC: " << key << endl;
		ui_elem->Dec();
		env->GetDependencies(ui_elem, *e_list);
	}
	else
		assert(false);
}

//
void UIBind_map::AddCounter(const string& key, int number) {
	UIBind_elem *ui_elem;
	if ( ( ui_elem = Find(key) ) != NULL )
		ui_elem->counter += number;
}

//
void UIBind_map::IncBindElem(Entity *e1, Entity *e2, const string& b1, const string& b2, const string& key, 
		vector<Map_Elem *> *e_list) 
{
	Map_Elem *map_elem = NULL;
	Element *elem = NULL;
	AffRuleComplex *aff;
	BBinders *a,*b;
	pair<Map_Elem *,Map_Elem *> p_action;
	UIBind_elem *ui_elem;
	hash_map<string,UIBind_elem *>::iterator i;
	if ( ( ui_elem = Find(key) ) != NULL ) {
		ui_elem->Inc();
	}
	else {
		a = e1->GetBB(b1);
		b = e2->GetBB(b2);
		// Sono sicuro che l'affinita' e' di quel tipo
		aff = (AffRuleComplex *)env->GetTypeAffinity()->GetAffinity(a->GetType(),b->GetType());
		p_action = e1->AmbientAction(e2,a,b,INTER_BIND,aff);
		if ( aff->unbind > 0. ) {
			elem = new Unbind(env, e1,e2,aff->unbind,a,b);
		}

		if (elem != NULL) {
			map_elem = new Map_Elem(elem);
			if (aff->unbind == HUGE_VAL)
				env->GetInfActMap()->Insert(e1,e2,elem);
			else
				env->GetActMap()->Insert(e1,e2,map_elem);  
		}

		ui_elem = new UIBind_elem(map_elem,p_action.first,p_action.second);
		ui_map[key] = ui_elem;
	}
	if (e_list != NULL)
		env->GetDependencies(ui_elem, *e_list);
}

//
UIBind_elem *UIBind_map::Find(const string& key) {
	hash_map<string,UIBind_elem *>::iterator i;
	i = ui_map.find(key);
	if ( i == ui_map.end() ) return NULL;
	return (*i).second;
}

//
void UIBind_map::Insert(string const& key,UIBind_elem *elem) {
	ui_map[key] = elem;
}

//
void UIBind_map::Print() {
	hash_map<string,UIBind_elem *>::iterator i;
	for( i = ui_map.begin() ; i!= ui_map.end() ; i++ ) {
		cout << (*i).first << " - " << (*i).second->counter << endl;
	}
	cout << endl << endl << endl;
}

//
void UIBind_map::GetSignature(string& signature)
{
	string elem;
	signature.clear();
	hash_map<string,UIBind_elem *>::iterator index = ui_map.begin();
	while(  index != ui_map.end() )
	{	
		if ( (*index).second->counter > 0 )
		{
			elem = (*index).first + "*" + Utility::i2s((*index).second->counter) + " ";
			signature.append(elem);
		}
		index++;
	}
}

//
void UIBind_map::SetSignature(std::vector<string>::const_iterator& tokens_iter,
		const std::vector<string>& tokens) 
{
	string key;
	int cardinality;
	string::size_type position;


	hash_map<string,UIBind_elem *>::iterator index = ui_map.begin();
	while( index != ui_map.end() )
	{
		(*index++).second->counter = 0;
	}

	while ( tokens_iter != tokens.end() )
	{
		position = (*tokens_iter).find("*",0);
		key = (*tokens_iter).substr(0,position).c_str();
		cardinality = atoi( (*tokens_iter).substr(position+1).c_str() );

		// This condition always hold
		assert( ui_map.find(key) != ui_map.end() );

		ui_map[key]->counter = cardinality;

		tokens_iter++;
	}

}


/////////////////////////////////////////////////////////////////
// CLASS INFACTMAP METHODS DEFINITION
/////////////////////////////////////////////////////////////////

//
InfActMap::InfActMap() 
{}

//
InfActMap::~InfActMap() {
}

void InfActMap::InitializeAmbientInf()
{
	ambient_inf.first.clear();
	ambient_inf.second = 0;
}

void InfActMap::GenerateAmbientInf()
{
	list<Element *>::iterator i;
	for(i=act_map.begin();i!=act_map.end();i++) 
		if ((*i)->Time() == -1.0) {
			ambient_inf.first.push_front(*i);
			ambient_inf.second += (*i)->GetTotal();
		}
}

//
void InfActMap::Insert(const vector<Entity*>& keys, Element *elem) 
{
	//TODO: with update
	act_map.push_back(elem);
}

//
void InfActMap::Insert(Entity* key1, Entity* key2, Element *elem) 
{
	//TODO: with update
	act_map.push_back(elem);
}

void InfActMap::Insert(Entity* key, Element *elem) 
{
	//TODO: with update
	act_map.push_back(elem);
}

//
void InfActMap::Update(list<Entity *> *modify) {
	list<Element *>::iterator k;
	list<Entity *>::iterator i;
	for(i=modify->begin();i!=modify->end();i++) 
		for(k=act_map.begin();k!=act_map.end();k++)
			if ((*k)->Is(*i))
				(*k)->Rate();
}

// returns true if it performed a selection
bool InfActMap::SS(Action& action, Environment* env) 
{
	//double tmp_time = 0.;   

	GenerateAmbientInf();

	if (!ambient_inf.first.empty()) 
	{
		double value;
		double rn = env->Random0Upto1();
		double rnn = ambient_inf.second * rn;
		list<Element *>::iterator k = ambient_inf.first.begin();
		while(k!=ambient_inf.first.end()) 
		{
			action.first = *k; 
			action.second = 0.;
			value =  (*k)->GetTotal();
			if (rnn <= value)
				break;
			rnn -= value;
			k++;
		}
		return true;
	}
	return false;
}

//
void InfActMap::Print() {
	list<Element *>::iterator i;
	for(i=act_map.begin();i!=act_map.end();i++) 
		(*i)->Print();
}

//
Iterator_Interface *InfActMap::GetIterator(Environment *env)
{
	return ( new InfActMap_Iterator(ambient_inf.first.begin(),ambient_inf.first.size(),env ) );
}



/////////////////////////////////////////////////////////////////
// CLASS ACTMAP METHODS DEFINITION
/////////////////////////////////////////////////////////////////

//
ActMap::ActMap(DependencyMap<Entity*>& helper_ent_map, DependencyMap<StateVar*>& helper_var_map) 
: my_ent_map(helper_ent_map) , my_var_map(helper_var_map)
{
	my_gd_manager = new GDManager();
}

//
ActMap::~ActMap() 
{
	list<Map_Elem *>::iterator act_it;
	for (act_it = act_map.begin(); act_it != act_map.end(); ++act_it)
		delete (*act_it);

	hash_map<double, StateVarList*>::iterator var_it;
	for(var_it = delta_to_vars_map.begin(); var_it != delta_to_vars_map.end(); ++var_it)   
		delete (var_it->second);   
}

void ActMap::Insert(StateVar* var)
{
	my_var_map.ReserveEntry(var);

	if (var->GetType() == STATEVAR_CONTINUOUS)
	{
		// Find if there is already an element in the ambient
		// with same delta. In that case, add it
		StateVarList* list;
		double delta = var->GetDeltaT();

		hash_map<double, StateVarList*>::iterator m_it;
		m_it = delta_to_vars_map.find(delta);

		if (m_it == delta_to_vars_map.end())
		{
			list = new StateVarList();
			delta_to_vars_map[delta] = list;
			DeltaVars dv(list, delta);

			std::deque<DeltaVars>::iterator d_it;
			for(d_it = delta_list.begin(); d_it != delta_list.end(); ++d_it)
			{
				if (d_it->next_firing > dv.next_firing)
					break;
			}
			delta_list.insert(d_it, dv);
		}
		else
		{
			list = m_it->second;
		}

		list->Insert(var);
	}
}

void ActMap::Insert(const vector<Entity*>& keys, Map_Elem *elem) 
{
	//add the keys
	vector<Entity*>::const_iterator it;
	for (it = keys.begin(); it != keys.end(); ++it)
	{
		my_ent_map.AddEntry(*it, elem);
	}

	//add the element pointed by the keys to the map & priority queue
	act_map.push_back(elem);
	PQ_Elem pq_elem(elem, HUGE_VAL);
	my_pq.Insert(pq_elem);
}

void ActMap::Insert(Entity* key1, Entity* key2, Map_Elem *elem) 
{
	act_map.push_back(elem);
	my_ent_map.AddEntry(key1, elem);
	my_ent_map.AddEntry(key2, elem);

	PQ_Elem pq_elem(elem, HUGE_VAL);
	my_pq.Insert(pq_elem);
}

void ActMap::InsertGD(Entity* key1, Entity* key2, Map_Elem *elem, AffRuleBase *aff) 
{
	act_map.push_back(elem);
	my_ent_map.AddEntry(key1, elem);
	my_ent_map.AddEntry(key2, elem);

	GDBase* new_gd_elem = NULL;
	AffRuleNormal *aff1;
	AffRuleGamma *aff2;
	AffRuleHypExp *aff3;
	
	switch( aff->GetCategory() )
	{
		case RULE_NORMAL:
			aff1 = (AffRuleNormal *)aff;
			new_gd_elem = new GDNormal(aff1->mean,aff1->variance,elem,this->my_gd_manager);
			break;
		case RULE_GAMMA:
			aff2 = (AffRuleGamma *)aff;
			new_gd_elem = new GDGamma(aff2->scale,aff2->shape,elem,this->my_gd_manager);
			break;
		case RULE_HYPEXP:
			aff3 = (AffRuleHypExp *)aff;
			new_gd_elem = new GDHypExp(aff3->parameters,elem,this->my_gd_manager);
			break;
      default:
         assert(false);
         break;
	}

	elem->SetGDElem(new_gd_elem);
	this->my_gd_manager->AddElement(new_gd_elem);
}

void ActMap::InsertGD(const vector<Entity*>& keys, Map_Elem *elem, GDBase *new_gd_elem) 
{
	//add the keys
	act_map.push_back(elem);
	vector<Entity*>::const_iterator it;
	for (it = keys.begin(); it != keys.end(); ++it)
	{
		my_ent_map.AddEntry(*it, elem);
	}

	elem->SetGDElem(new_gd_elem);
	this->my_gd_manager->AddElement(new_gd_elem);
}

void ActMap::Insert(Entity* key, Map_Elem *elem) 
{
	act_map.push_back(elem);
	my_ent_map.AddEntry(key, elem);

	PQ_Elem pq_elem(elem, HUGE_VAL);
	my_pq.Insert(pq_elem);
}

void ActMap::AddDependencies(const vector<StateVar*>& statevar_keys,
		const vector<Entity*>& entity_keys, 
		Map_Elem *elem) 
{
	AddDependencies(statevar_keys, elem);
	AddDependencies(entity_keys, elem);		
}

void ActMap::AddDependencies(const vector<StateVar*>& statevar_keys,                               
		Map_Elem *elem) 
{
	//add the keys
	vector<StateVar*>::const_iterator v_it;
	for (v_it = statevar_keys.begin(); v_it != statevar_keys.end(); ++v_it)
	{
		my_var_map.AddEntry(*v_it, elem);
	}
}

void ActMap::AddDependencies(const vector<Entity*>& entity_keys, 
		Map_Elem *elem) 
{
	//add the keys
	vector<Entity*>::const_iterator e_it;
	for (e_it = entity_keys.begin(); e_it != entity_keys.end(); ++e_it)
	{
		my_ent_map.AddEntry(*e_it, elem);
	}	
}

bool ActMap::SameElements(const vector<Map_Elem*>& elements, const std::vector<Entity*>& modify)
{

	list<Entity *> mod(modify.begin(), modify.end());
	// ordino le entita' per ID
	mod.sort(Entity_Predicate);
	// elimino i doppioni
	mod.unique(); 

	std::set<Map_Elem*> elements2;
	list<Map_Elem *>::iterator k;
	list<Entity *>::const_iterator it;
	for(it = mod.begin(); it != mod.end(); it++) 
	{
		for(k = act_map.begin(); k != act_map.end(); k++)
			if ((*k)->action->Is(*it))
				elements2.insert((*k)); //->action
	}

	int end = (int)elements.size();
	assert(end == (int)elements2.size());

	for(int i = 0; i < end; ++i) 
	{
		assert(elements2.find(elements[i]) != elements2.end());
		if (elements2.find(elements[i]) == elements2.end())
			return false;
	}
	return true;
}

bool ActMap::Update(const std::vector<Map_Elem *>& elements, 
		unsigned long current_step, double current_time, Action &action) 
{
	//elements.reserve(elem_list.size());

	/*
		elements.resize(0);
		std::vector<Map_Elem *>::const_iterator j;
		for( j = elem_list.begin() ; j != elem_list.end() ; j++ ) 
		{
	// avoid duplication
	if ((*j)->simulation_step != current_step)
	{
	(*j)->simulation_step != current_step
	elements.push_back(*j);
	}
	}
	*/

	//assert(SameElements(elements, modify));

	vector< std::pair<Map_Elem*, double> > new_values(elements.size());
	list<Map_Elem*> gd_elements;
	
	int start = 0;
	int end = (int)elements.size();

#ifdef PARALLEL_WIN32
	int middle = end / 2;

	//parallel code
	EntityThread thread1(elements, new_values, start, middle);
	EntityThread thread2(elements, new_values, middle, end);

	EntityWorker::thread1 = &thread1;
	EntityWorker::thread2 = &thread2;   

	EntityWorker::Run(); 
#else
	//serial code
	bool sort = false;
	for(int i = start; i < end; ++i) 
	{
		assert (elements[i]->simulation_step == current_step);
		if ( elements[i]->s_type == GENERAL )
		{
			new_values[i].first = NULL;
			if ( action.first == elements[i]->action )
			{
				elements[i]->gd_index->UpdateWithExecution(current_time);
			}
			else
			{
				elements[i]->gd_index->Update(current_time);
			}
			sort = true;
		}
		else
		{
			new_values[i].first = elements[i];
			double new_time = elements[i]->action->Time();
			if (new_time < 0.0)
			{
				Error_Manager::PrintError(0, "this element generated a negative time. Check your functions.");
				elements[i]->action->Print();
				return false;
			}

			new_values[i].second = new_time + current_time;
		}
	}   
#endif //PARALLEL

	for(int i = 0; i < end; ++i) 
	{
		if ( new_values[i].first != NULL )
		{
			my_pq.Adjust(new_values[i].first->pq_index, new_values[i].second);
		}
	}
	//assert(my_pq.CheckSanity());

	// Update of GD actions if any
	if (sort)
	{
		this->my_gd_manager->Sort();
	}

	return true;
}

//
void ActMap::Init() {
	list<Map_Elem *>::iterator i;
	double n_time;
	for(i=act_map.begin();i!=act_map.end();i++)
	{
		if ( (*i)->s_type != GENERAL )
		{
			n_time = (*i)->action->Time();
			my_pq.Adjust((*i)->pq_index, n_time);
		}
		else
		{
			this->my_gd_manager->Init();
		}
	}
}

//
void ActMap::Print() {
	/*
		list<Map_Elem *>::iterator i;
		for(i=act_map.begin();i!=act_map.end();i++) 
		(*i)->action->Print();*/
	my_pq.Print();
}

//
Iterator_Interface *ActMap::GetIterator(Environment *env)
{	
	return ( new ActMap_Iterator( act_map.begin(), act_map.size(), env ) );
}


/////////////////////////////////////////////////////////////////
// SIMPLE_ITERATOR DEFINITION 
/////////////////////////////////////////////////////////////////

Simple_Iterator::Simple_Iterator(Iterator_Interface * first_it, Iterator_Interface * second_it) :
	Iterator_Interface(IT_SIMPLE, 2){
		this->SetIterator(first_it, 1);
		this->SetIterator(second_it, 2);
}


void Simple_Iterator::IteratorReset(){
	if (this->GetIterator(1) != NULL){
		this->GetIterator(1)->IteratorReset();
	}
	if (this->GetIterator(2) != NULL){
		this->GetIterator(2)->IteratorReset();
	}
}

void Simple_Iterator::IteratorNext(){
	if (this->GetIterator(1) != NULL && this->GetIterator(2) != NULL) {
		this->GetIterator(2)->IteratorNext();
		if (this->GetIterator(2)->IteratorIsEnd()){
			this->GetIterator(2)->IteratorReset();
			this->GetIterator(1)->IteratorNext();
		}
	} else if (this->GetIterator(1) != NULL) {
		this->GetIterator(1)->IteratorNext();
	} else if (this->GetIterator(2) != NULL) {
		this->GetIterator(2)->IteratorNext();
	}
}

bool Simple_Iterator::IteratorIsEnd(){
	if (this->GetIterator(1) != NULL && this->GetIterator(2) != NULL){
		return (this->GetIterator(1)->IteratorIsEnd() || this->GetIterator(2)->IteratorIsEnd());
	} else if (this->GetIterator(1) != NULL) {
		return (this->GetIterator(1)->IteratorIsEnd());
	} else if (this->GetIterator(2) != NULL) {
		return (this->GetIterator(2)->IteratorIsEnd());
	}
	return true;
}



/////////////////////////////////////////////////////////////////
// ENVIRONMENT FUNCTIONS
/////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////
// Private Functions
//

// Assumo che la firma sia ordinata con prima i valori delle
// entita' e poi dei complessi, anch'essi ordinati per ID.
// Quindi non faccio il controllo sul tipo e il settaggio 
// scorre al massimo una volta entrmbe le liste.
// Ritorna true se esistono entita' con azioni immediate, 
// false altrimenti

bool Environment::SetSignature(const TS_Signature& signature) 
{
	assert( (entity_list.size() + complex_list.size() + statevar_list.size() + 1 ) >= signature.elements.size() );
	std::vector<TS_sig_elem>::const_iterator i = signature.elements.begin();
	std::vector<Entity *>::iterator entity_iter = entity_list.begin();
	std::list<StateVar *>::iterator statevar_iter = statevar_list.begin();

	bool inf_act_entities = false;

	while( entity_iter != entity_list.end() )
	{
		if ( i != signature.elements.end() && (*i).id == (*entity_iter)->GetId() )
		{
			if ( !(*entity_iter)->InfListEmpty() ) 
				inf_act_entities = true;
			(*entity_iter++)->SetCounter((*i++).cardinality);
		}
		else 
		{
			//Imposto a zero
			(*entity_iter++)->SetCounter(0);
		}
	}

	std::vector<Complex *>::iterator complex_iter = complex_list.begin();
	while( complex_iter != complex_list.end() )
	{
		if ( i !=signature.elements.end() && (*i).id == (*complex_iter)->GetIdN() )
		{
			(*complex_iter++)->SetNumber((*i++).cardinality);
		}
		else 
		{
			// Imposto a zero
			(*complex_iter++)->SetNumber(0);
		}
	}

	// Questo e' il delimitatore
	i++;

	while ( statevar_iter != statevar_list.end() )
	{
		(*statevar_iter++)->SetValue((*i++).value);
	}

	return inf_act_entities;
}

//
TS_Signature *Environment::GetSignature() 
{
	TS_Signature *signature = new TS_Signature();
	std::vector<Entity *>::iterator entity_iter;
	std::vector<Complex *>::iterator complex_iter;
	std::list<StateVar *>::iterator statevar_iter;
	
	// Definisco il delimitatore
	TS_sig_elem delimiter;
	delimiter.id = 0;
	delimiter.cardinality = 0;
	
	for( entity_iter = entity_list.begin() ; entity_iter != entity_list.end() ; entity_iter++ )
	{
		if ( (*entity_iter)->GetCounter() > 0 )
		{
			TS_sig_elem elem;
			elem.id = (*entity_iter)->GetId();
			elem.cardinality = (*entity_iter)->GetCounter();
			signature->elements.push_back(elem);
		}
	}

	for( complex_iter = complex_list.begin() ; complex_iter != complex_list.end() ; complex_iter++ )
	{
		if ( (*complex_iter)->GetNumber() > 0 )
		{
			TS_sig_elem elem;
			elem.id = (*complex_iter)->GetIdN();
			elem.cardinality = (*complex_iter)->GetNumber();
			signature->elements.push_back(elem);
		}
	}
	
	// Inserisco delimitatore
	signature->elements.push_back(delimiter);
	int counter = 1;

	for ( statevar_iter = statevar_list.begin() ; statevar_iter != statevar_list.end() ; statevar_iter++ )
	{
		TS_sig_elem elem;
		elem.id = counter++;
		elem.value = (float) (*statevar_iter)->ReadValue();
		signature->elements.push_back(elem);
	}

	return signature;
}


////////////////////////////////////////////////////////////////
// Output functions
//

void Environment::Print() {
	cout << "RATES:" << endl;
	map<string,double>::iterator i;
	cout << rate_map.size() << endl;
	for(i=rate_map.begin(); i!=rate_map.end(); i++)
		cout << (*i).first << " " << (*i).second << endl;
	cout << endl << "BETA PROCESSES" << endl;
	vector<Entity *>::iterator k;
	for(k=entity_list.begin(); k!=entity_list.end(); k++) {
		cout << endl;	
		(*k)->Print();
	}
	/*cout << endl << "AMBIENT" << endl;
	list<Bimolecular *>::iterator j;
	for(j=bim_list.begin(); j!=bim_list.end(); j++) {
		(*j)->Print();
		cout << "\n";
	}*/
}

Entity* Environment::GetEntityFromId(int id)
{
   vector<Entity *>::iterator i;
	for(i=entity_list.begin();i!=entity_list.end();i++)    
   {
      if ((**i).GetId() == id)
         return (*i);
   }
   return NULL;
}

Complex* Environment::GetComplexFromId(int id)
{
   vector<Complex *>::iterator j;
	for( j = complex_list.begin(); j != complex_list.end(); j++) 
   {
      if ((**j).GetId() == id)
         return (*j);
   }
   return NULL;
}

string Environment::ReactionKeyToString(reaction_key key, const map<int, string>& entityGroups, const map<int, string>& complexGroups)
{
	std::stringstream reaction;
	string res;
	int reactant;

   ReactionType react = (ReactionType)key.data[4];
   bool immediate = false;

	for( int i = 0; i < 2; i++ ){
		if ( key.data[i] != 0 ) 
		{
			if ( key.data[i] > 0 ) 
         {
            map<int, string>::const_iterator mit;
            mit = entityGroups.find(key.data[i]);

            std::string label = "";
            if (mit != entityGroups.end())
               label = mit->second + ":";

            reaction << label << symbol_table->GetEntNameFromID(key.data[i]) << " ";
            Entity* ent = GetEntityFromId(key.data[i]);
            if (!ent->InfListEmpty())
               immediate = true;
         }
			else
         {
            map<int, string>::const_iterator mit;
            mit = complexGroups.find(std::abs(key.data[i]));

            std::string label = "";
            if (mit != complexGroups.end())
               label = mit->second + ":";

            reaction << label << symbol_table->GetMolNameFromID(std::abs(key.data[i])) << " ";
            Complex* c = GetComplexFromId(std::abs(key.data[i]));
            if (!c->ContainsInfiniteActions())
               immediate = true;
         }
		}
	}

   if ((react == R_MONO ||
      react == R_DELETE || 
      react == R_UNBIND || 
      react == R_DELETE) && immediate)
   {
	   reaction << " --(I " << ToString(react) << ")-> ";
   }
   else
   {
	   reaction << " --(" << ToString(react) << ")-> ";
   }

	bool hasRight = false;

	for( int i = 2; i < 4; i++ )
	{
		if ( key.data[i] != 0 ) 
		{
			hasRight = true;
			if ( key.data[i] > 0 ) 
         {
            map<int, string>::const_iterator mit;
            mit = entityGroups.find(key.data[i]);

            std::string label = "";
            if (mit != entityGroups.end())
               label = mit->second + ":";

            reaction << label << symbol_table->GetEntNameFromID(key.data[i]) << " ";
         }
			else
         {
            map<int, string>::const_iterator mit;
            mit = complexGroups.find(std::abs(key.data[i]));

            std::string label = "";
            if (mit != complexGroups.end())
               label = mit->second + ":";

            reaction << label << symbol_table->GetMolNameFromID(std::abs(key.data[i])) << " ";
         }
		}
	}

	if (!hasRight) {
		switch(key.data[4]){
			case R_NEW:
				reactant = key.data[0] + key.data[1];
				if ( reactant > 0 ) 
					reaction <<  symbol_table->GetEntNameFromID(reactant) << " ";
				else
					reaction << symbol_table->GetMolNameFromID(std::abs(reactant)) << " ";
				break;
			default:
				reaction << "Nil";
				break;
		}
	}

	return reaction.str();
}

string Environment::ReactionKeyToString(reaction_key key)
{
	std::stringstream reaction;
	string res;
	int reactant;
	for( int i = 0; i < 2; i++ ){
		if ( key.data[i] != 0 ) 
		{
			if ( key.data[i] > 0 ) 
				reaction << symbol_table->GetEntNameFromID(key.data[i]) << " ";
			else
				reaction << symbol_table->GetMolNameFromID(std::abs(key.data[i])) << " ";
		}
	}

	reaction << " --(" << ToString((ReactionType)key.data[4]) << ")-> ";

	bool hasRight = false;

	for( int i = 2; i < 4; i++ )
	{
		if ( key.data[i] != 0 ) 
		{
			hasRight = true;
			if ( key.data[i] > 0 ) 
				reaction <<  symbol_table->GetEntNameFromID(key.data[i]) << " ";
			else
				reaction << symbol_table->GetMolNameFromID(std::abs(key.data[i])) << " ";
		}
	}

	if (!hasRight) {
		switch(key.data[4]){
			case R_NEW:
				reactant = key.data[0] + key.data[1];
				if ( reactant > 0 ) 
					reaction <<  symbol_table->GetEntNameFromID(reactant) << " ";
				else
					reaction << symbol_table->GetMolNameFromID(std::abs(reactant)) << " ";
				break;
			default:
				reaction << "Nil";
				break;
		}
	}

	return reaction.str();
}

bool Environment::IsEvent(ReactionType rt){
	return (rt == R_SPLIT ||
			rt == R_NEW ||
			rt == R_DELETE ||
			rt == R_JOIN ||
			rt == R_UPDATE);
}

void Environment::PrintFile2Helper(ofstream *fstr, ChainEntry* chain, int& indent, int lastLevel, std::string& label)
{
   (*fstr) << symbol_table->GetEntNameFromID(chain->id);
  
   list<ChainEntry*>::iterator ent_it;
   ent_it = chain->childs.begin();
   
   bool loop = (ent_it != chain->childs.end());
   while(loop)
   {
      (*fstr) << " - ";
      PrintFile2Helper(fstr, *ent_it, indent, lastLevel, label);
      ++indent;
      ++ent_it;

      if (ent_it != chain->childs.end())
      {
         (*fstr) << std::endl;
         ++lastLevel;
         string currentLabel = label + "." + Utility::i2s(lastLevel) + ": ";
         (*fstr) << currentLabel;
         
         for (int i = 0; i < indent; ++i)
            (*fstr) << " X ";
      }
      else
         loop = false;
   }
}

bool ExpandRight(int currentNode, std::list<int>& currentChain, const set<int>& nodesCycle, const map<int, set<int> >& successors)
{
   if (nodesCycle.find(currentNode) != nodesCycle.end())
   {
      currentChain.push_back(currentNode);
      return true;
   }
   else
   {
      // not a cycle node, this test should always fail but...
      assert((find(currentChain.begin(), currentChain.end(), currentNode) == currentChain.end()));
      
      currentChain.push_back(currentNode);

      map<int, set<int> >::const_iterator map_it = successors.find(currentNode);
      if (map_it != successors.end())
      {
         set<int>::const_iterator ent_it = map_it->second.begin();         
         if (ent_it != successors.find(currentNode)->second.end())
         {
            //we expand at most one, separate brenches will be treated as different chains
            ExpandRight(*ent_it, currentChain, nodesCycle, successors);
         }
      }      
      return false;
   }
}

bool ExpandLeft(int currentNode, std::list<int>& currentChain, const set<int>& usedNodes, const map<int, set<int> >& predecessors)
{
   if (usedNodes.find(currentNode) != usedNodes.end()) 
   {
      // already in cycle or already expanded right
      currentChain.push_front(currentNode);
      return true;
   }
   else if (find(currentChain.begin(), currentChain.end(), currentNode) != currentChain.end())
   {
      // or already expanded right
      return true;
   }
   else
   {
      map<int, set<int> >::const_iterator map_it = predecessors.find(currentNode);
      if (map_it != predecessors.end())
      {
         set<int>::const_iterator ent_it = map_it->second.begin();         
         if (ent_it != predecessors.find(currentNode)->second.end())
         {
            //we expand at most one, separate brenches will be treated as different chains
            ExpandLeft(*ent_it, currentChain, usedNodes, predecessors);
         }      
      }
      return false;
   }
}

void FindChains(list< list<int> >& cycles, list< list<int> >& chains, map<int, set<int> >& successors, map<int, set<int> >& predecessors, set<int>& allNodes)
{
   // discard nodes that are already in cycles
   set<int> nodesCycles;
   list< list<int> >::iterator lit;
   list<int>::iterator it;

   for (lit = cycles.begin(); lit != cycles.end(); ++lit)
   {      
      for (it = lit->begin(); it != lit->end(); ++it)
      {
         nodesCycles.insert(*it);
      }
   } 

   //loop over all nodes
   set<int>::iterator all_it;
   set<int> freeNodes;
   set_difference(allNodes.begin(), allNodes.end(), nodesCycles.begin(), nodesCycles.end(),inserter(freeNodes, freeNodes.begin()));


   for (all_it = freeNodes.begin(); all_it != freeNodes.end(); ++all_it)
   {
      int currentNode = *all_it;
      set<int>::iterator pred_it;
      set<int>::iterator succ_it;

      // skip already chained nodes.
      // If they are the end or start of another chain, 
      // this chain will start from a single node and they will be included by
      // ExpandLeft or ExpandRight
      if (nodesCycles.find(currentNode) == nodesCycles.end())
      {
         pred_it = predecessors[currentNode].begin();
         for (succ_it = successors[currentNode].begin(); succ_it != successors[currentNode].end(); ++succ_it)
         {
            if (*succ_it != currentNode)
            {
               list<int> currentChain;
               currentChain.push_back(currentNode);

               ExpandRight(*succ_it, currentChain, nodesCycles, successors);
               if (pred_it != predecessors[currentNode].end())
               {
                  // if we can expand left, do it
                  ExpandLeft(*pred_it, currentChain, nodesCycles, predecessors);
                  ++pred_it;
               }

               //add each of these chains
               if (currentChain.size() > 1)
               {
                  chains.push_back(currentChain);
                  // add all to the usedNoded list
                  for (it = currentChain.begin(); it != currentChain.end(); ++it)
                  {
                     nodesCycles.insert(*it);
                  }
               }
            }
         }

         // any left expansion left?
         for (; pred_it != predecessors[currentNode].end(); ++pred_it)
         {
            if (*pred_it != currentNode)
            {
               list<int> currentChain;
               currentChain.push_back(currentNode);

               //right expansions are no more possible, but what about a left one?
               ExpandLeft(*pred_it, currentChain, nodesCycles, predecessors);

               if (currentChain.size() > 1)
               {
                  chains.push_back(currentChain);
                  // add all to the usedNoded list
                  for (it = currentChain.begin(); it != currentChain.end(); ++it)
                  {
                     nodesCycles.insert(*it);
                  }
               }
            }
         }
      }      
   }
}

bool FindCycles(int startNode, int currentNode, map<int, set<int> >& successors, std::list<int>& currentCycle, list< list<int> >& cycles)
{
   bool newNode = (find(currentCycle.begin(), currentCycle.end(), currentNode) == currentCycle.end());
   currentCycle.push_back(currentNode);

   if (currentNode == startNode)
   {   
      cycles.push_back(currentCycle);
      return true;
   }  
   else 
   {
      if (newNode)
      {
         // not already dealt with 
        
         // Iterate through its child, in pre-order
         set<int>::iterator ent_it = successors[currentNode].begin();
         
         bool loop = (ent_it != successors[currentNode].end());
         while(loop)
         {  
            FindCycles(startNode, *ent_it, successors, currentCycle, cycles);
            currentCycle.pop_back();

            ++ent_it;
            loop = (ent_it != successors[currentNode].end());
         }
      }
   }
   return false;
}

//void Environment::PrintFile2HelperE(ofstream *fstr, int start, map<int, set<int> >& successors, std::list<int>& label, map<int, std::string>& groups)
//{
//   // Print current ID
//   (*fstr) << symbol_table->GetEntNameFromID(start);
//
//   if (groups.find(start) == groups.end())
//   {
//      //not already dealt with
//
//
//      // Assign a label to it
//      string currentLabel;
//      std::list<int>::iterator lbl_it;
//
//      bool first = true;
//      for (lbl_it = label.begin(); lbl_it != label.end(); ++lbl_it)
//      {
//         if (first)
//         {
//            currentLabel = Utility::i2s(*lbl_it);
//            first = false;
//         }
//         else
//         {
//            currentLabel = currentLabel + "." + Utility::i2s(*lbl_it);
//         }
//      }
//
//      //if (groups.find(start) == groups.end())
//      groups[start] = currentLabel;
//      currentLabel = currentLabel + ": ";
//     
//      // Iterate through its child, in pre-order
//      set<int>::iterator ent_it = successors[start].begin();
//      
//      int n = 1;
//      bool loop = (ent_it != successors[start].end());
//      while(loop)
//      {  
//         //first one is "free", other add indentation
//         if (n > 1)
//         {
//            int currentN = label.back();
//            label.pop_back();
//            label.push_back(currentN + 1);
//            label.push_back(1);
//
//            // print this "subsection" header
//            string currentLabel;
//            std::list<int>::iterator lbl_it;
//
//            first = true;
//            for (lbl_it = label.begin(); lbl_it != label.end(); ++lbl_it)
//            {
//               if (first)
//               {
//                  currentLabel = Utility::i2s(*lbl_it);
//                  first = false;
//               }
//               else
//               {
//                  currentLabel = currentLabel + "." + Utility::i2s(*lbl_it);
//               }
//            }
//            currentLabel = currentLabel + ": ";
//            (*fstr) << currentLabel;
//
//
//            (*fstr) << symbol_table->GetEntNameFromID(start);
//         }
//
//         (*fstr) << " - ";
//         PrintFile2HelperE(fstr, *ent_it, successors, label, groups);
//
//         ++ent_it;
//
//         // If it's not the last entry in the list, we need to print
//         // a little "header" for the continuation
//         if (ent_it != successors[start].end())
//         {
//            (*fstr) << std::endl;  
//            ++n;         
//         }
//         else
//         {
//            loop = false;
//            if (n > 1)
//               label.pop_back();
//         }
//      }
//   }
//}

void Environment::PrintFile2HelperC(ofstream* fstr, int start, map<int, set<int> >& successors, std::list<int>& label, map<int, std::string>& groups)
{
   (*fstr) << symbol_table->GetMolNameFromID(start);

   if (groups.find(start) == groups.end())
   {
      //not already dealt with

      // Assign a label to it
      string currentLabel;
      std::list<int>::iterator lbl_it;

      bool first = true;
      for (lbl_it = label.begin(); lbl_it != label.end(); ++lbl_it)
      {
         if (first)
         {
            currentLabel = Utility::i2s(*lbl_it);
            first = false;
         }
         else
         {
            currentLabel = currentLabel + "." + Utility::i2s(*lbl_it);
         }
      }

      groups[start] = currentLabel;
      currentLabel = currentLabel + ": ";


      set<int>::iterator c_it = successors[start].begin();
   
      int n = 1;
      bool loop = (c_it != successors[start].end());
      while(loop)
      {
         //first one is "free", other add indentation
         if (n > 1)
         {
            int currentN = label.back();
            label.pop_back();
            label.push_back(currentN + 1);
            label.push_back(1);

            // print this "subsection" header
            string currentLabel;
            std::list<int>::iterator lbl_it;

            first = true;
            for (lbl_it = label.begin(); lbl_it != label.end(); ++lbl_it)
            {
               if (first)
               {
                  currentLabel = Utility::i2s(*lbl_it);
                  first = false;
               }
               else
               {
                  currentLabel = currentLabel + "." + Utility::i2s(*lbl_it);
               }
            }
            currentLabel = currentLabel + ": ";
            (*fstr) << currentLabel;


            (*fstr) << symbol_table->GetMolNameFromID(start);
         }

         (*fstr) << " - ";
         PrintFile2HelperC(fstr, *c_it, successors, label, groups);

         ++c_it;

         if (c_it != successors[start].end())
         {
            // new subsection, new line
            (*fstr) << std::endl;
            ++n;
         }
         else
         {
            loop = false;
            if (n > 1)
               label.pop_back();
         }
      }
   }
}

void RotateCycles(list< list<int> >& cycles)
{
   list< list<int> >::iterator lit;
   for (lit = cycles.begin(); lit != cycles.end(); ++lit)
   {
      // delete last elem in the chain (duplicated)
      lit->pop_back();

      list<int>::iterator it = lit->begin();
      //find the min
      list<int>::iterator min_it = it;
      int min_pos = 0;
      ++it;
      int i = 1;
      for (; it != lit->end(); ++it)
      {
         if (*it < *min_it)
         {
            min_it = it;
            min_pos = i;
         }
         ++i;
      }

      for (int k = 0; k < min_pos; ++k)
      {
         int current = lit->front();
         lit->pop_front();
         lit->push_back(current);
      }
   }
}

bool IsEqual(const list<int>& l1, const list<int>& l2)
{
   if (l1.size() != l2.size())
      return false;

   list<int>::const_iterator l1it = l1.begin();
   list<int>::const_iterator l2it = l2.begin();

   for (size_t i = 0; i < l1.size(); ++i)
   {
      if (*l1it != *l2it)
         return false;

      ++l1it;
      ++l2it;
   }

   return true;
}

bool IsLess(const list<int>& l1, const list<int>& l2)
{
   size_t i = 0;
   list<int>::const_iterator l1it = l1.begin();
   list<int>::const_iterator l2it = l2.begin();

   if (l1.size() < l2.size())
      return true;
   else if (l1.size() > l2.size())
      return false;
   else
   {
      while (i < l1.size())
      {
         if (*l1it < *l2it)
            return true;
         else if (*l1it > *l2it)
            return false;
         ++l1it;
         ++l2it;
         ++i;
      }
      return false;
   }
}

void EliminateDuplicates(list< list<int> >& cycles)
{
   cycles.sort(IsLess);
   cycles.unique(IsEqual);
}

void LabelChains(const std::list< list<int> >& chains, int startNum, std::list<std::string>& chainLabels, map<int, std::string>& groups)
{
   // loop over all chains
   std::list< list<int> >::const_iterator lit;
   for (lit = chains.begin(); lit != chains.end(); ++lit)
   {
      // loop over this chain
      list<int>::const_iterator it;
      std::string currentLabel;
      for (it = lit->begin(); it != lit->end(); ++it)
      {
         map<int, std::string>::iterator git;
         git = groups.find(*it);
         if (git != groups.end())
         {
            currentLabel = git->second;
            break;
         }
      }

      //we have a number, or nothing at all.
      if (currentLabel.length() == 0)
      {
         ++startNum;
         currentLabel = Utility::i2s(startNum);
      }

      // assing it to the whole chain
      chainLabels.push_back(currentLabel);
      for (it = lit->begin(); it != lit->end(); ++it)
      {
         map<int, std::string>::iterator git;
         git = groups.find(*it);
         if (git == groups.end())
         {
            groups[*it] = currentLabel;
            break;
         }
      }
   }
}

// Print the spec2 files, for the new plotter
void Environment::PrintFile2(ofstream *fstr)
{
   map<int, std::string> entityGroups;
   map<int, std::string> complexGroups; 

   //
   // our book-keeping structures
   map<int, set<int> > complexSucc;
   map<int, set<int> > entitySucc;

   map<int, set<int> > entityPred;
   map<int, set<int> > complexPred;

   set<int> allEntities;
   set<int> allComplexes;

   // first, find successors, predecessors and node names for both complexes and entities
   ReactionMap::iterator react_it;
	for(react_it = reaction_map.begin(); react_it !=reaction_map.end(); react_it++)
   {
      int r1 = (*react_it).first.data[0];
      int p1 = (*react_it).first.data[2];

      if (r1 < 0 && p1 < 0)
      {
         complexSucc[-r1].insert(-p1);
         complexPred[-p1].insert(-r1);
      }
      else if (r1 > 0 && p1 > 0)
      {
         entitySucc[r1].insert(p1);
         entityPred[p1].insert(r1);
      }

      int r2 = (*react_it).first.data[1];
      int p2 = (*react_it).first.data[3];

      if (r2 < 0 && p2 < 0)
      {
         complexSucc[-r2].insert(-p2);
         complexPred[-p2].insert(-r2);
      }
      else if (r2 == 0 && r1 < 0 && p2 < 0)
      {
         complexSucc[-r1].insert(-p2);
         complexPred[-p2].insert(-r1);
      }
      else if (r2 > 0 && p2 > 0)
      {
         entitySucc[r2].insert(p2);
         entityPred[p2].insert(r2);
      }

      for (int i = 0; i < 4; ++i)
      {
         if ((*react_it).first.data[i] > 0)
            allEntities.insert((*react_it).first.data[i]);
         else if ((*react_it).first.data[i] < 0)
            allComplexes.insert(-(*react_it).first.data[i]);
      }
   }

   // go through the map and print out
   map<int, set<int> >::iterator succ_it;   
   list< list<int> >::iterator lit;

   //for (succ_it = entitySucc.begin(); succ_it != entitySucc.end(); ++succ_it)
   //{
   //   if (entityGroups.find((*succ_it).first) == entityGroups.end())
   //   {
   //      std::list<int> label;
   //      label.push_back(n);
   //      label.push_back(1);
   //      (*fstr) << Utility::i2s(n) << ".1: ";

   //      PrintFile2HelperE(fstr, (*succ_it).first, entitySucc, label, entityGroups);
   //      (*fstr) << std::endl;

   //      label.pop_back();
   //      label.pop_back();
   //      ++n;
   //   }
   //}

   list< list<int> > entityCycles;
   list< list<int> > entityChains;

   list< list<int> > complexCycles;
   list< list<int> > complexChains;

   // 1) find cycles
   // First, for entities
   for (succ_it = entitySucc.begin(); succ_it != entitySucc.end(); ++succ_it)
   {
      set<int>::iterator c_it;
      for (c_it = entitySucc[succ_it->first].begin(); c_it != entitySucc[succ_it->first].end(); ++c_it)
      {
         list<int> currentCycle;
         currentCycle.push_back(succ_it->first);
         FindCycles(succ_it->first, *c_it, entitySucc, currentCycle, entityCycles);
      }
   }
   RotateCycles(entityCycles);
   EliminateDuplicates(entityCycles);

   (*fstr) << "# CYCLES" << std::endl;    
   
   // print entity cycles
   for (lit = entityCycles.begin(); lit != entityCycles.end(); ++lit)
   {
      //(*fstr) << *nit <<  ": ";

      list<int>::iterator it;
      for (it = lit->begin(); it != lit->end(); ++it)
      {
         (*fstr) << symbol_table->GetEntNameFromID(*it) << " - ";
      }
      (*fstr) << symbol_table->GetEntNameFromID(lit->front()) << std::endl;    

      //++nit;
   }
   (*fstr) << std::endl;    

   // Then, for molecules
   for (succ_it = complexSucc.begin(); succ_it != complexSucc.end(); ++succ_it)
   {
      set<int>::iterator c_it;
      for (c_it = complexSucc[succ_it->first].begin(); c_it != complexSucc[succ_it->first].end(); ++c_it)
      {
         list<int> currentCycle;
         currentCycle.push_back(succ_it->first);
         FindCycles(succ_it->first, *c_it, complexSucc, currentCycle, complexCycles);
      }
   }
   RotateCycles(complexCycles);
   EliminateDuplicates(complexCycles);

   // print complex cycles
   for (lit = complexCycles.begin(); lit != complexCycles.end(); ++lit)
   {
      list<int>::iterator it;
      for (it = lit->begin(); it != lit->end(); ++it)
      {
         (*fstr) << symbol_table->GetMolNameFromID(*it) << " - ";
      }
      (*fstr) << symbol_table->GetMolNameFromID(lit->front()) << std::endl;    
   }
   (*fstr) << std::endl;    

   //2) find and print chains
   FindChains(entityCycles, entityChains, entitySucc, entityPred, allEntities);

   (*fstr) << "# CHAINS" << std::endl;    

   //print entity chains
   for (lit = entityChains.begin(); lit != entityChains.end(); ++lit)
   {
      list<int>::iterator it;
      for (it = lit->begin(); it != lit->end(); ++it)
      {
         (*fstr) << symbol_table->GetEntNameFromID(*it) << " - ";
      }
      (*fstr) << std::endl;    
   }
   (*fstr) << std::endl;

   FindChains(complexCycles, complexChains, complexSucc, complexPred, allComplexes);

   //print complex chains
   for (lit = complexChains.begin(); lit != complexChains.end(); ++lit)
   {
      list<int>::iterator it;
      for (it = lit->begin(); it != lit->end(); ++it)
      {
         (*fstr) << symbol_table->GetMolNameFromID(*it) << " - ";
      }
      (*fstr) << std::endl;    
   }
   (*fstr) << std::endl;


 //  for (succ_it = complexSucc.begin(); succ_it != complexSucc.end(); ++succ_it)
 //  {
 //     if (complexGroups.find((*succ_it).first) == complexGroups.end())
 //     {
 //        std::list<int> label;
 //        label.push_back(n);
 //        label.push_back(1);
 //        (*fstr) << Utility::i2s(n) << ".1: ";

 //        PrintFile2HelperC(fstr, (*succ_it).first, complexSucc, label, complexGroups);
 //        (*fstr) << std::endl;

 //        label.pop_back();
 //        label.pop_back();
 //        ++n;
 //     }
 //  }
 //  (*fstr) << std::endl;    

 //  

   // 3) rpint reactions
   (*fstr) << "# REACTIONS" << std::endl;    
   ReactionMap::iterator i;
	for(i = reaction_map.begin(); i != reaction_map.end(); i++)
      (*fstr) << ReactionKeyToString((*i).first, entityGroups, complexGroups) << " [" << (*i).second << "]" << endl;
}

//
void Environment::PrintFile(ofstream *fstr) {
	(*fstr) << "REACTIONS" << endl << endl;
	//if (!this->FLAG_COMPLEXES_ELIMINATION)
	//{
		ReactionMap::iterator i;
		for(i=reaction_map.begin();i!=reaction_map.end();i++)
			(*fstr) << ReactionKeyToString((*i).first) << " [" << (*i).second << "]" << endl << endl;
	//}

	vector<Entity *>::iterator k;
	(*fstr) << endl << endl << "ENTITIES" << endl << endl;
	for(k=entity_list.begin(); k!=entity_list.end(); k++)
	{
		// First piprocess Implosion
		(*k)->ImplodePiProcess();
		(*k)->PrintFile(fstr);
	}

	(*fstr) << endl << endl << "COMPLEXES" << endl << endl;
	vector<Complex *>::iterator j;
	for( j = complex_list.begin(); j != complex_list.end(); j++) {
		if (!this->FLAG_COMPLEXES_ELIMINATION)
		{
			(*j)->Print(fstr);
			(*fstr) << endl << endl;
		}
		else if ( (*j)->GetNumber() > 0 )
		{
			(*j)->Print(fstr);
			(*fstr) << endl << endl;
		}
	}

	if (!this->FLAG_COMPLEXES_ELIMINATION)
	{
		(*fstr) << endl << endl << "VARIABLES" << endl << endl;
		list<StateVar *>::iterator l;
		for( l = statevar_list.begin(); l != statevar_list.end(); l++) {
			(*fstr) << this->GetST()->GetVarNameFromID((*l)->GetId());
			(*fstr) << endl << endl;
		}
	}
}

//
void Environment::PrintState(ofstream *fstr, double time) {
	vector<Entity *>::iterator i;
	(*fstr) << time << "\t";
	for(i=entity_list.begin();i!=entity_list.end();i++)
		(*fstr) << Utility::i2s((*i)->GetCounter()) << "\t";
	(*fstr) << endl;
}

void Environment::PrintState(FILE* file_e, FILE* file_c, FILE* file_v, set<int> &observed_complexes, double time) {

	// stampa sul file delle entita'
	vector<Entity *>::iterator i;
	fprintf(file_e, "%.10e\t", time);
	for(i = entity_list.begin(); i!=entity_list.end(); ++i)
	{
		fprintf(file_e, "%d\t", (*i)->GetCounter());
	}
	fprintf(file_e, "\n");

	// stampo sul file dei complessi
	if (!complex_list.empty()) {
		fprintf(file_c, "%.10e\t", time);
		vector<Complex *>::iterator j;
		for( j = complex_list.begin(); j != complex_list.end(); j++) {
			if (!this->FLAG_COMPLEXES_ELIMINATION)
			{
				fprintf(file_c, "%d\t", (*j)->GetNumber());
			}
			else if ((*j)->GetNumber() > 0)
			{
				fprintf(file_c, "%d(%s)\t", (*j)->GetNumber(), (this->GetST()->GetMolNameFromID((*j)->GetId())).c_str());	
				observed_complexes.insert((*j)->GetId());
			}
		}
		fprintf(file_c, "\n");
	}

	// print variables values
	if (!statevar_list.empty()) {
		fprintf(file_v, "%.10e\t", time);
		list<StateVar *>::iterator k;
		for( k = statevar_list.begin(); k != statevar_list.end(); k++) {
			fprintf(file_v, "%e\t", (*k)->ReadValue());
		}
		fprintf(file_v, "\n");
	}

}

//
void Environment::PrintAndDeleteComplexes(FILE* file_tmp, set<int> &observed_complexes)
{
	int id;
	Complex *tmp;
	vector<string> skey;
	string key;
	bool go;
	// check all the complexes and delete/print the ones with countdown at zero
	if (!complex_list.empty()) {
		vector<Complex *>::iterator j;
		std::list<Complex *> *c_list;
		std::list<Complex *>::iterator c_list_it;
		j = complex_list.begin();
		while(j != complex_list.end()) {
			tmp = *j;
			if (tmp->IsToEliminate())
			{
				j = complex_list.erase(j);
				tmp->GetComplex()->InitIT();
				while( ( id = tmp->GetComplex()->GetITK() ) != 0 )
				{
					c_list = this->comp_rel->GetComplexesList(id);
					c_list_it = c_list->begin();
					go = true;
					if (c_list_it == c_list->end())
						go = false;
					while(go)
					{
						if ( (*c_list_it) == tmp )
						{
							c_list_it = c_list->erase(c_list_it);
							this->comp_rel->DeleteKey(id);
							go = false;
						}
						else
						{
							c_list_it++;
							if (c_list_it == c_list->end())
								go = false;
						}
					}
				}

				if (!this->FLAG_COMPLEXES_SIGNATURE)
				{
					skey.clear();
					key.clear();
					tmp->GetComplex()->Key(&skey);
					Utility::VKey(skey,&key);

					c_list = this->comp_map->GetComplexesList(key);
					c_list_it = c_list->begin();
					go = true;
					if (c_list_it == c_list->end())
						go = false;
					while(go)
					{
						if ( (*c_list_it) == tmp )
						{
							c_list_it = c_list->erase(c_list_it);
							this->comp_map->DeleteKey(key);
							go = false;
						}
						else
						{
							c_list_it++;
							if (c_list_it == c_list->end())
								go = false;
						}
					}
				}

				tmp->GetComplex()->CleanPrint();
				set<int>::iterator set_it;
				if ( ( set_it = observed_complexes.find(tmp->GetId()) ) != observed_complexes.end() )
				{
					cout << this->GetST()->GetMolNameFromID(*set_it) << " ";
					tmp->Print(file_tmp);
					fprintf(file_tmp,"\n\n");
					observed_complexes.erase(set_it);
				}
				delete(tmp);
				if (complex_list.empty())
					break;
			}
			else
			{
				tmp->DecCountdown();
				j++;
			}
		}
	}
}

//
void Environment::PrintFinalComplexesStructure() 
{
	vector<Complex *>::iterator j;
	int key;
	hash_map<int,int> distribution;
	hash_map<int,int>::iterator i_dis; 
	int counter = 1;
	for( j = complex_list.begin(); j != complex_list.end(); j++) {
		if ( (*j)->GetNumber() > 0 )
		{
			(*j)->PrintDOT(counter++);
			key = (*j)->GetComplex()->GetNodes();
			if ( ( i_dis = distribution.find(key) ) != distribution.end() )
				(*i_dis).second += (*j)->GetNumber() ;
			else
				distribution[key] = (*j)->GetNumber();
		}
	}

	/*string fileName = "CompDis.txt";
	ofstream *fstr_complex = new ofstream(fileName.c_str(),ios::out);

	for ( i_dis = distribution.begin() ; i_dis != distribution.end() ; i_dis++ )
		 (*fstr_complex) << (*i_dis).first << "\t" << (*i_dis).second << endl; 

	(*fstr_complex).close();*/
}

////////////////////////////////////////////////////////////////
// Action dependencies from variables and entities
//

void Environment::GetDependencies(Entity* from_entity, std::vector<Map_Elem*>& to_elements)
{
	list<Map_Elem*>* mod_elem_list = ent_map.GetNodeList(from_entity->GetId());
	list<Map_Elem*>::iterator elem_it;
	for (elem_it = mod_elem_list->begin(); elem_it != mod_elem_list->end(); ++elem_it)
	{
		if ((*elem_it)->simulation_step != current_step)
		{
			(*elem_it)->simulation_step = current_step;
			to_elements.push_back(*elem_it);
		}
	}
}

void Environment::GetDependencies(StateVar* from_var, std::vector<Map_Elem*>& to_elements)
{
	list<Map_Elem*>* mod_elem_list = var_map.GetNodeList(from_var->GetId());
	list<Map_Elem*>::iterator elem_it;
	for (elem_it = mod_elem_list->begin(); elem_it != mod_elem_list->end(); ++elem_it)
	{
		if ((*elem_it)->simulation_step != current_step)
		{
			(*elem_it)->simulation_step = current_step;
			to_elements.push_back(*elem_it);
		}
	}
}

void Environment::GetDependencies(const std::vector<Entity*>& from_entities, std::vector<Map_Elem*>& to_elements)
{
	for(size_t i = 0; i < from_entities.size(); ++i) 
	{
		if (from_entities[i] != NULL)
			GetDependencies(from_entities[i], to_elements);
	}
}

void Environment::GetDependencies(const std::vector<StateVar*>& from_vars, std::vector<Map_Elem*>& to_elements)
{
	for(size_t i = 0; i < from_vars.size(); ++i) 
	{
		GetDependencies(from_vars[i], to_elements);
	}
}

////////////////////////////////////////////////////////////////
// Ambient management: add functions
//

//
void Environment::AddRate(string key,double rate) {
	rate_map[key] = rate;
}

bool Environment::AddEvent(EventReaction* new_event)
{
	vector< pair<double,double> > parameters;
    return AddEvent(new_event,0,0,parameters);
}

bool Environment::AddEvent(EventReaction* new_event,double param1,double param2,vector< pair<double,double> > parameters)
{   
	// Initializes EvenCond and EventVerb. There might be errors (usage of wrong Entities, etc)
	// so check it before adding it to the environment
	if (!new_event->Init())
		return false;

	if (new_event->GetRate() == HUGE_VAL)
	{
		inf_act_map.Insert(new_event->GetSourceEntities(), new_event);
	}
	else 
	{
		Map_Elem* this_event = new Map_Elem(new_event);

		//In ogni caso:
		// aggiungo tutte le voci alla tabella di lookup veloce
		ent_map.ReserveEntry(new_event->GetSourceEntities());
		ent_map.ReserveEntry(new_event->GetTargetEntities());
		ent_map.ReserveEntry(new_event->GetCondEntities());

		// aggiungo l'evento		   
		vector<Entity*> keys;
		//filter out those already there
		std::set<int> entity_ids;
		std::vector<Entity*>::const_iterator e_it;

		for (e_it = new_event->GetSourceEntities().begin(); e_it != new_event->GetSourceEntities().end(); ++e_it)
		{
			entity_ids.insert((**e_it).GetId());
			keys.push_back(*e_it);
		}

		for (e_it = new_event->GetTargetEntities().begin(); e_it != new_event->GetTargetEntities().end(); ++e_it)
		{
			Entity* ent = *e_it;
			//can be null is Nil bproc
			if (ent != NULL)
			{
				if (entity_ids.find((*ent).GetId()) == entity_ids.end())
				{
					entity_ids.insert((*ent).GetId());
					keys.push_back(ent);
				}
			}
		}

		for (e_it = new_event->GetCondEntities().begin(); e_it != new_event->GetCondEntities().end(); ++e_it)
		{
			if (entity_ids.find((**e_it).GetId()) == entity_ids.end())
			{
				entity_ids.insert((**e_it).GetId());
				keys.push_back(*e_it);
			}
		}


		// Insert action in ActMap considering the fact that the actions could be an
		// action with a general distribution associated
		if ( new_event->GetEventCondType() == COND_RATE_NORMAL )
		{
			// the first parameter is the mean, the second is the variance
			GDNormal *distr_normal = new GDNormal(param1,param2,this_event,act_map.GetGDManager());
			act_map.InsertGD(keys,this_event,distr_normal);
		}
		else if ( new_event->GetEventCondType() == COND_RATE_GAMMA )
		{
			// the first parameter is the scale (sigma), the second is the shape (k)
			GDGamma *distr_gamma = new GDGamma(param2,param1,this_event,act_map.GetGDManager());
			act_map.InsertGD(keys,this_event,distr_gamma);
		}
		else if ( new_event->GetEventCondType() == COND_RATE_HYPEXP )
		{
			// list of parameters of the hyperexponential distribution
			GDHypExp *distr_hypexp = new GDHypExp(parameters,this_event,act_map.GetGDManager());
			act_map.InsertGD(keys,this_event,distr_hypexp);
		}
		else
		{
			act_map.Insert(keys, this_event);
		}

      // now add additional dependencies to other variables and/or entites.
      // two cases:
      // 1) a rate function that specifies variables and entities
      // 2) a cond made with state var list

      std::vector<Entity*> entity_keys;
		std::vector<StateVar*> var_keys;

		std::set<int> var_ids;

      std::vector<StateVar*> direct_var_dep;
      std::vector<Entity*> direct_entity_dep;

		if (new_event->GetEventCondType() == COND_RATE_FUN)      
      {
			//Get entities and variables
         direct_var_dep = new_event->funRate->GetStateVarList();         
			direct_entity_dep = new_event->funRate->GetEntityList();
      }
      else if (new_event->GetEventCondType() == COND_STATELIST)
      {
         new_event->GetEventCond()->GetVarList(direct_var_dep);
      }


      if (!direct_var_dep.empty() || !direct_entity_dep.empty())
      {
         //Add the direct dependencies:
         // Entities
		   for (e_it = direct_entity_dep.begin(); e_it != direct_entity_dep.end(); ++e_it)
		   {
			   if (entity_ids.find((**e_it).GetId()) == entity_ids.end())
			   {
				   entity_keys.push_back(*e_it);
				   entity_ids.insert((**e_it).GetId());
			   }                
		   }                

		   //Variables: note that this is recursive, use a queue
		   std::deque<StateVar*> vars_to_examine;
   		

		   std::vector<StateVar*>::iterator v_it;
		   for (v_it = direct_var_dep.begin(); v_it != direct_var_dep.end(); ++v_it)
		   {
			   var_keys.push_back(*v_it);
			   assert(var_ids.find((**v_it).GetId()) == var_ids.end());
			   var_ids.insert((**v_it).GetId());
			   vars_to_examine.push_back(*v_it);
		   }    


		   //Add indirect dependecies
		   while (!vars_to_examine.empty())
		   {
			   StateVar* curr_var = vars_to_examine.front();
			   vars_to_examine.pop_front();

			   std::vector<StateVar*>& indirect_vars = curr_var->GetStatevars();

			   std::vector<StateVar*>::iterator v_it2;
			   for (v_it2 = indirect_vars.begin(); v_it2 != indirect_vars.end(); ++v_it2)
			   {
				   if (var_ids.find((**v_it2).GetId()) != var_ids.end())
				   {
					   var_ids.insert((**v_it2).GetId());
					   vars_to_examine.push_back(*v_it2);
					   var_keys.push_back(*v_it2);

					   // Add all entities
					   std::vector<Entity*>& indirect_entities = (**v_it2).GetEntities();
					   std::vector<Entity*>::iterator e_it2;
					   for (e_it2 = indirect_entities.begin(); e_it2 != indirect_entities.end(); ++e_it2)
					   {
						   if (entity_ids.find((**e_it2).GetId()) == entity_ids.end())
						   {
							   entity_keys.push_back(*e_it2);
							   entity_ids.insert((**e_it2).GetId());
						   }
					   }   
				   }
			   }
		   }

		   //Add all the keys
		   ent_map.ReserveEntry(entity_keys);
		   var_map.ReserveEntry(var_keys);
		   act_map.AddDependencies(var_keys, entity_keys, this_event);     
      }

	}
	return true;
}

void Environment::AddStatevar(StateVar* new_statevar)
{ 
	//aggiungo la variabile e le sue dipendenze al grafo delle dipendenze
	var_dependency_graph.Add(new_statevar, new_statevar->GetStatevars());

   statevar_list.push_back(new_statevar);

	// reserve place for this variable to the map of variables    
	// (state variable we are referring to will be already there)
	act_map.Insert(new_statevar);
}

list<EventReaction*> Environment::GetEventList(int e_id)
{
	list<EventReaction*> res;
	list<Map_Elem*>* elems = ent_map.GetNodeList(e_id);
	list<Map_Elem*>::iterator it;

	for (it = elems->begin(); it != elems->end(); ++it)
	{
		if ((*it)->type == EVENT)
			res.push_back((EventReaction*)((*it)->action));
	}
	return res;
}

Entity* Environment::AddEntity(list<BBinders *> *p_bb_list, PP_Node *p_pproc, int number, int e_id) 
{
	return AddEntity(p_bb_list, p_pproc, number, e_id, NULL);
}

//
Entity *Environment::AddEntity(list<BBinders *> *p_bb_list, PP_Node *p_pproc, int number, int e_id, bool* is_new) 
{
	vector<Entity *>::iterator i;
	BBinders *tmp;
	Entity *e;
	for(i=entity_list.begin();i!=entity_list.end();i++) 
		if ((*i)->Equal(p_bb_list,p_pproc)) {
			(*i)->AddCounter(number);
			delete(p_pproc);
			while(!p_bb_list->empty()) {
				tmp = p_bb_list->front();
				p_bb_list->pop_front();
				delete tmp;
			}
			delete(p_bb_list);
			if (is_new != NULL)
				*is_new = false;
			return (*i);
		}
	if (e_id == EMPTY_ID)
		e_id = symbol_table->NewEntID();

	e = new Entity(this, e_id, p_bb_list, p_pproc);
	p_pproc->UpdateState(e,"");
	e->AddCounter(number);
	e->CodifyBB();
	e->EvalActions();
	e->Rate();
	entity_list.push_back(e);
	e->AddAmbient();
	if (is_new != NULL)
		*is_new = true;
	return e;
}

//
pair<Entity *,Entity *> Environment::RedBim(Bimolecular *elem, string mod, string *name, Iterator_Interface *c_iter) 
{
	pair<Entity *,Entity *> p;
	Iterator_Interface* bim_iter = NULL;
	if (mod == "OUTPUT") 
	{
		p.first = elem->GetOutput();      
		if (c_iter != NULL)
		{
			bim_iter = c_iter->GetIterator(1);
		}
		p.second = p.first->GetAction(elem->GetOutBinder()->GetSubject(),name,mod,bim_iter);
	}
	else 
	{
		p.first = elem->GetInput();
		if (c_iter != NULL)
		{
			bim_iter = c_iter->GetIterator(2);
		}
		p.second = p.first->GetAction(elem->GetInBinder()->GetSubject(),name,mod,bim_iter);
	}
	return p;
}

//
pair<Entity *,Entity *> Environment::RedBimBind(Bimolecular_Bind *elem, string mod, string *name, Iterator_Interface *c_iter) 
{
	pair<Entity *,Entity *> p;
	Iterator_Interface* bim_iter = NULL;
	if (mod == "OUTPUT")
	{
		if (c_iter != NULL)
		{
			bim_iter = c_iter->GetIterator(1);
		}
		p.first = elem->GetOutput();
		p.second = p.first->GetAction(elem->GetOutBinder()->GetSubject(),name,mod,bim_iter);
	}
	else 
	{
		if (c_iter != NULL)
		{
			bim_iter = c_iter->GetIterator(2);
		}
		p.first = elem->GetInput();
		p.second = p.first->GetAction(elem->GetInBinder()->GetSubject(),name,mod,bim_iter);
	}
	return p;
}

//TODO: in desctructor??
void Environment::Destroy_Env() {

	Entity *tmp_e;
	while(!entity_list.empty()) {
		tmp_e = entity_list.back();
		entity_list.pop_back();
		delete(tmp_e);
	}

	/*Bimolecular *tmp_b;
	while(!bim_list.empty()) {
		tmp_b = bim_list.front();
		bim_list.pop_front();
		delete(tmp_b);
	}*/
}

//
void Environment::AddReaction(reaction_key *key) 
{

	ReactionMap::iterator it = reaction_map.find(*key);
	if (it != reaction_map.end())
	{
		it->second++;
	} else {
		reaction_map[*key] = 1;	
	}
}

//
Iterator_Interface *Environment::GetMonoActIterator(Entity *ent)
{
	return ( new Mono_Act_Iterator( this->comp_rel->GetIteratorOne(ent->GetId()), ent->GetIterator() ) );
}

//
Iterator_Interface *Environment::GetMonoInfActIterator(Entity *ent)
{
	return ( new Mono_Act_Iterator( this->comp_rel->GetIteratorOne(ent->GetId()), ent->GetInfIterator() ) );
}

//
void Environment::Execute(FILE* file, ofstream* fstr_spec, ofstream* fstr_spec2, FILE* file_c, FILE* file_v, FILE *fstr_tmp) 
{
	set<int> observed_complexes;
	vector<Map_Elem *> elem_list;
	elem_list.reserve(1000);

	double c_delta = 0.0;
	double multiple;

	reaction_key reaction;

	// imposto la precisione sui decimali per il filestream
	//(*fstr).precision(10);

	act_map.Init();
	// EntityWorker::Init();

	// create structure for containing the actual action
	// to be executed
	Action action;

	//////////////////////////////////////////////////////////////////////
   actual_step = 1;

   boost::int64_t realSteps = 0;

   // print initial state
   // PrintState(file, file_c, file_v, observed_complexes, 0.0);

	while (STEP >= 0 && current_time < E_TIME) 
	{

		// Select an action to perform in this simulation step
		action.first = NULL;
		action.second = HUGE_VAL;

		//elem_list.clear();
		//safer, guaranteed to not allocate/deallocate every time
		elem_list.resize(0);

		// inizializzo la struttura che tiene traccia delle
		// azioni con rate infinito
		inf_act_map.InitializeAmbientInf();

		// invoco la selezione stocastica globale
		bool made_selection;		
		made_selection = inf_act_map.SS(action, this);

		// if we do not have infinite rate actions, select finite rate action
		if (!made_selection)
		{
			made_selection = act_map.SS(action);
		}

		if (!made_selection)
		{
			//we have no more actions, finished!
			break;
		}

      ++realSteps;

		// In base alla precisione di stampa verifico
		// che non venga stampata un'azione con tempo approssimato
		// (in base alla precisione di stampa impostata)
		// uguale a quello dell'azione precedente.

      //do not print immediate actions
      if (action.second > 0.0) 
      {
         // discard also actions within the print precision
         if (action.second - last_print_time > PRINT_EPSILON)
         {
            switch (MODE)
            {
			case M_TIME:
				{
					PrintState(file, file_c, file_v, observed_complexes, current_time);
					last_print_time = current_time;
					++actual_step;
				}
				break;

			case M_STEP:
				{
					PrintState(file, file_c, file_v, observed_complexes, current_time);
					last_print_time = current_time;
					STEP--;
					++actual_step;
				}
				break;

            case M_STEP_DELTA:
               {
                  // always print the first step
                  if (current_time == 0.0)
                  {
					 c_delta += action.second;
                     PrintState(file, file_c, file_v, observed_complexes, current_time);
                     last_print_time = current_time;
                     STEP--;
                     ++actual_step;                  
                  }
                  else
                  {  
                     //
                     c_delta += (action.second - current_time);
                     if (c_delta >= DELTA) 
                     {
                        multiple = ( c_delta - fmod(c_delta, DELTA) ) ;
                        int decr = (int)( multiple / DELTA + 0.00001 );

						if (FLAG_FIXED_DELTA)
						{
							for (int i = 0; i < decr; ++i)
							{
								double print_time = (actual_step - 1) * DELTA;
								PrintState(file, file_c, file_v, observed_complexes, print_time);
								last_print_time = print_time;
								++actual_step;
							}
						}
						else
						{
							PrintState(file, file_c, file_v, observed_complexes, current_time);
							last_print_time = current_time;
							actual_step += decr;
						}
				            // quando specifico il numero di passi il decremento viene sempre fatto
                        c_delta -= multiple;
                        STEP -= decr;
                     }
                  }
               }
               break;

            default:
               assert(false);
            }
         }
      }

		// Execution of the selected action
		ExecuteStep(elem_list, action, NULL, &reaction);

		// imposto il tempo
		if (action.second > 0.0)
		{
			assert(action.second >= current_time);
			current_time = action.second;
		}

		// aggiorno le variabili continue se eseguito una di esse
		if (action.first->GetCategory() == STATEVAR)
		{
			act_map.UpdateDeltas(current_time);
		}

		// aggiorno i tempi e i rate delle azioni coinvolte;		
		if (!act_map.Update(elem_list, current_step, current_time, action))
		{
			Error_Manager::PrintError(0, "cannot update action map, simulation aborted at step " + Utility::i2s(current_step));
			break;
		}

		++current_step;

		if (this->FLAG_COMPLEXES_ELIMINATION)
		{
			this->PrintAndDeleteComplexes(fstr_tmp,observed_complexes);
		}
	}

	// La funzione Print() stampa a video tutte le informazioni delle entita' 
	// che compongono lo stato finale del sistema.
	// Print();
	// stampa su file della lista di specie dello stato finale e
	// della loro struttura
	// comp_rel.Print();
	PrintFile(fstr_spec);

   if (fstr_spec2 != NULL)
      PrintFile2(fstr_spec2);

	// Stampa stato finale
	if (!this->FLAG_COMPLEXES_ELIMINATION && !this->FLAG_FIXED_DELTA)
		PrintState(file, file_c, file_v, observed_complexes, current_time);
	if ( this->FLAG_COMPLEX_STRUCTURE )
		this->PrintFinalComplexesStructure();

   //cout << "Real steps: "<< endl << realSteps << endl;
}

bool Environment::CheckTSGeneration()
{
	// Verifico se ci sono variabili continue
	std::list<StateVar *>::iterator i;
	
	for ( i = statevar_list.begin() ; i != statevar_list.end() ; i++ )
	{
		if ( (*i)->GetType() == STATEVAR_CONTINUOUS )
			return false;
	}

	return true;
}

bool Environment::GenerateTS(Transition_System *TS,ofstream *fstr_spec)
{
	// Controllo se e' possibile generare il TS
	if ( !this->CheckTSGeneration() )
		return false;

	vector<Map_Elem *> elem_list;

	Action action;
	action.second = 0.0;

	TS_Signature *actual_signature;
	TS_Signature *next_signature;
	vector<Entity *>::iterator i;

	actual_step = -1;
	current_time = (double)(-1);

	actual_signature = this->GetSignature();

	TS->AddNode(actual_signature);
	TS->IteratorReset();

	double single_rate;
	bool inf_act_entities;
	bool inf_act_tot;

	Iterator_Interface *current_iterator;

	reaction_key reaction;

	// TS state iteration
	while ( !TS->IteratorIsEnd() )
	{
		//counter++;

		inf_act_tot = false;

		actual_signature = TS->IteratorGetActualSignature();
		inf_act_entities = this->SetSignature(*actual_signature);

		inf_act_map.InitializeAmbientInf();
		inf_act_map.GenerateAmbientInf();

		if ( inf_act_entities || !inf_act_map.IsEmpty() )
			inf_act_tot = true;

		size_t size = entity_list.size();
		for(size_t i = 0; i < size; i++) 
		{
			if ( entity_list[i]->GetCounter() > 0 ) 
			{
				// Se l'entita' non e' in un complesso allora questo do viene
				// fatto solo una volta per l'entita' corrente. Altrimenti il ciclo
				// continua iterando su tutti i complessi contenenti questa entita'. 
				if ( entity_list[i]->InfListEmpty() && !inf_act_tot )
				{
					current_iterator = this->GetMonoActIterator(entity_list[i]);
					current_iterator->IteratorReset();
					while( !current_iterator->IteratorIsEnd() ) 
					{
						action.first = entity_list[i];
						single_rate = entity_list[i]->GetRate(current_iterator);
						ExecuteStep(elem_list,action,current_iterator,&reaction);
						next_signature = this->GetSignature();
						TS->AddEdge(actual_signature,next_signature,EMPTY_STRING,single_rate);
						// Ripristino la actual_signature
						this->SetSignature(*actual_signature);
						// Next entity action
						current_iterator->IteratorNext();
						elem_list.clear();
					}
					delete current_iterator;
				}
				else if ( inf_act_entities )
				{
					current_iterator = this->GetMonoInfActIterator(entity_list[i]);
					current_iterator->IteratorReset();
					while( !current_iterator->IteratorIsEnd() ) 
					{
						action.first = entity_list[i];
						ExecuteStep(elem_list,action,current_iterator, &reaction);
						next_signature = this->GetSignature();
						TS->AddEdge(actual_signature,next_signature,EMPTY_STRING,HUGE_VAL);
						// Ripristino la actual_signature
						this->SetSignature(*actual_signature);
						// Next entity action
						current_iterator->IteratorNext();
						elem_list.clear();
					}
					delete current_iterator;
				}				
			} 
		}

		// l'actual signature e' gia' impostata
		if ( inf_act_map.IsEmpty() && !inf_act_tot )
		{
			// Ricavo dall'iteratore l'azione attuale
			current_iterator = this->act_map.GetIterator(this);
			current_iterator->IteratorReset();

			while ( !current_iterator->IteratorIsEnd() )
			{
				action.first = (*((ActMap_Iterator *)current_iterator)->GetCurrent())->action;

				ExecuteStep(elem_list,action,current_iterator,&reaction);
				next_signature = this->GetSignature();
				// Ripristino la actual_signature
				this->SetSignature(*actual_signature);
				elem_list.clear();

				// The single rate is calculated after the execution and the resetting of the
				// initial signature. This because only after the execution we know if the BIM
				// or BIND action has been executed on the same complex or not.
				single_rate = action.first->GetRate(current_iterator);
				TS->AddEdge(actual_signature,next_signature,EMPTY_STRING,single_rate);

				current_iterator->IteratorNext();
			}
			delete current_iterator;
		}
		else if ( !inf_act_map.IsEmpty() )
		{
			current_iterator = this->inf_act_map.GetIterator(this);
			current_iterator->IteratorReset();

			while ( !current_iterator->IteratorIsEnd() )
			{
				action.first = (*((InfActMap_Iterator *)current_iterator)->GetCurrent());

				ExecuteStep(elem_list,action,current_iterator,&reaction);
				next_signature = this->GetSignature();
				TS->AddEdge(actual_signature,next_signature,EMPTY_STRING,HUGE_VAL);
				// Ripristino la actual_signature
				this->SetSignature(*actual_signature);
				elem_list.clear();

				current_iterator->IteratorNext();
			}
			delete current_iterator;
		}

		TS->IteratorNext();
	}

	// Print of the spec file
	PrintFile(fstr_spec);
	//PrintFile2(fstr_spec2);

	return true;
}


// Non eseguo nessun controllo di validita' sul tipo di iteratore a questo livello
// e assumo siano corretti rispetto all'azione che viene eseguita. Voglio evitare
// controlli che potrebbero rallentare la simulazione e la generazione del TS. In
// ogni caso e' possibile introdurre i controlli verificando se il tipo di iteratore
// e' conforme al tipo di azione
void Environment::ExecuteStep(vector<Map_Elem *>& elem_list, Action& action, 
		Iterator_Interface *c_iter, reaction_key *reaction)
{
	Entity *ent,*n_ent;
	Entity *bind_1,*bind_2;
	pair<Entity *,Entity *> p_in, p_out, p;
	string name,bind_state;
	Bind *act_bind;
	Unbind *act_unbind;
	Bimolecular *bim_act;
	Bimolecular_Bind *bimbind_act;

	Iterator_Interface *comb_iter = NULL;
	Iterator_Interface *act_iter = NULL;

	if ( c_iter != NULL ) 
	{
		// sicuramente se e' diverso da NULL ha due iteratori, di cui uno pero' puo 
		// anche essere NULL+
		act_iter = c_iter->GetIterator(1);
		comb_iter = c_iter->GetIterator(2);
	}


	// eseguo l'azione a seconda di che tipo e'
	switch (action.first->GetCategory())
	{
	case MONO:
		{
			ent = (Entity *)action.first;
			GetDependencies(ent, elem_list);

			n_ent = ent->RedMono(act_iter);
			if (n_ent != NULL) 
			{
				GetDependencies(n_ent, elem_list); 
			}

			comp_rel->Mono(reaction, ent,n_ent,&elem_list,comb_iter);


			//const std::string& reaction = SymbolTable::GetReactionName(ent, n_ent);	
			// aggiorno la mappa di tutte le azioni eseguite
			AddReaction(reaction);

		}
		break;

	case BIM:
		{
			// Can have functions (and so state variables, both continuous and discrete)
			name = EMPTY_STRING;
			bim_act = (Bimolecular *)action.first;

			p_out = RedBim(bim_act, "OUTPUT", &name, act_iter);
			GetDependencies(p_out.first, elem_list);
			GetDependencies(p_out.second, elem_list);

			p_in = RedBim(bim_act,"INPUT",&name,act_iter);
			GetDependencies(p_in.first, elem_list);
			GetDependencies(p_in.second, elem_list);

			comp_rel->Bim(reaction,p_in.first,p_out.first,p_in.second,p_out.second,
				&elem_list,comb_iter);
			AddReaction(reaction);
		}
		break;

	case INTER_BIND:
		{
			name = EMPTY_STRING;
			bimbind_act = (Bimolecular_Bind *)action.first;
			p_out = RedBimBind(bimbind_act,"OUTPUT",&name,act_iter);
			GetDependencies(p_out.first, elem_list);
			GetDependencies(p_out.second, elem_list);

			p_in = RedBimBind(bimbind_act,"INPUT",&name,act_iter);
			GetDependencies(p_in.first, elem_list);
			GetDependencies(p_in.second, elem_list);

			comp_rel->BimBind(reaction,p_in.first,p_out.first,p_in.second,p_out.second,
				bimbind_act->GetInBinder()->GetSubject(),bimbind_act->GetOutBinder()->GetSubject(),&elem_list,
				comb_iter);
			AddReaction(reaction);
		}
		break;

	case BIND:
		{
			act_bind = (Bind *)action.first;
			bind_1 = act_bind->GetEntity1()->Binding(act_bind->GetBinder1()->GetSubject());
			bind_2 = act_bind->GetEntity2()->Binding(act_bind->GetBinder2()->GetSubject());

			comp_rel->Bind(reaction, act_bind->GetEntity1(),act_bind->GetEntity2(),
				bind_1,bind_2,act_bind->GetBinder1()->GetSubject(),act_bind->GetBinder2()->GetSubject(),
				&elem_list,comb_iter);
			AddReaction(reaction);

			GetDependencies(act_bind->GetEntity1(), elem_list);
			GetDependencies(act_bind->GetEntity2(), elem_list);
			GetDependencies(bind_1, elem_list);
			GetDependencies(bind_2, elem_list);

		}
		break;

	case UNBIND:
		{
			act_unbind = (Unbind *)action.first;
			bind_1 = act_unbind->GetEntity1()->Unbinding(act_unbind->GetBinder1()->GetSubject());
			bind_2 = act_unbind->GetEntity2()->Unbinding(act_unbind->GetBinder2()->GetSubject());

			comp_rel->Unbind(reaction, act_unbind->GetEntity1(),act_unbind->GetEntity2(),
				bind_1,bind_2,act_unbind->GetBinder1()->GetSubject(),act_unbind->GetBinder2()->GetSubject(),
				&elem_list,comb_iter);
			AddReaction(reaction);

			GetDependencies(act_unbind->GetEntity1(), elem_list);
			GetDependencies(act_unbind->GetEntity2(), elem_list);
			GetDependencies(bind_1, elem_list);
			GetDependencies(bind_2, elem_list);
		}
		break;

	case EVENT:
		{
			EventReaction* act_event = (EventReaction*)action.first;
			act_event->DoEvent();
			act_event->GetAllModified(elem_list);

			switch (act_event->GetAffectedEntitiesCount())
			{
			case 2:
				{	
					comp_rel->Event(reaction, act_event->GetFirstSourceEntity(), 
						act_event->GetFirstAffectedEntity(), 
						act_event->GetSecondAffectedEntity(), 
						act_event->GetFirstCardinality(),
						act_event->GetSecondCardinality(),
						act_event->GetEventVerbType(),
						&elem_list);
					AddReaction(reaction);
				}
				break;

			case 1:
				{	
					comp_rel->Event(reaction, act_event->GetFirstSourceEntity(),
						act_event->GetSecondSourceEntity(), 
						act_event->GetFirstAffectedEntity(), 
						act_event->GetFirstCardinality(),
						act_event->GetEventVerbType(),
						&elem_list);
					AddReaction(reaction);
				}
				break;

			case 0:
				{	
					comp_rel->Event(reaction, act_event->GetFirstSourceEntity(), 
						act_event->GetEventVerbType(),
						&elem_list);
					AddReaction(reaction);
				}
				break;
			}
		}
		break;

	case STATEVAR:
		{
			// fill elem_list directly (more efficient)
			StateVarList* list = (StateVarList*)(action.first);

			GetDependencies(list->stateVarList, elem_list); 

			std::vector<StateVar*>::iterator v_it;
			for (v_it = list->stateVarList.begin(); v_it != list->stateVarList.end(); ++v_it)
			{
				(**v_it).UpdateValue();
			}
	
		}
		break;

	}

}

bool Environment::IsSBMLCompliant(){
	list<StateVar *>::iterator v_it;
	list<EventReaction*> events;
	list<Element*> *elem;
	list<Element*>::iterator e_it;
	list<Map_Elem*> *map_elem;
	list<Map_Elem*>::iterator me_it;

	EventReaction *event;


	for (v_it = statevar_list.begin(); v_it != statevar_list.end(); v_it ++){
		if ((*v_it)->GetType() == STATEVAR_CONTINUOUS){
			cerr << "The model cannot be translated:" << endl;
			cerr << "Continuous variable " << symbol_table->GetVarNameFromID((*v_it)->GetId()) << " cannot be exported in SBML" << endl;
			return false;
		}
	}

	map_elem = act_map.GetActList();

	
	for (me_it = map_elem->begin(); me_it != map_elem->end(); me_it++){
		if((*me_it)->action->GetCategory() == EVENT){
			assert(dynamic_cast<EventReaction *>((*me_it)->action));
			event = static_cast<EventReaction *>((*me_it)->action);
			if (event->GetEventCondType() != COND_RATE && event->GetEventCondType() != COND_RATE_FUN && event->GetEventCondType() != COND_RATE_IMMEDIATE){
				cerr << "The model cannot be translated:" << endl;
				cerr << "There is a conditional event" << endl;	
				return false;
			}
		}
	}

	
	elem = inf_act_map.GetActList();

	for (e_it = elem->begin(); e_it != elem->end(); e_it++){
		if((*e_it)->GetCategory() == EVENT){
			assert(dynamic_cast<EventReaction *>(*e_it));
			event = static_cast<EventReaction *>(*e_it);
				if (event->GetEventCondType() != COND_RATE && event->GetEventCondType() != COND_RATE_FUN && event->GetEventCondType() != COND_RATE_IMMEDIATE){
				cerr << "The model cannot be translated:" << endl;
				cerr << "There is a conditional event" << endl;	
				return false;
			}

		}
	}

	return true;
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Iterators classes implementation

ActMap_Iterator::ActMap_Iterator(MAP_ELEM_LIST p_iterator_begin, size_t p_iterator_size, Environment *p_env)
: Iterator<MAP_ELEM_LIST>(p_iterator_begin,p_iterator_size,IT_ACT_MAP,2)
{
	env = p_env;
}

ActMap_Iterator::~ActMap_Iterator() 
{}

void ActMap_Iterator::IteratorReset()
{
	iterator_current = iterator_begin;
	current_size = 0;
	while ( current_size != size && ( (*iterator_current)->action->GetCategory() == MONO
		|| !(*iterator_current)->action->IsDiscrete() ) )
	{
		iterator_current++;
		current_size++;
	}
	ActionReset();
	// Controlliamo che in casi particolari gli iteratori non siano gia' ad END dopo
	// il loro Reset (succede solo se iteriamo con ONLY_BIM e ONLY_MONO)
	if (  this->GetIterator(1) != NULL && this->GetIterator(1)->IteratorIsEnd() )
	{
		this->IteratorNext();
	}
	if ( this->GetIterator(2) != NULL && this->GetIterator(2)->IteratorIsEnd() )
	{
		this->IteratorNext();
	}
}

void ActMap_Iterator::IteratorNext()
{
	bool next = false;
	if ( current_size != size )
	{
		if ( this->GetIterator(2) != NULL && this->GetIterator(1) != NULL )
		{
			this->GetIterator(1)->IteratorNext();
			if ( this->GetIterator(1)->IteratorIsEnd() )
			{
				this->GetIterator(2)->IteratorNext();
				this->GetIterator(1)->IteratorReset();
			}

			if ( this->GetIterator(2)->IteratorIsEnd() )
				next = true;
		}
		else if ( this->GetIterator(1) != NULL )
		{
			this->GetIterator(1)->IteratorNext();
			if ( this->GetIterator(1)->IteratorIsEnd() )
				next = true;
		}
		else if ( this->GetIterator(2) != NULL )
		{
			this->GetIterator(2)->IteratorNext();
			if ( this->GetIterator(2)->IteratorIsEnd() )
				next = true;
		}
		else
		{
			next = true;
		}

		if ( next )
		{
			iterator_current++;
			current_size++;
			while ( current_size != size && ( (*iterator_current)->action->GetCategory() == MONO
				|| !(*iterator_current)->action->IsDiscrete() ) )
			{
				current_size++;
				iterator_current++;
			}
			ActionReset();

			if ( this->GetIterator(1) != NULL && this->GetIterator(1)->IteratorIsEnd() )
			{
				this->IteratorNext();
			}
			if ( this->GetIterator(2) != NULL && this->GetIterator(2)->IteratorIsEnd() )
			{
				this->IteratorNext();
			}
		}
	}
}

void ActMap_Iterator::ActionReset()
{
	if ( current_size == size ) 
		return;

	Bimolecular *bim_act;
	Bimolecular_Bind *bim_bind_act;
	Bind *bind_act;
	Unbind *unbind_act;

	switch( (*iterator_current)->type )
	{
		case BIM:
			bim_act = (Bimolecular *)((*iterator_current)->action);
			this->SetIterator(bim_act->GetIterator(),1);
			// Qui stare attenti all'ordine di input e output
			this->SetIterator(env->GetComplexRel()->GetIteratorTwo(bim_act->GetInput()->GetId(),bim_act->GetOutput()->GetId()),2);
			break;
		case INTER_BIND:
			bim_bind_act = (Bimolecular_Bind *)((*iterator_current)->action);
			this->SetIterator(bim_bind_act->GetIterator(),1);
			this->SetIterator(env->GetComplexRel()->GetIteratorBind(bim_bind_act->GetInput(),bim_bind_act->GetOutput(),
						bim_bind_act->GetInBinder()->GetSubject(),bim_bind_act->GetOutBinder()->GetSubject()),2);
			break;
		case BIND:
			bind_act = (Bind *)((*iterator_current)->action);
			this->SetIterator(NULL,1);
			this->SetIterator(env->GetComplexRel()->GetIteratorTwo(bind_act->GetEntity1()->GetId(),
						bind_act->GetEntity2()->GetId()),2);
			break;
		case UNBIND:
			unbind_act = (Unbind *)((*iterator_current)->action);
			this->SetIterator(NULL,1);
			this->SetIterator(env->GetComplexRel()->GetIteratorBind(unbind_act->GetEntity1(),unbind_act->GetEntity2(),
						unbind_act->GetBinder1()->GetSubject(),unbind_act->GetBinder2()->GetSubject()),2);
			break;
		default:
			this->SetIterator(NULL,1);
			this->SetIterator(NULL,2);
			break;
	}
}


InfActMap_Iterator::InfActMap_Iterator(ELEM_LIST p_iterator_begin,size_t p_iterator_size, Environment *p_env) 
: Iterator<ELEM_LIST>(p_iterator_begin,p_iterator_size,IT_INF_ACT_MAP,2)
{
	env = p_env;
}

InfActMap_Iterator::~InfActMap_Iterator() 
{}

void InfActMap_Iterator::IteratorReset() 
{
	iterator_current = iterator_begin;
	current_size = 0;
	while ( current_size != size && ( (*iterator_current)->GetCategory() == MONO
		|| !(*iterator_current)->IsDiscrete() ) )
	{
		current_size++;
		iterator_current++;
	}
	ActionReset();
}

void InfActMap_Iterator::IteratorNext() 
{
	bool next = false;
	if ( current_size != size )
	{
		if ( this->GetIterator(2) != NULL && this->GetIterator(1) != NULL )
		{
			this->GetIterator(1)->IteratorNext();
			if ( this->GetIterator(1)->IteratorIsEnd() )
			{
				this->GetIterator(2)->IteratorNext();
				this->GetIterator(1)->IteratorReset();
			}

			if ( this->GetIterator(2)->IteratorIsEnd() )
				next = true;
		}
		else if ( this->GetIterator(1) != NULL )
		{
			this->GetIterator(1)->IteratorNext();
			if ( this->GetIterator(1)->IteratorIsEnd() )
				next = true;
		}
		else if ( this->GetIterator(2) != NULL )
		{
			this->GetIterator(2)->IteratorNext();
			if ( this->GetIterator(2)->IteratorIsEnd() )
				next = true;
		}
		else
		{
			next = true;
		}

		if ( next )
		{
		iterator_current++;
		current_size++;
			while ( current_size != size && ( (*iterator_current)->GetCategory() == MONO
				|| !(*iterator_current)->IsDiscrete() ))
			{
				current_size++;
			    iterator_current++;
			}
		ActionReset();
		}	
	}
}

void InfActMap_Iterator::ActionReset()
{
	if ( current_size == size ) 
		return;

	Bimolecular *bim_act;
	Bimolecular_Bind *bim_bind_act;
	Bind *bind_act;
	Unbind *unbind_act;
	
	switch( (*iterator_current)->GetCategory() )
	{
		case BIM:
			bim_act = (Bimolecular *)*iterator_current;
			this->SetIterator(bim_act->GetIterator(),1);
			this->SetIterator(env->GetComplexRel()->GetIteratorTwo(bim_act->GetOutput()->GetId(),bim_act->GetInput()->GetId()),2);
			break;
		case INTER_BIND:
			bim_bind_act = (Bimolecular_Bind *)*iterator_current;
			this->SetIterator(bim_bind_act->GetIterator(),1);
			this->SetIterator(env->GetComplexRel()->GetIteratorBind(bim_bind_act->GetInput(),bim_bind_act->GetOutput(),
						bim_bind_act->GetInBinder()->GetSubject(),bim_bind_act->GetOutBinder()->GetSubject()),2);
			break;
		case BIND:
			bind_act = (Bind *)*iterator_current;
			this->SetIterator(NULL,1);
			this->SetIterator(env->GetComplexRel()->GetIteratorTwo(bind_act->GetEntity1()->GetId(),
						bind_act->GetEntity2()->GetId()),2);
			break;
		case UNBIND:
			unbind_act = (Unbind *)*iterator_current;
			this->SetIterator(NULL,1);
			this->SetIterator(env->GetComplexRel()->GetIteratorBind(unbind_act->GetEntity1(),unbind_act->GetEntity2(),
						unbind_act->GetBinder1()->GetSubject(),unbind_act->GetBinder2()->GetSubject()),2);
			break;
		default:
			this->SetIterator(NULL,1);
			this->SetIterator(NULL,2);
			break;
	}
}

Mono_Act_Iterator::Mono_Act_Iterator(Iterator_Interface *comb_iterator, Iterator_Interface *act_iterator)
: Iterator_Interface(IT_MONO_ACT,2)
{
	this->SetIterator(act_iterator,1);
	this->SetIterator(comb_iterator,2);
}

Mono_Act_Iterator::~Mono_Act_Iterator()
{}

void Mono_Act_Iterator::IteratorReset()
{
	if ( this->GetIterator(1) != NULL )
		this->GetIterator(1)->IteratorReset();
	if ( this->GetIterator(2) != NULL )
		this->GetIterator(2)->IteratorReset();
}

void Mono_Act_Iterator::IteratorNext()
{
	if ( this->GetIterator(2) != NULL )
	{
		this->GetIterator(2)->IteratorNext();
		if ( this->GetIterator(2)->IteratorIsEnd() )
		{
			this->GetIterator(1)->IteratorNext();
			this->GetIterator(2)->IteratorReset();
		}
	}
	else
	{
		this->GetIterator(1)->IteratorNext();
	}
}

bool Mono_Act_Iterator::IteratorIsEnd()
{
	return this->GetIterator(1)->IteratorIsEnd();
}

