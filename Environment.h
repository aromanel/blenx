#ifndef ENVIRONMENT_H_INCLUDED
#define ENVIRONMENT_H_INCLUDED

#include <iostream>
#include <cstdlib>
#include <string>
#include <list>
#include <map>
#include <vector>
#include <deque>
#include <algorithm>
#include <cmath>
#include <ctime>
#include <iostream>
#include <iterator>
#include "Define.h"
#include "FunBase.h"

#include "hash.h"

#include "randomc.h"

#include "Pi_Process.h"
#include "Define.h"
#include "Monomolecular.h"
#include "Bimolecular.h"

//#include "Entity.h"
#include "Map_Elem.h"
#include "DependencyMap.h"
#include "Priority_Queue.h"
#include "Symbol_Table.h"
#include "Singleton.h"

#include "Type_Affinity.h"
#include "FuncMap.h"
#include "Complex_Rel.h"
#include "Complex_Map.h"
#include "Transition_System.h"
#include "StateVar.h"
#include "DependencyGraph.h"
#include "GDManager.h"

//#define _USE_C_RAND

using namespace std;

typedef std::pair<Element *,double> Action;

class PP_Node;
class Entity;
class Complex;
class Bimolecular;
//class Monomolecular;
class EventReaction;
class Priority_Queue;
class EntityMap;
//class Type_Affinity;
class Transition_System;
class GDManager;


typedef list<Map_Elem *>::iterator MAP_ELEM_LIST;

class ActMap_Iterator : public Iterator<MAP_ELEM_LIST>
{
private:
	Environment *env;
public:
	ActMap_Iterator(MAP_ELEM_LIST p_iterator_begin,size_t p_iterator_size, Environment *p_env);
	~ActMap_Iterator();
	void IteratorReset();
	void IteratorNext();
	void ActionReset();
};


class ActMap 
{
private:

	//for update function
	//std::vector<Map_Elem*> elements;


	struct DeltaVars
	{
		double original_delta;
		double next_firing;
		StateVarList* var_list;

		DeltaVars(StateVarList* p_var_list, double delta)
		{
			var_list = p_var_list;
			original_delta = delta;
			next_firing = delta;
		}
	};

	list<Map_Elem *> act_map; 

	DependencyMap<Entity*>& my_ent_map;
	DependencyMap<StateVar*>& my_var_map;

	hash_map<double, StateVarList*> delta_to_vars_map;
	std::deque<DeltaVars> delta_list;

	Priority_Queue my_pq;
	GDManager *my_gd_manager;

	//Forbid copy
	const ActMap& operator=(const ActMap&);
	ActMap(const ActMap&);

	//Test functions
	bool SameElements(const vector<Map_Elem*>& elements, const std::vector<Entity*>& modify);

public:
	ActMap(DependencyMap<Entity*>&, DependencyMap<StateVar*>&);
	~ActMap();
	void Insert(const vector<Entity*>& keys, Map_Elem *elem);
	void Insert(Entity *key1, Entity *key2, Map_Elem *elem);
	void Insert(Entity *key, Map_Elem *elem);
	void Insert(StateVar* var);
	void InsertGD(Entity* key1, Entity* key2, Map_Elem *elem, AffRuleBase *aff);
	void InsertGD(const vector<Entity*>& keys, Map_Elem *elem, GDBase *new_elem);
	void InitGDManager(Environment *env)
	{
		this->my_gd_manager->SetEnv(env);
	};

	GDManager *GetGDManager()
	{
		return my_gd_manager;
	};

	void AddDependencies(const vector<StateVar*>& statevar_keys,
		const vector<Entity*>& entity_keys, 
		Map_Elem *elem);
	void AddDependencies(const vector<StateVar*>& statevar_keys,
		Map_Elem *elem);
	void AddDependencies(const vector<Entity*>& entity_keys, 
		Map_Elem *elem);

	bool Update(const std::vector<Map_Elem *>& elem_list, 
		unsigned long current_step, double current_time, Action &action);

	void Init();
	void Print();

	double NextDelta()
	{
		if (delta_list.empty())
			return HUGE_VAL;

		return delta_list.front().next_firing;
	}

	bool SS(Action& action) 
	{
		double pq_time = my_pq.TopTime();
		double delta_time = NextDelta();
		double gd_time = my_gd_manager->GetMinTime();

		if  ( ( pq_time == HUGE_VAL ) && ( gd_time == HUGE_VAL ) && ( delta_time == HUGE_VAL ) )
		{
			//finished
			return false;
		}

		if ( pq_time < delta_time )
		{      
			if ( pq_time < gd_time )
			{
				return my_pq.SS(action);
			}
			else
			{
				return my_gd_manager->GetMinAction(action);
			}
		}
		else
		{
			if ( delta_time < gd_time)
			{
				action.second = delta_list.front().next_firing;
				action.first = delta_list.front().var_list;
				return true;
			}
			else
			{
				return my_gd_manager->GetMinAction(action);
			}
		}
	}

	// Action on the first delta was executed:
	// Take the first one, reset it, scale all the others and adjust 
	// its position
	void UpdateDeltas(double current_time)
	{     
		//the action just fired, adjust position
		DeltaVars dv = delta_list.front();
		dv.next_firing = current_time + dv.original_delta;
		delta_list.pop_front();

		std::deque<DeltaVars>::iterator it;
		for(it = delta_list.begin(); it != delta_list.end(); ++it)
		{
			if (it->next_firing > dv.next_firing)
				break;
		}

		delta_list.insert(it, dv);      
	}


	Iterator_Interface *GetIterator(Environment *env);

	list<Map_Elem*> *GetActList() { return &act_map; };

};

typedef list<Element *>::iterator ELEM_LIST;

class InfActMap_Iterator : public Iterator<ELEM_LIST>
{
private:
	Environment *env;
public:
	InfActMap_Iterator(ELEM_LIST p_iterator_begin,size_t p_iterator_size, Environment *p_env);
	~InfActMap_Iterator();
	void IteratorReset();
	void IteratorNext();
	void ActionReset();
};

class InfActMap {
private:
	list<Element *> act_map;
	// Structure that keeps track of infinite rate actions
	pair<list<Element *>,double> ambient_inf;

public:
	InfActMap();
	~InfActMap();
	bool IsEmpty() { return ambient_inf.first.empty(); } 
	void InitializeAmbientInf();
	void GenerateAmbientInf();
	void Insert(const vector<Entity*>& keys, Element *elem);
	void Insert(Entity *key1, Entity *key2, Element *elem);
	void Insert(Entity *key, Element *elem);
	void Update(list<Entity *> *modify);
	bool SS(Action& action, Environment* env);
	void Print();
	list<Element *> * GetActList(){return &act_map;};

	Iterator_Interface *GetIterator(Environment *env);
};


// Mappa che tiene traccia se per una coppia (ENTITY,binder) (ENTITY,binder)
// le azioni di unbind e inter_bind sono gia' state inserite o meno

class UIBind_map 
{
private:  

	hash_map<string,UIBind_elem *> ui_map;
	Environment* env;

public:
	UIBind_map(Environment* p_env) : env(p_env) {};
	~UIBind_map() {};
	bool IsEmpty() { return ui_map.empty(); }
	void AddCounter(const string& key, int number);
	UIBind_elem *Find(Entity *e1, const string& b1, Entity *e2, const string& b2);
	void DecBindElem(const string& key, vector<Map_Elem *> *e_list);
	void IncBindElem(Entity *e1, Entity *e2, const string& b1, const string& b2, const string& key, vector<Map_Elem *> *e_list); 
	UIBind_elem *Find(const string& key);
	void Insert(string const& key,UIBind_elem *elem);
	void GetSignature(string& signature);
	void SetSignature(std::vector<string>::const_iterator& tokens_iter, const std::vector<string>& tokens);
	void Print();
};


typedef list<Entity *>::iterator ENTITY_LIST;

class Mono_Act_Iterator : public Iterator_Interface
{
public:
	Mono_Act_Iterator(Iterator_Interface *comb_iterator, Iterator_Interface *act_iterator);
	~Mono_Act_Iterator();
	void IteratorReset();
	void IteratorNext();
	bool IteratorIsEnd();
};

class Simple_Iterator : public Iterator_Interface
{
public:
	Simple_Iterator(Iterator_Interface * first_it, Iterator_Interface *second_it);
	virtual void IteratorReset();
	virtual void IteratorNext();
	virtual bool IteratorIsEnd();
};

const double PRINT_EPSILON = 1.0e-10;


class Environment
{
private:
	double current_time;
	double last_print_time;
	unsigned long current_step;

	TRanrotWGenerator randomGen;


	/////////////////////////////////////////////////////////////////
	// GLOBAL STRUCTURE OF THE ENVIRONMENT
	/////////////////////////////////////////////////////////////////

	// this map contains the rate of all the free names of
	// of the system
	map<string,double> rate_map;

	// type map and affinity matrix
	Type_Affinity affinity;   

	// dependency graph on state variables 
	DependencyGraph<StateVar*> var_dependency_graph;

	// this list contains all the entity of the system
	vector<Entity *> entity_list;
	vector<Complex *> complex_list;
	list<StateVar *> statevar_list;

	//list<Bimolecular *> bim_list;

	typedef hash_map<reaction_key, int, reaction_hash_compare> ReactionMap;
	ReactionMap reaction_map;

	DependencyMap<Entity*> ent_map;
	DependencyMap<StateVar*> var_map;
	ActMap act_map;
	InfActMap inf_act_map;
	list<Map_Elem *> amb_inf;

	// Complex Map and Complex Relations
	Complex_Rel* comp_rel;
	Complex_Map* comp_map;

	UIBind_map* ui_bind_map;

	SymbolTable* symbol_table;

	int BIND_NUMBER;

	// Private functions
	//bool SetSignature(const string& signature);
	//void GetSignature(string& signature);

	bool SetSignature(const TS_Signature& signature);
	TS_Signature *GetSignature();

	string ReactionKeyToString(reaction_key key);
	bool IsEvent(ReactionType rt);

	//void UpdateStructures(reaction_key *reaction, hash_map<reaction_key, list<double>*, reaction_hash_compare>  *reactions, list<int> &molecules_list,	hash_map<int, bool> &molecules_map, double rate);
	//void FindPossibileBimoleculars(int molecule_id1, int molecule_id2, hash_map<int, list<Element *> *> &actions_entities, list<pair<bool,  Element *> > result[2]);

public:
	RunningMode::RunningModeType RMode;

	Environment(unsigned int seed)
		: current_time(0), last_print_time(0), current_step(1), randomGen(seed), act_map(ent_map, var_map),
		BIND_NUMBER(0), STEP(0), DELTA(0.0), E_TIME(HUGE_VAL)
	{ 
		symbol_table = new SymbolTable(this);
		comp_rel = new Complex_Rel(this);
		comp_map = new Complex_Map(this);
		ui_bind_map = new UIBind_map(this);
		act_map.InitGDManager(this);
		// I add a zero rate for the fictitious name used
		// in the empty output
		this->rate_map[EMPTY_OUTPUT] = 0.0;

		MODE = M_NONE;
		FLAG_FIXED_DELTA = false;
		FLAG_COMPLEXES_REACTION = false;
		FLAG_COMPLEXES_ELIMINATION = false;
		FLAG_COMPLEXES_SIGNATURE = false;
		FLAG_SINGLE_SELF_ASSEMBLY = false;
		countdown = 0;
	}

	~Environment()
	{
		delete symbol_table;
		symbol_table = NULL;

		delete comp_rel;
		comp_rel = NULL;

		delete comp_map;
		comp_map = NULL;

		delete ui_bind_map;
		ui_bind_map = NULL;
	}

	bool CheckTSGeneration();
	void Execute(FILE* file, ofstream *fstr_spec, ofstream *fstr_spec2, FILE* file_c, FILE* file_v, FILE *fstr_tmp);
	void ExecuteStep(vector<Map_Elem *>& elem_list, Action& action, Iterator_Interface *c_iter, reaction_key *key);
	bool GenerateTS(Transition_System *TS, ofstream *fstr_spec);
	//hash_map<reaction_key, list<double>*, reaction_hash_compare> * GenerateMatrix2(list<pair<int, int> > &molecule_in_run);
	//string ReactionKeyToString(reaction_key key);

	///////////////////////////////////////////////////////
	//Random number functions
	///////////////////////////////////////////////////////
	double Random0Upto1() { return (double)randomGen.Random(); }
	double Random0To1() 
	{
#ifdef _USE_C_RAND
		return (double)rand() / ((double)(RAND_MAX));
#else
		uint32 z = randomGen.BRandom();
		return (double)z * (1.0/((double)(uint32)(-1L)));
#endif
	}

	int Random0UptoN(int n)
	{
#ifdef _USE_C_RAND
		return (int)(((double)rand()/((double)(RAND_MAX)+(double)(1)) )*(double)(n));
#else
		uint32 z = randomGen.BRandom();      
		return (int)((double)z / ((double)(uint32)(-1L)+1.0) * (double)n);
#endif
	}

	int Random0ToN(int n)
	{
		uint32 z = randomGen.BRandom();
		return (int)((double)z / ((double)(uint32)(-1L)) * (double)n);
	}

	///////////////////////////////////////////////////////
	//Getters for environment state and maps
	///////////////////////////////////////////////////////
	double GetCurrentTime() { return current_time; }
	unsigned long GetCurrentStep() { return current_step; }

	int GetActualStep() { return actual_step; }

	SymbolTable* GetST() { return symbol_table; }

	UIBind_map* GetUIBind_map() { return ui_bind_map; }

	Complex_Map* GetComplexMap() { return comp_map; }

	Complex_Rel* GetComplexRel() { return comp_rel; }

	vector<Entity *>* GetEntityList() { return &entity_list; }

	vector<Complex *>* GetComplexList() { return &complex_list; }

	ActMap* GetActMap() { return &act_map; }

	InfActMap* GetInfActMap() { return &inf_act_map; }

	DependencyMap<Entity*>* GetEntityMap() { return &ent_map; }
	DependencyMap<StateVar*>* GetStatevarMap() { return &var_map; }

	void GetDependencies(const std::vector<Entity*>& from_entities, std::vector<Map_Elem*>& to_elements);
	void GetDependencies(const std::vector<StateVar*>& from_vars, std::vector<Map_Elem*>& to_elements);   

	void GetDependencies(Entity* from_entity, std::vector<Map_Elem*>& to_elements);
	void GetDependencies(StateVar* from_var, std::vector<Map_Elem*>& to_elements);

	void AddDependency(Map_Elem* elem, std::vector<Map_Elem*>& to_elements)
	{
		if (elem != NULL && 
			elem->action->GetRate(NULL) != HUGE_VAL && 
			elem->simulation_step != current_step)
		{
			elem->simulation_step = current_step;
			to_elements.push_back(elem);
		}
	}

	void GetDependencies(UIBind_elem* ui_elem, std::vector<Map_Elem*>& to_elements)
	{
		AddDependency(ui_elem->unbind, to_elements);
		AddDependency(ui_elem->inter1, to_elements);
		AddDependency(ui_elem->inter2, to_elements);
	}

	Type_Affinity* GetTypeAffinity() { return &affinity; }

	int GetNextBindNumber() { return BIND_NUMBER++; }

	map<string,double>* GetRateMap() { return &rate_map; } 

	///////////////////////////////////////////////////////
	// Simulation duration variables and flags
	///////////////////////////////////////////////////////

	// Continue simulation? (interactive command line)
	bool FLAG_CONTINUE;  
	bool FLAG_COMPLEX_STRUCTURE;
	bool FLAG_FIXED_DELTA;

	// modalita' di simulazione 
	SimMode MODE;

	// step number
	int STEP;
	int actual_step;

	// time length of the single step
	double DELTA;

	// end time of the simulation
	double E_TIME;

	// options for complexes managment
	bool FLAG_COMPLEXES_REACTION;
	bool FLAG_COMPLEXES_SIGNATURE;
	bool FLAG_COMPLEXES_ELIMINATION;
	bool FLAG_SINGLE_SELF_ASSEMBLY;
	int countdown;

	///////////////////////////////////////////////////////
	// Ambient modification
	///////////////////////////////////////////////////////
	pair<Entity *,Entity *> RedBim(Bimolecular *elem, string mod, string *name, Iterator_Interface *c_iter); 

	//
	pair<Entity *,Entity *> RedBimBind(Bimolecular_Bind *elem, string mod, string *name, Iterator_Interface *c_iter); 

	//
	void AddReaction(reaction_key *key); 

	//
	void Destroy_Env();

	//
	void Print();

	//
	void PrintState(ofstream *fstr, double time);

	//
	void PrintFinalComplexesStructure(); 

	//
	void PrintState(FILE* file_e, FILE* file_c, FILE* file_v, set<int> &observed_complexes, double time);

	//
	void PrintAndDeleteComplexes(FILE* file_tmp, set<int> &observed_complexes);

	//
	void PrintFile(ofstream *fstr);
	void PrintFile2(ofstream *fstr);
	void PrintFile2Helper(ofstream *fstr, ChainEntry* chain, int& indent, int lastLevel, std::string& label);
	void PrintFile2HelperE(ofstream* fstr, int start, map<int, set<int> >& successors, std::list<int>& label, map<int, std::string>& groups);
	void PrintFile2HelperC(ofstream* fstr, int start, map<int, set<int> >& successors, std::list<int>& label, map<int, std::string>& groups);

	//	
	void AddRate(string key,double rate);

	// param1, param2 and parameters added for general distributions implementation
	bool AddEvent(EventReaction* new_event,double param1,double param2,vector< pair<double,double> > parameters);
	bool AddEvent(EventReaction* new_event);

	//
	list<EventReaction*> GetEventList(int e_id);

	//
	Entity *AddEntity(list<BBinders *> *p_bb_list, PP_Node *p_pproc, int number, int e_id);
	Entity *AddEntity(list<BBinders *> *p_bb_list, PP_Node *p_pproc, int number, int e_id, bool* is_new);

	//
	void AddStatevar(StateVar* new_statevar);

	//
	Iterator_Interface *GetMonoActIterator(Entity *ent);
	Iterator_Interface *GetMonoInfActIterator(Entity *ent);

	Entity* GetEntityFromId(int id);
	Complex* GetComplexFromId(int id);

	string ReactionKeyToString(reaction_key key, const std::map<int, string>& entityGroups, const std::map<int, string>& complexGroups);

	bool IsSBMLCompliant();

	// Verifies if an isomorphic complex is present in the list of complexes
	Complex *ComplexInList(Complex *elem, int id)
	{
		bool val;
		vector<Complex *>::iterator it = complex_list.begin();
		while( it != complex_list.end() )
		{
			if ( (*it)->GetComplex() == NULL )
			{
				it++;
			}
			else if ( this->FLAG_COMPLEXES_ELIMINATION && (*it)->IsToEliminate() )
			{
				it++;
			}
			else if ( (*it)->GetComplex()->GetNodes() != elem->GetComplex()->GetNodes() )
			{
				it++;
			}
			else if ( (*it)->GetComplex()->GetEdges() != elem->GetComplex()->GetEdges() )
			{
				it++;
			}
			else
			{
				val = (*it)->GetComplex()->Isomorphism(elem->GetComplex());
				(*it)->GetComplex()->Clean();
				elem->GetComplex()->Clean();
				if ( val ) {
					(*it)->AddCounter(elem->GetNumber());
					return (*it);
				} 
				it++;
			}
		}
		if (id == EMPTY_ID)
			elem->SetId(symbol_table->NewMolID());
		else 
			elem->SetId(id);
		complex_list.push_back(elem);
		return NULL;
	}

	Complex *InsertComplex(Complex *c, string &key, int id)
	{
		if (!this->FLAG_COMPLEXES_SIGNATURE)
		{
			return comp_map->Insert(c,key,id);
		}
		else
		{
			return ComplexInList(c,id);
		}
	}
};


#endif //ENVIRONMENT_H_INCLUDED

