

#include "Error_Manager.h"
#include <stdio.h>

set<string> Error_Manager::firedWarnings;
set<string> Error_Manager::firedErrors;


void Error_Manager::PrintError(const Pos& pos, int type, string message) 
{
   if (pos.Line >= 0) 
      cout << pos.Filename << "(" << pos.Line << ", " << pos.Col <<") : error C" << type << ": " << message << endl;
   else
      cout << ">> error C" << type << " " << message << endl;
}

void Error_Manager::PrintErrorOnce(const Pos& pos, int type, string message) 
{
   if (firedErrors.find(message) == firedErrors.end())
   {   
      if (pos.Line >= 0) 
         cout << pos.Filename << "(" << pos.Line << ", " << pos.Col <<") : error C" << type << ": " << message << endl;
      else
         cout << ">> error C" << type << " " << message << endl;

         firedErrors.insert(message);
   }
}

void Error_Manager::PrintErrorOnce(int type, string message) 
{  
   if (firedErrors.find(message) == firedErrors.end())
   {
      cout << ">> error C" << type << ": " << message << endl;
      firedErrors.insert(message);
   }
}

void Error_Manager::PrintError(int type, string message) 
{  
   cout << ">> error C" << type << ": " << message << endl;
}

void CDECL Error_Manager::PrintError(const Pos& pos, char* errorstring, ...) 
{
   static char errmsg[10000];
   va_list args;

   va_start(args, errorstring);
   vsprintf(errmsg, errorstring, args);
   va_end(args);

   fprintf(stdout, "%s(%ld, %ld) : error: %s\n", pos.Filename.c_str(), pos.Line, pos.Col, errmsg);
}


void Error_Manager::PrintWarningOnce(int type, string message) 
{  
   if (firedWarnings.find(message) == firedWarnings.end())
   {
      cout << ">> warning C" << type << ": " << message << endl;
      firedWarnings.insert(message);
   }
}

void Error_Manager::PrintWarning(int type, string message) 
{  
   cout << ">> warning C" << type << ": " << message << endl;
}

void Error_Manager::PrintWarning(const Pos& pos, int type, string message) 
{
   if (pos.Line >= 0) 
      cout << pos.Filename << "(" << pos.Line << ", " << pos.Col <<") : warning C" << type << ": " << message << endl;
   else
      cout << ">> warning C" << type << " " << message << endl;
}

