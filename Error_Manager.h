
#ifndef ERROR_MANAGER_H
#define ERROR_MANAGER_H

#include <cstdlib>
#include <cstring>
#include <string>
#include <iostream>
#include <cstdarg>
#include "Define.h"

#include <set>

using namespace std;

#ifdef _MSC_VER
#define CDECL __cdecl
#else
#define CDECL
#endif

//////////////////////////////////////////////////////
// ERROR_MANAGER CLASS
//////////////////////////////////////////////////////

class Error_Manager
{
private:
   static set<string> firedWarnings;
   static set<string> firedErrors;


public:
   static void PrintError(const Pos& pos, int type, string message);
   static void PrintError(int type, string message);
   static void PrintErrorOnce(int type, string message);
   static void PrintErrorOnce(const Pos& pos, int type, string message);

   static void CDECL PrintError(const Pos& pos, char* errorstring, ...);

   static void PrintWarningOnce(int type, string message);
   static void PrintWarning(int type, string message);
   static void PrintWarning(const Pos& pos, int type, string message);
};

#endif

