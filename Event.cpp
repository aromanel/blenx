
#include <iostream>
#include "Event.h"


using std::cout;
using std::endl;

//
// Event main class
//
Entity* EventReaction::FindEntity(const std::string &id)
{
   /*
   std::vector<Entity*>::iterator it;
   for (it = OriginatingEntities.begin(); it != OriginatingEntities.end(); ++it)
   {
   if (env->GetST()->GetEntNameFromID((**it).GetId()) == id)
   return (*it);
   }
   return NULL;
   */

   ST_Beta_Proc* st_node = (ST_Beta_Proc*)env->GetST()->Find(id, BPROC);
   if (st_node == NULL)
      return NULL;

   //if present, we MUST have already the entity
   Entity* entity = st_node->GetEntity();
   assert (entity != NULL);
   return entity;
}

double EventReaction::GetTotal()
{
   // This function returns the number of all possible combinations
   // of species

   if (this->cond == NULL)
   {
      // This event don't have conditions, so compute total in the standard way.
      if (funRate != NULL)
         return ( funRate->ActionNumber() );
      else
      {
         // compute combinations
         double total = 1.0;

         switch (OriginatingEntities.size())
         {
         case 1:
            total = (double)(GetFirstSourceEntity()->GetCounter());
            break;
         case 2:
            {
               if (OriginatingEntities[0]->GetId() == OriginatingEntities[1]->GetId())
               {
                  total = ( 0.5 * (double)OriginatingEntities[0]->GetCounter() * 
                     ((double)OriginatingEntities[0]->GetCounter() - 1.0) );
               }
               else
               {
                  total = (double)(OriginatingEntities[0]->GetCounter()) * (double)(OriginatingEntities[1]->GetCounter());
               }
            }
            break;
         default:
            assert(false);
            break;
         }

         return total;
      }      
   }
   else 
   {
      // This event has some conditions. 
      // The event fires only when the conditions apply, so it has little sense to 
      // compute combinations or similar. 
      // Hence, return 1.0
      return 1.0;
   }
}

//
double EventReaction::GetActualRate() 
{
   if (CanFire())
   {
      if (condType == COND_RATE ||
         condType == COND_RATE_FUN)
      {
         return funRate->ActualRate();
      }
      else
      {
         return HUGE_VAL;
      }
   }
   return 0.0;
}

bool EventReaction::OutOfGas()
{
	bool res = false;
	std::vector<Entity*>::iterator it;
	std::vector<Entity*>::iterator it2;
	/*for (it = OriginatingEntities.begin(); it != OriginatingEntities.end() && !res; ++it)
	{
		
		if ((**it).GetCounter() < 1)
		{
			res = true;
		}
		else
		{
			(**it).DecCounter(1);
		}
	}*/
    it = OriginatingEntities.begin();
	while(it != OriginatingEntities.end() && !res)
	{
		if ((**it).GetCounter() < 1)
		{
			res = true;
		}
		else
		{
			(**it).DecCounter(1);
			it++;
		}
	}

	if (it == OriginatingEntities.begin())
	{
		return res;
	}

	for (it2 = OriginatingEntities.begin(); it2 != it; ++it2)
	{
		(**it2).AddCounter(1);
	}

	return res;
}

double EventReaction::Time()
{
   if (CanFire())
   {
      if (condType == COND_RATE ||
         condType == COND_RATE_FUN)      
      {   
         double actual_rate = funRate->ActualRate();

         if ( actual_rate < 0.0 && funRate->GetType() == RATE_GENERIC )
         {
            Error_Manager::PrintErrorOnce(funRate->GetPos(), 61, "function '" + funRate->GetId() + "' returned a value < 0, and negative rates are not allowed.");
            return HUGE_VAL;
         }

         if ( actual_rate == 0.0 )
            return HUGE_VAL;

         //first, check if we have something!
         if (OutOfGas() && GetEventVerbType() != VERB_NEW)
         {
            //not 0.0, and out of gas: warning
            Error_Manager::PrintWarningOnce(23, "The event tried to schedule itself (actual_rate != 0) even if some originating entities are = 0; check the '" 
               + funRate->GetId() + "' rate function!");
            return HUGE_VAL;
         }

         if ( actual_rate == HUGE_VAL) 
            return -1.0;
         double rn = env->Random0To1();

         if (rn == 0.0)
            return HUGE_VAL;

         double time = ( 1.0 / actual_rate ) * log( 1.0 / rn ); 
         assert(time >= 0.0);        

         return time;
      }
      else
      {
         //it can fire, and have no rate: do it!
         return -1.0;
      }
   }
   return HUGE_VAL;
}


double EventReaction::GetRate(Iterator_Interface *c_iter)
{
   // In this case the function Rate() is equal to the function ActualRate()
   // When we will in future to add also Events on complexed entities
   // than we will have change the definition of this functions
   return GetActualRate();
}

double EventReaction::GetRate()
{
   switch (condType)
   {
   case COND_RATE:
   case COND_RATE_FUN:
      {
         return funRate->Rate(NULL);         
      }
   case COND_RATE_NORMAL:
      {
         return funRate->Rate(NULL);         
      }
  case COND_RATE_GAMMA:
      {
         return funRate->Rate(NULL);         
      }
  case COND_RATE_HYPEXP:
      {
         return funRate->Rate(NULL);         
      }
   default:
   // function will return HUGE_VAL
   return HUGE_VAL;
   }

}

Expr* EventReaction::GetExpr(Iterator_Interface *c_iter)
{
   vector<Entity *>	reactants = this->GetSourceEntities();
   int mol1 = 0;
   int mol2 = 0;

	mol1 = reactants[0]->GetId();
	if (reactants.size() == 2){
		mol2 = reactants[1]->GetId();
	}
	return funRate->GetExpr(mol1, mol2, c_iter);
}

void EventReaction::GetAllModified(vector<Map_Elem*>& elem_list)
{
   env->GetDependencies(verb->GetStateVars(), elem_list);
   env->GetDependencies(verb->GetAffectedEntityList(), elem_list);
   env->GetDependencies(OriginatingEntities, elem_list);      
}

void EventReaction::GetAllDependencies(vector<Map_Elem*>& elem_list)
{
   if (funRate != NULL)
   {
      env->GetDependencies(funRate->GetEntityList(), elem_list);
      env->GetDependencies(funRate->GetStateVarList(), elem_list);
   }  
   env->GetDependencies(var_list, elem_list);
}

void EventReaction::Print()
{
}

bool EventReaction::IsInheritable(const string& binding_type)
{
   if (inheritable && (condType == COND_RATE || condType == COND_RATE_FUN))
   {
      switch (verb->verbType)
      {
      case VERB_SPLIT:
         {  
            // when it has the right binders, let "inherit"
            if (BBinders::IsOkToInheritSplit(binding_type, GetFirstSourceEntity(), verb->AffectedEntities))
               return true;
         }
         break;
      case VERB_JOIN:
         {
            // same for join
            if (BBinders::IsOkToInheritJoin(binding_type, OriginatingEntities, GetFirstAffectedEntity()))
               return true;
         }
         break;
      default:
         break;
      }
   }

   // COND_COUNT:
   // for counted entities it does not make sense to inherit it:
   // they are generated by that counter 
   // VERB_DELETE
   // do not delete bounded entities, so do not inherit
   return false;
}

void EventReaction::GetReactants(vector<Entity *> &reactants){
	reactants.assign(OriginatingEntities.begin(), OriginatingEntities.end());
}

void EventReaction::GetIterator(pair<Entity *, Entity *> *ent_pair, list<Complex *> *comp, pair<Iterator_Interface *, Iterator_Interface *> *it_pair){
	//This function is called for Element that are associated to entities that
	//belong to complexes. An entity that belongs to a complex can't have
	//associated an Element of type EVENT
	assert(0);	
}

Iterator_Interface * EventReaction::GetIterator(Entity *e1, Entity *e2, list<Complex *> *c1, list<Complex *> *c2, TwoIteratorKind iterator_kind){
	//Events doesn't need an iterator
	return NULL;
}

