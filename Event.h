#ifndef EVENT_H_INCLUDED
#define EVENT_H_INCLUDED

#include <cstdlib>
#include <string>
#include <list>
#include <vector>
#include <map>
#include <cmath>
#include "Entity.h"
#include "Define.h"
#include "Environment.h"
#include "FunStd.h"
#include "EventCond.h"
#include "EventVerb.h"

//////////////////////////////////////////////////////
// EVENT CLASS
// This class represents an action of type EVENT.
//////////////////////////////////////////////////////
class EventReaction : public Element 
{
private:
   bool inheritable;
   bool alreadyFired;

   EventCond* cond;
   EventVerb* verb;

   CondType condType;
   std::vector<Entity*> cond_entities;
   std::vector<StateVar*> var_list;

public:
   FunBase *funRate;	
   std::vector<Entity*> OriginatingEntities;  

public:
   EventReaction(Environment* p_env, 
      EventCond* p_cond,
      CondType p_condType,
      EventVerb* p_verb,
      FunBase *p_funRate,
      bool inherit,
      std::list<Entity*>& originating_entities,
      std::list< std::pair<Entity*, int> >& affected_entities)
      : Element(p_env, EVENT)
   {
      //COND_RATE, COND_RATE_FUN -> p_funRate != NULL
      //With general distributions this assert is no more valid
	  //assert (!(p_condType == COND_RATE || p_condType == COND_RATE_FUN) || (p_funRate != NULL));		
      assert (p_verb != NULL);

      cond = p_cond;
      condType = p_condType;
      verb = p_verb;
      funRate = p_funRate;

      inheritable = inherit;
      alreadyFired = false;

      std::list<Entity*>::iterator o_it;
      for (o_it = originating_entities.begin(); o_it != originating_entities.end(); ++o_it)
         OriginatingEntities.push_back(*o_it);

      std::list<std::pair<Entity*, int> >::const_iterator it;
      for (it = affected_entities.begin(); it != affected_entities.end(); ++it)
      {
         verb->AffectedEntities.push_back(it->first);
         verb->AffectedEntitiesNumber.push_back(it->second);
      }
   }	

   EventReaction(const EventReaction& other)
      : Element(other.env, EVENT),
      cond_entities(other.cond_entities),
      var_list(other.var_list),
      OriginatingEntities(other.OriginatingEntities)
   {
      //simple copy
      this->verb = other.verb->Clone();

      if (other.cond == NULL)
         this->cond = NULL;
      else
         this->cond = other.cond->Clone();

      if (other.funRate->IsManagedByMap())
         this->funRate = other.funRate;
      else
         this->funRate = other.funRate->Clone();

      this->inheritable = other.inheritable;
      this->alreadyFired = false;
   }

   virtual ~EventReaction() 
   {
      if (cond != NULL) 
         delete cond;

      delete verb;

      if (!funRate->IsManagedByMap())
         delete funRate;
   }

   //
   // Functions for EventCond or EventVerb support
   //

   // Find an entity, checking that it's between our originating ones
   Entity* FindEntity(const std::string& id);

   void SetFired(EventVerb* verb) 
   {
      this->alreadyFired = true;
      if (cond != NULL)
         cond->NotifyFired();
   }

   bool HasFired() { return alreadyFired; }

   //
   // EventReaction implementation
   //
   bool Init() 
   {
      if (cond != NULL)
      {
         if (!cond->Init(*this))
            return false;
         cond->GetEntityList(cond_entities);
         cond->GetVarList(var_list);
      }
      return true;
   }


   //condition is ok, get rate!
   bool CanFire() 
   {
      if (cond == NULL) 
         return true;
      else
         return cond->CanFire(*this); 
   }

   //invokes the correct verb (performing the event)   
   void DoEvent() { verb->DoEvent(*this); }

   //
   // Element implementation
   //

   // returns the number of actions of this type. 
	double GetTotal();
	bool IsDiscrete()
	{
		if ( cond == NULL )
		{
			return ( GetTotal() > 0 );
		}
		else
		{
			bool res = this->cond->IsDiscrete(); 
			return res;
		}
	}

	// calculates and returns the actual time of the action. The calculation is carried out
	// considering the actual information of the involved entities.
	virtual double Time();
	virtual double GetActualRate();
	virtual double GetBasalRate(Iterator_Interface *c_iter) { return funRate->GetConstant(); };

	virtual bool Is(Element *elem) { return (elem == this); }
	// print the information of the action
	virtual void Print();

	bool OutOfGas();

   //
   // EventReaction accessors
   //
   const std::vector<Entity*>& GetSourceEntities() const { return OriginatingEntities; };
   const std::vector<Entity*>& GetTargetEntities() const { return verb->GetAffectedEntityList(); };
   const std::vector<Entity*>& GetCondEntities() const { return cond_entities; } 

   double GetRate(Iterator_Interface *c_iter);
   double GetRate();
	Expr* GetExpr(Iterator_Interface *c_iter);

   void GetAllModified(vector<Map_Elem*>& modify);
   void GetAllDependencies(vector<Map_Elem*>& modify);

   Entity* GetFirstSourceEntity() 
   {        
      if (OriginatingEntities.size() < 1)
         return NULL;
      else
         return OriginatingEntities[0]; 
   }

   Entity* GetSecondSourceEntity() 
   { 
      if (OriginatingEntities.size() < 2)
         return NULL;
      else
         return OriginatingEntities[1]; 
   }

   EventVerb* GetEventVerb() { return verb; }
   EventCond* GetEventCond() { return cond; }

   VerbType GetEventVerbType() { return verb->verbType; }
   CondType GetEventCondType() { return condType; }

   Entity* GetFirstAffectedEntity() { if (verb->AffectedEntities.size() > 0){
													return verb->AffectedEntities[0];
												 } else {
													return this->OriginatingEntities[0];
												  }
												}
   Entity* GetSecondAffectedEntity() { return verb->AffectedEntities[1]; }
   int GetFirstCardinality() { 
		if (verb->AffectedEntities.size() > 0){
			return verb->AffectedEntitiesNumber[0];
		} else {
			return 1;
		}
	}
   int GetSecondCardinality() { return verb->AffectedEntitiesNumber[1]; } 

   size_t GetAffectedEntitiesCount() { return verb->GetAffectedEntitiesCount(); }

   bool IsInheritable(const string& binding_type);
	void GetReactants(vector<Entity *> &reactants);
	void GetIterator(pair<Entity *, Entity *> *ent_pair, list<Complex *> *comp, pair<Iterator_Interface *, Iterator_Interface *> *it_pair);
	Iterator_Interface * GetIterator(Entity *e1, Entity *e2, list<Complex *> *c1, list<Complex *> *c2, TwoIteratorKind iterator_kind);

	bool IsOnlyMono(int reactants[2]){
		vector<Entity*>::iterator it;
		int i;
		for (it = OriginatingEntities.begin(), i = 0; it != OriginatingEntities.end() && i < 2 ;i++, it++){
		  reactants[i] = (*it)->GetId();
		}

		if (i == 1) {
			reactants[1] = -1;
			return true;
		} else {
			return false;
		}
	}
};

#endif //EVENT_H_INCLUDED

