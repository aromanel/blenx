
#include "EventCond.h"
#include "Event.h"
#include "PT_Event_Node.h"


//
// Event conditions
//
EventCond_AtomStates::EventCond_AtomStates(const Pos& pos, CondType condType, PTNode* state_list_node)
   : EventCond(pos, condType)
{    
   this->condType = condType;

   PTN_EventCond_StateList*  stateList = (PTN_EventCond_StateList*)state_list_node;
   stateList->BuildList(eventStates);

   currentStateIterator = eventStates.begin();
}

bool EventCond_AtomStates::Init(EventReaction& my_event)
{
   std::list<EventState*>::iterator it;
   for (it = eventStates.begin(); it != eventStates.end(); ++it)
   {
      EventState* eventState = *it;
      // check id! 

      eventState->SetEntity(my_event.FindEntity(eventState->name));
      if (eventState->GetEntity() == NULL)
      {         
         //try a state var
         ST_Statevar* stateVar = (ST_Statevar*)my_event.GetEnv()->GetST()->Find(eventState->name, STATE_VAR);
         if (stateVar == NULL)
         {        
            Error_Manager::PrintError(pos, 41, "the entity '" + eventState->name +  "' is not defined.");
            return false;
         }
         else
         {
            eventState->SetStatevar(stateVar->GetStateVar());
         }
      }
   }
   return true;
}

bool EventCond_AtomStates::CanFire(EventReaction& my_event)
{
   //check if current state is fulfilled
   if (currentStateIterator == eventStates.end())
      return true;

   EventState* eventState = *currentStateIterator;

   bool doTransition = false;

   switch (eventState->stateType)
   {
   case STATE_UP:
      if (eventState->GetCurrentValue() > eventState->value)
         doTransition = true;
      break;

   case STATE_DOWN:
      if (eventState->GetCurrentValue() < eventState->value)
         doTransition = true;
      break;
   }

   if (doTransition)
      ++currentStateIterator;

   if (currentStateIterator == eventStates.end())
      return true;

   return false;
}

void EventCond_AtomStates::NotifyFired()
{
   assert (currentStateIterator == eventStates.end());
   //reset it
   currentStateIterator = eventStates.begin();
}

bool EventCond_AtomCount::Init(EventReaction& my_event)
{
   entityRef = my_event.FindEntity(entityName);
   if (entityRef == NULL)
   {
      Error_Manager::PrintError(pos, 41, "the entity '" + entityName +  "' is not defined.");
      return false;
   }
   return true;
}

bool EventCond_AtomCount::CanFire(EventReaction& my_event)
{
   switch (condType)
   {
   case COND_COUNT_EQUAL:
      {
         return (entityRef->GetCounter() == entityValue);
      }
   case COND_COUNT_NEQUAL:
      {
         return (entityRef->GetCounter() != entityValue);
      }
   case COND_COUNT_LESS:
      {
         return (entityRef->GetCounter() < entityValue);
      }
   case COND_COUNT_GREATER:
      {
         return (entityRef->GetCounter() > entityValue);
      }
   default:
      assert(false);
   }
   return false;
}


bool EventCond_AtomDet::CanFire(EventReaction& my_event)
{
   switch (condType)
   {
   case COND_TIME_EQUAL:
      {
         // we will enable or not based on condition         
         if (!my_event.HasFired() && my_event.GetEnv()->GetCurrentTime() >= param)
            return true;   
      }
      break;

   case COND_STEP_EQUAL:
      {
         // we will enable or not based on condition         
         if (!my_event.HasFired() && my_event.GetEnv()->GetActualStep() >= param)
            return true;
		 break;
      }    
   default:
      assert(false);
   }
   return false;
}

 double EventState::GetCurrentValue()
   {
      if (varType == EXPR_STATEVAR)
         return stateVarRef->GetValue();
      else if (varType == EXPR_ENTITY)
         return entityRef->GetCounter();
      else
      {
         assert(false);
         return -1.0;
      }
   }

