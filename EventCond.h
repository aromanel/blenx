
#ifndef EVENT_COND_H_INCLUDED
#define EVENT_COND_H_INCLUDED

#include <list>
#include <string>
#include "Define.h"
#include "Entity.h"
#include "PT_General_Node.h"

class EventReaction;
class EventCond;


struct EventState
{
   double value;
   StateType stateType;
   std::string name;

   ExprVarType varType;

   EventState()
   {
      varType = EXPR_UNDEFINED;
   }

   Entity* GetEntity()
   {
      assert(varType == EXPR_ENTITY);
      return entityRef;
   }

   void SetEntity(Entity* ent)
   {
      varType = EXPR_ENTITY;
      entityRef = ent;
   }

   StateVar* GetStatevar()
   {
     assert(varType == EXPR_STATEVAR);
     return stateVarRef;
   }

   void SetStatevar(StateVar* stateVar)
   {
     varType = EXPR_STATEVAR;
     stateVarRef = stateVar;
   }

   double GetCurrentValue();

private:
   StateVar* stateVarRef;
   Entity* entityRef;
};

//Base class for conditions
class EventCond
{
protected:
   Pos pos;

public:
   CondType condType;
   
public:
   EventCond(const Pos& p_pos, CondType p_type)
      : pos(p_pos) 
   {
      condType = p_type;
   }
   
   virtual ~EventCond() { }

   const Pos& GetPos() { return pos; }

   virtual bool Init(EventReaction& my_event) = 0;
   virtual bool CanFire(EventReaction& my_event) = 0;
   virtual void NotifyFired() { }

   virtual void GetEntityList(std::vector<Entity*>& list) = 0;
   virtual void GetVarList(std::vector<StateVar*>& list) = 0;

   virtual EventCond* Clone() = 0;
   virtual bool IsDiscrete() = 0;

   //size_t GetEntityCount();
};

class EventCond_AtomStates : public EventCond
{
private:
   std::list<EventState*> eventStates;
   std::list<EventState*>::iterator currentStateIterator;

public:

   EventCond_AtomStates(const Pos& pos, CondType condType, PTNode* state_list_node);

   EventCond_AtomStates(const Pos& pos, CondType condType, std::list<EventState*> states)
      : EventCond(pos, condType), eventStates(states)
   { 
      currentStateIterator = eventStates.begin();
   }

   bool Init(EventReaction& my_event);
   bool CanFire(EventReaction& my_event);

   void NotifyFired();

   void GetVarList(std::vector<StateVar*>& list) 
   {
      std::list<EventState*>::iterator it;
      for (it = eventStates.begin(); it != eventStates.end(); ++it)
      {
         if ((**it).varType == EXPR_STATEVAR)
            list.push_back((**it).GetStatevar());
      }
   }

   void GetEntityList(std::vector<Entity*>& list)
   {
      std::list<EventState*>::iterator it;
      for (it = eventStates.begin(); it != eventStates.end(); ++it)
      {
         if ((**it).varType == EXPR_ENTITY)
            list.push_back((**it).GetEntity());
      }
   }

   EventCond* Clone()
   {
      EventCond_AtomStates* ret = new EventCond_AtomStates(this->pos,
         this->condType, this->eventStates);
      return ret;
   }

   bool IsDiscrete() { return false; } 
};


class EventCond_AtomCount : public EventCond
{
private:
   std::string entityName;
   Entity* entityRef;
   int entityValue;

   EventCond_AtomCount(const Pos& pos, CondType condType) : EventCond(pos, condType) { }

public:
   EventCond_AtomCount(const Pos& pos, CondType condType, const std::string& id, const std::string& value)
      : EventCond(pos, condType)
   {
      entityName = id;
      entityValue = atoi(value.c_str());
   }

   bool Init(EventReaction& my_event);
   bool CanFire(EventReaction& my_event);
  

   void GetEntityList(std::vector<Entity*>& list) { list.push_back(entityRef); }
   void GetVarList(std::vector<StateVar*>& list) { }

   EventCond* Clone()
   {
      EventCond_AtomCount* ret = new EventCond_AtomCount(pos, condType);

      ret->entityRef = this->entityRef;
      ret->entityValue = this->entityValue;

      return ret;
   }

   bool IsDiscrete() { return true; }

};

class EventCond_AtomDet : public EventCond
{
private:
   double param;
   EventCond_AtomDet(const Pos& pos, CondType condType) : EventCond(pos, condType) { }

public:
   EventCond_AtomDet(const Pos& pos, CondType condType, const std::string& value)
      : EventCond(pos, condType)
   {
      param = atof(value.c_str());
   }

   bool Init(EventReaction& my_event) { return true; }
   bool CanFire(EventReaction& my_event);

   void GetEntityList(std::vector<Entity*>& list) { }
   void GetVarList(std::vector<StateVar*>& list) { }

   EventCond* Clone()
   {
      EventCond_AtomDet* ret = new EventCond_AtomDet(pos, condType);

      ret->param = this->param;

      return ret;
   }

   bool IsDiscrete() { return false; }
};

class EventCond_And : public EventCond
{
private:
   EventCond* left;
   EventCond* right;

public:
   EventCond_And(const Pos& pos, EventCond* p_left, EventCond* p_right)
      : EventCond(pos, COND_EXPRESSION)
   {
      left = p_left;
      right = p_right;
   }

   bool Init(EventReaction& my_event) 
   { 
      return (left->Init(my_event) && right->Init(my_event));
   }

   bool CanFire(EventReaction& my_event)
   { 
      return (left->CanFire(my_event) && right->CanFire(my_event));
   }

   void GetEntityList(std::vector<Entity*>& list) 
   { 
      left->GetEntityList(list);
      right->GetEntityList(list);
   }
   void GetVarList(std::vector<StateVar*>& list) 
   {
      left->GetVarList(list);
      right->GetVarList(list);
   }

   EventCond* Clone()
   {
      EventCond_And* ret = new EventCond_And(pos, left->Clone(), right->Clone());
      return ret;
   }

   bool IsDiscrete() { return ( right->IsDiscrete() && left->IsDiscrete() ) ; }
};

class EventCond_Or : public EventCond
{
private:
   EventCond* left;
   EventCond* right;

public:
   EventCond_Or(const Pos& pos, EventCond* p_left, EventCond* p_right)
      : EventCond(pos, COND_EXPRESSION)
   {
      left = p_left;
      right = p_right;
   }

   bool Init(EventReaction& my_event) 
   { 
      return (left->Init(my_event) && right->Init(my_event));
   }

   bool CanFire(EventReaction& my_event)
   { 
      return (left->CanFire(my_event) || right->CanFire(my_event));
   }

   void GetEntityList(std::vector<Entity*>& list) 
   { 
      left->GetEntityList(list);
      right->GetEntityList(list);
   }
   void GetVarList(std::vector<StateVar*>& list) 
   {
      left->GetVarList(list);
      right->GetVarList(list);
   }

   EventCond* Clone()
   {
      EventCond_Or* ret = new EventCond_Or(pos, left->Clone(), right->Clone());
      return ret;
   }

    bool IsDiscrete() { return ( right->IsDiscrete() && left->IsDiscrete() ) ; }
};

class EventCond_Not : public EventCond
{
private:
   EventCond* cond;
   
public:
   EventCond_Not(const Pos& pos, EventCond* p_cond)
      : EventCond(pos, COND_EXPRESSION)
   {
      cond = p_cond;
   }

   bool Init(EventReaction& my_event) 
   { 
      return (cond->Init(my_event));
   }

   bool CanFire(EventReaction& my_event)
   { 
      return (cond->CanFire(my_event));
   }

   void GetEntityList(std::vector<Entity*>& list)
   { 
      cond->GetEntityList(list);
   }
   void GetVarList(std::vector<StateVar*>& list) 
   {
      cond->GetVarList(list);
   }
   
   EventCond* Clone()
   {
      EventCond_Not* ret = new EventCond_Not(pos, cond->Clone());
      return ret;
   }

    bool IsDiscrete() { return ( cond->IsDiscrete() ) ; }
};



#endif //EVENT_COND_H_INCLUDED

