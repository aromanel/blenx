
#include "EventVerb.h"
#include "Event.h"

static std::vector<StateVar*> emptyStateVarVector;

void EntityVerb::DoEvent(EventReaction& my_event)
{
   bool firedCompletely = true;
   switch (verbType)
   {
   case VERB_SPLIT:
      {
         for (size_t i = 0; i < AffectedEntities.size(); ++i)
         {
            if (AffectedEntities[i] != NULL)
            AffectedEntities[i]->AddCounter(AffectedEntitiesNumber[i]);
         }
         my_event.GetFirstSourceEntity()->DecCounter();
      }
      break;

   case VERB_NEW:
      {
         if (AffectedEntities.size() == 0)
         {
            for (size_t i = 0; i < my_event.OriginatingEntities.size(); ++i)
            {
				my_event.OriginatingEntities[i]->AddCounter(1);
            }
         }
         else
         {
            for (size_t i = 0; i < AffectedEntities.size(); ++i)
            {
               if (AffectedEntities[i] != NULL)
               AffectedEntities[i]->AddCounter(AffectedEntitiesNumber[i]);
            }
         }
      }
      break;

   case VERB_DELETE:
      {        
         if (AffectedEntities.size() == 0)
         {
            for (size_t i = 0; i < my_event.OriginatingEntities.size(); ++i)
            {
               my_event.OriginatingEntities[i]->DecCounter();
            }
         }
         else
         {
            for (size_t i = 0; i < AffectedEntities.size(); ++i)
            {
               int n = AffectedEntities[i]->DecCounter(AffectedEntitiesNumber[i]);
               alreadyFiredNum = alreadyFiredNum + n;
               if (AffectedEntitiesNumber[i] == alreadyFiredNum)
               {
                  firedCompletely = true;
                  alreadyFiredNum = 0;
               }
               else
               {
                  firedCompletely = false;
               }
            }
         }
      }
      break;

   case VERB_JOIN:
      {
         for (size_t i = 0; i < AffectedEntities.size(); ++i)
         {
            if (AffectedEntities[i] != NULL)
            AffectedEntities[i]->AddCounter(AffectedEntitiesNumber[i]);
         }
         vector<Entity*>::iterator it;
         for (it = my_event.OriginatingEntities.begin(); it != my_event.OriginatingEntities.end(); ++it)
         {
            (*it)->DecCounter();
         }
      }
      break;

   case VERB_UPDATE:
      //TODO! generic conditions also for update verb
      assert (false);
      break;
   case VERB_NOTHING:
      assert (false);
      break;
   }

   // For timed events, set it to done
   if (firedCompletely)
      my_event.SetFired(this);
}

void StateVarVerb::DoEvent(EventReaction& my_event)
{
   stateVar->SetValue(funUpdate->ActualRate());
   // For timed events, set it to done
   my_event.SetFired(this);
}

const std::vector<StateVar*>& EventVerb::GetStateVars()
{
   return emptyStateVarVector;
}


