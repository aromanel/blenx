
#ifndef EVENT_VERB_H_INCLUDED
#define EVENT_VERB_H_INCLUDED

#include <vector>
#include "StateVar.h"
#include "FunBase.h"
#include "Entity.h"
#include "Define.h"

class EventReaction;

class EventVerb
{
public:
   std::vector<Entity*> AffectedEntities;
   std::vector<int> AffectedEntitiesNumber;

   VerbType verbType;

public:
   EventVerb(VerbType p_verbType)
   {
      verbType = p_verbType;
   }
   
   virtual ~EventVerb() { }

   const std::vector<Entity*>& GetAffectedEntityList() { return AffectedEntities; }
   size_t GetAffectedEntitiesCount() { return AffectedEntities.size(); }
   virtual const std::vector<StateVar*>& GetStateVars();


   virtual void DoEvent(EventReaction& my_event) = 0;
   virtual EventVerb* Clone() = 0;
};

class EntityVerb : public EventVerb
{
private:
   int alreadyFiredNum;

public:
   EntityVerb(VerbType p_verbType)
      : EventVerb(p_verbType) { alreadyFiredNum = 0; }

   void DoEvent(EventReaction& my_event);

   EventVerb* Clone()
   {
      return new EntityVerb(verbType);
   }
};


class StateVarVerb : public EventVerb
{
private:
   StateVar* stateVar;
   FunBase* funUpdate;
   std::vector<StateVar*> vect;

public:
   StateVarVerb(StateVar* stateVar, FunBase* funUpdate)
      : EventVerb(VERB_UPDATE)
   {
      SetStateVar(stateVar);
      SetUpdateFun(funUpdate);
   }

   void SetStateVar(StateVar* stateVar)
   {
      assert(stateVar != NULL && verbType == VERB_UPDATE);
      this->stateVar = stateVar;
      vect.push_back(this->stateVar);
   }

   void SetUpdateFun(FunBase* funUpdate)
   {
      assert(funUpdate != NULL && verbType == VERB_UPDATE);
      this->funUpdate = funUpdate;
   }

   void DoEvent(EventReaction& my_event);

   EventVerb* Clone()
   {
      return new StateVarVerb(stateVar, funUpdate);
   }

   const std::vector<StateVar*>& GetStateVars()
   {
      return vect;
   }
};


#endif //EVENT_VERB_H_INCLUDED


