
#include "Expr.h"
#include "Symbol_Table.h"
#include "Entity.h"
#include "StateVar.h"
#include "Environment.h"
#include "Error_Manager.h"

bool ExprConcentration::Check(bool check_def)
{
   if (id == BPROC1)
   {
      return true;
   }
   else if (id == BPROC2)
   {
      return true;
   }
   else
   {
      if (entity == NULL)
      {
         ST_Beta_Proc* entityNode = (ST_Beta_Proc*)symbolTable->Find(id, BPROC);
         if (entityNode == NULL)
         {
            if (check_def){
               Error_Manager::PrintError(position, 45, " the entity '" + id + "' is not defined as a bproc.");	
            }
            return false;
         }

         entity = entityNode->GetEntity();
         assert(entity != NULL);
      }
      return true;
   }
}

double ExprConcentration::Eval()
{
   assert (entity != NULL);
   return entity->GetCounter();
}

void ExprConcentration::GetEntitiesAndVariables(std::set<Entity*>& entities, std::set<StateVar*>& variables)
{
   if (id == BPROC1 || id == BPROC2) return;
   assert(entity != NULL);
   entities.insert(this->entity);
}

ExprVar::ExprVar(const Pos& pos, SymbolTable* symbol_table, const std::string& p_id)
: Expr(pos, symbol_table), id(p_id)
{
   refExpression = NULL;
   stateVar = NULL;
   varType = EXPR_UNDEFINED;
}

void ExprVar::Print(ostream *fstr, PrintMode mode) 
{
   switch(mode){
         case PRINT_PARAM:
         case PRINT_GRAPH:
            (*fstr) << id;
            break;
         case PRINT_SBML:
            Check(false);
            switch(varType){
         case EXPR_FUNCREF:
            refExpression->Print(fstr, PRINT_SBML);
            break;
         case EXPR_STATEVAR:
            //stateVar->GetExpr()->Print(fstr, PRINT_SBML);
            (*fstr) << this->symbolTable->GetVarNameFromID(stateVar->GetId());
            break;
         default:
            (*fstr) << value;
            break;
            }
            break;
   }
}


bool ExprVar::Check(bool check_def)
{
   // check which one is defined
   if (symbolTable->FindConstant(id))
   {
      varType = EXPR_CONSTANT; 
      value = symbolTable->GetConstant(id);
      return true;
   }

   ST_Statevar* st_node = (ST_Statevar*)symbolTable->Find(id, STATE_VAR);
   if (st_node != NULL)
   {
      varType = EXPR_STATEVAR;      
      stateVar = st_node->GetStateVar();
      if (stateVar == NULL)
         return false;
      return stateVar->CheckExpression();
   }

   FunBase* refFunBase = symbolTable->FindFunction(id);
   if (refFunBase != NULL)
   {
      FunGeneric* refFunGeneric = dynamic_cast<FunGeneric*>(refFunBase);
      if (refFunGeneric != NULL)
      {
         varType = EXPR_FUNCREF;
         refExpression = refFunGeneric->GetExpression();
         return refExpression->Check(check_def);
      }
   }   

   //if we arrived here, symbol is not defined
   varType = EXPR_UNDEFINED;
   Error_Manager::PrintError(position, 25, "the symbol '" + id + "' is not defined.");   
   return false;
}

double ExprVar::Eval() 
{ 
   double res = 0.0;
   switch (varType)
   {
   case EXPR_STATEVAR:
      res = stateVar->GetValue();
      break;

   case EXPR_FUNCREF:         
      res = refExpression->Eval(); 
      break;

   case EXPR_CONSTANT:
      res = value;
      break;

   case EXPR_UNDEFINED:
      Error_Manager::PrintWarning(position, 27, "the symbol '" + id + "' is not defined, will have default value of 0");   
      break;

   default:
      assert(false);
      break;
   }
   return res;
}

void ExprVar::GetEntitiesAndVariables(std::set<Entity*>& entities, std::set<StateVar*>& variables) 
{
   switch (varType)
   {
   case EXPR_STATEVAR:
      assert(this->stateVar != NULL);
      variables.insert(this->stateVar);
      break;

   case EXPR_FUNCREF: 
      refExpression->GetEntitiesAndVariables(entities, variables);
      break;
   default:
      break;
   }
}


void ExprVar::GetDependences(set<int> *entities){
   //This is a trick, Expression should be checked when created
   if (!checked){
      this->Check(false);
   }

   if (varType == EXPR_UNDEFINED){
      assert(false);
   }
   if (varType == EXPR_STATEVAR){
      stateVar->GetExpr()->GetDependences(entities);
   } else if (varType == EXPR_FUNCREF){
      refExpression->GetDependences(entities);
   }	
   Expr::GetDependences(entities);
}

void ExprConcentration::GetDependences(set<int> *entities){
   entities->insert(symbolTable->GetEntIDFromName(id));
   Expr::GetDependences(entities);
}

Expr* Expr::Add(Expr *expr){
   Pos pos(0,0,"");
   return new ExprOp(pos, symbolTable, this, expr, EXPR_PLUS);
}


Expr* Expr::DivideBy(int n){
   assert(n != 0);
   if (n == 1){
      return Clone();
   } else {
      Pos pos(0,0,"");
      return new ExprOp(pos, symbolTable, this, new ExprNumber(pos, symbolTable, n), EXPR_DIV);
   }
}

double ExprRand::Eval()
{
   return this->symbolTable->GetEnv()->Random0To1();
}
