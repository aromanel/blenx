
#ifndef EXPR_H_INCLUDED
#define EXPR_H_INCLUDED

#include <vector>
#include <list>
#include <set>
#include <math.h>
#include <fstream>
#include "Define.h"
#include "Error_Manager.h"

class SymbolTable;
class Entity;
class StateVar;

enum ExprVarType
{
   EXPR_CONSTANT,
   EXPR_STATEVAR,
   EXPR_FUNCREF,
   EXPR_ENTITY,
   EXPR_TYPEBOUNDED,
   EXPR_UNDEFINED
};

enum PrintMode{
	PRINT_GRAPH,
	PRINT_SBML,
	PRINT_PARAM
};

//
// Base class for all expression nodes
//////////////////////////////////////////////////////////////

class Expr
{
protected:
   Pos position;
   SymbolTable* symbolTable;
   std::vector<Expr*> children;

   bool checked;

public:
   Expr(const Pos& pos, SymbolTable* symbol_table) 
      : position(pos), symbolTable(symbol_table)
   {
      checked = false;
   }
   
   virtual ~Expr() 
   { 
      //for (size_t i = 0; i < children.size(); ++i)
	    // delete children[i];
   }

   virtual Pos& GetPos() { return position; }
   
   virtual double Eval() = 0;
   virtual bool EvalConst(double& value) = 0;

   virtual bool Check(bool check_def = true)
   {
      if (checked)
         return true;

      checked = true;

      std::vector<Expr*>::iterator it;
      for (it = children.begin(); it != children.end(); ++it)
      {
         if (!(*it)->Check(check_def))
            return false;
      }
      return true;
   }

   virtual void GetEntitiesAndVariables(std::set<Entity*>& entities, std::set<StateVar*>& variables) 
   {
      std::vector<Expr*>::iterator it;
      for (it = children.begin(); it != children.end(); ++it)
      {
         (*it)->GetEntitiesAndVariables(entities, variables);  
      }
   }

   virtual void ReplaceEntities(Entity* e1, Entity* e2) 
   {
      for (size_t i = 0; i < children.size(); ++i)
         children[i]->ReplaceEntities(e1, e2);
   }

   virtual Expr* Clone() = 0;
   virtual void Print(ostream *fstr, PrintMode mode) = 0;

	Expr* Add(Expr *expr);
	Expr* DivideBy(int n);
	virtual void GetDependences(set<int> *entities){
		vector<Expr*>::iterator it;
		for (it = children.begin(); it != children.end(); it++){
			(*it)->GetDependences(entities);
		}
	};
};

//
// Constants and references
//////////////////////////////////////////////////////////////

class ExprNumber : public Expr
{
private:
   double value;

public:
   ExprNumber(const Pos& pos, SymbolTable* symbol_table, double val)
      : Expr(pos, symbol_table)
   {
      value = val;
   }

   virtual double Eval() { return value; }
   virtual bool EvalConst(double& val)
   {
      val = value;
      return true;
   }

   virtual Expr* Clone()
   {
      return new ExprNumber(position, symbolTable, value);
   }

   virtual void Print(ostream *fstr, PrintMode mode) 
   {
		(*fstr) << value;
   }
};

class ExprVar : public Expr
{
private:
   ExprVarType varType;
   Expr* refExpression;
   StateVar* stateVar;
   std::string id;
   double value;

public:
   ExprVar(const Pos& pos, SymbolTable* symbol_table, const std::string& p_id);
   
   virtual double Eval();
   virtual void GetEntitiesAndVariables(std::set<Entity*>& entities, std::set<StateVar*>& variables);
   virtual bool Check(bool check_def = true);

   virtual bool EvalConst(double& val) 
   { 
      // First, initialize this node (we have to find if it is a constant indeed)
      // If not, it will fail (we have a not fully initialize ST yet) but
      // we don't care (we already give an error for the constant not being
      // constant
     
		if (!this->Check())
         return false;

      if (varType == EXPR_CONSTANT)
      {
         val = value;
         return true;
      }
      return false; 
   }

   virtual Expr* Clone()
   {
      return new ExprVar(position, symbolTable, id);
   }

   virtual void Print(ostream *fstr, PrintMode mode);
	void GetDependences(set<int> *entities);
};

class ExprConcentration : public Expr
{
private:
   Entity* entity;
   std::string id;

public:
   ExprConcentration(const Pos& pos, SymbolTable* symbol_table, const std::string& p_id)
      : Expr(pos, symbol_table), id(p_id)
   {
      // DO NOT check if id is defined: it will be defined later!
      entity = NULL;
   }

   virtual double Eval();
   virtual bool Check(bool check_def=true);
   virtual void GetEntitiesAndVariables(std::set<Entity*>& entities, std::set<StateVar*>& variables);

   virtual bool EvalConst(double& val) { return false; }

   virtual void ReplaceEntities(Entity* e1, Entity* e2) 
   {
      if (id == BPROC1)      
         entity = e1;     
      else if (id == BPROC2)
         entity = e2;
   }

   virtual Expr* Clone()
   {
      ExprConcentration* expr = new ExprConcentration(position, symbolTable, id);
      expr->entity = this->entity;
      return expr;
   }

	virtual void Print(ostream *fstr, PrintMode mode) {
		switch(mode){
			case PRINT_PARAM:
				(*fstr) << "|" << id << "|";
				break;
			case PRINT_GRAPH:
				(*fstr) << "|S_" << id << "|";
				break;
			case PRINT_SBML:
				(*fstr) << "S_" << id;
				break;
		}		
	}

	virtual void GetDependences(set<int> *entities);
};

//To be used only in SBML mode
class ExprCompConcentration : public Expr {
	private:
		string id;
	public:
		ExprCompConcentration(const Pos& pos, SymbolTable* symbol_table, const std::string& p_id)
			: Expr(pos, symbol_table), id(p_id){
		}
		
		double Eval(){
			//To be defined if used with mode different than SBML
			assert(false);
			return 0;
		}

		bool EvalConst(double&){
			//To be defined if used with mode different than SBML
			assert(false);
			return true;
		}

		Expr *Clone(){
			return new ExprCompConcentration(position, symbolTable, id);
		}

		void Print(ostream *fstr, PrintMode mode){
			switch(mode){
				case PRINT_PARAM:
					(*fstr) << "|" << id << "|";
					break;
				case PRINT_GRAPH:
					(*fstr) << "|M_" << id << "|";
					break;
				case PRINT_SBML:
					(*fstr) << "M_" << id;
					break;
			}				
		}
};

//
// Math functions and operators
//////////////////////////////////////////////////////////////

class ExprLog : public Expr
{
public:
   ExprLog(const Pos& pos, SymbolTable* symbol_table, Expr* expr)
      : Expr(pos, symbol_table)
   {
      children.push_back(expr);
   }

   double Eval()
   {
      return log(children[0]->Eval());
   }

   virtual bool EvalConst(double& val) 
   { 
      double tmp;
      if (!children[0]->EvalConst(tmp))
      {
         return false;
      }
      else
      {
         val = log(tmp);
         return true;
      }
   }

   virtual Expr* Clone()
   {
      return new ExprLog(position, symbolTable, children[0]->Clone());
   }

   virtual void Print(ostream *fstr, PrintMode mode) 
   {
		(*fstr) << "log("; 
		children[0]->Print(fstr, mode);
		(*fstr) << ")";
   }
};

class ExprSqrt : public Expr
{
public:
   ExprSqrt(const Pos& pos, SymbolTable* symbol_table, Expr* expr)
      : Expr(pos, symbol_table)
   {
      children.push_back(expr);
   }

   double Eval()
   {
      return sqrt(children[0]->Eval());
   }

   virtual bool EvalConst(double& val) 
   { 
      double tmp;
      if (!children[0]->EvalConst(tmp))
      {
         return false;
      }
      else
      {
         val = sqrt(tmp);
         return true;
      }
   }

   virtual Expr* Clone()
   {
      return new ExprSqrt(position, symbolTable, children[0]->Clone());
   }

   virtual void Print(ostream *fstr, PrintMode mode) 
   {
		(*fstr) << "sqrt(";
		children[0]->Print(fstr, mode);
		(*fstr) << ")";
   }
};

class ExprPow : public Expr
{
public:
   ExprPow(const Pos& pos, SymbolTable* symbol_table, Expr* expr1, Expr* expr2)
      : Expr(pos, symbol_table)
   {
      children.push_back(expr1);
      children.push_back(expr2);
   }

   double Eval()
   {
      return pow(children[0]->Eval(), children[1]->Eval());
   }

   virtual bool EvalConst(double& val) 
   { 
      double tmp1, tmp2;
      if (children[0]->EvalConst(tmp1) && children[1]->EvalConst(tmp2))
      {
         val = pow(tmp1, tmp2);
         return true;
      }
      else
      {
         return false;
      }   
   }

   virtual Expr* Clone()
   {
      return new ExprPow(position, symbolTable, children[0]->Clone(), children[1]->Clone());
   }

   virtual void Print(ostream *fstr, PrintMode mode) 
   {
		(*fstr) << "pow(";
		children[0]->Print(fstr, mode);
		(*fstr) << ",";
		children[1]->Print(fstr, mode);
		(*fstr) << ")";
   }
};

class ExprExp : public Expr
{
public:
   ExprExp(const Pos& pos, SymbolTable* symbol_table, Expr* expr)
      : Expr(pos, symbol_table)
   {
      children.push_back(expr);
   }

   double Eval()
   {
      return exp(children[0]->Eval());
   }

   virtual bool EvalConst(double& val) 
   { 
      double tmp;
      if (!children[0]->EvalConst(tmp))
      {
         return false;
      }
      else
      {
         val = exp(tmp);
         return true;
      }
   }

   virtual Expr* Clone()
   {
      return new ExprExp(position, symbolTable, children[0]->Clone());
   }

   virtual void Print(ostream *fstr, PrintMode mode) 
   {
		(*fstr) << "exp(";
		children[0]->Print(fstr, mode);
		(*fstr) << ")";
   }
};

// Trascendental functions
///////////////////////////

class ExprSin : public Expr
{
public:
   ExprSin(const Pos& pos, SymbolTable* symbol_table, Expr* expr)
      : Expr(pos, symbol_table)
   {
      children.push_back(expr);
   }

   double Eval()
   {
      return sin(children[0]->Eval());
   }

   virtual bool EvalConst(double& val) 
   { 
      double tmp;
      if (!children[0]->EvalConst(tmp))
      {
         return false;
      }
      else
      {
         val = sin(tmp);
         return true;
      }
   }

   virtual Expr* Clone()
   {
      return new ExprSin(position, symbolTable, children[0]->Clone());
   }

   virtual void Print(ostream *fstr, PrintMode mode) 
   {
		(*fstr) << "sin(";
		children[0]->Print(fstr, mode);
		(*fstr) << ")";
   }
};

class ExprCos : public Expr
{
public:
   ExprCos(const Pos& pos, SymbolTable* symbol_table, Expr* expr)
      : Expr(pos, symbol_table)
   {
      children.push_back(expr);
   }

   double Eval()
   {
      return cos(children[0]->Eval());
   }

   virtual bool EvalConst(double& val) 
   { 
      double tmp;
      if (!children[0]->EvalConst(tmp))
      {
         return false;
      }
      else
      {
         val = cos(tmp);
         return true;
      }
   }

   virtual Expr* Clone()
   {
      return new ExprCos(position, symbolTable, children[0]->Clone());
   }

   virtual void Print(ostream *fstr, PrintMode mode) 
   {
		(*fstr) << "cos(";
		children[0]->Print(fstr, mode);
		(*fstr) << ")";
   }
};

class ExprASin : public Expr
{
public:
   ExprASin(const Pos& pos, SymbolTable* symbol_table, Expr* expr)
      : Expr(pos, symbol_table)
   {
      children.push_back(expr);
   }

   double Eval()
   {
      return asin(children[0]->Eval());
   }

   virtual bool EvalConst(double& val) 
   { 
      double tmp;
      if (!children[0]->EvalConst(tmp))
      {
         return false;
      }
      else
      {
         val = asin(tmp);
         return true;
      }
   }

   virtual Expr* Clone()
   {
      return new ExprASin(position, symbolTable, children[0]->Clone());
   }

   virtual void Print(ostream *fstr, PrintMode mode) 
   {
		(*fstr) << "asin(";
		children[0]->Print(fstr, mode);
		(*fstr) << ")";
   }
};

class ExprACos : public Expr
{
public:
   ExprACos(const Pos& pos, SymbolTable* symbol_table, Expr* expr)
      : Expr(pos, symbol_table)
   {
      children.push_back(expr);
   }

   double Eval()
   {
      return acos(children[0]->Eval());
   }

   virtual bool EvalConst(double& val) 
   { 
      double tmp;
      if (!children[0]->EvalConst(tmp))
      {
         return false;
      }
      else
      {
         val = acos(tmp);
         return true;
      }
   }

   virtual Expr* Clone()
   {
      return new ExprACos(position, symbolTable, children[0]->Clone());
   }

   virtual void Print(ostream *fstr, PrintMode mode) 
   {
		(*fstr) << "acos(";
		children[0]->Print(fstr, mode);
		(*fstr) << ")";
   }
};



class ExprSg : public Expr
{
public:
   ExprSg(const Pos& pos, SymbolTable* symbol_table, Expr* expr)
      : Expr(pos, symbol_table)
   {
      children.push_back(expr);
   }

   double Eval()
   {
      double val = children[0]->Eval();
      if (val == 0.0)
         return 0.0;
      else if (val > 0.0)
         return 1.0;
      else //if (val < 0.0)
         return -1.0;
   }

   virtual bool EvalConst(double& val) 
   { 
      double tmp;
      if (!children[0]->EvalConst(tmp))
      {
         return false;
      }
      else
      {
         if (tmp == 0.0)
            val =  0.0;
         else if (tmp > 0.0)
            val = 1.0;
         else //if (tmp < 0.0)
            val =  -1.0;
         return true;
      }
   }

   virtual Expr* Clone()
   {
      return new ExprSg(position, symbolTable, children[0]->Clone());
   }

   virtual void Print(ostream *fstr, PrintMode mode) 
   {
		(*fstr) << "sg(";
		children[0]->Print(fstr, mode);
		(*fstr) << ")";
   }
};

class ExprRand : public Expr
{
public:
   ExprRand(const Pos& pos, SymbolTable* symbol_table)
      : Expr(pos, symbol_table)
   { }

   virtual double Eval();  
  
   virtual bool EvalConst(double& val) 
   { 
      return false;
   }

   virtual Expr* Clone()
   {
      return new ExprRand(position, symbolTable);
   }

   virtual void Print(ostream *fstr, PrintMode mode) 
   {
		(*fstr) << "rand()";
   }
};


class ExprStep : public Expr
{
public:
   ExprStep(const Pos& pos, SymbolTable* symbol_table, Expr* expr, Expr* exprTrue, Expr* exprFalse)
      : Expr(pos, symbol_table)
   {
      children.push_back(expr);
      children.push_back(exprTrue);
      children.push_back(exprFalse);
   }

   double Eval()
   {
      double val = children[0]->Eval();
      if (val > 0.0)
         return children[1]->Eval();
      else 
         return children[2]->Eval();
   }

   virtual bool EvalConst(double& val) 
   { 
      double tmp;
      if (!children[0]->EvalConst(tmp))
      {
         return false;
      }
      else
      {
         if (tmp > 0.0)                     
            return children[1]->EvalConst(val);
         else 
            return children[2]->EvalConst(val);
      }
   }

   virtual Expr* Clone()
   {
      return new ExprStep(position, symbolTable, 
         children[0]->Clone(), children[1]->Clone(), children[2]->Clone());
   }

   virtual void Print(ostream *fstr, PrintMode mode) 
   {
		(*fstr) << "step(";
		children[0]->Print(fstr, mode);
      (*fstr) << ", ";
		children[1]->Print(fstr, mode);
      (*fstr) << ", ";
		children[2]->Print(fstr, mode);
		(*fstr) << ")";
   }
};

// Binary operators
class ExprOp : public Expr
{
private:
   ExprOpType opType;
public:
   ExprOp(const Pos& pos, SymbolTable* symbol_table, Expr* expr1, Expr* expr2, ExprOpType op_type)
      : Expr(pos, symbol_table)
   {
      children.push_back(expr1);
      children.push_back(expr2);
      opType = op_type;
   }

   double Eval()
   {
      double val1 = children[0]->Eval();
      double val2 = children[1]->Eval();
      double res = 0.0;

      switch (opType)
      {
      case EXPR_PLUS:
         res = val1 + val2;
         break;

      case EXPR_MINUS:
         res = val1 - val2;
         break;

      case EXPR_TIMES:
         res = val1 * val2;
         break;

      case EXPR_DIV:
         if (val2 == 0.0)
         {
            Error_Manager::PrintError(position, 28, "attempt to divide by 0!");   
            res = 0.0;
         }
         else
         {
            res = val1 / val2;
         }
         break;
      }
      return res;
   }

   virtual bool EvalConst(double& res) 
   { 
      double val1, val2;
      if (children[0]->EvalConst(val1) && children[1]->EvalConst(val2))
      {
         switch (opType)
         {
         case EXPR_PLUS:
            res = val1 + val2;
            break;

         case EXPR_MINUS:
            res = val1 - val2;
            break;

         case EXPR_TIMES:
            res = val1 * val2;
            break;

         case EXPR_DIV:
            res = val1 / val2;
            break;
         }
         return true;
      }
      else
      {
         return false;
      }   
   }

   virtual Expr* Clone()
   {
      return new ExprOp(position, symbolTable, children[0]->Clone(), children[1]->Clone(), opType);
   }

   virtual void Print(ostream *fstr, PrintMode mode) 
   {
		(*fstr) << "(";
		children[0]->Print(fstr, mode);
		(*fstr) << ToString(opType);
		children[1]->Print(fstr, mode);
		(*fstr) << ")";
   }
};

class ExprNeg : public Expr
{
public:
   ExprNeg(const Pos& pos, SymbolTable* symbol_table, Expr* expr)
      : Expr(pos, symbol_table)
   {
      children.push_back(expr);
   }

   double Eval()
   {
      return -(children[0]->Eval());
   }

   virtual bool EvalConst(double& val) 
   { 
      double tmp;
      if (!children[0]->EvalConst(tmp))
      {
         return false;
      }
      else
      {
         val = -tmp;
         return true;
      }
   }

   virtual Expr* Clone()
   {
      return new ExprNeg(position, symbolTable, children[0]->Clone());
   }

    virtual void Print(ostream *fstr, PrintMode mode)
   {
		(*fstr) << "(-";
		children[0]->Print(fstr, mode);
		(*fstr) << ")";
   }
};



#endif //EXPR_H_INCLUDED

