#include "FunBase.h"

///////////////////////////////////////////////////////////////////////////
// FUNBASE CLASS IMPLEMENTATION
///////////////////////////////////////////////////////////////////////////

//
FunBase::FunBase(const FunBase &right)
{
	// Copy of the Entities
	entity_variables.resize( right.entity_variables.size() );

	for ( int i = 0 ; i < (int)entity_variables.size() ; i++ )
	{
		entity_variables[i] = right.entity_variables[i];
	}

	// Copy of the State Variables
	state_variables.resize( right.state_variables.size() );

	for ( int i = 0 ; i < (int)state_variables.size() ; i++ )
	{
		state_variables[i] = right.state_variables[i];
	}

	// Copy of the other variables
	type = right.type;
	env = right.env;
}

//
FunBase::FunBase(Environment *p_env, FunctionRateType p_type)
{
	// Initialization of the other variables
	type = p_type;
	env = p_env;
}

//
FunBase::FunBase(Environment *p_env, FunctionRateType p_type, int entity_size, int state_size)
{
	// Initialization of the Entities vector
   entity_variables.resize(entity_size);

	for ( int i = 0 ; i < entity_size ; i++ )
	{
		entity_variables[i] = NULL;
	}

	// Initialization of the State Variables vector
	state_variables.resize(state_size);

	for ( int i = 0 ; i < state_size ; i++ )
	{
		state_variables[i] = NULL;
	}

	// Initialization of the other variables
	type = p_type;
	env = p_env;
}

// 
void FunBase::SetEntityVariable(Entity *elem, int position)
{
	if ( position < 0 || position >= (int)entity_variables.size() ) 
		return;

	entity_variables[position] = elem;
}

// 
Entity* FunBase::GetEntityVariable(int position)
{
	if ( position < 0 || position >= (int)entity_variables.size() ) 
		return NULL;

	return entity_variables[position];
}

//
void FunBase::SetStateVariable(StateVar *var, int position)
{
	if ( position < 0 || position >= (int)state_variables.size() ) 
		return;

	state_variables[position] = var;
}
  
//
StateVar* FunBase::GetStateVariable(int position)
{
	if ( position < 0 || position >= (int)state_variables.size() ) 
		return NULL;

	return state_variables[position];
}

