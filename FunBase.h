
#ifndef FUN_BASE_H
#define FUN_BASE_H

#include <vector>
#include <fstream>
#include "Define.h"
#include "Expr.h"

class StateVar;
class Entity;
class Environment;

const std::string BUILT_IN("BUILT_IN");

// Classe base per la definizione di una funzione di rate
class FunBase
{
protected:
   std::vector<Entity*> entity_variables;
   std::vector<StateVar*> state_variables;

   FunctionRateType type;

   Environment *env;

   // disable copy constructor, must use clone
   FunBase(const FunBase &right);

public:
   FunBase(Environment* p_env, FunctionRateType p_type);
   FunBase(Environment *p_env, FunctionRateType p_type, int entity_size, int state_size);

   virtual ~FunBase() { }

   // Every implementor must implement cloning:
   // build a copy of itself using new, return the new pointer.
   virtual FunBase* Clone() = 0;

   virtual double ActualRate() = 0;
   virtual double Rate(Iterator_Interface *c_iter) { return ActualRate(); }
   virtual double ActionNumber() { return 1; }
  
   // Entity accessors
   void SetEntityVariable(Entity *elem, int position);
   Entity* GetEntityVariable(int position);
   std::vector<Entity*>& GetEntityList() 
   { 
      return entity_variables; 
   }

   virtual void ReplaceEntities(Entity* e1, Entity* e2) { }

   // State variables accessors
   void SetStateVariable(StateVar *var, int position);
   StateVar* GetStateVariable(int position);
   std::vector<StateVar*>& GetStateVarList() { return state_variables; }

   virtual bool IsManagedByMap() { return false; }
   virtual bool Init() { return true; }

   virtual const std::string& GetId() { return BUILT_IN;  }
   virtual Pos& GetPos() { return Pos::Empty; }
   virtual FunctionRateType GetType() { return type; }

   virtual double GetConstant() = 0;
   virtual void Print(ofstream *fstr) {}

	virtual Expr* GetExpr (int mol1, int mol2, Iterator_Interface *c_iter) = 0;
};

#endif //FUN_BASE_H


