
#ifndef FUN_GENERIC_H_INCLUDED
#define FUN_GENERIC_H_INCLUDED

#include "FunBase.h"
#include "Expr.h"

#include <assert.h>
#include <string>

class FunGeneric : public FunBase
{
private:
   Expr* expression;
   bool managed_by_map;
   std::string id;

public:
   FunGeneric(Environment* p_env, Expr* expression, const std::string& p_id, bool managed)
      : FunBase(p_env, RATE_GENERIC)
   {
      assert(expression != NULL);
      this->expression = expression;      
      managed_by_map = managed;     
      id = p_id;
   }

   virtual FunBase* Clone()
   {
      Expr* newExpr = expression->Clone();

      FunGeneric* newFun = new FunGeneric(this->env, newExpr , this->id, false);
      newFun->entity_variables.insert(newFun->entity_variables.begin(),
         this->entity_variables.begin(), this->entity_variables.end());

      newFun->state_variables.insert(newFun->state_variables.begin(),
         this->state_variables.begin(), this->state_variables.end());

      return newFun;
   }

   virtual double ActualRate()
   {
      return expression->Eval();
   }

   virtual bool IsManagedByMap() { return managed_by_map; }

   Expr* GetExpression() { return expression; }

   virtual void ReplaceEntities(Entity* e1, Entity* e2) 
   { 
      expression->ReplaceEntities(e1, e2);

      //refresh
      /*
      entity_variables.clear();

      std::set<Entity*> entities;
      std::set<StateVar*> variables;
      expression->GetEntitiesAndVariables(entities, variables);
      entity_variables.insert(entity_variables.end(), entities.begin(), entities.end());
      */
   }

   virtual bool Init()
   {
	   if (!expression->Check())
	   {         
		   return false;
	   }
	   std::set<Entity*> entities;
	   std::set<StateVar*> variables;
	   expression->GetEntitiesAndVariables(entities, variables);
	   entity_variables.insert(entity_variables.end(), entities.begin(), entities.end());
	   state_variables.insert(state_variables.end(), variables.begin(), variables.end());
	   return true;
   }

   virtual std::string& GetId() { return id; }
   virtual Pos& GetPos() { return expression->GetPos(); }


   double GetConstant(){ return expression->Eval(); };
   void Print(ofstream *fstr)
   {
	   (*fstr) << "let " << id << " : function = ";
	   expression->Print(fstr, PRINT_PARAM);
	   (*fstr) << ";";
   }

	Expr* GetExpr (int mol1, int mol2, Iterator_Interface *c_iter){
		return expression->Clone();
	}
};

#endif //FUN_GENERIC_H_INCLUDED


