#include "FunStd.h"
#include "Entity.h"
#include "Complex_Rel.h"
#include "Environment.h"

///////////////////////////////////////////////////////////////////////////
// FUNBIM CLASS IMPLEMENTATION
///////////////////////////////////////////////////////////////////////////

//
FunBim::FunBim(Environment *p_env, double p_constant, int p_io_combinations)
: FunBase(p_env,RATE_BIM,2,0)
{
	constant = p_constant;
	io_combinations = p_io_combinations;
}


//
FunBim::FunBim(Environment *p_env, double p_constant, Entity *first, Entity *second, int p_io_combinations)
: FunBase(p_env,RATE_BIM,2,0)
{
	constant = p_constant;
	io_combinations = p_io_combinations;
	this->SetEntityVariable(first,0);
	this->SetEntityVariable(second,1);
}

//
FunBim::FunBim(const FunBim &right) : FunBase(right)
{
	constant = right.constant;
	io_combinations = right.io_combinations;
}

//
FunBim::~FunBim()
{}

//
FunBase* FunBim::Clone()
{
	return ( new FunBim(*this) );
}

//
double FunBim::Rate(Iterator_Interface *c_iter)
{
	Complex *c1,*c2;
	c1 = c2 = NULL;

	if ( c_iter != NULL )
	{
		assert(c_iter->GetCategory() == IT_ACT_MAP);
		double c_rate = this->constant;
		if ( c_iter->GetIterator(2) == NULL )
		{
			// sono specie non complessate
			if ( entity_variables[0] == entity_variables[1] )
			{	// homodimerization
				c_rate *= ( 0.5 * (double)entity_variables[0]->GetCounter() * 
					((double)entity_variables[1]->GetCounter() - 1.0) );
			}
			else 
			{ 
				c_rate *=  ( ((double)entity_variables[0]->GetCounter()) * 
						   ((double)entity_variables[1]->GetCounter()) ); 
			}
		}
		else
		{
			assert(c_iter->GetIterator(2)->GetCategory() == IT_COMPLEX_REL_TWO);
			CR_Two_Iterator * c_iter_comp = (CR_Two_Iterator *)(c_iter->GetIterator(2));
			CR_One_Iterator * c_iter1 = (CR_One_Iterator*)(c_iter_comp->GetIterator(1));
			if (c_iter1 != NULL)
				c1 = (*((CR_One_Iterator *)c_iter1)->GetCurrent());
			CR_One_Iterator * c_iter2 = (CR_One_Iterator*)(c_iter_comp->GetIterator(2));
			if (c_iter2 != NULL)
				c2 = (*((CR_One_Iterator *)c_iter2)->GetCurrent());
			if ( c_iter_comp->IsWait() )
			{
				// For sure c1 and c2 are equal and different from NULL, because
				// otherwise IsWait() returns always false
				c_rate *= c1->GetNumber();
			}
			else
			{
				if ( c_iter1 != NULL && c_iter2 != NULL && !c_iter1->IsEqual(c_iter2) )
				{
					c_rate *= 2;
				}

				if ( c_iter1 == NULL )
				{
					c_rate *= this->entity_variables[0]->GetCounter();
				}
				else
				{
					c_rate *= c1->GetNumber();
				}

				if ( c_iter2 == NULL )
				{
					c_rate *= this->entity_variables[1]->GetCounter();
				}
				else
				{
					if ( c1 == c2 )
					{
						// non devo contare lo stesso complesso
						c_rate *= c2->GetNumber() - 1;
						c_rate = c_rate / 2 ;
					}
					else
					{
						c_rate *= c2->GetNumber();
					}

				}
			}
		}

		return c_rate;
	}

	return constant;
}
	
//
double FunBim::ActualRate()
{
	double action_number = ActionNumber();

   if (action_number == 0.0)
      return 0.0;

	if ( constant == HUGE_VAL )
		return HUGE_VAL;

	return ( action_number * constant );
}

//
double FunBim::ActionNumber()
{
	double total;
	if ( entity_variables[0] == NULL || entity_variables[1] == NULL )
		return 0;

	if ( entity_variables[0] == entity_variables[1] )
	{	// homodimerization
		total = ( 0.5 * (double)entity_variables[0]->GetCounter() * 
			((double)entity_variables[1]->GetCounter() - 1.0) );
	}
	else 
	{ 
		total =  ( ((double)entity_variables[0]->GetCounter()) * ((double)entity_variables[1]->GetCounter()) ); 
	}
	
	return ( total * ((double)io_combinations) ); 
}

//
void FunBim::SetConstant(double p_constant)
{
	constant = p_constant;
}

double FunBim::GetConstant()
{
	return constant;
}

void FunBim::SetIOCombinations(int p_io_combinations)
{
	io_combinations = p_io_combinations;
}

double FunBim::GetIOCombinations()
{
	return ((double)io_combinations);
}

Expr* FunBim::GetExpr (int mol1, int mol2, Iterator_Interface *c_iter)
{
	Pos pos(0,0,"");
	Expr *expr1, *expr;
   Expr* expr2 = NULL;
	CR_Two_Iterator *cr_iter;
	string name;

	cr_iter = NULL;

	if (constant == HUGE_VAL){
		return NULL;
	}

	if (c_iter == NULL || c_iter->GetIterator(2) == NULL)
   {
		name = env->GetST()->GetEntNameFromID(mol1);
		expr1 = new ExprConcentration(pos, env->GetST(), name.substr(2));
		name = env->GetST()->GetEntNameFromID(mol2);
		expr2 = new ExprConcentration(pos, env->GetST(), name.substr(2));
	} 
   else 
   {
		assert(dynamic_cast<CR_Two_Iterator*>(c_iter->GetIterator(2)));
		cr_iter = (CR_Two_Iterator*)(c_iter->GetIterator(2));
		if (mol1 > 0)
      {
			name = env->GetST()->GetEntNameFromID(mol1);
			expr1 = new ExprConcentration(pos, env->GetST(), name.substr(2));
		} 
      else 
      {
			name = env->GetST()->GetMolNameFromID(abs(mol1));
			expr1 = new ExprCompConcentration(pos, env->GetST(), name.substr(2));
		}

		//In the case of an Event cr_iter is set to NULL
		if (cr_iter == NULL || !cr_iter->IsWait())
      {
			if (mol2 > 0)
         {
				name = env->GetST()->GetEntNameFromID(mol2);
				expr2 = new ExprConcentration(pos, env->GetST(), name.substr(2));
			} 
         else if (mol2 < 0)
         {
				name = env->GetST()->GetMolNameFromID(abs(mol2));
				expr2 = new ExprCompConcentration(pos, env->GetST(), name.substr(2));
			}
		}
	}

	if (cr_iter != NULL && cr_iter->IsWait())
   {
		//Monomolecular reactions
		cerr << "Found a MONO while expecting a BIM" << endl;
		expr = new ExprOp(pos, env->GetST(), expr1, new ExprNumber(pos, env->GetST(),constant), EXPR_TIMES);
	} 
   else 
   {
		if (mol1 == mol2)
      {
			//Homodimerization reaction
         assert (expr2 != NULL);
			expr2 = new ExprOp(pos, env->GetST(), expr2, new ExprNumber(pos, env->GetST(), 1), EXPR_MINUS);
			expr = new ExprOp(pos, env->GetST(), expr1, expr2, EXPR_TIMES);	
			expr = new ExprOp(pos, env->GetST(), expr, new ExprNumber(pos, env->GetST(),constant), EXPR_TIMES);
			expr = new ExprOp(pos, env->GetST(), expr, new ExprNumber(pos, env->GetST(), 2), EXPR_DIV);
		} 
      else 
      {
			//Bimoleculare Reaction
         assert (expr2 != NULL);
			expr = new ExprOp(pos, env->GetST(), expr1, expr2, EXPR_TIMES);
			expr = new ExprOp(pos, env->GetST(), expr, new ExprNumber(pos, env->GetST(),constant), EXPR_TIMES);
		}
	}
	
	expr->Check(false);
	return expr;
}


///////////////////////////////////////////////////////////////////////////
// FUNMONO CLASS IMPLEMENTATION
///////////////////////////////////////////////////////////////////////////

//
FunMono::FunMono(Environment *p_env, double p_constant, Entity *first) 
	: FunBase(p_env,RATE_MONO,1,0)
{
	constant = p_constant;
}

//
FunMono::FunMono(const FunMono &right) : FunBase(right)
{
	constant = right.constant;
}

//
FunMono::~FunMono()
{}

//
FunBase* FunMono::Clone()
{
	return ( new FunMono(*this) );
}

//
double FunMono::Rate(Iterator_Interface *c_iter)
{
	if ( c_iter != NULL )
	{
		assert(c_iter->GetCategory() == IT_MONO_ACT);
		double c_rate = ((Entity_Iterator *)(c_iter->GetIterator(1)))->GetCurrentAction()->GetRate();
		
		if ( c_iter->GetIterator(2) == NULL )
		{
			c_rate *=  entity_variables[1]->GetCounter();
		}
		else
		{
			c_rate *= (*((CR_One_Iterator *)c_iter->GetIterator(2))->GetCurrent())->GetNumber();
		}

		return c_rate;
	}

	return constant;
}

//
double FunMono::ActualRate()
{
	if ( entity_variables[0] == NULL )
	{
		return 0.0;
	}

	double action_number  = ((double)entity_variables[0]->GetCounter());

	if (action_number == 0.0)
		return 0.0;

	if ( constant == HUGE_VAL )
		return HUGE_VAL;

	return ( action_number * constant );
}

//
double FunMono::ActionNumber()
{
	return ((double)entity_variables[0]->GetCounter());
}

//
void FunMono::SetConstant(double p_constant)
{
	constant = p_constant;
}

//
double FunMono::GetConstant()
{
	return constant;
}

Expr* FunMono::GetExpr(int mol1, int mol2, Iterator_Interface *c_iter){
	Expr *expr;
	string name;
	Pos pos(0,0,"");
	
	if (constant == HUGE_VAL){
		return NULL;
	}

	if (mol1 > 0){
		name = env->GetST()->GetEntNameFromID(mol1);
		expr = new ExprConcentration(pos, env->GetST(), name.substr(2));
	} else {
		name = env->GetST()->GetMolNameFromID(abs(mol1));
		expr = new ExprCompConcentration(pos, env->GetST(), name.substr(2));
	}

	expr = new ExprOp(pos, env->GetST(), expr, new ExprNumber(pos, env->GetST(),constant), EXPR_TIMES);

	expr->Check();
	return expr;
}
///////////////////////////////////////////////////////////////////////////
// FUNCONSTANT CLASS IMPLEMENTATION
///////////////////////////////////////////////////////////////////////////

//
FunConstant::FunConstant(Environment *p_env, double p_constant, bool p_always_execute) 
	: FunBase(p_env,RATE_MONO,1,0)
{
	constant = p_constant;
   always_execute = p_always_execute;
}

//
FunConstant::FunConstant(const FunConstant &right) : FunBase(right)
{
	constant = right.constant;
   always_execute = right.always_execute;
}

//
FunBase* FunConstant::Clone()
{
	return ( new FunConstant(*this) );
}

//???
double FunConstant::Rate(Iterator_Interface *c_iter)
{
	if ( c_iter != NULL )
	{
		assert(c_iter->GetCategory() == IT_MONO_ACT);
		double c_rate = ((Entity_Iterator *)(c_iter->GetIterator(1)))->GetCurrentAction()->GetRate();
		
      if (always_execute)
         return c_rate;

      double counter;
		if ( c_iter->GetIterator(2) == NULL )
		{
			counter = entity_variables[1]->GetCounter();
		}
		else
		{
			counter = (*((CR_One_Iterator *)c_iter->GetIterator(2))->GetCurrent())->GetNumber();
		}

      if (counter == 0)
         return 0;

		return c_rate;
	}

	return constant;
}

//
double FunConstant::ActualRate()
{
   if (always_execute)
      return constant;

   if ( entity_variables[0] == NULL )
	{
		return 0.0;
	}

	double action_number  = ((double)entity_variables[0]->GetCounter());

	if (action_number == 0.0)
		return 0.0;

	if ( constant == HUGE_VAL )
		return HUGE_VAL;

   //!! here is the difference with FunMono...
	return constant;
}

//
double FunConstant::ActionNumber()
{
   if (always_execute)
      return 1;

   double action_number  = ((double)entity_variables[0]->GetCounter());

   if (action_number == 0.0)
		return 0.0;

   return 1;
}

//
void FunConstant::SetConstant(double p_constant)
{
	constant = p_constant;
}

//
double FunConstant::GetConstant()
{
	return constant;
}

Expr* FunConstant::GetExpr (int mol1, int mol2, Iterator_Interface *c_iter){
	Pos pos(0,0,"");
	Expr *expr;

	if (constant == HUGE_VAL) {
		return NULL;
	}
	expr =  new ExprNumber(pos, env->GetST(), constant);
	expr->Check();
	return expr;
}


