#ifndef FUN_STD_H
#define FUN_STD_H 

#include "FunBase.h"

///////////////////////////////////////////////////////////////////////////
// FUNBIM CLASS DEFINITION
///////////////////////////////////////////////////////////////////////////

class FunBim : public FunBase
{
private:
	int io_combinations;
	double constant;
public:
	FunBim(Environment *p_env, double p_constant, int p_io_combinations);
	FunBim(Environment *p_env, double p_constant, 
		Entity *first, 
		Entity *second, 
		int p_io_combinations);

	FunBim(const FunBim &right);

	~FunBim();
	
	FunBase* Clone();
	
	double Rate(Iterator_Interface *c_iter);
	double ActualRate();
	double ActionNumber();

	// constant accessors
	void SetConstant(double p_constant);
	double GetConstant();

	// io_combinations accessors
	void SetIOCombinations(int p_io_combinations);
	double GetIOCombinations();
	Expr* GetExpr (int mol1, int mol2, Iterator_Interface *c_iter);
};


///////////////////////////////////////////////////////////////////////////
// FUNMONO CLASS DEFINITION
///////////////////////////////////////////////////////////////////////////

class FunMono : public FunBase
{
private:
	double constant;
public:
	FunMono(Environment *p_env, double p_constant, Entity *first);

	FunMono(const FunMono &right);

	~FunMono();
	
	FunBase* Clone();

	double Rate(Iterator_Interface *c_iter);
	double ActualRate();
	double ActionNumber();

	// constant accessors
	void SetConstant(double p_constant);
	double GetConstant();

	Expr* GetExpr (int mol1, int mol2, Iterator_Interface *c_iter);
};

class FunConstant : public FunBase
{
private:
	double constant;
   bool always_execute;

public:
	FunConstant(Environment *p_env, double p_constant, bool p_always_execute);

	FunConstant(const FunConstant &right);
	
	FunBase* Clone();

	double Rate(Iterator_Interface *c_iter);
	double ActualRate();
	double ActionNumber();

	// constant accessors
	void SetConstant(double p_constant);
	double GetConstant();

	Expr* GetExpr(int mol1, int mol2, Iterator_Interface *c_iter);
};

#endif


