%{
 #include <iostream>
 #include <string>
 #include "FuncParser.h"
 #include "FuncParser.hpp"
 #include "Error_Manager.h"

 using namespace std;

 #ifdef _MSC_VER 
 #pragma warning (push,0)
 #pragma warning( disable : 4505 )
 #endif
 
 #define YY_USER_ACTION { yylloc->first_line = yylloc->last_line = yylineno; yylloc->last_column=yycolumn+yyleng-1; yylloc->first_column=yycolumn;yycolumn+=yyleng; }


%}


%option prefix="func_"
%option nounistd
%option noyywrap
%option never-interactive
%option stack
%option bison-bridge
%option bison-locations
%option reentrant
%option yylineno


/*regular definitions*/

delim [ \t]
fline (\r\n?|\n)
NotEnd [^\r\n]
sb {delim}+

DLetter [a-z]
ULetter [A-Z]
Digit [0-9]
Exp [Ee][+\-]?{Digit}+

id ({DLetter}|{ULetter}|_)({ULetter}|{DLetter}|{Digit}|_)*

decimal {Digit}+
real1 {Digit}+{Exp}
real2 {Digit}*"."{Digit}+({Exp})?
real3 {Digit}+"."{Digit}*({Exp})?

CmntStrt     \/\*
CmntEnd      \*\/
CmntContent  [^\*\n\r]*

%x CMMNT

%%

{sb}    {  }
{fline} {  }

^{CmntStrt}{CmntContent}\**          { yy_push_state(CMMNT, yyscanner); }
^{CmntStrt}{CmntContent}\**{CmntEnd} { }

<CMMNT>{CmntContent}\**              { }
<CMMNT>{CmntContent}\**{CmntEnd}     { yy_pop_state(yyscanner); }
\/\/{NotEnd}*                        { }

"|"			{ return(LPIPE); }
"("			{ return(LPOPEN); }
")"			{ return(LPCLOSE); }

"+"			{ return(LPLUS); }
"*"         { return(LTIMES); }
"/"         { return(LDIV); }
"-"         { return(LMINUS); }
"exp"       { return(LEXP); }
"pow"       { return(LPOW); }
"log"       { return(LLOG); }
"sqrt"      { return(LSQRT); }

"sin"       { return(LSIN); }
"cos"       { return(LCOS); }
"acos"      { return(LACOS); }
"asin"      { return(LASIN); }

"step"      { return(LSTEP); }
"sg"        { return(LSG); }
"rand"      { return(LRAND); }
"inf"       { return(LINF); }

","			{ return(LCOMMA); }
":"			{ return(LDDOT); }
";"			{ return(LDOTCOMMA); }
"="			{ return(LEQUAL); }
"let"		   { return(LLET); }
"time"		{ return(LTIME); }

"var"       { return(LSTATEVAR); }
"function"  { return(LFUNCTION); }
"const"     { return(LCONST) ; }
"init"      { return(LINIT); }

"1B"        { return (LB1); }
"2B"        { return (LB2); }

{id}		{ 
			 string * new_string = new string(yytext);
			 yylval->ptr_string = new_string;
			 return(LID);
			}

{decimal}	{
			 string * new_string = new string(yytext);
			 yylval->ptr_string = new_string;
			 return(LDECIMAL);
			}

{real1} {
			 string * new_string = new string(yytext);
			 yylval->ptr_string = new_string;
			 return(LREAL);
        }
        
{real2} {
			 string * new_string = new string(yytext);
			 yylval->ptr_string = new_string;
			 return(LREAL);
        }
        
{real3} {
			 string * new_string = new string(yytext);
			 yylval->ptr_string = new_string;
			 return(LREAL);
        }

.       { 
			 Error_Manager::PrintError(Pos(yylloc->first_line, yylloc->first_column, ""), "(Failed recognition) token %s not admitted.", yytext);
			 yyterminate();
        }
%%

 #ifdef _MSC_VER 
 #pragma warning (pop)
 #endif
