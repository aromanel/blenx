
#include "FuncMap.h"
#include "Error_Manager.h"

static std::list<FunBase*> empty_list;

FunGeneric* FuncMap::Insert(const Pos &pos, const std::string &id, Expr* expr)
{
   FunGeneric* fun = new FunGeneric(this->env, expr, id, true);

   FuncMapEntry entry;
   entry.func_list.push_back(fun);
   entry.managedByMap = true;
   funcMap[id] = entry;

   return fun;
}

void FuncMap::Insert(const std::string& id, FunBase* function)
{
   FuncMapEntry entry;
   entry.func_list.push_back(function);
   entry.managedByMap = false;
   funcMap[id] = entry;
}

void FuncMap::InsertClone(const std::string& id, FunBase* function)
{
   funcMap[id].func_list.push_back(function);
}

std::list<FunBase*>& FuncMap::Find(const std::string& id)
{
   hash_map<std::string, FuncMapEntry>::iterator it;
   it = funcMap.find(id);
   if (it != funcMap.end())
      return it->second.func_list;

   return empty_list;
}

void FuncMap::Reset()
{
   hash_map<std::string, FuncMapEntry>::iterator it;
   for (it = funcMap.begin(); it != funcMap.end(); ++it)
   {
      if (it->second.managedByMap)
      {
         std::list<FunBase*>::iterator it2;
         for (it2 = it->second.func_list.begin(); it2 != it->second.func_list.end(); ++it2)
            delete (*it2);
      }
   }
   funcMap.clear();
}

bool FuncMap::InitAM()
{
   hash_map<std::string, FuncMapEntry>::iterator it;
   for (it = funcMap.begin(); it != funcMap.end(); ++it)
   {
      if (it->second.managedByMap)
      {
         std::list<FunBase*>::iterator it2;
         for (it2 = it->second.func_list.begin(); it2 != it->second.func_list.end(); ++it2)
         {
            bool res = (**it2).Init();
            if (!res)
               return false;
         }
      }
   }
   return true;
}

