
#ifndef FUNC_MAP_H_INCLUDED
#define FUNC_MAP_H_INCLUDED

#include "Expr.h"
#include "hash.h"
#include "FunBase.h"
#include "FunGeneric.h"
#include "Define.h"

#include <string>

class FuncMap
{
   // A reference to whatever is required in symboltable to hold:
   //1) symbol name
   //2) symbol type (statevar or function)
   //3) FunBase*
   //std::vector<FunBase*>& functions;
private:

   struct FuncMapEntry
   {
      bool managedByMap;
      std::list<FunBase*> func_list;
   };

   hash_map<std::string, FuncMapEntry> funcMap;
   Environment* env;

public:
   FuncMap(Environment* p_env)
   {
      env = p_env;
   }

   ~FuncMap() { Reset(); }

   // Insert a new FunGeneric, starting form an expression
   // Pos is used to signal errors, after that it is discarded
   FunGeneric* Insert(const Pos& pos, const std::string& id, Expr* expression);

   // Insert a function in the map; these will NOT be deleted
   void Insert(const std::string& id, FunBase* function);
   void InsertClone(const std::string& id, FunBase* function);

   std::list<FunBase*>& Find(const std::string& id);

   void Reset();
   bool InitAM();

   void Print(ofstream *fstr)
   {
	   hash_map<std::string, FuncMapEntry>::iterator i;
	   for ( i = funcMap.begin() ; i != funcMap.end() ; i++ )
	   {
		   (*i).second.func_list.front()->Print(fstr);
		   (*fstr) << endl;
	   }
   }
};

#endif //FUNC_MAP_H_INCLUDED

