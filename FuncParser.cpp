/* A Bison parser, made by GNU Bison 2.1.  */

/* Skeleton parser for Yacc-like parsing with Bison,
   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

/* Written by Richard Stallman by simplifying the original so called
   ``semantic'' parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.1"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 1

/* Using locations.  */
#define YYLSP_NEEDED 1

/* Substitute the variable and function names.  */
#define yyparse func_parse
#define yylex   func_lex
#define yyerror func_error
#define yylval  func_lval
#define yychar  func_char
#define yydebug func_debug
#define yynerrs func_nerrs
#define yylloc func_lloc

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     LID = 258,
     LDECIMAL = 259,
     LREAL = 260,
     LPIPE = 261,
     LPOPEN = 262,
     LPCLOSE = 263,
     LCOMMA = 264,
     LDDOT = 265,
     LDOTCOMMA = 266,
     LEQUAL = 267,
     LTIME = 268,
     LLET = 269,
     LSTATEVAR = 270,
     LFUNCTION = 271,
     LCONST = 272,
     LINIT = 273,
     LB1 = 274,
     LB2 = 275,
     LMINUS = 276,
     LPLUS = 277,
     LDIV = 278,
     LTIMES = 279,
     POS = 280,
     NEG = 281,
     LSQRT = 282,
     LPOW = 283,
     LLOG = 284,
     LEXP = 285,
     LASIN = 286,
     LACOS = 287,
     LSIN = 288,
     LCOS = 289,
     LINF = 290,
     LRAND = 291,
     LSG = 292,
     LSTEP = 293
   };
#endif
/* Tokens.  */
#define LID 258
#define LDECIMAL 259
#define LREAL 260
#define LPIPE 261
#define LPOPEN 262
#define LPCLOSE 263
#define LCOMMA 264
#define LDDOT 265
#define LDOTCOMMA 266
#define LEQUAL 267
#define LTIME 268
#define LLET 269
#define LSTATEVAR 270
#define LFUNCTION 271
#define LCONST 272
#define LINIT 273
#define LB1 274
#define LB2 275
#define LMINUS 276
#define LPLUS 277
#define LDIV 278
#define LTIMES 279
#define POS 280
#define NEG 281
#define LSQRT 282
#define LPOW 283
#define LLOG 284
#define LEXP 285
#define LASIN 286
#define LACOS 287
#define LSIN 288
#define LCOS 289
#define LINF 290
#define LRAND 291
#define LSG 292
#define LSTEP 293




/* Copy the first part of user declarations.  */
#line 4 "FuncParser.y"

  #include <string>
  #include "Define.h"
  #include "Error_Manager.h"
  #include "FuncMap.h"
  
  #include "Expr.h"
  #include "FuncParser.h"
  #include "FuncParser.hpp"
 
  using namespace std;
   
  //FuncLexer forward definitions
  typedef void* yyscan_t;
  int  func_lex(YYSTYPE *lvalp, YYLTYPE *llocp, yyscan_t scanner);
  void func_set_in(FILE * in_str, yyscan_t scanner);
  int  func_lex_init (yyscan_t* scanner);
  int  func_lex_destroy (yyscan_t yyscanner);
  
  void func_error(YYLTYPE *llocp, FuncParser* parser, yyscan_t scanner, char const* msg);
    
#ifdef _MSC_VER
#pragma warning(push,1)
#pragma warning(disable: 4706)
#pragma warning(disable: 4702)
#pragma warning(disable: 4505)
#endif
  


/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 1
#endif

/* Enabling the token table.  */
#ifndef YYTOKEN_TABLE
# define YYTOKEN_TABLE 0
#endif

#if ! defined (YYSTYPE) && ! defined (YYSTYPE_IS_DECLARED)
#line 37 "FuncParser.y"
typedef union YYSTYPE {
   std::string* ptr_string;
   Expr* expression;
} YYSTYPE;
/* Line 196 of yacc.c.  */
#line 204 "FuncParser.cpp"
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif

#if ! defined (YYLTYPE) && ! defined (YYLTYPE_IS_DECLARED)
typedef struct YYLTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
} YYLTYPE;
# define yyltype YYLTYPE /* obsolescent; will be withdrawn */
# define YYLTYPE_IS_DECLARED 1
# define YYLTYPE_IS_TRIVIAL 1
#endif


/* Copy the second part of user declarations.  */


/* Line 219 of yacc.c.  */
#line 228 "FuncParser.cpp"

#if ! defined (YYSIZE_T) && defined (__SIZE_TYPE__)
# define YYSIZE_T __SIZE_TYPE__
#endif
#if ! defined (YYSIZE_T) && defined (size_t)
# define YYSIZE_T size_t
#endif
#if ! defined (YYSIZE_T) && (defined (__STDC__) || defined (__cplusplus))
# include <stddef.h> /* INFRINGES ON USER NAME SPACE */
# define YYSIZE_T size_t
#endif
#if ! defined (YYSIZE_T)
# define YYSIZE_T unsigned int
#endif

#ifndef YY_
# if YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

#if ! defined (yyoverflow) || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if defined (__STDC__) || defined (__cplusplus)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#     define YYINCLUDED_STDLIB_H
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning. */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2005 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM ((YYSIZE_T) -1)
#  endif
#  ifdef __cplusplus
extern "C" {
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if (! defined (malloc) && ! defined (YYINCLUDED_STDLIB_H) \
	&& (defined (__STDC__) || defined (__cplusplus)))
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if (! defined (free) && ! defined (YYINCLUDED_STDLIB_H) \
	&& (defined (__STDC__) || defined (__cplusplus)))
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifdef __cplusplus
}
#  endif
# endif
#endif /* ! defined (yyoverflow) || YYERROR_VERBOSE */


#if (! defined (yyoverflow) \
     && (! defined (__cplusplus) \
	 || (defined (YYLTYPE_IS_TRIVIAL) && YYLTYPE_IS_TRIVIAL \
             && defined (YYSTYPE_IS_TRIVIAL) && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  short int yyss;
  YYSTYPE yyvs;
    YYLTYPE yyls;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (short int) + sizeof (YYSTYPE) + sizeof (YYLTYPE))	\
      + 2 * YYSTACK_GAP_MAXIMUM)

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined (__GNUC__) && 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  YYSIZE_T yyi;				\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (0)
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack)					\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack, Stack, yysize);				\
	Stack = &yyptr->Stack;						\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (0)

#endif

#if defined (__STDC__) || defined (__cplusplus)
   typedef signed char yysigned_char;
#else
   typedef short int yysigned_char;
#endif

/* YYFINAL -- State number of the termination state. */
#define YYFINAL  7
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   206

/* YYNTOKENS -- Number of terminals. */
#define YYNTOKENS  39
/* YYNNTS -- Number of nonterminals. */
#define YYNNTS  7
/* YYNRULES -- Number of rules. */
#define YYNRULES  36
/* YYNRULES -- Number of states. */
#define YYNSTATES  111

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   293

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const unsigned char yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const unsigned char yyprhs[] =
{
       0,     0,     3,     5,     7,     9,    11,    13,    16,    24,
      32,    43,    53,    66,    74,    76,    78,    82,    86,    90,
      95,   100,   105,   112,   117,   122,   127,   132,   141,   146,
     150,   154,   158,   162,   166,   169,   172
};

/* YYRHS -- A `-1'-separated list of the rules' RHS. */
static const yysigned_char yyrhs[] =
{
      42,     0,    -1,     5,    -1,     4,    -1,     3,    -1,    43,
      -1,    44,    -1,    44,    43,    -1,    14,    41,    10,    16,
      12,    45,    11,    -1,    14,    41,    10,    15,    12,    45,
      11,    -1,    14,    41,     7,    40,     8,    10,    15,    12,
      45,    11,    -1,    14,    41,    10,    15,    12,    45,    18,
      40,    11,    -1,    14,    41,     7,    40,     8,    10,    15,
      12,    45,    18,    40,    11,    -1,    14,    41,    10,    17,
      12,    45,    11,    -1,    40,    -1,    41,    -1,     6,    19,
       6,    -1,     6,    20,     6,    -1,     6,    41,     6,    -1,
      29,     7,    45,     8,    -1,    27,     7,    45,     8,    -1,
      30,     7,    45,     8,    -1,    28,     7,    45,     9,    45,
       8,    -1,    33,     7,    45,     8,    -1,    34,     7,    45,
       8,    -1,    32,     7,    45,     8,    -1,    31,     7,    45,
       8,    -1,    38,     7,    45,     9,    45,     9,    45,     8,
      -1,    37,     7,    45,     8,    -1,    36,     7,     8,    -1,
      45,    22,    45,    -1,    45,    21,    45,    -1,    45,    24,
      45,    -1,    45,    23,    45,    -1,    21,    45,    -1,    22,
      45,    -1,     7,    45,     8,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const unsigned char yyrline[] =
{
       0,    75,    75,    76,    80,    84,    88,    89,    93,    94,
      95,    96,    97,    99,   116,   122,   123,   124,   125,   126,
     127,   128,   129,   130,   131,   132,   133,   134,   135,   136,
     137,   138,   139,   140,   141,   142,   143
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals. */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "LID", "LDECIMAL", "LREAL", "LPIPE",
  "LPOPEN", "LPCLOSE", "LCOMMA", "LDDOT", "LDOTCOMMA", "LEQUAL", "LTIME",
  "LLET", "LSTATEVAR", "LFUNCTION", "LCONST", "LINIT", "LB1", "LB2",
  "LMINUS", "LPLUS", "LDIV", "LTIMES", "POS", "NEG", "LSQRT", "LPOW",
  "LLOG", "LEXP", "LASIN", "LACOS", "LSIN", "LCOS", "LINF", "LRAND", "LSG",
  "LSTEP", "$accept", "number", "id", "program", "dec_list", "dec", "exp", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const unsigned short int yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const unsigned char yyr1[] =
{
       0,    39,    40,    40,    41,    42,    43,    43,    44,    44,
      44,    44,    44,    44,    45,    45,    45,    45,    45,    45,
      45,    45,    45,    45,    45,    45,    45,    45,    45,    45,
      45,    45,    45,    45,    45,    45,    45
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const unsigned char yyr2[] =
{
       0,     2,     1,     1,     1,     1,     1,     2,     7,     7,
      10,     9,    12,     7,     1,     1,     3,     3,     3,     4,
       4,     4,     6,     4,     4,     4,     4,     8,     4,     3,
       3,     3,     3,     3,     2,     2,     3
};

/* YYDEFACT[STATE-NAME] -- Default rule to reduce with in state
   STATE-NUM when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const unsigned char yydefact[] =
{
       0,     0,     0,     5,     6,     4,     0,     1,     7,     0,
       0,     3,     2,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    14,    15,     0,
       0,     0,     0,     0,     0,     0,     0,    34,    35,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       9,     0,     0,     0,     0,     0,     8,    13,     0,    16,
      17,    18,    36,     0,     0,     0,     0,     0,     0,     0,
       0,    29,     0,     0,     0,    31,    30,    33,    32,     0,
      20,     0,    19,    21,    26,    25,    23,    24,    28,     0,
      11,    10,     0,     0,     0,     0,    22,     0,    12,     0,
      27
};

/* YYDEFGOTO[NTERM-NUM]. */
static const yysigned_char yydefgoto[] =
{
      -1,    37,    38,     2,     3,     4,    39
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -19
static const short int yypact[] =
{
      -3,    16,    22,   -19,    -3,   -19,     3,   -19,   -19,    13,
      -1,   -19,   -19,    15,    17,    18,    27,    33,    73,    73,
      73,    36,     1,    73,    73,    73,    42,    47,    52,    53,
      58,    64,    65,    67,    68,    75,    80,   -19,   -19,   160,
     174,   182,    76,    84,    85,    86,     4,   -19,   -19,    73,
      73,    73,    73,    73,    73,    73,    73,    88,    73,    73,
     -19,    13,    73,    73,    73,    73,   -19,   -19,    73,   -19,
     -19,   -19,   -19,    34,   133,    40,    45,    62,    91,   100,
     108,   -19,   112,   137,    87,   -15,   -15,   -19,   -19,   168,
     -19,    73,   -19,   -19,   -19,   -19,   -19,   -19,   -19,    73,
     -19,   -19,    13,   117,   153,   106,   -19,    73,   -19,   129,
     -19
};

/* YYPGOTO[NTERM-NUM].  */
static const yysigned_char yypgoto[] =
{
     -19,    -9,     2,   -19,    93,   -19,   -18
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -1
static const unsigned char yytable[] =
{
      13,    40,    41,     6,     5,    46,    47,    48,    64,    65,
       9,     1,    72,    10,    14,    15,    16,    11,    12,     5,
      43,    44,     7,    17,    45,    62,    63,    64,    65,    18,
      19,    73,    74,    75,    76,    77,    78,    79,    80,    20,
      82,    83,    90,    21,    85,    86,    87,    88,    92,    49,
      89,    42,    84,    93,    50,    62,    63,    64,    65,    51,
      52,    62,    63,    64,    65,    53,    62,    63,    64,    65,
      94,    54,    55,   103,    56,    57,     5,    11,    12,    22,
      23,   104,    58,    62,    63,    64,    65,    59,    68,   109,
      69,    70,    71,   105,    24,    25,    81,     8,   100,    95,
      26,    27,    28,    29,    30,    31,    32,    33,    96,    34,
      35,    36,    62,    63,    64,    65,    97,   108,     0,     0,
      98,    62,    63,    64,    65,   106,     0,     0,     0,    62,
      63,    64,    65,    62,    63,    64,    65,   110,    62,    63,
      64,    65,    91,     0,     0,     0,    99,     0,     0,     0,
      62,    63,    64,    65,    62,    63,    64,    65,    62,    63,
      64,    65,   107,     0,     0,     0,     0,     0,     0,     0,
       0,    60,     0,     0,    62,    63,    64,    65,    61,   101,
       0,    62,    63,    64,    65,    66,   102,     0,     0,    62,
      63,    64,    65,    67,     0,    62,    63,    64,    65,     0,
       0,     0,     0,    62,    63,    64,    65
};

static const yysigned_char yycheck[] =
{
       9,    19,    20,     1,     3,    23,    24,    25,    23,    24,
       7,    14,     8,    10,    15,    16,    17,     4,     5,     3,
      19,    20,     0,     8,    22,    21,    22,    23,    24,    12,
      12,    49,    50,    51,    52,    53,    54,    55,    56,    12,
      58,    59,     8,    10,    62,    63,    64,    65,     8,     7,
      68,    15,    61,     8,     7,    21,    22,    23,    24,     7,
       7,    21,    22,    23,    24,     7,    21,    22,    23,    24,
       8,     7,     7,    91,     7,     7,     3,     4,     5,     6,
       7,    99,     7,    21,    22,    23,    24,     7,    12,   107,
       6,     6,     6,   102,    21,    22,     8,     4,    11,     8,
      27,    28,    29,    30,    31,    32,    33,    34,     8,    36,
      37,    38,    21,    22,    23,    24,     8,    11,    -1,    -1,
       8,    21,    22,    23,    24,     8,    -1,    -1,    -1,    21,
      22,    23,    24,    21,    22,    23,    24,     8,    21,    22,
      23,    24,     9,    -1,    -1,    -1,     9,    -1,    -1,    -1,
      21,    22,    23,    24,    21,    22,    23,    24,    21,    22,
      23,    24,     9,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    11,    -1,    -1,    21,    22,    23,    24,    18,    11,
      -1,    21,    22,    23,    24,    11,    18,    -1,    -1,    21,
      22,    23,    24,    11,    -1,    21,    22,    23,    24,    -1,
      -1,    -1,    -1,    21,    22,    23,    24
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const unsigned char yystos[] =
{
       0,    14,    42,    43,    44,     3,    41,     0,    43,     7,
      10,     4,     5,    40,    15,    16,    17,     8,    12,    12,
      12,    10,     6,     7,    21,    22,    27,    28,    29,    30,
      31,    32,    33,    34,    36,    37,    38,    40,    41,    45,
      45,    45,    15,    19,    20,    41,    45,    45,    45,     7,
       7,     7,     7,     7,     7,     7,     7,     7,     7,     7,
      11,    18,    21,    22,    23,    24,    11,    11,    12,     6,
       6,     6,     8,    45,    45,    45,    45,    45,    45,    45,
      45,     8,    45,    45,    40,    45,    45,    45,    45,    45,
       8,     9,     8,     8,     8,     8,     8,     8,     8,     9,
      11,    11,    18,    45,    45,    40,     8,     9,    11,    45,
       8
};

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */

#define YYFAIL		goto yyerrlab

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yytoken = YYTRANSLATE (yychar);				\
      YYPOPSTACK;						\
      goto yybackup;						\
    }								\
  else								\
    {								\
      yyerror (&yylloc, parser, scanner, YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (0)


#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#define YYRHSLOC(Rhs, K) ((Rhs)[K])
#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)				\
    do									\
      if (N)								\
	{								\
	  (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;	\
	  (Current).first_column = YYRHSLOC (Rhs, 1).first_column;	\
	  (Current).last_line    = YYRHSLOC (Rhs, N).last_line;		\
	  (Current).last_column  = YYRHSLOC (Rhs, N).last_column;	\
	}								\
      else								\
	{								\
	  (Current).first_line   = (Current).last_line   =		\
	    YYRHSLOC (Rhs, 0).last_line;				\
	  (Current).first_column = (Current).last_column =		\
	    YYRHSLOC (Rhs, 0).last_column;				\
	}								\
    while (0)
#endif


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if YYLTYPE_IS_TRIVIAL
#  define YY_LOCATION_PRINT(File, Loc)			\
     fprintf (File, "%d.%d-%d.%d",			\
              (Loc).first_line, (Loc).first_column,	\
              (Loc).last_line,  (Loc).last_column)
# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX yylex (&yylval, &yylloc, YYLEX_PARAM)
#else
# define YYLEX yylex (&yylval, &yylloc, scanner)
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (0)

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)		\
do {								\
  if (yydebug)							\
    {								\
      YYFPRINTF (stderr, "%s ", Title);				\
      yysymprint (stderr,					\
                  Type, Value, Location);	\
      YYFPRINTF (stderr, "\n");					\
    }								\
} while (0)

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yy_stack_print (short int *bottom, short int *top)
#else
static void
yy_stack_print (bottom, top)
    short int *bottom;
    short int *top;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (/* Nothing. */; bottom <= top; ++bottom)
    YYFPRINTF (stderr, " %d", *bottom);
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yy_reduce_print (int yyrule)
#else
static void
yy_reduce_print (yyrule)
    int yyrule;
#endif
{
  int yyi;
  unsigned long int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu), ",
             yyrule - 1, yylno);
  /* Print the symbols being reduced, and their result.  */
  for (yyi = yyprhs[yyrule]; 0 <= yyrhs[yyi]; yyi++)
    YYFPRINTF (stderr, "%s ", yytname[yyrhs[yyi]]);
  YYFPRINTF (stderr, "-> %s\n", yytname[yyr1[yyrule]]);
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (Rule);		\
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif



#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined (__GLIBC__) && defined (_STRING_H)
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
#   if defined (__STDC__) || defined (__cplusplus)
yystrlen (const char *yystr)
#   else
yystrlen (yystr)
     const char *yystr;
#   endif
{
  const char *yys = yystr;

  while (*yys++ != '\0')
    continue;

  return yys - yystr - 1;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined (__GLIBC__) && defined (_STRING_H) && defined (_GNU_SOURCE)
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
#   if defined (__STDC__) || defined (__cplusplus)
yystpcpy (char *yydest, const char *yysrc)
#   else
yystpcpy (yydest, yysrc)
     char *yydest;
     const char *yysrc;
#   endif
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      size_t yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

#endif /* YYERROR_VERBOSE */



#if YYDEBUG
/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yysymprint (FILE *yyoutput, int yytype, YYSTYPE *yyvaluep, YYLTYPE *yylocationp)
#else
static void
yysymprint (yyoutput, yytype, yyvaluep, yylocationp)
    FILE *yyoutput;
    int yytype;
    YYSTYPE *yyvaluep;
    YYLTYPE *yylocationp;
#endif
{
  /* Pacify ``unused variable'' warnings.  */
  (void) yyvaluep;
  (void) yylocationp;

  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  YY_LOCATION_PRINT (yyoutput, *yylocationp);
  YYFPRINTF (yyoutput, ": ");

# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  switch (yytype)
    {
      default:
        break;
    }
  YYFPRINTF (yyoutput, ")");
}

#endif /* ! YYDEBUG */
/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep, YYLTYPE *yylocationp)
#else
static void
yydestruct (yymsg, yytype, yyvaluep, yylocationp)
    const char *yymsg;
    int yytype;
    YYSTYPE *yyvaluep;
    YYLTYPE *yylocationp;
#endif
{
  /* Pacify ``unused variable'' warnings.  */
  (void) yyvaluep;
  (void) yylocationp;

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  switch (yytype)
    {

      default:
        break;
    }
}


/* Prevent warnings from -Wmissing-prototypes.  */

#ifdef YYPARSE_PARAM
# if defined (__STDC__) || defined (__cplusplus)
int yyparse (void *YYPARSE_PARAM);
# else
int yyparse ();
# endif
#else /* ! YYPARSE_PARAM */
#if defined (__STDC__) || defined (__cplusplus)
int yyparse (FuncParser* parser, yyscan_t scanner);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */






/*----------.
| yyparse.  |
`----------*/

#ifdef YYPARSE_PARAM
# if defined (__STDC__) || defined (__cplusplus)
int yyparse (void *YYPARSE_PARAM)
# else
int yyparse (YYPARSE_PARAM)
  void *YYPARSE_PARAM;
# endif
#else /* ! YYPARSE_PARAM */
#if defined (__STDC__) || defined (__cplusplus)
int
yyparse (FuncParser* parser, yyscan_t scanner)
#else
int
yyparse (parser, scanner)
    FuncParser* parser;
    yyscan_t scanner;
#endif
#endif
{
  /* The look-ahead symbol.  */
int yychar;

/* The semantic value of the look-ahead symbol.  */
YYSTYPE yylval;

/* Number of syntax errors so far.  */
int yynerrs;
/* Location data for the look-ahead symbol.  */
YYLTYPE yylloc;

  int yystate;
  int yyn;
  int yyresult;
  /* Number of tokens to shift before error messages enabled.  */
  int yyerrstatus;
  /* Look-ahead token as an internal (translated) token number.  */
  int yytoken = 0;

  /* Three stacks and their tools:
     `yyss': related to states,
     `yyvs': related to semantic values,
     `yyls': related to locations.

     Refer to the stacks thru separate pointers, to allow yyoverflow
     to reallocate them elsewhere.  */

  /* The state stack.  */
  short int yyssa[YYINITDEPTH];
  short int *yyss = yyssa;
  short int *yyssp;

  /* The semantic value stack.  */
  YYSTYPE yyvsa[YYINITDEPTH];
  YYSTYPE *yyvs = yyvsa;
  YYSTYPE *yyvsp;

  /* The location stack.  */
  YYLTYPE yylsa[YYINITDEPTH];
  YYLTYPE *yyls = yylsa;
  YYLTYPE *yylsp;
  /* The locations where the error started and ended. */
  YYLTYPE yyerror_range[2];

#define YYPOPSTACK   (yyvsp--, yyssp--, yylsp--)

  YYSIZE_T yystacksize = YYINITDEPTH;

  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;
  YYLTYPE yyloc;

  /* When reducing, the number of symbols on the RHS of the reduced
     rule.  */
  int yylen;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss;
  yyvsp = yyvs;
  yylsp = yyls;
#if YYLTYPE_IS_TRIVIAL
  /* Initialize the default location before parsing starts.  */
  yylloc.first_line   = yylloc.last_line   = 1;
  yylloc.first_column = yylloc.last_column = 0;
#endif

  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed. so pushing a state here evens the stacks.
     */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack. Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	short int *yyss1 = yyss;
	YYLTYPE *yyls1 = yyls;

	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow (YY_("memory exhausted"),
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),
		    &yyls1, yysize * sizeof (*yylsp),
		    &yystacksize);
	yyls = yyls1;
	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	short int *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyexhaustedlab;
	YYSTACK_RELOCATE (yyss);
	YYSTACK_RELOCATE (yyvs);
	YYSTACK_RELOCATE (yyls);
#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;
      yylsp = yyls + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

/* Do appropriate processing given the current state.  */
/* Read a look-ahead token if we need one and don't already have one.  */
/* yyresume: */

  /* First try to decide what to do without reference to look-ahead token.  */

  yyn = yypact[yystate];
  if (yyn == YYPACT_NINF)
    goto yydefault;

  /* Not known => get a look-ahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid look-ahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yyn == 0 || yyn == YYTABLE_NINF)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Shift the look-ahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the token being shifted unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  *++yyvsp = yylval;
  *++yylsp = yylloc;

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  yystate = yyn;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];

  /* Default location. */
  YYLLOC_DEFAULT (yyloc, yylsp - yylen, yylen);
  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 75 "FuncParser.y"
    { (yyval.ptr_string) = (yyvsp[0].ptr_string); ;}
    break;

  case 3:
#line 76 "FuncParser.y"
    { (yyval.ptr_string) = (yyvsp[0].ptr_string); ;}
    break;

  case 4:
#line 80 "FuncParser.y"
    { (yyval.ptr_string) = (yyvsp[0].ptr_string); ;}
    break;

  case 8:
#line 93 "FuncParser.y"
    { parser->control_flag &= parser->symbolTable->AddFunction(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), *(yyvsp[-5].ptr_string), (yyvsp[-1].expression)); delete((yyvsp[-5].ptr_string)); ;}
    break;

  case 9:
#line 94 "FuncParser.y"
    { parser->control_flag &= parser->symbolTable->AddStatevar(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), *(yyvsp[-5].ptr_string), (yyvsp[-1].expression)); delete((yyvsp[-5].ptr_string)); ;}
    break;

  case 10:
#line 95 "FuncParser.y"
    { parser->control_flag &= parser->symbolTable->AddStatevar(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), *(yyvsp[-8].ptr_string), *(yyvsp[-6].ptr_string), (yyvsp[-1].expression)); delete((yyvsp[-8].ptr_string)); delete((yyvsp[-6].ptr_string)); ;}
    break;

  case 11:
#line 96 "FuncParser.y"
    { parser->control_flag &= parser->symbolTable->AddStatevar(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), *(yyvsp[-7].ptr_string), (yyvsp[-3].expression), *(yyvsp[-1].ptr_string)); delete((yyvsp[-7].ptr_string)); delete((yyvsp[-1].ptr_string)); ;}
    break;

  case 12:
#line 98 "FuncParser.y"
    { parser->control_flag &= parser->symbolTable->AddStatevar(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), *(yyvsp[-10].ptr_string), *(yyvsp[-8].ptr_string), (yyvsp[-3].expression), *(yyvsp[-1].ptr_string)); delete((yyvsp[-10].ptr_string)); delete((yyvsp[-8].ptr_string)); delete((yyvsp[-1].ptr_string)); ;}
    break;

  case 13:
#line 99 "FuncParser.y"
    {
                                                                           double value;
                                                                           Pos pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename());
                                                                           
                                                                           if ((yyvsp[-1].expression)->EvalConst(value)) 
                                                                           {
                                                                              parser->control_flag &= parser->symbolTable->AddConstant(pos, *(yyvsp[-5].ptr_string), value);
                                                                           }
                                                                           else
                                                                           {
                                                                              parser->control_flag = false;
                                                                              Error_Manager::PrintError(pos, 31, "expression is not constant. Check expression members.");
                                                                           }
                                                                        ;}
    break;

  case 14:
#line 116 "FuncParser.y"
    { 
                                                                  double num = (double)(atof((*(yyvsp[0].ptr_string)).c_str()));  
                                                                  delete((yyvsp[0].ptr_string)); 
                                                                  (yyval.expression) = new ExprNumber(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), parser->symbolTable, num); 
                                                               ;}
    break;

  case 15:
#line 122 "FuncParser.y"
    { (yyval.expression) = new ExprVar(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), parser->symbolTable, *(yyvsp[0].ptr_string)); delete((yyvsp[0].ptr_string)); ;}
    break;

  case 16:
#line 123 "FuncParser.y"
    { (yyval.expression) = new ExprConcentration(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), parser->symbolTable, "$B1"); ;}
    break;

  case 17:
#line 124 "FuncParser.y"
    { (yyval.expression) = new ExprConcentration(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), parser->symbolTable, "$B2"); ;}
    break;

  case 18:
#line 125 "FuncParser.y"
    { (yyval.expression) = new ExprConcentration(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), parser->symbolTable, *(yyvsp[-1].ptr_string)); delete((yyvsp[-1].ptr_string)); ;}
    break;

  case 19:
#line 126 "FuncParser.y"
    { (yyval.expression) = new ExprLog(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), parser->symbolTable, (yyvsp[-1].expression)); ;}
    break;

  case 20:
#line 127 "FuncParser.y"
    { (yyval.expression) = new ExprSqrt(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), parser->symbolTable, (yyvsp[-1].expression)); ;}
    break;

  case 21:
#line 128 "FuncParser.y"
    { (yyval.expression) = new ExprExp(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), parser->symbolTable, (yyvsp[-1].expression)); ;}
    break;

  case 22:
#line 129 "FuncParser.y"
    { (yyval.expression) = new ExprPow(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), parser->symbolTable, (yyvsp[-3].expression), (yyvsp[-1].expression)); ;}
    break;

  case 23:
#line 130 "FuncParser.y"
    { (yyval.expression) = new ExprSin(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), parser->symbolTable, (yyvsp[-1].expression)); ;}
    break;

  case 24:
#line 131 "FuncParser.y"
    { (yyval.expression) = new ExprCos(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), parser->symbolTable, (yyvsp[-1].expression)); ;}
    break;

  case 25:
#line 132 "FuncParser.y"
    { (yyval.expression) = new ExprACos(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), parser->symbolTable, (yyvsp[-1].expression)); ;}
    break;

  case 26:
#line 133 "FuncParser.y"
    { (yyval.expression) = new ExprASin(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), parser->symbolTable, (yyvsp[-1].expression)); ;}
    break;

  case 27:
#line 134 "FuncParser.y"
    { (yyval.expression) = new ExprStep(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), parser->symbolTable, (yyvsp[-5].expression), (yyvsp[-3].expression), (yyvsp[-1].expression)); ;}
    break;

  case 28:
#line 135 "FuncParser.y"
    { (yyval.expression) = new ExprSg(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), parser->symbolTable, (yyvsp[-1].expression)); ;}
    break;

  case 29:
#line 136 "FuncParser.y"
    { (yyval.expression) = new ExprRand(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), parser->symbolTable); ;}
    break;

  case 30:
#line 137 "FuncParser.y"
    { (yyval.expression) = new ExprOp(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), parser->symbolTable, (yyvsp[-2].expression), (yyvsp[0].expression), EXPR_PLUS); ;}
    break;

  case 31:
#line 138 "FuncParser.y"
    { (yyval.expression) = new ExprOp(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), parser->symbolTable, (yyvsp[-2].expression), (yyvsp[0].expression), EXPR_MINUS); ;}
    break;

  case 32:
#line 139 "FuncParser.y"
    { (yyval.expression) = new ExprOp(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), parser->symbolTable, (yyvsp[-2].expression), (yyvsp[0].expression), EXPR_TIMES); ;}
    break;

  case 33:
#line 140 "FuncParser.y"
    { (yyval.expression) = new ExprOp(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), parser->symbolTable, (yyvsp[-2].expression), (yyvsp[0].expression), EXPR_DIV); ;}
    break;

  case 34:
#line 141 "FuncParser.y"
    { (yyval.expression) = new ExprNeg(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), parser->symbolTable, (yyvsp[0].expression)); ;}
    break;

  case 35:
#line 142 "FuncParser.y"
    { (yyval.expression) = (yyvsp[0].expression); ;}
    break;

  case 36:
#line 143 "FuncParser.y"
    { (yyval.expression) = (yyvsp[-1].expression); ;}
    break;


      default: break;
    }

/* Line 1126 of yacc.c.  */
#line 1514 "FuncParser.cpp"

  yyvsp -= yylen;
  yyssp -= yylen;
  yylsp -= yylen;

  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;
  *++yylsp = yyloc;

  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if YYERROR_VERBOSE
      yyn = yypact[yystate];

      if (YYPACT_NINF < yyn && yyn < YYLAST)
	{
	  int yytype = YYTRANSLATE (yychar);
	  YYSIZE_T yysize0 = yytnamerr (0, yytname[yytype]);
	  YYSIZE_T yysize = yysize0;
	  YYSIZE_T yysize1;
	  int yysize_overflow = 0;
	  char *yymsg = 0;
#	  define YYERROR_VERBOSE_ARGS_MAXIMUM 5
	  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
	  int yyx;

#if 0
	  /* This is so xgettext sees the translatable formats that are
	     constructed on the fly.  */
	  YY_("syntax error, unexpected %s");
	  YY_("syntax error, unexpected %s, expecting %s");
	  YY_("syntax error, unexpected %s, expecting %s or %s");
	  YY_("syntax error, unexpected %s, expecting %s or %s or %s");
	  YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s");
#endif
	  char *yyfmt;
	  char const *yyf;
	  static char const yyunexpected[] = "syntax error, unexpected %s";
	  static char const yyexpecting[] = ", expecting %s";
	  static char const yyor[] = " or %s";
	  char yyformat[sizeof yyunexpected
			+ sizeof yyexpecting - 1
			+ ((YYERROR_VERBOSE_ARGS_MAXIMUM - 2)
			   * (sizeof yyor - 1))];
	  char const *yyprefix = yyexpecting;

	  /* Start YYX at -YYN if negative to avoid negative indexes in
	     YYCHECK.  */
	  int yyxbegin = yyn < 0 ? -yyn : 0;

	  /* Stay within bounds of both yycheck and yytname.  */
	  int yychecklim = YYLAST - yyn;
	  int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
	  int yycount = 1;

	  yyarg[0] = yytname[yytype];
	  yyfmt = yystpcpy (yyformat, yyunexpected);

	  for (yyx = yyxbegin; yyx < yyxend; ++yyx)
	    if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
	      {
		if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
		  {
		    yycount = 1;
		    yysize = yysize0;
		    yyformat[sizeof yyunexpected - 1] = '\0';
		    break;
		  }
		yyarg[yycount++] = yytname[yyx];
		yysize1 = yysize + yytnamerr (0, yytname[yyx]);
		yysize_overflow |= yysize1 < yysize;
		yysize = yysize1;
		yyfmt = yystpcpy (yyfmt, yyprefix);
		yyprefix = yyor;
	      }

	  yyf = YY_(yyformat);
	  yysize1 = yysize + yystrlen (yyf);
	  yysize_overflow |= yysize1 < yysize;
	  yysize = yysize1;

	  if (!yysize_overflow && yysize <= YYSTACK_ALLOC_MAXIMUM)
	    yymsg = (char *) YYSTACK_ALLOC (yysize);
	  if (yymsg)
	    {
	      /* Avoid sprintf, as that infringes on the user's name space.
		 Don't have undefined behavior even if the translation
		 produced a string with the wrong number of "%s"s.  */
	      char *yyp = yymsg;
	      int yyi = 0;
	      while ((*yyp = *yyf))
		{
		  if (*yyp == '%' && yyf[1] == 's' && yyi < yycount)
		    {
		      yyp += yytnamerr (yyp, yyarg[yyi++]);
		      yyf += 2;
		    }
		  else
		    {
		      yyp++;
		      yyf++;
		    }
		}
	      yyerror (&yylloc, parser, scanner, yymsg);
	      YYSTACK_FREE (yymsg);
	    }
	  else
	    {
	      yyerror (&yylloc, parser, scanner, YY_("syntax error"));
	      goto yyexhaustedlab;
	    }
	}
      else
#endif /* YYERROR_VERBOSE */
	yyerror (&yylloc, parser, scanner, YY_("syntax error"));
    }

  yyerror_range[0] = yylloc;

  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse look-ahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
        {
	  /* Return failure if at end of input.  */
	  if (yychar == YYEOF)
	    YYABORT;
        }
      else
	{
	  yydestruct ("Error: discarding", yytoken, &yylval, &yylloc);
	  yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse look-ahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (0)
     goto yyerrorlab;

  yyerror_range[0] = yylsp[1-yylen];
  yylsp -= yylen;
  yyvsp -= yylen;
  yyssp -= yylen;
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (yyn != YYPACT_NINF)
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;

      yyerror_range[0] = *yylsp;
      yydestruct ("Error: popping", yystos[yystate], yyvsp, yylsp);
      YYPOPSTACK;
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  *++yyvsp = yylval;

  yyerror_range[1] = yylloc;
  /* Using YYLLOC is tempting, but would change the location of
     the look-ahead.  YYLOC is available though. */
  YYLLOC_DEFAULT (yyloc, yyerror_range - 1, 2);
  *++yylsp = yyloc;

  /* Shift the error token. */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#ifndef yyoverflow
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (&yylloc, parser, scanner, YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEOF && yychar != YYEMPTY)
     yydestruct ("Cleanup: discarding lookahead",
		 yytoken, &yylval, &yylloc);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
		  yystos[*yyssp], yyvsp, yylsp);
      YYPOPSTACK;
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
  return yyresult;
}


#line 147 "FuncParser.y"


// Syntax error message 
void func_error(YYLTYPE *llocp, FuncParser* parser, yyscan_t scanner, char const* msg)
{
   Pos pos(llocp->first_line, llocp->first_column, parser->currentFilename);
   Error_Manager::PrintError(pos, 0, msg);
}

// Parse the func file with name file_name
int FuncParser::Parse() 
{
   int res;
   control_flag = true;
      
   symbolTable->ResetFuncMap();
   const char* file_name = currentFilename.c_str();
   
   FILE* func_file = fopen(file_name,"r+");
   if (func_file == NULL) 
   {
      Error_Manager::PrintError(0, "Cannot open '" + currentFilename + "'");
      return -1;   
   }
   
   // Initiliaze reentrant flex scanner   
   yyscan_t scanner;
   func_lex_init(&scanner);
   func_set_in(func_file, scanner);
   res = func_parse(this, scanner);
   
   if (control_flag == false)
      return -1;
   
   func_lex_destroy(scanner);
   fclose(func_file);
   return res;
}

#ifdef _MSC_VER
#pragma warning(pop)
#pragma warning(default : 4706)
#pragma warning(default : 4702)
#pragma warning(default : 4505)
#endif


