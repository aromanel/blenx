
#ifndef FUNC_PARSER_H_INCLUDED
#define FUNC_PARSER_H_INCLUDED

#include "FunBase.h"
#include "Symbol_Table.h"
#include "FuncMap.h"

class FuncParser
{
public:
   bool control_flag;

   
   // A reference to whatever is required in symboltable to hold:
   //1) symbol name
   //2) symbol type (statevar or function)
   //3) FunBase*
   //std::vector<FunBase*>& functions;
   SymbolTable* symbolTable;

   std::string currentFilename;

private:
   //Disable copy
   FuncParser (const FuncParser& other);
   FuncParser& operator=(const FuncParser& other);

public:

   FuncParser (SymbolTable* symbol_table, const std::string& filename)
      : symbolTable(symbol_table), currentFilename(filename) { }

   int Parse();

   std::string& GetCurrentFilename() { return currentFilename; }
};


#endif //FUNC_PARSER_H_INCLUDED
