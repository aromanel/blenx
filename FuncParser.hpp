/* A Bison parser, made by GNU Bison 2.1.  */

/* Skeleton parser for Yacc-like parsing with Bison,
   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     LID = 258,
     LDECIMAL = 259,
     LREAL = 260,
     LPIPE = 261,
     LPOPEN = 262,
     LPCLOSE = 263,
     LCOMMA = 264,
     LDDOT = 265,
     LDOTCOMMA = 266,
     LEQUAL = 267,
     LTIME = 268,
     LLET = 269,
     LSTATEVAR = 270,
     LFUNCTION = 271,
     LCONST = 272,
     LINIT = 273,
     LB1 = 274,
     LB2 = 275,
     LMINUS = 276,
     LPLUS = 277,
     LDIV = 278,
     LTIMES = 279,
     POS = 280,
     NEG = 281,
     LSQRT = 282,
     LPOW = 283,
     LLOG = 284,
     LEXP = 285,
     LASIN = 286,
     LACOS = 287,
     LSIN = 288,
     LCOS = 289,
     LINF = 290,
     LRAND = 291,
     LSG = 292,
     LSTEP = 293
   };
#endif
/* Tokens.  */
#define LID 258
#define LDECIMAL 259
#define LREAL 260
#define LPIPE 261
#define LPOPEN 262
#define LPCLOSE 263
#define LCOMMA 264
#define LDDOT 265
#define LDOTCOMMA 266
#define LEQUAL 267
#define LTIME 268
#define LLET 269
#define LSTATEVAR 270
#define LFUNCTION 271
#define LCONST 272
#define LINIT 273
#define LB1 274
#define LB2 275
#define LMINUS 276
#define LPLUS 277
#define LDIV 278
#define LTIMES 279
#define POS 280
#define NEG 281
#define LSQRT 282
#define LPOW 283
#define LLOG 284
#define LEXP 285
#define LASIN 286
#define LACOS 287
#define LSIN 288
#define LCOS 289
#define LINF 290
#define LRAND 291
#define LSG 292
#define LSTEP 293




#if ! defined (YYSTYPE) && ! defined (YYSTYPE_IS_DECLARED)
#line 37 "FuncParser.y"
typedef union YYSTYPE {
   std::string* ptr_string;
   Expr* expression;
} YYSTYPE;
/* Line 1447 of yacc.c.  */
#line 119 "FuncParser.hpp"
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif



#if ! defined (YYLTYPE) && ! defined (YYLTYPE_IS_DECLARED)
typedef struct YYLTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
} YYLTYPE;
# define yyltype YYLTYPE /* obsolescent; will be withdrawn */
# define YYLTYPE_IS_DECLARED 1
# define YYLTYPE_IS_TRIVIAL 1
#endif




