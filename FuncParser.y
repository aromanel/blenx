//Includes and local variable 


%{
  #include <string>
  #include "Define.h"
  #include "Error_Manager.h"
  #include "FuncMap.h"
  
  #include "Expr.h"
  #include "FuncParser.h"
  #include "FuncParser.hpp"
 
  using namespace std;
   
  //FuncLexer forward definitions
  typedef void* yyscan_t;
  int  func_lex(YYSTYPE *lvalp, YYLTYPE *llocp, yyscan_t scanner);
  void func_set_in(FILE * in_str, yyscan_t scanner);
  int  func_lex_init (yyscan_t* scanner);
  int  func_lex_destroy (yyscan_t yyscanner);
  
  void func_error(YYLTYPE *llocp, FuncParser* parser, yyscan_t scanner, char const* msg);
    
#ifdef _MSC_VER
#pragma warning(push,1)
#pragma warning(disable: 4706)
#pragma warning(disable: 4702)
#pragma warning(disable: 4505)
#endif
  
%}

/* Definizione della Sintassi e delle azioni semantiche */

%union
{
   std::string* ptr_string;
   Expr* expression;
};

%error-verbose
%pure-parser
%locations
%name-prefix="func_"
%parse-param { FuncParser* parser }
%parse-param { yyscan_t scanner }
%lex-param { yyscan_t scanner }


%token <ptr_string> LID 
%token <ptr_string> LDECIMAL
%token <ptr_string> LREAL

%token LPIPE LPOPEN LPCLOSE LCOMMA LDDOT LDOTCOMMA LEQUAL LTIME LLET
%token LSTATEVAR LFUNCTION LCONST LINIT
%token LB1 LB2

%left LPLUS LMINUS
%left LTIMES LDIV
%left NEG POS    /* unary minus and plus */
%right LEXP LLOG LPOW LSQRT
%right LCOS LSIN LACOS LASIN
%right LSTEP LSG LRAND LINF

%type <ptr_string> number
%type <ptr_string> id
%type <expression> exp

%start program

%%

number : 
     LREAL                                                     { $$ = $1; }
   | LDECIMAL                                                  { $$ = $1; }
   ;

id :
     LID                                                       { $$ = $1; }
   ;

program : 
     dec_list													            
   ;

dec_list: 
     dec                                                       
   | dec dec_list                                              
   ;

dec :
     LLET id LDDOT LFUNCTION LEQUAL exp LDOTCOMMA                       { parser->control_flag &= parser->symbolTable->AddFunction(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), *$2, $6); delete($2); }
   | LLET id LDDOT LSTATEVAR LEQUAL exp LDOTCOMMA                       { parser->control_flag &= parser->symbolTable->AddStatevar(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), *$2, $6); delete($2); }   
   | LLET id LPOPEN number LPCLOSE LDDOT LSTATEVAR LEQUAL exp LDOTCOMMA { parser->control_flag &= parser->symbolTable->AddStatevar(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), *$2, *$4, $9); delete($2); delete($4); }   
   | LLET id LDDOT LSTATEVAR LEQUAL exp LINIT number LDOTCOMMA          { parser->control_flag &= parser->symbolTable->AddStatevar(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), *$2, $6, *$8); delete($2); delete($8); }   
   | LLET id LPOPEN number LPCLOSE LDDOT LSTATEVAR LEQUAL exp 
             LINIT number LDOTCOMMA                                     { parser->control_flag &= parser->symbolTable->AddStatevar(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), *$2, *$4, $9, *$11); delete($2); delete($4); delete($11); }   
   | LLET id LDDOT LCONST LEQUAL exp LDOTCOMMA                          {
                                                                           double value;
                                                                           Pos pos(@$.first_line, @$.first_column, parser->GetCurrentFilename());
                                                                           
                                                                           if ($6->EvalConst(value)) 
                                                                           {
                                                                              parser->control_flag &= parser->symbolTable->AddConstant(pos, *$2, value);
                                                                           }
                                                                           else
                                                                           {
                                                                              parser->control_flag = false;
                                                                              Error_Manager::PrintError(pos, 31, "expression is not constant. Check expression members.");
                                                                           }
                                                                        }
   ;
   
exp:
     number                                                    { 
                                                                  double num = (double)(atof((*$1).c_str()));  
                                                                  delete($1); 
                                                                  $$ = new ExprNumber(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), parser->symbolTable, num); 
                                                               }
//   | LINF                                                      { $$ = new ExprNumber(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), parser->symbolTable, HUGE_VAL); }
   | id                                                        { $$ = new ExprVar(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), parser->symbolTable, *$1); delete($1); }
   | LPIPE LB1 LPIPE                                           { $$ = new ExprConcentration(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), parser->symbolTable, "$B1"); }
   | LPIPE LB2 LPIPE                                           { $$ = new ExprConcentration(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), parser->symbolTable, "$B2"); }
   | LPIPE id LPIPE                                            { $$ = new ExprConcentration(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), parser->symbolTable, *$2); delete($2); }
   | LLOG LPOPEN exp LPCLOSE                                   { $$ = new ExprLog(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), parser->symbolTable, $3); }
   | LSQRT LPOPEN exp LPCLOSE                                  { $$ = new ExprSqrt(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), parser->symbolTable, $3); }
   | LEXP LPOPEN exp LPCLOSE                                   { $$ = new ExprExp(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), parser->symbolTable, $3); }
   | LPOW LPOPEN exp LCOMMA exp LPCLOSE                        { $$ = new ExprPow(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), parser->symbolTable, $3, $5); }
   | LSIN LPOPEN exp LPCLOSE                                   { $$ = new ExprSin(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), parser->symbolTable, $3); }   
   | LCOS LPOPEN exp LPCLOSE                                   { $$ = new ExprCos(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), parser->symbolTable, $3); }   
   | LACOS LPOPEN exp LPCLOSE                                  { $$ = new ExprACos(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), parser->symbolTable, $3); }   
   | LASIN LPOPEN exp LPCLOSE                                  { $$ = new ExprASin(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), parser->symbolTable, $3); }   
   | LSTEP LPOPEN exp LCOMMA exp LCOMMA exp LPCLOSE            { $$ = new ExprStep(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), parser->symbolTable, $3, $5, $7); }
   | LSG LPOPEN exp LPCLOSE                                    { $$ = new ExprSg(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), parser->symbolTable, $3); }   
   | LRAND LPOPEN LPCLOSE                                      { $$ = new ExprRand(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), parser->symbolTable); }   
   | exp LPLUS exp                                             { $$ = new ExprOp(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), parser->symbolTable, $1, $3, EXPR_PLUS); }
   | exp LMINUS exp                                            { $$ = new ExprOp(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), parser->symbolTable, $1, $3, EXPR_MINUS); }
   | exp LTIMES exp                                            { $$ = new ExprOp(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), parser->symbolTable, $1, $3, EXPR_TIMES); }
   | exp LDIV exp                                              { $$ = new ExprOp(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), parser->symbolTable, $1, $3, EXPR_DIV); }
   | LMINUS exp      %prec NEG                                 { $$ = new ExprNeg(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), parser->symbolTable, $2); }
   | LPLUS exp       %prec POS                                 { $$ = $2; }
   | LPOPEN exp LPCLOSE                                        { $$ = $2; }
   ;
     

%%

// Syntax error message 
void func_error(YYLTYPE *llocp, FuncParser* parser, yyscan_t scanner, char const* msg)
{
   Pos pos(llocp->first_line, llocp->first_column, parser->currentFilename);
   Error_Manager::PrintError(pos, 0, msg);
}

// Parse the func file with name file_name
int FuncParser::Parse() 
{
   int res;
   control_flag = true;
      
   symbolTable->ResetFuncMap();
   const char* file_name = currentFilename.c_str();
   
   FILE* func_file = fopen(file_name,"r+");
   if (func_file == NULL) 
   {
      Error_Manager::PrintError(0, "Cannot open '" + currentFilename + "'");
      return -1;   
   }
   
   // Initiliaze reentrant flex scanner   
   yyscan_t scanner;
   func_lex_init(&scanner);
   func_set_in(func_file, scanner);
   res = func_parse(this, scanner);
   
   if (control_flag == false)
      return -1;
   
   func_lex_destroy(scanner);
   fclose(func_file);
   return res;
}

#ifdef _MSC_VER
#pragma warning(pop)
#pragma warning(default : 4706)
#pragma warning(default : 4702)
#pragma warning(default : 4505)
#endif

