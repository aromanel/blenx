#include "GDManager.h"
#include "Environment.h"

///////////////////////////////////////////////////////////////////////////////////////////
// GDBase Class
///////////////////////////////////////////////////////////////////////////////////////////

#define MIN_RESIZE 100


//
bool GDBase_Predicate(GDBase *left, GDBase *right) {
	return (left->GetMinTime() < right->GetMinTime());
}

//
GDBase::GDBase(Map_Elem *p_action, DistrType p_type, GDManager *p_myManager)
{
	action = p_action;
	type = p_type;
	myManager = p_myManager;
	time = HUGE_VAL;
	inf_times = 0;
	time_iter = times.end();
}

//
GDBase::~GDBase() {}

//
void GDBase::AddTimes(int number,double current_time)
{
	double current;
	for( int i = 0 ; i < number ; i++ )
	{
		current = GetNumb();
		times.push_back(current+current_time);
	}

	SetMinTime();
}

//
bool GDBase::DeleteTimes(int number)
{
	int rn;
	if ( number > (int)times.size() )
	{
		return false;
	}

	for( int i = 0 ; i < number ; i++ )
	{
		// Genera numero da 0 a size()-1
		size_t val = times.size() - inf_times;
		rn = this->myManager->GetEnv()->Random0UptoN((int)val) + inf_times;
		// Metto in testa il tempo infinito
		times[rn] = times[inf_times];
		times[inf_times] = HUGE_VAL;
		inf_times++;
	}

	// When more than half of times is HUGE_VAL resize the vector
	// by eliminating infinite times
	// MIN_RESIZE is the minimal length for resize
	if ( ( inf_times * 2 ) > times.size() && times.size() > MIN_RESIZE )
	{
		Resize();
	}

	SetMinTime();

	return true;
}


//
void GDBase::SetMinTime()
{
	time = HUGE_VAL;
	std::vector<double>::iterator i;
	for( i = times.begin() + inf_times ; i != times.end() ; i++ )
	{
		if ( (*i) < time )
		{
			time = (*i);
			time_iter = i;
		}
	}
}

//
void GDBase::Resize()
{
	times.erase(times.begin(),times.begin()+inf_times);
	inf_times = 0;
}

//
double GDBase::GetMinTime() 
{
	return time;
}

//
Element *GDBase::GetAction()
{
	return action->action;
}

//
void GDBase::Update(double current_time) 
{
	if ( action->action->GetTotal() == 0 )
	{
		time = HUGE_VAL;
		times.clear();
	}
	else
	{
		int val = (int)times.size() - (int)inf_times;
		int diff = val - (int)action->action->GetTotal();
		if ( diff < 0 )
		{
			AddTimes(abs(diff),current_time);
		}
		else if ( diff > 0 )
		{
			DeleteTimes(diff);
		}
	}
}

//
void GDBase::UpdateWithExecution(double current_time) 
{
	if ( action->action->GetTotal() == 0 )
	{
		time = HUGE_VAL;
		times.clear();
	}
	else
	{
		int val = (int)times.size() - (int)inf_times;
		int diff = val - (int)action->action->GetTotal();
		if ( diff < 0 )
		{
			times.erase(time_iter);
			AddTimes(abs(diff)+1,current_time);
		}
		else if ( diff > 0 )
		{
			times.erase(time_iter);
			DeleteTimes(diff-1);
		}
	}
}

//
void GDBase::Init()
{
	if ( !times.empty() )
	{
		times.clear();
	}

	if ( action->action->GetTotal() == 0 )
	{
		time = HUGE_VAL;
	}
	else
	{
		int val = (int)action->action->GetTotal();
		AddTimes(val,0);
	}
}


///////////////////////////////////////////////////////////////////////////////////////////
// GDNormal Class
///////////////////////////////////////////////////////////////////////////////////////////

GDNormal::GDNormal(double p_mean, double p_sigma, Map_Elem *p_action, GDManager *p_myManager)
	:GDBase(p_action,NORMAL,p_myManager)
{
	mean = p_mean;
	sigma = p_sigma;
	// Generate the seed for the random number generator of the distribution
	double rn = this->myManager->GetEnv()->Random0Upto1();
	unsigned long seed = (unsigned long)(std::numeric_limits<unsigned long>::max() * rn);  
	rng = new boost::mt19937((uint32_t)seed);;   
	normal_distr = new boost::normal_distribution<>(mean,sigma);
	numb = new boost::variate_generator< boost::mt19937&,boost::normal_distribution<> >((*rng),(*normal_distr));
}

GDNormal::~GDNormal()
{
	delete rng;
	delete normal_distr;
	delete numb;
}

double GDNormal::GetNumb()
{
	return (*numb)();
}


///////////////////////////////////////////////////////////////////////////////////////////
// GDGamma Class
///////////////////////////////////////////////////////////////////////////////////////////

GDGamma::GDGamma(double p_scale, double p_shape, Map_Elem *p_action, GDManager *p_myManager)
	:GDBase(p_action,GAMMA,p_myManager)
{
	scale = p_scale;
	shape = p_shape;
	// Generate the seed for the random number generator of the distribution
	double rn = this->myManager->GetEnv()->Random0Upto1();
	unsigned long seed = (unsigned long)(std::numeric_limits<unsigned long>::max() * rn);  
	rng = new boost::mt19937((uint32_t)seed);   
	gamma_distr = new boost::gamma_distribution<>(shape);
	numb = new boost::variate_generator< boost::mt19937&,boost::gamma_distribution<> >((*rng),(*gamma_distr));
}

GDGamma::~GDGamma()
{
	delete rng;
	delete gamma_distr;
	delete numb;
}

double GDGamma::GetNumb()
{
	double val = (*numb)();
	return ( scale * val );
}


///////////////////////////////////////////////////////////////////////////////////////////
// GDGamma Class
///////////////////////////////////////////////////////////////////////////////////////////

GDHypExp::GDHypExp(vector< pair<double,double> >& p_parameters, Map_Elem *p_action, GDManager *p_myManager)
	:GDBase(p_action,HYPEXP,p_myManager)
{
	total_weight = 0;
	this->parameters = p_parameters;
	vector< pair<double,double> >::iterator i;
	for( i = parameters.begin() ; i != parameters.end() ; i++ )
	{
		total_weight += (*i).first;
	}
}

GDHypExp::~GDHypExp()
{
}

double GDHypExp::GetNumb()
{
	double selection = this->myManager->GetEnv()->Random0To1() * total_weight;
	vector< pair<double,double> >::iterator i;
	
	for( i = parameters.begin() ; i != parameters.end() ; i++ )
	{
			if ( (*i).first > selection )
			{
				break;
			}
			selection -= (*i).first;
	}

	double rn = this->myManager->GetEnv()->Random0To1();
	return ( 1.0 / (*i).second ) * log( 1.0 / rn ) ; 
}


///////////////////////////////////////////////////////////////////////////////////////////
// GDManager Class
///////////////////////////////////////////////////////////////////////////////////////////

GDManager::GDManager()
{
}

GDManager::~GDManager()
{
}

void GDManager::SetEnv(Environment *p_env) 
{ 
	env = p_env; 
}


Environment *GDManager::GetEnv() 
{ 
	return env; 
}

void GDManager::AddElement(GDBase *new_elem)
{
	std::vector<GDBase *>::iterator it;
	double min_time = new_elem->GetMinTime();
	for( it = actions.begin() ; it != actions.end() ; it++ )
	{
		if ( (*it)->GetMinTime() > min_time )
		{
			actions.insert(it,new_elem);				
			return;
		}
	}

	actions.push_back(new_elem);		
}


double GDManager::GetMinTime()
{
	if ( actions.empty() )
	{
		return HUGE_VAL;
	}
	return actions.front()->GetMinTime();
}

bool GDManager::GetMinAction(Action &action)
{
	if ( actions.empty() )
	{
		return false;
	}

	action.first = this->actions.front()->GetAction();
	action.second = this->actions.front()->GetMinTime();
	
	return true;
}

void GDManager::Sort()
{
	sort(actions.begin(),actions.end(),GDBase_Predicate);
}

void GDManager::Init()
{
	std::vector<GDBase *>::iterator iter;
	for( iter = actions.begin() ; iter != actions.end() ; iter++ )
	{
		(*iter)->Init();
	}
	sort(actions.begin(),actions.end(),GDBase_Predicate);
}
