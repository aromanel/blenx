#ifndef GDMANAGER_H_INCLUDED
#define GDMANAGER_H_INCLUDED

#include "Define.h"
#include "Map_Elem.h"

#include <cstdlib>
#include <cmath>
#include <algorithm>
#include <limits>
#include <queue>
#include <vector>
#include <vector>

#include <boost/random/variate_generator.hpp>
#include <boost/random/normal_distribution.hpp>
#include <boost/random/gamma_distribution.hpp>
#include <boost/random/mersenne_twister.hpp>


////////////////////////////////////////////////////////
// This classes define a data structure used to manage
// actions with stochastic behaviour described
// through general distributions
////////////////////////////////////////////////////////

class Environment;
class GDBase;
struct Map_Elem;

// Action structure
typedef std::pair<Element *,double> Action;

class GDManager {
private:
	Environment *env;
	std::vector<GDBase *> actions;
public:
	GDManager();
	~GDManager();
	Environment *GetEnv(); 
	void SetEnv(Environment *p_env); 
	void AddElement(GDBase *new_elem);
	double GetMinTime();
	bool GetMinAction(Action &action);
	void Sort();
	void Init();
};

//
enum DistrType 
{
	NORMAL,
	GAMMA,
	HYPEXP
};

//
class GDBase
{
protected:
	Map_Elem *action;
	std::vector<double> times; 
	GDManager *myManager;
	DistrType type;

	long unsigned inf_times;
	double time;
	std::vector<double>::iterator time_iter;
	
	void SetMinTime();
	void Resize();
public:
	GDBase(Map_Elem *p_action, DistrType p_type, GDManager *p_myManager);
	
	virtual ~GDBase();
	virtual void AddTimes(int number,double current_time);
	virtual bool DeleteTimes(int number);
	virtual double GetMinTime();
	virtual void Update(double current_time);
	virtual void UpdateWithExecution(double current_time);
	virtual void Init();
	virtual Element *GetAction();
	virtual double GetNumb() = 0;
};

// TODO: find how to give positive support to the distribution
class GDNormal : public GDBase {
private:
	double mean;
	double sigma;
	boost::mt19937 *rng;
	boost::normal_distribution<> *normal_distr;
	boost::variate_generator< boost::mt19937&,boost::normal_distribution<> > *numb;

private:
   // disable copy constructor and assignment
   GDNormal(const GDNormal& other);
   GDNormal& operator=(const GDNormal& other);

public:
	// Constructor with distribution and action parameters
	GDNormal(double p_mean, double p_sigma, Map_Elem *p_action, GDManager *p_myManager);
	// Destructor
	~GDNormal();
	double GetNumb();
};

class GDGamma : public GDBase {
private:
	double scale;
	double shape;
	boost::mt19937 *rng;
	boost::gamma_distribution<> *gamma_distr;
	boost::variate_generator< boost::mt19937&,boost::gamma_distribution<> > *numb;
public:
	// Constructor with distribution and action parameters
	GDGamma(double p_scale, double p_shape, Map_Elem *p_action, GDManager *p_myManager);
	// Destructor
	~GDGamma();
	double GetNumb();
};

class GDHypExp : public GDBase {
private:
	vector< pair<double,double> > parameters;
	double total_weight;
public:
	// Constructor with distribution and action parameters
	GDHypExp(vector< pair<double,double> >& p_parameters, Map_Elem *p_action, GDManager *p_myManager);
	// Destructor
	~GDHypExp();
	double GetNumb();
};


#endif
