#ifndef LOGGER_H_INCLUDED
#define LOGGER_H_INCLUDED

#include <cstdlib>
#include <string>
#include <iostream>
#include <fstream>
#include <ctime>
#include "Define.h"
#include "Singleton.h"

using namespace std;

//////////////////////////////////////////////////////
// LOGGER CLASS
//////////////////////////////////////////////////////

class Logger : public Singleton<Logger>
{
private:
	bool mode;
	ofstream f_log;
	time_t rawtime;
	struct tm * ti;

public:

   Logger()
   {
      f_log.open("./log.txt", ios::app);
   }
	
   void SetMode(bool mode)
   {
      if (f_log.is_open())
         f_log.close();
		if (mode)
			f_log.open("./log.html", ios::app);
		else
			f_log.open("./log.txt", ios::app);
		f_log << endl << endl << "----------------------" << endl;
	}

	~Logger() { f_log.close(); }

	void log(string message) {
	  time ( &rawtime );
	  ti = localtime ( &rawtime );
	  f_log << "[" << ti->tm_mday << "-" << ti->tm_mon+1 << "-" << ti->tm_year+1900 << " " ;
	  f_log << ti->tm_hour << ":" << ti->tm_min << ":" << ti->tm_sec << "] " << message << endl;
	}

	void logFree(string message) {
		f_log << message;
	}

	void logDouble(double val) {
		f_log << val;
	}
	
};

#endif //LOGGER_H_INCLUDED

