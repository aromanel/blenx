
#include "Define.h"

#include <iostream>
#include <cstdlib>
#include <list>
#include <map>
#include <string>
#include <algorithm>
#include <iterator>
#include <functional>
#include <cmath>
#include <ctime>
#include <limits>
#include <queue>
#include <vector>

#include <sbml/SBMLWriter.h>
#include <sbml/SBMLDocument.h>

#include <boost/random/variate_generator.hpp>
#include <boost/random/uniform_int.hpp>
#include <boost/random/lognormal_distribution.hpp>
#include <boost/random/uniform_01.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/gamma_distribution.hpp>

#include "ProgParser.h"
#include "TypeParser.h"
#include "FuncParser.h"
#include "Error_Manager.h"
#include "Logger.h"
#include "Environment.h"
#include "FunStd.h"
#include "Transition_System.h"
#include "ReactionGraph.h"
#include "ReactionGraphFunc.h"
#include "Matrix.h"
#include "MatrixFunc.h"
#include "GDManager.h"
#include "Expr.h"

//args
#include <tclap/CmdLine.h>

#ifdef _MSC_VER
#include <hash_map>
using namespace stdext;
#else
#include <ext/hash_map>
#endif

using namespace std;

Logger TheLogger;

#ifndef SIM_VERSION
#define SIM_VERSION "4.1(beta)"
#endif

#ifndef SIM_ARCH
#define SIM_ARCH "x64"
#endif

int main(int argc,char* argv[])
{
	RunningMode::RunningModeType flag_mode = RunningMode::GILLESPIE;
	bool flag_complex_structure = false;
	bool flag_spec2 = false;
	bool print_info = false;

	//Input
	TCLAP::UnlabeledValueArg<std::string> progArg("progfile", "Program file name", true, "", "string");
	TCLAP::UnlabeledValueArg<std::string> typeArg("typefile", "Type file name", true, "", "string");
	TCLAP::UnlabeledValueArg<std::string> funcArg("funcfile", "Functions and variables file name", false, NULL_STRING, "string");

	//Output options
	TCLAP::ValueArg<std::string> outBaseArg("o", "output", "Base output name", false, NULL_STRING, "string");
	TCLAP::SwitchArg spec2Arg("z", "spec_2", "Save spec2 file");
	TCLAP::SwitchArg noPrintInfoArg("i", "no_info", "Omit info (file saved and version)");
	TCLAP::SwitchArg fixedDeltaArg("f", "fixed_delta", "Save state at fixed time steps");
	//Duplicates/overriding of .prog file info
	TCLAP::ValueArg<int> runningStepsArg("p", "running_steps", "Number of steps to run", false, 0, "int");
	TCLAP::ValueArg<double> runningDeltaArg("d", "running_delta", "Delta between two recorded timestamps", false, 0, "double");
	TCLAP::ValueArg<double> runningTimeArg("t", "running_time", "Simulated time to reach", false, 0, "double");

	TCLAP::ValuesConstraint<std::string> allowedRunningModes( RunningMode::GetInstance().stringModes );
	TCLAP::ValueArg<std::string> runningModeArg("r", "running_mode",
		"Which analysis, simulation or transformation will be done on the provided program", false, "GILLESPIE", &allowedRunningModes);

	//CTMC options
	TCLAP::ValueArg<unsigned long> statesLimitCTMC("g", "globalStateLimit", "global states number limit", false, std::numeric_limits<unsigned long>::max(), "unsigned long");
	TCLAP::ValueArg<int> cardinalityLimitCTMC("c", "cardinalityLimit", "cardinality of species and complexes limit", false, std::numeric_limits<int>::max(), "int");
	TCLAP::ValueArg<int> complexStructureLimitCTMC("n", "complexLimit", "complex nodes number limit", false, std::numeric_limits<int>::max(), "int");

	//ReactionGraph options
	TCLAP::ValueArg<int> sbmlLevel("l", "SbmlLevel", "SBML level", false, 1, "int");
	TCLAP::ValueArg<int> sbmlVersion("v", "SbmlVersion", "SBML version", false, 1, "int");
	TCLAP::ValueArg<unsigned int> sbmlMolecules("m", "SbmlMolecules", "Maximum number of kinds of molecules to be discovered in SBML mode", false, std::numeric_limits<unsigned int>::max(), "unsigned int");

	TCLAP::ValueArg<unsigned long> seedArg("s", "seed",
		"The random seed to be used in the simulation (makes sense only for some running modes)", false, GetTime(), "unsigned long");


	TCLAP::SwitchArg complexStructArg("x", "complex_struct", "Complex structure mode");
	TCLAP::SwitchArg crmapArg("y", "cr_map", "Enable the complexes reactions map");
	TCLAP::ValueArg<int> complexDelArg("k", "complexes_delete", "Number of simulation steps at zero before deletion", false, -1, "int");
	TCLAP::SwitchArg complexSignArg("a", "complexes_signature", "Disable the use of signatures for complexes");
	//TCLAP::SwitchArg complexSingSelfAss("q", "single_self_assembly", "Optimized mode for the self assembly of single structures");

	//TCLAP::SwitchArg cmdparam("w", "parameters", "parameters");

	TCLAP::CmdLine cmdline("BlenX Simulator", '=', SIM_VERSION);


	cmdline.add(progArg);
	cmdline.add(typeArg);
	cmdline.add(funcArg);

	cmdline.add(outBaseArg);
	cmdline.add(spec2Arg);
	cmdline.add(noPrintInfoArg);
	cmdline.add(runningModeArg);

	cmdline.add(fixedDeltaArg);
	cmdline.add(runningStepsArg);
	cmdline.add(runningDeltaArg);
	cmdline.add(runningTimeArg);

	cmdline.add(statesLimitCTMC);
	cmdline.add(cardinalityLimitCTMC);
	cmdline.add(complexStructureLimitCTMC);
	cmdline.add(seedArg);
	cmdline.add(sbmlMolecules);
	cmdline.add(sbmlLevel);
	cmdline.add(sbmlVersion);
	cmdline.add(complexStructArg);
	cmdline.add(crmapArg);
	cmdline.add(complexDelArg);
	cmdline.add(complexSignArg);
	//cmdline.add(complexSingSelfAss);
	//cmdline.add(cmdparam);

	cmdline.parse(argc, argv);

	flag_complex_structure = complexStructArg.getValue();
	flag_spec2 = spec2Arg.getValue();
	print_info = !noPrintInfoArg.getValue();

	flag_mode = RunningMode::GetInstance().FromString(runningModeArg.getValue());
	unsigned long seedVal = seedArg.getValue();
	int sbml_level = sbmlLevel.getValue();
	int sbml_version = sbmlVersion.getValue();
	int max_molecules_number = sbmlMolecules.getValue();

	// Check the size of the log file
	FILE * stream = fopen( "./log.txt", "r" );
	if (stream != NULL) {
		fseek( stream, 0L, SEEK_END );
		long endPos = ftell( stream );
		fclose( stream );
		if (endPos > 10000) {
			// ottimizzare questa cosa
			stream = fopen( "./log.txt", "w" );
			fclose( stream );
		}
	}

	std::string& program_file = progArg.getValue();
	std::string& type_file = typeArg.getValue();
	std::string& function_file = funcArg.getValue();

	std::string& output_file = outBaseArg.getValue();
	if (output_file == NULL_STRING)
		output_file = program_file;

	string filename_out_e(output_file);
	string filename_out_c(output_file);
	string filename_out_v(output_file);
	string filename_species(output_file);
	string filename_graph(output_file);
	string filename_nodes(output_file);
	string filename_ctmc(output_file);
	string filename_complexes_tmp(output_file);

	int res;

	clock_t start = clock();

	try
	{
		// Build the environment (with all the maps, symbol table, etc...)
		Environment* env = new Environment(seedVal);

		// Imposto il tipo di computazione da eseguire
		// GILLESPIE = simulazione stocastica
		// TS = Transition System Generation
		env->RMode = flag_mode;
		env->FLAG_COMPLEX_STRUCTURE = flag_complex_structure;
		env->FLAG_COMPLEXES_REACTION = crmapArg.getValue();
		env->FLAG_COMPLEXES_SIGNATURE = complexSignArg.getValue();
		//env->FLAG_SINGLE_SELF_ASSEMBLY = complexSingSelfAss.getValue();
		if ( complexDelArg.getValue() < 0 )
			env->FLAG_COMPLEXES_ELIMINATION = false;
		else
		{
			env->FLAG_COMPLEXES_ELIMINATION = true;
			env->countdown = complexDelArg.getValue();
		}

		if (env->FLAG_SINGLE_SELF_ASSEMBLY)
		{
			env->FLAG_COMPLEXES_REACTION = false;
			env->FLAG_COMPLEXES_SIGNATURE = true;
			env->FLAG_COMPLEXES_ELIMINATION = true;
			env->countdown = 0;
		}

		Logger::GetInstance().SetMode(TXT);
		Logger::GetInstance().log("Random seed: " + Utility::i2s(seedVal));

		// Compilation
		FuncParser func_parser(env->GetST(), function_file);
		ProgParser prog_parser(env, program_file);
		TypeParser type_parser(*(env->GetTypeAffinity()), env->GetST(), type_file);

		if (function_file != NULL_STRING)
		{
			res = func_parser.Parse();
			if (res != 0)
			{
				cout << "BlenX Simulator - version " << SIM_VERSION << endl
					<< "Error parsing function file\n";
				exit(1);
			}
		}

		res = type_parser.Parse();
		if (res != 0)
		{
			cout << "BlenX Simulator - version " << SIM_VERSION << endl
				<< "Error parsing type file\n";
			exit(1);
		}
		res = prog_parser.Parse();
		if (res != 0)
		{
			cout << "BlenX Simulator - version " << SIM_VERSION << endl
				<< "Error parsing program file\n";
			exit(1);
		}

		// After parsing prog, recording mode and parameters (time, step, ect.) may be set-up or not.
		// ovverride them with the ones passed from the command line (if they are present)
		// if there is nothing, complain
		bool runningDefined = runningStepsArg.isSet() || runningTimeArg.isSet();
		if (!runningDefined && env->MODE == M_NONE)
		{
			cout << "Run parameters (time or steps) are not defined.\nPlease provide them in the .prog file or on the command-line" << endl;
			exit(1);
		}

		if (runningTimeArg.isSet())
		{
			env->MODE = M_TIME;
			env->STEP = 1;
			env->E_TIME = runningTimeArg.getValue();
		}
		else if (runningStepsArg.isSet())
		{
			if (runningDeltaArg.isSet())
			{
				env->MODE = M_STEP_DELTA;
				env->STEP = runningStepsArg.getValue();
				env->DELTA = runningDeltaArg.getValue();
			}
			else
			{
				env->MODE = M_STEP;
				env->STEP = runningStepsArg.getValue();
				env->DELTA = 0.0;
			}
		}
		env->FLAG_FIXED_DELTA = fixedDeltaArg.getValue();


		// Deletion of the AST and the Symbol Table
		// TODO: da sistemare i distruttori
		//delete(root);
		//delete(st);

		filename_species = filename_species + ".spec";
		ofstream *fstr_species = new ofstream(filename_species.c_str(), ios::out);
		string filename_species_2 = filename_species + "2";
		ofstream* fstr_species2 = NULL;
		if (flag_spec2)
			fstr_species2 = new ofstream(filename_species_2.c_str(), ios::out);
		filename_complexes_tmp = filename_complexes_tmp + ".tmp";
		FILE* file_tmp = NULL;
		if (env->FLAG_COMPLEXES_ELIMINATION)
			file_tmp = fopen(filename_complexes_tmp.c_str(), "w");

		//if (!cmdparam.getValue()){

		if ( env->RMode == RunningMode::GILLESPIE )
		{

			// Build output files
			filename_out_e = filename_out_e + ".E.out";
			filename_out_c = filename_out_c + ".C.out";
			filename_out_v = filename_out_v + ".V.out";

			FILE* file_out_e = fopen(filename_out_e.c_str(), "w");
			FILE* file_out_c = fopen(filename_out_c.c_str(), "w");
			FILE* file_out_v = fopen(filename_out_v.c_str(), "w");

			//string filename_out_aff = string(output_file) + ".aff.out";
			//env->GetTypeAffinity()->PrintFile(filename_out_aff.c_str());

			// Execution of the simulation
			if (print_info)
			{
				cout << "BlenX Simulator - version " << SIM_VERSION << endl
					<< "Simulation...";
				cout.flush();
			}

			env->Execute(file_out_e, fstr_species, fstr_species2, file_out_c, file_out_v, file_tmp);

			if (env->FLAG_COMPLEXES_ELIMINATION)
			{
				fclose(file_tmp);
				fstr_species->close();
				fstr_species = new ofstream(filename_species.c_str(),std::ios::binary|std::ios::app);
				ifstream fstr_tmp(filename_complexes_tmp.c_str(),std::ios::binary);
				*fstr_species << fstr_tmp.rdbuf();
				*fstr_species << "\n \n VARIABLES";
				fstr_tmp.close();
				remove(filename_complexes_tmp.c_str());
			}


			if (print_info)
			{
				cout << "finished" << endl << endl;
				cout << "Output files: " << endl
					<< " - " << filename_species.c_str() << endl

					<< " - " << filename_out_e.c_str() << endl
					<< " - " << filename_out_c.c_str() << endl
					<< " - " << filename_out_v.c_str() << endl;
			}

			Logger::GetInstance().log("SIMULATION: terminated.");


			// Deallocation of the Environment structures
			env->Destroy_Env();
			delete env;

			Logger::GetInstance().log("ENVIRONMENT: deallocated.");

			fclose(file_out_c);
			fclose(file_out_e);
		}
		else if ( env->RMode == RunningMode::TS )
		{
			cout << "BlenX Simulator - version " << SIM_VERSION << endl
				<< endl;
			cout.flush();

			filename_graph = filename_out_e + ".sts";
			filename_nodes = filename_out_c + ".nodes";
			filename_ctmc = filename_out_e + ".ctmc";

			FILE* file_graph = fopen(filename_graph.c_str(), "w");
			FILE* file_nodes = fopen(filename_nodes.c_str(), "w");
			FILE* file_ctmc = fopen(filename_ctmc.c_str(), "w");

			// Transition System generation
			cout << "STS generation:" << endl << " - STS creation...";

			Transition_System *ts = new Transition_System(env,statesLimitCTMC.getValue(),
				cardinalityLimitCTMC.getValue(),complexStructureLimitCTMC.getValue());

			if ( env->GenerateTS(ts,fstr_species) )
			{
				cout << "finished(N=" << ts->GetNodes() << ",E=" << ts->GetEdges() << ")" << endl << " - STS printing (DOT format)...";
				ts->Clean();
				ts->PrintGraphDOT(file_graph);
				cout << "finished" << endl << " - STS printing nodes...";
				ts->PrintNodes(file_nodes);
				cout << "finished" << endl;

				// Logger
				Logger::GetInstance().log("STS GENERATION: terminated.");

				// closing files of STS
				fclose(file_graph);
				fclose(file_nodes);

				// Continuous Time Markov Chain generation
				cout << endl << "CTMC generation..." << endl;
				ts->CleanMarks();
				cout << " - Immediate actions implosion...";
				bool ctmc_creation = false;
				ctmc_creation = ts->ImmediateActionsImplosion();

				if ( ctmc_creation )
				{
					cout << "finished(N=" << ts->GetNodes() << ",E=" << ts->GetEdges() << ")" << endl << " - CTMC printing (DOT format)...";
					ts->CleanMarks();
					ts->PrintGraphDOT(file_ctmc);
					cout << "finished" << endl;
				}
				else
				{
					cout << "failed (loops of infinite actions are present)" << endl;
				}

				cout << endl << "Output files: " << endl
					<< " - " << filename_species.c_str() << endl
					<< " - " << filename_graph.c_str() << endl
					<< " - " << filename_nodes.c_str() << endl;

				if ( ctmc_creation )
				{
					cout << " - " << filename_ctmc.c_str() << endl;
					// Logger
					Logger::GetInstance().log("CTMC GENERATION: terminated.");
				}

			}
			else
			{
				cout << "The Transition System cannot be generated (continuos variables are present in the model)!!!" << endl;
			}

			fclose(file_ctmc);
		}
		else if (env->RMode == RunningMode::SBML)
		{
			cout <<"Verifying if the model can be exported in SBML..." << endl;
			if (!env->IsSBMLCompliant()){
				return -1;
			}
			cout <<"Translating..."<< endl;
			hash_map<int, int> molecule_in_run;
			list<pair<int, int> >::iterator lit;
			string file_name1;
			string file_name2;
			string file_name3;
			MatrixFunc MatrixFunc(env, max_molecules_number);
			ReactionsGraphFunc *graph = new ReactionsGraphFunc(MatrixFunc.GetReactions(), MatrixFunc.GetMoleculeInRun(), MatrixFunc.GetNewDeleteEvents(), env);
			file_name1 = output_file + ".or.dot";
			graph->PrintDot(file_name1.c_str());
			env->PrintFile(fstr_species);
			cout << "Reducing..."<< endl;
			graph->Reduce();
			file_name2 = output_file + ".re.dot";
			graph->PrintDot(file_name2.c_str());
			SBMLDocument_t * doc = graph->BuildSBML(sbml_level, sbml_version);
			file_name3 = output_file + ".xml";
			writeSBML(doc, file_name3.c_str());

			cout << endl << "Output files" << endl;

			cout << "\t" << file_name1 << endl << "\t" << file_name2 << endl << "\t" << file_name3 << endl << "\t" << filename_species << endl;


			/*Matrix Matrix(env);
			//hash_map<reaction_key, list<double>*, reaction_hash_compare> *matrix = env->GenerateMatrix2(molecule_in_run);
			ReactionsGraph	*graph = new ReactionsGraph(Matrix.GetReactions(), Matrix.GetMoleculeInRun(), env);
			ofstream *fstr_species = new ofstream(filename_species.c_str(),ios::out);
			env->PrintFile(fstr_species);
			file_name = output_file + ".or.dot";
			graph->PrintDot(file_name.c_str());
			graph->Reduce();
			file_name = output_file + ".re.dot";
			graph->PrintDot(file_name.c_str());
			SBMLDocument_t * doc = graph->BuildSBML();
			file_name = output_file + ".xml";
			writeSBML(doc, file_name.c_str());*/
		}

		//}
		//else
		//{

		/*int CSIZE = 6;
		int step_size = 1000;
		//string constants[21] = {"kcs","kms","kmd","kca","kci","kmd1","kws","kwa","kwi","kcd","kcd1","kma",
		//	  "kmi","kwd","kwd1","Jca","Jci","Jwa","Jwi","Jma","Jmi"};

		string constants[6] = {"kcs","kcd","kcd1","kws","kwd","kwd1"};

		// Creo le mappe
		hash_map<double,int> const_map;
		hash_map<string,paramValues> const_values;
		paramValues current_values;

		// Genero la mappa delle costanti ( DA COMMENTARE)
		//Constants_Iterator *const_iter = env->GetST()->GetConstantsIterator();
		//ConstantMap::iterator current;
		//const_iter->IteratorReset();
		//while ( !const_iter->IteratorIsEnd() )
		//{
		//	  current = const_iter->GetCurrent();
		//	  current_values.init_value = (*current).second;
		//	  const_values[(*current).first] = current_values;
		//	  const_iter->IteratorNext();
		//}

		// Inizializzo la mappa delle costanti con i valori iniziali
		for( int index = 0 ; index < CSIZE ; index++ )
		{
		if ( env->GetST()->FindConstant(constants[index]))
		{
		current_values.init_value = env->GetST()->GetConstant(constants[index]);
		const_values[constants[index]] = current_values;
		}
		}

		// Inizializzo il generatore di numeri casuali con una lognormale LOCATION 1 e SCALA 0.5
		ofstream *file_parameters_variationM1 = new ofstream("parameters_variationM1.txt",ios::out);
		ofstream *file_parameters_variationM2 = new ofstream("parameters_variationM2.txt",ios::out);
		ofstream *file_distribution = new ofstream("distribution.txt",ios::out);
		ofstream *file_param;
		boost::mt19937 rng;
		//boost::lognormal_distribution<> const_distr(1,0.5);
		//boost::normal_distribution<> normal_distr(-1,1);
		boost::uniform_real<> unif_distr(-1,1);
		boost::variate_generator<boost::mt19937&, boost::uniform_real<> > numb(rng, unif_distr);

		// Genero i 1000 files modificando le costanti usando il generatore
		double x;
		double parameters_variationM1;
		double parameters_variationM2;
		hash_map<string,paramValues>::iterator cons_iter;
		string file_cons_name;

		int first = 1;
		int second = 1;
		int module;

		string actual_const;
		double actual_val;

		//std::vector< pair<double,double> > vect;
		//std::pair<double,double> pairv;
		//pairv.first = 0.3;
		//pairv.second = 0.0025;
		//vect.push_back(pairv);
		//pairv.first = 0.7;
		//pairv.second = 0.085312793;
		//vect.push_back(pairv);
		//GDManager *manag = new GDManager();
		//manag->SetEnv(env);
		//GDHypExp *hedist = new GDHypExp(vect,NULL,manag);

		//for (int i = 1; i <= 10000; i++ )
		//{
		//	  cout << hedist->GetNumb() << endl;
		//}

		for (int i = 1; i <= 1000; i++)
		{
		parameters_variationM1 = 0;
		parameters_variationM2 = 0;
		// Imposto tutte le costanti con nuovi valori normalizzati col numero generato
		// dal generato lognormale
		for ( cons_iter = const_values.begin() ; cons_iter != const_values.end() ; cons_iter ++)
		{
		// genero il numero causuale
		// x = 0.93 * pow(1.150538,numb());

		x = pow(10,numb());
		cout<<x<<endl;
		(*cons_iter).second.current_value = (*cons_iter).second.init_value * x;
		env->GetST()->SetConstant((*cons_iter).first,(*cons_iter).second.current_value);

		actual_const = (*cons_iter).first;
		actual_val = (*cons_iter).second.current_value;


		if ( (*cons_iter).first == "kcs" || (*cons_iter).first == "kcd" || (*cons_iter).first == "kcd1")
		{
		parameters_variationM1 += abs(log10(x));
		}
		else if ( (*cons_iter).first == "kws" || (*cons_iter).first == "kwd" || (*cons_iter).first == "kwd1")
		{
		parameters_variationM2 += abs(log10(x));
		}
		else
		{
		parameters_variationM1 += abs(log10(x));
		parameters_variationM2 += abs(log10(x));
		}

		(*file_distribution) << x << endl;

		}

		// Creo il file func con le costanti modificate
		file_cons_name = output_file + Utility::i2s(first) + "_" + Utility::i2s(second) + ".func";
		file_param = new ofstream(file_cons_name.c_str(),ios::out);
		// Aggiungo commento in testa al file func
		(*file_param) << "// Parameters variation M1 = " << parameters_variationM1 << endl;
		(*file_param) << "// Parameters variation M2 = " << parameters_variationM2 << endl << endl;
		// Stampo il file func
		env->GetST()->Print(file_param);
		file_param->close();

		(*file_parameters_variationM1) << parameters_variationM1 << endl;
		(*file_parameters_variationM2) << parameters_variationM2 << endl;

		module = i % step_size;

		if ( module == 0 )
		{
		first += 1;
		second = 1;
		}
		else
		{
		second++;
		}

		}

		file_parameters_variationM1->close();
		file_parameters_variationM2->close();
		file_distribution->close();

		}

		fstr_species->close();*/
	}
	catch (...)
	{
		cout << "Run Time Error." << endl
			<< "In order to detect and solve this problem\nwe kindly ask you to send this model to:" << endl
			<< "a.romanel@gmail.com" << endl
			<< "Thank you." << endl;
	}

	cout << endl << "Execution time: " << ( (double)clock() - start ) / CLOCKS_PER_SEC << "s" << endl;

	return(0);
}

