all: prog

LIBRARIES = -lsbml
LIBRARIESSTATIC = -lsbml -lbz2 -lz -lxml2 -lxerces-c -lpthread -static
APPNAME64 = sim
CC = g++
CFLAGS = -O3 -Wall

COMPILE = $(CC) $(CFLAGS) -I./include -c
OBJFILES := $(patsubst %.cpp,%.o,$(wildcard *.cpp))

prog: $(OBJFILES)
	$(CC) $(CFLAGS) -o $(APPNAME64) $(OBJFILES) $(LIBRARIES)

static: $(OBJFILES)
	$(CC) $(CFLAGS) -o $(APPNAME64) $(OBJFILES) $(LIBRARIESSTATIC)

%.o: %.cpp %.h Define.h
	$(COMPILE) -o $@ $<

clean: 
	rm -f *.o 
