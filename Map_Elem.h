#ifndef MAP_ELEM_H_INCLUDED
#define MAP_ELEM_H_INCLUDED

#include <boost/random/variate_generator.hpp>
#include <boost/random/normal_distribution.hpp>
#include <boost/random/gamma_distribution.hpp>
#include <boost/random/mersenne_twister.hpp>

#include <cstdlib>
#include <deque>
#include <cmath>
#include <algorithm>
#include "Define.h"
#include "StateVar.h"
#include "GDManager.h"

class GDBase;

enum StochLawType
{
	EXP,
	GENERAL,
};

struct Map_Elem
{
	Element* action;
	ElementType type;
	StochLawType s_type;
	unsigned long simulation_step;
	size_t pq_index;
	GDBase *gd_index;

	Map_Elem(Element *p_action) 
	{
		action = p_action;
		pq_index = 0;
		gd_index = NULL;
		s_type = EXP;
		type = action->GetCategory();
		simulation_step = 0;
	}

	void SetGDElem(GDBase *p_gd_index)
	{
		s_type = GENERAL;
		pq_index = 0;
		gd_index = p_gd_index;
	}

	~Map_Elem() { /*delete action;*/ }
};

//
//
struct UIBind_elem 
{
	Map_Elem *unbind;
	Map_Elem *inter1;
	Map_Elem *inter2;
	int counter;

	/*UIBind_elem() {
	unbind = inter1 = inter2 = NULL;
	counter = 0;
	}*/
	UIBind_elem(Map_Elem *p_unbind, Map_Elem *p_inter1, Map_Elem *p_inter2) 
	{
		unbind = p_unbind;
		inter1 = p_inter1;
		inter2 = p_inter2;
		counter = 1;
	}
	void Inc() { 
		counter++; 
	}
	void Dec() { 
		//assert(counter>0); 
		counter--; 
	}
};



#endif //MAP_ELEM_H_INCLUDED

