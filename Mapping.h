
#ifndef MAPPING_H_INCLUDED
#define MAPPING_H_INCLUDED

#include "hash.h"

#include <string>

using std::string; 

class Mapping {
private:
	hash_map<string,string> ch_mapping;
	bool identity;
public:
	Mapping() {}
	~Mapping() {}
	void InsertMapping(string a,string b) {
		ch_mapping[a] = b;
	}
	bool IsIdentity() { return identity; }
	void SetIdentityT() { identity = true; }
	void SetIdentityF() { identity = false; }
	string GetMapping(string a) {
		hash_map<string,string>::iterator i;
		if ( (i = ( ch_mapping.find(a) )) != ch_mapping.end())
			return (*i).second;
		return "";
	}
};


#endif //MAPPING_H_INCLUDED

