#include"Matrix.h"

Matrix::Matrix(Environment *p_env){
	env = p_env;
	complex_list = env->GetComplexList();
	entity_list = env->GetEntityList();
	reactions = new hash_map<reaction_key, list<double>*, reaction_hash_compare>;
	first_loop = true;

	//support variables
	list<int>::iterator it_mol_list;
	list<int>::iterator tmp_it_mol_list;
	list<int>::iterator prev_it_mol_list;
	list<Map_Elem *>::iterator act_map_it;
	list<Element *>::iterator inf_act_map_it;

	pair<int, int> p;
	unsigned int i;
	//int reactants[2];
	//int elements_list_number;
	//int entity_id;
	bool infinite_actions;
	Entity *entity;
	Iterator_Interface *iit;
	Action action;
	vector<Map_Elem *> elem_list;
	//list<pair<bool, Element *> > bimolecular_reactions;
	//list<pair<bool, Element *> > inf_reactions;
	list<pair<bool, Element *> > bim_reactions;
	list<Element *>::iterator elem_it;
	list<pair<bool, Element*> >::iterator pair_it;
	list<Element *> *entity_reactions = NULL;
	list<Complex *> comp[2];
	Complex_Graph *comp_graph = NULL;


	//initialize actions lists
	act_map_list = env->GetActMap()->GetActList();
	last_act_map = act_map_list->begin();
	inf_act_map_list = env->GetInfActMap()->GetActList();
	last_inf_act_map = inf_act_map_list->begin();

	//To fire all the possible reactions we have to ensure that all the entities
	//have their counter set at least to two. In this cycle we save the stand
	//alone existing entities too.
	for (i = 0; i < entity_list->size(); i++){
		if((*entity_list)[i]->GetCounter() > 0){
			p.first = (*entity_list)[i]->GetId();
			p.second = (*entity_list)[i]->GetCounter();
			if (!(*entity_list)[i]->IsBound()){
				molecule_in_run.push_back(p);
				molecules_list.push_back(p.first);
				molecules_map[p.first]=true;
			}
			(*entity_list)[i]->SetCounter(numeric_limits<int>::max());
			instantiated_entities[(*entity_list)[i]]= new list<Element*>;
		}
	}

	//We have also to be sure that all the complexes have their counter set at
	//least to two. In this cycle we save the existing complexes too.
	for (i = 0; i < (*complex_list).size(); i++){
		if ((*complex_list)[i]->GetNumber() > 0){
			p.first = -(*complex_list)[i]->GetId();
			p.second = (*complex_list)[i]->GetNumber();
			molecule_in_run.push_back(p);
			molecules_list.push_back(p.first);
			molecules_map[p.first]=true;
			(*complex_list)[i]->AddCounter(numeric_limits<int>::max());
		}
	}

	//main loop
	it_mol_list = molecules_list.begin();
	while (it_mol_list != molecules_list.end()){

		AssociateNewActions();
		AssociateNewInfActions();
	
		//This variable is set to true if the current molecule can perform
		//monomolecular actions with infinite rate
		infinite_actions = false;

		//Check if the molecule is an unbounded entity or a molecule
		if (*it_mol_list > 0){
			entity = (*entity_list)[*it_mol_list-1];
			
			//Check if the entity can perform actions with infinite rate
			if (!entity->InfListEmpty()){
				infinite_actions = true;
				transient_molecules[*it_mol_list] = true;
				iit = env->GetMonoInfActIterator(entity);
				ExecuteMonoActions(entity, iit);
				//ExecuteInfEntityActions(entity, infinite_actions, it_mol_list);
			}

			//Check if there are monomolecular events with infinite rate (it is a
			//monomolecular reactions). If there are, they are executed
			if (actions_entities.find(entity->GetId()) != actions_entities.end()){
				entity_reactions = actions_entities[entity->GetId()];
				if (ExecuteEvents(&entity_reactions[1], it_mol_list, true)){
					infinite_actions = true;
					transient_molecules[*it_mol_list] = true;
				}
			}
			
			//If the entity cannot perform actions with infinite rate, actions
			//with finite rate are executed
			if (infinite_actions == false){
				iit = env->GetMonoActIterator(entity);
				ExecuteMonoActions(entity, iit);

				if (entity_reactions == NULL && actions_entities.find(entity->GetId()) != actions_entities.end()){
					entity_reactions = actions_entities[entity->GetId()];
				}

				//Check if there are monomolecular events with finite rate (it is a
				//monomolecular reactions). If there are, they are executed
				if (actions_entities.find(entity->GetId()) != actions_entities.end()){
					ExecuteEvents(&entity_reactions[0], it_mol_list, false);
				}

			}	

			entity_reactions = NULL;
		} else {
			//We are analysing a complex
				
			//Check if the complex can perform monomolecular actions with infinite
			//rate. To do that we check if each molecule it is made of can do any
			//action.
			comp_graph = (*complex_list)[abs(*it_mol_list)-1]->GetComplex();
			comp_graph->InitIT();
			comp[0].push_back((*complex_list)[abs(*it_mol_list)-1]);
			for (entity = comp_graph->GetIT(); entity != NULL; entity = comp_graph->GetIT()){
				if (!entity->InfListEmpty()) {
					infinite_actions = true;
					transient_molecules[*it_mol_list] = true;
					iit = entity->GetInfIterator(entity, NULL, &comp[0], NULL, ONLY_MONO);
					ExecuteMonoActions(entity, iit);
				}
			}

			//Find out if thes complex can perform monomolecular actions with
			//infinite rate that involves two entities it is made of
			this->FindPossibleTwoEntitiesElem(*it_mol_list, *it_mol_list, bim_reactions, true, false);
			this->FindPossibleTwoEntitiesElem(*it_mol_list, *it_mol_list, bim_reactions, true, true);

			//Check if this elem can fire a monomolecular reaction with the complex in analysis
			if(ExecuteElementWithTwoEntities(*it_mol_list, *it_mol_list, ONLY_MONO, bim_reactions)){
				infinite_actions = true;
				transient_molecules[*it_mol_list] = true;
			}

			bim_reactions.clear();
			
			//If the complex cannot perform monomolecular reactions with infinite
			//rate, we perform the monomolecular reaction with finite rate
			if (infinite_actions == false){
				comp_graph->InitIT();
				for (entity = comp_graph->GetIT(); entity != NULL; entity = comp_graph->GetIT()){
					iit = entity->GetIterator(entity, NULL, &comp[0], NULL, ONLY_MONO);
					ExecuteMonoActions(entity, iit);
				}

				//Now it is necessary to find out if this complex can perform
				//monomolecular actions with finite rate that involves two entities
				//it is made of
				this->FindPossibleTwoEntitiesElem(*it_mol_list, *it_mol_list, bim_reactions, false, false);
				this->FindPossibleTwoEntitiesElem(*it_mol_list, *it_mol_list, bim_reactions, false, true);
				
				//Check exclusively the reaction with finite rate
				ExecuteElementWithTwoEntities(*it_mol_list, *it_mol_list, ONLY_MONO, bim_reactions);
				bim_reactions.clear();
			}

			comp[0].clear();
			comp[1].clear();
		}

		//Check if current molecule can perform bimolecular action with
		//previously analysed molecules
		tmp_it_mol_list = molecules_list.begin();
		do {
			//Store in inf_reactions the bimolecular recations with infinite rate that
			//*it_mol_list and *tmp_mol_list can perform
			this->FindPossibleTwoEntitiesElem(*it_mol_list, *tmp_it_mol_list, bim_reactions, true, false);

			//Given that we are considering unbounded entities the
			//bimolecular reactions are for sure only bimolecular.  It is
			//not the same for complexes where a bimolecular reaction can
			//lead to a monomolecular reaction
			
			//if there are monomolecular reaction with infinite rate that
			//involve one of the two considered molecules we consider only
			//bimolecular reaction with infinite rate
			if (!infinite_actions && (transient_molecules.find(*tmp_it_mol_list) == transient_molecules.end())){
				this->FindPossibleTwoEntitiesElem(*it_mol_list, *tmp_it_mol_list, bim_reactions, false, false);
			}

			ExecuteElementWithTwoEntities(*it_mol_list, *tmp_it_mol_list, ONLY_BIM, bim_reactions);
			bim_reactions.clear();

			prev_it_mol_list = tmp_it_mol_list;
			tmp_it_mol_list++;
		}while (prev_it_mol_list != it_mol_list);	

		
		it_mol_list++;
	}	

	//ricordati che la unbind è una mono!!!!!!!! (pensa anche a quali sono mono di tutte le altre)
}

//This function makes the following three operations:
//1) updates instances number of the molecules involved in the reaction
//2) if reactions has generated new molecules the function add them to molecules_list and molecules_map
//3) stores the reaction in the reactions structure
void Matrix::UpdateStructures(reaction_key *reaction, double rate){
	int i;
	int id;
	Complex *comp;
	Entity *entity;

	//Update instances number of the molecule involved in the reaction
	for (i = 0; i < 4; i++){
		id = reaction->data[i];
		if (id > 0){
			//The involved molecule is an entity
			(*entity_list)[id-1]->SetCounter(numeric_limits<int>::max());
		} else if (id < 0){
			//The involved molecule is a complex
			comp = (*complex_list)[abs(id)-1];
			comp->SetNumber(numeric_limits<int>::max());

			//Update the counter of all the entities belong to the complex
			comp->GetComplex()->InitIT();
			for (entity = comp->GetComplex()->GetIT(); entity != NULL; entity = comp->GetComplex()->GetIT()){
				entity->SetCounter(numeric_limits<int>::max());
			}
		}
	}

	//Add new molecules to molecules_list and molecules_map
	for (i = 2; i < 4; i++){
		id = reaction->data[i];
		if (id != 0 && molecules_map.find(id) == molecules_map.end()){
			molecules_map[id] = true;
			molecules_list.push_back(id);
		}
	}

	//Store the new reaction in the reactions structure
	//Add the executed reaction to the reactions map
	if (reactions->find(*reaction) == reactions->end()){
		(*reactions)[*reaction] = new list<double>;
	}
	//This let us store infinite reactions only once
	if ((rate == numeric_limits<double>::infinity() && (*reactions)[*reaction]->size() == 0)||
		 (rate != numeric_limits<double>::infinity())){
		(*reactions)[*reaction]->push_back(rate);
	}	
}

void Matrix::FindPossibleTwoEntitiesElem(int molecule_id1, int molecule_id2,  list<pair<bool,Element *> > &result, bool inf, bool mono){
	int finite_elem_kind;
	int inf_elem_kind;
	Complex_Graph *comp = NULL;
	Entity *entity = NULL;
	list<Element *>::iterator el_it;

	//we store finite bimolecular at position 0, and infinite ones at position 1
	list<Element *> elements;

	hash_map<int, bool> entities_molecule1;
	hash_map<int, bool> entities_molecule2;

	hash_map<int, bool>::iterator en_it1;
	hash_map<int, bool>::iterator en_it2;
	int reactants[2];
	pair<bool, Element*> p;
	bool elem_added;

	hash_map<Element*, bool> added_elements;

	if (mono){
		finite_elem_kind = 0;
		inf_elem_kind = 1;
	} else {
		finite_elem_kind = 2;
		inf_elem_kind = 3;
	}

	//Collects entities that belong to molecule_id1, at the same time collects
	//all the bimolecular elements that involve the molecule_id1
	if (molecule_id1 > 0){
		entities_molecule1[molecule_id1] = true;
		if (actions_entities.find(molecule_id1) != actions_entities.end()){
			if (inf) {
				elements.insert(elements.end(), actions_entities[molecule_id1][inf_elem_kind].begin(),  actions_entities[molecule_id1][inf_elem_kind].end());
			} else {
				elements.insert(elements.end(), actions_entities[molecule_id1][finite_elem_kind].begin(),  actions_entities[molecule_id1][finite_elem_kind].end());
			}
		}
	} else {
		comp = (*complex_list)[abs(molecule_id1) - 1]->GetComplex();
		comp->InitIT();
		for (entity = comp->GetIT(); entity != NULL; entity = comp->GetIT()){
			entities_molecule1[entity->GetId()] = true;	
			if (actions_entities.find(entity->GetId()) != actions_entities.end()){
				if (inf){
					elements.insert(elements.end(), actions_entities[entity->GetId()][inf_elem_kind].begin(),  actions_entities[entity->GetId()][inf_elem_kind].end());
				} else {

					elements.insert(elements.end(), actions_entities[entity->GetId()][finite_elem_kind].begin(),  actions_entities[entity->GetId()][finite_elem_kind].end());
				}
			}
		}
	}

	//Collects entities that belong to molecule_id2
	if (molecule_id2 > 0){
		entities_molecule2[molecule_id2] = true;
	} else {
		comp = (*complex_list)[abs(molecule_id2) - 1]->GetComplex();
		comp->InitIT();
		for (entity = comp->GetIT(); entity != NULL; entity = comp->GetIT()){
			entities_molecule2[entity->GetId()] = true;
		}
	}


	//Find out which are the elements that involves both molecule_id1, and
	//molecule_id2. When an element is found we store it in the result list. 
	for (el_it = elements.begin(); el_it != elements.end(); el_it++){
		if (added_elements.find(*el_it) == added_elements.end()){
			//This variable avoid to add the same elem twice
			elem_added = false;
			for (en_it1 = entities_molecule1.begin(); !elem_added && en_it1 != entities_molecule1.end(); en_it1++) {
				for (en_it2 = entities_molecule2.begin(); !elem_added && en_it2 != entities_molecule2.end(); en_it2++){
					(*el_it)->IsOnlyMono(reactants);
					if (reactants[0] == (*en_it1).first && reactants[1] == (*en_it2).first){
						//True means that molecule_id1 is the first reactant and
						//molecule_id2 is the second one
						p.first = true;
						p.second = *el_it;
						added_elements[p.second] = true;
						result.push_back(p);	
						elem_added = true;
					} else if (reactants[1] == (*en_it1).first && reactants[0] == (*en_it2).first){
						//True means that molecule_id2 is the first reactant and
						//molecule_id1 is the second one
						p.first = false;
						p.second = *el_it;
						added_elements[p.second] = true;
						result.push_back(p);	
						elem_added = true;
					}
				}
			}
		}
	}
}
	

void Matrix::AssociateNewActions(){
		Map_Elem *map_elem;
		int reactants[2];
		int elements_list_number;
		int i;
		int entity_id;
		//Update last_act_map in order to point to the first element of the
		//act_map that has to be yet analysed
		if (!first_loop){
			last_act_map++;
		}
		
		//associate actions with finite rate with entities that can perform them
		while(last_act_map != act_map_list->end()){
			map_elem = *last_act_map;

			//Consider only act#ions that involve more then one entity
			if (map_elem->action->GetCategory() != MONO){
				//check if actions can be only monomolecular or it they can be also
				//bimolecular (In this context an action is bimolecular if it
				//involve more than one molecule (not only more than one entity)
				if (map_elem->action->IsOnlyMono(reactants)){
					elements_list_number = 0;
				} else {
					elements_list_number = 2;
				}
				
				//Associate the element to the entities that are its reactants
				for (i = 0; i < 2; i++){
					if (reactants[i] != -1){
						entity_id = reactants[i];

						if (actions_entities.find(entity_id) == actions_entities.end()){
							actions_entities[entity_id] = new list<Element *>[4];
						}
						actions_entities[entity_id][elements_list_number].push_back(map_elem->action);
					}
				}
			}

			last_act_map++;
		}

		if (act_map_list->size() > 0){
			last_act_map = --(act_map_list->end());
		} else {
			last_act_map = act_map_list->end();
		} 
}


void Matrix::AssociateNewInfActions(){
		Element *elem;
		int reactants[2];
		int elements_list_number;
		int i;
		int entity_id;
		//Update the last_inf_act_map in order tu point to the first element of
		//the inf_act_map that has to be analysed
		if (!first_loop){
			last_inf_act_map++;
		} else{
			first_loop = false;
		}		

		//associate actions with finite rate with entities that can perform them
		while(last_inf_act_map != inf_act_map_list->end()){
			elem = *last_inf_act_map;

			//Consider only actions that involve more then one entity
			if (elem->GetCategory() != MONO){
				//check if the actions is monomolecular or bimolecular (In this
				//context an action is bimolecular if it involve more than one
				//molecule (not only more than one entity)
				if (elem->IsOnlyMono(reactants)){
					elements_list_number = 1;
				} else {
					elements_list_number = 3;
				}
				
				//Associate the element to the entities that are its reactants
				for (i = 0; i < 2; i++){
					if (reactants[i] != -1){
						entity_id = reactants[i];


						if (actions_entities.find(entity_id) == actions_entities.end()){
							actions_entities[entity_id] = new list<Element *>[4];
						}
						actions_entities[entity_id][elements_list_number].push_back(elem);
					}
				}
			}

			last_inf_act_map++;
		}


      if (inf_act_map_list->size() > 0){
		   last_inf_act_map = --(inf_act_map_list->end());
		} else {
		   last_inf_act_map = inf_act_map_list->end();
		}
}

//Execute the MONO actions of entity that have infinite rate, infinite_actions is set to true if at least one action is executed
/*void Matrix::ExecuteInfEntityActions(Entity *entity, bool &infinite_actions, list<int>::iterator &current_mol){
	if (!entity->InfListEmpty()) {
		vector<Map_Elem *> elem_list;
		Action action;
		Iterator_Interface *iit;
		reaction_key reaction;

		infinite_actions = true;
		transient_molecules[*current_mol] = true;
		iit = env->GetMonoInfActIterator(entity);
		iit->IteratorReset();
		while (!iit->IteratorIsEnd()){
			action.first = entity;
			env->ExecuteStep(elem_list, action, iit, &reaction);
			elem_list.clear();
			this->UpdateStructures(&reaction, action.first->GetBasalRate(iit));
			iit->IteratorNext();
		}
	}
}*/


//Execute the MONO actions that are iterated through iit
void Matrix::ExecuteMonoActions(Entity *entity, Iterator_Interface *iit){
	//if (!entity->InfListEmpty()) {
	vector<Map_Elem *> elem_list;
	Action action;
	reaction_key reaction;

	//infinite_actions = true;
	//transient_molecules[*current_mol] = true;
	//iit = env->GetMonoInfActIterator(entity);
	iit->IteratorReset();
	while (!iit->IteratorIsEnd()){
		action.first = entity;
		env->ExecuteStep(elem_list, action, iit, &reaction);
		elem_list.clear();
		this->UpdateStructures(&reaction, action.first->GetBasalRate(iit));
		iit->IteratorNext();
	}
}

//Execute the MONO actions of entity
/*void Matrix::ExecuteEntityActions(Entity *entity){
	reaction_key reaction;
	vector<Map_Elem *> elem_list;
	Action action;
	Iterator_Interface *iit;

	iit = env->GetMonoActIterator(entity);
	iit->IteratorReset();
	while (!iit->IteratorIsEnd()){
		action.first = entity;
		env->ExecuteStep(elem_list, action, iit, &reaction);
		elem_list.clear();
		this->UpdateStructures(&reaction, action.first->GetBasalRate(iit));
		iit->IteratorNext();
	}
}*/

//Execute Events present in the element list. It returns True if at least one
//element is executed
bool Matrix::ExecuteEvents(list<Element *> *elements, list<int>::iterator &current_mol, bool inf){
	bool events_executed = false;
	vector<Map_Elem *> elem_list;
	Action action;
	list<Element *>::iterator elem_it;
	reaction_key reaction;

	for (elem_it = elements->begin();elem_it != elements->end(); elem_it++){
		if ((*elem_it)->GetCategory() == EVENT){
			if (inf) {
				events_executed = true;
				transient_molecules[*current_mol] = true;
			}
			//If we are looking to Events to be executed we are sure that the
			//molecule we are analysing is an entity
			action.first = *elem_it;
			env->ExecuteStep (elem_list, action, NULL, &reaction);
			elem_list.clear();
			this->UpdateStructures(&reaction, action.first->GetBasalRate(NULL));
		}
	}
	return events_executed;
}

/*bool Matrix::ExecuteElementWithTwoEntities(list<int>::iterator &curr_mol1, list<int>::iterator &curr_mol2, TwoIteratorKind kind, list<pair<bool, Element *> > reactions[2]){
	list<Complex *> comp[2];
	list<pair<bool, Element*> >::iterator pair_it;
	int reactants[2];
	Iterator_Interface *iit;
	Action action;
	reaction_key reaction;
	vector<Map_Elem *> elem_list;
	bool action_executed = false;

	if (*curr_mol1 < 0){
		comp[0].push_back((*complex_list)[abs(*curr_mol1)-1]);
	}
	if (*curr_mol2 < 0){
		comp[1].push_back((*complex_list)[abs(*curr_mol2)-1]);
	}

	for (int i=0; i < 2; i++){
		for (pair_it = reactions[i].begin(); pair_it != reactions[i].end(); pair_it++){
			//If the second molecule we are analysing is a complex, we
			//store this complex in a list

			//Get the two entities that are reactants of the reactions we
			//are analysing 
			(*pair_it).second->IsOnlyMono(reactants);

			//Check how the entities are stored in the Elem we are
			//considering
			if ((*pair_it).first){
				iit = (*pair_it).second->GetIterator((*entity_list)[reactants[0] - 1], (*entity_list)[reactants[1] - 1], &comp[0], &comp[1], kind);
			} else {
				iit = (*pair_it).second->GetIterator((*entity_list)[reactants[0] - 1], (*entity_list)[reactants[1] - 1], &comp[1], &comp[0], kind);
			}


			iit->IteratorReset();


			action.first = (*pair_it).second;
			//If we have two unbound entities we have to execute an
			//action also if the iit->IteratorIsEnd==true, it is to
			//handle the bimolecular reaction between the two entities 
			if (comp[0].empty() && comp[1].empty()){
				env->ExecuteStep (elem_list, action, iit, &reaction);
				elem_list.clear();
				this->UpdateStructures(&reaction, action.first->GetBasalRate(iit));	
				action_executed = true;
			}

			while (!iit->IteratorIsEnd()){
				env->ExecuteStep (elem_list, action, iit, &reaction);
				elem_list.clear();
				this->UpdateStructures(&reaction, action.first->GetBasalRate(iit));
				iit->IteratorNext();
				action_executed = true;
			}
		}
	}
	return action_executed;
}*/


bool Matrix::ExecuteElementWithTwoEntities(int curr_mol1, int curr_mol2, TwoIteratorKind kind, list<pair<bool, Element *> >reactions){
	list<Complex *> comp[2];
	list<pair<bool, Element*> >::iterator pair_it;
	int reactants[2];
	Iterator_Interface *iit;
	Action action;
	reaction_key reaction;
	vector<Map_Elem *> elem_list;
	bool action_executed = false;

	if (curr_mol1 < 0){
		comp[0].push_back((*complex_list)[abs(curr_mol1)-1]);
	}
	if (curr_mol2 < 0){
		comp[1].push_back((*complex_list)[abs(curr_mol2)-1]);
	}

	for (pair_it = reactions.begin(); pair_it != reactions.end(); pair_it++){
		//If the second molecule we are analysing is a complex, we
		//store this complex in a list

		//Get the two entities that are reactants of the reactions we
		//are analysing 
		(*pair_it).second->IsOnlyMono(reactants);

		//Check how the entities are stored in the Elem we are
		//considering
		if ((*pair_it).first){
			iit = (*pair_it).second->GetIterator((*entity_list)[reactants[0] - 1], (*entity_list)[reactants[1] - 1], &comp[0], &comp[1], kind);
		} else {
			iit = (*pair_it).second->GetIterator((*entity_list)[reactants[0] - 1], (*entity_list)[reactants[1] - 1], &comp[1], &comp[0], kind);
		}


		iit->IteratorReset();


		action.first = (*pair_it).second;
		//If we have two unbound entities we have to execute an
		//action also if the iit->IteratorIsEnd==true, it is to
		//handle the bimolecular reaction between the two entities 
		if (comp[0].empty() && comp[1].empty() && iit->IteratorIsEnd()){
			env->ExecuteStep (elem_list, action, iit, &reaction);
			elem_list.clear();
			this->UpdateStructures(&reaction, action.first->GetBasalRate(iit));	
			action_executed = true;
		}

		while (!iit->IteratorIsEnd()){
			env->ExecuteStep (elem_list, action, iit, &reaction);
			elem_list.clear();
			this->UpdateStructures(&reaction, action.first->GetBasalRate(iit));
			iit->IteratorNext();
			action_executed = true;
		}
	}
	
	return action_executed;

}
