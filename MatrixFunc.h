#ifndef MATRIXFUNC_H_INCLUDED
#define MATRIXFUNC_H_INCLUDED

#include <limits>
#include "Environment.h"
#include "Expr.h"


class MatrixFunc{
	private:
		//Hash map of instantiated entities.
		hash_map<Entity*, list<Element*>* > instantiated_entities;


		//Stores the id of the instantiated molecules. They are stored in order of
		//appearance.
		list<int> molecules_list;
		//Stores the id of the instantiated molecules. These structure is used to
		//quickly check if a given molecule has been already instantiated.
		hash_map<int, bool> molecules_map;

		//For each entity stores the action it can perform. The keys of the map
		//represent the id of the entity, the object stored is a vector of four
		//list. In this list are stored: 
		//0) actions that can be only monomolecular and with finite rate
		//1) actions that can be only monomolecular and with infinite rate
		//2) actions with finite rate that can be both monomolecular and bimolecular
		//3) actions with finite rate that can be both monomolecular and bimolecular
		hash_map<int, list<Element *> *> actions_entities;

		//Stores molecules that can perform at least one monomolecular reaction with
		//infinite rate.
		hash_map<int, bool> transient_molecules;

		//List of elements in the act_map
		list<Map_Elem *> *act_map_list;
		//Points the last analysed element of the act_map list
		list<Map_Elem *>::iterator last_act_map;
		//Save the first iterator
		list<Map_Elem *>::iterator first_last_act_map;

		//List of elements in the inf_act_map
		list<Element *> *inf_act_map_list;
		//Points the last analysed element of the inf_act_map
		list<Element *>::iterator last_inf_act_map;
		//Save the first iterator
		list<Element *>::iterator first_last_inf_act_map;
		
	
		//Stores the reactions that are found
		hash_map<reaction_key, Expr*, reaction_hash_compare>  * reactions;

		//Stores the events with verb of type VERB_NEW and VERB_delete
		list<EventReaction*> new_delete_events;

		//Point to the environment
		Environment *env;

		//list of the molecule listed in the run directive of the input program
		hash_map<int, int> molecules_in_run;

		//List of complexes stored by the environment
		vector<Complex *> *complex_list;
		//List of entities stored by the environment
		vector<Entity *> *entity_list;

		//It si true if we are analysing the first molecule 
		bool first_loop;
		bool first_loop_inf;

		//Maximum number of molecules to be discovered (useful to stop the
		//algorithm when a number of infinite kinds of molecules can be
		//generated)
		unsigned int max_molecules_number;

	public:
		MatrixFunc(Environment *p_env, int max_molecules_number);
		void ExecuteNewEvents();
		void UpdateStructures(reaction_key *reaction, Element *elem, Expr *expr);
		//void FindPossibileBimoleculars(int molecule_id1, int molecule_id2,  list<pair<bool,Element *> > result[2]);
		//void FindPossibileBimoleculars(int molecule_id1, int molecule_id2,  list<pair<bool,Element *> > &result, bool inf);
		void FindPossibleTwoEntitiesElem(int molecule_id1, int molecule_id2,  list<pair<bool,Element *> > &result, bool inf, bool mono);
		hash_map<reaction_key, Expr*, reaction_hash_compare> *GetReactions(){return reactions;}
		hash_map<int, int>* GetMoleculeInRun(){return &molecules_in_run;} 
		
		list<EventReaction*>* GetNewDeleteEvents(){return &new_delete_events;}
		void AssociateNewActions();
		void AssociateNewInfActions();
		//void ExecuteInfEntityActions(Entity *entity, bool &infinite_actions, list<int>::iterator &it_mol_list);
		//void ExecuteEntityActions(Entity *entity);
		void ExecuteMonoActions(Entity *entity, Iterator_Interface *iit);
		bool ExecuteEvents(list<Element *> *elements, list<int>::iterator &current_mol, bool inf);
		//bool ExecuteElementWithTwoEntities(list<int>::iterator &curr_mol1, list<int>::iterator &curr_mol2, TwoIteratorKind kind, list<pair<bool, Element *> >reactions[2]);
		bool ExecuteElementWithTwoEntities(int curr_mol1, int curr_mol2, TwoIteratorKind kind, list<pair<bool, Element *> >reactions);
};

#endif

