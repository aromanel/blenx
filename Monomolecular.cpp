#include <iostream>
#include "Monomolecular.h"
#include "Environment.h"

using namespace std;


//////////////////////////////////////////////////////
// TAU METHODS DEFINITION
//////////////////////////////////////////////////////


//	
Tau::Tau(Element* p_elem, double p_rate,PP_Node *p_element, string p_label)
   : ElementMONO(p_elem, MONO_TAU) 
{
   rate = p_rate;
   element = p_element;
   label = p_label;
   actual_rate = 0.;
}

//
Tau::~Tau() {};

//
double Tau::GetRate() { 
	return rate; 
}

//
double Tau::GetActualRate() {
	return actual_rate;
}

//
bool Tau::ARateZero() { 
	return (actual_rate == 0.); 
}

//
string Tau::GetAction(PP_Node **e_ref,bool *is_sum) {
	string::size_type loc;
	loc = label.find("+", 0);
	if ( loc != string::npos ) *is_sum = true;
	else *is_sum = false;
	*e_ref = element;
	return label;
}


//
double Tau::EvalRate() {
	if (rate == HUGE_VAL) actual_rate = HUGE_VAL;
	else actual_rate = rate;
    return actual_rate;
}

//
double Tau::EvalTime(int n) {
   double rn = my_elem->GetEnv()->Random0To1();
	if ( actual_rate == 0. || rn == 0. )
		return HUGE_VAL;
	if ( actual_rate == HUGE_VAL) return 0.;
	return ( (double)(1) / ((double)n * actual_rate) ) * log( (double)(1) / rn ); 
}


//
void Tau::Print() {
	cout << actual_rate << " - " << label;
}

//
int Tau::GetNumber() {
	return 1;
}


//////////////////////////////////////////////////////
// DIE METHODS DEFINITION
//////////////////////////////////////////////////////

//	
Die::Die(Element* p_elem, double p_rate,PP_Node *p_element, string p_label)
   : ElementMONO(p_elem, MONO_DIE) 
{
	rate = p_rate;
	element = p_element;
	label = p_label;
	actual_rate = 0.;
}

//
Die::~Die() {};

//
double Die::GetRate() { 
	return rate; 
}

//
double Die::GetActualRate() {
	return actual_rate;
}

//
bool Die::ARateZero() { 
	return (actual_rate == 0.); 
}

//
string Die::GetAction(PP_Node **e_ref,bool *is_sum) {
	string::size_type loc;
	loc = label.find("+", 0);
	if ( loc != string::npos ) *is_sum = true;
	else *is_sum = false;
	*e_ref = element;
	return label;
}


//
double Die::EvalRate() {
	if (rate == HUGE_VAL) actual_rate = HUGE_VAL;
	else actual_rate = rate;
    return actual_rate;
}

//
double Die::EvalTime(int n) {
   double rn = my_elem->GetEnv()->Random0To1();
	if ( actual_rate == 0. || rn == 0. )
		return HUGE_VAL;
	if ( actual_rate == HUGE_VAL) return 0.;
	return ( (double)(1) / ((double)n * actual_rate) ) * log( (double)(1) / rn ); 
}


//
void Die::Print() {
	cout << actual_rate << " - " << label;
}

//
int Die::GetNumber() {
	return 1;
}

//////////////////////////////////////////////////////
// CHANGE METHODS DEFINITION
//////////////////////////////////////////////////////


//
Change::Change(Element* p_elem, double p_rate)
   : ElementMONO(p_elem, MONO_CHANGE) 
{ 
	rate = p_rate; 
	actual_rate = change = 0;
}

//
Change::~Change() {}

//
double Change::GetRate() { 
	return rate; 
}

//
double Change::GetActualRate() {
	return actual_rate;
}

//
bool Change::ARateZero() { 
	return (actual_rate == 0.); 
} 

//
string Change::GetAction(PP_Node **h_ref,bool *is_sum,Iterator_Interface *c_iter) {
	string::size_type loc;
	list<PAIR_RL>::iterator i;
	
	if ( c_iter != NULL )
	{
		assert(c_iter->GetCategory() == IT_MONO_CHANGE);
		i = ((Change_Iterator *)c_iter)->GetCurrent();
	}
	else 
	{
		i = change_list.begin();
		int rn = my_elem->GetEnv()->Random0UptoN(change);
		while(rn>0) { i++; rn--; }
	}

	loc = (*i).second.find("+", 0);
	if ( loc != string::npos ) *is_sum = true;
	else *is_sum = false;
	*h_ref = (*i).first;
	return (*i).second;
}

//
void Change::AddChange(PP_Node *element, string label) { 
	PAIR_RL p;
	p.first = element;
	p.second = label;
	change_list.push_front(p); 
	change++; 
}

//
double Change::EvalRate() {
	if ( change == 0 ) actual_rate = 0.0;
	else if (rate == HUGE_VAL) actual_rate = HUGE_VAL;
		 else actual_rate = rate * (double)change;
    return actual_rate;
}

//
double Change::EvalTime(int n) {
	double rn = my_elem->GetEnv()->Random0To1();
	if ( actual_rate == 0. || rn == 0. )
		return HUGE_VAL;
	if ( actual_rate == HUGE_VAL) return 0.;
	return ( (double)(1) / ((double)n * actual_rate) ) * log( (double)(1) / rn ); 
}

//
void Change::Print() {
	cout << rate << " " << change << " ";
	list<PAIR_RL>::iterator i;
	for(i=change_list.begin();i!=change_list.end();i++)
		cout << " [" << (*i).second << "]";
}

//
int Change::GetNumber() {
	return change;
}

//
Iterator_Interface *Change::GetIterator()
{
	return ( new Change_Iterator( this->change_list.begin(),this->change_list.size() ) );
}





//////////////////////////////////////////////////////
// INTRA METHODS DEFINITION
//////////////////////////////////////////////////////

//
Intra::Intra(Element* p_elem)
   : ElementMONO(p_elem, MONO_INTRA)
{ 
	in = out = mix = 0;  
	actual_rate = 0.; 
}

//
Intra::~Intra() {}

//
double Intra::GetRate() { 
	return rate; 
}

//
double Intra::GetActualRate() {
	return actual_rate;
}

//
string Intra::GetActionIn(PP_Node **ref, bool *is_sum, Iterator_Interface *c_iter) 
{
	string::size_type loc;
	list<PAIR_RL>::iterator i;
	if ( c_iter != NULL )
	{
		assert(c_iter->GetCategory() == IT_MONO_INTRA_IN);
		i = ((Intra_Iterator_In *)c_iter)->GetCurrent();
	}
	else
	{
		int rn = my_elem->GetEnv()->Random0UptoN(in);
		i = in_list.begin();
		while(rn>0) { i++; rn--; }
	}
	loc = (*i).second.find("+", 0);
	if ( loc != string::npos ) *is_sum = true;
	else *is_sum = false;
	*ref = (*i).first;
	return (*i).second;
}

//
string Intra::GetActionOut(list<BBinders *> *bb_list,PP_Node **ref, bool *is_sum, Iterator_Interface *c_iter) 
{
	string::size_type loc;
	PAIR_RL tmp_elem;
	int rn, tmp;
	list<PAIR_RL>::iterator i;

	if ( c_iter != NULL )
	{
		assert(c_iter->GetCategory() == IT_MONO_INTRA_OUT);
		i = ((Intra_Iterator_Out *)c_iter)->GetCurrent();
	}
	else
	{
		tmp = out;
		while(tmp>0) { 
			i = out_list.begin();
			rn = my_elem->GetEnv()->Random0UptoN(tmp);
			while (rn>0) { i++; rn--; }
			if (((PP_Output *)(*i).first)->ObjectBinder(bb_list)) {
				tmp_elem = *i;
				out_list.erase(i);
				out_list.push_back(tmp_elem);
				tmp--;
			}
			else break;
		}
	}

	loc = (*i).second.find("+", 0);
	if ( loc != string::npos ) *is_sum = true;
	else *is_sum = false;
	*ref = (*i).first;
	return (*i).second;
}

//
string Intra::GetAction(PP_Node **in_ref, PP_Node **out_ref, bool *is_sum1, bool *is_sum2, Iterator_Interface *c_iter) 
{
	string::size_type loc;
	list< pair<PAIR_RL *,PAIR_RL *> >::iterator i;

	if ( c_iter != NULL )
	{
		assert(c_iter->GetCategory() == IT_MONO_INTRA);
		i = ((Intra_Iterator *)c_iter)->GetCurrent();
	}
	else
	{
		i = actions.begin();
		int rn = my_elem->GetEnv()->Random0UptoN((int)actions.size());	
		while(rn>0) { i++; rn--; }
	}

	loc = (*i).first->second.find("+", 0);
	if ( loc != string::npos ) *is_sum1 = true;
	else *is_sum1 = false;
	loc = (*i).second->second.find("+", 0);
	if ( loc != string::npos ) *is_sum2 = true;
	else *is_sum2 = false;
	*in_ref = (*i).first->first;
	*out_ref = (*i).second->first;
	return ((*i).first->second + "," + (*i).second->second);
}

//
bool Intra::ARateZero() { 
	return (actual_rate == 0.); 
}

//
void Intra::SetRate(double p_rate) { 
	rate = p_rate; 
}

//
int Intra::GetIn() { 
	return in; 
}

//
int Intra::GetOut() { 
	return out; 
}

//
void Intra::EvalActions() {
	pair<PAIR_RL *,PAIR_RL *> p;
	string s_in,s_out;
	string::size_type loc;
	list<PAIR_RL>::iterator i;
	list<PAIR_RL>::iterator j;
	for(i=in_list.begin();i!=in_list.end();i++) {
		s_in = (*i).second;
		for(j=out_list.begin();j!=out_list.end();j++) {
			s_out = (*j).second;
			loc = s_in.find("+", 0);
			if (loc == string::npos || s_out.substr(0,s_out.size()-1)!=s_in.substr(0,s_in.size()-1)) {
				p.first = &(*i);
				p.second = &(*j);
				actions.push_front(p);
			}
		}
	}
	mix = (( in * out ) - (int)actions.size());
}

//
//
int Intra::GetNumber() {
	return ((in * out) - mix);
}

//
void Intra::PE() {
	list< pair<PAIR_RL *,PAIR_RL *> >::iterator k;
	for(k=actions.begin();k!=actions.end();k++) {
		cout << (*k).first->second << " {--} " << (*k).second->second << endl;
	}
}

//
void Intra::EvalMix() { 
	mix = in * out; 
}

//
void Intra::AddIn(PP_Node *element,string label) { 
	pair<PP_Node *,string> p;
	p.first = element;
	p.second = label;
	in_list.push_front(p); 
	in++;
}

//
void Intra::AddOut(PP_Node *element, string label) { 
	pair<PP_Node *,string> p;
	p.first = element;
	p.second = label;
	out_list.push_front(p);
	out++;
}

//
void Intra::AddSum(Intra *element) { 
	in += element->in;
	out += element->out;
	mix += element->mix;
	in_list.splice(in_list.end(),element->in_list);
	out_list.splice(out_list.end(),element->out_list);
}
	
//
double Intra::EvalRate() {
	double val = ( (double)in * (double)out ) - (double)mix;
	if ( val == 0. ) actual_rate = 0.0;
	else if (rate == HUGE_VAL) actual_rate = HUGE_VAL;
		 else actual_rate = rate * val;
    return actual_rate;
}

//
int Intra::ActionNumber() {
	return ( ( in * out ) - mix );
}
	
//
double Intra::EvalTime(int n) {
	double rn = my_elem->GetEnv()->Random0To1();
	if ( actual_rate == 0. || rn == 0. )
		return HUGE_VAL;
	if ( actual_rate == HUGE_VAL) return 0.;
	return ( (double)(1) / ((double)n * actual_rate) ) * log( (double)(1) / rn ); 
}

//	
void Intra::Print() { 
	cout << rate << " " << in << " " << out << " " << mix << " " << actual_rate << " ";
	list< pair<PP_Node *,string> >::iterator i;
	for(i=in_list.begin();i!=in_list.end();i++)
		cout << " [" << (*i).second << "]";
	cout << " - ";
	for(i=out_list.begin();i!=out_list.end();i++)
		cout << " [" << (*i).second << "]";
	cout << endl;
	cout << actions.size() << endl;
}

//
Iterator_Interface *Intra::GetIterator()
{
	return ( new Intra_Iterator( this->actions.begin(), this->actions.size() ) );
}

//
Iterator_Interface *Intra::GetIteratorIn()
{
	return ( new Intra_Iterator_In( this->in_list.begin(), this->in_list.size() ) );
}

//
Iterator_Interface *Intra::GetIteratorOut()
{
	return ( new Intra_Iterator_Out( this->out_list.begin(), this->out_list.size() ) );
}



//////////////////////////////////////////////////////
// EXPOSE METHODS DEFINITION
//////////////////////////////////////////////////////


//	
Expose::Expose(Element* p_elem, double p_rate,PP_Node *p_element, string p_label)
   : ElementMONO(p_elem, MONO_EXPOSE)
{
	rate = p_rate;
	element = p_element;
	label = p_label;
	actual_rate = 0.;
}

//
Expose::~Expose() {};

//
double Expose::GetRate() { 
	return rate; 
}

//
double Expose::GetActualRate() {
	return actual_rate;
}

//
bool Expose::ARateZero() { 
	return (actual_rate == 0.); 
}

//
string Expose::GetAction(PP_Node **e_ref,bool *is_sum) {
	string::size_type loc;
	loc = label.find("+", 0);
	if ( loc != string::npos ) *is_sum = true;
	else *is_sum = false;
	*e_ref = element;
	return label;
}


//
double Expose::EvalRate() {
	if (rate == HUGE_VAL) actual_rate = HUGE_VAL;
	else actual_rate = rate;
    return actual_rate;
}

//
double Expose::EvalTime(int n) {
	double rn = my_elem->GetEnv()->Random0To1();
	if ( actual_rate == 0. || rn == 0. )
		return HUGE_VAL;
	if ( actual_rate == HUGE_VAL) return 0.;
	return ( (double)(1) / ((double)n * actual_rate) ) * log( (double)(1) / rn ); 
}

//
void Expose::Print() {
	cout << actual_rate << " - " << label;
}

//
int Expose::GetNumber() {
	return 1;
}



//////////////////////////////////////////////////////
// HIDE METHODS DEFINITION
//////////////////////////////////////////////////////


//
Hide::Hide(Element* p_elem, double p_rate)
   : ElementMONO(p_elem, MONO_HIDE) 
{ 
	rate = p_rate; 
	actual_rate = hide = 0;
}

//
Hide::~Hide() {}

//
double Hide::GetRate() { 
	return rate; 
}

//
double Hide::GetActualRate() {
	return actual_rate;
}

//
bool Hide::ARateZero() { 
	return (actual_rate == 0.); 
} 

//
string Hide::GetAction(PP_Node **h_ref,bool *is_sum, Iterator_Interface *c_iter) {
	string::size_type loc;
	list<PAIR_RL>::iterator i;

	if ( c_iter != NULL )
	{
		assert(c_iter->GetCategory() == IT_MONO_HIDE);
		i = ((Hide_Iterator *)c_iter)->GetCurrent();
	}
	else
	{
		i = hide_list.begin();
		int rn = my_elem->GetEnv()->Random0UptoN(hide);
		while(rn>0) { i++; rn--; }
	}

	loc = (*i).second.find("+", 0);
	if ( loc != string::npos ) *is_sum = true;
	else *is_sum = false;
	*h_ref = (*i).first;
	return (*i).second;
}

//
void Hide::AddHide(PP_Node *element, string label) { 
	PAIR_RL p;
	p.first = element;
	p.second = label;
	hide_list.push_front(p); 
	hide++; 
}

//
double Hide::EvalRate() {
	if ( hide == 0 ) actual_rate = 0.0;
	else if (rate == HUGE_VAL) actual_rate = HUGE_VAL;
		 else actual_rate = rate * (double)hide;
    return actual_rate;
}

//
double Hide::EvalTime(int n) {
	double rn = my_elem->GetEnv()->Random0To1();
	if ( actual_rate == 0. || rn == 0. )
		return HUGE_VAL;
	if ( actual_rate == HUGE_VAL) return 0.;
	return ( (double)(1) / ((double)n * actual_rate) ) * log( (double)(1) / rn ); 
}

//
void Hide::Print() {
	cout << rate << " " << hide << " ";
	list<PAIR_RL>::iterator i;
	for(i=hide_list.begin();i!=hide_list.end();i++)
		cout << " [" << (*i).second << "]";
}

//
int Hide::GetNumber() {
	return hide;
}

//
Iterator_Interface *Hide::GetIterator()
{
	return ( new Hide_Iterator( this->hide_list.begin(), this->hide_list.size() ) );
}


//////////////////////////////////////////////////////
// UNHIDE METHODS DEFINITION
//////////////////////////////////////////////////////


//
Unhide::Unhide(Element* p_elem, double p_rate)
   : ElementMONO(p_elem, MONO_UNHIDE) 
{ 
	rate = p_rate; 
	actual_rate = unhide = 0; 
}

//
Unhide::~Unhide() {};

//
double Unhide::GetRate() { 
	return rate; 
}

//
double Unhide::GetActualRate() {
	return actual_rate;
}

//
bool Unhide::ARateZero() { 
	return (actual_rate == 0.); 
}

//
string Unhide::GetAction(PP_Node **u_ref, bool *is_sum, Iterator_Interface *c_iter) {
	string::size_type loc;
	list<PAIR_RL>::iterator i;

	if ( c_iter != NULL )
	{
		assert(c_iter->GetCategory() == IT_MONO_UNHIDE);
		i = ((Unhide_Iterator *)c_iter)->GetCurrent();
	}
	else
	{
		i = unhide_list.begin();
		int rn = my_elem->GetEnv()->Random0UptoN(unhide);
		while(rn>0) { i++; rn--; }
	}

	loc = (*i).second.find("+", 0);
	if ( loc != string::npos ) *is_sum = true;
	else *is_sum = false;
	*u_ref = (*i).first;
	return (*i).second;
}

//
void Unhide::AddUnhide(PP_Node *element, string label) { 
	PAIR_RL p;
	p.first = element;
	p.second = label;
	unhide_list.push_front(p); 
	unhide++; 
}

//
double Unhide::EvalRate() {
	if ( unhide == 0 ) actual_rate = 0.0;
	else if (rate == HUGE_VAL) actual_rate = HUGE_VAL;
		 else actual_rate = rate * (double)unhide;
    return actual_rate;
}

//
double Unhide::EvalTime(int n) {
	double rn = my_elem->GetEnv()->Random0To1();
	if ( actual_rate == 0. || rn == 0. )
		return HUGE_VAL;
	if ( actual_rate == HUGE_VAL) return 0.;
	return ( (double)(1) / ((double)n * actual_rate) ) * log( (double)(1) / rn ); 
}

//
void Unhide::Print() {
	cout << rate << " ";
	list<PAIR_RL>::iterator i;
	for(i=unhide_list.begin();i!=unhide_list.end();i++)
		cout << " [" << (*i).second << "]";
}

//
int Unhide::GetNumber() {
	return unhide;
}

//
Iterator_Interface *Unhide::GetIterator()
{
	return ( new Unhide_Iterator( this->unhide_list.begin(), this->unhide_list.size() ) );
}





