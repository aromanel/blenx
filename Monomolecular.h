#ifndef MONOMOLECULAR_H
#define MONOMOLECULAR_H

#include <cstdlib>
#include <string>
#include <list>
#include <map>
#include <vector>
#include <algorithm>
#include <cmath>
#include <ctime>
#include "Pi_Process.h"
#include "Entity.h"
#include "Define.h"

using namespace std;

class Entity;
class PP_Node;
class BBinders;

typedef pair<PP_Node *,string> PAIR_RL;

//////////////////////////////////////////////////////
// TAU CLASS
//////////////////////////////////////////////////////


class Tau : public ElementMONO {
private:
	PP_Node *element;
	string label;
	double actual_rate;
	double rate;
public:
	Tau(Element* p_elem, double p_rate,PP_Node *p_element, string p_label);
	~Tau();
	void Print();
	int GetNumber();
	double GetRate();
	double GetActualRate();
	double EvalRate();
	double EvalTime(int n);
	string GetAction(PP_Node **h_ref,bool *is_sum);
	bool ARateZero();
};


//////////////////////////////////////////////////////
// DIE CLASS
//////////////////////////////////////////////////////


class Die : public ElementMONO {
private:
	PP_Node *element;
	string label;
	double actual_rate;
	double rate;
public:
	Die(Element* p_elem, double p_rate,PP_Node *p_element, string p_label);
	~Die();
	void Print();
	int GetNumber();
	double GetRate();
	double GetActualRate();
	double EvalRate();
	double EvalTime(int n);
	string GetAction(PP_Node **h_ref,bool *is_sum);
	bool ARateZero();
};


//////////////////////////////////////////////////////
// CHANGE CLASS
//////////////////////////////////////////////////////

//
class Change_Iterator : public Iterator< list<PAIR_RL>::iterator >
{
public:
	Change_Iterator(list<PAIR_RL>::iterator p_iterator_begin, size_t p_iterator_size)
		: Iterator< list<PAIR_RL>::iterator >(p_iterator_begin,p_iterator_size,IT_MONO_CHANGE,0) {}
	~Change_Iterator() {}
};

//
class Change : public ElementMONO {
private:
	int change;
	double rate;
	double actual_rate;
	list<PAIR_RL> change_list;
public:
	Change(Element* p_elem, double p_rate);
	~Change();
	void Print();
	int GetNumber();
	void AddChange(PP_Node *element, string label);
	double GetRate();
	double GetActualRate();
	double EvalRate();
	double EvalTime(int n);
	string GetAction(PP_Node **h_ref,bool *is_sum, Iterator_Interface *c_iter);
	bool ARateZero();

	// Get Iterator
	Iterator_Interface *GetIterator();

};

//////////////////////////////////////////////////////
// INTRA CLASS
//////////////////////////////////////////////////////

//
class Intra_Iterator : public Iterator< list< pair<PAIR_RL *,PAIR_RL *> >::iterator >
{
public:
	Intra_Iterator(list< pair<PAIR_RL *,PAIR_RL *> >::iterator p_iterator_begin,
		size_t p_iterator_size) 
		: Iterator< list< pair<PAIR_RL *,PAIR_RL *> >::iterator >(p_iterator_begin,p_iterator_size,IT_MONO_INTRA,0) {}
	~Intra_Iterator() {}
};

//
class Intra_Iterator_In : public Iterator< list<PAIR_RL>::iterator >
{
public:
	Intra_Iterator_In(list<PAIR_RL>::iterator p_iterator_begin, size_t p_iterator_size)
		: Iterator< list<PAIR_RL>::iterator >(p_iterator_begin,p_iterator_size,IT_MONO_INTRA_IN,0) {}
	~Intra_Iterator_In() {}
};

//
class Intra_Iterator_Out : public Iterator< list<PAIR_RL>::iterator >
{
public:
	Intra_Iterator_Out(list<PAIR_RL>::iterator p_iterator_begin, size_t p_iterator_size)
		: Iterator< list<PAIR_RL>::iterator >(p_iterator_begin,p_iterator_size,IT_MONO_INTRA_OUT,0) {}
	~Intra_Iterator_Out() {}
};

//
class Intra : public ElementMONO {
private:
	int in;
	int out;
	int mix;
	double rate;
	double actual_rate;
	list<PAIR_RL> in_list;
	list<PAIR_RL> out_list;
	list< pair<PAIR_RL *,PAIR_RL *> > actions;
public:
	Intra(Element* p_elem);
	~Intra();
	void PE();
	void EvalActions();
	int GetNumber();
	void EvalMix();
	void AddIn(PP_Node *element,string label);
	void AddOut(PP_Node *element, string label);
	void AddSum(Intra *element);
	void SetRate(double p_rate);
	void Print();
	int GetIn();
	int GetOut();
	int ActionNumber();
	double EvalRate();
	double EvalTime(int n);
	double GetRate();
	double GetActualRate();
	string GetActionIn(PP_Node **ref, bool *is_sum, Iterator_Interface *c_iter);
	string GetActionOut(list<BBinders *> *bb_list, PP_Node **ref, bool *is_sum, Iterator_Interface *c_iter);
	string GetAction(PP_Node **in_ref, PP_Node **out_ref, bool *is_sum1, bool *is_sum2, Iterator_Interface *c_iter);
	bool ARateZero();

	// iterator methods
	Iterator_Interface *GetIterator();
	Iterator_Interface *GetIteratorIn();
	Iterator_Interface *GetIteratorOut();
};


//////////////////////////////////////////////////////
// EXPOSE CLASS
//////////////////////////////////////////////////////

class Expose : public ElementMONO {
private:
	PP_Node *element;
	string label;
	double actual_rate;
	double rate;
public:
	Expose(Element* p_elem, double p_rate,PP_Node *p_element, string p_label);
	~Expose();
	void Print();
	int GetNumber();
	double GetRate();
	double GetActualRate();
	double EvalRate();
	double EvalTime(int n);
	string GetAction(PP_Node **h_ref,bool *is_sum);
	bool ARateZero();
};


//////////////////////////////////////////////////////
// HIDE CLASS
//////////////////////////////////////////////////////

//
class Hide_Iterator : public Iterator< list<PAIR_RL>::iterator >
{
public:
	Hide_Iterator(list<PAIR_RL>::iterator p_iterator_begin, size_t p_iterator_size)
		: Iterator< list<PAIR_RL>::iterator >(p_iterator_begin,p_iterator_size,IT_MONO_HIDE,0) {}
	~Hide_Iterator() {}
};

//
class Hide : public ElementMONO {
private:
	int hide;
	double rate;
	double actual_rate;
	list<PAIR_RL> hide_list;
public:
	Hide(Element* p_elem, double p_rate);
	~Hide();
	void Print();
	int GetNumber();
	void AddHide(PP_Node *element, string label);
	double GetRate();
	double GetActualRate();
	double EvalRate();
	double EvalTime(int n);
	string GetAction(PP_Node **h_ref,bool *is_sum, Iterator_Interface *c_iter);
	bool ARateZero();

	// iterator methods
	Iterator_Interface *GetIterator();
};


//////////////////////////////////////////////////////
// UNHIDE CLASS
//////////////////////////////////////////////////////

//
class Unhide_Iterator : public Iterator< list<PAIR_RL>::iterator >
{
public:
	Unhide_Iterator(list<PAIR_RL>::iterator p_iterator_begin, size_t p_iterator_size)
		: Iterator< list<PAIR_RL>::iterator >(p_iterator_begin,p_iterator_size,IT_MONO_UNHIDE,0) {}
	~Unhide_Iterator() {}
};

//
class Unhide : public ElementMONO {
private:
	int unhide;
	double rate;
	double actual_rate;
	list<PAIR_RL> unhide_list;
public:
	Unhide(Element* p_elem, double p_rate);
	~Unhide();
	void Print();
	int GetNumber();
	void AddUnhide(PP_Node *element, string label);
	double GetRate();
	double GetActualRate();
	double EvalRate();
	double EvalTime(int n);
	string GetAction(PP_Node **u_ref,bool *is_sum, Iterator_Interface *c_iter);
	bool ARateZero();

	// iterator methods
	Iterator_Interface *GetIterator();
};


#endif


