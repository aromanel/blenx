
#include <iostream>
#include "PT_Action_Node.h"

#include "Symbol_Table.h"
#include "Environment.h"

using namespace std;

//////////////////////////////////////////////////////
// PTN_ACTION_TAU METHODS DEFINITION
//////////////////////////////////////////////////////

//
PTN_Action_Tau::PTN_Action_Tau(const Pos& pos, string p_rate):PTNode(0,pos,TAU) {
	rate = p_rate;
}

//
PTN_Action_Tau::~PTN_Action_Tau() {
}

//
bool PTN_Action_Tau::GenerateIC(PP_Node **parent, SymbolTable *st) 
{
   if (!CheckRate(st, rate, "TAU"))
      return false;

	PP_Node *element = new PP_Tau(rate);
	if (*parent != NULL) (*parent)->InsertChild(element);
	else *parent = element;
   return true;
}

//
void PTN_Action_Tau::PrintType() {
	cout << "@:" << rate;
}


//////////////////////////////////////////////////////
// PTN_ACTION_DIE METHODS DEFINITION
//////////////////////////////////////////////////////

//
PTN_Action_Die::PTN_Action_Die(const Pos& pos, string p_rate):PTNode(0,pos,DIE) {
	rate = p_rate;
}

//
PTN_Action_Die::~PTN_Action_Die() {
}

//
bool PTN_Action_Die::GenerateIC(PP_Node **parent, SymbolTable *st) 
{
   if (!CheckRate(st, rate, "DIE"))
      return false;

	PP_Node *element = new PP_Die(rate);
	if (*parent != NULL) (*parent)->InsertChild(element);
	else *parent = element;
   return true;
}

//
void PTN_Action_Die::PrintType() {
	cout << "die(" << rate << ")";
}


//////////////////////////////////////////////////////
// PTN_ACTION_CHANGE METHODS DEFINITION
//////////////////////////////////////////////////////

//
PTN_Action_Change::PTN_Action_Change(const Pos& pos, const string& p_rate, const string& p_subject, const string& p_type):PTNode(0,pos,CHANGE){
	rate = p_rate;
	subject = p_subject;
	type = p_type;
}

//
PTN_Action_Change::~PTN_Action_Change() {
}

//
void PTN_Action_Change::FnList(list<Bound_Name> *bn, list<Free_Name> *fn, SymbolTable *st) {
	std::list<Bound_Name>::iterator i;
	for(i=bn->begin();i!=bn->end();i++) {
		if ( this->subject == (*i).name && (*i).type == BN_INPUT ) {
			string message = "The subject of the change action is bounded by an input object. Deadlock subprocesses could be generated.";
			Error_Manager::PrintWarning(pos, 1, message);
			return;
		}
	}
	Free_Name fn_object(subject,FN_BINDER_SUB);
	fn->push_front(fn_object);
}

//
bool PTN_Action_Change::GenerateIC(PP_Node **parent, SymbolTable *st) 
{
   if (!CheckRate(st, rate, "CHANGE"))
      return false;

	PP_Node *element = new PP_Change(rate,subject,type);
	if (*parent != NULL) (*parent)->InsertChild(element);
	else *parent = element;
   return true;
}

//
void PTN_Action_Change::PrintType() {
	cout << "ch(" << subject << "," << type << "," << rate << ")";
}

//
bool PTN_Action_Change::GenerateST(SymbolTable *st) 
{
	string message;
   if (!CheckRate(st, rate, "CHANGE"))
   {
		message = "rate for CHANGE action not defined, or it is not a valid constant, template name or number";
		Error_Manager::PrintError(pos, 2, message);
		return false;
	}
   if (!st->FindType(type)) 
	{
		message = "type '" + type + "' not defined";
		Error_Manager::PrintError(pos, 3, message);
		return false;
	}
   if (st->GetEnv()->GetTypeAffinity()->Find(subject))
	{
		message = "'" + subject + "' must be a subject, not a type";
		Error_Manager::PrintError(pos, 4, message);
		return false;
	}

	return true;
}



//////////////////////////////////////////////////////
// PTN_ACTION_IO METHODS DEFINITION
//////////////////////////////////////////////////////

//
PTN_Action_IO::PTN_Action_IO(const Pos& pos,string p_subject, string p_object, ActionType p_type):PTNode(0,pos,IO) {
	subject = p_subject;
	object = p_object;
	type = p_type;
}

//
PTN_Action_IO::~PTN_Action_IO() {
}

//
void PTN_Action_IO::FnList(list<Bound_Name> *bn, list<Free_Name> *fn, SymbolTable *st) {
	list<Bound_Name>::iterator i;
	bool is_sub = false;
	bool is_obj = false;
	for(i=bn->begin();i!=bn->end();i++) {
		if ((*i).name == subject) is_sub = true;
		if ((*i).name == object) is_obj = true;
	}
	if (!is_sub) {
		Free_Name fn_subject(subject,FN_GENERAL);
		fn->push_front(fn_subject);
	}
	if (!is_obj && type == ACTION_OUTPUT && object != EMPTY_STRING) {
		Free_Name fn_object(object,FN_GENERAL);
		fn->push_front(fn_object);
	}
	if (type == ACTION_INPUT && object != EMPTY_STRING) {
		Bound_Name bn_object(object,BN_INPUT);
		bn->push_front(bn_object);
	}
}

//
bool PTN_Action_IO::GenerateIC(PP_Node **parent, SymbolTable *st)
{
	PP_Node *element;
	if (type == ACTION_INPUT) 
      element = new PP_Input(subject,object);
	else 
      element = new PP_Output(subject,object);
	if (*parent != NULL) (*parent)->InsertChild(element);
	else *parent = element;
   return true;
}

//
void PTN_Action_IO::PrintType() {
	cout << subject << " - " << object << " - " << type;
}


//////////////////////////////////////////////////////
// PTN_ACTION_EXPOSE METHODS DEFINITION
//////////////////////////////////////////////////////

//
PTN_Action_Expose::PTN_Action_Expose(const Pos& pos, string p_name, string p_rate, string p_name_rate, string p_id):PTNode(0,pos,EXPOSE) {
	name = p_name;
	name_rate = p_name_rate;
	rate = p_rate;
	id = p_id;
}

//
PTN_Action_Expose::~PTN_Action_Expose() {
}

//
void PTN_Action_Expose::FnList(list<Bound_Name> *bn, list<Free_Name> *fn, SymbolTable *st) { 
	// the name is inserted in front of the list
	Bound_Name bn_object(name,BN_EXPOSE);
	bn->push_front(bn_object);
}

//
bool PTN_Action_Expose::GenerateST(SymbolTable *st) {
	string message;
	if (!CheckRate(st, rate, "EXPOSE")) 
   {
		message = "rate of type 'EXPOSE' not defined, or it is not a valid constant, template name or number";
		Error_Manager::PrintError(pos, 2, message);
		return false;
	}
   if (!st->FindType(id)) 
   {
		message = "type '" + id + "' not defined";
      Error_Manager::PrintError(pos, 3, message);
		return false;
	}
	return true;
}

//
bool PTN_Action_Expose::GenerateIC(PP_Node **parent, SymbolTable *st) {
	PP_Node *element = new PP_Expose(name,id,rate,name_rate);
	if (*parent != NULL) (*parent)->InsertChild(element);
	else *parent = element;
   return true;
}

//
void PTN_Action_Expose::PrintType() {
	cout << "Expose " << rate << " - " << name << " : " << name_rate << "," << id;
}



//////////////////////////////////////////////////////
// PTN_ACTION_HU METHODS DEFINITION
//////////////////////////////////////////////////////

//
PTN_Action_HU::PTN_Action_HU(const Pos& pos, string p_name, string p_rate, ActionType p_type)
   : PTNode(0, pos, HIDEUNHIDE) {
	name = p_name;
	rate = p_rate;
	type = p_type;
}

//
PTN_Action_HU::~PTN_Action_HU() {
}

//
bool PTN_Action_HU::GenerateST(SymbolTable *st) 
{
	string message;
	
	if (type == ACTION_HIDE && 
      !CheckRate(st, rate, "HIDE")) 
   {
		message = "rate of type 'HIDE' not defined, or it is not a valid constant, template name or number";
		Error_Manager::PrintError(pos, 2,message);
		return false;
	}
	else if (type == ACTION_UNHIDE && 
            !CheckRate(st, rate, "UNHIDE"))
   {
		message = "rate of type 'UNHIDE' not defined, or it is not a valid constant, template name or number";
		Error_Manager::PrintError(pos, 2,message);
		return false;
	}
   
	return true;
}

//
void PTN_Action_HU::FnList(list<Bound_Name> *bn, list<Free_Name> *fn, SymbolTable *st) {
	std::list<Bound_Name>::iterator i;
	for(i=bn->begin();i!=bn->end();i++) {
		if ( this->name == (*i).name && (*i).type == BN_INPUT ) {
			string message = "The subject of the hide or unhide action is bounded by an input object. Deadlock subprocesses could be generated.";
			Error_Manager::PrintWarning(pos, 1, message);
			return;
		}
	}
	Free_Name fn_object(name,FN_BINDER_SUB);
	fn->push_front(fn_object);
}

//
bool PTN_Action_HU::GenerateIC(PP_Node **parent, SymbolTable *st) {
	PP_Node *element;
	if (type == ACTION_HIDE) 
      element = new PP_Hide(name,rate);
	else 
      element = new PP_Unhide(name,rate);
	if (*parent != NULL) (*parent)->InsertChild(element);
	else *parent = element;
   return true;
}

//
void PTN_Action_HU::PrintType() {
	cout << "Hide/Unhide " << name << " - " << rate; 
}

