#ifndef PT_ACTION_NODE_H
#define PT_ACTION_NODE_H

#include <cstdlib>
#include <string>
#include "PT_General_Node.h"
#include "Type_Affinity.h"

using namespace std;


//////////////////////////////////////////////////////
// PTN_ACTION_TAU CLASS
//////////////////////////////////////////////////////

class PTN_Action_Tau : public PTNode {
private:
	string rate;
public:
	PTN_Action_Tau(const Pos& pos, string p_rate);
	~PTN_Action_Tau();

	bool GenerateIC(PP_Node **parent, SymbolTable *st);
	void PrintType();
};


//////////////////////////////////////////////////////
// PTN_ACTION_DIE CLASS
//////////////////////////////////////////////////////

class PTN_Action_Die : public PTNode {
private:
	string rate;
public:
	PTN_Action_Die(const Pos& pos, string p_rate);
	~PTN_Action_Die();

	bool GenerateIC(PP_Node **parent, SymbolTable *st);
	void PrintType();
};


//////////////////////////////////////////////////////
// PTN_ACTION_CHANGE CLASS
//////////////////////////////////////////////////////

class PTN_Action_Change : public PTNode {
private:
	string rate;
	string subject;
	string type;
public:
	PTN_Action_Change(const Pos& pos, const string& p_rate, const string& p_subject, const string& p_type);
	~PTN_Action_Change();

	void FnList(list<Bound_Name> *bn, list<Free_Name> *fn, SymbolTable *st);
	bool GenerateIC(PP_Node **parent, SymbolTable *st);
	void PrintType();

	bool GenerateST(SymbolTable *st);
};


//////////////////////////////////////////////////////
// PTN_ACTION_IO CLASS
//////////////////////////////////////////////////////

class PTN_Action_IO : public PTNode {
private:
	string subject;
	string object;
	ActionType type;
public:
	PTN_Action_IO(const Pos& pos,string p_subject, string p_object, ActionType p_type);
	~PTN_Action_IO();

	void FnList(list<Bound_Name> *bn, list<Free_Name> *fn, SymbolTable *st);
	bool GenerateIC(PP_Node **parent, SymbolTable *st);
	void PrintType();
};


//////////////////////////////////////////////////////
// PTN_ACTION_EXPOSE CLASS
//////////////////////////////////////////////////////

class PTN_Action_Expose : public PTNode {
private:
	string name;
	string name_rate;
	string id;
	string rate;
public:
	PTN_Action_Expose(const Pos& pos, string p_name, string p_rate, string p_name_rate, string p_id);
	~PTN_Action_Expose();

	void FnList(list<Bound_Name> *bn, list<Free_Name> *fn, SymbolTable *st);
	bool GenerateIC(PP_Node **parent, SymbolTable *st);
	void PrintType();

	bool GenerateST(SymbolTable *st);
};


//////////////////////////////////////////////////////
// PTN_ACTION_HU CLASS
//////////////////////////////////////////////////////

class PTN_Action_HU : public PTNode {
private:
	string name;
	string rate;
	ActionType type;
public:
	PTN_Action_HU(const Pos& pos, string p_name, string p_rate, ActionType p_type);
	~PTN_Action_HU();

	void FnList(list<Bound_Name> *bn, list<Free_Name> *fn, SymbolTable *st);
	bool GenerateIC(PP_Node **parent, SymbolTable *st);
	void PrintType();

	bool GenerateST(SymbolTable *st);
};

#endif

