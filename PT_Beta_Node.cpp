#include <iostream>
#include "PT_Beta_Node.h"
#include "Symbol_Table.h"
#include "Environment.h"

using namespace std;




//////////////////////////////////////////////////////
// PTN_BETA METHODS DEFINITION
//////////////////////////////////////////////////////

//
PTN_Beta::PTN_Beta(const Pos& pos,PTNode *node1, PTNode *node2):PTNode(2,pos,BETA) {
	this->Insert(node1,0);
	this->Insert(node2,1);
}

//
PTN_Beta::~PTN_Beta() {
}

//
void PTN_Beta::PrintType() {
	cout << "Beta Process";
}

//////////////////////////////////////////////////////
// PTN_BINDER METHODS DEFINITION
//////////////////////////////////////////////////////

//
PTN_Binder::PTN_Binder(const Pos& pos, string p_subject, string p_id, BinderState p_state, string p_rate):PTNode(0,pos,BINDER) {
	subject = p_subject;
	id = p_id;
	rate = p_rate;
	state = p_state;
}
	
//
PTN_Binder::~PTN_Binder() {
}

//
void PTN_Binder::FnList(list<Bound_Name> *bn, list<Free_Name> *fn, SymbolTable *st) {
	// the name is inserted in front of the list
	Bound_Name bn_object(subject,BN_BINDER);
	bn->push_front(bn_object);
}

//
bool PTN_Binder::GenerateST(SymbolTable *st) 
{
	string message;
	double v_rate;
	bool res = true;

   if (st->GetCurrent()->GetCategory() == BPROC)
   {
      if (!st->FindType(id)) 
      {
		   message = "type '" + id + "' not defined";
		   Error_Manager::PrintError(pos, 3,message);
		   res = false;
	   }
   	// If the current ST element is not NULL we control the correctness of the
	   // binder.

      ST_Beta_Proc* current = (ST_Beta_Proc*)(st->GetCurrent()); 
      if (!TranslateRate(st, rate, v_rate))
         return false;
		
		BBinders *elem = new BBinders(subject,v_rate,id,state);
      if (!current->InsertBinder(elem)) 
      {
         message = "subject or type of binder '(" + subject + ":" + id + ")' already present in the binder list.";
         Error_Manager::PrintError(pos, 4, message);
         delete elem;
         res = false;
      }
   }
   else if (st->GetCurrent()->GetCategory() == BPROC_TEMPLATE)
   {
      ST_Bproc_Template* current = (ST_Bproc_Template*)(st->GetCurrent());
      
      if (!TranslateRate(st, rate, v_rate))
         return false;
      
      BBinders *elem = new BBinders(subject,v_rate,id,state);
      if (!current->InsertBinder(elem)) 
      {
			message = "subject or type of binder '(" + subject + ":" + id + ")' already present in the binder list.";
			Error_Manager::PrintError(pos, 4, message);
			delete elem;
			res = false;
		}
	}
   else
   {
      message = "symbols of type '" + ToString(st->GetCurrent()->GetCategory()) + "' cannot have a binder list.";
      Error_Manager::PrintError(pos, 4, message);
      res = false;
   }

	return res;
}

//
void PTN_Binder::PrintType() {
	cout << subject << ":" << id;
}


//////////////////////////////////////////////////////
// PTN_BINDER_PAIR METHODS DEFINITION
//////////////////////////////////////////////////////

//
PTN_Binder_Pair::PTN_Binder_Pair(const Pos& pos, PTNode *node1, PTNode *node2):PTNode(2,pos,BINDER_PAIR) {
	this->Insert(node1,0);
	this->Insert(node2,1);
}

//
PTN_Binder_Pair::~PTN_Binder_Pair() {
}

//
void PTN_Binder_Pair::PrintType() {
	cout << "Binder List";
}


//////////////////////////////////////////////////////
// PTN_BP METHODS DEFINITION
//////////////////////////////////////////////////////


//
PTN_Bp::PTN_Bp(const Pos& pos, const string& p_id, const string& p_number)
   : PTNode(0, pos, BP)
{
	id = p_id;
	number = p_number;
}

//
PTN_Bp::~PTN_Bp() {
}

//
bool PTN_Bp::GenerateST(SymbolTable *st) 
{
	ST_Beta_Proc* current_beta = (ST_Beta_Proc *)st->Find(id, BPROC);
   if (current_beta != NULL) 
   {
      current_beta->AddCounter(atoi(number.c_str()));
      return true;
   }
   
   ST_Mol_Def* current_mol = (ST_Mol_Def*)st->Find(id, MOL);   
   if (current_mol != NULL)
   {
      current_mol->AddCounter(atoi(number.c_str()));
      return true;
   }
    
   string message = "beta-process or molecule '" + id + "' not defined";
		Error_Manager::PrintError(pos, 3, message);
		return false;
}

//
void PTN_Bp::PrintType() {
	cout << id << " " << number;
}


//////////////////////////////////////////////////////
// PTN_BP_PAIR METHODS DEFINITION
//////////////////////////////////////////////////////

//	
PTN_Bp_Pair::PTN_Bp_Pair(const Pos& pos, PTNode *node1, PTNode *node2):PTNode(2,pos,BP_PAIR) {
	this->Insert(node1,0);
	this->Insert(node2,1);
}

//
PTN_Bp_Pair::~PTN_Bp_Pair() {
}

//
void PTN_Bp_Pair::PrintType() {
	cout << "Bp List";
}

