#ifndef PT_BETA_NODE_H
#define PT_BETA_NODE_H

#include <cstdlib>
#include <string>
#include "PT_General_Node.h"
#include "Type_Affinity.h"

using namespace std;


//////////////////////////////////////////////////////
// PTN_BETA CLASS
//////////////////////////////////////////////////////

class PTN_Beta : public PTNode {
public:
	PTN_Beta(const Pos& pos,PTNode *node1, PTNode *node2);
	~PTN_Beta();

	void PrintType();
};


//////////////////////////////////////////////////////
// PTN_BINDER CLASS
//////////////////////////////////////////////////////

class PTN_Binder : public PTNode {
private:
	string subject;
	string id;
	string rate;
	BinderState state;
public:
	PTN_Binder(const Pos& pos, string p_subject, string p_id, BinderState p_state, string p_rate);
	~PTN_Binder();

	void FnList(list<Bound_Name> *bn, list<Free_Name> *fn, SymbolTable *st);
	void PrintType();

	bool GenerateST(SymbolTable *st);
};


//////////////////////////////////////////////////////
// PTN_BINDER_PAIR CLASS
//////////////////////////////////////////////////////

class PTN_Binder_Pair : public PTNode {
public:
	PTN_Binder_Pair(const Pos& pos, PTNode *node1, PTNode *node2);
	~PTN_Binder_Pair();

	void PrintType();
};


//////////////////////////////////////////////////////
// PTN_BP CLASS
//////////////////////////////////////////////////////

class PTN_Bp : public PTNode {
private:
	string id;
	string number;
public:
	PTN_Bp(const Pos& pos, const string& p_id, const string& p_number);
	~PTN_Bp();

	bool GenerateST(SymbolTable *st);
	void PrintType();
};


//////////////////////////////////////////////////////
// PTN_BP_PAIR CLASS
//////////////////////////////////////////////////////

class PTN_Bp_Pair : public PTNode {
public:
	PTN_Bp_Pair(const Pos& pos, PTNode *node1, PTNode *node2);
	~PTN_Bp_Pair();

	void PrintType();
};

#endif

