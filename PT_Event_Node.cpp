
#include "PT_Event_Node.h"
#include <iostream>
#include <sstream>

using std::cout;

void PTN_Event::PrintType(void)
{
   cout << "Event";
}

void PTN_Event_Verb::PrintType(void)
{
   cout << "Event Verb";
}

void PTN_Event_Cond::PrintType(void)
{
   cout << "Event Condition";
}

void PTN_EventCond_State::PrintType(void)
{
   cout << "Event State";
}

void PTN_EventCond_StateList::PrintType(void)
{
   cout << "Event State list";
}

void PTN_Event_Entity::PrintType(void)
{
   cout << "Event originating Entity";
}

void PTN_Event_EntityList::PrintType(void)
{
   cout << "Event Entity list";
}

bool PTN_Event::CheckRateOrFunction(const Pos& pos, SymbolTable *st, const std::string& rate_or_func)
{
   if (!(rate_or_func == INF_RATE ||
         st->FindConstant(rate_or_func) ||
         Utility::IsNumeric(rate_or_func) ||
         st->FindFunction(rate_or_func)))
   {
      Error_Manager::PrintError(pos, 35, "'" + rate_or_func + "' is not a valid rate constant, number, or rate function.");
      return false;
   }

   return true;
}


bool PTN_Event::GenerateST(SymbolTable* st)
{
   if (!st->AddEvent(inheritable, this))
      return false;

   ST_Event* current_event = (ST_Event*)st->GetCurrent();

   //generate the children
   PTN_Event_Cond* cond = (PTN_Event_Cond*)Get(0);
   PTN_Event_Verb* verb = (PTN_Event_Verb*)Get(1);

   if (!cond->GenerateST(st))
      return false;

   if (!verb->GenerateST(st))
      return false;

   if (!CheckRateOrFunction(pos, st, cond->rateOrFun))
      return false;

   current_event->SetEventRate(cond->rateOrFun);
   current_event->SetParameters(cond->param1,cond->param2);

   // Creo lista di valori 
   if ( cond->parameters != NULL )
   {
	   std::vector< pair<double,double> > v_parameters = cond->parameters->GetParameters();
	   current_event->SetParameterVector(v_parameters);
   }
   //verb will be set later, just before Event creation (ST_Event::InitAM1)
   current_event->SetEventCond(cond->GetEventCond());

   return true;
}

bool PTN_Event_Verb::GenerateIC(PP_Node** parent, SymbolTable* st)
{
   //now for the fake entries, generate real ones taking as 
   //template the source_processes

   //perform substitution on BBinders

   if (!join_processes.empty())
   {
      PP_Node* current;
      if (*parent != NULL)
      {
         if ((*parent)->GetCategory() == PP_PARALLEL) 
            current = *parent;
         else 
            (*parent)->InsertChild(current = new PP_Parallel());
      }
      else 
         current = *parent = new PP_Parallel();

      std::vector<ST_Beta_Proc*>::iterator it;
      for (it = join_processes.begin(); it != join_processes.end(); ++it)
      {
         // AddEntity is called in SymbolTable::InitAM1, but we can generate a 
         // valid bproc first.
         // However, we MUST do this AFTER
         // GenerateIC is called on ST_Beta_Node*s in source_processes

         if (!(**it).GenerateIC(st))
            return false;

         // we want to rename before copying...?
         hash_map< ST_Beta_Proc*, std::list< std::pair<string, string> > >::iterator map_it;
         map_it = replaced_names.find(*it);
         if (map_it != replaced_names.end())
         {
            std::list< std::pair<string, string> >::iterator list_it;
            for (list_it = map_it->second.begin(); list_it != map_it->second.end(); ++list_it)
            {            
               (**it).GetIC()->Renaming(list_it->first, list_it->second);
            }
         }

         PP_Node* element = (**it).GetIC()->GetCopy();
         current->InsertChild(element);      
      }         
   }
   return true;
}

bool PTN_Event_Verb::GenerateST(SymbolTable* st)
{
   ST_Event* current_event = (ST_Event*)st->GetCurrent();

   current_event->verbType = verbType;

   bool quantitesOk = false;

   // check some conditions on verbs
   switch (current_event->OriginatingProcesses.size())
   {
   case 0:
      {
         if (verbType == VERB_UPDATE) 
            quantitesOk = true;
         else
            quantitesOk = false;
      }
      break;

   case 1:
      {
         if (verbType == VERB_JOIN || verbType == VERB_UPDATE)
            quantitesOk = false;
         else
            quantitesOk = true;
      }
      break;

   case 2:
      {
         if (verbType == VERB_JOIN)
            quantitesOk = true;
         else
            quantitesOk = false;
      }
      break;
   }
  
   if (!quantitesOk)
   {
      Error_Manager::PrintError(pos, 42, "wrong number of processes (" + Utility::i2s((int)current_event->OriginatingProcesses.size()) + ") for event " + ToString(verbType));
      return false;
   }

   if (verbType == VERB_UPDATE)
   {
      string var_name = affected_list.front().first;
      string fun_name = affected_list.back().first;


      ST_Statevar* stateVar = dynamic_cast<ST_Statevar*>(st->Find(var_name, STATE_VAR));
      if (stateVar == NULL)
      {
         string message = "state variable '" + var_name + "' not defined";
         Error_Manager::PrintError(pos, 3, message);
         return false;
      }

      FunBase* funcRef = st->FindFunction(fun_name);
      if (funcRef == NULL)
      {
         string message = "function '" + fun_name + "' not defined";
         Error_Manager::PrintError(pos, 3, message);
         return false;
      }

      current_event->verbStateVar = stateVar;
      current_event->verbUpdateFunc = funcRef;
   }
   else
   {
      // First, build a list of affected entities using ST informations
      list< ST_Beta_Proc* >::iterator s_it = current_event->OriginatingProcesses.begin();

      std::list< std::pair<string, int> >::iterator it;
      for (it = affected_list.begin(); it != affected_list.end(); ++it)
      {
         if (it->first == NO_PROCESS)
         {
            // this is the case of delete(3) or new(2); 
            assert (verbType == VERB_DELETE || verbType == VERB_NEW);

            if (s_it == current_event->OriginatingProcesses.end())
            {
               // raise error
               Error_Manager::PrintError(pos, 15, "source and destination lists of this event are mismatched");
               return false;
            }
            else
            {
               current_event->AffectedProcesses.push_back(pair<ST_Beta_Proc*, int>(*s_it, it->second));
               ++s_it;
            }
         }
         else
         {
            //can also be the NIL BPROC!
            ST_Beta_Proc* beta_proc;
            if (it->first == NIL_BPROC)
            {
               beta_proc = NULL;
            }
            else
            {
               beta_proc = dynamic_cast<ST_Beta_Proc*>(st->Find(it->first, BPROC));
            if (beta_proc == NULL)
            {
               string message = "beta-process '" + it->first + "' not defined";
               Error_Manager::PrintError(pos, 3, message);
               return false;
               }
            }
            current_event->AffectedProcesses.push_back(pair<ST_Beta_Proc*, int>(beta_proc, it->second));
         }
      }

      // Then, use the list. 
      if (current_event->AffectedProcesses.empty() && verbType == VERB_JOIN)
      {
         //automatically generate target entity        
         //already checked that it has two
         string join_proc_name = "$join_" + current_event->OriginatingProcesses.front()->GetId() + "_" + current_event->OriginatingProcesses.back()->GetId();

         //need: PP_node, BBinders -> Entity
         //here we only reserve an entry in SymbolTable (ST_Beta_Proc)
         ST_Beta_Proc* join_proc = new ST_Beta_Proc(join_proc_name, BPROC, this);

         //BBinders
         //first pass: rename if necessay
         list<ST_Beta_Proc*>::iterator it;
         hash_map<string, std::pair<string, ST_Beta_Proc*> > type_binders_map;

         for (it = current_event->OriginatingProcesses.begin(); it != current_event->OriginatingProcesses.end(); ++it)
         {            
            std::list<BBinders*>::iterator bb_it;

            // be careful with names!
            for (bb_it = (*it)->GetBinderList()->begin(); bb_it != (*it)->GetBinderList()->end(); ++bb_it)
            {   
               hash_map<string, std::pair<string, ST_Beta_Proc*> >::iterator type_bb_it;
               type_bb_it = type_binders_map.find((**bb_it).GetType());

               if (type_bb_it != type_binders_map.end())
               {
                  //type already in another source beta-process
                  std::pair<string, ST_Beta_Proc*>& prev_entry = type_bb_it->second;

                  if ((**bb_it).GetSubject() != prev_entry.first)
                  {
                     // rename both, and join too
                     string new_subject_name = "$" + type_bb_it->first;

                     // Issue a warining
                     stringstream message;
                     message << "the subjects of type '" << (**bb_it).GetType() << "' are named '" 
                        << (**bb_it).GetSubject() << "' in process '" << (**it).GetIdentifier() << "' and '" 
                        << prev_entry.first <<  "' in process '" << prev_entry.second->GetIdentifier() 
                        << "': renaming both to '" + new_subject_name + "' to allow a join";

                     Error_Manager::PrintWarning(pos, 14, message.str());

                     replaced_names[*it].push_back(pair<string, string>((**bb_it).GetSubject(), new_subject_name));
                     replaced_names[prev_entry.second].push_back(pair<string, string>(prev_entry.first, new_subject_name));


                     //rename binders also:
                     //actual binder
                     (**bb_it).SetSubject(new_subject_name);
                     //previos binder
                     BBinders* prev_binder = prev_entry.second->FindBinderByName(prev_entry.first);
                     prev_binder->SetSubject(new_subject_name);
                  }
               }
               else
               {
                  type_binders_map[(**bb_it).GetType()] = std::pair<string, ST_Beta_Proc*>((**bb_it).GetSubject(), *it);
               }
            }
         }

         //second pass: copy
         for (it = current_event->OriginatingProcesses.begin(); it != current_event->OriginatingProcesses.end(); ++it)
         {
            std::list<BBinders*>::iterator bb_it;
            for (bb_it = (*it)->GetBinderList()->begin(); bb_it != (*it)->GetBinderList()->end(); ++bb_it)
            {  
               // copy the binder list     
               BBinders* new_binder = new BBinders(**bb_it);
               join_proc->InsertBinder(new_binder);
            }
         }

         current_event->AffectedProcesses.push_back(std::pair<ST_Beta_Proc*, int>(join_proc, 1));

         join_processes.push_back(current_event->OriginatingProcesses.front());
         join_processes.push_back(current_event->OriginatingProcesses.back());

         st->STInsert(join_proc);
         // see PTN_Event::GenerateIC for the next step
      } //endif (JOIN && empty(affected_list)
   }         

   return true;
}

bool PTN_Event_Cond::GenerateST(SymbolTable *st) 
{
   ST_Event* current_event = (ST_Event*)st->GetCurrent();
   current_event->condType = this->conditionType;

   // Checks on quantities done by PTN_Event_Verb::GenerateST
   std::list<std::string>::iterator s_it;
   for (s_it = originating_entities_names.begin(); s_it != originating_entities_names.end(); ++s_it)
   {
      ST_Beta_Proc* beta_proc = dynamic_cast<ST_Beta_Proc*>(st->Find(*s_it, BPROC));
      if (beta_proc == NULL)
      {
         string message = "beta-process '" + *s_it + "' not defined";
         Error_Manager::PrintError(pos, 3, message);
         return false;
      }

      current_event->OriginatingProcesses.push_back(beta_proc);
   }
   return true;
}

PTN_Event_Cond::PTN_Event_Cond(const Pos& pos, CondType conditionType, PTNode* entity_list, EventCond* cond, const std::string& rateOrFun)
	: PTNode(0, pos, EVENT_COND)
{
	this->conditionType = conditionType;
	this->cond = cond;
	this->rateOrFun = rateOrFun;
	param1 = param2 = "0";
	parameters = NULL;

	if (entity_list != NULL)
	{
		((PTN_Event_EntityList*)entity_list)->BuildList(this->originating_entities_names);
		delete entity_list;
	}
}

PTN_Event_Cond::PTN_Event_Cond(const Pos& pos, CondType conditionType, PTNode* entity_list, EventCond* cond, const std::string& p_param1,
			   const std::string& p_param2)
	: PTNode(0, pos, EVENT_COND)
{
	this->conditionType = conditionType;
	this->cond = cond;
	this->rateOrFun = "0";
	this->param1 = p_param1;
	this->param2 = p_param2;
	parameters = NULL;

	if (entity_list != NULL)
	{
		((PTN_Event_EntityList*)entity_list)->BuildList(this->originating_entities_names);
		delete entity_list;
	}
}

PTN_Event_Cond::PTN_Event_Cond(const Pos& pos, CondType conditionType, PTNode* entity_list, EventCond* cond, PTNode *p_parameters)
	: PTNode(0, pos, EVENT_COND)
{
	this->conditionType =  conditionType;
	this->cond = cond;
	this->rateOrFun = "0";
	param1 = param2 = "0";
	parameters = (PTN_HypExp_Parameter *)p_parameters;

	if (entity_list != NULL)
	{
		((PTN_Event_EntityList*)entity_list)->BuildList(this->originating_entities_names);
		delete entity_list;
	}
}
