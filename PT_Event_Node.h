
#ifndef EVENT_NODE_H_INCLUDED
#define EVENT_NODE_H_INCLUDED


#include "PT_General_Node.h"
#include "Define.h"
#include "Symbol_Table.h"
#include <string>
#include "hash.h"
#include "Error_Manager.h"
#include "PT_GD_Node.h"

class PTN_Event : public PTNode
{
private:
   bool inheritable;

public:
   PTN_Event(const Pos& pos, PTNode* condition, PTNode* verb)
      : PTNode(2, pos, EVENT_EVENT)
   {
      Insert(condition, 0);
      Insert(verb, 1);
      this->inheritable = false;
   } 

   PTN_Event(const Pos& pos, PTNode* condition, PTNode* verb, bool inheritable)
      : PTNode(2, pos, EVENT_EVENT)
   {
      Insert(condition, 0);
      Insert(verb, 1);   
      this->inheritable = inheritable;
   } 

   virtual void PrintType(void);   
   virtual bool GenerateST(SymbolTable* st);

private:
   bool CheckRateOrFunction(const Pos& pos, SymbolTable *st, const std::string& rate_or_func);
};


class PTN_Event_Cond : public PTNode
{
private:
   EventCond* cond;

public:
   CondType conditionType;
   std::string rateOrFun;
   // parametri per gestire distribuzioni generali
   std::string param1;
   std::string param2;
   PTN_HypExp_Parameter *parameters;
   ///////////////////////////////////////////////
   std::list<std::string> originating_entities_names;

   PTN_Event_Cond(const Pos& pos, CondType conditionType, PTNode* entity_list, EventCond* cond, const std::string& rateOrFun);
   // Used for events with 2 parameters general distributions
   PTN_Event_Cond(const Pos& pos, CondType conditionType, PTNode* entity_list, EventCond* cond, const std::string& p_param1,
				  const std::string& p_param2);
   PTN_Event_Cond(const Pos& pos, CondType conditionType, PTNode* entity_list, EventCond* cond, PTNode *p_parameters);

   virtual bool GenerateST(SymbolTable *st);
   virtual void PrintType(void);

   EventCond* GetEventCond() { return cond; }
}; 

class PTN_Event_Verb : public PTNode
{
private:
   hash_map< ST_Beta_Proc*, std::list< std::pair<string, string> > > replaced_names;

protected:
   EventVerb* verb;

public:

   VerbType verbType;
   std::list< std::pair<string, int> > affected_list;

   //normally empty, used if we have a nameless join
   std::vector<ST_Beta_Proc*> join_processes;

   PTN_Event_Verb(const Pos& pos, const std::string& name1, const std::string& name2, VerbType verbType)
      : PTNode(0, pos, EVENT_VERB)
   {
      this->verbType = verbType;
      affected_list.push_back(pair<string, int>(name1, 1));
      affected_list.push_back(pair<string, int>(name2, 1));      

      // will be built during GenerateST
      verb = NULL;
   }

   PTN_Event_Verb(const Pos& pos, const std::string& name, int count, VerbType verbType)
      : PTNode(0, pos, EVENT_VERB)
   {
      this->verbType = verbType;
      affected_list.push_back(pair<string, int>(name, count));

      // will be built during GenerateST
      verb = NULL;
   }

   PTN_Event_Verb(const Pos& pos, int count, VerbType verbType)
      : PTNode(0, pos, EVENT_VERB)
   {
      this->verbType = verbType;
      affected_list.push_back(pair<string, int>(NO_PROCESS, count));

      // will be built during GenerateST
      verb = NULL;
   }

   PTN_Event_Verb(const Pos& pos, VerbType verbType)
      : PTNode(0, pos, EVENT_VERB)
   {
      this->verbType = verbType;

      // will be built during GenerateST
      verb = NULL;
   }

   PTN_Event_Verb(const Pos& pos, string name, VerbType verbType)
      : PTNode(0, pos, EVENT_VERB)
   {
      this->verbType = verbType;
      affected_list.push_back(pair<string, int>(name, 1));

      // will be built during GenerateST
      verb = NULL;
   }

   
   virtual void PrintType(void);
   virtual bool GenerateST(SymbolTable* st);
   virtual bool GenerateIC(PP_Node** parent, SymbolTable* st);

   std::list< std::pair<ST_Beta_Proc*, int> >& GetAffectedProcessesList();

   EventVerb* GetEventVerb() { assert(verb != NULL); return verb; }
};

//
// CONDITIONS
//
class PTN_EventCond_State : public PTNode_ListElem<EventState*>
{
private :
   EventState eventState;

public:
   PTN_EventCond_State(const Pos& pos, const std::string& id, const std::string& str_val, StateType stateType)
      : PTNode_ListElem<EventState*>(0, pos, EVENT_COND_STATE)
   {
      double value = atof(str_val.c_str());
      eventState.name = id;
      eventState.stateType = stateType;
      eventState.value = value;
   }

   bool GenerateST(SymbolTable* st)
   {
      //try an entity
      if (st->Find(eventState.name, BPROC) != NULL)
      {
         eventState.varType = EXPR_ENTITY;
         return true;
      }

      //try a statevar
      if (st->Find(eventState.name, STATE_VAR) != NULL)
      {
         eventState.varType = EXPR_STATEVAR;
         return true;
      }
      
      Error_Manager::PrintError(pos, 33, "undefined symbol: '" + eventState.name + "' is not a valid entity or statevar.");
      return false;      
   }


   void BuildList(std::set<EventState*>& elem_list)
   {
      elem_list.insert(&eventState);
   }
   void BuildList(std::list<EventState*>& elem_list)
   {
      elem_list.push_back(&eventState);
   }

   virtual void PrintType();
};

class PTN_EventCond_StateList : public PTNode_List<EventState*>
{
public:
   PTN_EventCond_StateList(const Pos& pos, PTNode* state, PTNode* otherStates)
      : PTNode_List<EventState*>(pos, state, otherStates, EVENT_COND_STATELIST) { }

   PTN_EventCond_StateList(const Pos& pos, PTNode* state)
      : PTNode_List<EventState*>(pos, state, EVENT_COND_STATELIST) { }

   virtual void PrintType();
};

class PTN_Event_Entity : public PTNode_ListElem<string>
{
public:
   string name;   

   PTN_Event_Entity(const Pos& pos, string id)
      : PTNode_ListElem<string>(0, pos, EVENT_ENTITY), name(id) { }

   void BuildList(std::set<string>& elem_list)
      {
      elem_list.insert(name);
      }
   void BuildList(std::list<string>& elem_list)
   {
      elem_list.push_back(name);
   }

   virtual void PrintType();

};

class PTN_Event_EntityList : public PTNode_List<string>
   { 
public:   
   PTN_Event_EntityList(const Pos& pos, PTNode* entity, PTNode* otherEntities)
      : PTNode_List<string>(pos, entity, otherEntities, EVENT_ENTITY_LIST) { }
  
   PTN_Event_EntityList(const Pos& pos, PTNode* entity)
      : PTNode_List<string>(pos, entity, EVENT_ENTITY_LIST) { }

   virtual void PrintType();
};


#endif //EVENT_NODE_H_INCLUDED

