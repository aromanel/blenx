#include <iostream>
#include "PT_GD_Node.h"

//
PTN_HypExp_Parameter::PTN_HypExp_Parameter(const Pos& pos, PTNode *node1, const string& p_weight, const string& p_param)
	:PTNode(1,pos,HYPEXP_PARAM)
{
	weight = p_weight;
	param = p_param;
    this->Insert(node1,0);
}

//
PTN_HypExp_Parameter::~PTN_HypExp_Parameter()
{}

//
void PTN_HypExp_Parameter::PrintType()
{}

//
double PTN_HypExp_Parameter::GetWeight()
{
	return atof(weight.c_str());
}

//
double PTN_HypExp_Parameter::GetParam()
{
	return atof(param.c_str());
}

//
std::vector< pair<double,double> > PTN_HypExp_Parameter::GetParameters()
{
    std::vector< pair<double,double> > parameters;
	pair<double,double> current_param;

	PTNode *current = this;
	while ( current != NULL ) 
	{
		current_param.first = ((PTN_HypExp_Parameter *)current)->GetWeight();
		current_param.second = ((PTN_HypExp_Parameter *)current)->GetParam();
		parameters.push_back(current_param);
		current = current->Get(0);
	}

	return parameters;
}

