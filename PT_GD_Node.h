#ifndef PT_GD_NODE_H
#define PT_GD_NODE_H

#include <cstdlib>
#include <string>
#include "PT_General_Node.h"

using namespace std;

//////////////////////////////////////////////////////
// PTN_HYPEXP_PARAMETER CLASS
//////////////////////////////////////////////////////

class PTN_HypExp_Parameter : public PTNode {
private:
	string weight;
	string param;
public:
	PTN_HypExp_Parameter(const Pos& pos, PTNode *node1, const string& p_weight, const string& p_param);
	~PTN_HypExp_Parameter();

	std::vector< pair<double,double> > GetParameters();
	double GetWeight();
	double GetParam();
	void PrintType();
};

#endif

