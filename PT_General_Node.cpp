
#include <iostream>

#include "PT_General_Node.h"
#include "Symbol_Table.h"
#include "Environment.h"

using namespace std;

//
bool Free_Name_Predicate_Sort(Free_Name left, Free_Name right) {
	return (left.name < right.name);
}

//
bool Free_Name_Predicate_Unique(Free_Name left, Free_Name right) {
	return (left.name == right.name && left.type == right.type);
}


//////////////////////////////////////////////////////
// PTNODE METHODS DEFINITION
//////////////////////////////////////////////////////

//
PTNode::PTNode()
   : pos(0, 0, "")
{
	num_sons = 0;
	sons = NULL;
	ST_ref = NULL;
}

//
PTNode::~PTNode() {
	for (int i=1;i<num_sons;i++) delete sons[i];
	delete []sons;
}

//
PTNode::PTNode(int p_num_sons, const Pos& ppos, ASTNodeType p_type)
: pos(ppos.Line, ppos.Col, ppos.Filename)
{
	num_sons = p_num_sons;
	type = p_type;
	ST_ref = NULL;
	//if ( (num_sons<0) ) exit(1);
	if ( num_sons==0 ) sons = NULL;
	else {
		sons = new PTNode *[num_sons];
		for (int i=0;i<num_sons;i++) sons[i] = NULL;
	}
}

//
ASTNodeType PTNode::GetType() {
	return type;
}

//
int PTNode::Insert(PTNode *son, int index){
	if ( (index<0) || (index>num_sons) ) return 1;
	if ( sons[index] != NULL ) delete sons[index];
	sons[index] = son;
	return 0;
}

//
PTNode *PTNode::Get(int index){
	if ( (index<0) || (index>=num_sons) ) return NULL;
	return sons[index];
}

//
void PTNode::Print(int space){
	for (int i=0; i<space; i++) cout << "          ";
	this->PrintType();
	cout << type;
	cout << " - " << pos.Line << endl;
	if (sons!=NULL)
		for (int i=0; i<num_sons;i++)
		   if (sons[i]!=NULL) sons[i]->Print(space+1);
}

//
void PTNode::FnList(list<Bound_Name> *bn, list<Free_Name> *fn, SymbolTable *st) {
	if (sons != NULL)
		for (int i=0; i<num_sons;i++)
			if (sons[i]!=NULL) sons[i]->FnList(bn,fn,st);
}

//
void PTNode::SetStRef(ST_Node *elem) 
{
	ST_ref = elem;
}

//
ST_Node *PTNode::GetStRef() {
	return ST_ref;
}

//
bool PTNode::Ref() {
	return ( ST_ref != NULL );
}

//
bool PTNode::GenerateST(SymbolTable *st) {
	bool res = true;
	if (sons != NULL)
		for (int i=0; i<num_sons;i++)
			if (sons[i]!=NULL) res = sons[i]->GenerateST(st) && res;
	return res;
}

//
bool PTNode::GenerateIC(PP_Node **parent, SymbolTable *st) 
{
   bool res = true;
	if (sons != NULL)
		for (int i=0; i<num_sons;i++)
			if (sons[i]!=NULL)
				res &= sons[i]->GenerateIC(parent,st);
   return res;
}

bool PTNode::TranslateRate(SymbolTable *st, const std::string& rate, double& v_rate)
{
   if (rate == INF_RATE) 
      v_rate = HUGE_VAL;

   else if (st->FindConstant(rate))
      v_rate = st->GetConstant(rate);

   else if (Utility::IsNumeric(rate))
      v_rate = (double)atof(rate.c_str());

   else
   {
      Error_Manager::PrintError(this->GetPos(), 35, "'" + rate +"' is not a valid rate constant, template name or number");
      return false;
   }

   return true;
}

bool PTNode::CheckRate(SymbolTable *st, const std::string& rate)
{
   if (!(rate == INF_RATE ||
         st->FindConstant(rate) ||
         Utility::IsNumeric(rate) ||
         st->Find(rate, RATE_TEMPLATE_REF) != NULL))
   {
      Error_Manager::PrintError(pos, 35, "'" + rate + "' is not a valid rate constant or value.");
      return false;
   }
   return true;
}

bool PTNode::CheckRate(SymbolTable *st, const string& rate, const string& rate_class)
{
   if (rate.empty())
   {
      if (st->GetEnv()->GetRateMap()->find("BASERATE") == st->GetEnv()->GetRateMap()->end() && 
         st->GetEnv()->GetRateMap()->find(rate_class) == st->GetEnv()->GetRateMap()->end())
      {
         Error_Manager::PrintError(pos, 35, " no base rate specified for " + rate_class);
         return false;
      }
      else
         return true;
   }
   else
      return CheckRate(st, rate);
}


//////////////////////////////////////////////////////
// PTN_PROGRAM METHODS DEFINITION
//////////////////////////////////////////////////////

//
PTN_Program::PTN_Program(const Pos& pos, PTNode *node2, PTNode *node3, PTNode *node4):PTNode(3,pos,PROGRAM) {
	this->Insert(node2,0);
	this->Insert(node3,1);
	this->Insert(node4,2);
}

PTN_Program::PTN_Program(const Pos& pos, PTNode *node2, PTNode *node3):PTNode(2,pos,PROGRAM) {
	this->Insert(node2,0);
	this->Insert(node3,1);
}

//
PTN_Program::~PTN_Program(){
}

//
void PTN_Program::PrintType() {
	cout << "PROGRAM";
}


//////////////////////////////////////////////////////
// PTN_RATE METHODS DEFINITION
//////////////////////////////////////////////////////

// 
PTN_Rate::PTN_Rate(const Pos& pos,  string p_name,  string p_rate):PTNode(0,pos,RATE) {
	name = p_name;
	rate = p_rate;
}

//
PTN_Rate::~PTN_Rate() {
}

//
bool PTN_Rate::GenerateST(SymbolTable *st) 
{
	string message;
	map<string, double>& rate_map = *(st->GetEnv()->GetRateMap());
	if (rate_map.find(name) != rate_map.end())
	{
		message = "rate of name '" + name + "' already defined.";
		Error_Manager::PrintError(pos, 2,message);
		return false;
	}

   double v_rate = 0;
   
   if (!TranslateRate(st, rate, v_rate))
      return false;
   
   rate_map[name] = v_rate;
	return true;
}

//
void PTN_Rate::PrintType() {
	cout << name << ":" << rate; 
}


//////////////////////////////////////////////////////
// PTN_PATE_PAIR METHODS DEFINITION
//////////////////////////////////////////////////////

// 
PTN_Rate_Pair::PTN_Rate_Pair(const Pos& pos, PTNode *node1, PTNode *node2):PTNode(2,pos,RATE_PAIR) {
	this->Insert(node1,0);
	this->Insert(node2,1);
}

//
PTN_Rate_Pair::~PTN_Rate_Pair() {
}

//
void PTN_Rate_Pair::PrintType() {
	cout << "RATE DEC";
}



//////////////////////////////////////////////////////
// PTN_DEC METHODS DEFINITION
//////////////////////////////////////////////////////

//
PTN_Dec::PTN_Dec(const Pos& pos, const string& p_id, EntityType p_type, PTNode *node)
   : PTNode(1,pos,DEC) 
{
	id = p_id;
	type = p_type;
	this->Insert(node,0);
}

//
PTN_Dec::~PTN_Dec() {
}

//
void PTN_Dec::PrintType() {
	cout << id << ":" << type;
}

//
bool PTN_Dec::GenerateST(SymbolTable *st) 
{
	ST_Node *elem = NULL;
	bool res = true;
	if (st->Is(id)) 
	{
		string message = "id '" + id + "' already defined.";
		Error_Manager::PrintError(pos,2,message);
		res = false;
	}
	else 
	{
		switch (type)
      {
      case PPROC:
		   {
			   elem = new ST_Pi_Proc(id,type,this);
			   list<Bound_Name> bn;
			   this->Get(0)->FnList(&bn,((ST_Pi_Proc *)elem)->GetFnList(),st);
			   ((ST_Pi_Proc *)elem)->GetFnList()->sort(Free_Name_Predicate_Sort);
			   ((ST_Pi_Proc *)elem)->GetFnList()->unique(Free_Name_Predicate_Unique);
			   res = Get(0)->GenerateST(st) && res;
			   st->STInsert(elem);
		   }
         break;

      case SEQUENCE:
		   {
			   elem = new ST_Pi_Sequence(id,type,this);
			   list<Bound_Name> bn;
			   this->Get(0)->FnList(&bn,((ST_Pi_Proc *)elem)->GetFnList(),st);
			   ((ST_Pi_Proc *)elem)->GetFnList()->sort(Free_Name_Predicate_Sort);
			   ((ST_Pi_Proc *)elem)->GetFnList()->unique(Free_Name_Predicate_Unique);
			   res = Get(0)->GenerateST(st) && res;
			   st->STInsert(elem);
		   }
         break;

      case BPROC:
		   {
			   elem = new ST_Beta_Proc(id,type,this);
			   st->STInsert(elem);
			   list<Bound_Name> bn;
			   this->Get(0)->FnList(&bn,((ST_Beta_Proc *)elem)->GetFnList(),st);
			   ((ST_Beta_Proc *)elem)->GetFnList()->sort(Free_Name_Predicate_Sort);
			   ((ST_Beta_Proc *)elem)->GetFnList()->unique(Free_Name_Predicate_Unique);
			   res = Get(0)->GenerateST(st) && res;
		   }
         break;
		
      case MOL:
		   {
			   elem = new ST_Mol_Def(id,type,this);
			   st->STInsert(elem);
			   /*
			   list<Bound_Name> bn;
			   this->Get(0)->FnList(&bn,((ST_Beta_Proc *)elem)->GetFnList(),st);
			   ((ST_Beta_Proc *)elem)->GetFnList()->sort();
			   ((ST_Beta_Proc *)elem)->GetFnList()->unique();
			   */
			   res = Get(0)->GenerateST(st) && res;
		   }
         break;
		default:
			assert(false);
      }

      assert(elem != NULL);
		SetStRef(elem);
	}
	return res;
}


//////////////////////////////////////////////////////
// PTN_DEC_PAIR METHODS DEFINITION
//////////////////////////////////////////////////////

//
PTN_Dec_Pair::PTN_Dec_Pair(const Pos& pos, PTNode *node1, PTNode *node2):PTNode(2,pos,DEC_PAIR) {
	this->Insert(node1,0);
	this->Insert(node2,1);
}

//
PTN_Dec_Pair::~PTN_Dec_Pair() {
}

//
void PTN_Dec_Pair::PrintType() {
	cout << "DEC";
}




