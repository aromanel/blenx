#ifndef PT_GENERAL_NODE_H
#define PT_GENERAL_NODE_H

#include <cstdlib>
#include <string>
#include "Error_Manager.h"
#include "Pi_Process.h"

using namespace std;

class ST_Node;
class SymbolTable;
class PP_Node;


bool Free_Name_Predicate_Sort(Free_Name left, Free_Name right);
bool Free_Name_Predicate_Unique(Free_Name left, Free_Name right);

//////////////////////////////////////////////////////
// PTNODE CLASS
//////////////////////////////////////////////////////

class PTNode {
 protected:
	int num_sons;		 // number of sons 
	ASTNodeType type;	 // type of the node
	PTNode **sons;		 // pointer to the sons list
	ST_Node *ST_ref;	 // reference to the symbol table
	Pos pos;           // position in the source file

public:

	PTNode();
	PTNode(int p_num_sons, const Pos& pos, ASTNodeType p_type);
	virtual ~PTNode();

	void Print(int space);
	void SetStRef(ST_Node *elem);

	int Insert(PTNode *son, int index);

	bool Ref();

	ASTNodeType GetType();
	const Pos& GetPos() { return pos; }

	PTNode *Get(int index);

	ST_Node *GetStRef();

	virtual void PrintType() = 0;
	virtual void FnList(list<Bound_Name> *bn, list<Free_Name> *fn, SymbolTable *st);
	virtual bool GenerateIC(PP_Node **parent, SymbolTable *st);
	virtual bool GenerateST(SymbolTable *st);

protected:
   bool TranslateRate(SymbolTable *st, const std::string& rate, double& v_rate);
   bool CheckRate(SymbolTable *st, const std::string& rate);
   bool CheckRate(SymbolTable *st, const std::string& rate, const string& rate_class);
};

// This AST node is part of a list
   // In BNF is something like list :: elem | elem list;
   // T is the type of the node that will be inserted in the List
   // for example:
   // NUMLIST :: NUM | NUM NUMLIST
   // the actual class is NUM: Node, but the resulting type in list
   // should be int, so NUM: NodeListElem<int>
template <class T>
class PTNode_ListElem : public PTNode
{
public:
   PTNode_ListElem(int num_sons, const Pos& pos, ASTNodeType node_type)
      : PTNode(num_sons, pos, node_type) { }
   virtual void BuildList(std::set<T>& elem_list) = 0;
   virtual void BuildList(std::list<T>& elem_list) = 0;
   virtual void BuildVector(std::vector<T>& elem_list) {}
};

template <class T>
class PTNode_List : public PTNode
{
public:
   typedef PTNode_ListElem<T> U;
   typedef PTNode_List<T> ME;

   PTNode_List(const Pos& pos, PTNode* elem, PTNode* elem_list, ASTNodeType node_type)
      : PTNode(2, pos, node_type) 
   { 
      Insert(elem, 0);
      Insert(elem_list, 1);
   }

   PTNode_List(const Pos& pos, PTNode* elem, ASTNodeType node_type)
      : PTNode(1, pos, node_type) 
   { 
      Insert(elem, 0);
   }   
   void BuildList(std::set<T>& elem_collection)
   {
      U* list_elem = (U*)Get(0);
      list_elem->BuildList(elem_collection);

      if (num_sons > 1)
      {
         ME* other_list = (ME*)Get(1);
         other_list->BuildList(elem_collection);
      }
   }

   void BuildList(std::list<T>& elem_collection)
   {
      U* list_elem = (U*)Get(0);
      list_elem->BuildList(elem_collection);

      if (num_sons > 1)
      {
         ME* other_list = (ME*)Get(1);
         other_list->BuildList(elem_collection);
      }
   }

   void BuildVector(std::vector<T>& elem_collection)
   {
      U* list_elem = (U*)Get(0);
      list_elem->BuildVector(elem_collection);

      if (num_sons > 1)
      {
         ME* other_list = (ME*)Get(1);
         other_list->BuildVector(elem_collection);
      }
   }
};


//////////////////////////////////////////////////////
// PTN_PROGRAM CLASS
//////////////////////////////////////////////////////

class PTN_Program : public PTNode {
public:
	PTN_Program(const Pos& pos, PTNode *node2, PTNode *node3, PTNode *node4);
	PTN_Program(const Pos& pos, PTNode *node2, PTNode *node3);
	~PTN_Program();

	void PrintType();
};



//////////////////////////////////////////////////////
// PTN_RATE CLASS
//////////////////////////////////////////////////////

class PTN_Rate : public PTNode {
private:
	 string name;
	 string rate;
public:
	PTN_Rate(const Pos& pos,  string p_name,  string p_rate);
	~PTN_Rate();

	bool GenerateST(SymbolTable *st);
	void PrintType();
};



//////////////////////////////////////////////////////
// PTN_RATE_PAIR CLASS
//////////////////////////////////////////////////////

class PTN_Rate_Pair : public PTNode {
public:
	PTN_Rate_Pair(const Pos& pos, PTNode *node1, PTNode *node2);
	~PTN_Rate_Pair();

	void PrintType();
};



//////////////////////////////////////////////////////
// PTN_DEC CLASS
//////////////////////////////////////////////////////

class PTN_Dec : public PTNode {
private:
	 string id;
	 EntityType type;
public:
	PTN_Dec(const Pos& pos, const string& p_id, EntityType p_type, PTNode *node);
	~PTN_Dec();

	bool GenerateST(SymbolTable *st);
	void PrintType();
};



//////////////////////////////////////////////////////
// PTN_DEC_PAIR CLASS
//////////////////////////////////////////////////////


class PTN_Dec_Pair : public PTNode {
public:
	PTN_Dec_Pair(const Pos& pos, PTNode *node1, PTNode *node2);
	~PTN_Dec_Pair();

	void PrintType();
};

#endif 

