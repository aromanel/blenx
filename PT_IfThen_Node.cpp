#include <iostream>
#include <string>
#include "PT_IfThen_Node.h"
#include "Symbol_Table.h"
#include "Environment.h"

using namespace std;


/////////

PTN_Atom_IfThen_Node::PTN_Atom_IfThen_Node(const Pos& pos, const string& p_bsubject, const string& p_btype, 
										   const BinderState& p_bstate)
	:PTNode(0,pos,ATOM_IFTHEN)
{
	bsubject = p_bsubject;
	btype = p_btype;
	bstate = p_bstate;
}

PTN_Atom_IfThen_Node::~PTN_Atom_IfThen_Node() {}

bool PTN_Atom_IfThen_Node::GenerateIC(PP_Node **parent, SymbolTable *st) 
{
	PP_Node *element = new PP_Atom(bsubject,btype,bstate);
	if (*parent != NULL) (*parent)->InsertChild(element);
	else *parent = element;
   return true;
}

void PTN_Atom_IfThen_Node::PrintType() {
	cout << "(" << btype << "," << bstate << ")" << endl;
}

bool PTN_Atom_IfThen_Node::GenerateST(SymbolTable *st) 
{ 
	string message;
   if (!st->FindType(this->btype) && btype != EMPTY_STRING) 
	{
		message = "type '" + btype + "' not defined";
		Error_Manager::PrintError(pos, 3,message);
		return false;
	}
	return true; 
}

//
void PTN_Atom_IfThen_Node::FnList(list<Bound_Name> *bn, list<Free_Name> *fn, SymbolTable *st) {
	std::list<Bound_Name>::iterator i;
	for(i=bn->begin();i!=bn->end();i++) {
		if ( this->bsubject == (*i).name && (*i).type == BN_INPUT ) {
			string message = "The subject of the change action is bounded by an input object. Deadlock subprocesses could be generated.";
			Error_Manager::PrintWarning(pos, 1, message);
			return;
		}
	}
	Free_Name fn_object(bsubject,FN_BINDER_SUB);
	fn->push_front(fn_object);
}


/////////

PTN_Not_IfThen_Node::PTN_Not_IfThen_Node(const Pos& pos, PTNode* node)
	:PTNode(1,pos,NOT_IFTHEN) 
{
	this->Insert(node,0);
}

PTN_Not_IfThen_Node::~PTN_Not_IfThen_Node() {}

bool PTN_Not_IfThen_Node::GenerateIC(PP_Node **parent, SymbolTable *st) 
{
	PP_Node *current = new PP_Expression(NOT);
	if (*parent != NULL) (*parent)->InsertChild(current);
	else *parent = current;

	if (!Get(0)->GenerateIC(&current,st))
      return false;
   return true;
}

void PTN_Not_IfThen_Node::PrintType() {
	cout << ToString(this->GetType());
}


/////////

PTN_And_IfThen_Node::PTN_And_IfThen_Node(const Pos& pos, PTNode* node1, PTNode* node2)
	:PTNode(2,pos,AND_IFTHEN) 
{
	this->Insert(node1,0);
	this->Insert(node2,1);
}

PTN_And_IfThen_Node::~PTN_And_IfThen_Node() {}

bool PTN_And_IfThen_Node::GenerateIC(PP_Node **parent, SymbolTable *st) 
{
	PP_Node *current = new PP_Expression(AND);
	if (*parent != NULL) (*parent)->InsertChild(current);
	else *parent = current;

   bool res = true;
	res &= Get(0)->GenerateIC(&current,st);
	res &= Get(1)->GenerateIC(&current,st);
   return res;
}

void PTN_And_IfThen_Node::PrintType() {
	cout << ToString(this->GetType());
}


/////////

PTN_Or_IfThen_Node::PTN_Or_IfThen_Node(const Pos& pos, PTNode* node1, PTNode* node2)
	:PTNode(2,pos,AND_IFTHEN) 
{
	this->Insert(node1,0);
	this->Insert(node2,1);
}

PTN_Or_IfThen_Node::~PTN_Or_IfThen_Node() {}

bool PTN_Or_IfThen_Node::GenerateIC(PP_Node **parent, SymbolTable *st) 
{
	PP_Node *current = new PP_Expression(OR);
	if (*parent != NULL) (*parent)->InsertChild(current);
	else *parent = current;

	bool res = true;
   res &= Get(0)->GenerateIC(&current,st);
	res &= Get(1)->GenerateIC(&current,st);
   return res;
}

void PTN_Or_IfThen_Node::PrintType() {
	cout << ToString(this->GetType());
}


///////////

PTN_IfThen_Node::PTN_IfThen_Node(const Pos& pos, PTNode* node1, PTNode* node2)
	:PTNode(2,pos,IFTHEN)
{
	this->Insert(node1,0);
	this->Insert(node2,1);	
}

PTN_IfThen_Node::~PTN_IfThen_Node() {}

bool PTN_IfThen_Node::GenerateIC(PP_Node **parent, SymbolTable *st) 
{
	PP_Node *current = new PP_IfThen();
	if (*parent != NULL) (*parent)->InsertChild(current);
	else *parent = current;

	bool res = true;
   res &= Get(0)->GenerateIC(&current,st);
	res &= Get(1)->GenerateIC(&current,st);
   return res;
}

void PTN_IfThen_Node::PrintType() {
	cout << ToString(this->GetType());
}
