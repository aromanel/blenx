#ifndef PT_IFTHEN_NODE_H_INCLUDED
#define PT_IFTHEN_NODE_H_INCLUDED


#include "PT_General_Node.h"
#include "Define.h"
#include <string>

using namespace std;

//////////////////////////////////////////////////////
// PTN_ATOM_IFTHEN_NODE CLASS
//////////////////////////////////////////////////////

class PTN_Atom_IfThen_Node : public PTNode {
private:
	string bsubject;
	string btype;
	BinderState bstate;
public:
	PTN_Atom_IfThen_Node(const Pos& pos, const string& p_bsubject, const string& p_btype, const BinderState& p_bstate);
	~PTN_Atom_IfThen_Node();

	void FnList(list<Bound_Name> *bn, list<Free_Name> *fn, SymbolTable *st);
	bool GenerateIC(PP_Node **parent, SymbolTable *st);
	void PrintType();

	bool GenerateST(SymbolTable *st);
};


//////////////////////////////////////////////////////
// PTN_NOT_IFTHEN_NODE CLASS
//////////////////////////////////////////////////////

class PTN_Not_IfThen_Node : public PTNode {
public:
	PTN_Not_IfThen_Node(const Pos& pos, PTNode* node);
	~PTN_Not_IfThen_Node();

	bool GenerateIC(PP_Node **parent, SymbolTable *st);
	void PrintType();
};


//////////////////////////////////////////////////////
// PTN_AND_IFTHEN_NODE CLASS
//////////////////////////////////////////////////////

class PTN_And_IfThen_Node : public PTNode {
public:
	PTN_And_IfThen_Node(const Pos& pos, PTNode* node1, PTNode* node2);
	~PTN_And_IfThen_Node();

	bool GenerateIC(PP_Node **parent, SymbolTable *st);
	void PrintType();
};


//////////////////////////////////////////////////////
// PTN_OR_IFTHEN_NODE CLASS
//////////////////////////////////////////////////////

class PTN_Or_IfThen_Node : public PTNode {
public:
	PTN_Or_IfThen_Node(const Pos& pos, PTNode* node1, PTNode* node2);
	~PTN_Or_IfThen_Node();

	bool GenerateIC(PP_Node **parent, SymbolTable *st);
	void PrintType();
};

//////////////////////////////////////////////////////
// PTN_IFTHEN_NODE CLASS
//////////////////////////////////////////////////////

class PTN_IfThen_Node : public PTNode {
public:
	PTN_IfThen_Node(const Pos& pos, PTNode* node1, PTNode* node2);
	~PTN_IfThen_Node();

	bool GenerateIC(PP_Node **parent, SymbolTable *st);
	void PrintType();
};

#endif
