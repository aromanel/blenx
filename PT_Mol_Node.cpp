

#include "PT_Mol_Node.h"
#include "Symbol_Table.h"
//#include <hash_map>
//using stdext::hash_map;

PTN_Molecule::PTN_Molecule(const Pos& pos, PTNode* signature, PTNode* molNodeList)
: PTNode(2, pos, MOLECULE)
{
   Insert(signature, 0);
   Insert(molNodeList, 1);
}

//
void PTN_Molecule::PrintType() 
{
   cout << "Molecule";
}

void PTN_MolEdge_List::PrintType() 
{
   cout << "list(MolEdge)";
}

void PTN_MolEdge::PrintType() 
{
   cout << "MolEdge";
}

void PTN_MolNode_List::PrintType() 
{
   cout << "list(MolNode)";
}

void PTN_MolNode::PrintType() 
{
   cout << "MolNode";
}

void PTN_MolBinder::PrintType() 
{
   cout << "MolBinder";
}

void PTN_MolBinder_List::PrintType() 
{
   cout << "list(MolBinder)";
}

bool PTN_Molecule::GenerateST(SymbolTable* st)
{
   bool res = true;

   ST_Mol_Def* current_mol = dynamic_cast<ST_Mol_Def*>(st->GetCurrent());
   if (current_mol == NULL) 
      return false;   

   PTN_MolEdge_List* signature = dynamic_cast<PTN_MolEdge_List*>(Get(0));
   PTN_MolNode_List* molNodeList = dynamic_cast<PTN_MolNode_List*>(Get(1));   

   if (signature == NULL || molNodeList == NULL)
      return false;

   // first make definition of nodes
   res &= molNodeList->GenerateST(st);

   // then of edges
   res &= signature->GenerateST(st);


   /*
   if (res == true)
   {
   signature->BuildList(MolSignature);
   molNodeList->BuildList(NodeList);

   // find types for nodes that have them
   hash_map<std::string, PTN_MolNode*> defindedNodes;

   // first pass, store defined
   list<PTN_MolNode*>::iterator it;
   for (it = NodeList.begin(); it != NodeList.end(); ++it)
   {
   PTN_MolNode* molNode = *it;
   if (molNode->boxTypeName.size() > 0)
   {
   if (st->Find(molNode->boxTypeName, BPROC) == NULL)
   {
   string message = "beta-process '" + molNode->boxTypeName + "' not defined";
   Error_Manager::PrintError(line, 3 ,message);
   res = false;
   }
   defindedNodes[molNode->subName] = molNode;
   }
   }

   // second pass, set stored
   for (it = NodeList.begin(); it != NodeList.end(); ++it)
   {
   PTN_MolNode* molNode = *it;
   if (molNode->boxTypeName.size() > 0)
   {
   molNode->boxTypeName = defindedNodes[molNode->refType]->boxTypeName;
   molNode->binders = defindedNodes[molNode->refType]->binders;                       
   }
   }  
   }
   */

   return res;
}


bool PTN_MolEdge::GenerateST(SymbolTable* st)
{
   // checks of existence
   ST_Mol_Def* current_mol = dynamic_cast<ST_Mol_Def*>(st->GetCurrent());
   if (current_mol == NULL) 
   {
      return false;
   }


   if (!current_mol->FindNode(SignatureElem.node1)) 
   {
      string message = "molecular-node '" + SignatureElem.node1 + "' not defined";
      Error_Manager::PrintError(pos, 3 ,message);
      return false;
   }

   //controlla che i nomi dei binder esistano 
   ST_Beta_Proc* node1 = current_mol->GetNode(SignatureElem.node1);
   if (node1->FindBinderByName(SignatureElem.binder1) == NULL)
   {
      string message = "binder '" + SignatureElem.binder1 +  "' is not defined in entity '" + SignatureElem.node1 + "'";
      Error_Manager::PrintError(pos, 3 ,message);
      return false;
   }

   if (!current_mol->FindNode(SignatureElem.node2)) 
   {
      string message = "molecular-node '" + SignatureElem.node2 + "' not defined";
      Error_Manager::PrintError(pos, 3 ,message);
      return false;
   }

   ST_Beta_Proc* node2 = current_mol->GetNode(SignatureElem.node2);
   if (node2->FindBinderByName(SignatureElem.binder2) == NULL)
   {
      string message = "binder '" + SignatureElem.binder2 +  "' is not defined in entity '" + SignatureElem.node2 + "'";
      Error_Manager::PrintError(pos, 3 ,message);
      return false;
   }

   // add edge
   current_mol->AddEdge(SignatureElem.node1, SignatureElem.binder1, 
      SignatureElem.node2 , SignatureElem.binder2);
   return true;
}

bool PTN_MolNode::GenerateST(SymbolTable* st)
{
   bool res = true;

   ST_Mol_Def* current_mol = dynamic_cast<ST_Mol_Def*>(st->GetCurrent());
   if (current_mol != NULL) 
   { 
      if (num_sons > 0)
      {
         ST_Beta_Proc* bproc = dynamic_cast<ST_Beta_Proc*>(st->Find(boxTypeName, BPROC));
         if (bproc == NULL)
         {
            Error_Manager::PrintError(pos, 3, "box '" + boxTypeName + "' not defined");
            res = false;
         }
         else
         {
            // we have a directly defined node
            this->binders = new set<string>();
            PTN_MolBinder_List* binderList = dynamic_cast<PTN_MolBinder_List*>(Get(0));
            binderList->BuildList(*binders);  
            res &= binderList->GenerateST(st);

            // find if the binder list is ok
            set<string>::iterator it;
            for (it = binders->begin(); it != binders->end(); ++it)
            {
               BBinders* bbinders = bproc->FindBinderByName(*it);
               if (bbinders == NULL)
               {                          
                  string message = "box '" + bproc->GetIdentifier() + "' do not have a binder '"
                     + *it + "'";
                  Error_Manager::PrintError(pos, 3 ,message);
                  return false;
               }
            }  

            res = current_mol->AddNode(subName, bproc, binders);
            if (res == false)
               Error_Manager::PrintError(pos, 3, "node \'" + subName + "\' already defined");
         }
      }
      else 
      {
         // we have a node defined by reference
         res = current_mol->AddNode(subName, refType);         
      }
   }
   else
   {
      res = false;
   }

   return res;
}
