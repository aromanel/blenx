

#ifndef MOL_NODE_H_INCLUDED
#define MOL_NODE_H_INCLUDED

#include "PT_General_Node.h"
#include "Define.h"
#include <string>
#include <list>
#include <set>
using std::string;
using std::list;

#include "hash.h"

struct MolSignatureElem
{
   string node1;
   string binder1;
   string node2;
   string binder2;
};


class PTN_MolNode : public PTNode_ListElem<PTN_MolNode*>
{
public:
   string boxTypeName;
   string subName;   
   string refType;

   std::set<std::string>* binders;

   PTN_MolNode(const Pos& pos, const string& name, const string& baseName, PTNode* binderList)
      : PTNode_ListElem<PTN_MolNode*>(1, pos, MOL_NODE)
   {
      subName = name;
      boxTypeName = baseName;
      Insert(binderList, 0);  
      binders = NULL;
   }

   PTN_MolNode(const Pos& pos, const string& name, const string& eqName)
      : PTNode_ListElem<PTN_MolNode*>(0, pos, MOL_NODE)
   {
      subName = name;
      refType = eqName;
   }

   virtual ~PTN_MolNode()
   {
      if (binders != NULL)      
         delete binders;
   }

   virtual void BuildList(std::set<PTN_MolNode*>& elem_list)
   {
      elem_list.insert(this);
   }
   virtual void BuildList(std::list<PTN_MolNode*>& elem_list)
   {
      elem_list.push_back(this);
   }

   virtual bool GenerateST(SymbolTable* st);
   virtual void PrintType();
   
};

class PTN_Molecule : public PTNode
{
public:
   list<MolSignatureElem*> MolSignature;
   list<PTN_MolNode*> NodeList;

public:
   PTN_Molecule(const Pos& pos, PTNode* signature, PTNode* molNodeList);

   virtual bool GenerateST(SymbolTable* st);
   virtual void PrintType();
};



class PTN_MolEdge_List : public  PTNode_List<MolSignatureElem*>
{
public:
   PTN_MolEdge_List(const Pos& pos, PTNode* edge)
      : PTNode_List<MolSignatureElem*>(pos, edge, MOL_EDGE_LIST) { }
     

   PTN_MolEdge_List(const Pos& pos, PTNode* edge, PTNode* edgeList)
      : PTNode_List<MolSignatureElem*>(pos, edge, edgeList, MOL_EDGE_LIST) { } 

   virtual void PrintType();
};

class PTN_MolEdge : public PTNode_ListElem<MolSignatureElem*>
{
public:
   MolSignatureElem SignatureElem;

   PTN_MolEdge(const Pos& pos, const string& entity1, const string& binder1, 
      const string& entity2, const string& binder2)
      : PTNode_ListElem<MolSignatureElem*>(0, pos, MOL_EDGE)
   {
      SignatureElem.node1 = entity1;
      SignatureElem.node2 = entity2;
      SignatureElem.binder1 = binder1;
      SignatureElem.binder2 = binder2;
   }

   virtual bool GenerateST(SymbolTable* st);   
   virtual void PrintType();
   virtual void BuildList(std::set<MolSignatureElem*>& elem_list)
   {
      elem_list.insert(&SignatureElem);
   }
   virtual void BuildList(std::list<MolSignatureElem*>& elem_list)
   {
      elem_list.push_back(&SignatureElem);
   }
};

class PTN_MolNode_List : public PTNode_List<PTN_MolNode*>
{
public:
   PTN_MolNode_List(const Pos& pos, PTNode* node)
      : PTNode_List<PTN_MolNode*>(pos, node, MOL_NODE_LIST) { }

   PTN_MolNode_List(const Pos& pos, PTNode* node, PTNode* nodeList)
      : PTNode_List<PTN_MolNode*>(pos, node, nodeList, MOL_NODE_LIST) { }

   virtual void PrintType();
};


class PTN_MolBinder : public PTNode_ListElem<string>
{
public:
   string name;   

   PTN_MolBinder(const Pos& pos, string binderName)
      : PTNode_ListElem<string>(0, pos, MOL_BINDER), name(binderName) { }

   void BuildList(std::set<string>& elem_list)
   {
      elem_list.insert(name);
   }
   void BuildList(std::list<string>& elem_list)
   {
      elem_list.push_back(name);
   }

   virtual void PrintType();

};

class PTN_MolBinder_List : public PTNode_List<string>
{
public:
   
   PTN_MolBinder_List(const Pos& pos, PTNode* binder, PTNode* otherBinders)
      : PTNode_List<string>(pos, binder, otherBinders, MOL_BINDER_LIST) { }
  
   PTN_MolBinder_List(const Pos& pos, PTNode* binder)
      : PTNode_List<string>(pos, binder, MOL_BINDER_LIST) { }

   virtual void PrintType();
};



#endif //MOL_NODE_H_INCLUDED

