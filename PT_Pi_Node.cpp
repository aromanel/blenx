
#include <iostream>
#include <string>
#include "PT_Pi_Node.h"
#include "Symbol_Table.h"

using namespace std;

//////////////////////////////////////////////////////
// PTN_PI_NIL METHODS DEFINITION
//////////////////////////////////////////////////////

//
PTN_Pi_Nil::PTN_Pi_Nil(const Pos& pos):PTNode(0,pos,NIL) {
}

//
PTN_Pi_Nil::~PTN_Pi_Nil() {
}

//
bool PTN_Pi_Nil::GenerateIC(PP_Node **parent, SymbolTable *st) {
	if (*parent != NULL) (*parent)->InsertChild(new PP_Nil());
	else *parent = new PP_Nil();
   return true;
}

//
void PTN_Pi_Nil::PrintType() {
	cout << "NIL" ;
}


//////////////////////////////////////////////////////
// PTN_PI_ID METHODS DEFINITION
//////////////////////////////////////////////////////

//
PTN_Pi_Id::PTN_Pi_Id(const Pos& pos, string  p_id):PTNode(0,pos,ID) {
	id = p_id;
}

//
PTN_Pi_Id::~PTN_Pi_Id() {
}

//
bool PTN_Pi_Id::GenerateST(SymbolTable *st) 
{
	ST_Node *current;
   current = st->Find(id, PPROC);
	if (current != NULL)
      return true;

   // Try template
   current = st->Find(id, PPROC_TEMPLATE_REF);
	if (current != NULL)
      return true;

   // Try a template sequence
   current = st->Find(id, SEQ_TEMPLATE_REF);
   if (current != NULL)
      return true;
   
   // If both failed, emit error
	string message = "pi-process '" + id + "' not defined";
   Error_Manager::PrintError(pos, 3, message);
   return false;
}

//
bool PTN_Pi_Id::GenerateIC(PP_Node **parent, SymbolTable *st) 
{
	ST_Pi_Proc *st_element = (ST_Pi_Proc *)(st->Find(id,PPROC));
	if (st_element != NULL) 
   {
		PP_Node* ppnode = st_element->GetIC();
      if (ppnode == NULL)
      {
         if (!st_element->GenerateIC(st))
            return false;
         ppnode = st_element->GetIC();
      }
      PP_Node* element = ppnode->GetCopy();

		if (*parent != NULL) 
         (**parent).InsertChild(element);
		else 
         *parent = element;

      return true;
   }
   else
   {
      //a template param?
      //if so, the st SymbolTable will have a TempAmbient pushed
      //so we can find ST_Template_Name's on it
      ST_Template_Name* st_element = (ST_Template_Name*)(st->Find(id, PPROC_TEMPLATE_REF));
      if (st_element != NULL)
      {
         // at template declaration, we don't know the pproc
         // insert dummy node that will be substituted by PTN_PiInv_Temp::GenerateIC()
         // the dummy node will hold the position
         PP_Node *element = new PP_Template_Name(id, PPROC_TEMPLATE_REF);
         if (*parent != NULL) 
            (*parent)->InsertChild(element);
         else 
            *parent = element;

         return true;
      }
      else
      {
         //or maybe a sequence?
         st_element = (ST_Template_Name*)(st->Find(id, SEQ_TEMPLATE_REF));
         if (st_element != NULL)
         {
            // at template declaration, we don't know the pproc
            // insert dummy node that will be substituted by PTN_PiInv_Temp::GenerateIC()
            // the dummy node will hold the position
            PP_Node *element = new PP_Template_Name(id, SEQ_TEMPLATE_REF);
            if (*parent != NULL) 
               (*parent)->InsertChild(element);
            else 
               *parent = element;
            
            return true;
         }
      }
   }
   return false;
}

//
void PTN_Pi_Id::FnList(list<Bound_Name> *bn, list<Free_Name> *fn, SymbolTable *st) { 
	list<Free_Name> *l = st->GetFnList(id);
	list<Bound_Name>::iterator j;
	list<Free_Name>::iterator i;
	Free_Name *free_name;
	bool is;
	if (l!=NULL) {
		for(i=l->begin();i!=l->end();i++) {
			is = false;
			for(j=bn->begin();j!=bn->end();j++)
				if ((*j).name == (*i).name) {
					is = true;
					if ((*j).type == BN_INPUT && (*i).type == FN_BINDER_SUB) {
						string message = "With the " + this->id +
							"pi-process concatenation change/hide/unhide/if subject result bounded by an input object. Deadlock subprocesses could be generated.";
						Error_Manager::PrintWarning(pos, 1, message);
						return;
					}
				}
			if (!is) {
				free_name = new Free_Name(*i);
				fn->push_front(*free_name);
			}
		}
	}
}

//
void PTN_Pi_Id::PrintType() {
	cout << id;
}


//////////////////////////////////////////////////////
// PTN_PI_ACTION METHODS DEFINITION
//////////////////////////////////////////////////////

//
PTN_Pi_Action::PTN_Pi_Action(const Pos& pos, PTNode *node1, PTNode *node2):PTNode(2,pos,ACTION) {
	this->Insert(node1,0);
	this->Insert(node2,1);
}

//
PTN_Pi_Action::~PTN_Pi_Action() {
}

//
bool PTN_Pi_Action::GenerateIC(PP_Node **parent, SymbolTable *st) 
{
	PP_Node *current = new PP_Seq();
	if (*parent != NULL) (*parent)->InsertChild(current);
	else *parent = current;

	if (!this->Get(0)->GenerateIC(&current,st))
		return false;

   assert(current->ChildrenCount() > 0);

	if (this->num_sons > 1)
		if (!this->Get(1)->GenerateIC(&current,st))
         return false;

   assert(current->ChildrenCount() > 0);
   return true;
}


void PTN_Pi_Action::FnList(list<Bound_Name> *bn, list<Free_Name> *fn, SymbolTable *st) {
	// copio la lista in una nuova lista
	list<Bound_Name> bn_tmp;
	list<Bound_Name>::iterator i;
	for(i=bn->begin();i!=bn->end();i++) {
		bn_tmp.push_front(*i);
	}
	this->Get(0)->FnList(&bn_tmp,fn,st);
	if (this->num_sons > 1)
		this->Get(1)->FnList(&bn_tmp,fn,st);
}

//
void PTN_Pi_Action::PrintType() {
	cout << "Action";
}


//////////////////////////////////////////////////////
// PTN_Action_List METHODS DEFINITION
//////////////////////////////////////////////////////

//
bool PTN_Action_List::GenerateIC(PP_Node **parent, SymbolTable *st) 
{
	PP_Node *current = new PP_Seq();
	if (*parent != NULL) (*parent)->InsertChild(current);
	else *parent = current;

	this->Get(0)->GenerateIC(&current,st);
   assert(current->ChildrenCount() > 0);

	if (this->num_sons > 1)
		this->Get(1)->GenerateIC(&current,st);
   assert(current->ChildrenCount() > 0);

   return true;
}

//same as ptn_pi_action
void PTN_Action_List::FnList(list<Bound_Name> *bn, list<Free_Name> *fn, SymbolTable *st) 
{
	// copio la lista in una nuova lista
	list<Bound_Name> bn_tmp;
	list<Bound_Name>::iterator i;
	for(i=bn->begin();i!=bn->end();i++) {
		bn_tmp.push_front(*i);
	}
	this->Get(0)->FnList(&bn_tmp,fn,st);
	if (this->num_sons > 1)
		this->Get(1)->FnList(&bn_tmp,fn,st);
}

void PTN_Action_List::PrintType()
{
   cout << "Sequence";
}


//////////////////////////////////////////////////////
// PTN_PI_REPLICATION METHODS DEFINITION
//////////////////////////////////////////////////////

//
PTN_Pi_Replication::PTN_Pi_Replication(const Pos& pos, PTNode *node1, PTNode *node2):PTNode(2,pos,REPLICATION) {
	this->Insert(node1,0);
	this->Insert(node2,1);
}

//
PTN_Pi_Replication::~PTN_Pi_Replication() {
}

//
bool PTN_Pi_Replication::GenerateIC(PP_Node **parent, SymbolTable *st) {
	PP_Node *current = new PP_Replication();
	if (*parent != NULL) (*parent)->InsertChild(current);
	else *parent = current;
	this->Get(0)->GenerateIC(&current,st);
	this->Get(1)->GenerateIC(&current,st);
   assert(current->ChildrenCount() > 0);

   return true;
}

//
void PTN_Pi_Replication::PrintType() {
	cout << "Replication";
}



//////////////////////////////////////////////////////
// PTN_PI_CHOICE METHODS DEFINITION
//////////////////////////////////////////////////////

//
PTN_Pi_Choice::PTN_Pi_Choice(const Pos& pos, PTNode *node1, PTNode *node2):PTNode(2,pos,CHOICE) {
	this->Insert(node1,0);
	this->Insert(node2,1);
}

//
PTN_Pi_Choice::~PTN_Pi_Choice() {
}

//
bool PTN_Pi_Choice::GenerateIC(PP_Node **parent, SymbolTable *st) 
{
	PP_Node *current;
	if (*parent != NULL)
		if ((*parent)->GetCategory() == PP_CHOICE) 
			current = *parent;
		else (*parent)->InsertChild(current = new PP_Choice());
	else current = *parent = new PP_Choice();
	
   bool res = true;
   res &= Get(0)->GenerateIC(&current,st);
	res &= Get(1)->GenerateIC(&current,st);
   return res;
}

//
void PTN_Pi_Choice::PrintType() {
	cout << "CHOICE";
}


//////////////////////////////////////////////////////
// PTN_PI_PARALLEL METHODS DEFINITION
//////////////////////////////////////////////////////

//
PTN_Pi_Parallel::PTN_Pi_Parallel(const Pos& pos, PTNode *node1, PTNode *node2):PTNode(2,pos,PARALLEL) {
	this->Insert(node1,0);
	this->Insert(node2,1);
}

//
PTN_Pi_Parallel::~PTN_Pi_Parallel() {
}

//
bool PTN_Pi_Parallel::GenerateIC(PP_Node **parent, SymbolTable *st) {
	PP_Node *current;
	if (*parent != NULL)
		if ((*parent)->GetCategory() == PP_PARALLEL) 
			current = *parent;
		else (*parent)->InsertChild(current = new PP_Parallel());
	else current = *parent = new PP_Parallel();
	
   bool res = true;
   res &= Get(0)->GenerateIC(&current,st);
	res &= Get(1)->GenerateIC(&current,st);
   return res;
}

//
void PTN_Pi_Parallel::PrintType() {
	cout << "PARALLEL";
}

