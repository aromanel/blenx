#ifndef PT_PI_NODE_H
#define PT_PI_NODE_H

#include <cstdlib>
#include <string>
#include "PT_General_Node.h"

using namespace std;


//////////////////////////////////////////////////////
// PTN_PI_NIL CLASS
//////////////////////////////////////////////////////

class PTN_Pi_Nil : public PTNode {
public:
	PTN_Pi_Nil(const Pos& pos);
	~PTN_Pi_Nil();

	bool GenerateIC(PP_Node **parent, SymbolTable *st);
	void PrintType();
};


//////////////////////////////////////////////////////
// PTN_PI_ID CLASS
//////////////////////////////////////////////////////

class PTN_Pi_Id : public PTNode {
private:
	string   id;
public:
	PTN_Pi_Id(const Pos& pos, string  p_id);
	~PTN_Pi_Id();

	void FnList(list<Bound_Name> *bn, list<Free_Name> *fn, SymbolTable *st);
	bool GenerateIC(PP_Node **parent, SymbolTable *st);
	void PrintType();

	bool GenerateST(SymbolTable *st);
};



//////////////////////////////////////////////////////
// PTN_PI_ACTION CLASS
//////////////////////////////////////////////////////

class PTN_Pi_Action : public PTNode {
public:
	PTN_Pi_Action(const Pos& pos, PTNode *node1, PTNode *node2);
	~PTN_Pi_Action();

	bool GenerateIC(PP_Node **parent, SymbolTable *st);
	void FnList(list<Bound_Name> *bn, list<Free_Name> *fn, SymbolTable *st);
	void PrintType();
};



//////////////////////////////////////////////////////
// PTN_PI_REPLICATION CLASS
//////////////////////////////////////////////////////

class PTN_Pi_Replication : public PTNode {
public:
	PTN_Pi_Replication(const Pos& pos, PTNode *node1, PTNode *node2);
	~PTN_Pi_Replication();

	bool GenerateIC(PP_Node **parent, SymbolTable *st);
	void PrintType();
};



//////////////////////////////////////////////////////
// PTN_PI_CHOICE CLASS
//////////////////////////////////////////////////////

class PTN_Pi_Choice : public PTNode {
public:
	PTN_Pi_Choice(const Pos& pos, PTNode *node1, PTNode *node2);
	~PTN_Pi_Choice();

	bool GenerateIC(PP_Node **parent, SymbolTable *st);
	void PrintType();
};



//////////////////////////////////////////////////////
// PTN_PI_PARALLEL CLASS
//////////////////////////////////////////////////////

class PTN_Pi_Parallel : public PTNode {
public:
	PTN_Pi_Parallel(const Pos& pos, PTNode *node1, PTNode *node2);
	~PTN_Pi_Parallel();

	bool GenerateIC(PP_Node **parent, SymbolTable *st);
	void PrintType();
};


class PTN_Action_List : public PTNode
{
public:
   PTN_Action_List(const Pos& pos, PTNode* node)
      : PTNode(1,pos,ACTION_LIST) 
   {
	   this->Insert(node,0);
   }
      
   PTN_Action_List(const Pos& pos, PTNode* node, PTNode* nodeList)
        : PTNode(2,pos,ACTION_LIST) 
   {
	   this->Insert(node,0);
      this->Insert(nodeList,1);
   }

   bool GenerateIC(PP_Node **parent, SymbolTable *st);
   void PrintType();
   void FnList(list<Bound_Name> *bn, list<Free_Name> *fn, SymbolTable *st);
};


#endif

