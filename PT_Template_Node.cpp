#include <iostream>
#include "PT_Template_Node.h"
#include "Symbol_Table.h"

using namespace std;

bool RecursiveReplace(const Pos& pos, PP_Node** ppnode, const std::vector<PTN_Inv_Temp_Elem*>& inv_list, ST_Temp_Ambient* temp_symbols, SymbolTable* st);
bool ReplaceBinders(std::list<BBinders*>* binder_list, const std::vector<PTN_Inv_Temp_Elem*>& inv_list, ST_Temp_Ambient* temp_symbols, SymbolTable* st);
void AdjustSeq(PP_Node* node);
void AdjustOneSeq(PP_Node* node);
PP_Node* FindLastSeq(PP_Node* start_node);
bool CheckTemplateArguments(PTNode* node, PTNode* ref, 
                            const std::string& id, 
                            const std::vector<PTN_Inv_Temp_Elem*>& InvList, 
                            ST_Temp_Ambient* TemplateAmbient,
                            SymbolTable* st);


//
void PTN_Dec_Temp_Elem::PrintType() {
	cout << "(" << ToString(param_type) << " " << id << ")";
}

//
void PTN_Dec_Temp_List::PrintType() {
	cout << ToString(this->GetType());
}

//
void PTN_Inv_Temp_Elem::PrintType() {
	cout << "(" << id << "," << ToString(state) << ")";
}

string PTN_Inv_Temp_Elem::GetLabel() 
{
   string label =  id + ":" + ToString(state);
   if (this->num_sons > 0)
   {
      label = label + "<<" + ((PTN_PiInv_Template*)Get(0))->GetLabel() + ">>";
   }
   return label;
}

//
void PTN_Inv_Temp_List::PrintType() {
	cout << ToString(this->GetType());
}


PTN_Template::PTN_Template(const Pos& pos, EntityType p_temp_type, const string& p_id, PTNode* node0, PTNode* node1)
: PTNode(2, pos, TEMPLATE)
{
    id = p_id;
	temp_type = p_temp_type;
	Insert(node0, 0);
	Insert(node1, 1);
}

//TODO: test
// This function insert the template in the Symboltable.
// It also performs some checks and walk in this subtree to 
// find and track "template param references"
bool PTN_Template::GenerateST(SymbolTable *st) 
{
	bool res = true;
	if (st->Is(id)) 
	{
		string message = "id '" + id + "' already defined.";
		Error_Manager::PrintError(pos, 2, message);
		res = false;
	}
	else 
	{
		if (temp_type == PPROC) 
		{
			ST_Pproc_Template* elem = new ST_Pproc_Template(id, PPROC_TEMPLATE, this);

         // build an ambient with "dummy params"; these will receive the pointers of the 
         // PTNodes referring to them during tree traversal
         // Normally, the PP tree is constructed in PTN_Pi*::GenerateIC;
         // Here, the PP tree is constructed in PTN_PiInv*::GenerateIC;
         // These classes only have GenerateST; GenerateIC is empty

         // do the same as in PTN_Dec, BUT with the "dummy params"
         PTN_Dec_Temp_List* dec_list = (PTN_Dec_Temp_List*)(this->Get(0));
         dec_list->BuildList(DecList);

         // Generate and save in this class a "TemplateAmbient"
         // from DecList
         std::set<PTN_Dec_Temp_Elem*>::iterator it;
         for (it = DecList.begin(); it != DecList.end(); ++it)
         {            
            ST_Template_Name* template_elem = new ST_Template_Name((**it).GetId(), (**it).GetType(), this);
            elem->GetTemplateAmbient()->Add(template_elem);
         }

         st->PushTempAmbient(elem->GetTemplateAmbient());

         //the second child node is "piprocess"
         //TODO: divide also names in Template_Name?
         list<Bound_Name> bn;
			this->Get(1)->FnList(&bn,elem->GetFnList(),st);
			elem->GetFnList()->sort(Free_Name_Predicate_Sort);
			elem->GetFnList()->unique(Free_Name_Predicate_Unique);
   
			st->STInsert(elem);
         res = Get(1)->GenerateST(st) && res;
         
         st->PopTempAmbient();
         SetStRef(elem);
		}
		else if (temp_type == BPROC) 
		{
			ST_Bproc_Template* elem = new ST_Bproc_Template(id, this);

         // build an ambient with "dummy params"; these will receive the pointers of the 
         // PTNodes referring to them during tree traversal
         // Normally, the PP tree is constructed in PTN_Pi*::GenerateIC;
         // Here, the PP tree is constructed in PTN_PiInv*::GenerateIC;
         // These classes only have GenerateST; GenerateIC is empty

         // do the same as in PTN_Dec, BUT with the "dummy params"
         PTN_Dec_Temp_List* dec_list = (PTN_Dec_Temp_List*)(this->Get(0));
         dec_list->BuildList(DecList);

         // Generate and save in this class a "TemplateAmbient"
         // from DecList
         std::set<PTN_Dec_Temp_Elem*>::iterator it;
         for (it = DecList.begin(); it != DecList.end(); ++it)
         {            
            ST_Template_Name* template_elem = new ST_Template_Name((**it).GetId(), (**it).GetType(), this);
            elem->GetTemplateAmbient()->Add(template_elem);
         }

         st->PushTempAmbient(elem->GetTemplateAmbient());
			
         list<Bound_Name> bn;
			this->Get(1)->FnList(&bn,elem->GetFnList(),st);
			elem->GetFnList()->sort(Free_Name_Predicate_Sort);
			elem->GetFnList()->unique(Free_Name_Predicate_Unique);
			
         st->STInsert(elem);
         res = Get(1)->GenerateST(st) && res;
         
         st->PopTempAmbient();
         SetStRef(elem);
      }
	}
	return res;
}

//
void PTN_Template::PrintType() {
	cout << ToString(this->GetType()) << ToString(temp_type);
}

//
PTN_PiInv_Template::PTN_PiInv_Template(const Pos& pos, const string& p_id, PTNode* node) 
: PTNode(1, pos, INV_TEMPLATE)
{
	id = p_id;
	Insert(node, 0);
}

//
void PTN_PiInv_Template::PrintType() {
	cout << ToString(this->GetType());
}

//
string PTN_PiInv_Template::GetLabel() 
{
   string label = id;
   std::set<PTN_Inv_Temp_Elem*> elem_list;
   ((PTN_Inv_Temp_List*)Get(0))->BuildList(elem_list);

   std::set<PTN_Inv_Temp_Elem*>::iterator it;
   for (it = elem_list.begin(); it != elem_list.end(); ++it)
   {
      label = label + ":" + (**it).GetLabel();
   }
   return label;
}


bool PTN_PiInv_Template::GenerateST(SymbolTable* st)
{
   if (st->Find(id, PPROC_TEMPLATE) == NULL)
   {

      Error_Manager::PrintError(4, "cannot instatiate template param with '" +
             id + "': undefined symbol");
      return false;
   }

   return true;
}


bool PTN_PiInv_Template::GenerateIC(PP_Node **parent, SymbolTable *st)
{
   ST_Pproc_Template* st_element = (ST_Pproc_Template *)(st->Find(id, PPROC_TEMPLATE));
	if (st_element != NULL) 
   {
      //Get a copy of the tree
      PP_Node* ppnode = st_element->GetIC();
      if (ppnode == NULL)
      {
         st_element->GenerateIC(st);
         ppnode = st_element->GetIC();
      }
      PP_Node* element = ppnode->GetCopy();
      
      //Central point: substitute template params with actual params
      PTN_Inv_Temp_List* inv_list = (PTN_Inv_Temp_List*)this->Get(0);
      vector<PTN_Inv_Temp_Elem*> InvList;
      inv_list->BuildVector(InvList);

      //SymbolTable element must provides the TemplateAmbient (DecList)
      //elem will provide Free_Name, Bound_Name... Template_Name?
      
      //replace names for PP_Input(subject,object), PP_Output(subject,object), etc.
      //replace PP_TemplateName with the right st_element->GetIC()->GetCopy();
      
      if (!CheckTemplateArguments(this, 
            st_element->GetRef(),
            st_element->GetIdentifier(), 
            InvList, 
            st_element->GetTemplateAmbient(),
            st))
      {
         *parent = NULL;
         return false;
      }
      
      // Recursively replace names.
      // Will print error messages by itself.
      if (!RecursiveReplace(this->GetPos(), &element, InvList, st_element->GetTemplateAmbient(), st))
         return false;
      AdjustSeq(element);


	   if (*parent != NULL) 
         (*parent)->InsertChild(element);
	   else 
         *parent = element;
      
	}
   else
   {
      Error_Manager::PrintError(4, "cannot instatiate template param with '" +
             id + "': undefined symbol");
      *parent = NULL;
      return false;
   }
   return true;
}

void AdjustSeq(PP_Node* node)
{
   switch (node->GetCategory())
   {
   case PP_CHOICE:
   case PP_PARALLEL:      
      AdjustSeq(node->GetChildren(0));
      AdjustSeq(node->GetChildren(1));
      break;

   case PP_REPLICATION:
      AdjustSeq(node->GetChildren(1));
      break;

   case PP_SEQ:
      AdjustOneSeq(node);
      break;
	default:
		break;
   }
}

void AdjustOneSeq(PP_Node* node)
{
   // if I have a structure like 
   // seq --> seq------------> AA
   //     |-> other(nil)  |--> seq ... 
   //                                 --> seq --> BB

   // re- adjust it!
   // seq --> AA
   //     |-> seq ... 
   //             --> seq --> BB
   //                     |-> other (nil)

   assert(node->GetCategory() == PP_SEQ);
   std::vector<PP_Node*> seq_queue;
   PP_Node* current_node = node;
   bool done = false;

   //Phase 1: find all anomalous cases, adjust structure and prune them
   while (!done)
   {
      if (current_node->ChildrenCount() == 2)
      {
         PP_Node* child0 = current_node->GetChildren(0);
         PP_Node* child1 = current_node->GetChildren(1);
         if (child0->GetCategory() == PP_SEQ)
         {
            seq_queue.push_back(child1);

            PP_Node* new_child0 = child0->GetChildren(0)->GetCopy();
            PP_Node* new_child1 = child0->GetChildren(1)->GetCopy();

            delete child0;

            current_node->SetChildren(0, new_child0);
            current_node->SetChildren(1, new_child1);
            if (new_child1->GetCategory() == PP_SEQ)
            {
               current_node = new_child1;
            }
            else if (new_child0->GetCategory() == PP_SEQ)
            {
               //keep current node
               done = false;
            }
            else
            {
               done = true;
            }
         }
         else
         {
            if (child1->GetCategory() == PP_SEQ)
               current_node = child1;
            else
               done = true;
         }
      }
      else
      {
         //only one child, break
         done = true;
      }
   }

   //Phase 2: append pruned nodes to the end
   PP_Node* last_seq = current_node;

   while (seq_queue.size() > 0)
   {
      //find last seq
      last_seq = FindLastSeq(last_seq);
      assert(last_seq != NULL);

      if (last_seq->ChildrenCount() == 1)
      {
         last_seq->InsertChild(seq_queue.back());
         seq_queue.pop_back();
         last_seq = last_seq->GetChildren(1);
      }
      else
      {
         PP_Seq* new_seq = new PP_Seq();   
         PP_Node* child0 = last_seq->GetChildren(0);
         PP_Node* child1 = last_seq->GetChildren(1);
         new_seq->InsertChild(child1);
         new_seq->InsertChild(seq_queue.back());
         seq_queue.pop_back();        

         last_seq->SetChildren(0, new_seq);
         last_seq->SetChildren(1, child0);

         last_seq = new_seq;
      }
   }
}

//PRE: assumes a tree in the "standard" form, i.e. 
// seq --> AA
//     |-> seq ... 
//             --> seq --> BB
//                     |-> other (nil)
PP_Node* FindLastSeq(PP_Node* start_node)
{
   PP_Node* last_node = NULL;
   if (start_node->GetCategory() == PP_SEQ)
      last_node = start_node;

   PP_Node* current_node = start_node;

   while (current_node->ChildrenCount() > 1)
   {
      PP_Node* child0 = current_node->GetChildren(0);
      PP_Node* child1 = current_node->GetChildren(1);
      
      //check PRE
      assert(child0->GetCategory() == PP_SEQ);

      if (child1->GetCategory() == PP_SEQ)
         last_node = child1;
   }
   return last_node;
}

//We know this is invoked only by RecursiveReplace.
//We assume the formal param TempType = TEMP_PPROC (EntityType = PPROC_TEMPLATE_REF)
//NEW: or, it can be SEQ_TEMPLATE_REF!
//And the actual param pproc IC or pproc ID1<<ID2>> (Instantiated pproc template)
//Case 1) not handled!
bool PTN_Inv_Temp_Elem::GenerateIC(PP_Node **parent, SymbolTable *st)
{
   // Four cases: 
   // 1) simple ID - (name, type): do nothing
   // 2) simple ID - (sequence): find sequence in SymbolTable and get a copy of its IC
   // 3) simple ID - (pproc): find pproc in SymbolTable and get a copy of its IC
   // 4) ID1<<ID2>> (pproc) Find on SymbolTable will fail.. call PTN_PiInv_Template::GenerateIC
   // Plus error case: for 2-3) no symbol in SymbolTable, for 4) defer checks to PTN_PiInv_Template

   //we assume case 2-3) or 4)   

   //Case 2-3) : no PTN sons
   if (this->num_sons == 0)
   {
      // try case 3) (pproc)
      ST_Node* st_node = st->Find(id, PPROC);      
      if (st_node != NULL)
      {
         ST_Pi_Proc* st_elem = (ST_Pi_Proc*)st_node;

         PP_Node* ppnode = st_elem->GetIC();
         if (ppnode == NULL)
         {
            if (!st_elem->GenerateIC(st))
               return false;

            ppnode = st_elem->GetIC();
         }
         PP_Node* element = ppnode->GetCopy();

         if (*parent != NULL) (*parent)->InsertChild(element);
	      else *parent = element;
      }
      else
      {
         // try case 2) (sequence)
         st_node = st->Find(id, SEQUENCE);      
         if (st_node != NULL)
         {
            ST_Pi_Sequence* st_elem = (ST_Pi_Sequence*)st_node;

            PP_Node* ppnode = st_elem->GetIC();
            if (ppnode == NULL)
            {
               if (!st_elem->GenerateIC(st))
                  return false;

               ppnode = st_elem->GetIC();
            }
            PP_Node* element = ppnode->GetCopy();

            if (*parent != NULL) 
               (*parent)->InsertChild(element);
	         else 
               *parent = element;
         }
         else
         {
            Error_Manager::PrintError(4, "cannot instatiate template param with '" +
             id + "': undefined symbol");
            return false;
         }
      }
   }
   else // case 4)
   {
      //con ID1<<ID2>>, la seconda... (ovvero, prima sostituzione e generazione di quella
      //interna, e poi il risultato sostituito dentro
                              
      // it is a PTN_PiInv_Template
      PTN_PiInv_Template* inner_template = (PTN_PiInv_Template*)(this->Get(0));
      if (!inner_template->GenerateIC(parent, st))
         return false;
      //The inner call will end in a PPROC IC copy
      //if (*parent != NULL) (*parent)->InsertChild(inner_template);
	   //else *parent = element;
   }
   return true;
}


bool ReplaceBinders(std::list<BBinders*>* binder_list, const std::vector<PTN_Inv_Temp_Elem*>& inv_list, ST_Temp_Ambient* temp_symbols, SymbolTable* st)
{
   int pos;
   std::list<BBinders*>::iterator it;

   for (it = binder_list->begin(); it != binder_list->end(); ++it)
   {
      // First try to match subject
      const string& sbj_place_holder = (**it).GetSubject();
      pos = temp_symbols->FindPosition(sbj_place_holder, NAME_TEMPLATE_REF);
      if (pos >= 0)
      {
         (**it).SetSubject(inv_list[pos]->Get_Id());
         if (inv_list[pos]->Get_Type() == STATE_NOT_SPECIFIED)
            (**it).SetState(STATE_UNHIDDEN);
         else
            (**it).SetState(inv_list[pos]->Get_Type());
      }

      // Then try to match type
      const string& type_place_holder = (**it).GetType();
      pos = temp_symbols->FindPosition(type_place_holder, TYPE_TEMPLATE_REF);
      if (pos >= 0)
      {
         (**it).SetType(inv_list[pos]->Get_Id());
		 if ( inv_list[pos]->Get_Type() == STATE_NOT_SPECIFIED )
			(**it).SetState(STATE_UNHIDDEN);
		 else
			(**it).SetState(inv_list[pos]->Get_Type());
      }
   }
   return true;
}

template<typename T>
inline bool TrySetRate(const Pos& pos, T* named_node, const std::vector<PTN_Inv_Temp_Elem*>& inv_list, ST_Temp_Ambient* temp_symbols, SymbolTable* st)
{
   int elem_pos = temp_symbols->FindPosition(named_node->GetRate(), RATE_TEMPLATE_REF);
   if (elem_pos >= 0)
   {
      const string& rate_const = inv_list[elem_pos]->Get_Id();

      if (Utility::IsNumeric(rate_const))
      {
         named_node->SetRate(rate_const);
      }
      else if (st->FindConstant(rate_const))
      {
         named_node->SetRate(st->GetConstantString(rate_const));
      }
      else
      {
         // try if we also are inside a template
         ST_Node* node = st->Find(rate_const, RATE_TEMPLATE_REF);
         if (node)
            named_node->SetRate(node->GetIdentifier());
         else
         {
            Error_Manager::PrintError(pos, 33, " undefined rate constant '" + rate_const + 
               "' at template instantiation");
            return false;
         }
      }               
   }
   return true;
}

bool RecursiveReplace(const Pos& pos, PP_Node** ppnode, const std::vector<PTN_Inv_Temp_Elem*>& inv_list, ST_Temp_Ambient* temp_symbols, SymbolTable* st)
{
   bool res = true;
   int elem_pos = -1;

   switch ((**ppnode).GetCategory())
   {
   case PP_TEMPLATE_NAME:
      {
         // replace with actual PP_*;
         PP_Template_Name* node = (PP_Template_Name*)*ppnode;
         elem_pos = temp_symbols->FindPosition(node->GetId(), node->GetEntityType());
         if (elem_pos >= 0)
         {
            PP_Node* instatiated_node = NULL;

            if (!inv_list[elem_pos]->GenerateIC(&instatiated_node, st))
            {
               Error_Manager::PrintError(pos, 33, " cannot generate IC for template.");
               return false;
            }

            if (instatiated_node == NULL)
            {
               Error_Manager::PrintError(pos, 33, " cannot generate IC for template.");
               return false;
            }
            
            //set my parent to the real one (not NULL!)
            node->PropagateParent(instatiated_node);

            //delete the old PP_Template_Name
            delete *ppnode;
            *ppnode = instatiated_node;
         }
      }
      break;

   // For other cases, simply replace names
   case PP_CHANGE:
      {
         PP_Change* named_node = (PP_Change*)*ppnode;

         elem_pos = temp_symbols->FindPosition(named_node->GetSubject(), NAME_TEMPLATE_REF);
         if (elem_pos >= 0)
         {
            named_node->SetSubject(inv_list[elem_pos]->Get_Id());
         }

         elem_pos = temp_symbols->FindPosition(named_node->GetType(), TYPE_TEMPLATE_REF);
         if (elem_pos >= 0)
         {
            named_node->SetType(inv_list[elem_pos]->Get_Id());
         }

         if (!TrySetRate(pos, named_node, inv_list, temp_symbols, st))
            return false;
      }
      break;

   case PP_INPUT:
      {
         PP_Input* named_node = (PP_Input*)*ppnode;

         elem_pos = temp_symbols->FindPosition(named_node->GetSubject(), NAME_TEMPLATE_REF);
         if (elem_pos >= 0)
         {
            named_node->SetSubject(inv_list[elem_pos]->Get_Id());
         }

         elem_pos = temp_symbols->FindPosition(named_node->GetObject(), NAME_TEMPLATE_REF);
         if (elem_pos >= 0)
         {
            named_node->SetObject(inv_list[elem_pos]->Get_Id());
         }     
      }
      break;

   case PP_OUTPUT:
      {
         PP_Output* named_node = (PP_Output*)*ppnode;

         elem_pos = temp_symbols->FindPosition(named_node->GetSubject(), NAME_TEMPLATE_REF);
         if (elem_pos >= 0)
         {
            named_node->SetSubject(inv_list[elem_pos]->Get_Id());
         }

         elem_pos = temp_symbols->FindPosition(named_node->GetObject(), NAME_TEMPLATE_REF);
         if (elem_pos >= 0)
         {
            named_node->SetObject(inv_list[elem_pos]->Get_Id());
         }     
      }
      break;

   case PP_EXPOSE:
      {
         PP_Expose* named_node = (PP_Expose*)*ppnode;

         elem_pos = temp_symbols->FindPosition(named_node->GetObject(), NAME_TEMPLATE_REF);
         if (elem_pos >= 0)
         {
            named_node->SetObject(inv_list[elem_pos]->Get_Id());
         }

         elem_pos = temp_symbols->FindPosition(named_node->GetType(), TYPE_TEMPLATE_REF);
         if (elem_pos >= 0)
         {
            named_node->SetType(inv_list[elem_pos]->Get_Id());
         }

         if (!TrySetRate(pos, named_node, inv_list, temp_symbols, st))
            return false;

         elem_pos = temp_symbols->FindPosition(named_node->GetRate(), RATE_TEMPLATE_REF);
         if (elem_pos >= 0)
         {
            const string& rate_const = inv_list[elem_pos]->Get_Id();
            if (st->FindConstant(rate_const))
               named_node->SetRate(st->GetConstantString(rate_const));
            else
            {
               // try if we also are inside a template
               ST_Node* node = st->Find(rate_const, RATE_TEMPLATE_REF);
               if (node)
                  named_node->SetRate(node->GetIdentifier());
               else
                  assert(false); //TODO: error? or at template instantiation?
            }               
         }
      }
      break;

   case PP_HIDE:
      {
         PP_Hide* named_node = (PP_Hide*)*ppnode;

         elem_pos = temp_symbols->FindPosition(named_node->GetObject(), NAME_TEMPLATE_REF);
         if (elem_pos >= 0)
         {
            named_node->SetObject(inv_list[elem_pos]->Get_Id());
         }

         if (!TrySetRate(pos, named_node, inv_list, temp_symbols, st))
            return false;
      }
      break;

   case PP_UNHIDE:
      {
         PP_Unhide* named_node = (PP_Unhide*)*ppnode;

         elem_pos = temp_symbols->FindPosition(named_node->GetObject(), NAME_TEMPLATE_REF);
         if (elem_pos >= 0)
         {
            named_node->SetObject(inv_list[elem_pos]->Get_Id());
         }

         if (!TrySetRate(pos, named_node, inv_list, temp_symbols, st))
            return false;
      }
      break;

   case PP_ATOM:
      {
         PP_Atom* named_node = (PP_Atom*)*ppnode;

         elem_pos = temp_symbols->FindPosition(named_node->GetSubject(), NAME_TEMPLATE_REF);
         if (elem_pos >= 0)
         {
            named_node->SetSubject(inv_list[elem_pos]->Get_Id());
         }

         elem_pos = temp_symbols->FindPosition(named_node->GetType(), TYPE_TEMPLATE_REF);
         if (elem_pos >= 0)
         {
            named_node->SetType(inv_list[elem_pos]->Get_Id());
         }
      }
      break;

   case PP_TAU:
      {
         PP_Tau* named_node = (PP_Tau*)*ppnode;
         if (!TrySetRate(pos, named_node, inv_list, temp_symbols, st))
            return false;
      }
      break;

   case PP_DIE:
      {
         PP_Die* named_node = (PP_Die*)*ppnode;
         if (!TrySetRate(pos, named_node, inv_list, temp_symbols, st))
            return false;
      }
      break;

	default:
		break;
   }

   std::list<PP_Node*>::iterator it;
   for (it = (**ppnode).GetChildren()->begin(); it != (**ppnode).GetChildren()->end(); ++it)
   {
      PP_Node* child_node = *it;
      res &= RecursiveReplace(pos, &child_node, inv_list, temp_symbols, st);
      *it = child_node;
   }
   return res;
}

bool CheckTemplateArguments(PTNode* node, PTNode* ref, 
                            const std::string& id, 
                            const std::vector<PTN_Inv_Temp_Elem*>& InvList, 
                            ST_Temp_Ambient* TemplateAmbient,
                            SymbolTable* st)
{
   //check list size
   if (InvList.size() != TemplateAmbient->Size())
   {

      Error_Manager::PrintError(node->GetPos(), 5, "template '" + id + "' has " + 
         Utility::i2s(TemplateAmbient->Size()) + " elements, but it was instantiated with " + 
         Utility::i2s(InvList.size()));
      Error_Manager::PrintError(ref->GetPos(), 5, "(related location)");
      return false;
   }

   //check matching types
   std::vector<PTN_Inv_Temp_Elem*>::const_iterator inv_it;
   std::vector<ST_Node*>::const_iterator par_it = TemplateAmbient->GetIterator();

   int i = 1;
   bool types_ok = true;
   
   for (inv_it = InvList.begin(); inv_it != InvList.end(); ++inv_it, ++par_it, ++i)
   {
      switch ((*par_it)->GetCategory())
      {
      case PPROC_TEMPLATE_REF:
         if (!st->Find((**inv_it).Get_Id(), PPROC) &&
             !st->Find((**inv_it).Get_Id(), PPROC_TEMPLATE))
         {
            Error_Manager::PrintError(node->GetPos(), 5, "template argument no." + 
               Utility::i2s(i) + " ('" + (**inv_it).Get_Id() + "') is of mismatching type. Expected " +
               ToString( (*par_it)->GetCategory()));
            Error_Manager::PrintError(ref->GetPos(), 5, "(related location)");
            types_ok = false;
         }
         break;

      case SEQ_TEMPLATE_REF:
         if (!st->Find((**inv_it).Get_Id(), SEQUENCE))
         {
            Error_Manager::PrintError(node->GetPos(), 5, "template argument no." + 
               Utility::i2s(i) + " ('" + (**inv_it).Get_Id() + "') is of mismatching type. Expected " +
               ToString( (*par_it)->GetCategory()));
            Error_Manager::PrintError(ref->GetPos(), 5, "(related location)");
            types_ok = false;
         }
         break;

      case TYPE_TEMPLATE_REF: 
         if (!st->FindType((**inv_it).Get_Id()))
         {
            Error_Manager::PrintError(node->GetPos(), 5, "template argument no." + 
               Utility::i2s(i) + " ('" + (**inv_it).Get_Id() + "') is of mismatching type. Expected " +
               ToString( (*par_it)->GetCategory()));
            Error_Manager::PrintError(ref->GetPos(), 5, "(related location)");
            types_ok = false;
         }
         break;

      case RATE_TEMPLATE_REF:
         {
            const std::string& rate_const = (**inv_it).Get_Id();
            if (!(Utility::IsNumeric(rate_const) ||
               st->FindConstant(rate_const) ||
               st->Find(rate_const, RATE_TEMPLATE_REF)))
            {        
               Error_Manager::PrintError(node->GetPos(), 5, "template argument no." + 
                  Utility::i2s(i) + " ('" + (**inv_it).Get_Id() + "') is of mismatching type. Expected " +
                  ToString( (*par_it)->GetCategory()));
               Error_Manager::PrintError(ref->GetPos(), 5, "(related location)");
               types_ok = false;
            }
         }
         break;

      case NAME_TEMPLATE_REF:
         if (Utility::IsNumeric((**inv_it).Get_Id()))              
         {
            Error_Manager::PrintError(node->GetPos(), 5, "template argument no." + 
               Utility::i2s(i) + " ('" + (**inv_it).Get_Id() + "') is of mismatching type. Expected " +
               ToString( (*par_it)->GetCategory()));
            Error_Manager::PrintError(ref->GetPos(), 5, "(related location)");
            types_ok = false;
         }
         break;

      default:
         assert(false);
      }

   }
   return types_ok;
}

//
PTN_BpInv_Template::PTN_BpInv_Template(const Pos& pos, const string& p_templateId, const string& p_number, PTNode* node) 
: PTNode(1, pos, INV_TEMPLATE)
{
	templateId = p_templateId;
   bprocId = NO_PROCESS;
   number = p_number;
	Insert(node, 0);
}

//
PTN_BpInv_Template::PTN_BpInv_Template(const Pos& pos, const string& p_templateId, PTNode* node, const string& p_bprocId) 
: PTNode(1, pos, INV_TEMPLATE)
{
	templateId = p_templateId;
	bprocId = p_bprocId;
	Insert(node, 0);
}

//
void PTN_BpInv_Template::PrintType() {
	cout << ToString(this->GetType());
}

string PTN_BpInv_Template::BuildTemplateName(const vector<PTN_Inv_Temp_Elem*>& InvList)
{
   string template_name = templateId + "<<";
   
   bool first = true;
   vector<PTN_Inv_Temp_Elem*>::const_iterator it;
   for (it = InvList.begin(); it != InvList.end(); ++it)
   {
      if (first == true)
      {
         template_name = template_name + (**it).GetLabel();
         first = false;
      }
      else
      {
         template_name = template_name + ":" + (**it).GetLabel();
      }    
   }   

   template_name = template_name + ">>";
   return template_name;
}


//
bool PTN_BpInv_Template::GenerateST(SymbolTable *st) 
{
   // cond := betaId == NO_PROCESS: 
   // 1) Find the template bproc 
   // 2) (cond) create a new ST_Beta_Proc   
   // 3) Create a copy of it (Binders and IC) to put in the beta proc
   // 4) RecursiveReplace and ReplaceBinders
   // 5) (cond) Put the newly created BPROC in SymbolTable, and call AddCounter

   ST_Beta_Proc* current_beta = NULL;
   bool inlineInstantiation = (bprocId == NO_PROCESS);

   //1)
   ST_Bproc_Template* temp_symbol = (ST_Bproc_Template*)st->Find(templateId, BPROC_TEMPLATE);

   if (temp_symbol != NULL)
   {
      //PUSH
      st->PushTempAmbient(temp_symbol->GetTemplateAmbient());

      // Build the vector of template instantiation elements
      PTN_Inv_Temp_List* inv_list = (PTN_Inv_Temp_List*)this->Get(0);
      vector<PTN_Inv_Temp_Elem*> InvList;
      inv_list->BuildVector(InvList);

      //2)
      if (inlineInstantiation)
      {
         string template_name = BuildTemplateName(InvList);

         current_beta = (ST_Beta_Proc *)st->Find(template_name, BPROC);
         if (current_beta == NULL) 
         {
            current_beta = new ST_Beta_Proc(template_name, BPROC, temp_symbol->GetRef());
         }         
      }
      else
      {
         current_beta = (ST_Beta_Proc *)st->Find(bprocId, BPROC);
      }

      assert(current_beta != NULL);

   
      //3)
      std::list<BBinders*>* binder_list = temp_symbol->GetBinderList();
      std::list<BBinders*>::iterator bit;
      for (bit = binder_list->begin(); bit != binder_list->end(); ++bit)
      {
         BBinders* elem = new BBinders(**bit);
         current_beta->InsertBinder(elem);
      }

      //At this point there is no IC!
      PP_Node* ppnode = temp_symbol->GetIC();
      if (ppnode == NULL)
      {
         temp_symbol->GenerateIC(st);
         ppnode = temp_symbol->GetIC();
      }
      PP_Node* new_node = ppnode->GetCopy();


      if (!CheckTemplateArguments(this, 
            temp_symbol->GetRef(),
            temp_symbol->GetIdentifier(), 
            InvList, 
            temp_symbol->GetTemplateAmbient(),
            st))
         return false;

      //4)
      if (!RecursiveReplace(this->GetPos(), &new_node, InvList, temp_symbol->GetTemplateAmbient(), st))
         return false;
      AdjustSeq(new_node);

      current_beta->SetIC(new_node);
      ReplaceBinders(current_beta->GetBinderList(), InvList, temp_symbol->GetTemplateAmbient(), st);

      //5)
      if (inlineInstantiation)
      {
         st->STInsert(current_beta);
         current_beta->AddCounter(atoi(number.c_str()));
      }

      //POP
      st->PopTempAmbient();
      return true;
   }

   string message = "beta-process template '" + templateId + "' not defined";
   Error_Manager::PrintError(pos, 3,message);
   return false;
}
