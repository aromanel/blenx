#ifndef PT_TEMPLATE_NODE_H
#define PT_TEMPLATE_NODE_H

#include <cstdlib>
#include <string>
#include "PT_General_Node.h"
#include "PT_Mol_Node.h"
#include "Define.h"

using namespace std;


////////////////////////////////////////////////
// CLASS PTN_DEC_TEMP_ELEM DEFINITION
////////////////////////////////////////////////

class PTN_Dec_Temp_Elem : public PTNode_ListElem<PTN_Dec_Temp_Elem*>
{
private:
   TempType param_type;
   string id;
public:
   PTN_Dec_Temp_Elem(const Pos& pos, TempType p_param_type, const string& p_id)
      : PTNode_ListElem<PTN_Dec_Temp_Elem*>(0, pos, DEC_TEMP_ELEM)
   {
      param_type = p_param_type;
      id = p_id;
   }

   virtual ~PTN_Dec_Temp_Elem() { }

   void BuildList(std::set<PTN_Dec_Temp_Elem*>& elem_list)
   {
      elem_list.insert(this);
   }
   void BuildList(std::list<PTN_Dec_Temp_Elem*>& elem_list)
   {
      elem_list.push_back(this);
   }
   void BuildVector(std::vector<PTN_Dec_Temp_Elem*>& elem_list)
   {
      elem_list.push_back(this);
   }

   TempType GetType() { return param_type; }
   string GetId() { return id; }

   virtual bool GenerateST(SymbolTable* st) {return true; }
   virtual void PrintType();

};


////////////////////////////////////////////////
// CLASS PTN_DEC_TEMP_LIST DEFINITION
////////////////////////////////////////////////

class PTN_Dec_Temp_List : public PTNode_List<PTN_Dec_Temp_Elem*>
{
public:
   PTN_Dec_Temp_List(const Pos& pos, PTNode* node)
      : PTNode_List<PTN_Dec_Temp_Elem*>(pos, node, DEC_TEMP_LIST) { }

   PTN_Dec_Temp_List(const Pos& pos, PTNode* node, PTNode* nodeList)
      : PTNode_List<PTN_Dec_Temp_Elem*>(pos, node, nodeList, DEC_TEMP_LIST) { }

   virtual bool GenerateST(SymbolTable* st) { return true; }
   virtual void PrintType();
};



////////////////////////////////////////////////
// CLASS PTN_TEMPLATE DEFINITION
////////////////////////////////////////////////

class PTN_Template : public PTNode
{
private:
   EntityType temp_type;
   string id;
public:
   std::set<PTN_Dec_Temp_Elem*> DecList;

   PTN_Template(const Pos& pos, EntityType p_temp_type, const string& p_id, PTNode* node0, PTNode* node1);

   virtual bool GenerateST(SymbolTable* st);
   virtual void PrintType();
};

////////////////////////////////////////////////
// CLASS PTN_DEC_TEMP_ELEM DEFINITION
////////////////////////////////////////////////

class PTN_Inv_Temp_Elem : public PTNode_ListElem<PTN_Inv_Temp_Elem*>
{
private:
   string id;
   BinderState state;

public:
   PTN_Inv_Temp_Elem(const Pos& pos, const BinderState& p_state, const string& p_id) //NAME_TEMPLATE_REF
      : PTNode_ListElem<PTN_Inv_Temp_Elem*>(0, pos, INV_TEMP_ELEM)
   {
      state = p_state;
      id = p_id;
   }

   PTN_Inv_Temp_Elem(const Pos& pos, const BinderState& p_state, const string& p_id, PTNode* inner_template)
      : PTNode_ListElem<PTN_Inv_Temp_Elem*>(1, pos, INV_TEMP_ELEM)
   {
      state = p_state;
      id = p_id;
      this->Insert(inner_template, 0);
   }

   virtual ~PTN_Inv_Temp_Elem() { }

   void BuildList(std::set<PTN_Inv_Temp_Elem*>& elem_list)
   {
      elem_list.insert(this);
   }
   void BuildList(std::list<PTN_Inv_Temp_Elem*>& elem_list)
   {
      elem_list.push_back(this);
   }
   void BuildVector(std::vector<PTN_Inv_Temp_Elem*>& elem_list)
   {
      elem_list.push_back(this);
   }

   BinderState Get_Type() { return state; }
   string Get_Id() { return id; }

   virtual bool GenerateST(SymbolTable* st) {return true; }
	virtual bool GenerateIC(PP_Node **parent, SymbolTable *st);
   virtual void PrintType();

   string GetLabel();

};



////////////////////////////////////////////////
// CLASS PTN_INV_TEMP_LIST DEFINITION
////////////////////////////////////////////////

class PTN_Inv_Temp_List : public PTNode_List<PTN_Inv_Temp_Elem*>
{
public:
   PTN_Inv_Temp_List(const Pos& pos, PTNode* node)
      : PTNode_List<PTN_Inv_Temp_Elem*>(pos, node, INV_TEMP_LIST) { }

   PTN_Inv_Temp_List(const Pos& pos, PTNode* node, PTNode* nodeList)
      : PTNode_List<PTN_Inv_Temp_Elem*>(pos, node, nodeList, INV_TEMP_LIST) { }

   virtual bool GenerateST(SymbolTable* st) { return true; }
   virtual void PrintType();
};


////////////////////////////////////////////////
// CLASS PTN_PIINV_TEMPLATE DEFINITION
////////////////////////////////////////////////

class PTN_PiInv_Template : public PTNode
{
private:
   string id;
public:
   PTN_PiInv_Template(const Pos& pos, const string& p_id, PTNode* node);

   virtual bool GenerateST(SymbolTable* st);
	virtual bool GenerateIC(PP_Node **parent, SymbolTable *st);
   virtual void PrintType();
   string GetLabel();
};

////////////////////////////////////////////////
// CLASS PTN_PIINV_TEMPLATE DEFINITION
////////////////////////////////////////////////
class PTN_BpInv_Template : public PTNode
{
private:
   string templateId;
   string number;
   string bprocId;

   string BuildTemplateName(const vector<PTN_Inv_Temp_Elem*>& InvList);

public:
   //contructor for "run list" style: template instantiation inline bound to a number 
   PTN_BpInv_Template(const Pos& pos, const string& p_templateId, const string& p_number, PTNode* node);

   //constructor for "namespace binding" style: create a "shortcut" for referring to that 
   //process
   PTN_BpInv_Template(const Pos& pos, const string& p_templateId, PTNode* node, const string& p_betaId);

   virtual bool GenerateST(SymbolTable* st);
   virtual void PrintType();
};


#endif 
