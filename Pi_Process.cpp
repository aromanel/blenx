#include <iostream>
#include <algorithm>
#include <iterator>
#include <functional>
#include <fstream>
#include "Pi_Process.h"
#include "Environment.h"

using namespace std;

/////////////////////////////////////////////////////////////////////
// PREDICATES METHODS DEFINITION
/////////////////////////////////////////////////////////////////////

bool L_Predicate(PP_Node *left, PP_Node *right) {
	return left->CLabel() < right->CLabel();
}


/////////////////////////////////////////////////////////////////////
// PP_NODE METHODS DEFINITION 
/////////////////////////////////////////////////////////////////////


//
//PP_Node::PP_Node() {
//	parent = NULL;
//}

//
PP_Node::PP_Node(NodeType p_category) {
	category = p_category;
	parent = NULL;
}

//
PP_Node::~PP_Node() {
	PP_Node *tmp;
	while(!children.empty()) {
		tmp = children.front();
		children.pop_front();
		delete(tmp);
	}
}

//
void PP_Node::InsertChild(PP_Node *child) {
	if (child != NULL) {
		child->parent = this;
		children.push_back(child);
	}
}

//
list<PP_Node *> *PP_Node::GetChildren() {
	return &children;
}

PP_Node* PP_Node::GetChildren(size_t pos) 
   {
      assert (pos < children.size());
      list<PP_Node *>::iterator it = children.begin();
      for (size_t i = 0; i < pos; ++i)
      {
         ++it;
      }
      return *it;
   }
 
   void PP_Node::SetChildren(size_t pos, PP_Node* child)
   {
      assert (pos < children.size());
      list<PP_Node *>::iterator it = children.begin();
      for (size_t i = 0; i < pos; ++i)
      {
         ++it;
      }
      *it = child;
      child->SetParent(this);
   }


//
NodeType PP_Node::GetCategory() {
	return category;
}

//
PP_Node *PP_Node::GetCopy() 
{
	PP_Node *element = this->DeepClone();
	return element;
}

//
PP_Node *PP_Node::GetCopyRed(PP_Node *ptr1, PP_Node *ptr2, PP_Node **new_ptr1, PP_Node **new_ptr2) 
{
	PP_Node* element = this->Clone();   

 	list<PP_Node *>::iterator i;
	for(i=children.begin();i!=children.end();i++)
		element->InsertChild((*i)->GetCopyRed(ptr1, ptr2, new_ptr1, new_ptr2));

   if (element->category == PP_SEQ)
      assert(element->ChildrenCount() > 0);

	if (ptr1 == this)
		(*new_ptr1) = element;
	if (ptr2 == this)
		(*new_ptr2) = element;
	return element;
}

// Dopo aver introdotto l'IFTHEN, is_sum non serve piu'
void PP_Node::RedProcess(PP_Node **root, bool is_sum, string *name) {
	// La riduzione deve essere su una SEQ
	// Controllo che il padre dell'azione che esegue sia una SEQ
	if ( parent == NULL || parent->category != PP_SEQ ) return;
	// prendo il processo guardato dall'azione che sto eseguendo
	PP_Node *subproc = parent->children.back();
	// se e' un output prendo il suo oggetto 
	if (category == PP_OUTPUT) *name = ((PP_Output *)this)->GetObject();
	else if (category == PP_INPUT) { 
		if (*name != EMPTY_STRING)
				// se sto ricevendo un nome faccio la sostituzione
				subproc->Renaming(((PP_Input *)this)->GetObject(),*name);
		// decremento tutti i valori dei binding per il processo guardato
		// dall'azione che sto eseguendo
		subproc->DecBN();
	}
	else if (category == PP_EXPOSE && *name != EMPTY_STRING) {
		// sostituisco l'oggetto della expose con il nuovo nome creato
		subproc->Renaming(((PP_Expose *)this)->GetObject(),*name);
		// decremento tutti i valori dei binding per il processo guardato
		// dall'azione che sto eseguendo
		subproc->DecBN();
	}

	// Stacco dall'albero sintattico il sottoprocesso guardato dall'azione
	// che viene eseguita
	parent->children.pop_back();

	// Qui controllo se il nodo SEQ che contiene l'azione sia o meno
	// la radice
	if ( parent->parent == NULL )
		{
		delete(*root);
		subproc->SetParent(NULL);
		(*root) = subproc;
		}
	else
	{
		parent->parent->Reduction(root,parent,subproc);
	}
}

void PP_Node::Reduction(PP_Node **root, PP_Node *child, PP_Node *new_child)
{}


//
bool PP_Node::Renaming(const string& old_name, const string& new_name) {
	list<PP_Node *>::iterator i;
	for(i=children.begin();i!=children.end();i++) 
		if (!(*i)->Renaming(old_name,new_name)) break;
	return true;
}

//
void PP_Node::Renaming(list<Sub> *sub_list) {
	list<PP_Node *>::iterator i;
	for(i=children.begin();i!=children.end();i++)
		if (!sub_list->empty())
			(*i)->Renaming(sub_list);
}

//
string PP_Node::NameLess(int index) {
	list<PP_Node *>::iterator i;
	for(i=children.begin();i!=children.end();i++) 
		(*i)->NameLess(index);
	return EMPTY_STRING;
}

//
string PP_Node::GetLabel() {
	return ToString(category);
}

//
void OrderR(bool guard, PP_Node **current) {
	PP_Node *tmp;
	if (!guard && (*current)->category == PP_REPLICATION) {
		tmp = ((PP_Replication *)(*current))->Expand();
		delete(*current);
		(*current) = tmp;
	}
	(*current)->label = (*current)->GetLabel();
	list<PP_Node *>::iterator i = (*current)->children.begin();
	while(i != (*current)->children.end()) {
		if ((*current)->category != PP_PARALLEL && (*current)->category != PP_CHOICE)
				guard = true;
		OrderR(guard,&(*i++));
	}
	if ((*current)->category == PP_PARALLEL || (*current)->category == PP_CHOICE)
		(*current)->children.sort(L_Predicate);
	for(i=(*current)->children.begin();i!=(*current)->children.end();i++) {
		(*current)->label += (*i)->CLabel();
		(*i)->RLabel();
	}
}

//
void Order(PP_Node **root) {
	OrderR(false,root);
	(*root)->RLabel();
}

//
bool PP_Node::Equal(PP_Node *element) {
	if (GetLabel() != element->GetLabel()) return false;
	if (children.size() != element->children.size()) return false;
	list<PP_Node *>::iterator i = children.begin();
	list<PP_Node *>::iterator j = element->children.begin();
	while(i!=children.end())
		if (!(*i++)->Equal(*j++)) return false;
	// posso farlo anche mantenendo le label delle root
	// e controllando la loro uguaglianza
	return true;
}

//
void PP_Node::Compress() {
	list<PP_Node *>::iterator i;
	for(i=children.begin();i!=children.end();i++) 
		(*i)->Compress();
}

//
void Implosion(PP_Node **element) {
	PP_Node *current,*tmp,*back;
	list<PP_Node *>::iterator i;
	for(i=(*element)->children.begin();i!=(*element)->children.end();i++) 
		Implosion(&(*i));
	if ((*element)->category == PP_SEQ) {
		//PP_Seq *c_element = (PP_Seq *)(*element);
		back = (*element)->children.back();
		if (back->category == PP_PARALLEL || back->category == PP_REPLICATION)
			if ((current = back->IsReplication((*element)->children.front())) != NULL) {
				tmp = *element;
				*element = current;
				if (back->category == PP_REPLICATION) tmp->children.pop_back();
				delete tmp;
			}
	}
}

//
void EliminateNil(PP_Node **element) {
	PP_Node *tmp;
	PP_Node *tmp_parent;
	list<PP_Node *>::iterator i;
	for(i=(*element)->children.begin();i!=(*element)->children.end();i++)
		 EliminateNil(&(*i));
	// Cancello i NIL dai paralleli e dalle somme
	if ((*element)->category == PP_PARALLEL || (*element)->category == PP_CHOICE) 
	{
		//int nil_number = 0;
		list<PP_Node *>::iterator i = (*element)->children.begin();
		while(i!=(*element)->children.end())
			if ((*i)->category == PP_NIL) {
				tmp = *i;
				i = (*element)->children.erase(i);
				delete(tmp);
			}
			else i++;
			tmp_parent = (*element)->parent;
			if ((*element)->children.size() == 0) {
				delete *element;
				*element = new PP_Nil();
			}
			else if ((*element)->children.size() == 1) {
				tmp = (*element)->children.front();
				(*element)->children.pop_front();
				delete *element;
				*element = tmp;
			}
			(*element)->SetParent(tmp_parent);
	}
}

//
PP_Node *PP_Node::IsReplication(PP_Node *action) {
	return NULL;
}

//
void PP_Node::DecBN() {
	list<PP_Node *>::iterator i;
	for(i=children.begin();i!=children.end();i++) 
		(*i)->DecBN();
}

//
void PP_Node::IncBN() {
	list<PP_Node *>::iterator i;
	for(i=children.begin();i!=children.end();i++) 
		(*i)->IncBN();
}

//
void PP_Node::UpdateState(Entity *e, const string& label)
{
	list<PP_Node *>::iterator i;
	for(i=children.begin();i!=children.end();i++) 
		(*i)->UpdateState(e,label);
}

//
void PP_Node::SetParent(PP_Node *element) { 
	parent = element; 
}

//
PP_Node *PP_Node::GetParent() { 
	return parent; 
}

//
void PP_Node::RLabel() { 
	label = ""; 
}

//
const string& PP_Node::CLabel() { 
	return label; 
}

//
bool PP_Node::Compare(PP_Node *element,bool *val) { 
	return true; 
}	

//
void PP_Node::PrintV() {
}

//
void PP_Node::Print(int space) {
	for (int i=0; i<space; i++) cout << "          ";
	cout << ToString(category) << " ";
	this->PrintV();
	cout << endl;
	list<PP_Node *>::iterator i;
	for(i=children.begin();i!=children.end();i++) {
		(*i)->Print(space+1);
	}
}

//
void PP_Node::PrintProc(ofstream *fstr) {}



/////////////////////////////////////////////////////////////////////
// PP_NIL METHODS DEFINITION
/////////////////////////////////////////////////////////////////////

//
PP_Nil::PP_Nil():PP_Node(PP_NIL) {
}

//
PP_Nil::~PP_Nil() {
}

void PP_Nil::PrintProc(ofstream *fstr) {
	(*fstr) << "nil";
}



/////////////////////////////////////////////////////////////////////
// PP_SEQ METHODS DEFINITION
/////////////////////////////////////////////////////////////////////

//
PP_Seq::PP_Seq():PP_Node(PP_SEQ) { }

//
PP_Seq::PP_Seq(PP_Node *action, PP_Node *process):PP_Node(PP_SEQ) {
	this->InsertChild(action);
	this->InsertChild(process);
}

//
void PP_Seq::Renaming(list<Sub> *sub_list) {
	list<Sub> tmp = *sub_list;
	children.front()->Renaming(&tmp);
	if (!tmp.empty()) children.back()->Renaming(&tmp);
}

//
string PP_Seq::NameLess(int index) {
	const string& ret = children.front()->NameLess(index);
	if (ret != EMPTY_STRING) 
		children.back()->Renaming(ret,BN+Utility::i2s(index));
	if (children.front()->GetCategory() == PP_INPUT || children.front()->GetCategory() == PP_EXPOSE) 
      index++;
	children.back()->NameLess(index);
	return EMPTY_STRING;
}

//
void PP_Seq::UpdateState(Entity *e, const string& label) {
	children.front()->UpdateState(e,label);
}

//
void PP_Seq::PrintProc(ofstream *fstr) {
	children.front()->PrintProc(fstr);
	(*fstr) << ".(";
	children.back()->PrintProc(fstr);
	(*fstr) << ")";
}


/////////////////////////////////////////////////////////////////////
// PP_REPLICATION METHODS DEFINITION
/////////////////////////////////////////////////////////////////////

//
PP_Replication::PP_Replication():PP_Node(PP_REPLICATION) { }

//
PP_Replication::PP_Replication(PP_Node *name, PP_Node *action):PP_Node(PP_REPLICATION) {
	this->InsertChild(name);
	this->InsertChild(action);
}

//
void PP_Replication::Renaming(list<Sub> *sub_list) {
	list<Sub> tmp = *sub_list;
	children.front()->Renaming(&tmp);
	if (!tmp.empty()) children.back()->Renaming(&tmp);
}

//
string PP_Replication::NameLess(int index) {
	const string& ret = children.front()->NameLess(index);
	if (ret != EMPTY_STRING) 
		children.back()->Renaming(ret,BN+Utility::i2s(index));
	if (children.front()->GetCategory() == PP_INPUT  || children.front()->GetCategory() == PP_EXPOSE) 
		index++;
	children.back()->NameLess(index);
	return EMPTY_STRING;
}

//
PP_Node *PP_Replication::IsReplication(PP_Node *action) { 
	bool res = false;
	if (children.front()->Compare(action,&res))
		if (children.back()->GetCategory() == PP_NIL) {
			DecBN();
			return this;
		}
	return NULL;
}

//
void PP_Replication::UpdateState(Entity *e, const string& label) {
}

//
PP_Node *PP_Replication::Expand() {
	PP_Node *right,*rep,*left,*root,*proc;
	list<PP_Node *>::iterator i;
	left = children.front()->GetCopy();
	right = children.back()->GetCopy();
	rep = this->GetCopy();
	if (left->GetCategory() == PP_INPUT  || children.front()->GetCategory() == PP_EXPOSE)
		rep->IncBN();
	if (children.back()->GetCategory() == PP_NIL) {
		root = new PP_Seq(left,rep);
	}
	else {
		proc = new PP_Parallel();
		if (children.back()->GetCategory() == PP_PARALLEL) {
			list<PP_Node *> *ch = children.back()->GetChildren();
			for(i=ch->begin();i!=ch->end();i++)
				proc->InsertChild((*i)->GetCopy());
		}
		else proc->InsertChild(children.back()->GetCopy());
		proc->InsertChild(rep);
		root = new PP_Seq(left,proc);
	}
	root->SetParent(this->GetParent());
	return root;
}

//
void PP_Replication::PrintProc(ofstream *fstr) {
	(*fstr) << "rep ";
	children.front()->PrintProc(fstr);
	(*fstr) << ".(";
	children.back()->PrintProc(fstr);
	(*fstr) << ")";
}


/////////////////////////////////////////////////////////////////////
// PP_PARALLEL METHODS DEFINITION
/////////////////////////////////////////////////////////////////////

//
PP_Parallel::PP_Parallel():PP_Node(PP_PARALLEL) {
}

//
PP_Parallel::~PP_Parallel() {
}

//
PP_Node *PP_Parallel::IsReplication(PP_Node *action) {
	list<PP_Node *>::iterator i = children.begin();
	PP_Node *tmp;
	PP_Node *elem;
	bool res = false;
	while(i!=children.end()) {
		if ((*i)->GetCategory() == PP_REPLICATION) {
			if ((*i)->GetChildren()->front()->Compare(action,&res)) {
				if (res) (*i)->DecBN();
				tmp = *i;
				i=children.erase(i);
			    if (children.size() > 1) elem = this;
				else elem = this->GetChildren()->front();
				if (elem->Equal(tmp->GetChildren()->back()))
					return tmp;
				else {
					if (i == children.end()) 
						children.push_back(tmp);
					else 
						children.insert(i++,tmp);
					if (res) 
						tmp->IncBN();
				}
			}
			else i++;
		}
		else i++;
	}
	return NULL;
}

//
void PP_Parallel::Compress() {
	list<PP_Node *> remove;
	list<PP_Node *>::iterator i;
	for(i=children.begin();i!=children.end();i++)
		if ((*i)->GetCategory() == this->GetCategory()) {
			list<PP_Node *> *child_list = (*i)->GetChildren();
			while(!child_list->empty()) {
				PP_Node *element = child_list->front();
				element->SetParent(this);
				children.push_back(element);
				child_list->pop_front();
			}
			remove.push_front((*i));
		}
	for(i=remove.begin();i!=remove.end();i++) {
			children.remove(*i);
			delete *i;
	}
}

//
void PP_Parallel::UpdateState(Entity *e, const string& label)
{
	list<PP_Node *>::iterator i;
	int index = 0;
	for(i=children.begin();i!=children.end();i++) 
		(*i)->UpdateState(e,label + "|" + Utility::i2s(index++));
}

//
void PP_Parallel::PrintProc(ofstream *fstr) {
	list<PP_Node *>::iterator i=children.begin();
	while(i!=children.end()) {
		(*i++)->PrintProc(fstr);
		if (i!=children.end())
			(*fstr) << " | ";
	}
}

void PP_Parallel::Reduction(PP_Node **root, PP_Node *child, PP_Node *new_child)
{
	std::list<PP_Node *>::iterator i = children.begin();
	while ( i != children.end() )
	{
		if ( (*i) == child )
		{
			if ( new_child->GetCategory() == PP_PARALLEL )
			{
				while(!new_child->GetChildren()->empty()) 
				{
					new_child->GetChildren()->front()->SetParent(this);
					this->children.push_back(new_child->GetChildren()->front());
					new_child->GetChildren()->pop_front();
					
				}
				delete(new_child);           

            // Avoid double delete!
            if (new_child == child)
            {
               // remember we already deleted "both"
               child = NULL;
            }
            new_child = NULL;

			}
			else
			{
				this->children.push_back(new_child);
				new_child->SetParent(this);
			}

			// stacco ed elimino il figlio ed esco dal ciclo
			i = this->children.erase(i);
         if (child != NULL)
			   delete(child);
			break;
		}
		else
			i++;
	}

	// Itero la chiamata sul padre se esiste. Il nodo parallel qui non
	// cambia e quindi i parametri sono uguali
	if ( parent != NULL )
		parent->Reduction(root,this,this);
}


/////////////////////////////////////////////////////////////////////
// PP_CHOICE METHODS DEFINITION
/////////////////////////////////////////////////////////////////////

//
PP_Choice::PP_Choice():PP_Node(PP_CHOICE) {
}

//
PP_Choice::~PP_Choice() {
}

//
void PP_Choice::Compress() {
	list<PP_Node *> remove;
	list<PP_Node *>::iterator i;
	for(i=children.begin();i!=children.end();i++)
		if ((*i)->GetCategory() == this->GetCategory()) {
			list<PP_Node *> *child_list = (*i)->GetChildren();
			while(!child_list->empty()) {
				PP_Node *element = child_list->front();
				element->SetParent(this);
				children.push_back(element);
				child_list->pop_front();
			}
			remove.push_front((*i));
		}
	for(i=remove.begin();i!=remove.end();i++) {
			children.remove(*i);
			delete *i;
	}
}

//
void PP_Choice::UpdateState(Entity *e, const string& label)
{
	list<PP_Node *>::iterator i;
	int index = 0;
	for(i=children.begin();i!=children.end();i++) 
		(*i)->UpdateState(e,label + "+" + Utility::i2s(index++));
}

//
void PP_Choice::PrintProc(ofstream *fstr) {
	(*fstr) << "( ";
 	list<PP_Node *>::iterator i=children.begin();
	while(i!=children.end()) {
		(*i++)->PrintProc(fstr);
		if (i!=children.end())
			(*fstr) << " + ";
	}
	(*fstr) << " )";
}

void PP_Choice::Reduction(PP_Node **root, PP_Node *child, PP_Node *new_child)
{
	if ( this == (*root) )
	{
		new_child->SetParent(NULL);
		(*root) = new_child;
		delete(this);
	}
	else
	{
		parent->Reduction(root,this,new_child);
	}
}


/////////////////////////////////////////////////////////////////////
// PP_TAU METHODS DEFINITION
/////////////////////////////////////////////////////////////////////

//
PP_Tau::PP_Tau(const string& p_rate):PP_Node(PP_TAU) {
	rate = p_rate;	
}

//
PP_Tau::PP_Tau(const PP_Tau& element):PP_Node(PP_TAU) {
	rate = element.rate;
}

//
PP_Tau::~PP_Tau() {}

//
string PP_Tau::GetLabel() {
	return "delay(" + rate + ")";
}

//
void PP_Tau::UpdateState(Entity *e, const string& label) {
	map<string,Tau *> *tau = e->GetTauMap();
	map<string,double>::iterator j;
	double c_rate = HUGE_VAL;
	if (rate == EMPTY_STRING) 
	{
		map<string, double>* rate_map = e->GetEnv()->GetRateMap();
		if ((j = rate_map->find("BASERATE")) != rate_map->end()) 
			c_rate = (*j).second;
	}
	else if (rate != INF_RATE)
	{
		c_rate = atof(rate.c_str());
	}
	(*tau)["$@" + label] = new Tau(e, c_rate,this,label);
}

//
void PP_Tau::PrintV() {
	cout << "delay(" << rate << ")";
}

//
void PP_Tau::PrintProc(ofstream *fstr) {
	(*fstr) << "delay(" << rate << ")";
}

//
bool PP_Tau::Compare(PP_Node *element,bool *val) {
	return (this->GetLabel() == element->GetLabel());
}


/////////////////////////////////////////////////////////////////////
// PP_TAU METHODS DEFINITION
/////////////////////////////////////////////////////////////////////

//
PP_Die::PP_Die(const string& p_rate):PP_Node(PP_DIE) {
	rate = p_rate;	
}

//
PP_Die::PP_Die(const PP_Die& element):PP_Node(PP_DIE) {
	rate = element.rate;
}

//
PP_Die::~PP_Die() {}

//
string PP_Die::GetLabel() {
	if ( rate != EMPTY_STRING )
		return "die(" + rate + ")";
	else
		return "die";
}

//
void PP_Die::UpdateState(Entity *e, const string& label) {
	map<string,Die *> *die = e->GetDieMap();
	map<string,double>::iterator j;
	double c_rate = HUGE_VAL;
	if (rate == EMPTY_STRING) 
	{
		map<string, double>* rate_map = e->GetEnv()->GetRateMap();
		if ((j = rate_map->find("BASERATE")) != rate_map->end()) 
			c_rate = (*j).second;
	}
	else if (rate != INF_RATE)
	{
		c_rate = atof(rate.c_str());
	}
	(*die)[label] = new Die(e, c_rate,this,label);
}

//
void PP_Die::PrintV() {
	if ( rate == EMPTY_STRING ) 
		cout << "die";
	else 
		cout << "die(" << rate << ")";
}

//
void PP_Die::PrintProc(ofstream *fstr) {
	if ( rate == EMPTY_STRING ) (*fstr) << "die";
	else (*fstr) << "die(" << rate << ")";
}

//
bool PP_Die::Compare(PP_Node *element,bool *val) {
	return (this->GetLabel() == element->GetLabel());
}


/////////////////////////////////////////////////////////////////////
// PP_CHANGE METHODS DEFINITION
/////////////////////////////////////////////////////////////////////

//
PP_Change::PP_Change(const string& p_rate,const string& p_subject,const string& p_type):PP_Node(PP_CHANGE) {
	rate = p_rate;
	type = p_type;
	subject = p_subject;
}

//
PP_Change::PP_Change(const PP_Change& element):PP_Node(PP_CHANGE) {
	rate = element.rate;
	type = element.type;
	subject = element.subject;
}

//
PP_Change::~PP_Change() {}

//
string PP_Change::GetLabel() {
	return "ch(" + rate + "," + subject + "," + type + ")";
}

//
void PP_Change::UpdateState(Entity *e, const string& label) {
	// Controls that the type or the subject 
	// do not already exist in the binder list
	if ( !e->BBIs(subject) ) return;
	if ( e->IsTypeBB(type,subject) ) return;
 	string key = subject + "$" + type + "$" + rate;
	map<string,Change *> *change = e->GetChangeMap();
	map<string,Change *>::iterator i;
	map<string,double>::iterator j;
	double c_rate = HUGE_VAL;
	if (rate == EMPTY_STRING) 
   {
      map<string, double>* rate_map = e->GetEnv()->GetRateMap();
		if ((j = rate_map->find("CHANGE")) != rate_map->end()) 
         c_rate = (*j).second;
		else if ((j = rate_map->find("BASERATE")) != rate_map->end()) 
         c_rate = (*j).second;
	}
	else if (rate != INF_RATE)
		c_rate = atof(rate.c_str());
	if ((i = change->find(key)) != change->end())
		(*i).second->AddChange(this,label);
	else {
		Change *current = new Change(e, c_rate);
		current->AddChange(this,label);
		(*change)[key] = current;
	}
}

//
void PP_Change::PrintV() {
	cout << "ch(";
	if (rate != EMPTY_STRING) cout << rate << ",";
	cout << subject << "," << type << ")";
}

//
void PP_Change::PrintProc(ofstream *fstr) {
	(*fstr) << "ch(";
	if (rate != EMPTY_STRING) (*fstr) << rate << ",";
	(*fstr) << subject << "," << type << ")";
}

//
void PP_Change::DecBN() {
	if (subject.substr(0,3) == BN)
		subject = BN + Utility::i2s(atoi(subject.substr(3).c_str())-1);
}

//
void PP_Change::IncBN() {
	if (subject.substr(0,3) == BN)
		subject = BN + Utility::i2s(atoi(subject.substr(3).c_str())+1);
}

//
bool PP_Change::Renaming(const string& old_name, const string& new_name) {
	if (subject == old_name) subject = new_name;
	return true;
}

//
bool PP_Change::Compare(PP_Node *element,bool *val) {
	return (this->GetLabel() == element->GetLabel());
}



/////////////////////////////////////////////////////////////////////
// PP_INPUT METHODS DEFINITION
/////////////////////////////////////////////////////////////////////

//
PP_Input::PP_Input(const string& p_subject, const string& p_object):PP_Node(PP_INPUT) {
	subject = p_subject;
	object = p_object;
}

//
PP_Input::PP_Input(const PP_Input& element):PP_Node(PP_INPUT) {
	subject = element.subject;
	object = element.object;
}

//
PP_Input::~PP_Input() {
}

//
bool PP_Input::Renaming(const string& old_name, const string& new_name) {
	if (old_name == subject) subject = new_name;
	if (old_name == object) return false;
	return true;
}

//
void PP_Input::Renaming(list<Sub> *sub_list) {
	list<Sub>::iterator i = sub_list->begin();
	bool res = true;
	while(i!=sub_list->end()) {
		if (subject == (*i).old_v && res) { 
			subject = (*i).new_v; 
			res = false;
		}
		if (object == (*i).old_v) i = sub_list->erase(i);
		else i++;
	}
}

//
string PP_Input::NameLess(int index) {
	string tmp = object;
	object = BN + Utility::i2s(index);
	return tmp;
}

//
string PP_Input::GetLabel() {
	return subject + "?(" + object + ")";
}

//
void PP_Input::UpdateState(Entity *e, const string& label)
{
	double rate = HUGE_VAL;
	map<string,double>::iterator j;
	map<string, double>* rate_map = e->GetEnv()->GetRateMap();

	if ((rate = e->GetSubjectRate(subject)) == -1)
	{
		if ((j = rate_map->find(subject)) != rate_map->end()) 
			rate = (*j).second;
		else if ((j = rate_map->find("BASERATE")) != rate_map->end()) 
			rate = (*j).second;
	}

	Intra *current;
	map<string,Intra *> *intra = e->GetIntraMap();
	map<string,Intra *>::iterator i;
	if ((i = intra->find(subject)) != intra->end()) 
	{
		(*i).second->AddIn(this,label);
		(*i).second->SetRate(rate);
	}
	else 
	{
		current = new Intra(e);
		current->AddIn(this,label);
		current->SetRate(rate);
		(*intra)[subject] = current;
	}
	e->BBAddIn(subject);
}

//
bool PP_Input::Compare(PP_Node *element,bool *val) {
	if (this->GetCategory() != element->GetCategory()) return false;
	PP_Input *c_element = (PP_Input *)element;
	if (subject != c_element->subject) return false;
	if (object != c_element->object) *val = true;
	return true;
}

//
void PP_Input::DecBN() {
	if (object.substr(0,3) == BN)
		object = BN + Utility::i2s(atoi(object.substr(3).c_str())-1);
	if (subject.substr(0,3) == BN)
		subject = BN + Utility::i2s(atoi(subject.substr(3).c_str())-1);
}

//
void PP_Input::IncBN() {
	if (object.substr(0,3) == BN)
		object = BN + Utility::i2s(atoi(object.substr(3).c_str())+1);
	if (subject.substr(0,3) == BN)
		subject = BN + Utility::i2s(atoi(subject.substr(3).c_str())+1);
}

//
const string& PP_Input::GetSubject() { 
	return subject; 
}

//
const string& PP_Input::GetObject() { 
	return object; 
}

//
void PP_Input::PrintV() {
	cout << subject << "?(" << object << ")";
}

//
void PP_Input::PrintProc(ofstream *fstr) {
	(*fstr) << subject << "?(" << object << ")";
}



/////////////////////////////////////////////////////////////////////
// PP_OUTPUT METHODS DEFINITION
/////////////////////////////////////////////////////////////////////

//
PP_Output::PP_Output(const string& p_subject, const string& p_object):PP_Node(PP_OUTPUT) {
	subject = p_subject;
	object = p_object;
}

//
PP_Output::PP_Output(const PP_Output& element):PP_Node(PP_OUTPUT) {
	subject = element.subject;
	object = element.object;
}

//
PP_Output::~PP_Output() {
}

//
bool PP_Output::Renaming(const string& old_name, const string& new_name) {
	if (old_name == subject) subject = new_name;
	if (old_name == object) object = new_name;
	return true;
}

//
void PP_Output::Renaming(list<Sub> *sub_list) {
	list<Sub>::iterator i;
	bool res_o, res_s;
	res_o = res_s = true;
	for(i=sub_list->begin();i!=sub_list->end();i++) {
		if (object == (*i).old_v && res_o) { 
			object = (*i).new_v; 
			res_o = false; 
		}
		if (subject == (*i).old_v && res_s) { 
			subject = (*i).new_v; 
			res_s = false; 
		}
		if (res_o && res_s) return;
	}
}

//
string PP_Output::GetLabel() {
	return subject + "!(" + object + ")";
}

//
void PP_Output::UpdateState(Entity *e, const string& label)
{
	double rate = HUGE_VAL;
	map<string,double>::iterator j;
	map<string, double>* rate_map = e->GetEnv()->GetRateMap();

	if ((rate = e->GetSubjectRate(subject)) == -1)
	{
		if ((j = rate_map->find(subject)) != rate_map->end()) 
			rate = (*j).second;
		else if ((j = rate_map->find("BASERATE")) != rate_map->end()) 
			rate = (*j).second;
	}
	Intra *current;
	map<string,Intra *> *intra = e->GetIntraMap();
	map<string,Intra *>::iterator i;
	if ((i = intra->find(subject)) != intra->end()) {
		(*i).second->AddOut(this,label);
		(*i).second->SetRate(rate);
	}
	else {
		current = new Intra(e);
		current->AddOut(this,label);
		current->SetRate(rate);
		(*intra)[subject] = current;
	}
	e->BBAddOut(subject,object);
}

//
bool PP_Output::ObjectBinder(list<BBinders *> *bb_list) {
	list<BBinders *>::iterator k;
	for(k=bb_list->begin();k!=bb_list->end();k++)
		if ((*k)->GetSubject() == object) return true;
	return false;
}

//
bool PP_Output::Compare(PP_Node *element, bool *val) {
	return (this->GetLabel() == element->GetLabel());
}

//
void PP_Output::DecBN() {
	if (object.substr(0,3) == BN)
		object = BN + Utility::i2s(atoi(object.substr(3).c_str())-1);
	if (subject.substr(0,3) == BN)
		subject = BN + Utility::i2s(atoi(subject.substr(3).c_str())-1);
}

//
void PP_Output::IncBN() {
	if (object.substr(0,3) == BN)
		object = BN + Utility::i2s(atoi(object.substr(3).c_str())+1);
	if (subject.substr(0,3) == BN)
		subject = BN + Utility::i2s(atoi(subject.substr(3).c_str())+1);
}

//
const string& PP_Output::GetSubject() { 
	return subject; 
}

//
const string& PP_Output::GetObject() { 
	return object; 
}

//
void PP_Output::PrintV() {
	cout << subject << "!(" << object << ")";
}

//
void PP_Output::PrintProc(ofstream *fstr) {
	(*fstr) << subject << "!(" << object << ")";
}


/////////////////////////////////////////////////////////////////////
// PP_EXPOSE METHODS DEFINITION
/////////////////////////////////////////////////////////////////////

//
PP_Expose::PP_Expose(const string& p_object, const string& p_type, const string& p_rate, const string& p_object_rate):PP_Node(PP_EXPOSE) {
	object = p_object;
	type = p_type;
	rate = p_rate;
	object_rate = p_object_rate;
}

//
PP_Expose::PP_Expose(const PP_Expose& element):PP_Node(PP_EXPOSE) {
	object = element.object;
	type = element.type;
	rate = element.rate;
	object_rate = element.object_rate;
}

//
PP_Expose::~PP_Expose() {
}

//
bool PP_Expose::Renaming(const string& old_name, const string& new_name) {
	if (object == old_name) return false;
	return true;
}

//
void PP_Expose::Renaming(list<Sub> *sub_list) {
	list<Sub>::iterator i = sub_list->begin();
	//bool res = true;
	while(i!=sub_list->end()) {
		if (object == (*i).old_v) i = sub_list->erase(i);
		else i++;
	}
}

//
string PP_Expose::GetLabel() {
	if (rate != EMPTY_STRING) 
		return "expose(" + rate + "," + object + ":" + object_rate + "," + type + ")";
	else
		return "expose(" + object + ":" + object_rate + "," + type + ")";
}

//
string PP_Expose::NameLess(int index) {
	string tmp = object;
	object = BN + Utility::i2s(index);
	return tmp;
}

//
bool PP_Expose::Compare(PP_Node *element,bool *val) {
	PP_Expose *c_element;
	if (this->GetCategory() != element->GetCategory()) return false;
	c_element = (PP_Expose *)element;
	if (rate != c_element->rate) return false;
	if (object_rate != c_element->object_rate) return false;
	if (object != c_element->object) *val = true;
	return true;
}

//
void PP_Expose::DecBN() {
	if (object.substr(0,3) == BN)
		object = BN + Utility::i2s(atoi(object.substr(3).c_str())-1);
}

//
void PP_Expose::IncBN() {
	if (object.substr(0,3) == BN)
		object = BN + Utility::i2s(atoi(object.substr(3).c_str())+1);
}

//
void PP_Expose::PrintV() {
	cout << "expose(";
	if (rate != EMPTY_STRING) cout << rate << ",";
	cout << object << ":" << object_rate << "," << type << ")";
}

//
void PP_Expose::UpdateState(Entity *e, const string& label)
{
	if (e->IsTypeBB(type)) return;
	map<string,Expose *> *expose = e->GetExposeMap();
	map<string, double>::iterator j;
   map<string, double>* rate_map = e->GetEnv()->GetRateMap();
	double c_rate = HUGE_VAL;
	if (rate == EMPTY_STRING)
   {
		if ((j = rate_map->find("EXPOSE")) != rate_map->end()) 
         c_rate = (*j).second;
		else if ((j = rate_map->find("BASERATE")) != rate_map->end()) 
         c_rate = (*j).second;
	}
	else if (rate != INF_RATE)
		c_rate = atof(rate.c_str());
	(*expose)[object + label] = new Expose(e, c_rate,this,label);
}

//
const string& PP_Expose::GetObject() { 
	return object; 
}

//
const string& PP_Expose::GetType() { 
	return type; 
}

//
const string& PP_Expose::GetRate() { 
	return rate; 
}

//
const string& PP_Expose::GetObjectRate() { 
	return object_rate; 
}

//
void PP_Expose::PrintProc(ofstream *fstr) {
	(*fstr) << "expose(";
	if (rate != EMPTY_STRING) (*fstr) << rate << ",";
	(*fstr) << object << ":" << object_rate << "," << type << ")";
}


/////////////////////////////////////////////////////////////////////
// PP_HIDE METHODS DEFINITION
/////////////////////////////////////////////////////////////////////

//
PP_Hide::PP_Hide(const string& p_object, const string& p_rate):PP_Node(PP_HIDE) {
	object = p_object;
	rate = p_rate;
}

//
PP_Hide::PP_Hide(const PP_Hide& element):PP_Node(PP_HIDE) {
	object = element.object;
	rate = element.rate;
}

//
PP_Hide::~PP_Hide() {
}

//
bool PP_Hide::Renaming(const string& old_name, const string& new_name) {
	if (object == old_name) object = new_name;
	return true;
}

//
void PP_Hide::Renaming(list<Sub> *sub_list) {
	list<Sub>::iterator i;
	for(i=sub_list->begin();i!=sub_list->end();i++)
		if (object == (*i).old_v) {
			object = (*i).new_v;
			return;
		}
}

//
string PP_Hide::GetLabel() {
	if (rate != EMPTY_STRING)
		return "hide(" + rate + "," + object + ")";
	else
		return "hide(" + object + ")";
}


//
void PP_Hide::DecBN() {
	if (object.substr(0,3) == BN)
		object = BN + Utility::i2s(atoi(object.substr(3).c_str())-1);
}

//
void PP_Hide::IncBN() {
	if (object.substr(0,3) == BN)
		object = BN + Utility::i2s(atoi(object.substr(3).c_str())+1);
}

//
void PP_Hide::UpdateState(Entity *e, const string& label)
{
	if (!e->BBUnhide(object)) return;
	string key = object + "$" + rate;
	double c_rate = HUGE_VAL;
	map<string, Hide *> *hide = e->GetHideMap();
	map<string, Hide *>::iterator i;
	map<string, double>::iterator j;
	map<string, double>* rate_map = e->GetEnv()->GetRateMap();

	if (rate == EMPTY_STRING) 
	{
		if ((j = rate_map->find("HIDE")) != rate_map->end()) 
			c_rate = (*j).second;
		else if ((j = rate_map->find("BASERATE")) != rate_map->end()) 
			c_rate = (*j).second;
	}
	else if (rate != INF_RATE)
		c_rate = atof(rate.c_str());
	if ((i = hide->find(key)) != hide->end())
		(*i).second->AddHide(this,label);
	else {
		Hide *current = new Hide(e, c_rate);
		current->AddHide(this,label);
		(*hide)[key] = current;
	}

}

//
const string& PP_Hide::GetObject() { 
	return object; 
}

//
void PP_Hide::PrintV() {
	cout << "hide(";
	if (rate != EMPTY_STRING) cout << rate << ",";
	cout << object << ")";
}

//
void PP_Hide::PrintProc(ofstream *fstr) {
	(*fstr) << "hide(";
	if (rate != EMPTY_STRING) (*fstr) << rate << ",";
	(*fstr) << object << ")";
}

//
bool PP_Hide::Compare(PP_Node *element,bool *val) {
	return (this->GetLabel() == element->GetLabel());
}


/////////////////////////////////////////////////////////////////////
// PP_UNHIDE METHODS DEFINITION
/////////////////////////////////////////////////////////////////////

//
PP_Unhide::PP_Unhide(const string& p_object, const string& p_rate):PP_Node(PP_UNHIDE) {
	object = p_object;
	rate = p_rate;
}

//
PP_Unhide::PP_Unhide(const PP_Unhide& element):PP_Node(PP_UNHIDE) {
	object = element.object;
	rate = element.rate;
}

//
PP_Unhide::~PP_Unhide() {
}

//
bool PP_Unhide::Renaming(const string& old_name, const string& new_name) {
	if (object == old_name) object = new_name;
	return true;
}

//
void PP_Unhide::Renaming(list<Sub> *sub_list) {
	list<Sub>::iterator i;
	for(i=sub_list->begin();i!=sub_list->end();i++)
		if (object == (*i).old_v) {
			object = (*i).new_v;
			return;
		}
}

//
string PP_Unhide::GetLabel() {
	if (rate != EMPTY_STRING)
		return "unhide(" + rate + "," + object + ")";
	else
		return "unhide(" + object + ")";
}


//
void PP_Unhide::DecBN() {
	if (object.substr(0,3) == BN)
		object = BN + Utility::i2s(atoi(object.substr(3).c_str())-1);
}

//
void PP_Unhide::IncBN() {
	if (object.substr(0,3) == BN)
		object = BN + Utility::i2s(atoi(object.substr(3).c_str())+1);
}

//
void PP_Unhide::UpdateState(Entity *e, const string& label)
{
	if (!e->BBHide(object)) return;
	string key = object + "$" + rate;
	double c_rate = HUGE_VAL;
	map<string, Unhide *> *unhide = e->GetUnhideMap();
	map<string, Unhide *>::iterator i;
	map<string, double>* rate_map = e->GetEnv()->GetRateMap();
	map<string, double>::iterator j;

	if (rate == EMPTY_STRING) 
	{
		if ((j = rate_map->find("UNHIDE")) != rate_map->end()) 
			c_rate = (*j).second;
		else if ((j = rate_map->find("BASERATE")) != rate_map->end()) 
			c_rate = (*j).second;
	}
	else if (rate != INF_RATE) c_rate = atof(rate.c_str());
	if ((i = unhide->find(key)) != unhide->end())
		(*i).second->AddUnhide(this,label);
	else {
		Unhide *current = new Unhide(e, c_rate);
		current->AddUnhide(this,label);
		(*unhide)[key] = current;
	}
}

//
const string& PP_Unhide::GetObject() { 
	return object; 
}

//
void PP_Unhide::PrintV() {
	cout << "unhide(";
	if (rate != EMPTY_STRING) cout << rate << ",";
	cout << object << ")";
}

//
void PP_Unhide::PrintProc(ofstream *fstr) {
	(*fstr) << "unhide(";
	if (rate != EMPTY_STRING) (*fstr) << rate << ",";
	(*fstr) << object << ")";
}

//
bool PP_Unhide::Compare(PP_Node *element,bool *val) {
	return (this->GetLabel() == element->GetLabel());
}


/////////////////////////////////////////////////////////////////////
// PP_ATOM METHODS DEFINITION
/////////////////////////////////////////////////////////////////////

PP_Atom::PP_Atom(const string& p_bsubject, const string& p_btype, const BinderState& p_bstate)
	:PP_Node(PP_ATOM)
{
	bsubject = p_bsubject;
	btype = p_btype;
	bstate = p_bstate;
}

PP_Atom::PP_Atom(const PP_Atom& element) 
	:PP_Node(PP_ATOM)
{
	bsubject = element.bsubject;
	btype = element.btype;
	bstate = element.bstate;
}

PP_Atom::~PP_Atom() {}

//
bool PP_Atom::Renaming(const string& old_name, const string& new_name) {
	if (bsubject == old_name) bsubject = new_name;
	return true;
}

void PP_Atom::PrintV() 
{
	cout  << "(" << bsubject << "," << btype << "," << ToStringC(bstate) << ")"; 
}

void PP_Atom::PrintProc(ofstream *fstr) 
{
	(*fstr) << "(" << bsubject ;
	if (btype != EMPTY_STRING) 
		(*fstr) << "," << btype;
	if (bstate != STATE_NOT_SPECIFIED)
		(*fstr) << "," << ToStringC(bstate); 
	(*fstr) << ")"; 
}

string PP_Atom::GetLabel() 
{ 
	return "(" + bsubject + "," + btype + "," + ToStringC(bstate) + ")"; 
}

bool PP_Atom::Compare(PP_Node *element, bool *val)
{
	return (this->GetLabel() == element->GetLabel());
}

bool PP_Atom::Satisfy(std::list<BBinders *> *bb_list) 
{
	list<BBinders *>::iterator k;
	for(k=bb_list->begin();k!=bb_list->end();k++)
		if ((*k)->GetSubject() == bsubject) {
			if (bstate != STATE_NOT_SPECIFIED) {
				if (btype != EMPTY_STRING) {
					if ((*k)->GetState() == bstate && (*k)->GetType() == btype)
						return true;
				}
				else if ((*k)->GetState() == bstate)
					return true;		
			}
			else if ((*k)->GetType() == btype)
				return true;
		}
	return false;
}

/////////////////////////////////////////////////////////////////////
// PP_EXPRESSION METHODS DEFINITION
/////////////////////////////////////////////////////////////////////

PP_Expression::PP_Expression(const ExpressionType& p_etype)
	:PP_Node(PP_EXPRESSION) 
{
	etype = p_etype;
}

PP_Expression::PP_Expression(const PP_Expression& element) 
	:PP_Node(PP_EXPRESSION)
{
	etype = element.etype;
}

PP_Expression::~PP_Expression() {}

void PP_Expression::PrintV() 
{
	std::list<PP_Node *>::iterator i = children.begin();
	cout << ToString(etype);
}

void PP_Expression::PrintProc(ofstream *fstr) 
{
	std::list<PP_Node *>::iterator i = children.begin();
	switch (etype) 
	{
	case NOT:
		(*fstr) << "(" << ToString(etype);
		(*i)->PrintProc(fstr);
		(*fstr) << ")";
		break;
	case AND:
		(*fstr) << "(";
		(*i++)->PrintProc(fstr);
		(*fstr) << ToString(etype);
		(*i)->PrintProc(fstr);
		(*fstr) << ")";
		break;
	case OR:
		(*fstr) << "(";
		(*i++)->PrintProc(fstr);
		(*fstr) << ToString(etype);
		(*i)->PrintProc(fstr);
		(*fstr) << ")";
		break;
	}
}

bool PP_Expression::Compare(PP_Node *element, bool *val)
{
	return (this->GetLabel() == element->GetLabel());
}

bool PP_Expression::Satisfy(std::list<BBinders *> *bb_list) 
{
	std::list<PP_Node *>::iterator i = children.begin();
	switch (etype) 
	{
	case NOT:
		return !( (*i)->Satisfy(bb_list) );
	case AND:
		return ( (*i++)->Satisfy(bb_list) && (*i)->Satisfy(bb_list) );
	case OR:
		return ( (*i++)->Satisfy(bb_list) || (*i)->Satisfy(bb_list) );
	default:
		return false;
	}
}


/////////////////////////////////////////////////////////////////////
// PP_ITHEN METHODS DEFINITION
/////////////////////////////////////////////////////////////////////

PP_IfThen::PP_IfThen()
	:PP_Node(PP_IFTHEN) {}

PP_IfThen::~PP_IfThen() {}

void PP_IfThen::PrintV() 
{
	std::list<PP_Node *>::iterator i = children.begin();
	cout << "if ";
	(*i++)->PrintV();
	cout << " then ";
	(*i)->PrintV();
	cout << " endif";
}

void PP_IfThen::PrintProc(ofstream *fstr) 
{
	std::list<PP_Node *>::iterator i = children.begin();
	(*fstr) << "if ";
	(*i++)->PrintProc(fstr);
	(*fstr) << " then ";
	(*i)->PrintProc(fstr);
	(*fstr) << " endif";
}

bool PP_IfThen::Compare(PP_Node *element, bool *val) 
{
	return (this->GetLabel() == element->GetLabel());
}

void PP_IfThen::UpdateState(Entity *e, const string& label) 
{
	if ( ((PP_Expression *)children.front())->Satisfy(e->GetBindersList()) )
		children.back()->UpdateState(e,label);
}

void PP_IfThen::Reduction(PP_Node **root, PP_Node *child, PP_Node *new_child)
{
	// prima stacco il figlio dall'IFTHEN
	this->children.pop_back();
	// se sono uguali sotto avevo un parallelo
	if ( child != new_child )
	{
		delete(child);
	}

	if ( this == (*root) )
	{
		new_child->SetParent(NULL);
		(*root) = new_child;
		delete(this);
	}
	else
	{
		parent->Reduction(root,this,new_child);
	}
}
