#ifndef PI_PROCESS_H
#define PI_PROCESS_H

#include <cstdlib>
#include <string>
#include <list>
#include <functional>
#include "Define.h"
//#include "Environment.h"
#include "Error_Manager.h"
#include "Entity.h"
#include "Monomolecular.h"

using namespace std;


//////////////////////////////////////////////////////
// PP_NODE CLASS
//////////////////////////////////////////////////////

class PP_Node 
{
protected:
	NodeType category;
	PP_Node *parent;
	string label;
	list<PP_Node *> children;
public:
	//PP_Node();
	PP_Node(NodeType p_category);

	void InsertChild(PP_Node *child);
	void Print(int space);
	void SetParent(PP_Node *element);
	void RedProcess(PP_Node **root, bool is_sum, string *name);

	bool Equal(PP_Node *element);

	list<PP_Node *> *GetChildren();
	PP_Node* GetChildren(size_t pos);
	void SetChildren(size_t pos, PP_Node* child);

	NodeType GetCategory();

	PP_Node *GetParent();
	PP_Node *GetCopy();
	PP_Node *GetCopyRed(PP_Node *ptr1, PP_Node *ptr2, PP_Node **new_ptr1, PP_Node **new_ptr2);
	

	friend void Order(PP_Node **root);
	friend void OrderR(bool guard, PP_Node **current);
	friend void Implosion(PP_Node **element);
	friend void EliminateNil(PP_Node **element);

	virtual ~PP_Node();
	virtual PP_Node *IsReplication(PP_Node *action);
	virtual void Reduction(PP_Node **root, PP_Node *child, PP_Node *new_child);
	virtual void Compress();
	virtual void Renaming(list<Sub> *sub_list);
	virtual void RLabel();
	virtual void DecBN();
	virtual void IncBN();
	virtual	void UpdateState(Entity *e, const string& label);
	virtual void PrintV();
	virtual void PrintProc(ofstream *fstr);
	virtual string GetLabel();
	virtual string NameLess(int index);
	virtual const string& CLabel();
	virtual bool Compare(PP_Node *element,bool *val);	
	virtual bool Renaming(const string& old_name, const string& new_name);

	virtual bool Satisfy(std::list<BBinders *> *bb_list) { return true; }
	virtual PP_Node* Clone() = 0;
	virtual PP_Node* DeepClone() 
	{ 
		PP_Node* node = Clone();
		list<PP_Node*>::iterator it;
		for (it = this->children.begin(); it != this->children.end(); ++it)
		{
			node->InsertChild((*it)->DeepClone());
		}
		return node;
	}

	size_t ChildrenCount() { return children.size(); }
};



//////////////////////////////////////////////////////
// PP_NIL CLASS
//////////////////////////////////////////////////////

class PP_Nil : public PP_Node {
public:
	PP_Nil();
	~PP_Nil();

	void PrintProc(ofstream *fstr);
   virtual PP_Node* Clone() { return new PP_Nil(); } 
};



//////////////////////////////////////////////////////
// PP_SEQ CLASS
//////////////////////////////////////////////////////

class PP_Seq : public PP_Node 
{
public:
   PP_Seq();
   PP_Seq(PP_Node *action, PP_Node *process);   

	void Renaming(list<Sub> *sub_list);
	void UpdateState(Entity *e, const string& label);
	void PrintProc(ofstream *fstr);

	string NameLess(int index);
   virtual PP_Node* Clone() 
   { 
      return new PP_Seq(); 
   }

   virtual PP_Node* DeepClone() 
   { 
      PP_Node* node = new PP_Seq(); 
      list<PP_Node*>::iterator it;
      for (it = this->children.begin(); it != this->children.end(); ++it)
      {
         node->InsertChild((*it)->DeepClone());
      }
      assert(node->ChildrenCount() == this->ChildrenCount());
      return node;
   }
};



//////////////////////////////////////////////////////
// PP_REPLICATION CLASS
//////////////////////////////////////////////////////

class PP_Replication : public PP_Node 
{
public:
   PP_Replication();
   PP_Replication(PP_Node *name, PP_Node *action);

	void Renaming(list<Sub> *sub_list);
	void UpdateState(Entity *e, const string& label);
	void PrintProc(ofstream *fstr);
	
	string NameLess(int index);
	
	PP_Node *IsReplication(PP_Node *action);
	PP_Node *Expand();

   virtual PP_Node* Clone() { return new PP_Replication(); }
};



//////////////////////////////////////////////////////
// PP_PARALLEL CLASS
//////////////////////////////////////////////////////

class PP_Parallel : public PP_Node {
public:
	PP_Parallel();
	~PP_Parallel();
	
	void UpdateState(Entity *e, const string& label);
	void Compress();
	void PrintProc(ofstream *fstr);
	void Reduction(PP_Node **root, PP_Node *child, PP_Node *new_child);

   PP_Node *IsReplication(PP_Node *action);

   virtual PP_Node* Clone() { return new PP_Parallel(); }
};



//////////////////////////////////////////////////////
// PP_CHOICE CLASS
//////////////////////////////////////////////////////

class PP_Choice : public PP_Node {
public:
	PP_Choice();
	~PP_Choice();

	void UpdateState(Entity *e, const string& label);
	void Compress();
	void PrintProc(ofstream *fstr);
	void Reduction(PP_Node **root, PP_Node *child, PP_Node *new_child);

   virtual PP_Node* Clone() { return new PP_Choice(); }     
};



//////////////////////////////////////////////////////
// PP_TAU CLASS
//////////////////////////////////////////////////////

class PP_Tau : public PP_Node {
private:
	string rate;
public:
	PP_Tau(const string& p_rate);
	PP_Tau(const PP_Tau& element);
	
	~PP_Tau();
	
	void UpdateState(Entity *e, const string& label);
	void PrintV();
	void PrintProc(ofstream *fstr);

	bool Compare(PP_Node *element,bool *val);

   const string& GetRate() { return rate; }
   void SetRate(const string& p_rate) { rate = p_rate; }

	string GetLabel();

   virtual PP_Node* Clone() { return new PP_Tau(*this); }
};


//////////////////////////////////////////////////////
// PP_DIE CLASS
//////////////////////////////////////////////////////

class PP_Die : public PP_Node {
private:
	string rate;
public:
	PP_Die(const string& p_rate);
	PP_Die(const PP_Die& element);
	
	~PP_Die();
	
	void UpdateState(Entity *e, const string& label);
	void PrintV();
	void PrintProc(ofstream *fstr);

	bool Compare(PP_Node *element,bool *val);

   const string& GetRate() { return rate; }
   void SetRate(const string& p_rate) { rate = p_rate; }

	string GetLabel();

   virtual PP_Node* Clone() { return new PP_Die(*this); }
};


//////////////////////////////////////////////////////
// PP_CHANGE CLASS
//////////////////////////////////////////////////////

class PP_Change : public PP_Node {
private:
	string rate;
	string subject;
	string type;
public:
	PP_Change(const string& p_rate,const string& p_subject,const string& p_type);
	PP_Change(const PP_Change& element);
	
	~PP_Change();
	
	void UpdateState(Entity *e, const string& label);
	void PrintV();
	void PrintProc(ofstream *fstr);
	const string& GetSubject() { return subject; }
	const string& GetType() { return type; }

   void SetSubject(const string& p_subject) { subject = p_subject; }
   void SetType(const string& p_type) { type = p_type; }

   const string& GetRate() { return rate; }
   void SetRate(const string& p_rate) { rate = p_rate; }

	void DecBN();
	void IncBN();
	bool Renaming(const string& old_name, const string& new_name);
	bool Compare(PP_Node *element,bool *val);

	string GetLabel();

   virtual PP_Node* Clone() { return new PP_Change(*this); }
};


//////////////////////////////////////////////////////
// PP_INPUT CLASS
//////////////////////////////////////////////////////

class PP_Input : public PP_Node {
private:
	string subject;
	string object;
public:
	PP_Input(const string& p_subject, const string& p_object);
	PP_Input(const PP_Input& element);

	~PP_Input();

	void DecBN();
	void IncBN();
	void Renaming(list<Sub> *sub_list);
	void UpdateState(Entity *e, const string& label);
	void PrintV();
	void PrintProc(ofstream *fstr);

	const string& GetSubject();
   void SetSubject(const string& p_subject) { subject = p_subject; }

   const string& GetObject();
   void SetObject(const string& p_object) { object = p_object; }

	string NameLess(int index);
	string GetLabel();

	bool Compare(PP_Node *element,bool *val);
	bool Renaming(const string& old_name, const string& new_name);	
	
	virtual PP_Node* Clone() { return new PP_Input(*this); }
};



//////////////////////////////////////////////////////
// PP_OUTPUT CLASS
//////////////////////////////////////////////////////

class PP_Output : public PP_Node {
private:
	string subject;
	string object;
public:
	PP_Output(const string& p_subject, const string& p_object);
	PP_Output(const PP_Output& element);

	~PP_Output();

	void DecBN();
	void IncBN();
	void UpdateState(Entity *e, const string& label);
	void PrintV();
	void Renaming(list<Sub> *sub_list);
	void PrintProc(ofstream *fstr);

	const string& GetSubject();
   void SetSubject(const string& p_subject) { subject = p_subject; }

   const string& GetObject();
   void SetObject(const string& p_object) { object = p_object; }

	string GetLabel();

	bool Compare(PP_Node *element,bool *val);
	bool Renaming(const string& old_name, const string& new_name);
	bool ObjectBinder(list<BBinders *> *bb_list);

	virtual PP_Node* Clone() { return new PP_Output(*this); }
};



//////////////////////////////////////////////////////
// PP_EXPOSE CLASS
//////////////////////////////////////////////////////

class PP_Expose : public PP_Node {
private:
	string object;
	string type;
	string rate;
	string object_rate;
public:
	PP_Expose(const string& p_object, const string& p_type, const string& p_rate, const string& p_object_rate);
	PP_Expose(const PP_Expose& element);

	~PP_Expose();

	void DecBN();
	void IncBN();
	void Renaming(list<Sub> *sub_list);
	void UpdateState(Entity *e, const string& label);
	void PrintV();
	void PrintProc(ofstream *fstr);
	
	const string& GetObject();
	const string& GetType();
	const string& GetRate();
	const string& GetObjectRate();
	
   void SetObject(const string& p_object) { object = p_object; }
   void SetType(const string& p_type) { type = p_type; }

   void SetRate(const string& p_rate) { rate = p_rate; }
   void SetObjectRate(const string& p_rate) { object_rate = p_rate; }
	
   string NameLess(int index);
	string GetLabel();

	bool Renaming(const string& old_name, const string& new_name);
	bool Compare(PP_Node *element,bool *val);
	
	virtual PP_Node* Clone() { return new PP_Expose(*this); }
};



//////////////////////////////////////////////////////
// PP_HIDE CLASS
//////////////////////////////////////////////////////

class PP_Hide : public PP_Node {
private:
	string object;
	string rate;
public:
	PP_Hide(const string& p_object,const string& p_rate);
	PP_Hide(const PP_Hide& element);

	~PP_Hide();

	void DecBN();
	void IncBN();
	void Renaming(list<Sub> *sub_list);
	void UpdateState(Entity *e, const string& label);
	void PrintV();
	void PrintProc(ofstream *fstr);

	string GetLabel();
	
	const string& GetObject();
   void SetObject(const string& p_object) { object = p_object; }

   const string& GetRate() { return rate; }
   void SetRate(const string& p_rate) { rate = p_rate; }

	bool Renaming(const string& old_name, const string& new_name);
	bool Compare(PP_Node *element, bool *val);
	
   virtual PP_Node* Clone() { return new PP_Hide(*this); }    
};



//////////////////////////////////////////////////////
// PP_UNHIDE CLASS
//////////////////////////////////////////////////////

class PP_Unhide : public PP_Node {
private:
	string object;
	string rate;
public:
	PP_Unhide(const string& p_object,const string& p_rate);
	PP_Unhide(const PP_Unhide& element);

	~PP_Unhide();

	void DecBN();
	void IncBN();
	void Renaming(list<Sub> *sub_list);
	void UpdateState(Entity *e, const string& label);
	void PrintV();
	void PrintProc(ofstream *fstr);

	const string& GetObject();
   void SetObject(const string& p_object) { object = p_object; }

   const string& GetRate() { return rate; }
   void SetRate(const string& p_rate) { rate = p_rate; }

	string GetLabel();

	bool Compare(PP_Node *element, bool *val);
	bool Renaming(const string& old_name, const string& new_name);

   virtual PP_Node* Clone() { return new PP_Unhide(*this); }    
};


//////////////////////////////////////////////////////
// PP_ATOM CLASS
//////////////////////////////////////////////////////

class PP_Atom : public PP_Node {
private:
	string bsubject;
	string btype;
	BinderState bstate;
public:
	PP_Atom(const string& p_bsubject, const string& p_btype, const BinderState& p_bstate);
	PP_Atom(const PP_Atom& element);
	~PP_Atom();

	bool Renaming(const string& old_name, const string& new_name);
	void PrintV();
	void PrintProc(ofstream *fstr);

   const string& GetSubject() { return bsubject; }
   const string& GetType() { return btype; }
   void SetSubject(const string& p_bsubject) { bsubject = p_bsubject; }
   void SetType(const string& p_btype) { btype = p_btype; }

	string GetLabel();
	bool Satisfy(std::list<BBinders *> *bb_list);

	bool Compare(PP_Node *element, bool *val);

   virtual PP_Node* Clone() { return new PP_Atom(*this); }    
};

//////////////////////////////////////////////////////
// PP_EXPRESSION CLASS
//////////////////////////////////////////////////////

class PP_Expression : public PP_Node {
private:
	ExpressionType etype;
public:
	PP_Expression(const ExpressionType& p_etype);
	PP_Expression(const PP_Expression& element);
	~PP_Expression();

	void PrintV();
	void PrintProc(ofstream *fstr);

	bool Satisfy(std::list<BBinders *> *bb_list);

	bool Compare(PP_Node *element, bool *val);

   virtual PP_Node* Clone() { return new PP_Expression(*this); }    
};

//////////////////////////////////////////////////////
// PP_IFTHEN CLASS
//////////////////////////////////////////////////////

class PP_IfThen : public PP_Node {
public:
	PP_IfThen();
	~PP_IfThen();

	void PrintV();
	void PrintProc(ofstream *fstr);
	void UpdateState(Entity *e, const string& label);
	void Reduction(PP_Node **root, PP_Node *child, PP_Node *new_child);

	bool Compare(PP_Node *element, bool *val);

   virtual PP_Node* Clone() { return new PP_IfThen(); }
};

//////////////////////////////////////////////////////
// PP_TEMPLATE_NAME CLASS
//////////////////////////////////////////////////////
class PP_Template_Name : public PP_Node
{
private:
   string id;
   EntityType entity_type;
public:
   PP_Template_Name(const string& p_id, EntityType p_entity_type)
      : PP_Node(PP_TEMPLATE_NAME)
   {
      id = p_id;
      entity_type = p_entity_type;
   }

   PP_Template_Name(const PP_Template_Name& other)
      :PP_Node(PP_TEMPLATE_NAME)
   {
      id = other.id;
	   entity_type = other.entity_type;
   }

   const string& GetId() { return id; }
   EntityType GetEntityType() { return entity_type; }

   void PropagateParent(PP_Node* instatiated_node) const
   {
      instatiated_node->SetParent(parent);
   }

   virtual PP_Node* Clone() { return new PP_Template_Name(*this); } 
};

#endif

