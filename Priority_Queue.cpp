
#include <cmath>
#include <iostream>
#include <cassert>
#include "Define.h"
#include "Priority_Queue.h"

//#include <queue>


using namespace std;

/////////////////////////////////////////////////////////////////
// CLASS PRIORITY_QUEUE METHODS DEFINITION
/////////////////////////////////////////////////////////////////

//
Priority_Queue::Priority_Queue() {}

//
Priority_Queue::~Priority_Queue() {}

//
void Priority_Queue::Insert(const PQ_Elem& elem) 
{
   queue.push_back(elem);
   queue.back().SetPosInReference(queue.size() -1);
}

inline void Priority_Queue::swap(PQ_Elem& elem1, PQ_Elem& elem2) 
{
   elem1.swap(elem2);
}

PQ_Elem& Priority_Queue::GetElem(size_t index)
{
   return queue[index];
}

void Priority_Queue::Adjust(size_t old_index, double new_time)
{
   assert (new_time >= 0.0);
   //rate == 0 -> time = HUGE_VAL
   assert ((queue[old_index].GetRef()->action->GetActualRate() > 0.0) || (new_time == HUGE_VAL));
   queue[old_index].SetTime(new_time);
   size_t new_index = adjust_heap(old_index);
   
   assert(queue[new_index].GetTime() == new_time);
}

//position is 1-based
inline size_t Priority_Queue::adjust_heap(size_t position)
{
  size_t test_node = position;

  for (size_t idx = (test_node - 1) / 2; 0 < test_node && queue[idx] > queue[test_node]; idx = (test_node - 1) / 2)
  {	
     // move Hole up to parent
     swap(queue[test_node], queue[idx]);
	  test_node = idx;
  }

  if (test_node != position)
	  return test_node;

   test_node = position;
   size_t idx2 = 2 * position + 2;
   size_t bottom = queue.size();

	for (; idx2 < bottom; idx2 = 2 * idx2 + 2)
	{	
      // move Hole down
		if (queue[idx2 - 1] < queue[idx2])
			--idx2;
      if (queue[test_node] > queue[idx2])		
      {
         //double prev_test_node_time = queue[test_node].time;
         //double prev_idx2_time = queue[idx2].time;
         
         swap(queue[test_node], queue[idx2]);
         test_node = idx2;
      }
      else
         break;
	}

	if (idx2 == bottom)		
   {	
      // only child at bottom, move Hole down to it
	  if (queue[bottom - 1] < queue[test_node])
	  {
		  swap(queue[test_node], queue[bottom - 1]);      
        return (bottom - 1);
	  }
	}
	
   
   //assert(CheckOrder());

  return test_node;
}

std::vector<PQ_Elem> Priority_Queue::Linerize()
{
   std::vector<PQ_Elem> retval;

   size_t test_node = 0;
	//size_t top = position;
	size_t idx = 2;
   size_t bottom = queue.size();

	for (; idx < bottom; idx = 2 * idx + 2)
	{	
      // move _Hole down
		if (queue[idx - 1] < queue[idx])
			--idx;
      assert (!(queue[test_node] > queue[idx]));
      retval.push_back(queue[test_node]);
      //{
      //   swap(queue[test_node], queue[idx]);
         test_node = idx;
      //}
      //else
      //   break;
	}
   return retval;
}

bool Priority_Queue::CheckOrder()
{
   std::vector<PQ_Elem> linear = Linerize();
   for (size_t i = 1; i < linear.size(); ++i)
   {
      assert(!(linear[i-1].GetTime() > linear[i].GetTime()));
      if (linear[i-1].GetTime() > linear[i].GetTime())
         return false;      
   }
   return true;
}


//Select the next action
bool Priority_Queue::SS(Action& min_action) 
{
   if ( queue.empty() ) 
   {
	   return false;
   }
   else 
   {
	   const PQ_Elem& first_elem = queue.front();

      min_action.first = first_elem.GetRef()->action;
      min_action.second = first_elem.GetTime();      
   }
   return true;
}


void Priority_Queue::Print() {
	std::vector<PQ_Elem>::iterator i;
	std::vector<PQ_Elem> a;
	a = Linerize();
	for(i=a.begin();i!=a.end();i++)
		cout << (*i).GetTime() << " ";
	cout << endl << endl;
	/*priority_queue<PQ_Elem>::iterator i;
	for(i=queue.begin();i!=queue.end();i++) {
		cout << "[" << (*i)->time;
		if ((*i)->ref->type == MONO) 
			cout << ", " << ((Entity *)((*i)->ref->action))->GetId() <<  "] ";
		else cout << "] ";
	}*/
}
