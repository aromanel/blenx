
#ifndef PRIORITY_QUEUE_INCLUDED
#define PRIORITY_QUEUE_INCLUDED

#include "Map_Elem.h"
#include <vector>

typedef std::pair<Element *,double> Action;

class PQ_Elem 
{
private:
   double time;
   Map_Elem* ref;

public:
   PQ_Elem()
   {
      ref = NULL;
      time = 0.0;
   }

   PQ_Elem(Map_Elem* map_elem, double next_time) 
   {
      ref = map_elem;
      time = next_time;
   }

	bool operator<(const PQ_Elem& other) const
	{
		return (this->time < other.time);
	}

	bool operator>(const PQ_Elem& other) const
	{
		return (this->time > other.time);
	}

   void SetPosInReference(size_t pos)
   {
      ref->pq_index = pos;
   }

   const Map_Elem* GetRef() const
   {
      return ref;
   }

   double GetTime() const
   {
      return time;
   }

   void SetTime(double value)
   {
      time = value;
   }

   void swap(PQ_Elem& other)
   {
      Map_Elem* ref_tmp = other.ref;
      double time_tmp = other.time;
      size_t index1_tmp = this->ref->pq_index;
      size_t index2_tmp = other.ref->pq_index;
      
      other.ref = this->ref;
      other.time = this->time;
      other.ref->pq_index = index2_tmp;

      this->time = time_tmp;
      this->ref = ref_tmp;
      this->ref->pq_index = index1_tmp;
   }
};


class Priority_Queue 
{
private:
	std::vector<PQ_Elem> queue;
	size_t adjust_heap(size_t position);
	void swap(PQ_Elem& elem1, PQ_Elem& elem2);

public:
	Priority_Queue();
	~Priority_Queue();
   double TopTime() const 
   { 
      if (queue.empty())
         return HUGE_VAL;
      return queue.front().GetTime(); 
   }
	void Insert(const PQ_Elem& elem);
	void Adjust(size_t old_index, double new_time);      
	PQ_Elem& GetElem(size_t index);
	bool SS(Action& action);
	void Print();

	std::vector<PQ_Elem> Linerize();
	bool CheckSanity();
	bool CheckOrder();
};

#endif //PRIORITY_QUEUE_INCLUDED

