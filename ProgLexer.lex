%{
 #include <iostream>
 #include <string>
 #include "ProgParser.h"
 #include "ProgParser.hpp"
 #include "Error_Manager.h"

 using namespace std;
 
 #ifdef _MSC_VER 
 #pragma warning (push,0)
 #pragma warning( disable : 4505 )
 #endif
 
 #define YY_USER_ACTION { yylloc->first_line = yylloc->last_line = yylineno; yylloc->last_column=yycolumn+yyleng-1; yylloc->first_column=yycolumn;yycolumn+=yyleng; }


%}


%option prefix="prog_"
%option nounistd
%option noyywrap
%option never-interactive
%option stack
%option bison-bridge
%option bison-locations
%option reentrant
%option yylineno

/*regular definitions*/

delim [ \t]
fline (\r\n?|\n)
NotEnd [^\r\n]
sb {delim}+

DLetter [a-z]
ULetter [A-Z]
Digit [0-9]
Exp [Ee][+\-]?{Digit}+

id ({DLetter}|{ULetter}|_)({ULetter}|{DLetter}|{Digit}|_)*

decimal {Digit}+
real1 {Digit}+{Exp}
real2 {Digit}*"."{Digit}+({Exp})?
real3 {Digit}+"."{Digit}*({Exp})?

CmntStrt     \/\*
CmntEnd      \*\/
CmntContent  [^\*\n\r]*

%x CMMNT

%%

{sb}    {  }
{fline} {  }

^{CmntStrt}{CmntContent}\**          { yy_push_state(CMMNT, yyscanner); }
^{CmntStrt}{CmntContent}\**{CmntEnd} { }

<CMMNT>{CmntContent}\**              { }
<CMMNT>{CmntContent}\**{CmntEnd}     { yy_pop_state(yyscanner); }
\/\/{NotEnd}*                        { }

"<"			{ return(LAOPEN); }
">"			{ return(LACLOSE); }
"{"			{ return(LGOPEN); }
"}"			{ return(LGCLOSE); }
"|"			{ return(LPARALLEL); }
"||"		{ return(LBPARALLEL); }
"rep"		{ return(LBANG); }
"!"			{ return(LEM);}
"?"			{ return(LQM); }
"("			{ return(LPOPEN); }
")"			{ return(LPCLOSE); }
"+"			{ return(LCHOICE); }
"^"			{ return(LRESTRICTION); }
"["			{ return(LSOPEN); }
"]"			{ return(LSCLOSE); }
"."			{ return(LDOT); }
","			{ return(LCOMMA); }
":"			{ return(LDDOT); }
";"			{ return(LDOTCOMMA); }
"="			{ return(LEQUAL); }
"!="        { return(LNEQUAL); }
"#"			{ return(LBB); }
"delay"   	{ return(LTAU); }
"if"        { return(LIF); }
"endif"		{ return(LENDIF); }
"then"      { return(LTHEN); }
"and"       { return(LAND); }
"or"        { return(LOR); }
"not"		   { return(LNOT); }
"/"			{ return(LBAR); }
"->"        { return(LLEFTARROW); }
"<-"        { return(LRIGHTARROW); }
"changed"   { return(LCHANGED); }

"when"      { return (LWHEN); }
"inherit"   { return (LINHERIT); }
"new"       { return (LNEW); }
"split"     { return (LSPLIT); }
"delete"    { return (LDELETE); }
"join"      { return (LJOIN); }
"update"    { return (LUPDATE); }
"#h"	   	{ return(LBBH); }
"<<"	   	{ return(LDAOPEN); }
">>"	   	{ return(LDACLOSE); }
"%%"	   	{ return(LDELIM); }
"let"		   { return(LLET); }
"nil"		   { return(LNIL); }
"Nil"		   { return(LBNIL); }
"steps"		{ return(LSTEPS); }
"step"		{ return(LSTEP); }
"delta"		{ return(LDELTA); }
"time"		{ return(LTIME); }
"timespan"	{ return(LTIMESPAN); }
"expose" 	{ return(LEXPOSE); }
"hide"		{ return(LHIDE); }
"unhide" 	{ return(LUNHIDE); }
"run"		   { return(LRUN); }
"binder"		{ return(LTYPE); }
"sort"		{ return(LTYPE); }
"pproc"		{ return(LPIPROCESS); }
"bproc"		{ return(LBETAPROCESS); }
"rate"      { return(LRATE); }
"identity"	{ return(LIDENTITY); }
"ch"		   { return(LCHANGE); }
"die"		   { return(LDIE); }
"complex"   { return(LMOLECULE); }
"prefix"    { return(LPREFIX); }
"template"	{ return(LTEMPLATE); }
"name"		{ return(LNAME); }
"unhidden"	{ return(LSTATEUNHIDE); }
"hidden"	   { return(LSTATEHIDE); }
"bound"		{ return(LSTATEBOUND); } 

"HIDE"		{ return(LRHIDE); }
"UNHIDE"	   { return(LRUNHIDE); }
"EXPOSE"	   { return(LREXPOSE); }
"BASERATE"	{ return(LBASERATE); }
"CHANGE"    {return (LRCHANGE); }

"normal"	{ return(LDIST_NORMAL); }
"gamma"		{ return(LDIST_GAMMA); }
"hyperexp"	{ return(LDIST_HYPEREXP); }


"inf"		{ 
             string * new_string = new string(yytext);
			    yylval->ptr_string = new_string;
    			 return(LINF); 
			}

{id}		{ 
			 string * new_string = new string(yytext);
			 yylval->ptr_string = new_string;
			 return(LID);
			}

{decimal}	{
			 string * new_string = new string(yytext);
			 yylval->ptr_string = new_string;
			 return(LDECIMAL);
			}

{real1} {
			 string * new_string = new string(yytext);
			 yylval->ptr_string = new_string;
			 return(LREAL);
        }
        
{real2} {
			 string * new_string = new string(yytext);
			 yylval->ptr_string = new_string;
			 return(LREAL);
        }
        
{real3} {
			 string * new_string = new string(yytext);
			 yylval->ptr_string = new_string;
			 return(LREAL);
        }

.       { 
			 Error_Manager::PrintError(Pos(yylloc->first_line, yylloc->first_column, ""), "(Failed recognition) token %s not admitted.", yytext);
			 yyterminate();
        }
%%

 #ifdef _MSC_VER 
 #pragma warning (pop)
 #endif
