/* A Bison parser, made by GNU Bison 2.1.  */

/* Skeleton parser for Yacc-like parsing with Bison,
   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

/* Written by Richard Stallman by simplifying the original so called
   ``semantic'' parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.1"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 1

/* Using locations.  */
#define YYLSP_NEEDED 1

/* Substitute the variable and function names.  */
#define yyparse prog_parse
#define yylex   prog_lex
#define yyerror prog_error
#define yylval  prog_lval
#define yychar  prog_char
#define yydebug prog_debug
#define yynerrs prog_nerrs
#define yylloc prog_lloc

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     LID = 258,
     LDECIMAL = 259,
     LREAL = 260,
     LINF = 261,
     LSTEPS = 262,
     LSTEP = 263,
     LAOPEN = 264,
     LACLOSE = 265,
     LGOPEN = 266,
     LGCLOSE = 267,
     LNIL = 268,
     LBNIL = 269,
     LPARALLEL = 270,
     LPOPEN = 271,
     LPCLOSE = 272,
     LCHOICE = 273,
     LRESTRICTION = 274,
     LSOPEN = 275,
     LSCLOSE = 276,
     LEQUAL = 277,
     LNEQUAL = 278,
     LDOT = 279,
     LDDOT = 280,
     LCOMMA = 281,
     LBB = 282,
     LBBH = 283,
     LDOTCOMMA = 284,
     LDAOPEN = 285,
     LDACLOSE = 286,
     LLET = 287,
     LBASERATE = 288,
     LREXPOSE = 289,
     LRHIDE = 290,
     LRUNHIDE = 291,
     LDELIM = 292,
     LTAU = 293,
     LDELTA = 294,
     LTIME = 295,
     LTIMESPAN = 296,
     LCHANGE = 297,
     LDIE = 298,
     LRCHANGE = 299,
     LRATE = 300,
     LQM = 301,
     LEM = 302,
     LSTATEUNHIDE = 303,
     LSTATEHIDE = 304,
     LSTATEBOUND = 305,
     LWHEN = 306,
     LINHERIT = 307,
     LNEW = 308,
     LSPLIT = 309,
     LDELETE = 310,
     LJOIN = 311,
     LLEFTARROW = 312,
     LRIGHTARROW = 313,
     LCHANGED = 314,
     LUPDATE = 315,
     LMIN = 316,
     LIF = 317,
     LTHEN = 318,
     LAND = 319,
     LOR = 320,
     LNOT = 321,
     LBAR = 322,
     LP1 = 323,
     LENDIF = 324,
     LP2 = 325,
     LB1 = 326,
     LB2 = 327,
     LFJOIN = 328,
     LFSPLIT = 329,
     LBOTTOM = 330,
     LIDENTITY = 331,
     LBPARALLEL = 332,
     LTYPE = 333,
     LPIPROCESS = 334,
     LBETAPROCESS = 335,
     LMOLECULE = 336,
     LPREFIX = 337,
     LTEMPLATE = 338,
     LNAME = 339,
     LDIST_NORMAL = 340,
     LDIST_GAMMA = 341,
     LDIST_HYPEREXP = 342,
     LEXPOSE = 343,
     LHIDE = 344,
     LUNHIDE = 345,
     LRUN = 346,
     LBANG = 347
   };
#endif
/* Tokens.  */
#define LID 258
#define LDECIMAL 259
#define LREAL 260
#define LINF 261
#define LSTEPS 262
#define LSTEP 263
#define LAOPEN 264
#define LACLOSE 265
#define LGOPEN 266
#define LGCLOSE 267
#define LNIL 268
#define LBNIL 269
#define LPARALLEL 270
#define LPOPEN 271
#define LPCLOSE 272
#define LCHOICE 273
#define LRESTRICTION 274
#define LSOPEN 275
#define LSCLOSE 276
#define LEQUAL 277
#define LNEQUAL 278
#define LDOT 279
#define LDDOT 280
#define LCOMMA 281
#define LBB 282
#define LBBH 283
#define LDOTCOMMA 284
#define LDAOPEN 285
#define LDACLOSE 286
#define LLET 287
#define LBASERATE 288
#define LREXPOSE 289
#define LRHIDE 290
#define LRUNHIDE 291
#define LDELIM 292
#define LTAU 293
#define LDELTA 294
#define LTIME 295
#define LTIMESPAN 296
#define LCHANGE 297
#define LDIE 298
#define LRCHANGE 299
#define LRATE 300
#define LQM 301
#define LEM 302
#define LSTATEUNHIDE 303
#define LSTATEHIDE 304
#define LSTATEBOUND 305
#define LWHEN 306
#define LINHERIT 307
#define LNEW 308
#define LSPLIT 309
#define LDELETE 310
#define LJOIN 311
#define LLEFTARROW 312
#define LRIGHTARROW 313
#define LCHANGED 314
#define LUPDATE 315
#define LMIN 316
#define LIF 317
#define LTHEN 318
#define LAND 319
#define LOR 320
#define LNOT 321
#define LBAR 322
#define LP1 323
#define LENDIF 324
#define LP2 325
#define LB1 326
#define LB2 327
#define LFJOIN 328
#define LFSPLIT 329
#define LBOTTOM 330
#define LIDENTITY 331
#define LBPARALLEL 332
#define LTYPE 333
#define LPIPROCESS 334
#define LBETAPROCESS 335
#define LMOLECULE 336
#define LPREFIX 337
#define LTEMPLATE 338
#define LNAME 339
#define LDIST_NORMAL 340
#define LDIST_GAMMA 341
#define LDIST_HYPEREXP 342
#define LEXPOSE 343
#define LHIDE 344
#define LUNHIDE 345
#define LRUN 346
#define LBANG 347




/* Copy the first part of user declarations.  */
#line 4 "ProgParser.y"

  #include <iostream>
  #include <string>
  #include "Define.h"
  #include "PT_General_Node.h"
  #include "PT_Pi_Node.h"
  #include "PT_Action_Node.h"
  #include "PT_Beta_Node.h"
  #include "PT_Mol_Node.h"
  #include "PT_Event_Node.h"
  #include "PT_IfThen_Node.h"
  #include "PT_Template_Node.h"
  #include "Symbol_Table.h"
  #include "Error_Manager.h"
  #include "Environment.h"
  #include "Logger.h"
  #include "ProgParser.h"
  #include "ProgParser.hpp"
  #include "PT_GD_Node.h"
 
  using namespace std;
  
  // ProgLexer forward definitions
  typedef void* yyscan_t;
  void prog_set_in(FILE* in_str, yyscan_t scanner);
  int  prog_lex_init (yyscan_t* scanner);
  int  prog_lex_destroy (yyscan_t scanner); 
  int  prog_lex (YYSTYPE *lvalp, YYLTYPE *llocp, yyscan_t scanner);
  
  void prog_error(YYLTYPE *llocp, Environment* currentEnv, ProgParser* parser, yyscan_t scanner, char const* msg);
  
#ifdef _MSC_VER 
#pragma warning (push,0)
#pragma warning(disable: 4706)
#pragma warning(disable: 4702)
#pragma warning(disable: 4505)

#endif
  


/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 1
#endif

/* Enabling the token table.  */
#ifndef YYTOKEN_TABLE
# define YYTOKEN_TABLE 0
#endif

#if ! defined (YYSTYPE) && ! defined (YYSTYPE_IS_DECLARED)
#line 48 "ProgParser.y"
typedef union YYSTYPE {
  PTNode *tree_node;
  EventCond* event_cond_node;
  string *ptr_string;
 } YYSTYPE;
/* Line 196 of yacc.c.  */
#line 324 "ProgParser.cpp"
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif

#if ! defined (YYLTYPE) && ! defined (YYLTYPE_IS_DECLARED)
typedef struct YYLTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
} YYLTYPE;
# define yyltype YYLTYPE /* obsolescent; will be withdrawn */
# define YYLTYPE_IS_DECLARED 1
# define YYLTYPE_IS_TRIVIAL 1
#endif


/* Copy the second part of user declarations.  */


/* Line 219 of yacc.c.  */
#line 348 "ProgParser.cpp"

#if ! defined (YYSIZE_T) && defined (__SIZE_TYPE__)
# define YYSIZE_T __SIZE_TYPE__
#endif
#if ! defined (YYSIZE_T) && defined (size_t)
# define YYSIZE_T size_t
#endif
#if ! defined (YYSIZE_T) && (defined (__STDC__) || defined (__cplusplus))
# include <stddef.h> /* INFRINGES ON USER NAME SPACE */
# define YYSIZE_T size_t
#endif
#if ! defined (YYSIZE_T)
# define YYSIZE_T unsigned int
#endif

#ifndef YY_
# if YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

#if ! defined (yyoverflow) || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if defined (__STDC__) || defined (__cplusplus)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#     define YYINCLUDED_STDLIB_H
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning. */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2005 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM ((YYSIZE_T) -1)
#  endif
#  ifdef __cplusplus
extern "C" {
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if (! defined (malloc) && ! defined (YYINCLUDED_STDLIB_H) \
	&& (defined (__STDC__) || defined (__cplusplus)))
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if (! defined (free) && ! defined (YYINCLUDED_STDLIB_H) \
	&& (defined (__STDC__) || defined (__cplusplus)))
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifdef __cplusplus
}
#  endif
# endif
#endif /* ! defined (yyoverflow) || YYERROR_VERBOSE */


#if (! defined (yyoverflow) \
     && (! defined (__cplusplus) \
	 || (defined (YYLTYPE_IS_TRIVIAL) && YYLTYPE_IS_TRIVIAL \
             && defined (YYSTYPE_IS_TRIVIAL) && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  short int yyss;
  YYSTYPE yyvs;
    YYLTYPE yyls;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (short int) + sizeof (YYSTYPE) + sizeof (YYLTYPE))	\
      + 2 * YYSTACK_GAP_MAXIMUM)

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined (__GNUC__) && 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  YYSIZE_T yyi;				\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (0)
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack)					\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack, Stack, yysize);				\
	Stack = &yyptr->Stack;						\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (0)

#endif

#if defined (__STDC__) || defined (__cplusplus)
   typedef signed char yysigned_char;
#else
   typedef short int yysigned_char;
#endif

/* YYFINAL -- State number of the termination state. */
#define YYFINAL  22
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   535

/* YYNTOKENS -- Number of terminals. */
#define YYNTOKENS  93
/* YYNNTS -- Number of nonterminals. */
#define YYNNTS  42
/* YYNRULES -- Number of rules. */
#define YYNRULES  161
/* YYNRULES -- Number of states. */
#define YYNSTATES  466

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   347

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const unsigned char yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const unsigned short int yyprhs[] =
{
       0,     0,     3,     5,     7,     9,    14,    16,    24,    29,
      36,    40,    46,    56,    62,    64,    66,    70,    74,    78,
      82,    86,    90,    94,    96,    99,   107,   115,   123,   134,
     142,   150,   157,   168,   179,   190,   193,   196,   199,   202,
     205,   207,   211,   213,   215,   220,   226,   232,   234,   238,
     240,   244,   248,   252,   254,   258,   260,   262,   266,   272,
     278,   284,   290,   294,   298,   300,   302,   306,   310,   313,
     317,   323,   329,   334,   345,   354,   359,   364,   374,   382,
     386,   392,   400,   407,   412,   417,   419,   421,   426,   428,
     435,   437,   439,   445,   447,   452,   457,   462,   468,   470,
     474,   478,   480,   484,   488,   492,   496,   500,   502,   506,
     510,   514,   518,   524,   530,   536,   542,   550,   558,   566,
     568,   572,   576,   579,   583,   588,   594,   599,   604,   610,
     621,   630,   635,   640,   647,   654,   663,   670,   672,   677,
     682,   687,   696,   705,   712,   719,   723,   726,   729,   735,
     739,   745,   749,   751,   755,   765,   767,   770,   779,   784,
     786,   788
};

/* YYRHS -- A `-1'-separated list of the rules' RHS. */
static const short int yyrhs[] =
{
      96,     0,    -1,     5,    -1,     4,    -1,    94,    -1,    45,
      16,     3,    17,    -1,     6,    -1,    97,    30,    99,    31,
     100,    91,   126,    -1,    97,   100,    91,   126,    -1,    30,
      99,    31,   100,    91,   126,    -1,   100,    91,   126,    -1,
      20,     7,    22,     4,    21,    -1,    20,     7,    22,     4,
      26,    39,    22,    94,    21,    -1,    20,    40,    22,    94,
      21,    -1,     3,    -1,    14,    -1,     3,    25,    95,    -1,
      44,    25,    95,    -1,    34,    25,    95,    -1,    36,    25,
      95,    -1,    35,    25,    95,    -1,    33,    25,    95,    -1,
      99,    26,    99,    -1,   101,    -1,   101,   100,    -1,    32,
       3,    25,    79,    22,   119,    29,    -1,    32,     3,    25,
      79,    22,   118,    29,    -1,    32,     3,    25,    80,    22,
     124,    29,    -1,    32,     3,    25,    80,    22,     3,    30,
     105,    31,    29,    -1,    32,     3,    25,    81,    22,   127,
      29,    -1,    32,     3,    25,    82,    22,   106,    29,    -1,
      51,    16,   113,    17,   115,    29,    -1,    83,     3,    25,
      79,    30,   103,    31,    22,   119,    29,    -1,    83,     3,
      25,    79,    30,   103,    31,    22,   118,    29,    -1,    83,
       3,    25,    80,    30,   103,    31,    22,   124,    29,    -1,
      84,     3,    -1,    79,     3,    -1,    78,     3,    -1,    82,
       3,    -1,    45,     3,    -1,   102,    -1,   102,    26,   103,
      -1,     3,    -1,     5,    -1,     3,    30,   105,    31,    -1,
      16,     3,    26,    48,    17,    -1,    16,     3,    26,    49,
      17,    -1,   104,    -1,   104,    26,   105,    -1,   123,    -1,
     123,    24,   106,    -1,     3,    57,    94,    -1,     3,    58,
      94,    -1,   107,    -1,   107,    26,   108,    -1,     3,    -1,
     109,    -1,   109,    26,   110,    -1,    15,     3,    15,    22,
       4,    -1,    15,     3,    15,    10,     4,    -1,    15,     3,
      15,     9,     4,    -1,    15,     3,    15,    23,     4,    -1,
      40,    22,     5,    -1,     8,    22,     4,    -1,   108,    -1,
     111,    -1,   112,    64,   112,    -1,   112,    65,   112,    -1,
      66,   112,    -1,    16,   112,    17,    -1,   110,    25,   112,
      25,    95,    -1,   110,    25,   112,    25,     3,    -1,   110,
      25,   112,    25,    -1,   110,    25,   112,    25,    86,    16,
      94,    26,    94,    17,    -1,   110,    25,   112,    25,    87,
      16,   114,    17,    -1,   110,    25,    25,    95,    -1,   110,
      25,    25,     3,    -1,   110,    25,    25,    86,    16,    94,
      26,    94,    17,    -1,   110,    25,    25,    87,    16,   114,
      17,    -1,    25,   112,    25,    -1,    16,    94,    26,    94,
      17,    -1,    16,    94,    26,    94,    17,    26,   114,    -1,
      54,    16,    98,    26,    98,    17,    -1,    53,    16,     4,
      17,    -1,    55,    16,     4,    17,    -1,    53,    -1,    55,
      -1,    56,    16,    98,    17,    -1,    56,    -1,    60,    16,
       3,    26,     3,    17,    -1,    13,    -1,   120,    -1,    62,
     122,    63,   118,    69,    -1,     3,    -1,     3,    30,   105,
      31,    -1,    92,   123,    24,   119,    -1,    92,   123,    24,
     118,    -1,    62,   122,    63,   119,    69,    -1,   116,    -1,
     118,    18,   118,    -1,    16,   118,    17,    -1,   117,    -1,
     118,    15,   118,    -1,   118,    15,   119,    -1,   119,    15,
     118,    -1,   119,    15,   119,    -1,    16,   119,    17,    -1,
     123,    -1,   123,    24,   119,    -1,   123,    24,   118,    -1,
       3,    24,   119,    -1,     3,    24,   118,    -1,    16,     3,
      26,     3,    17,    -1,    16,     3,    26,    49,    17,    -1,
      16,     3,    26,    48,    17,    -1,    16,     3,    26,    50,
      17,    -1,    16,     3,    26,     3,    26,    49,    17,    -1,
      16,     3,    26,     3,    26,    48,    17,    -1,    16,     3,
      26,     3,    26,    50,    17,    -1,   121,    -1,   122,    64,
     122,    -1,   122,    65,   122,    -1,    66,   122,    -1,    16,
     122,    17,    -1,    38,    16,    95,    17,    -1,     3,    47,
      16,     3,    17,    -1,     3,    47,    16,    17,    -1,     3,
      46,    16,    17,    -1,     3,    46,    16,     3,    17,    -1,
      88,    16,    95,    26,     3,    25,    95,    26,     3,    17,
      -1,    88,    16,     3,    25,    95,    26,     3,    17,    -1,
      89,    16,     3,    17,    -1,    90,    16,     3,    17,    -1,
      89,    16,    95,    26,     3,    17,    -1,    90,    16,    95,
      26,     3,    17,    -1,    42,    16,    95,    26,     3,    26,
       3,    17,    -1,    42,    16,     3,    26,     3,    17,    -1,
      43,    -1,    43,    16,    95,    17,    -1,   125,    20,   119,
      21,    -1,   125,    20,   118,    21,    -1,    27,    16,     3,
      25,    95,    26,     3,    17,    -1,    28,    16,     3,    25,
      95,    26,     3,    17,    -1,    27,    16,     3,    26,     3,
      17,    -1,    28,    16,     3,    26,     3,    17,    -1,   125,
      26,   125,    -1,     4,     3,    -1,     3,     3,    -1,     4,
       3,    30,   105,    31,    -1,   126,    77,   126,    -1,    11,
     128,    29,   131,    12,    -1,    16,   129,    17,    -1,   130,
      -1,   130,    26,   129,    -1,    16,     3,    26,     3,    26,
       3,    26,     3,    17,    -1,   132,    -1,   132,   131,    -1,
       3,    25,     3,    22,    16,   134,    17,    29,    -1,     3,
      22,     3,    29,    -1,     3,    -1,   133,    -1,   133,    26,
     134,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const unsigned short int yyrline[] =
{
       0,   147,   147,   148,   152,   153,   167,   171,   172,   173,
     174,   179,   184,   191,   200,   201,   205,   206,   207,   208,
     209,   210,   211,   215,   216,   220,   221,   222,   223,   226,
     227,   228,   229,   231,   233,   238,   239,   240,   241,   242,
     246,   247,   251,   252,   253,   259,   260,   264,   265,   272,
     273,   277,   278,   282,   283,   287,   291,   292,   296,   297,
     298,   299,   300,   301,   302,   306,   307,   308,   309,   310,
     314,   315,   316,   317,   318,   319,   320,   321,   322,   323,
     327,   328,   332,   333,   338,   343,   344,   345,   346,   347,
     351,   352,   353,   357,   358,   359,   360,   361,   365,   367,
     368,   372,   373,   374,   375,   376,   377,   381,   382,   383,
     384,   385,   390,   391,   392,   393,   394,   395,   396,   400,
     401,   402,   403,   404,   408,   409,   410,   411,   412,   413,
     414,   415,   416,   417,   418,   419,   420,   421,   422,   426,
     427,   431,   432,   433,   434,   435,   440,   441,   470,   471,
     475,   480,   484,   485,   489,   492,   493,   497,   498,   502,
     506,   507
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals. */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "LID", "LDECIMAL", "LREAL", "LINF",
  "LSTEPS", "LSTEP", "LAOPEN", "LACLOSE", "LGOPEN", "LGCLOSE", "LNIL",
  "LBNIL", "LPARALLEL", "LPOPEN", "LPCLOSE", "LCHOICE", "LRESTRICTION",
  "LSOPEN", "LSCLOSE", "LEQUAL", "LNEQUAL", "LDOT", "LDDOT", "LCOMMA",
  "LBB", "LBBH", "LDOTCOMMA", "LDAOPEN", "LDACLOSE", "LLET", "LBASERATE",
  "LREXPOSE", "LRHIDE", "LRUNHIDE", "LDELIM", "LTAU", "LDELTA", "LTIME",
  "LTIMESPAN", "LCHANGE", "LDIE", "LRCHANGE", "LRATE", "LQM", "LEM",
  "LSTATEUNHIDE", "LSTATEHIDE", "LSTATEBOUND", "LWHEN", "LINHERIT", "LNEW",
  "LSPLIT", "LDELETE", "LJOIN", "LLEFTARROW", "LRIGHTARROW", "LCHANGED",
  "LUPDATE", "LMIN", "LIF", "LTHEN", "LAND", "LOR", "LNOT", "LBAR", "LP1",
  "LENDIF", "LP2", "LB1", "LB2", "LFJOIN", "LFSPLIT", "LBOTTOM",
  "LIDENTITY", "LBPARALLEL", "LTYPE", "LPIPROCESS", "LBETAPROCESS",
  "LMOLECULE", "LPREFIX", "LTEMPLATE", "LNAME", "LDIST_NORMAL",
  "LDIST_GAMMA", "LDIST_HYPEREXP", "LEXPOSE", "LHIDE", "LUNHIDE", "LRUN",
  "LBANG", "$accept", "number", "rate", "program", "info", "betaid",
  "rate_dec", "dec_list", "dec", "dec_temp_elem", "dec_temp_list",
  "inv_temp_elem", "inv_temp_list", "dec_sequence", "state_op",
  "state_op_list", "entity", "entity_list", "cond_atom", "cond_expression",
  "cond", "hypexp_parameter_list", "verb", "sumelem", "parelem", "sum",
  "par", "seq", "atom", "expression", "action", "betaprocess", "binder",
  "bp", "molecule", "mol_signature", "edge_list", "edge", "node_list",
  "node", "mol_binder", "mol_binder_list", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const unsigned short int yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,   323,   324,
     325,   326,   327,   328,   329,   330,   331,   332,   333,   334,
     335,   336,   337,   338,   339,   340,   341,   342,   343,   344,
     345,   346,   347
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const unsigned char yyr1[] =
{
       0,    93,    94,    94,    95,    95,    95,    96,    96,    96,
      96,    97,    97,    97,    98,    98,    99,    99,    99,    99,
      99,    99,    99,   100,   100,   101,   101,   101,   101,   101,
     101,   101,   101,   101,   101,   102,   102,   102,   102,   102,
     103,   103,   104,   104,   104,   104,   104,   105,   105,   106,
     106,   107,   107,   108,   108,   109,   110,   110,   111,   111,
     111,   111,   111,   111,   111,   112,   112,   112,   112,   112,
     113,   113,   113,   113,   113,   113,   113,   113,   113,   113,
     114,   114,   115,   115,   115,   115,   115,   115,   115,   115,
     116,   116,   116,   117,   117,   117,   117,   117,   118,   118,
     118,   119,   119,   119,   119,   119,   119,   120,   120,   120,
     120,   120,   121,   121,   121,   121,   121,   121,   121,   122,
     122,   122,   122,   122,   123,   123,   123,   123,   123,   123,
     123,   123,   123,   123,   123,   123,   123,   123,   123,   124,
     124,   125,   125,   125,   125,   125,   126,   126,   126,   126,
     127,   128,   129,   129,   130,   131,   131,   132,   132,   133,
     134,   134
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const unsigned char yyr2[] =
{
       0,     2,     1,     1,     1,     4,     1,     7,     4,     6,
       3,     5,     9,     5,     1,     1,     3,     3,     3,     3,
       3,     3,     3,     1,     2,     7,     7,     7,    10,     7,
       7,     6,    10,    10,    10,     2,     2,     2,     2,     2,
       1,     3,     1,     1,     4,     5,     5,     1,     3,     1,
       3,     3,     3,     1,     3,     1,     1,     3,     5,     5,
       5,     5,     3,     3,     1,     1,     3,     3,     2,     3,
       5,     5,     4,    10,     8,     4,     4,     9,     7,     3,
       5,     7,     6,     4,     4,     1,     1,     4,     1,     6,
       1,     1,     5,     1,     4,     4,     4,     5,     1,     3,
       3,     1,     3,     3,     3,     3,     3,     1,     3,     3,
       3,     3,     5,     5,     5,     5,     7,     7,     7,     1,
       3,     3,     2,     3,     4,     5,     4,     4,     5,    10,
       8,     4,     4,     6,     6,     8,     6,     1,     4,     4,
       4,     8,     8,     6,     6,     3,     2,     2,     5,     3,
       5,     3,     1,     3,     9,     1,     2,     8,     4,     1,
       1,     3
};

/* YYDEFACT[STATE-NAME] -- Default rule to reduce with in state
   STATE-NUM when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const unsigned char yydefact[] =
{
       0,     0,     0,     0,     0,     0,     0,     0,     0,    23,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     1,     0,     0,     0,    24,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    55,     0,
      56,     0,     0,     0,     0,     0,     0,     0,    10,     0,
       3,     2,     0,     6,     0,     4,    16,    21,    18,    20,
      19,    17,    22,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    53,    64,    65,     0,     0,     0,
       0,     0,     0,     0,     8,   147,   146,     0,    11,     0,
      13,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    68,     0,    79,     0,     0,    57,     0,
       0,    85,     0,    86,    88,     0,     0,     0,     0,     0,
       0,   149,     0,     0,     9,    93,    90,     0,     0,     0,
     137,     0,     0,     0,     0,     0,    98,   101,     0,     0,
      91,   107,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    49,    51,    52,    63,     0,    69,    62,    54,    66,
      67,    76,     0,     0,    75,    72,     0,     0,     0,     0,
       0,    31,     0,     0,     0,     0,     0,    40,     0,     0,
       0,    42,    43,     0,    47,     0,     0,     5,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   119,
       0,     0,     0,     0,     0,     0,     0,    26,     0,    25,
       0,     0,     0,     0,    27,     0,     0,     0,     0,    29,
      30,     0,     0,     0,     0,     0,     0,     0,    71,     0,
       0,    70,     0,    14,    15,     0,     0,     0,     0,    39,
      37,    36,    38,    35,     0,     0,     0,     7,     0,     0,
       0,   148,     0,   111,   110,     0,     0,     0,   100,   106,
       0,     0,     0,     0,     0,     0,   122,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   102,   103,     0,
       0,     0,    99,   104,   105,   109,   108,     0,     0,     0,
       0,     0,   145,     0,     0,   152,     0,    50,    60,    59,
      58,    61,     0,     0,     0,     0,     0,    83,     0,    84,
      87,     0,    41,     0,     0,     0,     0,    48,    12,    94,
       0,   127,     0,   126,   124,     0,     0,   138,     0,   123,
       0,     0,   120,   121,     0,     0,   131,     0,   132,     0,
      96,    95,     0,     0,     0,     0,     0,     0,     0,   140,
     139,     0,   151,     0,     0,     0,   155,     0,     0,    78,
       0,     0,     0,     0,     0,     0,     0,    44,     0,     0,
     128,   125,     0,     0,     0,     0,     0,     0,    92,    97,
       0,     0,     0,     0,     0,    28,     0,     0,     0,     0,
       0,   153,     0,     0,   150,   156,     0,     0,     0,    74,
      82,    89,    33,    32,    34,    45,    46,   136,     0,   112,
       0,   114,   113,   115,     0,     0,   133,   134,     0,     0,
     143,     0,   144,     0,     0,     0,    77,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   158,     0,
      80,    73,   135,   117,   116,   118,   130,     0,   141,   142,
       0,     0,     0,     0,     0,   159,   160,     0,    81,   129,
       0,     0,     0,   154,   161,   157
};

/* YYDEFGOTO[NTERM-NUM]. */
static const short int yydefgoto[] =
{
      -1,    55,    56,     6,     7,   235,    18,     8,     9,   177,
     178,   184,   185,   150,    74,    75,    40,    41,    76,    77,
      42,   304,   116,   136,   137,   138,   139,   140,   199,   200,
     141,   145,   146,    48,   148,   218,   294,   295,   355,   356,
     456,   457
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -300
static const short int yypact[] =
{
      46,   105,   188,    22,    17,    26,    50,   137,   -75,    74,
      76,   111,    47,   126,   133,   145,   192,   224,   117,   278,
      23,   282,  -300,   188,   185,   293,  -300,   312,   297,   208,
     208,   208,   208,   208,   208,   188,    74,   147,  -300,   134,
     288,   304,   301,   187,   248,   293,   327,   328,   255,   238,
    -300,  -300,   314,  -300,   317,  -300,  -300,  -300,  -300,  -300,
    -300,  -300,   311,   247,   318,   319,   320,   321,   251,   322,
     333,   134,   323,   134,   313,  -300,  -300,   -10,   343,   116,
     215,   324,   325,    74,   255,  -300,   326,   293,  -300,   308,
    -300,   345,   293,    15,    33,   338,    27,   297,   297,   346,
     336,    75,   347,  -300,   350,  -300,   134,   134,  -300,    35,
     151,   341,   342,   344,   348,   349,   330,   152,   152,   271,
     249,   255,   339,   351,   255,   214,  -300,    15,   353,   354,
     355,     5,   356,   357,   360,    27,  -300,  -300,    73,   139,
    -300,   358,   337,   361,   362,   334,   231,   363,   352,   259,
     359,   365,  -300,  -300,  -300,   233,  -300,  -300,  -300,   246,
     315,  -300,   367,   368,  -300,    41,   382,   244,   383,   244,
     387,  -300,   388,   389,   390,   391,   392,   340,   366,   369,
     293,   371,  -300,   393,   372,   373,   297,  -300,    15,   249,
     386,   394,   270,   103,   208,   180,   208,    16,     5,  -300,
     227,   190,   201,   205,   375,    15,    21,  -300,    15,  -300,
      15,   249,   400,   402,  -300,    15,   285,   395,   377,  -300,
    -300,    27,   403,   404,   405,   408,   297,   397,  -300,   398,
     399,  -300,   401,  -300,  -300,   396,   406,   407,   409,  -300,
    -300,  -300,  -300,  -300,   152,   410,   411,   255,   249,   412,
     249,  -300,   413,  -300,  -300,   385,   216,   245,  -300,  -300,
     414,   415,   416,   419,   417,   113,  -300,    15,     5,     5,
     420,   418,   422,   421,   423,   424,    15,   184,   431,   194,
      21,     5,   430,   184,   431,  -300,  -300,   425,   294,   296,
     263,   262,   426,   427,   432,   428,   434,  -300,  -300,  -300,
    -300,  -300,   429,   297,   436,   297,   397,  -300,   244,  -300,
    -300,   448,  -300,    15,   285,   433,   275,  -300,  -300,  -300,
     440,  -300,   441,  -300,  -300,   456,   457,  -300,   131,  -300,
      -1,    12,   261,   364,   208,   458,  -300,   459,  -300,   460,
    -300,  -300,   310,   230,   437,   208,   462,   208,   464,  -300,
    -300,   442,  -300,   395,   264,   461,   434,   297,   443,  -300,
     444,   454,   455,   463,   172,   160,   445,  -300,   465,   466,
    -300,  -300,   467,   449,    82,   468,   469,   470,  -300,  -300,
     450,   452,   471,   472,    21,  -300,   453,   473,   474,   475,
     478,  -300,   488,   490,  -300,  -300,   477,   297,   297,  -300,
    -300,  -300,  -300,  -300,  -300,  -300,  -300,  -300,   492,  -300,
     250,  -300,  -300,  -300,   493,   208,  -300,  -300,    -7,   494,
    -300,   495,  -300,   476,   479,   481,  -300,   482,   484,   487,
     489,   496,   497,   498,   483,   499,   500,   502,  -300,   491,
     485,  -300,  -300,  -300,  -300,  -300,  -300,   507,  -300,  -300,
     486,   515,   397,   503,   516,  -300,   501,   504,  -300,  -300,
     505,   515,   506,  -300,  -300,  -300
};

/* YYPGOTO[NTERM-NUM].  */
static const short int yypgoto[] =
{
    -300,   -23,   -30,  -300,  -300,  -161,     0,    13,  -300,  -300,
    -108,  -300,   -88,   196,  -300,   316,  -300,   446,  -300,   166,
    -300,  -299,  -300,  -300,  -300,  -121,  -115,  -300,  -300,  -155,
     -83,   107,   203,   -36,  -300,  -300,    72,  -300,    70,  -300,
    -300,   -34
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -1
static const unsigned short int yytable[] =
{
      57,    58,    59,    60,    61,    52,   192,   361,   237,    84,
     179,   206,   193,   151,   205,   105,    25,   206,   125,   264,
      24,   197,    26,    44,   279,    19,    38,   208,   126,    21,
     149,   127,   197,    20,   126,    62,   142,   280,   161,    50,
      51,    53,   265,   266,   228,    50,    51,    53,    39,    63,
      22,   121,   204,   128,   106,   107,   124,   129,   130,   128,
     143,   144,   378,   129,   130,   128,     1,   253,   378,   129,
     130,   198,    29,   254,   152,   153,     2,   131,     3,   164,
      54,   379,   198,   281,   277,   282,    54,   283,   205,   285,
     278,   206,   156,   284,   290,   286,   119,     4,    27,   409,
     291,   255,   207,   132,   133,   134,     3,   135,   410,   132,
     133,   134,    10,   332,   333,   132,   133,   134,   208,    68,
     259,   162,   163,   287,    69,     4,   343,   229,   230,     5,
     329,    70,    71,    28,   374,   231,   312,    68,   151,   106,
     107,   109,    69,    35,   247,    11,   330,   362,    36,    70,
      71,    30,   331,   458,   208,   340,    72,     5,    31,   342,
     315,   341,   317,   252,   260,   262,   263,    23,   209,     3,
      32,   271,   273,   275,    72,   208,   165,   268,   269,   375,
     376,   377,    73,   261,    50,    51,    53,   205,     4,   403,
     206,    12,   364,   270,    50,    51,    53,   172,   365,   205,
      73,   402,   206,   302,   272,    50,    51,    53,   274,    50,
      51,    53,    50,    51,    53,   106,   107,    33,   188,   320,
       5,    13,    14,    15,    16,    54,    64,    65,    66,    67,
     173,   174,    17,   321,   175,    54,   176,   101,   188,   103,
     190,   191,   222,   223,   189,   110,    54,   233,   322,    34,
      54,   215,   181,    54,   182,   224,   225,   216,   234,    88,
     190,   191,   323,   418,    89,   183,    81,    82,   111,   112,
     113,   114,   159,   160,    35,   115,    45,   208,   205,    83,
     358,   206,   360,   350,   349,   205,   392,   258,   206,   393,
     267,   268,   269,   384,   268,   269,    46,    47,   430,   431,
     432,    50,    51,    37,   380,   190,   191,    43,    97,    98,
     106,   107,   143,   144,    78,   386,    49,   388,    80,   345,
     346,   347,   348,   368,   369,   268,   269,   258,   206,    79,
      85,    86,    87,    91,   396,    90,   100,    35,    92,   104,
      93,    94,    95,    96,    99,   102,    38,   122,   123,   147,
     154,   155,   157,    68,   117,   118,   120,   166,   167,   171,
     168,   186,   180,   214,   169,   170,   244,   211,   187,   194,
     195,   196,   201,   202,   427,   428,   203,   212,   213,   217,
     107,   219,   210,   226,   227,   434,   232,   236,   220,   221,
     238,   239,   240,   241,   242,   243,   249,   245,   250,   276,
     246,   248,   256,   288,   251,   289,   296,   298,   299,   300,
     257,   293,   301,   303,   305,   306,   319,   297,   307,   292,
     158,   366,   308,   309,   310,   391,   395,   464,     0,   269,
     351,   324,   313,   314,   318,   311,   327,   354,   316,   336,
     338,   325,   326,   328,   335,   334,   208,   337,   206,   352,
     339,   363,   216,   359,   353,   357,   344,   370,   371,   372,
     373,   381,   382,   383,   367,   387,   385,   389,   390,   397,
     398,   399,   400,   394,   404,   408,   414,   415,     0,   419,
     401,   423,   405,   406,   407,   411,   412,   413,   416,   417,
     420,   424,   422,   425,   426,   429,   433,   435,   436,   440,
     421,   441,   437,   439,   442,   450,   443,   451,   438,   447,
     453,   452,   454,   444,   445,   446,   448,   449,   455,   460,
     459,   462,   463,     0,   108,     0,     0,   461,     0,     0,
       0,     0,     0,     0,     0,   465
};

static const short int yycheck[] =
{
      30,    31,    32,    33,    34,    28,   127,   306,   169,    45,
     118,    18,   127,    96,    15,    25,    91,    18,     3,     3,
       7,    16,     9,    23,     3,     3,     3,    15,    13,     3,
       3,    16,    16,    16,    13,    35,     3,    16,     3,     4,
       5,     6,   197,   198,     3,     4,     5,     6,    25,    36,
       0,    87,   135,    38,    64,    65,    92,    42,    43,    38,
      27,    28,    69,    42,    43,    38,    20,   188,    69,    42,
      43,    66,    25,   188,    97,    98,    30,    62,    32,   109,
      45,    69,    66,    62,   205,   206,    45,   208,    15,   210,
     205,    18,    17,   208,   215,   210,    83,    51,    22,    17,
     215,   189,    29,    88,    89,    90,    32,    92,    26,    88,
      89,    90,     7,   268,   269,    88,    89,    90,    15,     3,
      17,    86,    87,   211,     8,    51,   281,    86,    87,    83,
      17,    15,    16,    22,     3,   165,   244,     3,   221,    64,
      65,    25,     8,    26,   180,    40,   267,   308,    31,    15,
      16,    25,   267,   452,    15,   276,    40,    83,    25,   280,
     248,   276,   250,   186,   194,   195,   196,    30,    29,    32,
      25,   201,   202,   203,    40,    15,    25,    64,    65,    48,
      49,    50,    66,     3,     4,     5,     6,    15,    51,    29,
      18,     3,   313,     3,     4,     5,     6,    45,   313,    15,
      66,    29,    18,   226,     3,     4,     5,     6,     3,     4,
       5,     6,     4,     5,     6,    64,    65,    25,    24,     3,
      83,    33,    34,    35,    36,    45,    79,    80,    81,    82,
      78,    79,    44,    17,    82,    45,    84,    71,    24,    73,
      46,    47,     9,    10,    30,    79,    45,     3,     3,    25,
      45,    20,     3,    45,     5,    22,    23,    26,    14,    21,
      46,    47,    17,   384,    26,    16,    79,    80,    53,    54,
      55,    56,   106,   107,    26,    60,    91,    15,    15,    31,
     303,    18,   305,    21,    21,    15,    22,    17,    18,    25,
      63,    64,    65,    63,    64,    65,     3,     4,    48,    49,
      50,     4,     5,    25,   334,    46,    47,    25,    57,    58,
      64,    65,    27,    28,    26,   345,     4,   347,    17,    25,
      26,    25,    26,    48,    49,    64,    65,    17,    18,    25,
       3,     3,    77,    16,   357,    21,     3,    26,    91,    26,
      22,    22,    22,    22,    22,    22,     3,    39,     3,    11,
       4,    15,     5,     3,    30,    30,    30,    16,    16,    29,
      16,    22,    91,    29,    16,    16,    26,    30,    17,    16,
      16,    16,    16,    16,   397,   398,    16,    16,    16,    16,
      65,    29,    24,    16,    16,   415,     4,     4,    29,    24,
       3,     3,     3,     3,     3,     3,     3,    31,    26,    24,
      31,    30,    16,     3,    31,     3,    29,     4,     4,     4,
      16,    16,     4,    16,    16,    16,    31,   221,    17,   216,
     104,   314,    26,    17,    17,   353,   356,   461,    -1,    65,
       3,    17,    22,    22,    21,    26,    17,     3,    26,    17,
      17,    26,    26,    26,    26,    25,    15,    26,    18,    17,
      26,     3,    26,    17,    26,    26,    31,    17,    17,     3,
       3,     3,     3,     3,    31,     3,    29,     3,    26,    26,
      26,    17,    17,    12,    29,    26,    26,    25,    -1,    26,
      17,     3,    17,    17,    17,    17,    17,    17,    17,    17,
      17,     3,    17,     3,    17,     3,     3,     3,     3,    17,
      26,    17,    26,    22,    17,     3,    17,    16,    29,    26,
       3,    26,    26,    17,    17,    17,    17,    17,     3,     3,
      17,    17,    17,    -1,    78,    -1,    -1,    26,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    29
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const unsigned char yystos[] =
{
       0,    20,    30,    32,    51,    83,    96,    97,   100,   101,
       7,    40,     3,    33,    34,    35,    36,    44,    99,     3,
      16,     3,     0,    30,   100,    91,   100,    22,    22,    25,
      25,    25,    25,    25,    25,    26,    31,    25,     3,    25,
     109,   110,   113,    25,    99,    91,     3,     4,   126,     4,
       4,     5,    94,     6,    45,    94,    95,    95,    95,    95,
      95,    95,    99,   100,    79,    80,    81,    82,     3,     8,
      15,    16,    40,    66,   107,   108,   111,   112,    26,    25,
      17,    79,    80,    31,   126,     3,     3,    77,    21,    26,
      21,    16,    91,    22,    22,    22,    22,    57,    58,    22,
       3,   112,    22,   112,    26,    25,    64,    65,   110,    25,
     112,    53,    54,    55,    56,    60,   115,    30,    30,   100,
      30,   126,    39,     3,   126,     3,    13,    16,    38,    42,
      43,    62,    88,    89,    90,    92,   116,   117,   118,   119,
     120,   123,     3,    27,    28,   124,   125,    11,   127,     3,
     106,   123,    94,    94,     4,    15,    17,     5,   108,   112,
     112,     3,    86,    87,    95,    25,    16,    16,    16,    16,
      16,    29,    45,    78,    79,    82,    84,   102,   103,   103,
      91,     3,     5,    16,   104,   105,    22,    17,    24,    30,
      46,    47,   118,   119,    16,    16,    16,    16,    66,   121,
     122,    16,    16,    16,   123,    15,    18,    29,    15,    29,
      24,    30,    16,    16,    29,    20,    26,    16,   128,    29,
      29,    24,     9,    10,    22,    23,    16,    16,     3,    86,
      87,    95,     4,     3,    14,    98,     4,    98,     3,     3,
       3,     3,     3,     3,    26,    31,    31,   126,    30,     3,
      26,    31,    94,   118,   119,   105,    16,    16,    17,    17,
      95,     3,    95,    95,     3,   122,   122,    63,    64,    65,
       3,    95,     3,    95,     3,    95,    24,   118,   119,     3,
      16,    62,   118,   118,   119,   118,   119,   105,     3,     3,
     118,   119,   125,    16,   129,   130,    29,   106,     4,     4,
       4,     4,    94,    16,   114,    16,    16,    17,    26,    17,
      17,    26,   103,    22,    22,   105,    26,   105,    21,    31,
       3,    17,     3,    17,    17,    26,    26,    17,    26,    17,
     118,   119,   122,   122,    25,    26,    17,    26,    17,    26,
     118,   119,   118,   122,    31,    25,    26,    25,    26,    21,
      21,     3,    17,    26,     3,   131,   132,    26,    94,    17,
      94,   114,    98,     3,   118,   119,   124,    31,    48,    49,
      17,    17,     3,     3,     3,    48,    49,    50,    69,    69,
      95,     3,     3,     3,    63,    29,    95,     3,    95,     3,
      26,   129,    22,    25,    12,   131,    94,    26,    26,    17,
      17,    17,    29,    29,    29,    17,    17,    17,    26,    17,
      26,    17,    17,    17,    26,    25,    17,    17,   118,    26,
      17,    26,    17,     3,     3,     3,    17,    94,    94,     3,
      48,    49,    50,     3,    95,     3,     3,    26,    29,    22,
      17,    17,    17,    17,    17,    17,    17,    26,    17,    17,
       3,    16,    26,     3,    26,     3,   133,   134,   114,    17,
       3,    26,    17,    17,   134,    29
};

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */

#define YYFAIL		goto yyerrlab

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yytoken = YYTRANSLATE (yychar);				\
      YYPOPSTACK;						\
      goto yybackup;						\
    }								\
  else								\
    {								\
      yyerror (&yylloc, currentEnv, parser, scanner, YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (0)


#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#define YYRHSLOC(Rhs, K) ((Rhs)[K])
#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)				\
    do									\
      if (N)								\
	{								\
	  (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;	\
	  (Current).first_column = YYRHSLOC (Rhs, 1).first_column;	\
	  (Current).last_line    = YYRHSLOC (Rhs, N).last_line;		\
	  (Current).last_column  = YYRHSLOC (Rhs, N).last_column;	\
	}								\
      else								\
	{								\
	  (Current).first_line   = (Current).last_line   =		\
	    YYRHSLOC (Rhs, 0).last_line;				\
	  (Current).first_column = (Current).last_column =		\
	    YYRHSLOC (Rhs, 0).last_column;				\
	}								\
    while (0)
#endif


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if YYLTYPE_IS_TRIVIAL
#  define YY_LOCATION_PRINT(File, Loc)			\
     fprintf (File, "%d.%d-%d.%d",			\
              (Loc).first_line, (Loc).first_column,	\
              (Loc).last_line,  (Loc).last_column)
# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX yylex (&yylval, &yylloc, YYLEX_PARAM)
#else
# define YYLEX yylex (&yylval, &yylloc, scanner)
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (0)

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)		\
do {								\
  if (yydebug)							\
    {								\
      YYFPRINTF (stderr, "%s ", Title);				\
      yysymprint (stderr,					\
                  Type, Value, Location);	\
      YYFPRINTF (stderr, "\n");					\
    }								\
} while (0)

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yy_stack_print (short int *bottom, short int *top)
#else
static void
yy_stack_print (bottom, top)
    short int *bottom;
    short int *top;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (/* Nothing. */; bottom <= top; ++bottom)
    YYFPRINTF (stderr, " %d", *bottom);
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yy_reduce_print (int yyrule)
#else
static void
yy_reduce_print (yyrule)
    int yyrule;
#endif
{
  int yyi;
  unsigned long int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu), ",
             yyrule - 1, yylno);
  /* Print the symbols being reduced, and their result.  */
  for (yyi = yyprhs[yyrule]; 0 <= yyrhs[yyi]; yyi++)
    YYFPRINTF (stderr, "%s ", yytname[yyrhs[yyi]]);
  YYFPRINTF (stderr, "-> %s\n", yytname[yyr1[yyrule]]);
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (Rule);		\
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif



#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined (__GLIBC__) && defined (_STRING_H)
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
#   if defined (__STDC__) || defined (__cplusplus)
yystrlen (const char *yystr)
#   else
yystrlen (yystr)
     const char *yystr;
#   endif
{
  const char *yys = yystr;

  while (*yys++ != '\0')
    continue;

  return yys - yystr - 1;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined (__GLIBC__) && defined (_STRING_H) && defined (_GNU_SOURCE)
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
#   if defined (__STDC__) || defined (__cplusplus)
yystpcpy (char *yydest, const char *yysrc)
#   else
yystpcpy (yydest, yysrc)
     char *yydest;
     const char *yysrc;
#   endif
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      size_t yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

#endif /* YYERROR_VERBOSE */



#if YYDEBUG
/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yysymprint (FILE *yyoutput, int yytype, YYSTYPE *yyvaluep, YYLTYPE *yylocationp)
#else
static void
yysymprint (yyoutput, yytype, yyvaluep, yylocationp)
    FILE *yyoutput;
    int yytype;
    YYSTYPE *yyvaluep;
    YYLTYPE *yylocationp;
#endif
{
  /* Pacify ``unused variable'' warnings.  */
  (void) yyvaluep;
  (void) yylocationp;

  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  YY_LOCATION_PRINT (yyoutput, *yylocationp);
  YYFPRINTF (yyoutput, ": ");

# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  switch (yytype)
    {
      default:
        break;
    }
  YYFPRINTF (yyoutput, ")");
}

#endif /* ! YYDEBUG */
/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep, YYLTYPE *yylocationp)
#else
static void
yydestruct (yymsg, yytype, yyvaluep, yylocationp)
    const char *yymsg;
    int yytype;
    YYSTYPE *yyvaluep;
    YYLTYPE *yylocationp;
#endif
{
  /* Pacify ``unused variable'' warnings.  */
  (void) yyvaluep;
  (void) yylocationp;

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  switch (yytype)
    {

      default:
        break;
    }
}


/* Prevent warnings from -Wmissing-prototypes.  */

#ifdef YYPARSE_PARAM
# if defined (__STDC__) || defined (__cplusplus)
int yyparse (void *YYPARSE_PARAM);
# else
int yyparse ();
# endif
#else /* ! YYPARSE_PARAM */
#if defined (__STDC__) || defined (__cplusplus)
int yyparse (Environment* currentEnv, ProgParser* parser, yyscan_t scanner);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */






/*----------.
| yyparse.  |
`----------*/

#ifdef YYPARSE_PARAM
# if defined (__STDC__) || defined (__cplusplus)
int yyparse (void *YYPARSE_PARAM)
# else
int yyparse (YYPARSE_PARAM)
  void *YYPARSE_PARAM;
# endif
#else /* ! YYPARSE_PARAM */
#if defined (__STDC__) || defined (__cplusplus)
int
yyparse (Environment* currentEnv, ProgParser* parser, yyscan_t scanner)
#else
int
yyparse (currentEnv, parser, scanner)
    Environment* currentEnv;
    ProgParser* parser;
    yyscan_t scanner;
#endif
#endif
{
  /* The look-ahead symbol.  */
int yychar;

/* The semantic value of the look-ahead symbol.  */
YYSTYPE yylval;

/* Number of syntax errors so far.  */
int yynerrs;
/* Location data for the look-ahead symbol.  */
YYLTYPE yylloc;

  int yystate;
  int yyn;
  int yyresult;
  /* Number of tokens to shift before error messages enabled.  */
  int yyerrstatus;
  /* Look-ahead token as an internal (translated) token number.  */
  int yytoken = 0;

  /* Three stacks and their tools:
     `yyss': related to states,
     `yyvs': related to semantic values,
     `yyls': related to locations.

     Refer to the stacks thru separate pointers, to allow yyoverflow
     to reallocate them elsewhere.  */

  /* The state stack.  */
  short int yyssa[YYINITDEPTH];
  short int *yyss = yyssa;
  short int *yyssp;

  /* The semantic value stack.  */
  YYSTYPE yyvsa[YYINITDEPTH];
  YYSTYPE *yyvs = yyvsa;
  YYSTYPE *yyvsp;

  /* The location stack.  */
  YYLTYPE yylsa[YYINITDEPTH];
  YYLTYPE *yyls = yylsa;
  YYLTYPE *yylsp;
  /* The locations where the error started and ended. */
  YYLTYPE yyerror_range[2];

#define YYPOPSTACK   (yyvsp--, yyssp--, yylsp--)

  YYSIZE_T yystacksize = YYINITDEPTH;

  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;
  YYLTYPE yyloc;

  /* When reducing, the number of symbols on the RHS of the reduced
     rule.  */
  int yylen;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss;
  yyvsp = yyvs;
  yylsp = yyls;
#if YYLTYPE_IS_TRIVIAL
  /* Initialize the default location before parsing starts.  */
  yylloc.first_line   = yylloc.last_line   = 1;
  yylloc.first_column = yylloc.last_column = 0;
#endif

  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed. so pushing a state here evens the stacks.
     */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack. Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	short int *yyss1 = yyss;
	YYLTYPE *yyls1 = yyls;

	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow (YY_("memory exhausted"),
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),
		    &yyls1, yysize * sizeof (*yylsp),
		    &yystacksize);
	yyls = yyls1;
	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	short int *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyexhaustedlab;
	YYSTACK_RELOCATE (yyss);
	YYSTACK_RELOCATE (yyvs);
	YYSTACK_RELOCATE (yyls);
#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;
      yylsp = yyls + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

/* Do appropriate processing given the current state.  */
/* Read a look-ahead token if we need one and don't already have one.  */
/* yyresume: */

  /* First try to decide what to do without reference to look-ahead token.  */

  yyn = yypact[yystate];
  if (yyn == YYPACT_NINF)
    goto yydefault;

  /* Not known => get a look-ahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid look-ahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yyn == 0 || yyn == YYTABLE_NINF)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Shift the look-ahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the token being shifted unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  *++yyvsp = yylval;
  *++yylsp = yylloc;

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  yystate = yyn;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];

  /* Default location. */
  YYLLOC_DEFAULT (yyloc, yylsp - yylen, yylen);
  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 147 "ProgParser.y"
    { (yyval.ptr_string) = (yyvsp[0].ptr_string); ;}
    break;

  case 3:
#line 148 "ProgParser.y"
    { (yyval.ptr_string) = (yyvsp[0].ptr_string); ;}
    break;

  case 4:
#line 152 "ProgParser.y"
    { (yyval.ptr_string) = (yyvsp[0].ptr_string); ;}
    break;

  case 5:
#line 153 "ProgParser.y"
    {
                                                                        //a constant
                                                                        if (parser->currentEnv->GetST()->FindConstant(*(yyvsp[-1].ptr_string)))
                                                                        {                                                                           
                                                                           (yyval.ptr_string) = new string(parser->currentEnv->GetST()->GetConstantString(*(yyvsp[-1].ptr_string)));
                                                                           delete ((yyvsp[-1].ptr_string));
                                                                        }
                                                                        else
                                                                        {
                                                                           (yyval.ptr_string) = (yyvsp[-1].ptr_string);
                                                                           //Error_Manager::PrintError(Pos(@3.first_line, @3.first_column, parser->GetCurrentFilename()), 23, "The constant '" + *$3 + "' is not defined. Check your function file.");   
                                                                           //parser->control_flag = false;                                                                            
                                                                        }                                                                                                                                       
                                                                     ;}
    break;

  case 6:
#line 167 "ProgParser.y"
    { (yyval.ptr_string) = (yyvsp[0].ptr_string); ;}
    break;

  case 7:
#line 171 "ProgParser.y"
    { parser->root = new PTN_Program(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), (yyvsp[-4].tree_node), (yyvsp[-2].tree_node), (yyvsp[0].tree_node)); ;}
    break;

  case 8:
#line 172 "ProgParser.y"
    { parser->root = new PTN_Program(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), (yyvsp[-2].tree_node), (yyvsp[0].tree_node)); ;}
    break;

  case 9:
#line 173 "ProgParser.y"
    { parser->root = new PTN_Program(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), (yyvsp[-4].tree_node), (yyvsp[-2].tree_node), (yyvsp[0].tree_node)); ;}
    break;

  case 10:
#line 174 "ProgParser.y"
    { parser->root = new PTN_Program(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), (yyvsp[-2].tree_node), (yyvsp[0].tree_node)); ;}
    break;

  case 11:
#line 179 "ProgParser.y"
    { 
	                                                                       currentEnv->MODE = M_STEP; 
	                                                                       currentEnv->STEP = atoi((*(yyvsp[-1].ptr_string)).c_str()); 
	                                                                       delete((yyvsp[-1].ptr_string)); 
	                                                                    ;}
    break;

  case 12:
#line 184 "ProgParser.y"
    { 
                                                                          currentEnv->MODE = M_STEP_DELTA;
                                                                          currentEnv->STEP = atoi((*(yyvsp[-5].ptr_string)).c_str()); 
                                                                          currentEnv->DELTA = (double)(atof((*(yyvsp[-1].ptr_string)).c_str())); 
                                                                          delete((yyvsp[-5].ptr_string)); 
                                                                          delete((yyvsp[-1].ptr_string));
                                                                       ;}
    break;

  case 13:
#line 191 "ProgParser.y"
    { 
                                                                          currentEnv->MODE = M_TIME;
                                                                          currentEnv->STEP = 1; 
                                                                          currentEnv->E_TIME = (double)(atof((*(yyvsp[-1].ptr_string)).c_str()));  
                                                                          delete((yyvsp[-1].ptr_string)); 
                                                                       ;}
    break;

  case 14:
#line 200 "ProgParser.y"
    { (yyval.ptr_string) = (yyvsp[0].ptr_string); ;}
    break;

  case 15:
#line 201 "ProgParser.y"
    { (yyval.ptr_string) = new std::string(NIL_BPROC); ;}
    break;

  case 16:
#line 205 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Rate(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),*(yyvsp[-2].ptr_string),*(yyvsp[0].ptr_string)); delete((yyvsp[-2].ptr_string)); delete((yyvsp[0].ptr_string)); ;}
    break;

  case 17:
#line 206 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Rate(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),"CHANGE",*(yyvsp[0].ptr_string)); delete((yyvsp[0].ptr_string)); ;}
    break;

  case 18:
#line 207 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Rate(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),"EXPOSE",*(yyvsp[0].ptr_string)); delete((yyvsp[0].ptr_string)); ;}
    break;

  case 19:
#line 208 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Rate(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),"UNHIDE",*(yyvsp[0].ptr_string)); delete((yyvsp[0].ptr_string)); ;}
    break;

  case 20:
#line 209 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Rate(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),"HIDE",*(yyvsp[0].ptr_string)); delete((yyvsp[0].ptr_string)); ;}
    break;

  case 21:
#line 210 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Rate(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),"BASERATE",*(yyvsp[0].ptr_string)); delete((yyvsp[0].ptr_string)); ;}
    break;

  case 22:
#line 211 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Rate_Pair(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),(yyvsp[-2].tree_node),(yyvsp[0].tree_node)); ;}
    break;

  case 23:
#line 215 "ProgParser.y"
    { (yyval.tree_node) = (yyvsp[0].tree_node); ;}
    break;

  case 24:
#line 216 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Dec_Pair(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), (yyvsp[-1].tree_node), (yyvsp[0].tree_node)); ;}
    break;

  case 25:
#line 220 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Dec(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), *(yyvsp[-5].ptr_string), PPROC, (yyvsp[-1].tree_node)); ;}
    break;

  case 26:
#line 221 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Dec(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), *(yyvsp[-5].ptr_string), PPROC, (yyvsp[-1].tree_node)); ;}
    break;

  case 27:
#line 222 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Dec(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), *(yyvsp[-5].ptr_string), BPROC, (yyvsp[-1].tree_node)); ;}
    break;

  case 28:
#line 224 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Dec(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), *(yyvsp[-8].ptr_string), BPROC,
                                                                                  new PTN_BpInv_Template(Pos((yylsp[-4]).first_line, (yylsp[-4]).first_column, parser->GetCurrentFilename()), *(yyvsp[-4].ptr_string), (yyvsp[-2].tree_node), *(yyvsp[-8].ptr_string))); delete((yyvsp[-8].ptr_string)); delete((yyvsp[-4].ptr_string));;}
    break;

  case 29:
#line 226 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Dec(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), *(yyvsp[-5].ptr_string), MOL, (yyvsp[-1].tree_node)); ;}
    break;

  case 30:
#line 227 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Dec(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), *(yyvsp[-5].ptr_string), SEQUENCE, (yyvsp[-1].tree_node)); ;}
    break;

  case 31:
#line 228 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Event(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), (yyvsp[-3].tree_node), (yyvsp[-1].tree_node)); ;}
    break;

  case 32:
#line 230 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Template(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), PPROC, *(yyvsp[-8].ptr_string), (yyvsp[-4].tree_node), (yyvsp[-1].tree_node)); ;}
    break;

  case 33:
#line 232 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Template(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), PPROC, *(yyvsp[-8].ptr_string), (yyvsp[-4].tree_node), (yyvsp[-1].tree_node)); ;}
    break;

  case 34:
#line 234 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Template(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), BPROC, *(yyvsp[-8].ptr_string), (yyvsp[-4].tree_node), (yyvsp[-1].tree_node)); ;}
    break;

  case 35:
#line 238 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Dec_Temp_Elem(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), TEMP_NAME, *(yyvsp[0].ptr_string)); ;}
    break;

  case 36:
#line 239 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Dec_Temp_Elem(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), TEMP_PPROC, *(yyvsp[0].ptr_string)); ;}
    break;

  case 37:
#line 240 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Dec_Temp_Elem(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), TEMP_TYPE, *(yyvsp[0].ptr_string)); ;}
    break;

  case 38:
#line 241 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Dec_Temp_Elem(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), TEMP_SEQ, *(yyvsp[0].ptr_string)); ;}
    break;

  case 39:
#line 242 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Dec_Temp_Elem(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), TEMP_RATE, *(yyvsp[0].ptr_string)); ;}
    break;

  case 40:
#line 246 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Dec_Temp_List(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),(yyvsp[0].tree_node)); ;}
    break;

  case 41:
#line 247 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Dec_Temp_List(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),(yyvsp[-2].tree_node),(yyvsp[0].tree_node)); ;}
    break;

  case 42:
#line 251 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Inv_Temp_Elem(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), STATE_NOT_SPECIFIED, *(yyvsp[0].ptr_string)); delete((yyvsp[0].ptr_string)); ;}
    break;

  case 43:
#line 252 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Inv_Temp_Elem(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), STATE_NOT_SPECIFIED, *(yyvsp[0].ptr_string)); delete((yyvsp[0].ptr_string)); ;}
    break;

  case 44:
#line 253 "ProgParser.y"
    {
																	(yyval.tree_node) = new PTN_Inv_Temp_Elem(
																		Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), STATE_NOT_SPECIFIED, *(yyvsp[-3].ptr_string),
																		new PTN_PiInv_Template(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), *(yyvsp[-3].ptr_string), (yyvsp[-1].tree_node))); 
																	delete((yyvsp[-3].ptr_string)); 
																;}
    break;

  case 45:
#line 259 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Inv_Temp_Elem(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),STATE_UNHIDDEN,*(yyvsp[-3].ptr_string)); delete((yyvsp[-3].ptr_string)); ;}
    break;

  case 46:
#line 260 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Inv_Temp_Elem(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),STATE_HIDDEN,*(yyvsp[-3].ptr_string)); delete((yyvsp[-3].ptr_string)); ;}
    break;

  case 47:
#line 264 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Inv_Temp_List(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),(yyvsp[0].tree_node)); ;}
    break;

  case 48:
#line 265 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Inv_Temp_List(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),(yyvsp[-2].tree_node),(yyvsp[0].tree_node)); ;}
    break;

  case 49:
#line 272 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Action_List(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),(yyvsp[0].tree_node)); ;}
    break;

  case 50:
#line 273 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Action_List(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),(yyvsp[-2].tree_node), (yyvsp[0].tree_node)); ;}
    break;

  case 51:
#line 277 "ProgParser.y"
    { (yyval.tree_node) = new PTN_EventCond_State(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), *(yyvsp[-2].ptr_string), *(yyvsp[0].ptr_string), STATE_UP); delete((yyvsp[-2].ptr_string)); delete((yyvsp[0].ptr_string)); ;}
    break;

  case 52:
#line 278 "ProgParser.y"
    { (yyval.tree_node) = new PTN_EventCond_State(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), *(yyvsp[-2].ptr_string), *(yyvsp[0].ptr_string), STATE_DOWN); delete((yyvsp[-2].ptr_string)); delete((yyvsp[0].ptr_string)); ;}
    break;

  case 53:
#line 282 "ProgParser.y"
    { (yyval.tree_node) = new PTN_EventCond_StateList(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), (yyvsp[0].tree_node)); ;}
    break;

  case 54:
#line 283 "ProgParser.y"
    { (yyval.tree_node) = new PTN_EventCond_StateList(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), (yyvsp[-2].tree_node), (yyvsp[0].tree_node)); ;}
    break;

  case 55:
#line 287 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Event_Entity(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), *(yyvsp[0].ptr_string)); delete((yyvsp[0].ptr_string)); ;}
    break;

  case 56:
#line 291 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Event_EntityList(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), (yyvsp[0].tree_node)); ;}
    break;

  case 57:
#line 292 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Event_EntityList(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), (yyvsp[-2].tree_node), (yyvsp[0].tree_node)); ;}
    break;

  case 58:
#line 296 "ProgParser.y"
    { (yyval.event_cond_node) = new EventCond_AtomCount(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), COND_COUNT_EQUAL,   *(yyvsp[-3].ptr_string), *(yyvsp[0].ptr_string)); delete((yyvsp[-3].ptr_string)); delete((yyvsp[0].ptr_string)); ;}
    break;

  case 59:
#line 297 "ProgParser.y"
    { (yyval.event_cond_node) = new EventCond_AtomCount(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), COND_COUNT_GREATER, *(yyvsp[-3].ptr_string), *(yyvsp[0].ptr_string)); delete((yyvsp[-3].ptr_string)); delete((yyvsp[0].ptr_string)); ;}
    break;

  case 60:
#line 298 "ProgParser.y"
    { (yyval.event_cond_node) = new EventCond_AtomCount(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), COND_COUNT_LESS,    *(yyvsp[-3].ptr_string), *(yyvsp[0].ptr_string)); delete((yyvsp[-3].ptr_string)); delete((yyvsp[0].ptr_string)); ;}
    break;

  case 61:
#line 299 "ProgParser.y"
    { (yyval.event_cond_node) = new EventCond_AtomCount(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), COND_COUNT_NEQUAL,  *(yyvsp[-3].ptr_string), *(yyvsp[0].ptr_string)); delete((yyvsp[-3].ptr_string)); delete((yyvsp[0].ptr_string)); ;}
    break;

  case 62:
#line 300 "ProgParser.y"
    { (yyval.event_cond_node) = new EventCond_AtomDet(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),   COND_TIME_EQUAL,    *(yyvsp[0].ptr_string)); delete((yyvsp[0].ptr_string));  ;}
    break;

  case 63:
#line 301 "ProgParser.y"
    { (yyval.event_cond_node) = new EventCond_AtomDet(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),   COND_STEP_EQUAL,    *(yyvsp[0].ptr_string)); delete((yyvsp[0].ptr_string));  ;}
    break;

  case 64:
#line 302 "ProgParser.y"
    { (yyval.event_cond_node) = new EventCond_AtomStates(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), COND_STATES,        (yyvsp[0].tree_node)); ;}
    break;

  case 65:
#line 306 "ProgParser.y"
    { (yyval.event_cond_node) = (yyvsp[0].event_cond_node); ;}
    break;

  case 66:
#line 307 "ProgParser.y"
    { (yyval.event_cond_node) = new EventCond_And(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),(yyvsp[-2].event_cond_node),(yyvsp[0].event_cond_node)); ;}
    break;

  case 67:
#line 308 "ProgParser.y"
    { (yyval.event_cond_node) = new EventCond_Or(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),(yyvsp[-2].event_cond_node),(yyvsp[0].event_cond_node)); ;}
    break;

  case 68:
#line 309 "ProgParser.y"
    { (yyval.event_cond_node) = new EventCond_Not(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),(yyvsp[0].event_cond_node)); ;}
    break;

  case 69:
#line 310 "ProgParser.y"
    { (yyval.event_cond_node) = (yyvsp[-1].event_cond_node); ;}
    break;

  case 70:
#line 314 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Event_Cond(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), COND_RATE,           (yyvsp[-4].tree_node), (yyvsp[-2].event_cond_node), *(yyvsp[0].ptr_string)); delete((yyvsp[0].ptr_string)); ;}
    break;

  case 71:
#line 315 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Event_Cond(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), COND_RATE_FUN,       (yyvsp[-4].tree_node), (yyvsp[-2].event_cond_node), *(yyvsp[0].ptr_string)); delete((yyvsp[0].ptr_string)); ;}
    break;

  case 72:
#line 316 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Event_Cond(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), COND_RATE_IMMEDIATE, (yyvsp[-3].tree_node), (yyvsp[-1].event_cond_node), INF_RATE); ;}
    break;

  case 73:
#line 317 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Event_Cond(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), COND_RATE_GAMMA,(yyvsp[-9].tree_node),(yyvsp[-7].event_cond_node),*(yyvsp[-3].ptr_string),*(yyvsp[-1].ptr_string)); delete((yyvsp[-3].ptr_string));delete((yyvsp[-1].ptr_string)); ;}
    break;

  case 74:
#line 318 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Event_Cond(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), COND_RATE_HYPEXP,(yyvsp[-7].tree_node),(yyvsp[-5].event_cond_node),(yyvsp[-1].tree_node)); delete((yyvsp[-1].tree_node));  ;}
    break;

  case 75:
#line 319 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Event_Cond(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), COND_RATE,           (yyvsp[-3].tree_node), NULL, *(yyvsp[0].ptr_string)); delete((yyvsp[0].ptr_string)); ;}
    break;

  case 76:
#line 320 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Event_Cond(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), COND_RATE_FUN,       (yyvsp[-3].tree_node), NULL, *(yyvsp[0].ptr_string)); delete((yyvsp[0].ptr_string)); ;}
    break;

  case 77:
#line 321 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Event_Cond(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), COND_RATE_GAMMA,(yyvsp[-8].tree_node),NULL,*(yyvsp[-3].ptr_string),*(yyvsp[-1].ptr_string)); delete((yyvsp[-3].ptr_string));delete((yyvsp[-1].ptr_string)); ;}
    break;

  case 78:
#line 322 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Event_Cond(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), COND_RATE_HYPEXP,(yyvsp[-6].tree_node),NULL,(yyvsp[-1].tree_node)); ;}
    break;

  case 79:
#line 323 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Event_Cond(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), COND_RATE_IMMEDIATE, NULL, (yyvsp[-1].event_cond_node), INF_RATE); ;}
    break;

  case 80:
#line 327 "ProgParser.y"
    { (yyval.tree_node) = new PTN_HypExp_Parameter(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),NULL,*(yyvsp[-3].ptr_string),*(yyvsp[-1].ptr_string)); delete((yyvsp[-3].ptr_string)); delete((yyvsp[-1].ptr_string));;}
    break;

  case 81:
#line 328 "ProgParser.y"
    { (yyval.tree_node) = new PTN_HypExp_Parameter(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),(yyvsp[0].tree_node),*(yyvsp[-5].ptr_string),*(yyvsp[-3].ptr_string)); delete((yyvsp[-5].ptr_string)); delete((yyvsp[-3].ptr_string));;}
    break;

  case 82:
#line 332 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Event_Verb(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), *(yyvsp[-3].ptr_string), *(yyvsp[-1].ptr_string), VERB_SPLIT); delete((yyvsp[-3].ptr_string)); delete((yyvsp[-1].ptr_string)); ;}
    break;

  case 83:
#line 333 "ProgParser.y"
    {  
                                                                  int count = atoi((*(yyvsp[-1].ptr_string)).c_str());
                                                                  delete (yyvsp[-1].ptr_string);
                                                                  (yyval.tree_node) = new PTN_Event_Verb(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), count, VERB_NEW); 
                                                               ;}
    break;

  case 84:
#line 338 "ProgParser.y"
    {  
                                                                  int count = atoi((*(yyvsp[-1].ptr_string)).c_str());
                                                                  delete (yyvsp[-1].ptr_string);
                                                                  (yyval.tree_node) = new PTN_Event_Verb(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), count, VERB_DELETE); 
                                                               ;}
    break;

  case 85:
#line 343 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Event_Verb(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), VERB_NEW); ;}
    break;

  case 86:
#line 344 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Event_Verb(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), VERB_DELETE); ;}
    break;

  case 87:
#line 345 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Event_Verb(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), *(yyvsp[-1].ptr_string), VERB_JOIN); delete((yyvsp[-1].ptr_string)); ;}
    break;

  case 88:
#line 346 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Event_Verb(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), VERB_JOIN); ;}
    break;

  case 89:
#line 347 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Event_Verb(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), *(yyvsp[-3].ptr_string), *(yyvsp[-1].ptr_string), VERB_UPDATE); ;}
    break;

  case 90:
#line 351 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Pi_Nil(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename())); ;}
    break;

  case 91:
#line 352 "ProgParser.y"
    { (yyval.tree_node) = (yyvsp[0].tree_node); ;}
    break;

  case 92:
#line 353 "ProgParser.y"
    { (yyval.tree_node) = new PTN_IfThen_Node(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),(yyvsp[-3].tree_node),(yyvsp[-1].tree_node)); ;}
    break;

  case 93:
#line 357 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Pi_Id(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),*(yyvsp[0].ptr_string)); delete((yyvsp[0].ptr_string)); ;}
    break;

  case 94:
#line 358 "ProgParser.y"
    { (yyval.tree_node) = new PTN_PiInv_Template(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), *(yyvsp[-3].ptr_string), (yyvsp[-1].tree_node)); delete((yyvsp[-3].ptr_string)); ;}
    break;

  case 95:
#line 359 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Pi_Replication(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),(yyvsp[-2].tree_node),(yyvsp[0].tree_node)); ;}
    break;

  case 96:
#line 360 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Pi_Replication(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),(yyvsp[-2].tree_node),(yyvsp[0].tree_node)); ;}
    break;

  case 97:
#line 361 "ProgParser.y"
    { (yyval.tree_node) = new PTN_IfThen_Node(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),(yyvsp[-3].tree_node),(yyvsp[-1].tree_node)); ;}
    break;

  case 98:
#line 365 "ProgParser.y"
    { (yyval.tree_node) = (yyvsp[0].tree_node); ;}
    break;

  case 99:
#line 367 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Pi_Choice(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), (yyvsp[-2].tree_node), (yyvsp[0].tree_node)); ;}
    break;

  case 100:
#line 368 "ProgParser.y"
    { (yyval.tree_node) = (yyvsp[-1].tree_node); ;}
    break;

  case 101:
#line 372 "ProgParser.y"
    { (yyval.tree_node) = (yyvsp[0].tree_node); ;}
    break;

  case 102:
#line 373 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Pi_Parallel(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),(yyvsp[-2].tree_node),(yyvsp[0].tree_node)); ;}
    break;

  case 103:
#line 374 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Pi_Parallel(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),(yyvsp[-2].tree_node),(yyvsp[0].tree_node)); ;}
    break;

  case 104:
#line 375 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Pi_Parallel(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),(yyvsp[-2].tree_node),(yyvsp[0].tree_node)); ;}
    break;

  case 105:
#line 376 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Pi_Parallel(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),(yyvsp[-2].tree_node),(yyvsp[0].tree_node)); ;}
    break;

  case 106:
#line 377 "ProgParser.y"
    { (yyval.tree_node) = (yyvsp[-1].tree_node); ;}
    break;

  case 107:
#line 381 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Pi_Action(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),(yyvsp[0].tree_node), new PTN_Pi_Nil(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()))); ;}
    break;

  case 108:
#line 382 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Pi_Action(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),(yyvsp[-2].tree_node),(yyvsp[0].tree_node)); ;}
    break;

  case 109:
#line 383 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Pi_Action(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),(yyvsp[-2].tree_node),(yyvsp[0].tree_node)); ;}
    break;

  case 110:
#line 384 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Pi_Action(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), new PTN_Pi_Id(Pos((yylsp[-2]).first_line, (yylsp[-2]).first_column, parser->GetCurrentFilename()), *(yyvsp[-2].ptr_string)), (yyvsp[0].tree_node)); delete((yyvsp[-2].ptr_string)); ;}
    break;

  case 111:
#line 385 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Pi_Action(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), new PTN_Pi_Id(Pos((yylsp[-2]).first_line, (yylsp[-2]).first_column, parser->GetCurrentFilename()), *(yyvsp[-2].ptr_string)), (yyvsp[0].tree_node)); delete((yyvsp[-2].ptr_string)); ;}
    break;

  case 112:
#line 390 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Atom_IfThen_Node(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),*(yyvsp[-3].ptr_string),*(yyvsp[-1].ptr_string),STATE_NOT_SPECIFIED); delete((yyvsp[-3].ptr_string)); delete((yyvsp[-1].ptr_string)); ;}
    break;

  case 113:
#line 391 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Atom_IfThen_Node(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),*(yyvsp[-3].ptr_string),EMPTY_STRING,STATE_HIDDEN); delete((yyvsp[-3].ptr_string));;}
    break;

  case 114:
#line 392 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Atom_IfThen_Node(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),*(yyvsp[-3].ptr_string),EMPTY_STRING,STATE_UNHIDDEN); delete((yyvsp[-3].ptr_string)); ;}
    break;

  case 115:
#line 393 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Atom_IfThen_Node(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),*(yyvsp[-3].ptr_string),EMPTY_STRING,STATE_BOUND); delete((yyvsp[-3].ptr_string)); ;}
    break;

  case 116:
#line 394 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Atom_IfThen_Node(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),*(yyvsp[-5].ptr_string),*(yyvsp[-3].ptr_string),STATE_HIDDEN); delete((yyvsp[-5].ptr_string)); delete((yyvsp[-3].ptr_string)); ;}
    break;

  case 117:
#line 395 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Atom_IfThen_Node(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),*(yyvsp[-5].ptr_string),*(yyvsp[-3].ptr_string),STATE_UNHIDDEN); delete((yyvsp[-5].ptr_string)); delete((yyvsp[-3].ptr_string)); ;}
    break;

  case 118:
#line 396 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Atom_IfThen_Node(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),*(yyvsp[-5].ptr_string),*(yyvsp[-3].ptr_string),STATE_BOUND); delete((yyvsp[-5].ptr_string)); delete((yyvsp[-3].ptr_string)); ;}
    break;

  case 119:
#line 400 "ProgParser.y"
    { (yyval.tree_node) = (yyvsp[0].tree_node); ;}
    break;

  case 120:
#line 401 "ProgParser.y"
    { (yyval.tree_node) = new PTN_And_IfThen_Node(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),(yyvsp[-2].tree_node),(yyvsp[0].tree_node)); ;}
    break;

  case 121:
#line 402 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Or_IfThen_Node(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),(yyvsp[-2].tree_node),(yyvsp[0].tree_node)); ;}
    break;

  case 122:
#line 403 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Not_IfThen_Node(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),(yyvsp[0].tree_node)); ;}
    break;

  case 123:
#line 404 "ProgParser.y"
    { (yyval.tree_node) = (yyvsp[-1].tree_node); ;}
    break;

  case 124:
#line 408 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Action_Tau(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),*(yyvsp[-1].ptr_string)); delete((yyvsp[-1].ptr_string)); ;}
    break;

  case 125:
#line 409 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Action_IO(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),*(yyvsp[-4].ptr_string),*(yyvsp[-1].ptr_string), ACTION_OUTPUT); delete((yyvsp[-4].ptr_string)); delete((yyvsp[-1].ptr_string)); ;}
    break;

  case 126:
#line 410 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Action_IO(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),*(yyvsp[-3].ptr_string),EMPTY_OUTPUT, ACTION_OUTPUT); delete((yyvsp[-3].ptr_string)); ;}
    break;

  case 127:
#line 411 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Action_IO(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),*(yyvsp[-3].ptr_string),"", ACTION_INPUT); delete((yyvsp[-3].ptr_string)); ;}
    break;

  case 128:
#line 412 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Action_IO(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),*(yyvsp[-4].ptr_string),*(yyvsp[-1].ptr_string), ACTION_INPUT); delete((yyvsp[-4].ptr_string)); delete((yyvsp[-1].ptr_string)); ;}
    break;

  case 129:
#line 413 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Action_Expose(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),*(yyvsp[-5].ptr_string),*(yyvsp[-7].ptr_string),*(yyvsp[-3].ptr_string),*(yyvsp[-1].ptr_string)); delete((yyvsp[-7].ptr_string)); delete((yyvsp[-5].ptr_string));;}
    break;

  case 130:
#line 414 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Action_Expose(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),*(yyvsp[-5].ptr_string),"",*(yyvsp[-3].ptr_string),*(yyvsp[-1].ptr_string)); delete((yyvsp[-5].ptr_string)); delete((yyvsp[-3].ptr_string)); delete((yyvsp[-1].ptr_string)); ;}
    break;

  case 131:
#line 415 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Action_HU(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),*(yyvsp[-1].ptr_string),"", ACTION_HIDE); delete((yyvsp[-1].ptr_string)); ;}
    break;

  case 132:
#line 416 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Action_HU(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),*(yyvsp[-1].ptr_string),"", ACTION_UNHIDE); delete((yyvsp[-1].ptr_string)); ;}
    break;

  case 133:
#line 417 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Action_HU(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),*(yyvsp[-1].ptr_string),*(yyvsp[-3].ptr_string), ACTION_HIDE); delete((yyvsp[-3].ptr_string)); delete((yyvsp[-1].ptr_string)); ;}
    break;

  case 134:
#line 418 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Action_HU(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),*(yyvsp[-1].ptr_string),*(yyvsp[-3].ptr_string), ACTION_UNHIDE); delete((yyvsp[-3].ptr_string)); delete((yyvsp[-1].ptr_string)); ;}
    break;

  case 135:
#line 419 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Action_Change(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),*(yyvsp[-5].ptr_string),*(yyvsp[-3].ptr_string),*(yyvsp[-1].ptr_string)); delete((yyvsp[-5].ptr_string)); delete((yyvsp[-3].ptr_string)); delete((yyvsp[-1].ptr_string)); ;}
    break;

  case 136:
#line 420 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Action_Change(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),"",*(yyvsp[-3].ptr_string),*(yyvsp[-1].ptr_string)); delete((yyvsp[-3].ptr_string)); delete((yyvsp[-1].ptr_string)); ;}
    break;

  case 137:
#line 421 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Action_Die(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),""); ;}
    break;

  case 138:
#line 422 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Action_Die(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),*(yyvsp[-1].ptr_string)); delete((yyvsp[-1].ptr_string)); ;}
    break;

  case 139:
#line 426 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Beta(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),(yyvsp[-3].tree_node),(yyvsp[-1].tree_node)); ;}
    break;

  case 140:
#line 427 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Beta(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),(yyvsp[-3].tree_node),(yyvsp[-1].tree_node)); ;}
    break;

  case 141:
#line 431 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Binder(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),*(yyvsp[-5].ptr_string),*(yyvsp[-1].ptr_string), STATE_UNHIDDEN, *(yyvsp[-3].ptr_string)); delete((yyvsp[-5].ptr_string)); delete((yyvsp[-3].ptr_string)); delete((yyvsp[-1].ptr_string)); ;}
    break;

  case 142:
#line 432 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Binder(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),*(yyvsp[-5].ptr_string),*(yyvsp[-1].ptr_string), STATE_HIDDEN, *(yyvsp[-3].ptr_string)); delete((yyvsp[-5].ptr_string)); delete((yyvsp[-3].ptr_string)); delete((yyvsp[-1].ptr_string));;}
    break;

  case 143:
#line 433 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Binder(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),*(yyvsp[-3].ptr_string),*(yyvsp[-1].ptr_string), STATE_UNHIDDEN, "0.0"); delete((yyvsp[-3].ptr_string)); delete((yyvsp[-1].ptr_string)); ;}
    break;

  case 144:
#line 434 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Binder(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),*(yyvsp[-3].ptr_string),*(yyvsp[-1].ptr_string), STATE_HIDDEN, "0.0"); delete((yyvsp[-3].ptr_string)); delete((yyvsp[-1].ptr_string)); ;}
    break;

  case 145:
#line 435 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Binder_Pair(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),(yyvsp[-2].tree_node),(yyvsp[0].tree_node)); ;}
    break;

  case 146:
#line 440 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Bp(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),*(yyvsp[0].ptr_string),*(yyvsp[-1].ptr_string)); delete((yyvsp[-1].ptr_string)); delete((yyvsp[0].ptr_string)); ;}
    break;

  case 147:
#line 441 "ProgParser.y"
    { 
                                                            Pos pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename());
                                                            std::string count_string("0");
                                                            
                                                            if (!parser->currentEnv->GetST()->FindConstant(*(yyvsp[-1].ptr_string)))
                                                            {
                                                               // Raise error: not a constant
                                                               
                                                               Error_Manager::PrintError(pos, 45, "'" + *(yyvsp[-1].ptr_string) + "' is not a valid constant");
                                                               parser->control_flag = false;
                                                            }
                                                            else
                                                            {
                                                               double constant = parser->currentEnv->GetST()->GetConstant(*(yyvsp[-1].ptr_string));
                                                               double intpart = 0.0;
                                                               double fracpart = modf(constant, &intpart);
                                                               
                                                               count_string = Utility::i2s((int)intpart);
                                                               
                                                               if (fracpart != 0.0)
                                                               {
                                                                  // warn if not a decimal value         
                                                                  Error_Manager::PrintWarning(pos, 45, "'" + *(yyvsp[-1].ptr_string) + "' has a non-integral part, and will be truncated to " + count_string);                                                         
                                                               } 
                                                            }
                                                            
                                                            (yyval.tree_node) = new PTN_Bp(pos, *(yyvsp[0].ptr_string), count_string); 
                                                            delete((yyvsp[-1].ptr_string)); delete((yyvsp[0].ptr_string));                                                             
                                                         ;}
    break;

  case 148:
#line 470 "ProgParser.y"
    { (yyval.tree_node) = new PTN_BpInv_Template(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), *(yyvsp[-3].ptr_string), *(yyvsp[-4].ptr_string), (yyvsp[-1].tree_node)); delete((yyvsp[-4].ptr_string)); delete((yyvsp[-3].ptr_string));;}
    break;

  case 149:
#line 471 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Bp_Pair(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()),(yyvsp[-2].tree_node),(yyvsp[0].tree_node)); ;}
    break;

  case 150:
#line 475 "ProgParser.y"
    { (yyval.tree_node) = new PTN_Molecule(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), (yyvsp[-3].tree_node), (yyvsp[-1].tree_node)); ;}
    break;

  case 151:
#line 480 "ProgParser.y"
    { (yyval.tree_node) = (yyvsp[-1].tree_node); ;}
    break;

  case 152:
#line 484 "ProgParser.y"
    { (yyval.tree_node) = new PTN_MolEdge_List(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), (yyvsp[0].tree_node)); ;}
    break;

  case 153:
#line 485 "ProgParser.y"
    { (yyval.tree_node) = new PTN_MolEdge_List(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), (yyvsp[-2].tree_node), (yyvsp[0].tree_node)); ;}
    break;

  case 154:
#line 489 "ProgParser.y"
    { (yyval.tree_node) = new PTN_MolEdge(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), *(yyvsp[-7].ptr_string), *(yyvsp[-5].ptr_string), *(yyvsp[-3].ptr_string), *(yyvsp[-1].ptr_string)); ;}
    break;

  case 155:
#line 492 "ProgParser.y"
    { (yyval.tree_node) = new PTN_MolNode_List(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), (yyvsp[0].tree_node)); ;}
    break;

  case 156:
#line 493 "ProgParser.y"
    { (yyval.tree_node) = new PTN_MolNode_List(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), (yyvsp[-1].tree_node), (yyvsp[0].tree_node)); ;}
    break;

  case 157:
#line 497 "ProgParser.y"
    { (yyval.tree_node) = new PTN_MolNode(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), *(yyvsp[-7].ptr_string), *(yyvsp[-5].ptr_string), (yyvsp[-2].tree_node)); ;}
    break;

  case 158:
#line 498 "ProgParser.y"
    { (yyval.tree_node) = new PTN_MolNode(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), *(yyvsp[-3].ptr_string), *(yyvsp[-1].ptr_string)); ;}
    break;

  case 159:
#line 502 "ProgParser.y"
    { (yyval.tree_node) = new PTN_MolBinder(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), *(yyvsp[0].ptr_string)); ;}
    break;

  case 160:
#line 506 "ProgParser.y"
    { (yyval.tree_node) = new PTN_MolBinder_List(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), (yyvsp[0].tree_node)); ;}
    break;

  case 161:
#line 507 "ProgParser.y"
    { (yyval.tree_node) = new PTN_MolBinder_List(Pos((yyloc).first_line, (yyloc).first_column, parser->GetCurrentFilename()), (yyvsp[-2].tree_node), (yyvsp[0].tree_node)); ;}
    break;


      default: break;
    }

/* Line 1126 of yacc.c.  */
#line 2648 "ProgParser.cpp"

  yyvsp -= yylen;
  yyssp -= yylen;
  yylsp -= yylen;

  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;
  *++yylsp = yyloc;

  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if YYERROR_VERBOSE
      yyn = yypact[yystate];

      if (YYPACT_NINF < yyn && yyn < YYLAST)
	{
	  int yytype = YYTRANSLATE (yychar);
	  YYSIZE_T yysize0 = yytnamerr (0, yytname[yytype]);
	  YYSIZE_T yysize = yysize0;
	  YYSIZE_T yysize1;
	  int yysize_overflow = 0;
	  char *yymsg = 0;
#	  define YYERROR_VERBOSE_ARGS_MAXIMUM 5
	  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
	  int yyx;

#if 0
	  /* This is so xgettext sees the translatable formats that are
	     constructed on the fly.  */
	  YY_("syntax error, unexpected %s");
	  YY_("syntax error, unexpected %s, expecting %s");
	  YY_("syntax error, unexpected %s, expecting %s or %s");
	  YY_("syntax error, unexpected %s, expecting %s or %s or %s");
	  YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s");
#endif
	  char *yyfmt;
	  char const *yyf;
	  static char const yyunexpected[] = "syntax error, unexpected %s";
	  static char const yyexpecting[] = ", expecting %s";
	  static char const yyor[] = " or %s";
	  char yyformat[sizeof yyunexpected
			+ sizeof yyexpecting - 1
			+ ((YYERROR_VERBOSE_ARGS_MAXIMUM - 2)
			   * (sizeof yyor - 1))];
	  char const *yyprefix = yyexpecting;

	  /* Start YYX at -YYN if negative to avoid negative indexes in
	     YYCHECK.  */
	  int yyxbegin = yyn < 0 ? -yyn : 0;

	  /* Stay within bounds of both yycheck and yytname.  */
	  int yychecklim = YYLAST - yyn;
	  int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
	  int yycount = 1;

	  yyarg[0] = yytname[yytype];
	  yyfmt = yystpcpy (yyformat, yyunexpected);

	  for (yyx = yyxbegin; yyx < yyxend; ++yyx)
	    if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
	      {
		if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
		  {
		    yycount = 1;
		    yysize = yysize0;
		    yyformat[sizeof yyunexpected - 1] = '\0';
		    break;
		  }
		yyarg[yycount++] = yytname[yyx];
		yysize1 = yysize + yytnamerr (0, yytname[yyx]);
		yysize_overflow |= yysize1 < yysize;
		yysize = yysize1;
		yyfmt = yystpcpy (yyfmt, yyprefix);
		yyprefix = yyor;
	      }

	  yyf = YY_(yyformat);
	  yysize1 = yysize + yystrlen (yyf);
	  yysize_overflow |= yysize1 < yysize;
	  yysize = yysize1;

	  if (!yysize_overflow && yysize <= YYSTACK_ALLOC_MAXIMUM)
	    yymsg = (char *) YYSTACK_ALLOC (yysize);
	  if (yymsg)
	    {
	      /* Avoid sprintf, as that infringes on the user's name space.
		 Don't have undefined behavior even if the translation
		 produced a string with the wrong number of "%s"s.  */
	      char *yyp = yymsg;
	      int yyi = 0;
	      while ((*yyp = *yyf))
		{
		  if (*yyp == '%' && yyf[1] == 's' && yyi < yycount)
		    {
		      yyp += yytnamerr (yyp, yyarg[yyi++]);
		      yyf += 2;
		    }
		  else
		    {
		      yyp++;
		      yyf++;
		    }
		}
	      yyerror (&yylloc, currentEnv, parser, scanner, yymsg);
	      YYSTACK_FREE (yymsg);
	    }
	  else
	    {
	      yyerror (&yylloc, currentEnv, parser, scanner, YY_("syntax error"));
	      goto yyexhaustedlab;
	    }
	}
      else
#endif /* YYERROR_VERBOSE */
	yyerror (&yylloc, currentEnv, parser, scanner, YY_("syntax error"));
    }

  yyerror_range[0] = yylloc;

  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse look-ahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
        {
	  /* Return failure if at end of input.  */
	  if (yychar == YYEOF)
	    YYABORT;
        }
      else
	{
	  yydestruct ("Error: discarding", yytoken, &yylval, &yylloc);
	  yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse look-ahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (0)
     goto yyerrorlab;

  yyerror_range[0] = yylsp[1-yylen];
  yylsp -= yylen;
  yyvsp -= yylen;
  yyssp -= yylen;
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (yyn != YYPACT_NINF)
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;

      yyerror_range[0] = *yylsp;
      yydestruct ("Error: popping", yystos[yystate], yyvsp, yylsp);
      YYPOPSTACK;
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  *++yyvsp = yylval;

  yyerror_range[1] = yylloc;
  /* Using YYLLOC is tempting, but would change the location of
     the look-ahead.  YYLOC is available though. */
  YYLLOC_DEFAULT (yyloc, yyerror_range - 1, 2);
  *++yylsp = yyloc;

  /* Shift the error token. */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#ifndef yyoverflow
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (&yylloc, currentEnv, parser, scanner, YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEOF && yychar != YYEMPTY)
     yydestruct ("Cleanup: discarding lookahead",
		 yytoken, &yylval, &yylloc);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
		  yystos[*yyssp], yyvsp, yylsp);
      YYPOPSTACK;
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
  return yyresult;
}


#line 510 "ProgParser.y"


// Syntax error message 
void prog_error(YYLTYPE *llocp, Environment* currentEnv, ProgParser* parser, yyscan_t scanner, char const* msg)
{
   Pos pos(llocp->first_line, llocp->first_column, parser->currentFilename);
   Error_Manager::PrintError(pos, 0, msg);
}

// Parse the file with name "file_name"
int ProgParser::Parse()
{
   control_flag = true;
   int res;
   string validate;
   const char* file_name = currentFilename.c_str();
   
  
   FILE* prog_file = fopen(file_name,"r+");
   if (prog_file == NULL) 
   {
      Logger::GetInstance().log("PROGRAM FILE: cannot open.");
      Error_Manager::PrintError(0, "Cannot open program file '" + currentFilename + "'");
      return -1;
   }
   
   yyscan_t scanner;
   prog_lex_init(&scanner);
   prog_set_in(prog_file, scanner);
   
   //scanner.Init(prog_file);

   res = prog_parse(currentEnv, this, scanner);
   if (res == 0 && (control_flag))
   {
      Logger::GetInstance().log("PROGRAM FILE: parsed.");
   }
   else
   {
      Logger::GetInstance().log("PROGRAM FILE: parsing failed.");
      return -1;
   }
  
   if (root->GenerateST(currentEnv->GetST()))
      Logger::GetInstance().log("SYMBOL TABLE: generated.");
   else
   {
      Logger::GetInstance().log("SYMBOL TABLE: failed.");
      return -1;
   }
   
 
   if (currentEnv->GetST()->Validate())
      Logger::GetInstance().log("VALIDATION: ok.");
   else
   {
      Logger::GetInstance().log("VALIDATION: failed.");
      return -1;
   }
 
   if (currentEnv->GetST()->GenerateIC())
      Logger::GetInstance().log("INTERMEDIATE CODE: generated.");
   else
   {
      Logger::GetInstance().log("GENERATE IC: failed.");
      return -1;
   }

   if (currentEnv->GetST()->InitAM())
      Logger::GetInstance().log("ENVIRONMENT: initialized.");
   else
   {
	   Logger::GetInstance().log("INITAM: failed.");
	   return -1;
   }
 
   prog_lex_destroy(scanner);
   fclose(prog_file);      
   return res;
}


#ifdef _MSC_VER 
#pragma warning (pop)
#pragma warning(default : 4706)
#pragma warning(default : 4702)
#pragma warning(default : 4505) 
#endif


