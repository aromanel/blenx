
#ifndef PROG_PARSER_H_INCLUDED
#define PROG_PARSER_H_INCLUDED

#include "PT_General_Node.h"
#include "Environment.h"

class ProgParser
{
public:
   Environment* currentEnv;
   std::string currentFilename;
   PTNode* root;

   bool control_flag;
  
public:
   ProgParser (Environment* env, const std::string& filename)
      : currentEnv(env), currentFilename(filename) { }

   std::string& GetCurrentFilename() { return currentFilename; }

   int Parse();
};


#endif //PROG_PARSER_H_INCLUDED

