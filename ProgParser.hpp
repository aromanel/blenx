/* A Bison parser, made by GNU Bison 2.1.  */

/* Skeleton parser for Yacc-like parsing with Bison,
   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     LID = 258,
     LDECIMAL = 259,
     LREAL = 260,
     LINF = 261,
     LSTEPS = 262,
     LSTEP = 263,
     LAOPEN = 264,
     LACLOSE = 265,
     LGOPEN = 266,
     LGCLOSE = 267,
     LNIL = 268,
     LBNIL = 269,
     LPARALLEL = 270,
     LPOPEN = 271,
     LPCLOSE = 272,
     LCHOICE = 273,
     LRESTRICTION = 274,
     LSOPEN = 275,
     LSCLOSE = 276,
     LEQUAL = 277,
     LNEQUAL = 278,
     LDOT = 279,
     LDDOT = 280,
     LCOMMA = 281,
     LBB = 282,
     LBBH = 283,
     LDOTCOMMA = 284,
     LDAOPEN = 285,
     LDACLOSE = 286,
     LLET = 287,
     LBASERATE = 288,
     LREXPOSE = 289,
     LRHIDE = 290,
     LRUNHIDE = 291,
     LDELIM = 292,
     LTAU = 293,
     LDELTA = 294,
     LTIME = 295,
     LTIMESPAN = 296,
     LCHANGE = 297,
     LDIE = 298,
     LRCHANGE = 299,
     LRATE = 300,
     LQM = 301,
     LEM = 302,
     LSTATEUNHIDE = 303,
     LSTATEHIDE = 304,
     LSTATEBOUND = 305,
     LWHEN = 306,
     LINHERIT = 307,
     LNEW = 308,
     LSPLIT = 309,
     LDELETE = 310,
     LJOIN = 311,
     LLEFTARROW = 312,
     LRIGHTARROW = 313,
     LCHANGED = 314,
     LUPDATE = 315,
     LMIN = 316,
     LIF = 317,
     LTHEN = 318,
     LAND = 319,
     LOR = 320,
     LNOT = 321,
     LBAR = 322,
     LP1 = 323,
     LENDIF = 324,
     LP2 = 325,
     LB1 = 326,
     LB2 = 327,
     LFJOIN = 328,
     LFSPLIT = 329,
     LBOTTOM = 330,
     LIDENTITY = 331,
     LBPARALLEL = 332,
     LTYPE = 333,
     LPIPROCESS = 334,
     LBETAPROCESS = 335,
     LMOLECULE = 336,
     LPREFIX = 337,
     LTEMPLATE = 338,
     LNAME = 339,
     LDIST_NORMAL = 340,
     LDIST_GAMMA = 341,
     LDIST_HYPEREXP = 342,
     LEXPOSE = 343,
     LHIDE = 344,
     LUNHIDE = 345,
     LRUN = 346,
     LBANG = 347
   };
#endif
/* Tokens.  */
#define LID 258
#define LDECIMAL 259
#define LREAL 260
#define LINF 261
#define LSTEPS 262
#define LSTEP 263
#define LAOPEN 264
#define LACLOSE 265
#define LGOPEN 266
#define LGCLOSE 267
#define LNIL 268
#define LBNIL 269
#define LPARALLEL 270
#define LPOPEN 271
#define LPCLOSE 272
#define LCHOICE 273
#define LRESTRICTION 274
#define LSOPEN 275
#define LSCLOSE 276
#define LEQUAL 277
#define LNEQUAL 278
#define LDOT 279
#define LDDOT 280
#define LCOMMA 281
#define LBB 282
#define LBBH 283
#define LDOTCOMMA 284
#define LDAOPEN 285
#define LDACLOSE 286
#define LLET 287
#define LBASERATE 288
#define LREXPOSE 289
#define LRHIDE 290
#define LRUNHIDE 291
#define LDELIM 292
#define LTAU 293
#define LDELTA 294
#define LTIME 295
#define LTIMESPAN 296
#define LCHANGE 297
#define LDIE 298
#define LRCHANGE 299
#define LRATE 300
#define LQM 301
#define LEM 302
#define LSTATEUNHIDE 303
#define LSTATEHIDE 304
#define LSTATEBOUND 305
#define LWHEN 306
#define LINHERIT 307
#define LNEW 308
#define LSPLIT 309
#define LDELETE 310
#define LJOIN 311
#define LLEFTARROW 312
#define LRIGHTARROW 313
#define LCHANGED 314
#define LUPDATE 315
#define LMIN 316
#define LIF 317
#define LTHEN 318
#define LAND 319
#define LOR 320
#define LNOT 321
#define LBAR 322
#define LP1 323
#define LENDIF 324
#define LP2 325
#define LB1 326
#define LB2 327
#define LFJOIN 328
#define LFSPLIT 329
#define LBOTTOM 330
#define LIDENTITY 331
#define LBPARALLEL 332
#define LTYPE 333
#define LPIPROCESS 334
#define LBETAPROCESS 335
#define LMOLECULE 336
#define LPREFIX 337
#define LTEMPLATE 338
#define LNAME 339
#define LDIST_NORMAL 340
#define LDIST_GAMMA 341
#define LDIST_HYPEREXP 342
#define LEXPOSE 343
#define LHIDE 344
#define LUNHIDE 345
#define LRUN 346
#define LBANG 347




#if ! defined (YYSTYPE) && ! defined (YYSTYPE_IS_DECLARED)
#line 48 "ProgParser.y"
typedef union YYSTYPE {
  PTNode *tree_node;
  EventCond* event_cond_node;
  string *ptr_string;
 } YYSTYPE;
/* Line 1447 of yacc.c.  */
#line 228 "ProgParser.hpp"
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif



#if ! defined (YYLTYPE) && ! defined (YYLTYPE_IS_DECLARED)
typedef struct YYLTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
} YYLTYPE;
# define yyltype YYLTYPE /* obsolescent; will be withdrawn */
# define YYLTYPE_IS_DECLARED 1
# define YYLTYPE_IS_TRIVIAL 1
#endif




