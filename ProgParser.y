//Includes and local variable 


%{
  #include <iostream>
  #include <string>
  #include "Define.h"
  #include "PT_General_Node.h"
  #include "PT_Pi_Node.h"
  #include "PT_Action_Node.h"
  #include "PT_Beta_Node.h"
  #include "PT_Mol_Node.h"
  #include "PT_Event_Node.h"
  #include "PT_IfThen_Node.h"
  #include "PT_Template_Node.h"
  #include "Symbol_Table.h"
  #include "Error_Manager.h"
  #include "Environment.h"
  #include "Logger.h"
  #include "ProgParser.h"
  #include "ProgParser.hpp"
  #include "PT_GD_Node.h"
 
  using namespace std;
  
  // ProgLexer forward definitions
  typedef void* yyscan_t;
  void prog_set_in(FILE* in_str, yyscan_t scanner);
  int  prog_lex_init (yyscan_t* scanner);
  int  prog_lex_destroy (yyscan_t scanner); 
  int  prog_lex (YYSTYPE *lvalp, YYLTYPE *llocp, yyscan_t scanner);
  
  void prog_error(YYLTYPE *llocp, Environment* currentEnv, ProgParser* parser, yyscan_t scanner, char const* msg);
  
#ifdef _MSC_VER 
#pragma warning (push,0)
#pragma warning(disable: 4706)
#pragma warning(disable: 4702)
#pragma warning(disable: 4505)

#endif
  
%}

/* Definizione della Sintassi e delle azioni semantiche */

%union
 {
  PTNode *tree_node;
  EventCond* event_cond_node;
  string *ptr_string;
 };

%error-verbose
%locations
%pure-parser
%name-prefix="prog_"
%parse-param {Environment* currentEnv}
%parse-param {ProgParser* parser}
%parse-param { yyscan_t scanner }
%lex-param { yyscan_t scanner }


%token <ptr_string> LID 
%token <ptr_string> LDECIMAL
%token <ptr_string> LREAL
%token <ptr_string> LINF

%token LSTEPS LSTEP LAOPEN LACLOSE LGOPEN LGCLOSE LNIL LBNIL LPARALLEL LPOPEN LPCLOSE 
       LCHOICE LRESTRICTION LSOPEN LSCLOSE LEQUAL LNEQUAL LDOT LDDOT LCOMMA LBB LBBH LDOTCOMMA 
       LDAOPEN LDACLOSE LLET LBASERATE LREXPOSE LRHIDE LRUNHIDE LDELIM LTAU LDELTA
       LTIME LTIMESPAN LCHANGE LDIE LRCHANGE LRATE LQM LEM
       
%token LSTATEUNHIDE LSTATEHIDE LSTATEBOUND
       
%token LWHEN LINHERIT LNEW LSPLIT LDELETE LJOIN LLEFTARROW LRIGHTARROW LCHANGED LUPDATE 
       
%token LMIN LIF LTHEN LAND LOR LNOT LBAR LP1 LENDIF
	   LP2 LB1 LB2 LFJOIN LFSPLIT LBOTTOM LIDENTITY

%token LBPARALLEL
%token LTYPE LPIPROCESS LBETAPROCESS LMOLECULE LPREFIX LTEMPLATE LNAME

%token LDIST_NORMAL LDIST_GAMMA LDIST_HYPEREXP       
       
%token LEXPOSE LHIDE LUNHIDE
%token LRUN 
 
%right LAND 
%right LOR 
%right LNOT
%right LPARALLEL LBPARALLEL 
%right LCHOICE 
%right LPCLOSE 
%right LDOT LCOMMA LDOTCOMMA
%right LBANG

%type <ptr_string> rate
%type <ptr_string> number
%type <ptr_string> betaid
%type <tree_node> rate_dec
%type <tree_node> dec
%type <tree_node> dec_list
%type <tree_node> parelem
%type <tree_node> sumelem
%type <tree_node> par
%type <tree_node> sum
%type <tree_node> seq
%type <tree_node> action
%type <tree_node> betaprocess
%type <tree_node> binder
%type <tree_node> bp
%type <tree_node> molecule 
%type <tree_node> mol_signature 
%type <tree_node> edge_list
%type <tree_node> edge
%type <tree_node> node_list 
%type <tree_node> node 
%type <tree_node> dec_sequence
%type <tree_node> mol_binder
%type <tree_node> mol_binder_list
%type <tree_node> dec_temp_elem
%type <tree_node> dec_temp_list
%type <tree_node> inv_temp_elem
%type <tree_node> inv_temp_list

%type <tree_node> cond
%type <tree_node> verb
%type <tree_node> state_op
%type <tree_node> state_op_list
%type <tree_node> entity
%type <tree_node> entity_list

%type <event_cond_node> cond_atom
%type <event_cond_node> cond_expression

%type <tree_node> hypexp_parameter_list

%type <tree_node> atom
%type <tree_node> expression

%start program

%%

number : 
     LREAL                                                           { $$ = $1; }
   | LDECIMAL                                                        { $$ = $1; }
   ;
   
rate :
     number                                                          { $$ = $1; }
   | LRATE LPOPEN LID LPCLOSE                                        {
                                                                        //a constant
                                                                        if (parser->currentEnv->GetST()->FindConstant(*$3))
                                                                        {                                                                           
                                                                           $$ = new string(parser->currentEnv->GetST()->GetConstantString(*$3));
                                                                           delete ($3);
                                                                        }
                                                                        else
                                                                        {
                                                                           $$ = $3;
                                                                           //Error_Manager::PrintError(Pos(@3.first_line, @3.first_column, parser->GetCurrentFilename()), 23, "The constant '" + *$3 + "' is not defined. Check your function file.");   
                                                                           //parser->control_flag = false;                                                                            
                                                                        }                                                                                                                                       
                                                                     }  
   | LINF                                                            { $$ = $1; }
   ;

program : 
	 info LDAOPEN rate_dec LDACLOSE dec_list LRUN bp                    { parser->root = new PTN_Program(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), $3, $5, $7); }
   | info dec_list LRUN bp                                              { parser->root = new PTN_Program(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), $2, $4); }
   | LDAOPEN rate_dec LDACLOSE dec_list LRUN bp                         { parser->root = new PTN_Program(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), $2, $4, $6); }
   | dec_list LRUN bp                                                   { parser->root = new PTN_Program(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), $1, $3); }

   ;

info:
	LSOPEN LSTEPS LEQUAL LDECIMAL LSCLOSE                               { 
	                                                                       currentEnv->MODE = M_STEP; 
	                                                                       currentEnv->STEP = atoi((*$4).c_str()); 
	                                                                       delete($4); 
	                                                                    }
|	LSOPEN LSTEPS LEQUAL LDECIMAL LCOMMA LDELTA LEQUAL number LSCLOSE   { 
                                                                          currentEnv->MODE = M_STEP_DELTA;
                                                                          currentEnv->STEP = atoi((*$4).c_str()); 
                                                                          currentEnv->DELTA = (double)(atof((*$8).c_str())); 
                                                                          delete($4); 
                                                                          delete($8);
                                                                       }
|	LSOPEN LTIME LEQUAL number LSCLOSE                                  { 
                                                                          currentEnv->MODE = M_TIME;
                                                                          currentEnv->STEP = 1; 
                                                                          currentEnv->E_TIME = (double)(atof((*$4).c_str()));  
                                                                          delete($4); 
                                                                       }
   ;

betaid :
	LID																		{ $$ = $1; }
   | LBNIL                                                     { $$ = new std::string(NIL_BPROC); }
   ;

rate_dec :
     LID LDDOT rate															{ $$ = new PTN_Rate(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),*$1,*$3); delete($1); delete($3); } 
   | LRCHANGE LDDOT rate													{ $$ = new PTN_Rate(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),"CHANGE",*$3); delete($3); }
   | LREXPOSE LDDOT rate													{ $$ = new PTN_Rate(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),"EXPOSE",*$3); delete($3); }
   | LRUNHIDE LDDOT rate													{ $$ = new PTN_Rate(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),"UNHIDE",*$3); delete($3); }
   | LRHIDE LDDOT rate														{ $$ = new PTN_Rate(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),"HIDE",*$3); delete($3); }
   | LBASERATE LDDOT rate													{ $$ = new PTN_Rate(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),"BASERATE",*$3); delete($3); }
   | rate_dec LCOMMA rate_dec												{ $$ = new PTN_Rate_Pair(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),$1,$3); }	
   ;

dec_list: 
     dec                                                       { $$ = $1; }
   | dec dec_list                                              { $$ = new PTN_Dec_Pair(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), $1, $2); }
   ;
		
dec :
     LLET LID LDDOT LPIPROCESS LEQUAL par LDOTCOMMA       { $$ = new PTN_Dec(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), *$2, PPROC, $6); }
   | LLET LID LDDOT LPIPROCESS LEQUAL sum LDOTCOMMA       { $$ = new PTN_Dec(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), *$2, PPROC, $6); }
   | LLET LID LDDOT LBETAPROCESS LEQUAL betaprocess LDOTCOMMA   { $$ = new PTN_Dec(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), *$2, BPROC, $6); }
   | LLET LID LDDOT LBETAPROCESS LEQUAL 
     LID LDAOPEN inv_temp_list LDACLOSE LDOTCOMMA               { $$ = new PTN_Dec(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), *$2, BPROC,
                                                                                  new PTN_BpInv_Template(Pos(@6.first_line, @6.first_column, parser->GetCurrentFilename()), *$6, $8, *$2)); delete($2); delete($6);}
   | LLET LID LDDOT LMOLECULE LEQUAL molecule LDOTCOMMA         { $$ = new PTN_Dec(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), *$2, MOL, $6); }
   | LLET LID LDDOT LPREFIX LEQUAL dec_sequence LDOTCOMMA       { $$ = new PTN_Dec(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), *$2, SEQUENCE, $6); }
   | LWHEN LPOPEN cond LPCLOSE verb LDOTCOMMA                  { $$ = new PTN_Event(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), $3, $5); }
   | LTEMPLATE LID LDDOT LPIPROCESS LDAOPEN dec_temp_list 
     LDACLOSE LEQUAL par LDOTCOMMA                       { $$ = new PTN_Template(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), PPROC, *$2, $6, $9); }
   | LTEMPLATE LID LDDOT LPIPROCESS LDAOPEN dec_temp_list 
     LDACLOSE LEQUAL sum LDOTCOMMA                       { $$ = new PTN_Template(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), PPROC, *$2, $6, $9); }
   | LTEMPLATE LID LDDOT LBETAPROCESS LDAOPEN dec_temp_list 
     LDACLOSE LEQUAL betaprocess LDOTCOMMA                     { $$ = new PTN_Template(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), BPROC, *$2, $6, $9); }
   ;
   
dec_temp_elem :
     LNAME LID                                                  { $$ = new PTN_Dec_Temp_Elem(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), TEMP_NAME, *$2); }
   | LPIPROCESS LID                                             { $$ = new PTN_Dec_Temp_Elem(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), TEMP_PPROC, *$2); }
   | LTYPE LID                                                  { $$ = new PTN_Dec_Temp_Elem(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), TEMP_TYPE, *$2); }
   | LPREFIX LID												{ $$ = new PTN_Dec_Temp_Elem(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), TEMP_SEQ, *$2); } 
   | LRATE LID                                                  { $$ = new PTN_Dec_Temp_Elem(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), TEMP_RATE, *$2); } 
	;
	
dec_temp_list :
		dec_temp_elem										{ $$ = new PTN_Dec_Temp_List(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),$1); }
	|	dec_temp_elem LCOMMA dec_temp_list				   	{ $$ = new PTN_Dec_Temp_List(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),$1,$3); }
	;
	
inv_temp_elem :			
	  LID														{ $$ = new PTN_Inv_Temp_Elem(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), STATE_NOT_SPECIFIED, *$1); delete($1); }								
	| LREAL                                      { $$ = new PTN_Inv_Temp_Elem(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), STATE_NOT_SPECIFIED, *$1); delete($1); }   
	| LID LDAOPEN inv_temp_list LDACLOSE			{
																	$$ = new PTN_Inv_Temp_Elem(
																		Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), STATE_NOT_SPECIFIED, *$1,
																		new PTN_PiInv_Template(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), *$1, $3)); 
																	delete($1); 
																}
	| LPOPEN LID LCOMMA LSTATEUNHIDE LPCLOSE     { $$ = new PTN_Inv_Temp_Elem(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),STATE_UNHIDDEN,*$2); delete($2); }
	| LPOPEN LID LCOMMA LSTATEHIDE LPCLOSE       { $$ = new PTN_Inv_Temp_Elem(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),STATE_HIDDEN,*$2); delete($2); }
	;
	
inv_temp_list :
      inv_temp_elem                             { $$ = new PTN_Inv_Temp_List(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),$1); }
   |  inv_temp_elem LCOMMA inv_temp_list        { $$ = new PTN_Inv_Temp_List(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),$1,$3); } 
   ;

//for declaration of a sequence of actions NOT ended by a pi process:
//this kind of sequence is NOT a pi-process, but it is useful for putting them in templates with
//"pieces" of sequences in common	
dec_sequence:
     action                                                    { $$ = new PTN_Action_List(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),$1); }
   | action LDOT dec_sequence                                  { $$ = new PTN_Action_List(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),$1, $3); }
   ;
  
state_op:
     LID LLEFTARROW number                                     { $$ = new PTN_EventCond_State(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), *$1, *$3, STATE_UP); delete($1); delete($3); }
   | LID LRIGHTARROW number                                    { $$ = new PTN_EventCond_State(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), *$1, *$3, STATE_DOWN); delete($1); delete($3); }
   ;
   
state_op_list:
     state_op                                                  { $$ = new PTN_EventCond_StateList(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), $1); }
   | state_op LCOMMA state_op_list                             { $$ = new PTN_EventCond_StateList(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), $1, $3); }
   ;
   
entity:
     LID                                                       { $$ = new PTN_Event_Entity(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), *$1); delete($1); }
   ;
   
entity_list:
     entity                                                    { $$ = new PTN_Event_EntityList(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), $1); }
   | entity LCOMMA entity_list                                 { $$ = new PTN_Event_EntityList(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), $1, $3); }
   ;
   
cond_atom:
     LPARALLEL LID LPARALLEL LEQUAL LDECIMAL						{ $$ = new EventCond_AtomCount(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), COND_COUNT_EQUAL,   *$2, *$5); delete($2); delete($5); }
   | LPARALLEL LID LPARALLEL LACLOSE LDECIMAL					{ $$ = new EventCond_AtomCount(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), COND_COUNT_GREATER, *$2, *$5); delete($2); delete($5); }
   | LPARALLEL LID LPARALLEL LAOPEN LDECIMAL						{ $$ = new EventCond_AtomCount(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), COND_COUNT_LESS,    *$2, *$5); delete($2); delete($5); }
   | LPARALLEL LID LPARALLEL LNEQUAL LDECIMAL					{ $$ = new EventCond_AtomCount(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), COND_COUNT_NEQUAL,  *$2, *$5); delete($2); delete($5); }
   | LTIME LEQUAL LREAL											{ $$ = new EventCond_AtomDet(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),   COND_TIME_EQUAL,    *$3); delete($3);  }
   | LSTEP LEQUAL LDECIMAL										{ $$ = new EventCond_AtomDet(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),   COND_STEP_EQUAL,    *$3); delete($3);  }
   | state_op_list												{ $$ = new EventCond_AtomStates(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), COND_STATES,        $1); }
   ;
   
cond_expression:
     cond_atom													{ $$ = $1; }
   | cond_expression LAND cond_expression						{ $$ = new EventCond_And(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),$1,$3); }
   | cond_expression LOR cond_expression						{ $$ = new EventCond_Or(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),$1,$3); }
   | LNOT cond_expression										{ $$ = new EventCond_Not(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),$2); }
   | LPOPEN cond_expression LPCLOSE								{ $$ = $2; }	
   ;

cond :
     entity_list LDDOT cond_expression LDDOT rate              { $$ = new PTN_Event_Cond(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), COND_RATE,           $1, $3, *$5); delete($5); }
   | entity_list LDDOT cond_expression LDDOT LID               { $$ = new PTN_Event_Cond(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), COND_RATE_FUN,       $1, $3, *$5); delete($5); }
   | entity_list LDDOT cond_expression LDDOT                   { $$ = new PTN_Event_Cond(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), COND_RATE_IMMEDIATE, $1, $3, INF_RATE); }
   | entity_list LDDOT cond_expression LDDOT LDIST_GAMMA LPOPEN number LCOMMA number LPCLOSE	{ $$ = new PTN_Event_Cond(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), COND_RATE_GAMMA,$1,$3,*$7,*$9); delete($7);delete($9); }
   | entity_list LDDOT cond_expression LDDOT LDIST_HYPEREXP LPOPEN hypexp_parameter_list LPCLOSE	{ $$ = new PTN_Event_Cond(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), COND_RATE_HYPEXP,$1,$3,$7); delete($7);  }
   | entity_list LDDOT LDDOT rate                              { $$ = new PTN_Event_Cond(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), COND_RATE,           $1, NULL, *$4); delete($4); }
   | entity_list LDDOT LDDOT LID                               { $$ = new PTN_Event_Cond(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), COND_RATE_FUN,       $1, NULL, *$4); delete($4); }
   | entity_list LDDOT LDDOT LDIST_GAMMA LPOPEN number LCOMMA number LPCLOSE	{ $$ = new PTN_Event_Cond(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), COND_RATE_GAMMA,$1,NULL,*$6,*$8); delete($6);delete($8); }
   | entity_list LDDOT LDDOT LDIST_HYPEREXP LPOPEN hypexp_parameter_list LPCLOSE	{ $$ = new PTN_Event_Cond(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), COND_RATE_HYPEXP,$1,NULL,$6); }
   | LDDOT cond_expression LDDOT                               { $$ = new PTN_Event_Cond(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), COND_RATE_IMMEDIATE, NULL, $2, INF_RATE); }
   ;

hypexp_parameter_list:
		LPOPEN number LCOMMA number LPCLOSE										{ $$ = new PTN_HypExp_Parameter(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),NULL,*$2,*$4); delete($2); delete($4);}
	|	LPOPEN number LCOMMA number LPCLOSE LCOMMA hypexp_parameter_list		{ $$ = new PTN_HypExp_Parameter(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),$7,*$2,*$4); delete($2); delete($4);}


verb :
     LSPLIT LPOPEN betaid LCOMMA betaid LPCLOSE                { $$ = new PTN_Event_Verb(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), *$3, *$5, VERB_SPLIT); delete($3); delete($5); }
   | LNEW LPOPEN LDECIMAL LPCLOSE                              {  
                                                                  int count = atoi((*$3).c_str());
                                                                  delete $3;
                                                                  $$ = new PTN_Event_Verb(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), count, VERB_NEW); 
                                                               }
   | LDELETE LPOPEN LDECIMAL LPCLOSE                           {  
                                                                  int count = atoi((*$3).c_str());
                                                                  delete $3;
                                                                  $$ = new PTN_Event_Verb(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), count, VERB_DELETE); 
                                                               }
   | LNEW                                                      { $$ = new PTN_Event_Verb(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), VERB_NEW); }
   | LDELETE                                                   { $$ = new PTN_Event_Verb(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), VERB_DELETE); }
   | LJOIN LPOPEN betaid LPCLOSE                                   { $$ = new PTN_Event_Verb(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), *$3, VERB_JOIN); delete($3); }
   | LJOIN                                                     { $$ = new PTN_Event_Verb(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), VERB_JOIN); }
   | LUPDATE LPOPEN LID LCOMMA LID LPCLOSE                       { $$ = new PTN_Event_Verb(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), *$3, *$5, VERB_UPDATE); }
   ;

sumelem:
     LNIL                                                  { $$ = new PTN_Pi_Nil(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename())); }
   | seq                                                   { $$ = $1; } 
   | LIF expression LTHEN sum LENDIF                       { $$ = new PTN_IfThen_Node(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),$2,$4); }
   ;

parelem:
	LID															{ $$ = new PTN_Pi_Id(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),*$1); delete($1); }
|	LID LDAOPEN inv_temp_list LDACLOSE							{ $$ = new PTN_PiInv_Template(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), *$1, $3); delete($1); }
|	LBANG action LDOT par										{ $$ = new PTN_Pi_Replication(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),$2,$4); }
|	LBANG action LDOT sum										{ $$ = new PTN_Pi_Replication(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),$2,$4); }
|	LIF expression LTHEN par LENDIF								{ $$ = new PTN_IfThen_Node(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),$2,$4); }
;

sum:
    sumelem                                               { $$ = $1; }
   //|	sum LCHOICE sumelem                              { $$ = new PTN_Pi_Choice(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), $1, $3); } 
   | sum LCHOICE sum											{ $$ = new PTN_Pi_Choice(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), $1, $3); } 
   | LPOPEN sum LPCLOSE											{ $$ = $2; }
   ;

par:
	parelem														{ $$ = $1; }
|	sum LPARALLEL sum											{ $$ = new PTN_Pi_Parallel(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),$1,$3); }
|	sum LPARALLEL par											{ $$ = new PTN_Pi_Parallel(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),$1,$3); }
|   par LPARALLEL sum											{ $$ = new PTN_Pi_Parallel(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),$1,$3); }	
|   par LPARALLEL par											{ $$ = new PTN_Pi_Parallel(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),$1,$3); }	
|	LPOPEN par LPCLOSE											{ $$ = $2; }
   ;
   
seq:
    action											            { $$ = new PTN_Pi_Action(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),$1, new PTN_Pi_Nil(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()))); }
|	action LDOT par												{ $$ = new PTN_Pi_Action(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),$1,$3); }
|	action LDOT sum												{ $$ = new PTN_Pi_Action(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),$1,$3); }
|	LID LDOT par												{ $$ = new PTN_Pi_Action(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), new PTN_Pi_Id(Pos(@1.first_line, @1.first_column, parser->GetCurrentFilename()), *$1), $3); delete($1); }
|	LID LDOT sum												{ $$ = new PTN_Pi_Action(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), new PTN_Pi_Id(Pos(@1.first_line, @1.first_column, parser->GetCurrentFilename()), *$1), $3); delete($1); }
   ;
   
   
atom:
	LPOPEN LID LCOMMA LID LPCLOSE										{ $$ = new PTN_Atom_IfThen_Node(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),*$2,*$4,STATE_NOT_SPECIFIED); delete($2); delete($4); }
|	LPOPEN LID LCOMMA LSTATEHIDE LPCLOSE								{ $$ = new PTN_Atom_IfThen_Node(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),*$2,EMPTY_STRING,STATE_HIDDEN); delete($2);}
|	LPOPEN LID LCOMMA LSTATEUNHIDE LPCLOSE								{ $$ = new PTN_Atom_IfThen_Node(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),*$2,EMPTY_STRING,STATE_UNHIDDEN); delete($2); }
|	LPOPEN LID LCOMMA LSTATEBOUND LPCLOSE								{ $$ = new PTN_Atom_IfThen_Node(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),*$2,EMPTY_STRING,STATE_BOUND); delete($2); }
|	LPOPEN LID LCOMMA LID LCOMMA LSTATEHIDE LPCLOSE						{ $$ = new PTN_Atom_IfThen_Node(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),*$2,*$4,STATE_HIDDEN); delete($2); delete($4); }
|	LPOPEN LID LCOMMA LID LCOMMA LSTATEUNHIDE LPCLOSE					{ $$ = new PTN_Atom_IfThen_Node(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),*$2,*$4,STATE_UNHIDDEN); delete($2); delete($4); }
|	LPOPEN LID LCOMMA LID LCOMMA LSTATEBOUND LPCLOSE					{ $$ = new PTN_Atom_IfThen_Node(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),*$2,*$4,STATE_BOUND); delete($2); delete($4); }
;

expression:
	atom																{ $$ = $1; }
|   expression LAND expression											{ $$ = new PTN_And_IfThen_Node(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),$1,$3); }
|   expression LOR expression											{ $$ = new PTN_Or_IfThen_Node(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),$1,$3); }
|   LNOT expression														{ $$ = new PTN_Not_IfThen_Node(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),$2); }
|   LPOPEN expression LPCLOSE											{ $$ = $2; }	
;

action :
	LTAU LPOPEN rate LPCLOSE											{ $$ = new PTN_Action_Tau(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),*$3); delete($3); }
|	LID LEM LPOPEN LID LPCLOSE											{ $$ = new PTN_Action_IO(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),*$1,*$4, ACTION_OUTPUT); delete($1); delete($4); }
|	LID LEM LPOPEN LPCLOSE												{ $$ = new PTN_Action_IO(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),*$1,EMPTY_OUTPUT, ACTION_OUTPUT); delete($1); }
|	LID LQM LPOPEN LPCLOSE												{ $$ = new PTN_Action_IO(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),*$1,"", ACTION_INPUT); delete($1); }
|	LID LQM LPOPEN LID LPCLOSE											{ $$ = new PTN_Action_IO(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),*$1,*$4, ACTION_INPUT); delete($1); delete($4); }
|	LEXPOSE LPOPEN rate LCOMMA LID LDDOT rate LCOMMA LID LPCLOSE			{ $$ = new PTN_Action_Expose(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),*$5,*$3,*$7,*$9); delete($3); delete($5);}
|	LEXPOSE LPOPEN LID LDDOT rate LCOMMA LID LPCLOSE						{ $$ = new PTN_Action_Expose(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),*$3,"",*$5,*$7); delete($3); delete($5); delete($7); }
| 	LHIDE LPOPEN LID LPCLOSE												{ $$ = new PTN_Action_HU(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),*$3,"", ACTION_HIDE); delete($3); }
|	LUNHIDE LPOPEN LID LPCLOSE											{ $$ = new PTN_Action_HU(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),*$3,"", ACTION_UNHIDE); delete($3); }
| 	LHIDE LPOPEN rate LCOMMA LID LPCLOSE									{ $$ = new PTN_Action_HU(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),*$5,*$3, ACTION_HIDE); delete($3); delete($5); }
|	LUNHIDE LPOPEN rate LCOMMA LID LPCLOSE								{ $$ = new PTN_Action_HU(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),*$5,*$3, ACTION_UNHIDE); delete($3); delete($5); }
|	LCHANGE LPOPEN rate LCOMMA LID LCOMMA LID LPCLOSE					{ $$ = new PTN_Action_Change(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),*$3,*$5,*$7); delete($3); delete($5); delete($7); }
|	LCHANGE LPOPEN LID LCOMMA LID LPCLOSE								{ $$ = new PTN_Action_Change(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),"",*$3,*$5); delete($3); delete($5); }
|	LDIE																{ $$ = new PTN_Action_Die(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),""); }
|	LDIE LPOPEN rate LPCLOSE											{ $$ = new PTN_Action_Die(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),*$3); delete($3); }
;

betaprocess :
 	binder LSOPEN par LSCLOSE										{ $$ = new PTN_Beta(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),$1,$3); }
 |	binder LSOPEN sum LSCLOSE										{ $$ = new PTN_Beta(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),$1,$3); }
;

binder :
     LBB LPOPEN LID LDDOT rate LCOMMA LID LPCLOSE                 { $$ = new PTN_Binder(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),*$3,*$7, STATE_UNHIDDEN, *$5); delete($3); delete($5); delete($7); }
   | LBBH LPOPEN LID LDDOT rate LCOMMA LID LPCLOSE                { $$ = new PTN_Binder(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),*$3,*$7, STATE_HIDDEN, *$5); delete($3); delete($5); delete($7);}
   | LBB LPOPEN LID LCOMMA LID LPCLOSE                            { $$ = new PTN_Binder(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),*$3,*$5, STATE_UNHIDDEN, "0.0"); delete($3); delete($5); }
   | LBBH LPOPEN LID LCOMMA LID LPCLOSE                           { $$ = new PTN_Binder(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),*$3,*$5, STATE_HIDDEN, "0.0"); delete($3); delete($5); }
|	binder LCOMMA binder												{ $$ = new PTN_Binder_Pair(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),$1,$3); }
;


bp:
	LDECIMAL LID														{ $$ = new PTN_Bp(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),*$2,*$1); delete($1); delete($2); }
|	LID LID															   { 
                                                            Pos pos(@$.first_line, @$.first_column, parser->GetCurrentFilename());
                                                            std::string count_string("0");
                                                            
                                                            if (!parser->currentEnv->GetST()->FindConstant(*$1))
                                                            {
                                                               // Raise error: not a constant
                                                               
                                                               Error_Manager::PrintError(pos, 45, "'" + *$1 + "' is not a valid constant");
                                                               parser->control_flag = false;
                                                            }
                                                            else
                                                            {
                                                               double constant = parser->currentEnv->GetST()->GetConstant(*$1);
                                                               double intpart = 0.0;
                                                               double fracpart = modf(constant, &intpart);
                                                               
                                                               count_string = Utility::i2s((int)intpart);
                                                               
                                                               if (fracpart != 0.0)
                                                               {
                                                                  // warn if not a decimal value         
                                                                  Error_Manager::PrintWarning(pos, 45, "'" + *$1 + "' has a non-integral part, and will be truncated to " + count_string);                                                         
                                                               } 
                                                            }
                                                            
                                                            $$ = new PTN_Bp(pos, *$2, count_string); 
                                                            delete($1); delete($2);                                                             
                                                         }
|	LDECIMAL LID LDAOPEN inv_temp_list LDACLOSE           { $$ = new PTN_BpInv_Template(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), *$2, *$1, $4); delete($1); delete($2);}
|	bp LBPARALLEL bp													{ $$ = new PTN_Bp_Pair(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),$1,$3); }
;

molecule :
     LGOPEN mol_signature LDOTCOMMA node_list LGCLOSE                { $$ = new PTN_Molecule(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), $2, $4); }
   ;
  

mol_signature : 
     LPOPEN edge_list LPCLOSE                                        { $$ = $2; }
   ;
   
edge_list : 
     edge                                                            { $$ = new PTN_MolEdge_List(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), $1); }
   | edge LCOMMA edge_list                                           { $$ = new PTN_MolEdge_List(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), $1, $3); }
   ; 

edge : 
     LPOPEN LID LCOMMA LID LCOMMA LID LCOMMA LID LPCLOSE                 { $$ = new PTN_MolEdge(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), *$2, *$4, *$6, *$8); };
   
node_list :
     node                                                            { $$ = new PTN_MolNode_List(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), $1); }
   | node node_list                                                  { $$ = new PTN_MolNode_List(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), $1, $2); }
   ;

node : 
     LID LDDOT LID LEQUAL LPOPEN mol_binder_list LPCLOSE LDOTCOMMA     { $$ = new PTN_MolNode(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), *$1, *$3, $6); }
   | LID LEQUAL LID LDOTCOMMA                                          { $$ = new PTN_MolNode(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), *$1, *$3); }
   ;
   
mol_binder : 
     LID																 { $$ = new PTN_MolBinder(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), *$1); }
   ;
     
mol_binder_list :
     mol_binder															{ $$ = new PTN_MolBinder_List(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), $1); } 
   | mol_binder LCOMMA mol_binder_list									{ $$ = new PTN_MolBinder_List(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()), $1, $3); } 
   ;
		
%%

// Syntax error message 
void prog_error(YYLTYPE *llocp, Environment* currentEnv, ProgParser* parser, yyscan_t scanner, char const* msg)
{
   Pos pos(llocp->first_line, llocp->first_column, parser->currentFilename);
   Error_Manager::PrintError(pos, 0, msg);
}

// Parse the file with name "file_name"
int ProgParser::Parse()
{
   control_flag = true;
   int res;
   string validate;
   const char* file_name = currentFilename.c_str();
   
  
   FILE* prog_file = fopen(file_name,"r+");
   if (prog_file == NULL) 
   {
      Logger::GetInstance().log("PROGRAM FILE: cannot open.");
      Error_Manager::PrintError(0, "Cannot open program file '" + currentFilename + "'");
      return -1;
   }
   
   yyscan_t scanner;
   prog_lex_init(&scanner);
   prog_set_in(prog_file, scanner);
   
   //scanner.Init(prog_file);

   res = prog_parse(currentEnv, this, scanner);
   if (res == 0 && (control_flag))
   {
      Logger::GetInstance().log("PROGRAM FILE: parsed.");
   }
   else
   {
      Logger::GetInstance().log("PROGRAM FILE: parsing failed.");
      return -1;
   }
  
   if (root->GenerateST(currentEnv->GetST()))
      Logger::GetInstance().log("SYMBOL TABLE: generated.");
   else
   {
      Logger::GetInstance().log("SYMBOL TABLE: failed.");
      return -1;
   }
   
 
   if (currentEnv->GetST()->Validate())
      Logger::GetInstance().log("VALIDATION: ok.");
   else
   {
      Logger::GetInstance().log("VALIDATION: failed.");
      return -1;
   }
 
   if (currentEnv->GetST()->GenerateIC())
      Logger::GetInstance().log("INTERMEDIATE CODE: generated.");
   else
   {
      Logger::GetInstance().log("GENERATE IC: failed.");
      return -1;
   }

   if (currentEnv->GetST()->InitAM())
      Logger::GetInstance().log("ENVIRONMENT: initialized.");
   else
   {
	   Logger::GetInstance().log("INITAM: failed.");
	   return -1;
   }
 
   prog_lex_destroy(scanner);
   fclose(prog_file);      
   return res;
}


#ifdef _MSC_VER 
#pragma warning (pop)
#pragma warning(default : 4706)
#pragma warning(default : 4702)
#pragma warning(default : 4505) 
#endif

