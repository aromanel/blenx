
## DESCRIPTION

BlenX is a language based on the process calculi and rule-based paradigms and it is specifically designed to account for the complexity of signalling networks. The *Beta Workbench (BetaWB)* is a collection of tools built on top of the *BlenX language*. The BetaWB includes the *BlenX simulator*, a network-free stochastic simulator based on an efficient variant of the Gillespie's stochastic algorithm. BlenX and BetaWB have been designed and implemented in collaboration with Lorenzo Dematté. A module to convert BlenX models into SBML descriptions has been implemented by Roberto Larcher. 

## MAIN REFERENCES
* Lorenzo Dematté, Corrado Priami, Alessandro Romanel. *The Beta Workbench: a computational tool to study the dynamics of biological systems*. Briefings in Bioinformatics 9(5):437-449, Oxford Journals, 2008. (here you find a presentation of the BetaWB)
 
* Lorenzo Dematté, Roberto Larcher, Alida Palmisano, Corrado Priami, Alessandro Romanel. *Programming Biology in BlenX*.  In Systems Biology for Signaling Networks 1:777-820, Ed. Sangdun Choi, © Springer, 2010. (here you find an informal presentation of the BlenX language and two case studies)
 
* Alessandro Romanel. *Dynamic Biological Modelling: a language-based approach*. PhD thesis , University of Trento, 2010. (here you find a formal presentation of the BlenX language and its abstract machine, details on its implementation and some case studies)
