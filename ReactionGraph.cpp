
#include "Define.h"

#include<cassert>
#include<cmath>
#include<iostream>


#include <sbml/SBMLDocument.h>
#include <sbml/Species.h>
#include <sbml/KineticLaw.h>
#include <sbml/Reaction.h>
#include <sbml/SpeciesReference.h>
#include <sbml/Model.h>
#include <sbml/Parameter.h>
#include <sbml/Compartment.h>

#include "ReactionGraph.h"

using namespace std;

ReactionNode::ReactionNode(int p_id){
	id = p_id;
	is_deleted = false;
}

ReactionNode::~ReactionNode(){
}

void ReactionNode::ReduceRateList(list<double> *l){
	list<double>::iterator it;
	for (it = l->begin(); it != l->end(); it++){
		if (*it == HUGE_VAL){
			l->clear();
			l->push_back(HUGE_VAL);
			return;
		}
	}
}


void ReactionNode::RemoveReactant(ReactionNode *node){
	if (node->GetId() % 2 == 0){
		//In this case node is a MoleculeNode
		mono_reactants.remove((MoleculeNode *) node);
	} else {
		//Int this case node is a MultiNode
		multi_reactants.remove((MultiNode *)node);
	}
}

void ReactionNode::RemoveOneReactant(ReactionNode *node){
	if (node->GetId() % 2 == 0){
		//In this case node is a MoleculeNode
		list<MoleculeNode *>::iterator it;
		for(it = mono_reactants.begin(); it != mono_reactants.end(); it++){
			if (node == *it){
				mono_reactants.erase(it);
				return;
			}
		}		
	} else {
		//Int this case node is a MultiNode
		list<MultiNode *>::iterator it;
		for(it = multi_reactants.begin(); it != multi_reactants.end(); it++){
			if (node == *it){
				multi_reactants.erase(it);
				return;
			}
		}	
	}
}


void ReactionNode::AddReactant(ReactionNode *node){
	if (node->GetId() % 2 == 0){
		mono_reactants.push_back((MoleculeNode*)node);
	} else {
		multi_reactants.push_back((MultiNode*)node);
	}
}

void ReactionNode::RemoveProduct(ReactionNode *node){
	if (node->GetId() % 2 == 0){
		//In this case node is a MoleculeNode
		list<pair <MoleculeNode *, list<double> *> >::iterator it;
		list<pair <MoleculeNode *, list<double> *> >::iterator to_be_deleted;
		for (it = this->mono_products.begin(); it != this->mono_products.end();){
			if ((*it).first->GetId() == node->GetId()){
				to_be_deleted = it;
				it++;
				delete (*to_be_deleted).second;
				mono_products.erase(to_be_deleted);
			} else {
				it++;
			}
		}
	} else {
		//Int this case node is a MultiNode
		multi_products.remove((MultiNode *)node);
	}
}

void ReactionNode::RemoveOneProduct(ReactionNode *node){
	if (node->GetId() % 2 == 0){
		//In this case node is a MoleculeNode
		list<pair <MoleculeNode *, list<double> *> >::iterator it;
		for (it = this->mono_products.begin(); it != this->mono_products.end();it++){
			if ((*it).first->GetId() == node->GetId()){
				delete it->second;
				mono_products.erase(it);
				return;
			}
		}
	} else {
		//Int this case node is a MultiNode
		list<MultiNode *>::iterator it;
		for (it = multi_products.begin(); it != multi_products.end();it++){
			if ((*it)->GetId() == node->GetId()){
				multi_products.erase(it);
				return;
			}
		}

	}
}




void ReactionNode::AddProduct(ReactionNode *node, list<double> *rates){
	if (node->GetId() % 2 == 0){
		pair<MoleculeNode *, list <double >* > p;

		p.first = (MoleculeNode*)node;
		p.second = rates;
		mono_products.push_back(p);
	} else {
		multi_products.push_back((MultiNode*)node);
	}
}

void ReactionNode::UpdateNodesToBeVisited(list<ReactionNode *> *nodesToBeVisited, hash_map<ReactionNode *, bool>  *visitedNodes){
	if (visitedNodes->find(this) == visitedNodes->end()){
		nodesToBeVisited->insert(nodesToBeVisited->end(), this->mono_reactants.begin(), this->mono_reactants.end());
		nodesToBeVisited->insert(nodesToBeVisited->end(), this->multi_reactants.begin(), this->multi_reactants.end());
	}	  
}


MoleculeNode::MoleculeNode(int p_id):ReactionNode(p_id){
	present_in_run = 0;
}

void MoleculeNode::Print(Environment *e){
	list<pair<MoleculeNode *, list<double> *> >::iterator n_it;
	list<double>::iterator r_it;
	int node_id;
	
	for (n_it = mono_products.begin(); n_it != mono_products.end(); n_it++){
		if (id < 0){
			cout << e->GetST()->GetMolNameFromID(abs(id/2)) << " ";
		} else {
			cout << e->GetST()->GetEntNameFromID(id/2) << " ";
		}

		cout << " --> ";

		node_id = (*n_it).first->GetId()/2;

		if (node_id < 0){
			cout << e->GetST()->GetMolNameFromID(abs(node_id)) << " ";
		} else {
			cout << e->GetST()->GetEntNameFromID(node_id) << " ";
		}

		for (r_it = (*n_it).second->begin(); r_it != (*n_it).second->end(); r_it++){
			cout << *r_it << " ";
		}
		cout << endl;
	}

}

void MoleculeNode::ReversePrint(Environment *e){
	list<MoleculeNode *>::iterator n_it;
	list<double>::iterator r_it;
	int node_id;
	
	for (n_it = mono_reactants.begin(); n_it != mono_reactants.end(); n_it++){
		node_id = (*n_it)->GetId()/2;

		if (node_id < 0){
			cout << e->GetST()->GetMolNameFromID(abs(node_id)) << " ";
		} else if (node_id > 0) {
			cout << e->GetST()->GetEntNameFromID(node_id) << " ";
		} else {
			cout << "Nil ";
		}


		cout << " --> ";

		if (id < 0){
			cout << e->GetST()->GetMolNameFromID(abs(id/2)) << " ";
		} else {
			cout << e->GetST()->GetEntNameFromID(id/2) << " ";
		}
		/*for (r_it = (*n_it).second->begin(); r_it != (*n_it).second->end(); r_it++){
			cout << *r_it << " ";
		}*/
		cout << endl;
	}

}


void  MoleculeNode::PrintDot(Environment * e, ofstream &fout){
	list<pair<MoleculeNode *, list<double> *> >::iterator n_it;
	list<double>::iterator r_it;
	int node_id;

	for (n_it = mono_products.begin(); n_it != mono_products.end(); n_it++){
		if (id < 0){
			fout << e->GetST()->GetMolNameFromID(abs(id/2)) << " ";
		} else {
			fout << e->GetST()->GetEntNameFromID(id/2) << " ";
		}

		fout << " -> ";

		node_id = (*n_it).first->GetId()/2;

		if (node_id < 0){
			fout << e->GetST()->GetMolNameFromID(abs(node_id)) << " ";
		} else if (node_id > 0) {
			fout << e->GetST()->GetEntNameFromID(node_id) << " ";
		} else {
			fout << "Nil ";
		}

		fout << "[color=darkgreen,label=\"";

		for (r_it = (*n_it).second->begin(); r_it != (*n_it).second->end(); r_it++){
			fout << *r_it << " ";
		}
		fout << "\"];" << endl;
	}

}

void MoleculeNode::InsertSBMLSpecie(Model_t *model, Environment *env){
	string molecule_name;
	if (this->id > 0)
		molecule_name = env->GetST()->GetEntNameFromID(this->id / 2);
	else if (this->id < 0){
		molecule_name = env->GetST()->GetMolNameFromID(abs(this->id / 2));
	} else {
		molecule_name = "Nil";
	}

	Species_t *specie =	Model_createSpecies(model);
	Species_setId(specie, molecule_name.c_str());
	Species_setName(specie, molecule_name.c_str());
	Species_setCompartment(specie, "compartment");
	Species_setInitialAmount(specie, this->present_in_run);
	Species_setBoundaryCondition(specie, false);
}


		
void MoleculeNode::InsertSBMLReactions(Model_t *model, Environment *env, int &constants_counter, int &reactions_counter){
	list<pair<MoleculeNode *, list<double> *> >::iterator products_it;
	list<double>::iterator rates_it;
	
	int id;
	Reaction_t *reaction;
	KineticLaw_t *kinetic_law;
	Parameter_t *parameter;
	SpeciesReference_t *reference1;
	SpeciesReference_t *reference2;
	stringstream stream;
	string reactant;

	for (products_it = mono_products.begin(); products_it != mono_products.end(); products_it++){
		for (rates_it = (*products_it).second->begin(); rates_it != (*products_it).second->end(); rates_it++){

			reaction = Model_createReaction(model);
			stream << "R" << reactions_counter++;
			Reaction_setId(reaction, stream.str().c_str());
			stream.str("");
			Reaction_setReversible(reaction, false);

			kinetic_law = Model_createKineticLaw(model);
			parameter = Model_createKineticLawParameter(model);

			stream << "c" << constants_counter++;
			Parameter_setName(parameter, stream.str().c_str());
			Parameter_setId(parameter, stream.str().c_str());
			Parameter_setValue(parameter, *rates_it);
			stream.str("");

			id = this->id/2;
			if (id > 0){
				stream << env->GetST()->GetEntNameFromID(id);
			} else if (id < 0){
				stream << env->GetST()->GetMolNameFromID(abs(id));
			} else {
				//reactant should never be Nil
				assert(false);
				stream << "Nil";
			}

			reactant = stream.str().c_str();

			stream << " * " << Parameter_getName(parameter);

			KineticLaw_setFormula(kinetic_law, stream.str().c_str());
			stream.str("");

			Reaction_setKineticLaw(reaction, kinetic_law);

			id = (*products_it).first->id/2;
			if (id > 0){
				stream << env->GetST()->GetEntNameFromID(id);
			} else if (id < 0){
				stream << env->GetST()->GetMolNameFromID(abs(id));
			} else {
				stream << "Nil";
			}


			reference1 = Model_createReactant(model);
			SpeciesReference_initDefaults(reference1);
			SpeciesReference_setSpecies(reference1, reactant.c_str());
			reference2 = Model_createProduct(model);
			SpeciesReference_initDefaults(reference2);
			SpeciesReference_setSpecies(reference2, stream.str().c_str());

			stream.str("");

		}
	}
}
void MultiNode::InsertSBMLReactions(Model_t *model, Environment *env, int &constants_counter, int &reactions_counter){
	list<double>::iterator rates_it;
	list<MoleculeNode *>::iterator reactants_it;
	list<MoleculeNode *>::iterator tmp_it;
	list<pair<MoleculeNode *, list<double> *> >::iterator products_it;
	hash_map<int, pair<int, int> > products_map;
	hash_map<int, pair<int, int> >::iterator products_map_it;
	pair <int, int> int_pair;
	int tmp_int;

	MoleculeNode* firstReactant;
	MoleculeNode* secondReactant;

	Reaction_t *reaction;
	KineticLaw_t *kinetic_law;
	Parameter_t *parameter;
	SpeciesReference_t *reference;

	int id, index;

	stringstream stream;

	string first_name;
	string second_name;

	string *products_name;

	//0 means monomolecular
	//1 means bimolecular
	//2 means homodimerization  
	int reaction_kind = -1;

	//cases with more than two reactants are not treated
	assert(mono_reactants.size() > 0 || mono_reactants.size() <= 2);


	tmp_it = mono_reactants.begin();
	id = (*tmp_it)->GetId()/2;
	if (id > 0){
		stream << env->GetST()->GetEntNameFromID(id);
	} else if (id < 0){
		stream << env->GetST()->GetMolNameFromID(abs(id));
	} else {
		//reactant should never be Nil
		assert(false);
		stream << "Nil";
	}

	first_name = stream.str();
	stream.str("");
	

	if (mono_reactants.size() == 1){
		reaction_kind = 0;
	} else if (mono_reactants.size() == 2){
		tmp_it = mono_reactants.begin();
		firstReactant = *tmp_it;
		tmp_it++;
		secondReactant = *(tmp_it);
		id = (*tmp_it)->GetId()/2;
		if (id > 0){
			stream << env->GetST()->GetEntNameFromID(id);
		} else if (id < 0){
			stream << env->GetST()->GetMolNameFromID(abs(id));
		} else {
			//reactant should never be Nil
			assert(false);
			stream << "Nil";
		}

		second_name = stream.str();
		stream.str("");

		if (secondReactant != firstReactant){
			reaction_kind  = 1;
		} else {
			reaction_kind = 2;
		}
	}

	products_name = new string[mono_products.size()];


	for (index = 0, products_it = mono_products.begin(); products_it != mono_products.end(); products_it++, index++){
		id = (*products_it).first->GetId()/2;
		if (id > 0){
			stream << env->GetST()->GetEntNameFromID(id);
		} else if (id < 0){
			stream << env->GetST()->GetMolNameFromID(abs(id));
		} else {
			stream << "Nil";
		}

		products_name[index] = stream.str();
		stream.str("");
	}



	for (rates_it = this->rates->begin(); rates_it != this->rates->end(); rates_it++){

		//for (reactants_it = mono_reactants.begin(); reactants_it != mono_reactants.end(); reactants_it++){
			reaction = Model_createReaction(model);
			stream << "R" << reactions_counter++;
			Reaction_setId(reaction, stream.str().c_str());
			stream.str("");
			Reaction_setReversible(reaction, false);

			kinetic_law = Model_createKineticLaw(model);
			parameter = Model_createKineticLawParameter(model);

			stream << "c" << constants_counter++;
			Parameter_setName(parameter, stream.str().c_str());
			Parameter_setId(parameter, stream.str().c_str());
			Parameter_setValue(parameter, *rates_it);
			stream.str("");


			if (reaction_kind == 0){
				stream << first_name << " * " << Parameter_getName(parameter);
			} else if (reaction_kind == 1){
				stream << first_name << " * " << second_name << " * " << Parameter_getName(parameter);
			} else {
				stream << "(" <<  Parameter_getName(parameter) << ")" << "/2 * " << first_name << " * " << "(" << first_name << " - 1)";
			}

			KineticLaw_setFormula(kinetic_law, stream.str().c_str());
			stream.str("");

			Reaction_setKineticLaw(reaction, kinetic_law);

			if (reaction_kind == 0){
				reference = Model_createReactant(model);
				SpeciesReference_initDefaults(reference);
				SpeciesReference_setSpecies(reference, first_name.c_str());
			} else if (reaction_kind == 1){
				reference = Model_createReactant(model);
				SpeciesReference_initDefaults(reference);
				SpeciesReference_setSpecies(reference, first_name.c_str());
				reference = Model_createReactant(model);
				SpeciesReference_initDefaults(reference);
				SpeciesReference_setSpecies(reference, second_name.c_str());
			} else if (reaction_kind == 2){
				reference = Model_createReactant(model);
				SpeciesReference_initDefaults(reference);
				SpeciesReference_setStoichiometry(reference, 2);
				SpeciesReference_setSpecies(reference, first_name.c_str());
			}

			for (index = 0, products_it = mono_products.begin(); products_it != mono_products.end(); products_it++, index++){
				if (products_map.find((*products_it).first->GetId()) != products_map.end()){
					int_pair.first = index;
					int_pair.second = 1;
					products_map[(*products_it).first->GetId()] = int_pair;
				} else {
					tmp_int = products_map[(*products_it).first->GetId()].second;
					products_map[(*products_it).first->GetId()].second = tmp_int + 1;
				}
			}

			for (products_map_it = products_map.begin(); products_map_it != products_map.end(); products_map_it++){
				reference = Model_createProduct(model);
				SpeciesReference_initDefaults(reference);
				SpeciesReference_setSpecies(reference, products_name[(*products_map_it).second.first].c_str());
				SpeciesReference_setStoichiometry(reference, (*products_map_it).second.second);
			}
		//}
	}

}
void MoleculeNode::ReduceRateLists(){
	list<pair <MoleculeNode *, list<double> *> >::iterator p_it;
	list<double>::iterator d_it;
	for (p_it = mono_products.begin(); p_it != mono_products.end(); p_it++){
			ReduceRateList((*p_it).second);
	}
}

//this function reduce cases like these:
//
//A--(inf)-->B    ---->    A--(inf)-->B
// \--(0.2)-->C
//
//
//A--(inf)-->B				
// \           /->C		---->   A--(inf)-->B
//  \--(0.2)--/
//					\->D
////////////////////////////////////////////////////
bool MoleculeNode::ReduceMonoInfActions(){
	list<pair <MoleculeNode *, list <double> *> >::iterator it;
	list <ReactionNode *> nodes;
	list <ReactionNode *>::iterator n_it;
	pair <MoleculeNode *, list <double> *> p;
	list <MultiNode *>::iterator m_it;
	bool infinite_reaction = false;

	//Check if there are mono reactions with infinite rate and store them in the
	//list "nodes"
	for (it = mono_products.begin(); it != mono_products.end(); it++){
		if (*((*it).second->begin()) == HUGE_VAL){
			infinite_reaction = true;
		} else {
			nodes.push_back((*it).first);
		}
	}

	
	//If there are mono reactions with infinite rate we have to delete all the
	//mono reactions that have finite rate
	if (!nodes.empty() && infinite_reaction){
		for (n_it = nodes.begin(); n_it != nodes.end(); n_it++){
			this->RemoveProduct(*n_it);
			(*n_it)->RemoveReactant(this);
			(*n_it)->DeleteUnrichableNodes();
		}
		nodes.clear();
	}

	//Check if there are finite multi reactions and store them in the list nodes
	for (m_it = multi_products.begin(); m_it != multi_products.end(); m_it++){
		if (*((*m_it)->GetRates()->begin()) != HUGE_VAL){
			nodes.push_back(*m_it);
		}
	}

	//Delete the finite multi reactions
	if (!nodes.empty() && infinite_reaction){
		for (n_it = nodes.begin(); n_it != nodes.end(); n_it++){
			this->RemoveProduct(*n_it);
			(*n_it)->RemoveReactant(this);
			//TODO: Sarebbe saggio verificare se il fatto che arrivino in questo punto
			//dei nodi già cancellati sia un errore oppure no.
			if (!(*n_it)->IsDeleted()){
				(*n_it)->Delete();
			}
		}
	}
	return (!nodes.empty());
}

/**************************************************************
 *	Eliminate this kind of infinite reactions
 * 
 *
 * A --(0.2)---> B --(inf)-->D						A--(0.1)--> D
 *				  /	\						----->		
 * C --(0.4)-/		 \--(inf)-->F						A--(0.1)--> F
 *
 *																C--(0.2)--> D
 *																
 *																C--(0.2)--> F
 *TODO: in questo commento scirivi anche che semplifica i multinodi a fininito.
 *
 **************************************************************/

void MoleculeNode::CollapseInfReactions(list<ReactionNode*> *nodes_to_be_checked, ReactionsGraph *graph){
	list<pair <MoleculeNode *, list <double> *> >::iterator mono_products_it;
	//list<ReactionNode *>::iterator n_it;
	list<MultiNode *>::iterator multi_products_it, multi_reactants_it;
	list<MoleculeNode *>::iterator mono_reactants_it;
	list<double>::iterator l_it;
	list<MultiNode *> multi_reactants_copy;
	list<MultiNode *> multi_products_copy;
	list<MultiNode *> multi_to_be_deleted;
	list<double> *new_rates, *old_rates, *infinite_rate_list;
	MultiNode *clone;
	MultiNode *clone2;
	int n_inf_reactions = 0;
	bool reduct = true;
	//list node_to_be_deleted;

	/*this->CheckDoubleInfArrows(&node_to_be_deleted);


	for (mono_reactants_it = mono_reactants.begin(); mono_reactants_it != mono_reactants.end(); mono_reactants_it++){
		(*mono_reactants_it)->CheckDoubleInfArrows(&node_to_be_deleted);
	}*/


	for (mono_products_it = mono_products.begin(); mono_products_it != mono_products.end(); mono_products_it++){
		if (*((*mono_products_it).second->begin()) == HUGE_VAL){
			n_inf_reactions++;
		}
	}

	for (multi_products_it = multi_products.begin(); multi_products_it != multi_products.end(); multi_products_it++){
		if (*((*multi_products_it)->GetRates()->begin()) == HUGE_VAL){
			n_inf_reactions++;
		}

		if ((*multi_products_it)->GetNumberOfReactants() > 1){
			reduct = false;
		}
	}

	if (multi_products.size() + mono_products.size() > 1){
		for (multi_reactants_it = multi_reactants.begin(); multi_reactants_it != multi_reactants.end(); multi_reactants_it++){
			if ((*multi_reactants_it)->GetNumberOfMultiReactants() > 0){
				reduct = false;
			}
		}
	}
	//da attivare per fare il test con tutto insieme
	if (reduct && n_inf_reactions > 0) {
	//if (false){
	//da attivare per testare nodi che hanno solo mono_products e mono_reactants
	//if (multi_reactants.size() == 0 && multi_products.size() == 0 && reduct && n_inf_reactions > 0) {
	//da attivare per fare il test sui nodi che hanno prodtti mono, prodtti multi e reagenti mono
	//if (multi_reactants.size() == 0 && reduct && n_inf_reactions > 0) {
	//da attiver per fare il test sui nodi che hanno prodtti mono, reagenti multi e reagenti mono
	//if (multi_products.size() == 0 && reduct && n_inf_reactions > 0) {
	//per testare tutti i casi ad eccezione di quelli che presentano sia multi prodotti che multi reaenti insieme
	//if ((multi_products.size() == 0 || multi_reactants.size() == 0) && n_inf_reactions > 0 && reduct){
		if (present_in_run > 0)
      {
         assert((int)(mono_products.size() + multi_products.size()) == n_inf_reactions);
         int step = present_in_run / n_inf_reactions;
         int entities_added = 0;
			for (mono_products_it = mono_products.begin(); mono_products_it != mono_products.end(); mono_products_it++)
         {
				(*mono_products_it).first->AddToInRun(step);
            entities_added += step;
			}
			for (multi_products_it = multi_products.begin(); multi_products_it != multi_products.end(); multi_products_it++)
         {
				(*multi_products_it)->AddToInRun(step);
            entities_added += step;
			}

         int diff = present_in_run - entities_added;
         assert (diff > 0);
         // add what's left to someone
         if (mono_products.size() > 0) 
            mono_products.front().first->AddToInRun(diff);
         else
            multi_products.front()->AddToInRun(diff);
		}
	
	
	
		//Create copies of the MultiNode and delete the original nodes
		for (multi_reactants_it =  multi_reactants.begin(); multi_reactants_it != multi_reactants.end(); multi_reactants_it++){
			old_rates = new list<double>(*(*multi_reactants_it)->GetRates());
			multi_reactants_copy.push_back((*multi_reactants_it)->CloneWithoutOneProduct(-1, this, old_rates));
		}

		multi_to_be_deleted.insert(multi_to_be_deleted.end(),multi_reactants.begin(), multi_reactants.end());
		
		for (multi_reactants_it =  multi_to_be_deleted.begin(); multi_reactants_it != multi_to_be_deleted.end(); multi_reactants_it++){
			(*multi_reactants_it)->NotRecursiveDelete();
		}


		for (multi_products_it = multi_products.begin(); multi_products_it != multi_products.end(); multi_products_it++){
			old_rates = new list<double>(*(*multi_products_it)->GetRates());
			multi_products_copy.push_front((*multi_products_it)->CloneWithoutOneReactant(-1, this, old_rates));
		}

		multi_to_be_deleted.clear();
		multi_to_be_deleted.insert(multi_to_be_deleted.end(),multi_products.begin(), multi_products.end());

		for (multi_reactants_it =  multi_to_be_deleted.begin(); multi_reactants_it != multi_to_be_deleted.end(); multi_reactants_it++){
			(*multi_reactants_it)->NotRecursiveDelete();
		}

		for (mono_reactants_it = mono_reactants.begin(); mono_reactants_it != mono_reactants.end(); mono_reactants_it++){
			if ((*mono_reactants_it) != this){
				nodes_to_be_checked->push_back(*mono_reactants_it);

				old_rates = new list<double>(*((*mono_reactants_it)->GetRates(this)));
				(*mono_reactants_it)->RemoveProduct(this);
				for (mono_products_it = mono_products.begin(); mono_products_it != mono_products.end(); mono_products_it++){
					if ((*mono_products_it).first != this) {
						if (mono_reactants_it == mono_reactants.begin()){
							(*mono_products_it).first->RemoveReactant(this);
						}

						new_rates = new list<double>;
						for (l_it = old_rates->begin(); l_it != old_rates->end(); l_it++){
							new_rates->push_back((*l_it)/n_inf_reactions);
						}
						(*mono_products_it).first->AddReactant(*mono_reactants_it);
						(*mono_reactants_it)->AddProduct((*mono_products_it).first, new_rates);
					}
				}

				for (multi_products_it = multi_products_copy.begin(); multi_products_it != multi_products_copy.end(); multi_products_it++){
					new_rates = new list<double>;
					for (l_it = old_rates->begin(); l_it != old_rates->end(); l_it++){
						new_rates->push_back((*l_it)/n_inf_reactions);
					}
					clone = (*multi_products_it)->CloneWithoutOneReactant(graph->GetNewMultiId(), NULL, new_rates);
					clone->AddReactant(*mono_reactants_it);
					(*mono_reactants_it)->AddProduct(clone, NULL);
					graph->AddNode(clone);
				}


				delete old_rates;
			}
		}

		for (multi_reactants_it = multi_reactants_copy.begin(); multi_reactants_it != multi_reactants_copy.end(); multi_reactants_it++){
			old_rates = new list<double>(*(*multi_reactants_it)->GetRates());
			(*multi_reactants_it)->RemoveProduct(this);
			for (mono_products_it = mono_products.begin(); mono_products_it != mono_products.end(); mono_products_it++){
				if ((*mono_products_it).first != this) {
					new_rates = new list<double>;
					for (l_it = old_rates->begin(); l_it != old_rates->end(); l_it++){
						new_rates->push_back((*l_it)/n_inf_reactions);
					}
					clone = (*multi_reactants_it)->CloneWithoutOneReactant(graph->GetNewMultiId(), NULL, new_rates);
					clone->AddProduct((*mono_products_it).first, NULL);
					(*mono_products_it).first->AddReactant(clone);
					graph->AddNode(clone);
				}
			}

			for (multi_products_it = multi_products_copy.begin(); multi_products_it != multi_products_copy.end(); multi_products_it++){
				new_rates = new list<double>;
				for (l_it = old_rates->begin(); l_it != old_rates->end(); l_it++){
					new_rates->push_back((*l_it)/n_inf_reactions);
				}


				infinite_rate_list = new list<double>;
				infinite_rate_list->push_back(HUGE_VAL);

				clone = (*multi_reactants_it)->CloneWithoutOneReactant(graph->GetNewMultiId(), NULL, new_rates);
				clone2 = (*multi_products_it)->CloneWithoutOneReactant(graph->GetNewMultiId(), NULL, infinite_rate_list);

				clone->AddProduct(clone2, NULL);
				clone2->AddReactant(clone);

				graph->AddNode(clone);
				graph->AddNode(clone2);
				nodes_to_be_checked->push_back(clone2);
			}




			delete old_rates;
		}

		this->is_deleted = true;

		for (multi_reactants_it = multi_reactants_copy.begin(); multi_reactants_it != multi_reactants_copy.end(); multi_reactants_it++){
			(*multi_reactants_it)->NotRecursiveDelete();
		}

		for (multi_products_it = multi_products_copy.begin(); multi_products_it != multi_products_copy.end(); multi_products_it++){
			(*multi_products_it)->NotRecursiveDelete();
		}

		for (mono_reactants_it = mono_reactants.begin(); mono_reactants_it != mono_reactants.end(); mono_reactants_it++){
			(*mono_reactants_it)->CheckDoubleInfArrows();
		}
	}


	//TODO: dealloca la lista di copie di nodi.
}

void MoleculeNode::CollapseFromMolToMol(list<ReactionNode*> *nodes_to_be_checked, ReactionsGraph *graph){
	//This function collapses nodes that are involved only in monomolecular
	//reactions
	
	//Check if the molecule is involved only in monomolecular reactions
	if (this->multi_products.size() == 0 && this->multi_reactants.size() == 0 && this->mono_products.size() > 0){
		list<pair <MoleculeNode *, list <double> *> >::iterator mono_products_it;
		list<MoleculeNode *>::iterator mono_reactants_it;
		list<double> *new_rates, *old_rates = NULL;
		list<double>::iterator l_it;

		bool reduce = true;
		int n_inf_reactions = 0;

		//Check if all the reactions this molecule can is involved as reactant
		//have infinite rate. During this operation reactions with infinite rate
		//are counted
		for (mono_products_it = mono_products.begin(); mono_products_it != mono_products.end() && reduce; mono_products_it++){
			if (*((*mono_products_it).second->begin()) == HUGE_VAL){
				n_inf_reactions++;
			} else {
				reduce = false;
			}
		}


		if (n_inf_reactions > 0 && reduce){
			if (this->present_in_run != 0)
         {
            assert((int)(mono_products.size()) == n_inf_reactions);
            int step = present_in_run / n_inf_reactions;
            int entities_added = 0;
				for (mono_products_it = mono_products.begin(); mono_products_it != mono_products.end(); mono_products_it++){
					(*mono_products_it).first->AddToInRun(step);
               entities_added += step;
				}

            int diff = present_in_run - entities_added;
            assert (diff > 0);
            mono_products.front().first->AddToInRun(diff);
			}


			//This molecule is bypassed and then deleted
			for (mono_reactants_it = mono_reactants.begin(); mono_reactants_it != mono_reactants.end(); mono_reactants_it++){
				old_rates = new list<double>(*((*mono_reactants_it)->GetRates(this)));
				(*mono_reactants_it)->RemoveProduct(this);
				for (mono_products_it = mono_products.begin(); mono_products_it != mono_products.end(); mono_products_it++){
					if ((*mono_products_it).first != this) {
						if (mono_reactants_it == mono_reactants.begin()){
							(*mono_products_it).first->RemoveReactant(this);
						}

						new_rates = new list<double>;
						for (l_it = old_rates->begin(); l_it != old_rates->end(); l_it++){
							new_rates->push_back((*l_it)/n_inf_reactions);
						}
						(*mono_products_it).first->AddReactant(*mono_reactants_it);
						(*mono_reactants_it)->AddProduct((*mono_products_it).first, new_rates);
					}
				}
				delete old_rates;
			}
			this->is_deleted = true;
			for (mono_reactants_it = mono_reactants.begin(); mono_reactants_it != mono_reactants.end(); mono_reactants_it++){
				(*mono_reactants_it)->CheckDoubleInfArrows();
			}
		}
	}
}


void MoleculeNode::CollapseFromMolMulTo1Mol(list<ReactionNode*> *nodes_to_be_checked, ReactionsGraph *graph){
	//Reduce molecules that is involved as reactant in only one monomolecular reaction

	//Check if the molecule is involved in only one monomolecular 
	if (this->multi_products.size() == 0 && this->mono_products.size() == 1){
		pair<MoleculeNode*, list<double> *> p = *(this->mono_products.begin());
		if (*(this->GetRates(p.first)->begin()) == HUGE_VAL) {
			list<pair <MoleculeNode *, list <double> *> >::iterator mono_products_it;
			list<MoleculeNode *>::iterator mono_reactants_it;
			list<MultiNode *>::iterator multi_reactants_it;
			list<double> *rates;
			list<double>::iterator l_it;

			(*(mono_products.begin())).first->AddToInRun(present_in_run);

			MoleculeNode *product = p.first;
			//Remove the monomolecular reaction that involve this molecule as reactant
			product->RemoveReactant(this);

			//Connect all the molecules involved as reactants with the molecule product
			for (mono_reactants_it = mono_reactants.begin(); mono_reactants_it != mono_reactants.end(); mono_reactants_it++){
				rates = new list<double>(*((*mono_reactants_it)->GetRates(this)));
				(*mono_reactants_it)->RemoveProduct(this);
				product->AddReactant(*mono_reactants_it);
				(*mono_reactants_it)->AddProduct(product, rates);
			}

			//Connect all the multinode involved as reactants with the molecule product
			for (multi_reactants_it = multi_reactants.begin(); multi_reactants_it != multi_reactants.end(); multi_reactants_it++){
				//rates = new list<double>(*((*multi_reactants_it)->GetRates()));
				(*multi_reactants_it)->RemoveProduct(this);
				product->AddReactant(*multi_reactants_it);
				(*multi_reactants_it)->AddProduct(product, NULL);
			}


			this->is_deleted = true;
			/*for (mono_reactants_it = mono_reactants.begin(); mono_reactants_it != mono_reactants.end(); mono_reactants_it++){
			  (*mono_reactants_it)->CheckDoubleInfArrows();
			  }*/
		}
	}
}


void MoleculeNode::CollapseFrom1MolToMolMul(list<ReactionNode*> *nodes_to_be_checked, ReactionsGraph *graph){
	//Collapse nodes that are involved as reactant in only one monomolecular reaction

	//Check if the molecule is involved as reactant in only one monomolecular reaction
	if (this->multi_reactants.size() == 0 && this->mono_reactants.size() == 1 && this->mono_products.size() > 0){
		list<pair <MoleculeNode *, list <double> *> >::iterator mono_product_it;
		list<MultiNode *>::iterator multi_product_it;
		list<double> rates;
		list<double> *new_rates;
		list<double>::iterator l_it;

		int n_inf_reactions = 0;
		bool reduce = true;

		//Check if the molecule is involved as reactant exclusively in reactions with infinite rate
		for (mono_product_it = mono_products.begin(); mono_product_it != mono_products.end(); mono_product_it++){
			if(*(this->GetRates(((*mono_product_it).first))->begin()) != HUGE_VAL){
				reduce = false;
			}
			n_inf_reactions++;
		}

		for (multi_product_it = multi_products.begin(); multi_product_it != multi_products.end(); multi_product_it++){
			if (*((*multi_product_it)->GetRates()->begin()) != HUGE_VAL){
				reduce = false;
			}
			n_inf_reactions++;
		}
		
		if (reduce){
			MoleculeNode * reactant;

			if (this->present_in_run > 0)
         {
            assert((int)(mono_products.size() + multi_products.size()) == n_inf_reactions);
            int step = present_in_run / n_inf_reactions;
            int entities_added = 0;
			   for (mono_product_it = mono_products.begin(); mono_product_it != mono_products.end(); mono_product_it++)
            {
				   (*mono_product_it).first->AddToInRun(step);
               entities_added += step;
			   }
			   for (multi_product_it = multi_products.begin(); multi_product_it != multi_products.end(); multi_product_it++)
            {
				   (*multi_product_it)->AddToInRun(step);
               entities_added += step;
			   }

            int diff = present_in_run - entities_added;
            assert (diff > 0);
            // add what's left to someone
            if (mono_products.size() > 0) 
               mono_products.front().first->AddToInRun(diff);
            else
               multi_products.front()->AddToInRun(diff);
			}
		
			reactant = *(mono_reactants.begin());

			new_rates = new list<double>(*(reactant->GetRates(this)));
			for (l_it = new_rates->begin(); l_it != new_rates->end(); l_it++){
					rates.push_back((*l_it)/n_inf_reactions);
			}

			
			for (mono_product_it = mono_products.begin(); mono_product_it != mono_products.end(); mono_product_it++){
				new_rates = new list<double>(rates);
				//this->RemoveProduct((*mono_product_it).first);
				(*mono_product_it).first->RemoveReactant(this);
				reactant->AddProduct((*mono_product_it).first, new_rates);
				(*mono_product_it).first->AddReactant(reactant);
			}		

			for (multi_product_it = multi_products.begin(); multi_product_it != multi_products.end(); multi_product_it++){
				new_rates = new list<double>(rates);
				//this->RemoveProduct(*multi_product_it);
				(*multi_product_it)->RemoveReactant(this);
				reactant->AddProduct(*multi_product_it, NULL);
				(*multi_product_it)->SetRates(new_rates);
				(*multi_product_it)->AddReactant(reactant);
			}		


			reactant->RemoveProduct(this);
			this->is_deleted = true;
			/*for (mono_reactants_it = mono_reactants.begin(); mono_reactants_it != mono_reactants.end(); mono_reactants_it++){
			  (*mono_reactants_it)->CheckDoubleInfArrows();
			  }*/

		}

		/*if(*(this->GetRates(p.first)->begin()) == HUGE_VAL) {

			MoleculeNode* reactant = *(this->mono_reactants.begin());
			list<pair <MoleculeNode *, list <double> *> >::iterator mono_products_it;
			list<MoleculeNode *>::iterator mono_reactants_it;
			list<MultiNode *>::iterator multi_reactants_it;
			list<double> *old_rates, *new_rates;
			list<double>::iterator l_it;

			MoleculeNode *product = p.first;

			product->RemoveOneReactant(this);

			for (mono_reactants_it = mono_reactants.begin(); mono_reactants_it != mono_reactants.end(); mono_reactants_it++){
				rates = new list<double>(*((*mono_reactants_it)->GetRates(this)));
				(*mono_reactants_it)->RemoveProduct(this);
				product->AddReactant(*mono_reactants_it);
				(*mono_reactants_it)->AddProduct(product, rates);
			}

			for (multi_reactants_it = multi_reactants.begin(); multi_reactants_it != multi_reactants.end(); multi_reactants_it++){
				rates = new list<double>(*((*multi_reactants_it)->GetRates()));
				(*multi_reactants_it)->RemoveProduct(this);
				product->AddReactant(*multi_reactants_it);
				(*multi_reactants_it)->AddProduct(product, rates);
			}


			this->is_deleted = true;*/
			/*for (mono_reactants_it = mono_reactants.begin(); mono_reactants_it != mono_reactants.end(); mono_reactants_it++){
			  (*mono_reactants_it)->CheckDoubleInfArrows();
			  }*/
		//}
	}
}



void MultiNode::CollapseInfReactions(list<ReactionNode*> *nodes_to_be_checked, ReactionsGraph *graph){
	list<pair <MoleculeNode *, list<double> *> >::iterator mo_it;
	list<MultiNode *>::iterator mu_it;
	MultiNode* reactant;
	if (multi_reactants.size() == 1 && mono_reactants.size() == 0){
		reactant = *(multi_reactants.begin());
		reactant->RemoveProduct(this);
		for (mo_it = mono_products.begin(); mo_it != mono_products.end(); mo_it++){
			(*mo_it).first->RemoveReactant(this);
			(*mo_it).first->AddReactant(reactant);
			reactant->AddProduct((*mo_it).first, NULL);
		}

		for (mu_it = multi_products.begin(); mu_it != multi_products.end(); mu_it++){
			(*mu_it)->RemoveReactant(this);
			(*mu_it)->AddReactant(reactant);
			reactant->AddProduct(*mu_it, NULL);
		}

		this->is_deleted = true;
	}
}

list<double> * MoleculeNode::GetRates(MoleculeNode *node){
	list<pair <MoleculeNode *, list <double> *> >::iterator it;
	for (it = mono_products.begin(); it != mono_products.end(); it++){
		if(node->GetId() == (*it).first->GetId()){
			return (*it).second;
		}
	}
	return NULL;	
}

void ReactionNode::EliminateMonoReaction(MoleculeNode* product){
	/*hash_map<ReactionNode *, bool> visited_nodes;
	hash_map<ReactionNode *, bool>::iterator it;
	list<ReactionNode *> to_be_visited;*/



	/*if (product->IsInRun()){
		return;
	}

	assert(!product->IsDeleted());

	product->UpdateNodesToBeVisited(&to_be_visited, &visited_nodes);

	if (to_be_visited.empty() && !product->IsInRun()){
		product->Delete();
	}
	
	for (ReactionNode* current = product; !to_be_visited.empty(); current = to_be_visited.front(), to_be_visited.pop_front()){
		assert(current->IsDeleted());
		if (current->IsInRun()){
			return;
		}	

		visited_nodes[current] = true;
		current->UpdateNodesToBeVisited(&to_be_visited, &visited_nodes);
	}
	
	//If we reach this point we have to mark for deletion all the visited nodes.
	//In fact they are unreachable, they will never be created during the
	//simulation
	for (it = visited_nodes.begin();	it != visited_nodes.end(); it++){
		(*it).first->Delete();
	}*/
}

/*void ReactionNode::EliminateMultiNode(MultiNode *product){
	this->RemoveProduct()
}*/

int MoleculeNode::IsReachable(hash_map<ReactionNode *, int> &visited_nodes, int index){
	list<MoleculeNode *>::iterator mo_it;
	list<MultiNode *>::iterator mu_it;
	int tmp;
	int undecidable_nodes = 0;

	if(this->present_in_run){
		return 1;
	}


	
	assert(visited_nodes.find(this) == visited_nodes.end());
	visited_nodes[this] = -1;
	
	for (mo_it = mono_reactants.begin(); mo_it != mono_reactants.end(); mo_it++){
		if (visited_nodes.find(*mo_it) == visited_nodes.end()){
			tmp =  (*mo_it)->IsReachable(visited_nodes, index + 1);
			visited_nodes[*mo_it] = tmp;
			if (tmp != -1){
				(*mo_it)->CheckProducts(visited_nodes);
			}
		} else {
			tmp = visited_nodes[*mo_it];
		}

		if (tmp == 1){
			return 1;
		} else if (tmp == -1){
			undecidable_nodes = true;
		}
	}

	for (mu_it = multi_reactants.begin(); mu_it != multi_reactants.end(); mu_it++){
		if (visited_nodes.find(*mu_it) == visited_nodes.end()){
			tmp = (*mu_it)->IsReachable(visited_nodes, index + 1);
			visited_nodes[*mu_it] = tmp;
			if (tmp != -1){
				(*mu_it)->CheckProducts(visited_nodes);
			}
		} else {
			tmp = visited_nodes[*mu_it];
		}

		if (tmp == 1){
			return 1;
		} else if (tmp == -1) {
			undecidable_nodes = true;
		}
	
	}

	if (undecidable_nodes) {
		return -1;
	}

	return 0;
}

int MultiNode::IsReachable(hash_map<ReactionNode *, int> &visited_nodes, int index){
	list<MoleculeNode *>::iterator mo_it;
	list<MultiNode *>::iterator mu_it;
	int tmp;
	bool undecidable_nodes = false;

	assert(visited_nodes.find(this) == visited_nodes.end());
	visited_nodes[this] = -1;
	

	for (mo_it = mono_reactants.begin(); mo_it != mono_reactants.end(); mo_it++){
		if (visited_nodes.find(*mo_it) == visited_nodes.end()){
			tmp = (*mo_it)->IsReachable(visited_nodes, index + 1);
			visited_nodes[*mo_it] = tmp;
			if (tmp != -1){
				(*mo_it)->CheckProducts(visited_nodes);
			}
		} else {
			tmp = visited_nodes[*mo_it];
		}
		if (tmp == 0){
			return 0;
		} else if (tmp == -1){
			undecidable_nodes = true;
		}
	}

	for (mu_it = multi_reactants.begin(); mu_it != multi_reactants.end(); mu_it++){
		if (visited_nodes.find(*mu_it) == visited_nodes.end()){
			tmp = (*mu_it)->IsReachable(visited_nodes, index + 1);
			visited_nodes[*mu_it] = tmp ;
			if (tmp != -1){
				(*mu_it)->CheckProducts(visited_nodes);
			}
		} else {
			tmp = visited_nodes[*mu_it]; 
		}
		
		if (tmp == 0){
			return 0;
		} else if (tmp == -1){
			undecidable_nodes = true;
		}
	}

	if (undecidable_nodes){
		return -1;
	}
	
	return 1;
}

void ReactionNode::CheckProducts(hash_map<ReactionNode *, int> &visited_nodes){
	list<pair<MoleculeNode *, list<double> *> >::iterator mo_it;
	list<MultiNode *>::iterator mu_it;

	assert(visited_nodes.find(this) != visited_nodes.end());
	int reachability = visited_nodes[this];

	for (mo_it = mono_products.begin(); mo_it != mono_products.end(); mo_it++){
		if (visited_nodes.find(mo_it->first) != visited_nodes.end()){
			if (visited_nodes[mo_it->first] == -1){
				visited_nodes[mo_it->first] = reachability;
				mo_it->first->CheckProducts(visited_nodes);
			}
		}
	}

	for (mu_it = multi_products.begin(); mu_it != multi_products.end(); mu_it++){
		if (visited_nodes.find(*mu_it) != visited_nodes.end()){
			if (visited_nodes[*mu_it] == -1){
				if(visited_nodes[*mu_it] == 0 || (*mu_it)->AreReactantsReachable(visited_nodes)){
					visited_nodes[*mu_it] = reachability;
					(*mu_it)->CheckProducts(visited_nodes);
				}
			}
		}

	}
}

//Check if all the reactants of the MultiNode has been visited and are reachable
bool MultiNode::AreReactantsReachable(hash_map<ReactionNode *, int> &visited_nodes){
	list<MoleculeNode*>::iterator mo_it;

	for (mo_it = mono_reactants.begin(); mo_it != mono_reactants.end(); mo_it++){
		if (visited_nodes.find(*mo_it) == visited_nodes.end() || visited_nodes[*mo_it] != 1){
			return false;
		}
	}
	return true;
}

	

/*void MoleculeNode::CheckDoubleInfArrows(){
	hash_map < MoleculeNode *, int> products;
	hash_map < MoleculeNode *, int>::iterator it;
	list<pair<MoleculeNode *, list<double> *> >::iterator mo_it;

	for (mo_it = mono_products.begin(); mo_it != mono_products.end(); mo_it++){
		if(*(mo_it->second->begin()) == HUGE_VAL){
			if ( products.find(mo_it->first) == products.end()){
				products[mo_it->first] = 0;
			} else {
				this->RemoveOneProduct(mo_it->first);
				mo_it->first->RemoveOneReactant(this);
			}
		}
	}
}*/

void MoleculeNode::CheckDoubleInfArrows(){
	hash_map < MoleculeNode *, list<double> *> products;
	list<pair<MoleculeNode *, list<double> *> >::iterator mo_it;
	list<double> * rates;

	for (mo_it = mono_products.begin(); mo_it != mono_products.end(); mo_it++){
		//if(*(mo_it->second->begin()) == HUGE_VAL){
			if ( products.find(mo_it->first) == products.end()){
				products[mo_it->first] = mo_it->second;
			} else {
				if(*(mo_it->second->begin()) != HUGE_VAL){
					rates = products[mo_it->first];
					mo_it->second->insert(mo_it->second->end(), rates->begin(), rates->end());
					products[mo_it->first] = mo_it->second;

				}
				this->RemoveOneProduct(mo_it->first);
				mo_it->first->RemoveOneReactant(this);
			}
		//}
	}
}


/*void MoleculeNode::CheckDoubleInfArrows(list<MoleculeNode*> *node_to_be_removed){
	hash_map < MoleculeNode *, int> products;
	hash_map < MoleculeNode *, int>::iterator it;
	list<pair<MoleculeNode *, list<double> *> >::iterator mo_it;

	for (mo_it = mono_products.begin(); mo_it != mono_products.end(); mo_it++){
		if(*(mo_it->second->begin()) == HUGE_VAL){
			if ( products.find(mo_it->first) == products.end()){
				products[mo_it->first] = 0;
			} else {
				node_to_be_removed->push_back(mo_it->first);
			}
		}
	}
}*/


//Deletes this nodes and it ancestors nodes if aren't reachable in the graph
void ReactionNode::DeleteUnrichableNodes(){
	/*hash_map<ReactionNode *, bool> visited_nodes;
	hash_map<ReactionNode *, bool>::iterator it;
	list<ReactionNode *> to_be_visited;

	if (this->IsInRun()){
		return;
	}

	assert(!this->IsDeleted());

	this->UpdateNodesToBeVisited(&to_be_visited, &visited_nodes);
	visited_nodes[this] = true;

	for (ReactionNode* current = NULL; !to_be_visited.empty(); to_be_visited.pop_front()){
		current = to_be_visited.front();
		if (!current->IsDeleted()){
			if (current->IsInRun()){
				return;
			}	

			current->UpdateNodesToBeVisited(&to_be_visited, &visited_nodes);
			visited_nodes[current] = true;
		}
	}

	//If we reach this point we have to mark for deletion all the visited nodes.
	//In fact they are unreachable, they will never be created during the
	//simulation
	for (it = visited_nodes.begin();	it != visited_nodes.end(); it++){
		if (!it->first->IsDeleted()){
			(*it).first->Delete();
		}
	}*/
	hash_map<ReactionNode *, int> visited_nodes;
	hash_map<ReactionNode *, int>::iterator it;
	int tmp;

	tmp = this->IsReachable(visited_nodes, 0);
	visited_nodes[this] = tmp;
	for (it = visited_nodes.begin(); it != visited_nodes.end(); it++){
		if (it->second <= 0){
			if (!it->first->IsDeleted()){
				it->first->Delete();
			}
		}
	}
	
}

MultiNode::MultiNode(int p_id, list<double> *p_rate):ReactionNode(p_id){
	rates = p_rate;
}

void MultiNode::Print(Environment *e){
	list<MoleculeNode *>::iterator reactant_it;
	list<pair<MoleculeNode *, list<double> *> >::iterator product_it;
	list<double>::iterator r_it;
	int node_id;

	for (reactant_it = mono_reactants.begin(); reactant_it != mono_reactants.end(); reactant_it++){
		node_id = (*reactant_it)->GetId()/2;
		if (node_id < 0){
			cout << e->GetST()->GetMolNameFromID(abs(node_id))	<< " ";
		} else if (node_id > 0) {
			cout << e->GetST()->GetEntNameFromID(node_id)	<< " ";
		} else {
			cout << "Nil ";
		} 
	}

	cout << " --> ";

	for (product_it = mono_products.begin(); product_it != mono_products.end(); product_it++){
		node_id = (*product_it).first->GetId()/2;
		if (node_id < 0){
			cout << e->GetST()->GetMolNameFromID(abs(node_id))	<< " ";
		} else if (node_id > 0) {
			cout << e->GetST()->GetEntNameFromID(node_id)	<< " ";
		} else {
			cout << "Nil ";
		}
	}

	cout << " rates: ";

	for (r_it = rates->begin(); r_it != rates->end(); r_it++) {
		cout << *r_it << " ";
	}

	cout << endl;
}


void MultiNode::ReversePrint(Environment *e){
	list<MoleculeNode *>::iterator reactant_it;
	list<pair<MoleculeNode *, list<double> *> >::iterator product_it;
	list<double>::iterator r_it;
	int node_id;

	for (reactant_it = mono_reactants.begin(); reactant_it != mono_reactants.end(); reactant_it++){
		node_id = (*reactant_it)->GetId()/2;
		if (node_id < 0){
			cout << e->GetST()->GetMolNameFromID(abs(node_id))	<< " ";
		} else {
			cout << e->GetST()->GetEntNameFromID(node_id)	<< " ";
		}
	}

	cout << " --> ";

	for (product_it = mono_products.begin(); product_it != mono_products.end(); product_it++){
		node_id = (*product_it).first->GetId()/2;
		if (node_id < 0){
			cout << e->GetST()->GetMolNameFromID(abs(node_id))	<< " ";
		} else {
			cout << e->GetST()->GetEntNameFromID(node_id)	<< " ";
		}

	}

	cout << " rates: ";

	for (r_it = rates->begin(); r_it != rates->end(); r_it++) {
		cout << *r_it << " ";
	}

	cout << endl;
}

void MultiNode::PrintDot(Environment *e, ofstream &fout){
	list<MoleculeNode *>::iterator reactant_it;
	list<pair<MoleculeNode *, list<double> *> >::iterator product_it;
	list<MultiNode *>::iterator multi_products_it;
	list<double>::iterator r_it;
	int node_id;

	


	fout << id << "[shape=polygon,sides=6,color=black,label=\"";
		
	for (r_it = rates->begin(); r_it != rates->end(); r_it++) {
		fout << *r_it << " ";
	}

   fout <<	"\"];" << endl;

	for (reactant_it = mono_reactants.begin(); reactant_it != mono_reactants.end(); reactant_it++){
		node_id = (*reactant_it)->GetId()/2;
		if (node_id < 0){
			fout << e->GetST()->GetMolNameFromID(abs(node_id))	<< " ";
		} else if (node_id > 0) {
			fout << e->GetST()->GetEntNameFromID(node_id)	<< " ";
		} else {
			fout << "Nil ";
		}

		fout << " -> ";

		fout << id << ";" << endl;

	}

	
	for (product_it = mono_products.begin(); product_it != mono_products.end(); product_it++){
		node_id = (*product_it).first->GetId()/2;
		fout << id << " -> ";
		if (node_id < 0){
			fout << e->GetST()->GetMolNameFromID(abs(node_id))	<< " ";
		} else if (node_id > 0) {
			fout << e->GetST()->GetEntNameFromID(node_id)	<< " ";
		} else {
			fout << "Nil ";
		}
		fout << ";" << endl;

	} 

	for (multi_products_it = multi_products.begin(); multi_products_it != multi_products.end(); multi_products_it++){
		fout << id << " -> " << (*multi_products_it)->GetId() << " ;" << endl;
	}
}

void MultiNode::ReduceRateLists(){
	ReduceRateList(rates);
}

MultiNode * MultiNode::CloneWithoutOneProduct(int new_id, ReactionNode *product_to_eliminate, list<double> *rates_list){
	list<MoleculeNode *>::iterator mo_it;
	list<MultiNode *>::iterator mu_it;
	list<pair<MoleculeNode *, list<double> *> >::iterator mo_r_it;

	MultiNode *res = new MultiNode(new_id, rates_list);
	
	for (mo_it = mono_reactants.begin(); mo_it != mono_reactants.end(); mo_it++){
		res->AddReactant(*mo_it);
		(*mo_it)->AddProduct(res, NULL);
	}

	for (mu_it = multi_reactants.begin(); mu_it != multi_reactants.end(); mu_it++){
		res->AddReactant(*mu_it);
		(*mu_it)->AddProduct(res, NULL);
	}

	for (mo_r_it = mono_products.begin(); mo_r_it != mono_products.end(); mo_r_it++){
		if (product_to_eliminate == NULL || mo_r_it->first != product_to_eliminate){
			res->AddProduct(mo_r_it->first, NULL);
			mo_r_it->first->AddReactant(res);
		}
	}

	for (mu_it = multi_products.begin(); mu_it != multi_products.end(); mu_it++){
		if (product_to_eliminate == NULL || (*mu_it) != product_to_eliminate){
			res->AddProduct(*mu_it, NULL);
			(*mu_it)->AddReactant(res);
		}
	}

	return res;
}


MultiNode * MultiNode::CloneWithoutOneReactant(int new_id, ReactionNode *reactant_to_eliminate, list<double> *rates_list){
	list<MoleculeNode *>::iterator mo_it;
	list<MultiNode *>::iterator mu_it;
	list<pair<MoleculeNode *, list<double> *> >::iterator mo_r_it;

	MultiNode *res = new MultiNode(new_id, rates_list);

	for (mo_it = mono_reactants.begin(); mo_it != mono_reactants.end(); mo_it++){
		if (reactant_to_eliminate == NULL || (*mo_it) != reactant_to_eliminate){
			res->AddReactant(*mo_it);
			(*mo_it)->AddProduct(res, NULL);
		}
	}

	for (mu_it = multi_reactants.begin(); mu_it != multi_reactants.end(); mu_it++){
		if (reactant_to_eliminate == NULL || (*mu_it) != reactant_to_eliminate){
			res->AddReactant(*mu_it);
			(*mu_it)->AddProduct(res, NULL);
		}
	}

	for (mo_r_it = mono_products.begin(); mo_r_it != mono_products.end(); mo_r_it++){
		res->AddProduct(mo_r_it->first, NULL);
		mo_r_it->first->AddReactant(res);
	}

	for (mu_it = multi_products.begin(); mu_it != multi_products.end(); mu_it++){
		res->AddProduct(*mu_it, NULL);
		(*mu_it)->AddReactant(res);
	}

	return res;
}

using namespace std;
ReactionsGraph::ReactionsGraph(hash_map<reaction_key, list<double>*, reaction_hash_compare> * reactions, list<pair<int, int> > *molecule_in_run, Environment *p_env){
	//Variables initialization
	n_multi_node = 1;
	env = p_env;

	//Iterators
	hash_map<int, ReactionNode *>::iterator n_it;
	hash_map<reaction_key, list<double>*, reaction_hash_compare>::iterator r_it;
	list<pair<int, int> >::iterator int_it;
	//list<double> rate_it;
	int i;

	//Number of reactants involved in the analysed reaction
	int n_reactants;
	//Numbers of products involved in the analysed reaction
	int n_products;

	//Temporary variables
	int node_id;
	const reaction_key *key;
	MultiNode *node;
	MoleculeNode *mol_node1 = NULL;
	MoleculeNode *mol_node2 = NULL;


	//Cycle all the reactions
	for (r_it = reactions->begin(); r_it != reactions->end(); r_it++){
		//Store the key of the current reaction;
		key = &(*r_it).first;

		n_reactants = 0;
		n_products = 0;

		//Represents the Nil Node. Used to represent reaction like this one:
		//A -> Nil
		nodes[0] = new MoleculeNode(0);

		for (i = 0; i < 4; i++){
			//Even numbers are used for MoleculeNode objects. Odd numbers are used
			//for MultiNode objects. So we compute the id for the current node
			//exploiting the id used in the reaction and multiplying it by two.
			node_id = key->data[i] * 2;

			if (node_id != 0){

				if (i < 2){
					n_reactants++;
				} else {
					n_products++;
				}

				//Check if the node has already been inserted in the graph
				n_it = nodes.find(node_id);
				if (n_it == nodes.end()){
					//If the node has not been already inserted in the graph we
					//insert the node now
					nodes[node_id] = new MoleculeNode(node_id);
				}
			}
		}

		if (n_reactants > 1 || n_products > 1){
			//In this case we have more than one reactant or more than one
			//products, so we need to create a MultiNode
			node = new MultiNode(n_multi_node, (*reactions)[*key]);
			//Insert the MultiNode in the Graph
			nodes[n_multi_node] = node;


			for (i = 0; i < 4; i++) {
				if (key->data[i] != 0){
					mol_node1 = (MoleculeNode*)nodes[key->data[i]*2];
					if (i < 2){
						//Add the reactants as reactant of the MultiNode
						node->AddMonoReactant(mol_node1);
						//Add the MultiNode as product of to the reactants
						mol_node1->AddMultiProduct(node);
					} else {
						//Add the products as product of the MultiNode
						node->AddMonoProduct((MoleculeNode*)nodes[key->data[i]*2], NULL);
						//Add the MultiNode as reactant of the products
						mol_node1->AddMultiReactant(node);
					}
				}
			}
			n_multi_node+=2;
		} else if (n_products == 0) {
			//In this case we have no products: A --> Nil
			for (i = 0; i < 2; i++){
				if (key->data[i] != 0){
					//The reactant of the reaction
					mol_node1 = (MoleculeNode *)nodes[key->data[i]*2];
					//The product of the reactions: in this case it is the special
					//node "0". It represents the Nil node
					mol_node2 = (MoleculeNode*)nodes[0];
					//update the edges of the graph
					mol_node1->AddMonoProduct(mol_node2, (*reactions)[*key]);
					mol_node2->AddMonoReactant(mol_node1);
				}
			}
		} else {
			//In this case we have a monomolecular reaction: A --> B
			assert(n_reactants == 1 && n_products == 1);
			for (i = 0; i < 2; i++) {
				//Find the reactant
				if (key->data[i] != 0){
					mol_node1 = (MoleculeNode*)nodes[key->data[i]*2];
				}
			}
			for (; i < 4; i++){
				//Find the product
				if (key->data[i] != 0){
					mol_node2 = (MoleculeNode*)nodes[key->data[i]*2];
				}
			}
			//Update the edges of the graph
			mol_node1->AddMonoProduct(mol_node2, (*reactions)[*key]);
			mol_node2->AddMonoReactant(mol_node1);
		}
	}

	for (int_it = molecule_in_run->begin(); int_it != molecule_in_run->end(); int_it++){
		if(nodes.find((*int_it).first * 2)!=nodes.end()){
			((MoleculeNode *)nodes[(*int_it).first * 2])->InRun((*int_it).second);
		}
	}
}

void ReactionsGraph::Print(){
	hash_map<int, ReactionNode *>::iterator it;

	for (it = nodes.begin(); it != nodes.end(); it++){
		(*it).second->Print(env);
	}
}

void ReactionsGraph::ReversePrint(){
	hash_map<int, ReactionNode *>::iterator it;

	for (it = nodes.begin(); it != nodes.end(); it++){
		(*it).second->ReversePrint(env);
	}
}

void ReactionsGraph::PrintDot(const char * file_name){
	hash_map<int, ReactionNode *>::iterator it;
	int id;

	ofstream fout(file_name,ios::out);
	fout << "digraph simpleDotFile {" << endl;
     
	fout << "Nil [label=Nil];" << endl;

	for (it = nodes.begin(); it != nodes.end(); it++){
		id = (*it).second->GetId();
		if (id != 0) {
			if (id % 2 == 0){
				if (id < 0) {
					id = abs(id);
					fout << env->GetST()->GetMolNameFromID(id/2) << "[color=red,label=" << env->GetST()->GetMolNameFromID(id/2) << "];" << endl ;
				} else {
					fout << env->GetST()->GetEntNameFromID(id/2) << "[color=blue,label=" << env->GetST()->GetEntNameFromID(id/2) << "];" << endl ;
				}
			}
		}
	}

	for (it = nodes.begin(); it != nodes.end(); it++){
		(*it).second->PrintDot(env, fout);
	}
	
	fout << "}" << endl;
}

void MoleculeNode::Delete(){
	assert(!is_deleted);
	list<pair<MoleculeNode *, list<double> *> >::iterator mo_it;
	list<MultiNode * >::iterator mu_it;

	for(mo_it = mono_products.begin(); mo_it != mono_products.end(); mo_it++){
		(*mo_it).first->RemoveReactant(this);
	}
	
	//TODO: controlla se funziona
	for(mu_it = multi_products.begin(); mu_it != multi_products.end(); mu_it++){
		(*mu_it)->RemoveReactant(this);
		(*mu_it)->Delete();
	}

	is_deleted = true;	
}


void MultiNode::Delete(){
	assert(!is_deleted);
	list<pair<MoleculeNode *, list<double> *> >::iterator mo_r_it;
	list<MultiNode * >::iterator mu_it;
	list<MoleculeNode *>::iterator mo_it;


	for(mo_r_it = mono_products.begin(); mo_r_it != mono_products.end(); mo_r_it++){
		(*mo_r_it).first->RemoveReactant(this);
	}

	//TODO: controlla se funziona
	for(mu_it = multi_products.begin(); mu_it != multi_products.end(); mu_it++){
		(*mu_it)->RemoveReactant(this);
		(*mu_it)->Delete();
	}

	for(mu_it = multi_reactants.begin(); mu_it != multi_reactants.end(); mu_it++){
		//Non so se abbia senso pensare di andare a eliminare uno dei prodotti di una multi...
		//Nel caso questa situazione si verifichi bisgona pensarci bene.
		assert(true);
		(*mu_it)->RemoveProduct(this);
	}

	for(mo_it = mono_reactants.begin();  mo_it != mono_reactants.end(); mo_it++){
		(*mo_it)->RemoveProduct(this);
	}


	is_deleted = true;	
}

void MultiNode::NotRecursiveDelete(){
	assert(!is_deleted);
	list<pair<MoleculeNode *, list<double> *> >::iterator mo_r_it;
	list<MultiNode * >::iterator mu_it;
	list<MoleculeNode *>::iterator mo_it;


	for(mo_r_it = mono_products.begin(); mo_r_it != mono_products.end(); mo_r_it++){
		(*mo_r_it).first->RemoveReactant(this);
	}

	//TODO: controlla se funziona
	for(mu_it = multi_products.begin(); mu_it != multi_products.end(); mu_it++){
		(*mu_it)->RemoveReactant(this);
	}

	for(mu_it = multi_reactants.begin(); mu_it != multi_reactants.end(); mu_it++){
		//Non so se abbia senso pensare di andare a eliminare uno dei prodotti di una multi...
		//Nel caso questa situazione si verifichi bisgona pensarci bene.
		assert(true);
		(*mu_it)->RemoveProduct(this);
	}

	for(mo_it = mono_reactants.begin();  mo_it != mono_reactants.end(); mo_it++){
		(*mo_it)->RemoveProduct(this);
	}


	is_deleted = true;	
}

void MultiNode::AddToInRun(int in_run){
	list<pair<MoleculeNode *, list<double> *> >::iterator mono_product_it;
	list<MultiNode *>::iterator multi_product_it;

	int n_soons = (int)(multi_products.size() + mono_reactants.size());
   int step = in_run /n_soons;
   int entities_added = 0;

	for (mono_product_it = mono_products.begin(); mono_product_it != mono_products.end(); mono_product_it++){
		(*mono_product_it).first->AddToInRun(step);
      entities_added += step;
	}
	for (multi_product_it = multi_products.begin(); multi_product_it != multi_products.end(); multi_product_it++){
		(*multi_product_it)->AddToInRun(step);
      entities_added += step;
	}

   int diff = in_run - entities_added;
   assert (diff > 0);
   // add what's left to someone
   if (mono_products.size() > 0) 
      mono_products.front().first->AddToInRun(diff);
   else
      multi_products.front()->AddToInRun(diff);
}
	

int ReactionsGraph::GetNewMultiId(){
	int res = n_multi_node;
	n_multi_node += 2;
	return res;
}

void ReactionsGraph::Reduce(){
	hash_map<int, ReactionNode *>::iterator it;
	list<ReactionNode *>::iterator r_it;
	list<int> nodes_to_be_removed;
	list<int>::iterator i_it;

	/*for (it = nodes.begin(); it != nodes.end(); it++) {
		(*it).second->ReduceRateLists();
	}*/

	

	//this->PrintDot("primaReductMonoInfActions.dot");

	/*for (it = nodes.begin(); it != nodes.end(); it++) {
		if((*it).second->GetId() % 2 == 0 && !(*it).second->IsDeleted()){
			((MoleculeNode*)(*it).second)->ReduceMonoInfActions();
		}
	}*/

	//Find MoleculeNodes that have to be erased
	//for (it = nodes.begin(); it != nodes.end(); it++) {
	//	if(/*(*it).second->GetId() % 2 == 0 && */(*it).second->IsDeleted()){
	//		nodes_to_be_removed.push_back((*it).first);
	//	}
	//}

	//Erase the nodes
	//TODO: è necessario eliminare i nodi in modo che nel grafo non restino
	//riferimenti a nodi che non esistono 
	/*for (i_it = nodes_to_be_removed.begin(); i_it != nodes_to_be_removed.end(); i_it++){
		nodes.erase(*i_it);
	}*/

	//this->PrintDot("primaDeleteUnreachable.dot");


	//ReactionNode *tmp = nodes[-16404];
	
	//Mark as deletable node that can't be reached during the simulation
	/*for (it = nodes.begin(); it != nodes.end(); it++) {
		if((*it).second->GetId() % 2 == 0 && !(*it).second->IsDeleted()){
			(*it).second->DeleteUnrichableNodes();
		}
	}*/
	//Find MoleculeNodes that have to be erased
	/*for (it = nodes.begin(); it != nodes.end(); it++) {
		if((*it).second->IsDeleted()){
			nodes_to_be_removed.push_back((*it).first);
		}
	}*/

	

	//TODO: La rimozione di nodi potrebbe implicare la rimozione di altri nodi
	//(sicuramnte per i multi pensaci per i mol) bisogna ricontrollarli
	
	//Erase the nodes
	//TODO: è necessario eliminare i nodi in modo che nel grafo non restino
	//riferimenti a nodi che non esistono 
	//for (i_it = nodes_to_be_removed.begin(); i_it != nodes_to_be_removed.end(); i_it++){
	//	nodes.erase(*i_it);
	//}
	
	//this->PrintDot("primaCollaps.dot");

	list<ReactionNode*> to_be_checked;

	/*for (it = nodes.begin(); it != nodes.end(); it++) {
		if((*it).second->GetId() % 2 == 0 && !(*it).second->IsDeleted()){
			((MoleculeNode*)(*it).second)->CollapseInfReactions(&to_be_checked, this);
		}
	}

	for (r_it = to_be_checked.begin(); r_it != to_be_checked.end(); r_it++){
		//if((*r_it)->GetId() % 2 == 0 && !(*r_it)->IsDeleted()){
			if (!(*r_it)->IsDeleted()){
				((MoleculeNode*)(*r_it))->CollapseInfReactions(&to_be_checked, this);
			}
		//}

	}*/


	for (it = nodes.begin(); it != nodes.end(); it++) {
		if((*it).second->GetId() % 2 == 0 && !(*it).second->IsDeleted()){
			((MoleculeNode*)(*it).second)->CollapseFromMolToMol(&to_be_checked, this);
		}
	}

	for (it = nodes.begin(); it != nodes.end(); it++) {
		if((*it).second->GetId() % 2 == 0 && !(*it).second->IsDeleted()){
			((MoleculeNode*)(*it).second)->CollapseFromMolMulTo1Mol(&to_be_checked, this);
		}
	}

	for (it = nodes.begin(); it != nodes.end(); it++) {
		if((*it).second->GetId() % 2 == 0 && !(*it).second->IsDeleted()){
			((MoleculeNode*)(*it).second)->CollapseFrom1MolToMolMul(&to_be_checked, this);
		}
	}

	for (it = nodes.begin(); it != nodes.end(); it++) {
		if((*it).second->GetId() % 2 == 0 && !(*it).second->IsDeleted()){
			((MoleculeNode*)(*it).second)->CollapseInfReactions(&to_be_checked, this);
		}
	}

	for (it = nodes.begin(); it != nodes.end(); it++) {
		if((*it).second->GetId() % 2 != 0 && !(*it).second->IsDeleted()){
			((MoleculeNode*)(*it).second)->CollapseInfReactions(&to_be_checked, this);
		}
	}




	//TODO: Qeusti cicli uguali consecutivi sono da eliminare, capire perché non
	//basta metterne uno solo

	//Mark as deletable node that can't be reached during the simulation
	/*for (it = nodes.begin(); it != nodes.end(); it++) {
		if((*it).second->GetId() % 2 == 0 && !(*it).second->IsDeleted()){
			(*it).second->DeleteUnrichableNodes();
		}
	}*/

	//Mark as deletable node that can't be reached during the simulation
	/*for (it = nodes.begin(); it != nodes.end(); it++) {
		if((*it).second->GetId() % 2 == 0 && !(*it).second->IsDeleted()){
			(*it).second->DeleteUnrichableNodes();
		}
	}

	//Mark as deletable node that can't be reached during the simulation
	for (it = nodes.begin(); it != nodes.end(); it++) {
		if((*it).second->GetId() % 2 == 0 && !(*it).second->IsDeleted()){
			(*it).second->DeleteUnrichableNodes();
		}
	}*/

	//Find MoleculeNodes that have to be erased
	for (it = nodes.begin(); it != nodes.end(); it++) {
		if((*it).second->IsDeleted()){
			nodes_to_be_removed.push_back((*it).first);
		}
	}

	//TODO: Chimare la delete sui nodi che si tirano fuori dalla lista di nodi
	//(implementare anche la delete dei nodi)

	//TODO: La rimozione di nodi potrebbe implicare la rimozione di altri nodi
	//(sicuramnte per i multi pensaci per i mol) bisogna ricontrollarli
	
	//Erase the nodes
	//TODO: è necessario eliminare i nodi in modo che nel grafo non restino
	//riferimenti a nodi che non esistono 
	for (i_it = nodes_to_be_removed.begin(); i_it != nodes_to_be_removed.end(); i_it++){
		nodes.erase(*i_it);
	}
}


SBMLDocument_t * ReactionsGraph::BuildSBML(){
	hash_map<int, ReactionNode *>::iterator nodes_it;
	Compartment *compartment;

	#if (LIBSBML_VERSION >= 30101)
		SBMLDocument_t *document = SBMLDocument_createWithLevelAndVersion(1,1);
	#else
		SBMLDocument_t *document = SBMLDocument_createWith(1,1);
	#endif

	Model_t *model = SBMLDocument_createModel(document);

	Model_setName(model, "SBMLmodel");

   compartment = Model_createCompartment(model);
	Compartment_setId(compartment,"compartment");
	Compartment_setName(compartment,"compartment");
	

	for (nodes_it = nodes.begin(); nodes_it != nodes.end(); nodes_it++){
		(*nodes_it).second->InsertSBMLSpecie(model, env);
	}
	
	int reactions_counter = 0, constants_counter = 0;

	for (nodes_it = nodes.begin(); nodes_it != nodes.end(); nodes_it++){
		(*nodes_it).second->InsertSBMLReactions(model, env, reactions_counter, constants_counter);
	}

	return document;
}



