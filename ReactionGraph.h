#ifndef REACTIONGRAPHFUNC_H_INCLUDED
#define REACTIONGRAPHFUNC_H_INCLUDED

#include<vector>
#include<list>
#include"hash.h"
#include"Environment.h"

using namespace std;

class MoleculeNode;
class MultiNode;
class ReactionsGraph;


//Represents a General node that composes the Reactions Graph
class ReactionNode {
	protected:
		//The id is unique for each node of the graph
		int id;
		bool is_deleted;

		//List of the molecules that are involved as reactant in reactions that
		//involve this object as product. This reactions are monomolecular reactions, indeed
		//reactions of this kind: A --> B
		list<MoleculeNode *> mono_reactants;
		//List of the molecules that are involved as reactant in reactions that
		//involve this object as product. This reactions are multi molecular reactions, indeed
		//reactions of this kind: A --> B + C or reactions of this kind A + B --> C
		list<MultiNode *>  multi_reactants;
		//List of the molecules that are involved as product in reactions that
		//involve this object as reactant. This reactions are monomolecular reactions
		list<pair<MoleculeNode *, list<double> *> > mono_products;
		//List of the molecules that are involved as product in reactions that
		//involve this object as reactant. This reactions are multi molecular reactions
		list<MultiNode * > multi_products;
	public:
		//Creates a node with the id specified as argument
		ReactionNode(int p_id);
		//Destroys a Reaction Node
		virtual  ~ReactionNode();

		virtual void ReduceRateList(list<double> *l);
		virtual void ReduceRateLists() = 0;
		virtual bool Reduct() = 0;

		virtual void Print(Environment * e) = 0;
		virtual void PrintDot(Environment * e, ofstream &fout) = 0;
		virtual void ReversePrint(Environment * e) = 0;
		void UpdateNodesToBeVisited(list<ReactionNode *> *nodesToBeVisited, hash_map<ReactionNode *, bool>  *visitedNodes);
		void EliminateMonoReaction(MoleculeNode* product);

		void AddMonoReactant(MoleculeNode *node){ mono_reactants.push_back(node); }
		void AddMonoProduct(MoleculeNode *node, list<double> *rates){ 
			mono_products.push_back(pair<MoleculeNode	*,	list<double> *>(node, rates)); 
		}
		void AddMultiReactant(MultiNode *node){ multi_reactants.push_back(node); }
		void AddMultiProduct(MultiNode *node){ 
			multi_products.push_back(node); 
		}

		virtual bool IsInRun(){ return false; }
		virtual int IsReachable(hash_map<ReactionNode *, int> &visited_nodes, int index) = 0;
		void CheckProducts(hash_map<ReactionNode *, int> &visited_nodes);
		virtual void Delete() = 0;
		bool IsDeleted(){ return is_deleted; }

		void RemoveReactant(ReactionNode *node);
		void RemoveOneReactant(ReactionNode *node);
		void AddReactant(ReactionNode *node);
		void RemoveProduct(ReactionNode *node);
		void RemoveOneProduct(ReactionNode *node);
		void AddProduct(ReactionNode *node,  list<double> *rates);
		void DeleteUnrichableNodes();
		virtual void CollapseInfReactions(list<ReactionNode*> *nodes_to_be_checked, ReactionsGraph *grph) = 0;
		size_t GetNumberOfReactants() { return multi_reactants.size() + mono_reactants.size(); }
		size_t GetNumberOfMultiReactants() { return multi_reactants.size(); }

		int GetId(){ return id ; }
		virtual void InsertSBMLSpecie(Model_t *model, Environment *env){};
		virtual void InsertSBMLReactions(Model_t *model, Environment *env, int &constants_counter, int &reactions_counter) = 0;

};

//Represents a molecule in The Reactions Graph
class MoleculeNode : public ReactionNode {
	private:
		//It is true if the Molecule represented by the node is present in the
		//run directive of the analysed model. In other words it is true if the
		//molecule is note just defined but also instantiated with the run
		//directive
		int present_in_run;
	public:
		MoleculeNode(int id);
		bool Reduct() {return true; } ;
		void Print(Environment *e);
		void PrintDot(Environment * e, ofstream &fout);
		void ReversePrint(Environment *e);
		void InRun(int val){ present_in_run = val; }
      bool IsInRun(){ return (present_in_run == 0 ? false : true); }
		int GetInRun(){ return present_in_run; }
		void AddToInRun(int in_run) {present_in_run += in_run; }
		 
		int IsReachable(hash_map<ReactionNode *, int> &visited_nodes, int index);
		void ReduceRateLists();
		bool ReduceMonoInfActions();
		void CollapseInfReactions(list<ReactionNode*> *nodes, ReactionsGraph *graph);
		void CollapseFromMolToMol(list<ReactionNode*> *nodes_to_be_checked, ReactionsGraph *graph);
		void CollapseFromMolMulTo1Mol(list<ReactionNode*> *nodes_to_be_checked, ReactionsGraph *graph);
		void CollapseFrom1MolToMolMul(list<ReactionNode*> *nodes_to_be_checked, ReactionsGraph *graph);
		void CheckDoubleInfArrows(list<MoleculeNode *> * node_to_be_deleted);
		void CheckDoubleInfArrows();
		void Delete();
		list<double> *GetRates(MoleculeNode *);
		void CheckForInconsistency(ReactionsGraph *graph);
		void HasProduct(ReactionNode *);
		void HasReactant(ReactionNode *);
		SBMLDocument *BuildSBML();
		void InsertSBMLSpecie(Model_t *model, Environment *env);
		void InsertSBMLReactions(Model_t *model, Environment *env, int &constant_counter, int &reactions_counter);
};

//MultiNode objects are used in the Reactions Graph to describe reactions that
//involves more than one reactant or more than one product.
//Example:
//Suppose that Mol represents nodes of type MoleculeNode and Mul represents
//node of type MultiNode;
//
//////////////////////////////////////
//1) This reaction						//		
//												//
//			A --> B							//
//												//
//	  is represented in this way:		//
//												//
//			Mol(A) --> Mol(B)				//
//												//
//2) This reaction						//
//												//
//		A -- > C+B							//
//												//
//		is represented in this way;	//
//												//
//							/->Mol(C)		//
//						  /					//
//		Mol(A) --> Mul						//
//						  \					//
//						   \->Mol(B)		//
//												//
//												//
//////////////////////////////////////
//
class MultiNode : public ReactionNode{
	private:
		//rate of the reaction that the MultiNode represents
		list<double> *rates;
	public:
		MultiNode(int id, list<double> *p_rate);
		bool Reduct() {return true; } ;
		void Print(Environment *e);
		void PrintDot(Environment * e, ofstream &fout);
		void ReversePrint(Environment *e);
		void ReduceRateLists();
		int IsReachable(hash_map<ReactionNode *, int> &visited_nodes, int index);
		list<double> *GetRates(){ return rates; }
		void Delete();
		void NotRecursiveDelete();
		void CollapseInfReactions(list<ReactionNode*> *nodes, ReactionsGraph *grph);
		MultiNode *CloneWithoutOneProduct(int new_id, ReactionNode *product_to_eliminate, list<double> *rates_list);
		MultiNode *CloneWithoutOneReactant(int new_id, ReactionNode *reactant_to_eliminate, list<double> *rates_list);
		SBMLDocument *BuildSBML();
		void InsertSBMLReactions(Model_t *model, Environment *env, int &constants_counter, int &reactions_counter);
		bool AreReactantsReachable(hash_map<ReactionNode *, int> &visited_nodes);
		void SetRates(list<double> *new_rates){
			delete rates;
			rates = new_rates;
		}
		void AddToInRun(int in_run);
};

//Represent all the reactions that can be performed in the model
class ReactionsGraph {
	private:
		Environment *env;
		//Number of nodes of type MultiNode that has been inserted since the
		//creation of the graph
		int n_multi_node;


		//Contains all the nodes that compose the graph
		hash_map<int, ReactionNode *> nodes;
	public:
		//Initialize a graph that represents the reaction passed as argument
		ReactionsGraph(hash_map<reaction_key, list<double>*, reaction_hash_compare> * reactions, list<pair<int, int> >*molecule_in_run, Environment *p_env);
		void Print();
		void PrintDot(const char * file_name);
		void ReversePrint();
		void Reduce();
		int GetNewMultiId();
		void AddNode(ReactionNode *new_node) { nodes[new_node->GetId()] = new_node; }
		void CheckForInconsistency();
		SBMLDocument *BuildSBML();
};
		

#endif
