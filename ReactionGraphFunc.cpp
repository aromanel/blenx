
#include "Define.h"

#include<cassert>
#include<cmath>
#include<iostream>


#include <sbml/SBMLDocument.h>
#include <sbml/Species.h>
#include <sbml/KineticLaw.h>
#include <sbml/Reaction.h>
#include <sbml/SpeciesReference.h>
#include <sbml/Model.h>
#include <sbml/Parameter.h>
#include <sbml/Compartment.h>

#include"ReactionGraphFunc.h"
#include "Event.h"

using namespace std;

ReactionNodeFunc::ReactionNodeFunc(int p_id){
	id = p_id;
	is_deleted = false;
	marked = false;
}

ReactionNodeFunc::~ReactionNodeFunc(){
}

void ReactionNodeFunc::RemoveReactant(ReactionNodeFunc *node){
	if (node->GetId() % 2 == 0){
		//In this case node is a MoleculeNodeFunc
		mono_reactants.remove((MoleculeNodeFunc *) node);
	} else {
		//Int this case node is a MultiNodeFunc
		multi_reactants.remove((MultiNodeFunc *)node);
	}
}

void ReactionNodeFunc::RemoveOneReactant(ReactionNodeFunc *node){
	if (node->GetId() % 2 == 0){
		//In this case node is a MoleculeNodeFunc
		list<MoleculeNodeFunc *>::iterator it;
		for(it = mono_reactants.begin(); it != mono_reactants.end(); it++){
			if (node == *it){
				mono_reactants.erase(it);
				return;
			}
		}		
	} else {
		//Int this case node is a MultiNodeFunc
		list<MultiNodeFunc *>::iterator it;
		for(it = multi_reactants.begin(); it != multi_reactants.end(); it++){
			if (node == *it){
				multi_reactants.erase(it);
				return;
			}
		}	
	}
}


void ReactionNodeFunc::AddReactant(ReactionNodeFunc *node){
	if (node->GetId() % 2 == 0){
		mono_reactants.push_back((MoleculeNodeFunc*)node);
	} else {
		multi_reactants.push_back((MultiNodeFunc*)node);
	}
}

void ReactionNodeFunc::RemoveProduct(ReactionNodeFunc *node){
	if (node->GetId() % 2 == 0){
		//In this case node is a MoleculeNodeFunc
		list<pair <MoleculeNodeFunc *, Expr *> >::iterator it;
		list<pair <MoleculeNodeFunc *, Expr *> >::iterator to_be_deleted;
		for (it = this->mono_products.begin(); it != this->mono_products.end();){
			if ((*it).first->GetId() == node->GetId()){
				to_be_deleted = it;
				it++;
				delete (*to_be_deleted).second;
				mono_products.erase(to_be_deleted);
			} else {
				it++;
			}
		}
	} else {
		//Int this case node is a MultiNodeFunc
		multi_products.remove((MultiNodeFunc *)node);
	}
}

void ReactionNodeFunc::RemoveOneProduct(ReactionNodeFunc *node){
	if (node->GetId() % 2 == 0){
		//In this case node is a MoleculeNodeFunc
		list<pair <MoleculeNodeFunc *, Expr *> >::iterator it;
		for (it = this->mono_products.begin(); it != this->mono_products.end();it++){
			if ((*it).first->GetId() == node->GetId()){
				delete it->second;
				mono_products.erase(it);
				return;
			}
		}
	} else {
		//Int this case node is a MultiNodeFunc
		list<MultiNodeFunc *>::iterator it;
		for (it = multi_products.begin(); it != multi_products.end();it++){
			if ((*it)->GetId() == node->GetId()){
				multi_products.erase(it);
				return;
			}
		}

	}
}

void ReactionNodeFunc::AddProduct(ReactionNodeFunc *node, Expr *expr){
	if (node->GetId() % 2 == 0){
		pair<MoleculeNodeFunc *, Expr * > p;

		p.first = (MoleculeNodeFunc*)node;
		p.second = expr;
		mono_products.push_back(p);
	} else {
		multi_products.push_back((MultiNodeFunc*)node);
	}
}

void ReactionNodeFunc::SetDeleted(bool val){
	is_deleted = val;
}

void ReactionNodeFunc::UpdateNodesToBeVisited(list<ReactionNodeFunc *> *nodesToBeVisited, hash_map<ReactionNodeFunc *, bool>  *visitedNodes){
	if (visitedNodes->find(this) == visitedNodes->end()){
		nodesToBeVisited->insert(nodesToBeVisited->end(), this->mono_reactants.begin(), this->mono_reactants.end());
		nodesToBeVisited->insert(nodesToBeVisited->end(), this->multi_reactants.begin(), this->multi_reactants.end());
	}	  
}


MoleculeNodeFunc::MoleculeNodeFunc(int p_id):ReactionNodeFunc(p_id){
	present_in_run = 0;
}


void  MoleculeNodeFunc::PrintDot(Environment * e, ofstream &fout){
	list<pair<MoleculeNodeFunc *, Expr *> >::iterator n_it;
	int node_id;

	for (n_it = mono_products.begin(); n_it != mono_products.end(); n_it++){
		if (id < 0){
			fout << e->GetST()->GetMolNameFromID(abs(id/2)) << " ";
		} else {
			fout << e->GetST()->GetEntNameFromID(id/2) << " ";
		}

		fout << " -> ";

		node_id = (*n_it).first->GetId()/2;

		if (node_id < 0){
			fout << e->GetST()->GetMolNameFromID(abs(node_id)) << " ";
		} else if (node_id > 0) {
			fout << e->GetST()->GetEntNameFromID(node_id) << " ";
		} else {
			fout << "Nil ";
		}

		fout << "[color=darkgreen,label=\"";

		/*for (r_it = (*n_it).second->begin(); r_it != (*n_it).second->end(); r_it++){
			fout << *r_it << " ";
		}*/
		if ((*n_it).second != NULL){
			(*n_it).second->Print(&fout, PRINT_GRAPH);
		} else {
			fout << INF_RATE;
		}

		fout << "\"];" << endl;
	}

}

void MoleculeNodeFunc::InsertSBMLSpecie(Model_t *model, Environment *env){
	string molecule_name;
	if (this->id > 0)
		molecule_name = env->GetST()->GetEntNameFromID(this->id / 2);
	else if (this->id < 0){
		molecule_name = env->GetST()->GetMolNameFromID(abs(this->id / 2));
	} else {
		molecule_name = "Nil";
	}

	Species_t *specie =	Model_createSpecies(model);
	Species_setId(specie, molecule_name.c_str());
	Species_setName(specie, molecule_name.c_str());
	Species_setCompartment(specie, "compartment");
	Species_setInitialAmount(specie, this->present_in_run);
	Species_setBoundaryCondition(specie, false);
}


void MoleculeNodeFunc::InsertSBMLReactions(Model_t *model, Environment *env, int &constants_counter, int &reactions_counter){
 
	list<pair<MoleculeNodeFunc *, Expr *> >::iterator products_it;
	list<double>::iterator rates_it;
	
	int id;
	Reaction_t *reaction;
	KineticLaw_t *kinetic_law;
	Parameter_t *parameter;
	SpeciesReference_t *reference1;
	SpeciesReference_t *reference2;
	stringstream stream;
	string reactant;

	for (products_it = mono_products.begin(); products_it != mono_products.end(); products_it++){
		//for (rates_it = (*products_it).second->begin(); rates_it != (*products_it).second->end(); rates_it++){

			reaction = Model_createReaction(model);
			stream << "R" << reactions_counter++;
			Reaction_setId(reaction, stream.str().c_str());
			stream.str("");
			Reaction_setReversible(reaction, false);

			kinetic_law = Model_createKineticLaw(model);
			parameter = Model_createKineticLawParameter(model);

			stream << "c" << constants_counter++;
			Parameter_setName(parameter, stream.str().c_str());
			Parameter_setId(parameter, stream.str().c_str());
			Parameter_setValue(parameter, 0);
			stream.str("");
		
			id = this->id/2;
			if (id > 0){
				stream << env->GetST()->GetEntNameFromID(id);
			} else if (id < 0){
				stream << env->GetST()->GetMolNameFromID(abs(id));
			} else {
				//reactant should never be Nil
				assert(false);
				stream << "Nil";
			}

			reactant = stream.str().c_str();
			stream.str("");

			/*stream << " * " << Parameter_getName(parameter);*/
			if ((*products_it).second != NULL){
				(*products_it).second->Print(&stream, PRINT_SBML);
			} else {
				stream << INF_RATE;
			}


			KineticLaw_setFormula(kinetic_law, stream.str().c_str());
			stream.str("");

			Reaction_setKineticLaw(reaction, kinetic_law);

			id = (*products_it).first->id/2;
			if (id > 0){
				stream << env->GetST()->GetEntNameFromID(id);
			} else if (id < 0){
				stream << env->GetST()->GetMolNameFromID(abs(id));
			} else {
				stream << "Nil";
			}

			reference1 = Model_createReactant(model);
			SpeciesReference_initDefaults(reference1);
			SpeciesReference_setSpecies(reference1, reactant.c_str());
			reference2 = Model_createProduct(model);
			SpeciesReference_initDefaults(reference2);
			SpeciesReference_setSpecies(reference2, stream.str().c_str());

			stream.str("");

		//}
	}
}


void MultiNodeFunc::InsertSBMLReactions(Model_t *model, Environment *env, int &constants_counter, int &reactions_counter){
	list<double>::iterator rates_it;
	list<MoleculeNodeFunc *>::iterator reactants_it;
	list<MoleculeNodeFunc *>::iterator tmp_it;
	list<pair<MoleculeNodeFunc *, Expr *> >::iterator products_it;

	MoleculeNodeFunc* firstReactant;
	MoleculeNodeFunc* secondReactant;

	Reaction_t *reaction;
	KineticLaw_t *kinetic_law;
	Parameter_t *parameter;
	SpeciesReference_t *reference;

	int id, index;

	stringstream stream;

	string first_name;
	string second_name;

	string *products_name;

	hash_map<int, pair<int, int> > products_map;
	hash_map<int, pair<int, int> >::iterator products_map_it;
	pair <int, int> int_pair;
	int tmp_int;

	//0 means monomolecular
	//1 means bimolecular
	//2 means homodimerization  
	int reaction_kind = -1;

	//cases with more than two reactants are not handled
	//TODO: come cavolo gestire due Multi di fila? che significato hanno in SBML?
	//Bisognerebbe stampare ricorsivamente le Multi... 
	assert(mono_reactants.size() > 0 && mono_reactants.size() <= 2);


	tmp_it = mono_reactants.begin();
	id = (*tmp_it)->GetId()/2;
	if (id > 0){
		stream << env->GetST()->GetEntNameFromID(id);
	} else if (id < 0){
		stream << env->GetST()->GetMolNameFromID(abs(id));
	} else {
		//reactant should never be Nil
		assert(false);
		stream << "Nil";
	}

	first_name = stream.str();
	stream.str("");
	

	if (mono_reactants.size() == 1){
		reaction_kind = 0;
	} else if (mono_reactants.size() == 2){
		tmp_it = mono_reactants.begin();
		firstReactant = *tmp_it;
		tmp_it++;
		secondReactant = *(tmp_it);
		id = (*tmp_it)->GetId()/2;
		if (id > 0){
			stream << env->GetST()->GetEntNameFromID(id);
		} else if (id < 0){
			stream << env->GetST()->GetMolNameFromID(abs(id));
		} else {
			//reactant should never be Nil
			assert(false);
			stream << "Nil";
		}

		second_name = stream.str();
		stream.str("");

		if (secondReactant != firstReactant){
			reaction_kind  = 1;
		} else {
			reaction_kind = 2;
		}
	}

	products_name = new string[mono_products.size()];

	for (index = 0, products_it = mono_products.begin(); products_it != mono_products.end(); products_it++, index++){
		id = (*products_it).first->GetId()/2;
		if (id > 0){
			stream << env->GetST()->GetEntNameFromID(id);
		} else if (id < 0){
			stream << env->GetST()->GetMolNameFromID(abs(id));
		} else {
			stream << "Nil";
		}

		products_name[index] = stream.str();
		stream.str("");
	}



	//for (rates_it = this->rates->begin(); rates_it != this->rates->end(); rates_it++){

		//for (reactants_it = mono_reactants.begin(); reactants_it != mono_reactants.end(); reactants_it++){
			reaction = Model_createReaction(model);
			stream << "R" << reactions_counter++;
			Reaction_setId(reaction, stream.str().c_str());
			stream.str("");
			Reaction_setReversible(reaction, false);

			kinetic_law = Model_createKineticLaw(model);
			parameter = Model_createKineticLawParameter(model);

			stream << "c" << constants_counter++;
			Parameter_setName(parameter, stream.str().c_str());
			Parameter_setId(parameter, stream.str().c_str());
			Parameter_setValue(parameter, 0);
			stream.str("");

			if (expr != NULL){
				expr->Print(&stream, PRINT_SBML);
			} else {
				stream << INF_RATE;
			}
			/*if (reaction_kind == 0){
				stream << first_name << " * " << Parameter_getName(parameter);
			} else if (reaction_kind == 1){
				stream << first_name << " * " << second_name << " * " << Parameter_getName(parameter);
			} else {
				stream << "(" <<  Parameter_getName(parameter) << ")" << "/2 * " << first_name << " * " << "(" << first_name << " - 1)";
			}*/

			KineticLaw_setFormula(kinetic_law, stream.str().c_str());
			stream.str("");

			Reaction_setKineticLaw(reaction, kinetic_law);

			if (reaction_kind == 0){
				reference = Model_createReactant(model);
				SpeciesReference_initDefaults(reference);
				SpeciesReference_setSpecies(reference, first_name.c_str());
			} else if (reaction_kind == 1){
				reference = Model_createReactant(model);
				SpeciesReference_initDefaults(reference);
				SpeciesReference_setSpecies(reference, first_name.c_str());
				reference = Model_createReactant(model);
				SpeciesReference_initDefaults(reference);
				SpeciesReference_setSpecies(reference, second_name.c_str());
			} else if (reaction_kind == 2){
				reference = Model_createReactant(model);
				SpeciesReference_initDefaults(reference);
				SpeciesReference_setStoichiometry(reference, 2);
				SpeciesReference_setSpecies(reference, first_name.c_str());
			}

			/*for (index = 0, products_it = mono_products.begin(); products_it != mono_products.end(); products_it++, index++){
				reference = Model_createProduct(model);
				SpeciesReference_initDefaults(reference);
				SpeciesReference_setSpecies(reference, products_name[index].c_str());
			}*/

			for (index = 0, products_it = mono_products.begin(); products_it != mono_products.end(); products_it++, index++){
				if (products_map.find((*products_it).first->GetId()) == products_map.end()){
					int_pair.first = index;
					int_pair.second = 1;
					products_map[(*products_it).first->GetId()] = int_pair;
				} else {
					tmp_int = products_map[(*products_it).first->GetId()].second;
					products_map[(*products_it).first->GetId()].second = tmp_int + 1;
				}
			}

			for (products_map_it = products_map.begin(); products_map_it != products_map.end(); products_map_it++){
				reference = Model_createProduct(model);
				SpeciesReference_initDefaults(reference);
				SpeciesReference_setSpecies(reference, products_name[(*products_map_it).second.first].c_str());
				SpeciesReference_setStoichiometry(reference, (*products_map_it).second.second);
			}

		//}
	//}

}
/*void MoleculeNodeFunc::ReduceRateLists(){
	list<pair <MoleculeNodeFunc *, list<double> *> >::iterator p_it;
	list<double>::iterator d_it;
	for (p_it = mono_products.begin(); p_it != mono_products.end(); p_it++){
			ReduceRateList((*p_it).second);
	}
}*/

/**************************************************************
 *	Eliminate this kind of infinite reactions
 * 
 *
 * A --(0.2)---> B --(inf)-->D						A--(0.1)--> D
 *				  /	\						----->		
 * C --(0.4)-/		 \--(inf)-->F						A--(0.1)--> F
 *
 *																C--(0.2)--> D
 *																
 *																C--(0.2)--> F
 *TODO: in questo commento scirivi anche che semplifica i multinodi a fininito.
 *
 **************************************************************/

void MoleculeNodeFunc::CollapseInfReactions(list<ReactionNodeFunc*> *nodes_to_be_checked, ReactionsGraphFunc *graph){
	list<pair <MoleculeNodeFunc *, Expr *> >::iterator mono_products_it;
	//list<ReactionNodeFunc *>::iterator n_it;
	list<MultiNodeFunc *>::iterator multi_products_it, multi_reactants_it;
	list<MoleculeNodeFunc *>::iterator mono_reactants_it;
	list<double>::iterator l_it;
	list<MultiNodeFunc *> multi_reactants_copy;
	list<MultiNodeFunc *> multi_products_copy;
	list<MultiNodeFunc *> multi_to_be_deleted;
	Expr *new_expr, *old_expr;
	MultiNodeFunc *clone;
	MultiNodeFunc *clone2;
	int n_inf_reactions = 0;
	bool reduct = true;
	//list node_to_be_deleted;

	/*this->CheckDoubleInfArrows(&node_to_be_deleted);


	for (mono_reactants_it = mono_reactants.begin(); mono_reactants_it != mono_reactants.end(); mono_reactants_it++){
		(*mono_reactants_it)->CheckDoubleInfArrows(&node_to_be_deleted);
	}*/


	for (mono_products_it = mono_products.begin(); mono_products_it != mono_products.end(); mono_products_it++){
		if ((*mono_products_it).second == NULL){
			n_inf_reactions++;
		}
	}

	for (multi_products_it = multi_products.begin(); multi_products_it != multi_products.end(); multi_products_it++){
		if ((*multi_products_it)->GetExpr() == NULL){
			n_inf_reactions++;
		}

		if ((*multi_products_it)->GetNumberOfReactants() > 1){
			reduct = false;
		}
	}

	if (multi_products.size() + mono_products.size() > 1){
		for (multi_reactants_it = multi_reactants.begin(); multi_reactants_it != multi_reactants.end(); multi_reactants_it++){
			if ((*multi_reactants_it)->GetNumberOfMultiReactants() > 0){
				reduct = false;
			}
		}
	}
	//da attivare per fare il test con tutto insieme
	if (reduct && n_inf_reactions > 0) {
	//if (false){
	//da attivare per testare nodi che hanno solo mono_products e mono_reactants
	//if (multi_reactants.size() == 0 && multi_products.size() == 0 && reduct && n_inf_reactions > 0) {
	//da attivare per fare il test sui nodi che hanno prodtti mono, prodtti multi e reagenti mono
	//if (multi_reactants.size() == 0 && reduct && n_inf_reactions > 0) {
	//da attiver per fare il test sui nodi che hanno prodtti mono, reagenti multi e reagenti mono
	//if (multi_products.size() == 0 && reduct && n_inf_reactions > 0) {
	//per testare tutti i casi ad eccezione di quelli che presentano sia multi prodotti che multi reaenti insieme
	//if ((multi_products.size() == 0 || multi_reactants.size() == 0) && n_inf_reactions > 0 && reduct){
		if (present_in_run > 0)
      {
         assert((int)(mono_products.size() + multi_products.size()) == n_inf_reactions);
         int step = present_in_run / n_inf_reactions;
         int entities_added = 0;
			for (mono_products_it = mono_products.begin(); mono_products_it != mono_products.end(); mono_products_it++)
         {
				(*mono_products_it).first->AddToInRun(step);
            entities_added += step;
			}
			for (multi_products_it = multi_products.begin(); multi_products_it != multi_products.end(); multi_products_it++)
         {
				(*multi_products_it)->AddToInRun(step);
            entities_added += step;
			}

         int diff = present_in_run - entities_added;
         assert (diff > 0);
         // add what's left to someone
         if (mono_products.size() > 0) 
            mono_products.front().first->AddToInRun(diff);
         else
            multi_products.front()->AddToInRun(diff);
		}
	
	
	
		//Create copies of the MultiNodeFunc and delete the original nodes
		for (multi_reactants_it =  multi_reactants.begin(); multi_reactants_it != multi_reactants.end(); multi_reactants_it++){
			old_expr = (*multi_reactants_it)->GetExpr();
			if (old_expr != NULL){
				old_expr = old_expr->Clone();
			}
			multi_reactants_copy.push_back((*multi_reactants_it)->CloneWithoutOneProduct(-1, this, old_expr));
		}

		multi_to_be_deleted.insert(multi_to_be_deleted.end(),multi_reactants.begin(), multi_reactants.end());
		
		for (multi_reactants_it =  multi_to_be_deleted.begin(); multi_reactants_it != multi_to_be_deleted.end(); multi_reactants_it++){
			(*multi_reactants_it)->NotRecursiveDelete();
		}


		for (multi_products_it = multi_products.begin(); multi_products_it != multi_products.end(); multi_products_it++){
			old_expr = (*multi_products_it)->GetExpr();
			if (old_expr != NULL){
				old_expr = old_expr->Clone();
			}
			multi_products_copy.push_front((*multi_products_it)->CloneWithoutOneReactant(-1, this, old_expr));
		}

		multi_to_be_deleted.clear();
		multi_to_be_deleted.insert(multi_to_be_deleted.end(),multi_products.begin(), multi_products.end());

		for (multi_reactants_it =  multi_to_be_deleted.begin(); multi_reactants_it != multi_to_be_deleted.end(); multi_reactants_it++){
			(*multi_reactants_it)->NotRecursiveDelete();
		}

		for (mono_reactants_it = mono_reactants.begin(); mono_reactants_it != mono_reactants.end(); mono_reactants_it++){
			if ((*mono_reactants_it) != this){
				nodes_to_be_checked->push_back(*mono_reactants_it);

				old_expr = (*mono_reactants_it)->GetExpr(this);
				if (old_expr != NULL){
					old_expr = old_expr->Clone();
				}

				(*mono_reactants_it)->RemoveProduct(this);
				for (mono_products_it = mono_products.begin(); mono_products_it != mono_products.end(); mono_products_it++){
					if ((*mono_products_it).first != this) {
						if (mono_reactants_it == mono_reactants.begin()){
							(*mono_products_it).first->RemoveReactant(this);
						}

						new_expr =  old_expr;
						if(new_expr != NULL){
							new_expr =new_expr->Clone();
							new_expr = new_expr->DivideBy(n_inf_reactions);
						}
						(*mono_products_it).first->AddReactant(*mono_reactants_it);
						(*mono_reactants_it)->AddProduct((*mono_products_it).first, new_expr);
					}
				}

				for (multi_products_it = multi_products_copy.begin(); multi_products_it != multi_products_copy.end(); multi_products_it++){
					new_expr =  old_expr;
					if(new_expr != NULL){
						new_expr =new_expr->Clone();
						new_expr = new_expr->DivideBy(n_inf_reactions);
					}

					clone = (*multi_products_it)->CloneWithoutOneReactant(graph->GetNewMultiId(), NULL, new_expr);
					clone->AddReactant(*mono_reactants_it);
					(*mono_reactants_it)->AddProduct(clone, NULL);
					graph->AddNode(clone);
				}


				delete old_expr;
			}
		}

		for (multi_reactants_it = multi_reactants_copy.begin(); multi_reactants_it != multi_reactants_copy.end(); multi_reactants_it++){
			old_expr = (*multi_reactants_it)->GetExpr();
			if (old_expr != NULL){
				old_expr = old_expr->Clone();
			}
			(*multi_reactants_it)->RemoveProduct(this);
			for (mono_products_it = mono_products.begin(); mono_products_it != mono_products.end(); mono_products_it++){
				if ((*mono_products_it).first != this) {
					new_expr =  old_expr;
					if(new_expr != NULL){
						new_expr =new_expr->Clone();
						new_expr = new_expr->DivideBy(n_inf_reactions);
					}

					clone = (*multi_reactants_it)->CloneWithoutOneReactant(graph->GetNewMultiId(), NULL, new_expr);
					clone->AddProduct((*mono_products_it).first, NULL);
					(*mono_products_it).first->AddReactant(clone);
					graph->AddNode(clone);
				}
			}

			for (multi_products_it = multi_products_copy.begin(); multi_products_it != multi_products_copy.end(); multi_products_it++){
				new_expr =  old_expr;
				if(new_expr != NULL){
					new_expr =new_expr->Clone();
					new_expr = new_expr->DivideBy(n_inf_reactions);
				}

				clone = (*multi_reactants_it)->CloneWithoutOneReactant(graph->GetNewMultiId(), NULL, new_expr);
				clone2 = (*multi_products_it)->CloneWithoutOneReactant(graph->GetNewMultiId(), NULL, NULL);

				clone->AddProduct(clone2, NULL);
				clone2->AddReactant(clone);

				graph->AddNode(clone);
				graph->AddNode(clone2);
				nodes_to_be_checked->push_back(clone2);
			}




			delete old_expr;
		}

		this->is_deleted = true;

		for (multi_reactants_it = multi_reactants_copy.begin(); multi_reactants_it != multi_reactants_copy.end(); multi_reactants_it++){
			(*multi_reactants_it)->NotRecursiveDelete();
		}

		for (multi_products_it = multi_products_copy.begin(); multi_products_it != multi_products_copy.end(); multi_products_it++){
			(*multi_products_it)->NotRecursiveDelete();
		}

		for (mono_reactants_it = mono_reactants.begin(); mono_reactants_it != mono_reactants.end(); mono_reactants_it++){
			(*mono_reactants_it)->CheckDoubleInfArrows();
		}
	}


	//TODO: dealloca la lista di copie di nodi.
}

void MoleculeNodeFunc::CollapseFromMolToMol(list<ReactionNodeFunc*> *nodes_to_be_checked, ReactionsGraphFunc *graph){
	//This function collapses nodes that are involved only in monomolecular
	//reactions
	
	//Check if the molecule is involved only in monomolecular reactions
	if (this->multi_products.size() == 0 && this->multi_reactants.size() == 0 && this->mono_products.size() > 0){
		list<pair <MoleculeNodeFunc *, Expr *> >::iterator mono_products_it;
		list<MoleculeNodeFunc *>::iterator mono_reactants_it;
		Expr *new_expr, *old_expr = NULL;
		list<double>::iterator l_it;

		bool reduce = true;
		int n_inf_reactions = 0;

		//Check if all the reactions this molecule can is involved as reactant
		//have infinite rate. During this operation reactions with infinite rate
		//are counted
		for (mono_products_it = mono_products.begin(); mono_products_it != mono_products.end() && reduce; mono_products_it++){
			if ((*mono_products_it).second == NULL){
				n_inf_reactions++;
			} else {
				reduce = false;
			}
		}


		if (n_inf_reactions > 0 && reduce){
			if (this->present_in_run != 0){

            assert((int)(mono_products.size()) == n_inf_reactions);
            int step = present_in_run / n_inf_reactions;
            int entities_added = 0;
				for (mono_products_it = mono_products.begin(); mono_products_it != mono_products.end(); mono_products_it++){
					(*mono_products_it).first->AddToInRun(step);
               entities_added += step;
				}

            int diff = present_in_run - entities_added;
            assert (diff >= 0);
            mono_products.front().first->AddToInRun(diff);				
			}


			//This molecule is bypassed and then deleted
			for (mono_reactants_it = mono_reactants.begin(); mono_reactants_it != mono_reactants.end(); mono_reactants_it++){
				old_expr = (*mono_reactants_it)->GetExpr(this);
				if (old_expr != NULL){
					old_expr = old_expr->Clone();
				}
				(*mono_reactants_it)->RemoveProduct(this);
				for (mono_products_it = mono_products.begin(); mono_products_it != mono_products.end(); mono_products_it++){
					if ((*mono_products_it).first != this) {
						if (mono_reactants_it == mono_reactants.begin()){
							(*mono_products_it).first->RemoveReactant(this);
						}

						new_expr = old_expr;
						if (new_expr != NULL){
							new_expr = old_expr->Clone();
							new_expr = new_expr->DivideBy(n_inf_reactions);
						}

						(*mono_products_it).first->AddReactant(*mono_reactants_it);
						(*mono_reactants_it)->AddProduct((*mono_products_it).first, new_expr);
					}
				}
				delete old_expr;
			}
			this->is_deleted = true;
			for (mono_reactants_it = mono_reactants.begin(); mono_reactants_it != mono_reactants.end(); mono_reactants_it++){
				(*mono_reactants_it)->CheckDoubleInfArrows();
			}
		}
	}
}


void MoleculeNodeFunc::CollapseFromMolMulTo1Mol(list<ReactionNodeFunc*> *nodes_to_be_checked, ReactionsGraphFunc *graph){
	//Reduce molecules that is involved as reactant in only one monomolecular reaction

	//Check if the molecule is involved in only one monomolecular 
	if (this->multi_products.size() == 0 && this->mono_products.size() == 1){
		pair<MoleculeNodeFunc*, Expr *> p = *(this->mono_products.begin());
		if (this->GetExpr(p.first) == NULL) {
			list<pair <MoleculeNodeFunc *, Expr *> >::iterator mono_products_it;
			list<MoleculeNodeFunc *>::iterator mono_reactants_it;
			list<MultiNodeFunc *>::iterator multi_reactants_it;
			Expr *expr;
			//list<double>::iterator l_it;

			(*(mono_products.begin())).first->AddToInRun(present_in_run);

			MoleculeNodeFunc *product = p.first;
			//Remove the monomolecular reaction that involve this molecule as reactant
			product->RemoveReactant(this);

			//Connect all the molecules involved as reactants with the molecule product
			for (mono_reactants_it = mono_reactants.begin(); mono_reactants_it != mono_reactants.end(); mono_reactants_it++){
				expr = (*mono_reactants_it)->GetExpr(this);
				if (expr != NULL){
					expr = expr->Clone();
				}
				(*mono_reactants_it)->RemoveProduct(this);
				product->AddReactant(*mono_reactants_it);
				(*mono_reactants_it)->AddProduct(product, expr);
			}

			//Connect all the multinode involved as reactants with the molecule product
			for (multi_reactants_it = multi_reactants.begin(); multi_reactants_it != multi_reactants.end(); multi_reactants_it++){
				(*multi_reactants_it)->RemoveProduct(this);
				product->AddReactant(*multi_reactants_it);
				(*multi_reactants_it)->AddProduct(product, NULL);
			}


			this->is_deleted = true;
			/*for (mono_reactants_it = mono_reactants.begin(); mono_reactants_it != mono_reactants.end(); mono_reactants_it++){
			  (*mono_reactants_it)->CheckDoubleInfArrows();
			  }*/
		}
	}
}


void MoleculeNodeFunc::CollapseFrom1MolToMolMul(list<ReactionNodeFunc*> *nodes_to_be_checked, ReactionsGraphFunc *graph){
	//Collapse nodes that are involved as reactant in only one monomolecular reaction

	//Check if the molecule is involved as reactant in only one monomolecular reaction
	if (this->multi_reactants.size() == 0 && this->mono_reactants.size() == 1 && this->mono_products.size() > 0){
		list<pair <MoleculeNodeFunc *, Expr *> >::iterator mono_product_it;
		list<MultiNodeFunc *>::iterator multi_product_it;
		Expr *expr, *new_expr;
		list<double>::iterator l_it;

		int n_inf_reactions = 0;
		bool reduce = true;

		//Check if the molecule is involved as reactant exclusively in reactions with infinite rate
		for (mono_product_it = mono_products.begin(); mono_product_it != mono_products.end(); mono_product_it++){
			if((this->GetExpr(((*mono_product_it).first))) != NULL){
				reduce = false;
			}
			n_inf_reactions++;
		}

		for (multi_product_it = multi_products.begin(); multi_product_it != multi_products.end(); multi_product_it++){
			if ((*multi_product_it)->GetExpr() != NULL){
				reduce = false;
			}
			n_inf_reactions++;
		}
		
		if (reduce){
			MoleculeNodeFunc * reactant;

			if (this->present_in_run > 0)
         {
            assert((int)(mono_products.size() + multi_products.size()) == n_inf_reactions);
            int step = present_in_run / n_inf_reactions;
            int entities_added = 0;

				for (mono_product_it = mono_products.begin(); mono_product_it != mono_products.end(); mono_product_it++){
					(*mono_product_it).first->AddToInRun(step);
               entities_added += step;
				}
				for (multi_product_it = multi_products.begin(); multi_product_it != multi_products.end(); multi_product_it++){
					(*multi_product_it)->AddToInRun(step);
               entities_added += step;
				}

            int diff = present_in_run - entities_added;
            assert (diff > 0);
            // add what's left to someone
            if (mono_products.size() > 0) 
               mono_products.front().first->AddToInRun(diff);
            else
               multi_products.front()->AddToInRun(diff);
			}
		
			reactant = *(mono_reactants.begin());

			expr = reactant->GetExpr(this);
			if (expr != NULL){
				expr = expr->Clone();
				expr = expr->DivideBy(n_inf_reactions);
			}

			
			for (mono_product_it = mono_products.begin(); mono_product_it != mono_products.end(); mono_product_it++){
				new_expr = expr;
				if(new_expr != NULL){
					new_expr =new_expr->Clone();
				}
				(*mono_product_it).first->RemoveReactant(this);
				reactant->AddProduct((*mono_product_it).first, new_expr);
				(*mono_product_it).first->AddReactant(reactant);
			}		

			for (multi_product_it = multi_products.begin(); multi_product_it != multi_products.end(); multi_product_it++){
				new_expr =  expr;
				if(new_expr != NULL){
					new_expr =new_expr->Clone();
				}
				(*multi_product_it)->RemoveReactant(this);
				reactant->AddProduct(*multi_product_it, NULL);
				(*multi_product_it)->SetRates(new_expr);
				(*multi_product_it)->AddReactant(reactant);
			}		


			reactant->RemoveProduct(this);
			this->is_deleted = true;
			/*for (mono_reactants_it = mono_reactants.begin(); mono_reactants_it != mono_reactants.end(); mono_reactants_it++){
			  (*mono_reactants_it)->CheckDoubleInfArrows();
			  }*/
			delete(expr);
		}

		/*if(*(this->GetRates(p.first)->begin()) == HUGE_VAL) {

			MoleculeNodeFunc* reactant = *(this->mono_reactants.begin());
			list<pair <MoleculeNodeFunc *, list <double> *> >::iterator mono_products_it;
			list<MoleculeNodeFunc *>::iterator mono_reactants_it;
			list<MultiNodeFunc *>::iterator multi_reactants_it;
			list<double> *old_rates, *new_rates;
			list<double>::iterator l_it;

			MoleculeNodeFunc *product = p.first;

			product->RemoveOneReactant(this);

			for (mono_reactants_it = mono_reactants.begin(); mono_reactants_it != mono_reactants.end(); mono_reactants_it++){
				rates = new list<double>(*((*mono_reactants_it)->GetRates(this)));
				(*mono_reactants_it)->RemoveProduct(this);
				product->AddReactant(*mono_reactants_it);
				(*mono_reactants_it)->AddProduct(product, rates);
			}

			for (multi_reactants_it = multi_reactants.begin(); multi_reactants_it != multi_reactants.end(); multi_reactants_it++){
				rates = new list<double>(*((*multi_reactants_it)->GetRates()));
				(*multi_reactants_it)->RemoveProduct(this);
				product->AddReactant(*multi_reactants_it);
				(*multi_reactants_it)->AddProduct(product, rates);
			}


			this->is_deleted = true;*/
			/*for (mono_reactants_it = mono_reactants.begin(); mono_reactants_it != mono_reactants.end(); mono_reactants_it++){
			  (*mono_reactants_it)->CheckDoubleInfArrows();
			  }*/
		//}
	}
}

void MoleculeNodeFunc::CollapseInvolvedOnlyInInfBim(list<ReactionNodeFunc*> *nodes_to_be_checked, ReactionsGraphFunc *graph){
	list<MultiNodeFunc *>::iterator mu_p_it;
	list<MultiNodeFunc *>::iterator mu_r_it;
	list<MoleculeNodeFunc *>::iterator mo_r_it;
	list<pair<MoleculeNodeFunc *, Expr *> >::iterator mo_p_it;
	MultiNodeFunc *mu_p;
	//, *mu_r;

	hash_map<int, bool> multi_reactatns_associated;

	marked = true;
	bool node_found = false;

	if (mono_reactants.empty() && mono_products.empty()){  //node has to be involved only in multimolecular reactions


		for(mu_p_it = multi_products.begin(); mu_p_it != multi_products.end() && marked; mu_p_it++){
			mu_p = *mu_p_it;
			//All the multi products have to involve more then one reactant otherwise the analysed molecule is involved
			//in a monomolecular reaction
			if (mu_p->GetNumberOfReactants() < 2){
				marked = false;
			}
			//All the multi products mast have infinite rate otherwise we are not dealing with an "immediate" molecule 
			if (mu_p->GetExpr() != NULL){
				marked = false;
			}
			
			//Now we want to find a multi reactant node that has as products the same molecule that the analysed multi
			//product node has as reactants (we suppose that this multi node aren't connected to others multi nodes)
			assert(mu_p->GetNumberOfMultiProducts() == 0);
			for(mu_r_it = multi_reactants.begin(),node_found = false; mu_r_it != multi_reactants.end() && !node_found && marked; mu_r_it++){
				//fare funzione che per ognuna controlla se è uguale o meno
			   node_found = mu_p->HasAsProductsSameReactants(*mu_r_it);
				if (node_found){
					multi_reactatns_associated[(*mu_r_it)->GetId()] = true;
				}
			}
			marked = node_found;
		}
		
		//If multi products list is bigger than the multi reactants one it is still possible that we have to reduce the
		//analysed node. We have to check if for each multi reactants node that has not been previously associated with a
		//multi products node there is a multi node products that have as reactants its products
		if (multi_reactants.size() > multi_products.size() && marked){
			for(mu_r_it = multi_reactants.begin(); mu_r_it != multi_reactants.end() && marked; mu_r_it++){
				//check if the multi reactant node has been previously associated 
				if (multi_reactatns_associated.find((*mu_r_it)->GetId()) == multi_reactatns_associated.end()){
					for(mu_p_it = multi_products.begin(), node_found = false; mu_p_it != multi_products.end() && node_found; mu_p_it++){
						node_found = (*mu_p_it)->HasAsProductsSameReactants(*mu_r_it);
					}
				}
				marked = node_found;
			}
		}
	} else {
		marked = false;
	}
}

void MultiNodeFunc::CollapseInfReactions(list<ReactionNodeFunc*> *nodes_to_be_checked, ReactionsGraphFunc *graph){
	list<pair <MoleculeNodeFunc *, Expr *> >::iterator mo_it;
	list<MultiNodeFunc *>::iterator mu_it;
	MultiNodeFunc* reactant;
	if (multi_reactants.size() == 1 && mono_reactants.size() == 0){
		reactant = *(multi_reactants.begin());
		reactant->RemoveProduct(this);
		for (mo_it = mono_products.begin(); mo_it != mono_products.end(); mo_it++){
			(*mo_it).first->RemoveReactant(this);
			(*mo_it).first->AddReactant(reactant);
			reactant->AddProduct((*mo_it).first, NULL);
		}

		for (mu_it = multi_products.begin(); mu_it != multi_products.end(); mu_it++){
			(*mu_it)->RemoveReactant(this);
			(*mu_it)->AddReactant(reactant);
			reactant->AddProduct(*mu_it, NULL);
		}

		this->is_deleted = true;
	}
}

Expr * MoleculeNodeFunc::GetExpr(MoleculeNodeFunc *node){
	list<pair <MoleculeNodeFunc *, Expr *> >::iterator it;
	for (it = mono_products.begin(); it != mono_products.end(); it++){
		if(node->GetId() == (*it).first->GetId()){
			return (*it).second;
		}
	}
	return NULL;	
}

void ReactionNodeFunc::EliminateMonoReaction(MoleculeNodeFunc* product){
	/*hash_map<ReactionNodeFunc *, bool> visited_nodes;
	hash_map<ReactionNodeFunc *, bool>::iterator it;
	list<ReactionNodeFunc *> to_be_visited;*/



	/*if (product->IsInRun()){
		return;
	}

	assert(!product->IsDeleted());

	product->UpdateNodesToBeVisited(&to_be_visited, &visited_nodes);

	if (to_be_visited.empty() && !product->IsInRun()){
		product->Delete();
	}
	
	for (ReactionNodeFunc* current = product; !to_be_visited.empty(); current = to_be_visited.front(), to_be_visited.pop_front()){
		assert(current->IsDeleted());
		if (current->IsInRun()){
			return;
		}	

		visited_nodes[current] = true;
		current->UpdateNodesToBeVisited(&to_be_visited, &visited_nodes);
	}
	
	//If we reach this point we have to mark for deletion all the visited nodes.
	//In fact they are unreachable, they will never be created during the
	//simulation
	for (it = visited_nodes.begin();	it != visited_nodes.end(); it++){
		(*it).first->Delete();
	}*/
}

/*void ReactionNodeFunc::EliminateMultiNodeFunc(MultiNodeFunc *product){
	this->RemoveProduct()
}*/

//Check if all the reactants of the MultiNodeFunc has been visited and are reachable

void MoleculeNodeFunc::CheckDoubleInfArrows(){
	hash_map < MoleculeNodeFunc *, Expr *> products;
	list<pair<MoleculeNodeFunc *, Expr *> >::iterator mo_it;
	Expr *expr;

	for (mo_it = mono_products.begin(); mo_it != mono_products.end(); mo_it++){
		if ( products.find(mo_it->first) == products.end()){
			products[mo_it->first] = mo_it->second;
		} else {
			if(mo_it->second != NULL){
				expr = products[mo_it->first];
				mo_it->second = mo_it->second->Add(expr);
				products[mo_it->first] = mo_it->second;
			}
			this->RemoveOneProduct(mo_it->first);
			mo_it->first->RemoveOneReactant(this);
		}
	}
}


/*void MoleculeNodeFunc::CheckDoubleInfArrows(list<MoleculeNodeFunc*> *node_to_be_removed){
	hash_map < MoleculeNodeFunc *, int> products;
	hash_map < MoleculeNodeFunc *, int>::iterator it;
	list<pair<MoleculeNodeFunc *, list<double> *> >::iterator mo_it;

	for (mo_it = mono_products.begin(); mo_it != mono_products.end(); mo_it++){
		if(*(mo_it->second->begin()) == HUGE_VAL){
			if ( products.find(mo_it->first) == products.end()){
				products[mo_it->first] = 0;
			} else {
				node_to_be_removed->push_back(mo_it->first);
			}
		}
	}
}*/


MultiNodeFunc::MultiNodeFunc(int p_id, Expr * p_expr):ReactionNodeFunc(p_id){
	expr = p_expr;
}

void MultiNodeFunc::PrintDot(Environment *e, ofstream &fout){
	list<MoleculeNodeFunc *>::iterator reactant_it;
	list<pair<MoleculeNodeFunc *, Expr *> >::iterator product_it;
	list<MultiNodeFunc *>::iterator multi_products_it;
	int node_id;

	


	fout << id << "[shape=polygon,sides=6,color=black,label=\"";
	
	if (expr != NULL){	
		expr->Print(&fout, PRINT_GRAPH);
	} else {
		fout << INF_RATE;
	}

   fout <<	"\"];" << endl;

	for (reactant_it = mono_reactants.begin(); reactant_it != mono_reactants.end(); reactant_it++){
		node_id = (*reactant_it)->GetId()/2;
		if (node_id < 0){
			fout << e->GetST()->GetMolNameFromID(abs(node_id))	<< " ";
		} else if (node_id > 0) {
			fout << e->GetST()->GetEntNameFromID(node_id)	<< " ";
		} else {
			fout << "Nil ";
		}

		fout << " -> ";

		fout << id << ";" << endl;

	}

	
	for (product_it = mono_products.begin(); product_it != mono_products.end(); product_it++){
		node_id = (*product_it).first->GetId()/2;
		fout << id << " -> ";
		if (node_id < 0){
			fout << e->GetST()->GetMolNameFromID(abs(node_id))	<< " ";
		} else if (node_id > 0) {
			fout << e->GetST()->GetEntNameFromID(node_id)	<< " ";
		} else {
			fout << "Nil ";
		}
		fout << ";" << endl;

	} 

	for (multi_products_it = multi_products.begin(); multi_products_it != multi_products.end(); multi_products_it++){
		fout << id << " -> " << (*multi_products_it)->GetId() << " ;" << endl;
	}
}

MultiNodeFunc * MultiNodeFunc::CloneWithoutOneProduct(int new_id, ReactionNodeFunc *product_to_eliminate, Expr *expression){
	list<MoleculeNodeFunc *>::iterator mo_it;
	list<MultiNodeFunc *>::iterator mu_it;
	list<pair<MoleculeNodeFunc *, Expr *> >::iterator mo_r_it;

	MultiNodeFunc *res = new MultiNodeFunc(new_id, expression);
	
	for (mo_it = mono_reactants.begin(); mo_it != mono_reactants.end(); mo_it++){
		res->AddReactant(*mo_it);
		(*mo_it)->AddProduct(res, NULL);
	}

	for (mu_it = multi_reactants.begin(); mu_it != multi_reactants.end(); mu_it++){
		res->AddReactant(*mu_it);
		(*mu_it)->AddProduct(res, NULL);
	}

	for (mo_r_it = mono_products.begin(); mo_r_it != mono_products.end(); mo_r_it++){
		if (product_to_eliminate == NULL || mo_r_it->first != product_to_eliminate){
			res->AddProduct(mo_r_it->first, NULL);
			mo_r_it->first->AddReactant(res);
		}
	}

	for (mu_it = multi_products.begin(); mu_it != multi_products.end(); mu_it++){
		if (product_to_eliminate == NULL || (*mu_it) != product_to_eliminate){
			res->AddProduct(*mu_it, NULL);
			(*mu_it)->AddReactant(res);
		}
	}

	return res;
}


MultiNodeFunc * MultiNodeFunc::CloneWithoutOneReactant(int new_id, ReactionNodeFunc *reactant_to_eliminate, Expr *expression){
	list<MoleculeNodeFunc *>::iterator mo_it;
	list<MultiNodeFunc *>::iterator mu_it;
	list<pair<MoleculeNodeFunc *, Expr *> >::iterator mo_r_it;

	MultiNodeFunc *res = new MultiNodeFunc(new_id, expression);

	for (mo_it = mono_reactants.begin(); mo_it != mono_reactants.end(); mo_it++){
		if (reactant_to_eliminate == NULL || (*mo_it) != reactant_to_eliminate){
			res->AddReactant(*mo_it);
			(*mo_it)->AddProduct(res, NULL);
		}
	}

	for (mu_it = multi_reactants.begin(); mu_it != multi_reactants.end(); mu_it++){
		if (reactant_to_eliminate == NULL || (*mu_it) != reactant_to_eliminate){
			res->AddReactant(*mu_it);
			(*mu_it)->AddProduct(res, NULL);
		}
	}

	for (mo_r_it = mono_products.begin(); mo_r_it != mono_products.end(); mo_r_it++){
		res->AddProduct(mo_r_it->first, NULL);
		mo_r_it->first->AddReactant(res);
	}

	for (mu_it = multi_products.begin(); mu_it != multi_products.end(); mu_it++){
		res->AddProduct(*mu_it, NULL);
		(*mu_it)->AddReactant(res);
	}

	return res;
}

//This function check if the reactants of the multi node calling the function are equals to the
//products of the multi node passed as argument
bool MultiNodeFunc::HasAsProductsSameReactants(MultiNodeFunc *p_multi){
	hash_map<int,int> found_nodes;
	hash_map<int,int>::iterator map_it;
	//list<MultiNodeFunc *>::iterator mu_p_it;
	//list<MultiNodeFunc *>::iterator mu_r_it;
	list<MoleculeNodeFunc *>::iterator mo_r_it;
	list<pair<MoleculeNodeFunc *, Expr *> >::iterator mo_p_it;

	bool ret_val = true;

	for (mo_r_it = mono_reactants.begin(); mo_r_it != mono_reactants.end(); mo_r_it++){
		if (found_nodes.find((*mo_r_it)->GetId()) == found_nodes.end()){
			found_nodes[(*mo_r_it)->GetId()] = 1;
		} else {
			found_nodes[(*mo_r_it)->GetId()] = found_nodes[(*mo_r_it)->GetId()] + 1;
		}
	}

	for (mo_p_it = p_multi->mono_products.begin(); mo_p_it != p_multi->mono_products.end() && ret_val; mo_p_it++){
		if (found_nodes.find((*mo_p_it).first->GetId()) == found_nodes.end()){
			ret_val = false;
		} else {
			if (found_nodes[(*mo_p_it).first->GetId()] == 0) {
				ret_val = false;
			}
			found_nodes[(*mo_p_it).first->GetId()] = found_nodes[(*mo_p_it).first->GetId()] - 1;
		}
	}

	for(map_it = found_nodes.begin(); map_it != found_nodes.end() && ret_val; map_it++){
		if((*map_it).second != 0){
			ret_val = false;
		}
	}


	return ret_val;
}

//This function is called to understand if a marked node can be deleted
//visited_node: nodes that have been already visited
//multi_nodes: couple of multi modes that will be used to bypass the molecular nodes to be eliminated
bool MoleculeNodeFunc::FindNodeToDelete(hash_map<MoleculeNodeFunc *, bool> *visited_node, set<pair<MultiNodeFunc*, MultiNodeFunc*> > *multi_nodes){
	list<MultiNodeFunc*>::iterator mu_p_it;
	list<MultiNodeFunc*>::iterator mu_r_it;
	list<MoleculeNodeFunc*>::iterator mo_r_it;
	MultiNodeFunc *multi_node;
	bool res;
	pair<MultiNodeFunc*, MultiNodeFunc*> p;
	
	if (marked == false){
		return false;
	} else {
		(*visited_node)[this] = true;
		for (mu_p_it = multi_products.begin(); mu_p_it != multi_products.end(); mu_p_it++){
			multi_node = *mu_p_it;
			for (mo_r_it = multi_node->GetMonoReactants()->begin(); mo_r_it != multi_node->GetMonoReactants()->end(); mo_r_it++){
				if (this != (*mo_r_it) && visited_node->find(*mo_r_it)==visited_node->end()){
					res = (*mo_r_it)->FindNodeToDelete(visited_node, multi_nodes);
					if (!res){
						return false;
					}
				}
			}
			for (mu_r_it = multi_reactants.begin(); mu_r_it != multi_reactants.end() && !multi_node->HasAsProductsSameReactants(*mu_r_it); mu_r_it++);
			p.first = (*mu_r_it);
			p.second = multi_node;
			multi_nodes->insert(p);
		}
	}

	is_deleted = true;
	return true;
}			

ReactionsGraphFunc::ReactionsGraphFunc(hash_map<reaction_key, Expr*, reaction_hash_compare> * reactions, hash_map<int, int>*molecules_in_run, list<EventReaction*> *p_new_delete_events, Environment *p_env){
	//Variables initialization
	n_multi_node = 1;
	env = p_env;

	new_delete_events = p_new_delete_events;

	//Iterators
	hash_map<int, ReactionNodeFunc *>::iterator n_it;
	hash_map<reaction_key, Expr*, reaction_hash_compare>::iterator r_it;
	hash_map<int, int>::iterator int_it;
	//list<double> rate_it;
	int i;

	//Number of reactants involved in the analysed reaction
	int n_reactants;
	//Numbers of products involved in the analysed reaction
	int n_products;

	//Temporary variables
	int node_id;
	const reaction_key *key;
	MultiNodeFunc *node;
	MoleculeNodeFunc *mol_node1 = NULL;
	
	list<EventReaction*>::iterator el_it;
	EventReaction *event;MoleculeNodeFunc *mol_node2 = NULL;

	//Insert in the graph all the entities used in state variable and functions
	set<int> entities;
	set<int>::iterator e_it;
	for(r_it = reactions->begin(); r_it != reactions->end(); r_it++){
		if ((*r_it).second != NULL){
			//reaction_key re = (*r_it).first;
			(*r_it).second->GetDependences(&entities);
		}
	}
	
	for(el_it = new_delete_events->begin(); el_it != new_delete_events->end(); el_it++){
		if((*el_it)->GetExpr(NULL) != NULL){
				(*el_it)->GetExpr(NULL)->GetDependences(&entities);
		}
	}

	for(e_it = entities.begin(); e_it != entities.end(); e_it++){
		nodes[(*e_it)*2] = new MoleculeNodeFunc((*e_it)*2);
	}

	//Cycle all the reactions
	for (r_it = reactions->begin(); r_it != reactions->end(); r_it++){
		//Store the key of the current reaction;
		key = &(*r_it).first;

		n_reactants = 0;
		n_products = 0;

		//Represents the Nil Node. Used to represent reaction like this one:
		//A -> Nil
		nodes[0] = new MoleculeNodeFunc(0);

		for (i = 0; i < 4; i++){
			//Even numbers are used for MoleculeNodeFunc objects. Odd numbers are used
			//for MultiNodeFunc objects. So we compute the id for the current node
			//exploiting the id used in the reaction and multiplying it by two.
			node_id = key->data[i] * 2;

			if (node_id != 0){

				if (i < 2){
					n_reactants++;
				} else {
					n_products++;
				}

				//Check if the node has already been inserted in the graph
				n_it = nodes.find(node_id);
				if (n_it == nodes.end()){
					//If the node has not been already inserted in the graph we
					//insert the node now
					nodes[node_id] = new MoleculeNodeFunc(node_id);
				}
			}
		}

		if (n_reactants > 1 || n_products > 1){
			//In this case we have more than one reactant or more than one
			//products, so we need to create a MultiNodeFunc
			node = new MultiNodeFunc(n_multi_node, (*reactions)[*key]);
			//Insert the MultiNodeFunc in the Graph
			nodes[n_multi_node] = node;


			for (i = 0; i < 4; i++) {
				if (key->data[i] != 0){
					mol_node1 = (MoleculeNodeFunc*)nodes[key->data[i]*2];
					if (i < 2){
						//Add the reactants as reactant of the MultiNodeFunc
						node->AddMonoReactant(mol_node1);
						//Add the MultiNodeFunc as product of to the reactants
						mol_node1->AddMultiProduct(node);
					} else {
						//Add the products as product of the MultiNodeFunc
						node->AddMonoProduct((MoleculeNodeFunc*)nodes[key->data[i]*2], NULL);
						//Add the MultiNodeFunc as reactant of the products
						mol_node1->AddMultiReactant(node);
					}
				}
			}
			n_multi_node+=2;
		} else if (n_products == 0) {
			//In this case we have no products: A --> Nil
			for (i = 0; i < 2; i++){
				if (key->data[i] != 0){
					//The reactant of the reaction
					mol_node1 = (MoleculeNodeFunc *)nodes[key->data[i]*2];
					//The product of the reactions: in this case it is the special
					//node "0". It represents the Nil node
					mol_node2 = (MoleculeNodeFunc*)nodes[0];
					//update the edges of the graph
					mol_node1->AddMonoProduct(mol_node2, (*reactions)[*key]);
					mol_node2->AddMonoReactant(mol_node1);
				}
			}
		} else {
			//In this case we have a monomolecular reaction: A --> B
			assert(n_reactants == 1 && n_products == 1);
			for (i = 0; i < 2; i++) {
				//Find the reactant
				if (key->data[i] != 0){
					mol_node1 = (MoleculeNodeFunc*)nodes[key->data[i]*2];
				}
			}
			for (; i < 4; i++){
				//Find the product
				if (key->data[i] != 0){
					mol_node2 = (MoleculeNodeFunc*)nodes[key->data[i]*2];
				}
			}
			//Update the edges of the graph
			mol_node1->AddMonoProduct(mol_node2, (*reactions)[*key]);
			mol_node2->AddMonoReactant(mol_node1);
		}
	}

	for(el_it = new_delete_events->begin(); el_it != new_delete_events->end(); el_it++){
		event = *el_it;
		node_id = event->GetFirstAffectedEntity()->GetId() * 2; 
		if (nodes.find(node_id) == nodes.end()){
			nodes[node_id] = new MoleculeNodeFunc(node_id);
		}
	}

	for (int_it = molecules_in_run->begin(); int_it != molecules_in_run->end(); int_it++){
		if(nodes.find((*int_it).first * 2)!=nodes.end()){
			((MoleculeNodeFunc *)nodes[(*int_it).first * 2])->InRun((*int_it).second);
		}
	}
}

void ReactionsGraphFunc::PrintDot(const char * file_name){
	hash_map<int, ReactionNodeFunc *>::iterator it;
	hash_map<int, bool> plotted_nodes;
	list<EventReaction*>::iterator el_it;
	int id;
	EventReaction *event;

	ofstream fout(file_name,ios::out);
	fout << "digraph simpleDotFile {" << endl;
     
	//fout << "Nil [label=Nil];" << endl;
	//fout << "Source [label=Source];" << endl;



	for (it = nodes.begin(); it != nodes.end(); it++){
		id = (*it).second->GetId();
		if (id != 0) {
			if (id % 2 == 0){
				if (id < 0) {
					id = abs(id);
					if ((*it).second->IsMarked()){
						fout << env->GetST()->GetMolNameFromID(id/2) << "[color=green,label=" << env->GetST()->GetMolNameFromID(id/2) << "];" << endl ;
					} else {
						fout << env->GetST()->GetMolNameFromID(id/2) << "[color=red,label=" << env->GetST()->GetMolNameFromID(id/2) << "];" << endl ;
					}
				} else {			
					if (plotted_nodes.find(id) == plotted_nodes.end()){ 
						if ((*it).second->IsMarked()){
						fout << env->GetST()->GetEntNameFromID(id/2) << "[color=green,label=" << env->GetST()->GetEntNameFromID(id/2) << "];" << endl ; 
						} else {
						fout << env->GetST()->GetEntNameFromID(id/2) << "[color=blue,label=" << env->GetST()->GetEntNameFromID(id/2) << "];" << endl ; 
						}
					}
				}
			}
		}
	}

	for(el_it = new_delete_events->begin(); el_it != new_delete_events->end(); el_it++){
		event = *el_it;
		id = event->GetFirstAffectedEntity()->GetId();
		switch(event->GetEventVerbType()){
			case VERB_NEW:
				fout << "Source ->" <<  env->GetST()->GetEntNameFromID(id);
				break;
			case VERB_DELETE:
				fout << env->GetST()->GetEntNameFromID(id) << " -> Nil" << endl;
				break;
			default:
					assert(false);
		}

		fout << "[color=darkgreen,label=\"";

		if (event->GetExpr(NULL) != NULL){
			event->GetExpr(NULL)->Print(&fout, PRINT_GRAPH);
		} else {
			fout << INF_RATE;
		}

		fout << " card: " << event->GetFirstCardinality() ;

		fout << "\"];" << endl;
	}


	for (it = nodes.begin(); it != nodes.end(); it++){
		(*it).second->PrintDot(env, fout);
	}
	
	fout << "}" << endl;
}

void MoleculeNodeFunc::Delete(){
	assert(!is_deleted);
	list<pair<MoleculeNodeFunc *, Expr *> >::iterator mo_it;
	list<MultiNodeFunc * >::iterator mu_it;

	for(mo_it = mono_products.begin(); mo_it != mono_products.end(); mo_it++){
		(*mo_it).first->RemoveReactant(this);
	}
	
	//TODO: controlla se funziona
	for(mu_it = multi_products.begin(); mu_it != multi_products.end(); mu_it++){
		(*mu_it)->RemoveReactant(this);
		(*mu_it)->Delete();
	}

	is_deleted = true;	
}


void MultiNodeFunc::Delete(){
	assert(!is_deleted);
	list<pair<MoleculeNodeFunc *, Expr *> >::iterator mo_r_it;
	list<MultiNodeFunc * >::iterator mu_it;
	list<MoleculeNodeFunc *>::iterator mo_it;


	for(mo_r_it = mono_products.begin(); mo_r_it != mono_products.end(); mo_r_it++){
		(*mo_r_it).first->RemoveReactant(this);
	}

	//TODO: controlla se funziona
	for(mu_it = multi_products.begin(); mu_it != multi_products.end(); mu_it++){
		(*mu_it)->RemoveReactant(this);
		(*mu_it)->Delete();
	}

	for(mu_it = multi_reactants.begin(); mu_it != multi_reactants.end(); mu_it++){
		//Non so se abbia senso pensare di andare a eliminare uno dei prodotti di una multi...
		//Nel caso questa situazione si verifichi bisgona pensarci bene.
		assert(false);
		(*mu_it)->RemoveProduct(this);
	}

	for(mo_it = mono_reactants.begin();  mo_it != mono_reactants.end(); mo_it++){
		(*mo_it)->RemoveProduct(this);
	}


	is_deleted = true;	
}

void MultiNodeFunc::NotRecursiveDelete(){
	assert(!is_deleted);
	list<pair<MoleculeNodeFunc *, Expr *> >::iterator mo_r_it;
	list<MultiNodeFunc * >::iterator mu_it;
	list<MoleculeNodeFunc *>::iterator mo_it;


	for(mo_r_it = mono_products.begin(); mo_r_it != mono_products.end(); mo_r_it++){
		(*mo_r_it).first->RemoveReactant(this);
	}

	//TODO: controlla se funziona
	for(mu_it = multi_products.begin(); mu_it != multi_products.end(); mu_it++){
		(*mu_it)->RemoveReactant(this);
	}

	for(mu_it = multi_reactants.begin(); mu_it != multi_reactants.end(); mu_it++){
		//Non so se abbia senso pensare di andare a eliminare uno dei prodotti di una multi...
		//Nel caso questa situazione si verifichi bisgona pensarci bene.
		assert(true);
		(*mu_it)->RemoveProduct(this);
	}

	for(mo_it = mono_reactants.begin();  mo_it != mono_reactants.end(); mo_it++){
		(*mo_it)->RemoveProduct(this);
	}


	is_deleted = true;	
}

void MultiNodeFunc::AddToInRun(int in_run){
	list<pair<MoleculeNodeFunc *, Expr *> >::iterator mono_product_it;
	list<MultiNodeFunc *>::iterator multi_product_it;

	int n_soons = (int)(multi_products.size() + mono_reactants.size());
   int step = in_run /n_soons;
   int entities_added = 0;

	for (mono_product_it = mono_products.begin(); mono_product_it != mono_products.end(); mono_product_it++){
		(*mono_product_it).first->AddToInRun(step);
      entities_added += step;
	}
	for (multi_product_it = multi_products.begin(); multi_product_it != multi_products.end(); multi_product_it++){
		(*multi_product_it)->AddToInRun(step);
      entities_added += step;
	}

   int diff = in_run - entities_added;
   assert (diff > 0);
   // add what's left to someone
   if (mono_products.size() > 0) 
      mono_products.front().first->AddToInRun(diff);
   else
      multi_products.front()->AddToInRun(diff);
}
	

int ReactionsGraphFunc::GetNewMultiId(){
	int res = n_multi_node;
	n_multi_node += 2;
	return res;
}

void ReactionsGraphFunc::Reduce(){
	hash_map<int, ReactionNodeFunc *>::iterator it;
	list<ReactionNodeFunc *>::iterator r_it;
	list<int> nodes_to_be_removed;
	list<int>::iterator i_it;

	list<ReactionNodeFunc*> to_be_checked;

	for (it = nodes.begin(); it != nodes.end(); it++) {
		if((*it).second->GetId() % 2 == 0 && !(*it).second->IsDeleted()){
			((MoleculeNodeFunc*)(*it).second)->CollapseFromMolToMol(&to_be_checked, this);
		}
	}

	for (it = nodes.begin(); it != nodes.end(); it++) {
		if((*it).second->GetId() % 2 == 0 && !(*it).second->IsDeleted()){
			((MoleculeNodeFunc*)(*it).second)->CollapseFromMolMulTo1Mol(&to_be_checked, this);
		}
	}

	for (it = nodes.begin(); it != nodes.end(); it++) {
		if((*it).second->GetId() % 2 == 0 && !(*it).second->IsDeleted()){
			((MoleculeNodeFunc*)(*it).second)->CollapseFrom1MolToMolMul(&to_be_checked, this);
		}
	}

	for (it = nodes.begin(); it != nodes.end(); it++) {
		if((*it).second->GetId() % 2 == 0 && !(*it).second->IsDeleted()){
			((MoleculeNodeFunc*)(*it).second)->CollapseInfReactions(&to_be_checked, this);
		}
	}

	for (it = nodes.begin(); it != nodes.end(); it++) {
		if((*it).second->GetId() % 2 != 0 && !(*it).second->IsDeleted()){
			assert(dynamic_cast<MultiNodeFunc*>((*it).second));
			((MultiNodeFunc*)(*it).second)->CollapseInfReactions(&to_be_checked, this);
		}
	}

	
	for (it = nodes.begin(); it != nodes.end(); it++) {
		if((*it).second->GetId() % 2 == 0 && !(*it).second->IsDeleted()){
			assert(dynamic_cast<MoleculeNodeFunc*>((*it).second));
			((MoleculeNodeFunc*)(*it).second)->CollapseInvolvedOnlyInInfBim(&to_be_checked, this);
		}
	}


	hash_map<MoleculeNodeFunc *, bool> *visited_nodes = new hash_map<MoleculeNodeFunc *, bool>;
	set<pair<MultiNodeFunc *,MultiNodeFunc *> > *pair_multi_nodes = new set<pair<MultiNodeFunc *,MultiNodeFunc *> >;
	set<pair<MultiNodeFunc *,MultiNodeFunc *> >::iterator set_it;

	//TODO: Dopo questa cancellazione credo che possano essere fatte ulteriori eliminazioni, 
	//queste eliminazioni posssono essere fatte dalle funzoini già implementate ma esse devono essere
	//richiamate sui nodi finora non eliminati (questi nodi sono quelli modificati nelle righe successive) 
	for (it = nodes.begin(); it != nodes.end(); it++) {
		if((*it).second->GetId() % 2 == 0 && !(*it).second->IsDeleted()){
			assert(dynamic_cast<MoleculeNodeFunc*>((*it).second));
			visited_nodes->clear();
			((MoleculeNodeFunc*)(*it).second)->FindNodeToDelete(visited_nodes, pair_multi_nodes);
		}
	}

	for (set_it = pair_multi_nodes->begin(); set_it != pair_multi_nodes->end(); set_it++){
		Bypass((*set_it).first, (*set_it).second);
	}

	delete visited_nodes;
	delete pair_multi_nodes;

	//Find nodes that have to be erased
	for (it = nodes.begin(); it != nodes.end(); it++) {
		if((*it).second->IsDeleted()){
			nodes_to_be_removed.push_back((*it).first);
		}
	}

	for (i_it = nodes_to_be_removed.begin(); i_it != nodes_to_be_removed.end(); i_it++){
		nodes.erase(*i_it);
	}
}


SBMLDocument_t * ReactionsGraphFunc::BuildSBML(int level, int version){
	hash_map<int, ReactionNodeFunc *>::iterator nodes_it;
	Compartment *compartment;
	list<EventReaction*>::iterator element_it;

	#if (LIBSBML_VERSION >= 30101)
		SBMLDocument_t *document = SBMLDocument_createWithLevelAndVersion(level,version);
	#else
		SBMLDocument_t *document = SBMLDocument_createWith(level,version);
	#endif

	Model_t *model = SBMLDocument_createModel(document);

	Model_setName(model, "SBMLmodel");

   compartment = Model_createCompartment(model);
	Compartment_setId(compartment,"compartment");
	Compartment_setName(compartment,"compartment");

	Species_t *specie = Model_createSpecies(model);
	Species_setId(specie, "Source");
	Species_setName(specie, "Source");
	Species_setCompartment(specie, "compartment");
	Species_setInitialAmount(specie, 1);
	Species_setBoundaryCondition(specie, false);

	for (nodes_it = nodes.begin(); nodes_it != nodes.end(); nodes_it++){
		(*nodes_it).second->InsertSBMLSpecie(model, env);
	}
	
	int reactions_counter = 0, constants_counter = 0;

	for (element_it = new_delete_events->begin(); element_it != new_delete_events->end(); element_it++){
		InsertSBMLReactionFromNewDeleteEvent(*element_it, model, reactions_counter, constants_counter);
	}

	for (nodes_it = nodes.begin(); nodes_it != nodes.end(); nodes_it++){
		(*nodes_it).second->InsertSBMLReactions(model, env, reactions_counter, constants_counter);
	}

	return document;
}

void ReactionsGraphFunc::InsertSBMLReactionFromNewDeleteEvent(EventReaction *ev, Model_t *model, int &reactions_counter, int &constants_counter){
	vector<Entity*> originating_entities;
   vector<Entity*> affected_entities;
	vector<Entity*>::iterator or_it;
   vector<Entity*>::iterator af_it;

	originating_entities = ev->GetSourceEntities();
	affected_entities = ev->GetTargetEntities();

	stringstream stream;
	string reactant;

	int id;
	Reaction_t *reaction;
	KineticLaw_t *kinetic_law;
	Parameter_t *parameter;
	SpeciesReference_t *reference1;

	reaction = Model_createReaction(model);
	stream << "R" << reactions_counter++;
	Reaction_setId(reaction, stream.str().c_str());
	stream.str("");
	Reaction_setReversible(reaction, false);

	kinetic_law = Model_createKineticLaw(model);
	parameter = Model_createKineticLawParameter(model);

	stream << "c" << constants_counter++;
	Parameter_setName(parameter, stream.str().c_str());
	Parameter_setId(parameter, stream.str().c_str());
	Parameter_setValue(parameter, 0);
	stream.str("");

	if (ev->GetExpr(NULL)	!= NULL){
		ev->GetExpr(NULL)->Print(&stream, PRINT_SBML);
	} else {
		stream << INF_RATE;
	}

	KineticLaw_setFormula(kinetic_law, stream.str().c_str());
	stream.str("");


	if (ev->GetEventVerbType()	== VERB_NEW){
		reference1 = Model_createReactant(model);
		SpeciesReference_initDefaults(reference1);
		SpeciesReference_setSpecies(reference1, "Source");
		reference1 = Model_createProduct(model);
		SpeciesReference_initDefaults(reference1);
		id = (ev->GetFirstAffectedEntity())->GetId();
		SpeciesReference_setSpecies(reference1, env->GetST()->GetEntNameFromID(id).c_str());
		SpeciesReference_setStoichiometry(reference1, ev->GetFirstCardinality());
		reference1 = Model_createProduct(model);
		SpeciesReference_initDefaults(reference1);
		SpeciesReference_setSpecies(reference1, "Source");
	} else {
		reference1 = Model_createReactant(model);
		SpeciesReference_initDefaults(reference1);
		id = (ev->GetFirstAffectedEntity())->GetId();
		SpeciesReference_setSpecies(reference1, env->GetST()->GetEntNameFromID(id).c_str());
		SpeciesReference_setStoichiometry(reference1, ev->GetFirstCardinality());
		reference1 = Model_createProduct(model);
		SpeciesReference_initDefaults(reference1);
		SpeciesReference_setSpecies(reference1, "Nil");
	}
}

void ReactionsGraphFunc::Bypass(MultiNodeFunc *mu_reactant, MultiNodeFunc *mu_product){
	if (mu_reactant->GetMonoReactants()->size() == 1 && mu_product->GetMonoProducts()->size()){
		MoleculeNodeFunc *reactant = *(mu_reactant->GetMonoReactants()->begin());
		MoleculeNodeFunc *product = (*(mu_product->GetMonoProducts()->begin())).first;
		Expr *expr = mu_reactant->GetExpr();
		
		reactant->RemoveProduct(mu_reactant);
		product->RemoveProduct(mu_product);
		reactant->AddMonoProduct(product, expr);
		product->AddMonoReactant(reactant);

		mu_reactant->SetDeleted(true);
		mu_product->SetDeleted(true);
	} else {
		//non testato
		list<pair <MoleculeNodeFunc *, Expr *> >::iterator mo_p_it;
		mu_reactant->GetMonoProducts()->clear();
		for (mo_p_it = mu_product->GetMonoProducts()->begin(); mo_p_it !=  mu_product->GetMonoProducts()->end(); mo_p_it++){
			(*mo_p_it).first->RemoveOneReactant(mu_product);
			mu_reactant->AddMonoProduct((*mo_p_it).first, NULL);
			(*mo_p_it).first->AddMultiReactant(mu_reactant);
		}
		(mu_product)->SetDeleted(true);
	}
}
