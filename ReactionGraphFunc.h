#ifndef REACTIONGRAPH_H_INCLUDED
#define REACTIONGRAPH_H_INCLUDED

#include<vector>
#include<list>
#include"hash.h"
#include"Environment.h"
#include"Expr.h"
#include"Define.h"

using namespace std;

class MoleculeNodeFunc;
class MultiNodeFunc;
class ReactionsGraphFunc;

//Represents a General node that composes the Reactions Graph
class ReactionNodeFunc {
	protected:
		//The id is unique for each node of the graph
		int id;
		bool is_deleted;

		//variable use to mark some nodes in one during the reduction of the graph
		bool marked;

		//List of the molecules that are involved as reactant in reactions that
		//involve this object as product. This reactions are monomolecular reactions, indeed
		//reactions of this kind: A --> B
		list<MoleculeNodeFunc *> mono_reactants;
		//List of the molecules that are involved as reactant in reactions that
		//involve this object as product. This reactions are multi molecular reactions, indeed
		//reactions of this kind: A --> B + C or reactions of this kind A + B --> C
		list<MultiNodeFunc *>  multi_reactants;
		//List of the molecules that are involved as product in reactions that
		//involve this object as reactant. This reactions are monomolecular reactions
		list<pair<MoleculeNodeFunc *, Expr *> > mono_products;
		//List of the molecules that are involved as product in reactions that
		//involve this object as reactant. This reactions are multi molecular reactions
		list<MultiNodeFunc * > multi_products;
	public:
		//Creates a node with the id specified as argument
		ReactionNodeFunc(int p_id);
		//Destroys a Reaction Node
		virtual  ~ReactionNodeFunc();

		virtual bool Reduct() = 0;

		virtual void PrintDot(Environment * e, ofstream &fout) = 0;
		void UpdateNodesToBeVisited(list<ReactionNodeFunc *> *nodesToBeVisited, hash_map<ReactionNodeFunc *, bool>  *visitedNodes);
		void EliminateMonoReaction(MoleculeNodeFunc* product);
		void AddMonoReactant(MoleculeNodeFunc *node){ mono_reactants.push_back(node); }
		void AddMonoProduct(MoleculeNodeFunc *node, Expr *expr){ 
			mono_products.push_back(pair<MoleculeNodeFunc	*,	Expr *>(node, expr)); 
		}
		void AddMultiReactant(MultiNodeFunc *node){ multi_reactants.push_back(node); }
		void AddMultiProduct(MultiNodeFunc *node){ 
			multi_products.push_back(node); 
		}

		virtual bool IsInRun(){ return false; }
		virtual void Delete() = 0;
		bool IsDeleted(){ return is_deleted; }

		void RemoveReactant(ReactionNodeFunc *node);
		void RemoveOneReactant(ReactionNodeFunc *node);
		void AddReactant(ReactionNodeFunc *node);
		void RemoveProduct(ReactionNodeFunc *node);
		void RemoveOneProduct(ReactionNodeFunc *node);
		void AddProduct(ReactionNodeFunc *node,  Expr *expr);
		void SetDeleted(bool val);
		void DeleteUnrichableNodes();
		virtual void CollapseInfReactions(list<ReactionNodeFunc*> *nodes_to_be_checked, ReactionsGraphFunc *grph) = 0;
		size_t GetNumberOfReactants() { return multi_reactants.size() + mono_reactants.size(); }
		size_t GetNumberOfMultiReactants() { return multi_reactants.size(); }
		size_t GetNumberOfMultiProducts() { return multi_products.size(); }

		int GetId(){ return id ; }
		virtual void InsertSBMLSpecie(Model_t *model, Environment *env){};
		virtual void InsertSBMLReactions(Model_t *model, Environment *env, int &constants_counter, int &reactions_counter) = 0;
		bool IsMarked(){ return marked; }
};

//Represents a molecule in The Reactions Graph
class MoleculeNodeFunc : public ReactionNodeFunc {
	private:
		//It is true if the Molecule represented by the node is present in the
		//run directive of the analysed model. In other words it is true if the
		//molecule is note just defined but also instantiated with the run
		//directive
		int present_in_run;
	public:
		MoleculeNodeFunc(int id);
		bool Reduct() {return true; } ;
		void PrintDot(Environment * e, ofstream &fout);
		void InRun(int val){ present_in_run = val; }
		bool IsInRun(){ return (present_in_run == 0 ? false : true); }
		int GetInRun(){ return present_in_run; }
		void AddToInRun(int in_run) {present_in_run += in_run; }
		 
		void CollapseInfReactions(list<ReactionNodeFunc*> *nodes, ReactionsGraphFunc *graph);
		void CollapseFromMolToMol(list<ReactionNodeFunc*> *nodes_to_be_checked, ReactionsGraphFunc *graph);
		void CollapseFromMolMulTo1Mol(list<ReactionNodeFunc*> *nodes_to_be_checked, ReactionsGraphFunc *graph);
		void CollapseFrom1MolToMolMul(list<ReactionNodeFunc*> *nodes_to_be_checked, ReactionsGraphFunc *graph);
		void CollapseInvolvedOnlyInInfBim(list<ReactionNodeFunc*> *nodes_to_be_checked, ReactionsGraphFunc *graph);
		void CheckDoubleInfArrows(list<MoleculeNodeFunc *> * node_to_be_deleted);
		void CheckDoubleInfArrows();
		void Delete();
		Expr *GetExpr(MoleculeNodeFunc *);
		void CheckForInconsistency(ReactionsGraphFunc *graph);
		void HasProduct(ReactionNodeFunc *);
		void HasReactant(ReactionNodeFunc *);
		SBMLDocument *BuildSBML();
		void InsertSBMLSpecie(Model_t *model, Environment *env);
		void InsertSBMLReactions(Model_t *model, Environment *env, int &constant_counter, int &reactions_counter);
		bool FindNodeToDelete(hash_map<MoleculeNodeFunc *, bool> *visited_node, set<pair<MultiNodeFunc*, MultiNodeFunc*> > *multi_nodes);
};

//MultiNodeFunc objects are used in the Reactions Graph to describe reactions that
//involves more than one reactant or more than one product.
//Example:
//Suppose that Mol represents nodes of type MoleculeNodeFunc and Mul represents
//node of type MultiNodeFunc;
//
//////////////////////////////////////
//1) This reaction						//		
//												//
//			A --> B							//
//												//
//	  is represented in this way:		//
//												//
//			Mol(A) --> Mol(B)				//
//												//
//2) This reaction						//
//												//
//		A -- > C+B							//
//												//
//		is represented in this way;	//
//												//
//							/->Mol(C)		//
//						  /					//
//		Mol(A) --> Mul						//
//						  \					//
//						   \->Mol(B)		//
//												//
//												//
//////////////////////////////////////
//
class MultiNodeFunc : public ReactionNodeFunc{
	private:
		//rate of the reaction that the MultiNodeFunc represents
		Expr *expr;
	public:
		MultiNodeFunc(int id, Expr *p_expr);
		bool Reduct() {return true; } ;
		void PrintDot(Environment * e, ofstream &fout);
		void ReduceRateLists();
		int IsReachable(hash_map<ReactionNodeFunc *, int> &visited_nodes, int index);
		Expr *GetExpr(){ return expr; }
		void Delete();
		void NotRecursiveDelete();
		void CollapseInfReactions(list<ReactionNodeFunc*> *nodes, ReactionsGraphFunc *grph);
		MultiNodeFunc *CloneWithoutOneProduct(int new_id, ReactionNodeFunc *product_to_eliminate, Expr *expr);
		MultiNodeFunc *CloneWithoutOneReactant(int new_id, ReactionNodeFunc *reactant_to_eliminate, Expr *rates_list);
		SBMLDocument *BuildSBML();
		void InsertSBMLReactions(Model_t *model, Environment *env, int &constants_counter, int &reactions_counter);
		void SetRates(Expr *new_expr){
			delete expr;
			expr = new_expr;
		}
		void AddToInRun(int in_run);
		bool HasAsProductsSameReactants(MultiNodeFunc *multi);
		list<MoleculeNodeFunc *> * GetMonoReactants(){ return &mono_reactants; }
		list<pair<MoleculeNodeFunc *, Expr*> > * GetMonoProducts(){ return &mono_products; }
};

//Represent all the reactions that can be performed in the model
class ReactionsGraphFunc {
	private:
		Environment *env;
		//Number of nodes of type MultiNodeFunc that has been inserted since the
		//creation of the graph
		int n_multi_node;

		list<EventReaction*> *new_delete_events;

		//Contains all the nodes that compose the graph
		hash_map<int, ReactionNodeFunc *> nodes;
	public:
		//Initialize a graph that represents the reaction passed as argument
		ReactionsGraphFunc(hash_map<reaction_key, Expr*, reaction_hash_compare> * reactions, hash_map<int, int>*molecules_in_run, list<EventReaction*> *delete_new_events, Environment *p_env);
		void PrintDot(const char * file_name);
		void Reduce();
		int GetNewMultiId();
		void AddNode(ReactionNodeFunc *new_node) { nodes[new_node->GetId()] = new_node; }
		void CheckForInconsistency();
		SBMLDocument *BuildSBML(int level, int version);
		void InsertSBMLReactionFromNewDeleteEvent(EventReaction *ev, Model_t *model, int &reaction_counter, int &constant_counter);
		void Bypass(MultiNodeFunc *reactant, MultiNodeFunc *product);
};
		

#endif
