
#ifndef SINGLETON_H_INCLUDED
#define SINGLETON_H_INCLUDED

#include <assert.h>
#include <cstddef>

//#pragma warning(disable: 4311)

template <typename T> 
class Singleton
{
private:
   static T* ms_Singleton;

public:
   Singleton( void )
   {
      assert( !ms_Singleton );
      ptrdiff_t offset = (ptrdiff_t)(T*)1 - (ptrdiff_t)(Singleton <T>*)(T*)1;
      ms_Singleton = (T*)((ptrdiff_t)this + offset);
   }

   virtual ~Singleton( void )
   {  
      assert( ms_Singleton );  
      ms_Singleton = NULL;  
   }

   static T& GetInstance( void )
   {  
      assert( ms_Singleton );  
      return ( *ms_Singleton );  
   }

   static T* GetSingletonPtr( void )
   {  
      return ( ms_Singleton );  
   }
};

template <typename T> T* Singleton <T>::ms_Singleton = NULL;

#endif //SINGLETON_H_INCLUDED
