
#include "StateVar.h"
#include "Entity.h"

double StateVar::GetValue() 
{ 
   double retval;

   if (getting_value)
   {
      //attempt to recursion, break it
      retval = value;
   }
   else
   {
      getting_value = true;
   
      if (explicitly_computed) 
      {
         explicitly_computed = false;
         retval = value;      
      }
      else
      {
         if (type ==  STATEVAR_CONTINUOUS)
            retval = value;
         else
         {
            retval = expression->Eval();
            value = retval;
         }
      }
   }   
   getting_value = false;   
   return retval;
}

double StateVar::SetValue(double newValue) 
{ 
   explicitly_computed = true;
   double oldValue = value; 
   value = newValue; 
   return oldValue; 
}

void StateVar::UpdateValue()
{
   if (type ==  STATEVAR_CONTINUOUS)
   {
      getting_value = true;      
      double eval_res = expression->Eval();
      value = value + (eval_res * delta_t);  
      getting_value = false;   
   }
}

bool StateVar::AddEntity(Entity* entity)
{
   //find if already in list
   std::vector<Entity*>::iterator it;
   for (it = entity_list.begin(); it != entity_list.end(); ++it)
   {
      if ((**it).GetId() == entity->GetId())
         return false;
   }

   entity_list.push_back(entity);
   return true;
}

