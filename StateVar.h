

#ifndef STATEFUN_H_INCLUDED
#define STATEFUN_H_INCLUDED

#include <cstdlib>
#include <string>
#include <list>
#include <vector>
#include <string>
#include "Define.h"
#include "Expr.h"

//////////////////////////////////////////////////////
// STATEVAR CLASS
// This class represents an state variable whose value
// is a function of the system
//////////////////////////////////////////////////////

class StateVar
{
private:
   int id;
   Expr* expression;

   std::vector<Entity*> entity_list;
   std::vector<StateVar*> statevar_list;
   StateVarType type;

   double value;
   double delta_t;
   bool explicitly_computed;
   bool getting_value;

public:
   StateVar(int p_id, Expr* p_expression, StateVarType p_type, double p_init_val) 
	{ 
      id = p_id;
      type = p_type;
      expression = p_expression;
      explicitly_computed = false;
      getting_value = false;
      value = p_init_val;
   }	

   StateVar(int p_id, double p_delta_t, Expr* p_expression, StateVarType p_type, double p_init_val) 
	{ 
      id = p_id;
      type = p_type;
      expression = p_expression;
      explicitly_computed = false;
      delta_t = p_delta_t;
      getting_value = false;
      value = p_init_val;
   }	

   bool CheckExpression()
   {
      return expression->Check();
   }

   bool Init()
   {      
      if (!CheckExpression())
         return false;
      std::set<Entity*> entities;
      std::set<StateVar*> variables;
      expression->GetEntitiesAndVariables(entities, variables);
      entity_list.insert(entity_list.end(), entities.begin(), entities.end());
      statevar_list.insert(statevar_list.end(), variables.begin(), variables.end());
      return true;
   }

	
   virtual ~StateVar() { } 

   double GetValue();
   double SetValue(double newValue);
   void UpdateValue();
   double ReadValue() 
   {
		return value;
   }

   int GetId() { return id; }
   StateVarType GetType() { return type; }
   double GetDeltaT() { assert(type == STATEVAR_CONTINUOUS); return delta_t; }

   // Accessors for this class
   std::vector<Entity*>& GetEntities() { return entity_list; }
   std::vector<StateVar*>& GetStatevars() { return statevar_list; }

   bool AddEntity(Entity* entity);

   template <class Iter>
   bool AddEntities(Iter first, Iter last)
   {
	   bool ret = true;
	   Iter it;
	   for (it = first; it != last; ++it)
	   {
		   ret &= AddEntity(*it);
	   }
	   return ret; 	  
   }

	Expr *GetExpr() {return expression; }
};


class StateVarList : public Element
{
public:
   std::vector<StateVar*> stateVarList;

public:
   StateVarList()
      : Element(NULL, STATEVAR) { }
   
   bool Insert(StateVar* var)
   {
      std::vector<StateVar*>::iterator it;
      for(it = stateVarList.begin(); it != stateVarList.end(); ++it)
      {
         if ((*it)->GetId() == var->GetId())
            return false;            
      }
      stateVarList.push_back(var);
      return true;
   }

   //
   // Element implementation (do nothing)
   double Time(void) { return 0.0; }
   bool Is(Element* element) { return (this == element); }
	void GetIterator(pair<Entity *, Entity *> *ent_pair, list<Complex *> *comp, pair<Iterator_Interface *, Iterator_Interface *> *it_pair){
		//This function should not be called on this kind of Element
		assert(0);	
	}
	Iterator_Interface * GetIterator(Entity *e1, Entity *e2, list<Complex *> *c1, list<Complex *> *c2, TwoIteratorKind iterator_kind){ 
		assert(false); 
		return NULL; 
	}


	Expr* GetExpr(Iterator_Interface *c_iter){
		//This function should not be called on this kind of Element
		assert(false);
		return NULL;
	}

	bool IsOnlyMono(int reactants[2]){
		//This function should not be called on this kind of Element
		assert(0);
		return false;
	}
};


#endif //STATEFUN_H_INCLUDED

