#include <iostream>
#include <cassert>
#include <sstream>
#include <set>

#include "Symbol_Table.h"
#include "Environment.h"
#include "Complex_Map.h"
#include "Complex_Rel.h"
#include "Event.h"

using namespace std;

//////////////////////////////////////////////////////
// ST_NODE METHODS DEFINITION
//////////////////////////////////////////////////////

//
ST_Node::ST_Node(EntityType p_category, PTNode *p_ref) {
	category = p_category;
	ref = p_ref;
}

//
PTNode *ST_Node::GetRef() {
	return ref;
}

//
EntityType ST_Node::GetCategory() {
	return category;
}

//
bool ST_Node::GenerateIC(SymbolTable *st) {
   return true;
}


//////////////////////////////////////////////////////
// ST_PI_PROC METHODS DEFINITION
//////////////////////////////////////////////////////

//
ST_Pi_Proc::ST_Pi_Proc(const string& p_id, EntityType p_category, PTNode *p_ref):ST_Node(p_category,p_ref) {
	id = p_id;
   pproc = NULL;
}

ST_Pi_Proc::~ST_Pi_Proc() {
	delete pproc;
}

//
const string& ST_Pi_Proc::GetIdentifier() {
	return id;
}

//
list<Free_Name> *ST_Pi_Proc::GetFnList() {
	return &fn;
}

//
bool ST_Pi_Proc::GenerateIC(SymbolTable *st) 
{
   if (pproc == NULL)
	   return this->GetRef()->GenerateIC(&pproc,st);
   else
      return true;
}

//
PP_Node *ST_Pi_Proc::GetIC() {
	return pproc;
}

//
void ST_Pi_Proc::PrintFN() {
	list<Free_Name>::iterator i;
	for(i=fn.begin(); i!=fn.end(); i++) 
		cout << " [" << (*i).name << "]";
	cout <<endl;
}

//////////////////////////////////////////////////////
// ST_Pi_Sequence METHODS DEFINITION
//////////////////////////////////////////////////////

//
ST_Pi_Sequence::ST_Pi_Sequence(const string& p_id, EntityType p_category, PTNode *p_ref):ST_Node(p_category,p_ref) {
	id = p_id;
   pproc = NULL;
}

ST_Pi_Sequence::~ST_Pi_Sequence() {
	delete pproc;
}

//
const string& ST_Pi_Sequence::GetIdentifier() {
	return id;
}

//
list<Free_Name>* ST_Pi_Sequence::GetFnList() {
	return &fn;
}

//
bool ST_Pi_Sequence::GenerateIC(SymbolTable *st) 
{
   if (pproc == NULL)
	   return this->GetRef()->GenerateIC(&pproc,st);
   else
      return true;
}

//
PP_Node* ST_Pi_Sequence::GetIC() {
	return pproc;
}

//
void ST_Pi_Sequence::PrintFN() {
	list<Free_Name>::iterator i;
	for(i=fn.begin(); i!=fn.end(); i++) 
		cout << " [" << (*i).name << "]";
	cout <<endl;
}

//////////////////////////////////////////////////////
// ST_BETA_PROC METHODS DEFINITION
//////////////////////////////////////////////////////

//
ST_Beta_Proc::ST_Beta_Proc(const string& p_id, EntityType p_category, PTNode *p_ref):ST_Node(p_category,p_ref) {
	id = p_id;
	counter = 0;
	pproc = NULL;
	binder_list = new list<BBinders *>();
   initialized = false;
   generated = false;
}

//
ST_Beta_Proc::~ST_Beta_Proc() {
	BBinders *tmp_b;
	if(binder_list!=NULL) {
		while(!binder_list->empty()) {
			tmp_b = binder_list->front();
			binder_list->pop_front();
			delete(tmp_b);
		}
		delete(binder_list);
	}
	if (pproc!=NULL) delete(pproc);
}

//
const string& ST_Beta_Proc::GetIdentifier() {
	return id;
}

//
void ST_Beta_Proc::AddCounter(int n) {
	counter+=n;
}

//
list<Free_Name> *ST_Beta_Proc::GetFnList() {
	return &fn;
}

//
bool ST_Beta_Proc::InsertBinder(BBinders *elem) {
	list<BBinders *>::iterator i;
    for(i=binder_list->begin(); i!=binder_list->end(); i++)
		if ( ((*i)->GetSubject() == elem->GetSubject()) || ((*i)->GetType() == elem->GetType()) )
				return false;
	binder_list->push_front(elem);
	return true;
}

BBinders* ST_Beta_Proc::FindBinderByName(const std::string& binder_name)
{
   list<BBinders *>::iterator i;
    for(i=binder_list->begin(); i!=binder_list->end(); i++)
		if ((*i)->GetSubject() == binder_name)
         return *i;

    return NULL;
}




//
bool ST_Beta_Proc::GenerateIC(SymbolTable *st) 
{
	if (!generated)
	{
		generated = true;
		pproc = NULL;
		return this->GetRef()->GenerateIC(&pproc,st);
	}
   else
      return true;
}

//
PP_Node *ST_Beta_Proc::GetIC() {
	if (!initialized)
		return pproc;
	return myEntity->_GetPProc();
}


void ST_Beta_Proc::SetIC(PP_Node* ppnode) 
 {       
      generated = true;
      
      // discard old node
      delete pproc;

      pproc = ppnode; 
   }

//
std::list<BBinders*>* ST_Beta_Proc::GetBinderList()
{
	if (!initialized)
		return binder_list;
	return myEntity->_GetBBList();
}


bool ST_Beta_Proc::IsInitialized()
{
   return initialized;
}

bool ST_Beta_Proc::IsGenerated()
{
   return generated;
}

//
bool ST_Beta_Proc::InitAM1(SymbolTable* st) 
{
	Sub *n_sub;
	if (IsInitialized())
		return true;
	else
		initialized = true;

	string bbn;
	int index;
	list<BBinders *>::iterator j;
	binder_list->sort(BB_Predicate);
	index = 0;
	for(j=binder_list->begin();j!=binder_list->end();j++) 
   {
		bbn = BIND_N + Utility::i2s(index++);
		pproc->Renaming((*j)->GetSubject(),bbn);
		n_sub = new Sub((*j)->GetSubject(),bbn);
		mySubs.push_back(n_sub);
		(*j)->SetSubject(bbn);
	}	
	pproc->Compress();
	EliminateNil(&pproc);
	pproc->NameLess(NAMELESS);
	Implosion(&pproc);
	Order(&pproc);
	int e_id = st->GetEntIDFromName(id);
	// save pointer
	myEntity = st->AddEntity(binder_list, pproc, counter, e_id);
	
	// se e' inizializzato questo vengono impostati a NULL
	// Se infatti l'entita' che creo e' nuova allora non devo fare la delete
	// dei due perche' vengono usati per creare la nuova entita'
	// altrimenti non serve fare la delete perche' viene fatta direttamente
	// dalla funzione AddEntity

   //TODO: delete them!!
	pproc = NULL;
	binder_list = NULL;
	
	return true;
}

bool ST_Beta_Proc::InitAM2(SymbolTable* st) 
{
	binder_list = NULL;
	pproc = NULL;

	return true;
}

//
void ST_Beta_Proc::PrintFN() {
	list<Free_Name>::iterator i;
	for(i=fn.begin(); i!=fn.end(); i++) 
		cout << " [" << (*i).name << "]";
	cout <<endl;
}


//////////////////////////////////////////////////////
// Symbol Table Pproc Template definition entry
//////////////////////////////////////////////////////

//
ST_Pproc_Template::ST_Pproc_Template(const string& p_id, EntityType p_category, PTNode *p_ref)
   : ST_Node(p_category,p_ref) 
{
   id = p_id;
   pproc = NULL;
}

//
ST_Pproc_Template::~ST_Pproc_Template() 
{
   //TODO!! recursively delete pproc (if != NULL)
}

bool ST_Pproc_Template::GenerateIC(SymbolTable *st) 
{
   if (pproc == NULL)
   {
      st->PushTempAmbient(&TemplateAmbient);
      bool res = this->GetRef()->GenerateIC(&pproc,st);
      st->PopTempAmbient();
      return res;
   }
   else
      return true;
}

//////////////////////////////////////////////////////
//
// Symbol table Molecule definition entry
//
//////////////////////////////////////////////////////

ST_Mol_Def::ST_Mol_Def(const string& p_id, 
                       EntityType p_category, 
                       PTNode *p_ref)
                       : ST_Node(p_category,p_ref), id(p_id) { 
	counter = 0;
}

ST_Mol_Def::~ST_Mol_Def()
{
}

const std::string& ST_Mol_Def::GetIdentifier()
{
   return id;
}

bool ST_Mol_Def::InitAM1(SymbolTable* st) 
{
   //hash_map or vector
   hash_map<string, pair<CG_Node*,ST_Beta_Proc*> > molnode_map;
   pair<CG_Node*,ST_Beta_Proc*> pair;
   std::list<BBinders*> *binder_list;

   //Build the complex graph
   Complex_Graph* comp_graph = new Complex_Graph(st->GetEnv());
   
   //For each beta process involved,
   std::vector< triple<string, ST_Beta_Proc*, std::set<string>*> >::iterator def_it;
   for (def_it = defined_nodes.begin(); def_it != defined_nodes.end(); ++def_it)
   {
      //Initalize the corresponding ST for the beta
      //processes involved

      if (!def_it->second()->IsInitialized())
      {
         //this will order and rename the binders,
         //reduce the trees, etc.
         def_it->second()->InitAM1(st);
      }
      
      // copy it, 
	  PP_Node* pproc = def_it->second()->GetIC()->GetCopy();

      // copy the binder list, 
	  binder_list = new std::list<BBinders*>();
      set<string>* bound_binders = def_it->third();
      std::list<BBinders*>::iterator bb_it;

	  string sub_subject;

	  for (bb_it = def_it->second()->GetBinderList()->begin(); bb_it != def_it->second()->GetBinderList()->end(); ++bb_it)
      {
         // copy
         BBinders* new_binder = new BBinders(*(*bb_it));
         binder_list->push_back(new_binder);

		 sub_subject = def_it->second()->GetSubOld(new_binder->GetSubject());

         // bind the necessary binders,
         if (bound_binders->find(sub_subject) != bound_binders->end())
         {
            new_binder->SetState(STATE_BOUND);
		 }     
	  }

	  // add it to the ambient      

	  //ONLY process not referring to others
	  const std::string& current_node_id = def_it->first();

	  //int e_id = st->GetIDFromName(current_node_id);
	  Entity* new_bound_entity = st->AddEntity(binder_list, pproc, counter, EMPTY_ID);

	  CG_Node* node = new CG_Node(comp_graph, new_bound_entity);

	  pair.first = node;
	  pair.second = def_it->second();
	  molnode_map[current_node_id] = pair;         

	  //now, scroll the list of referring names
	  std::vector<std::string>& referenced_nodes = referenced_nodes_map[current_node_id];
	  std::vector<std::string>::iterator ref_it;
	  for (ref_it = referenced_nodes.begin(); ref_it != referenced_nodes.end(); ++ref_it)
	  {
		  node = new CG_Node(comp_graph, new_bound_entity);
		  pair.first = node;
		  pair.second = def_it->second();
		  molnode_map[*ref_it] = pair;         
	  }
   }
   
   //and comp_graph it to the ambient

   //first, convert edge list from name-based to pointer-based
   list<ST_Mol_Edge>::iterator edge_it;
   string sub_subject1;
   string sub_subject2;
   for (edge_it = defined_edges.begin(); edge_it != defined_edges.end(); ++edge_it)
   {
	   CG_Node* node1 = molnode_map[edge_it->node1].first;
	   CG_Node* node2 = molnode_map[edge_it->node2].first;

	   if ( node1 == NULL || node2 == NULL ) {
		   Error_Manager::PrintError(0," the definition of the nodes in molecule " + id + " is not correct.");
		   return false;
	   }

	   sub_subject1 = molnode_map[edge_it->node1].second->GetSubNew(edge_it->binder1);
	   sub_subject2 = molnode_map[edge_it->node2].second->GetSubNew(edge_it->binder2);

	   BBinders* binder1 = molnode_map[edge_it->node1].first->GetRef()->GetBB(sub_subject1);
	   BBinders* binder2 = molnode_map[edge_it->node2].first->GetRef()->GetBB(sub_subject2);

	   if ( binder1 == NULL || binder2 == NULL ) 
      {
         Error_Manager::PrintError(this->GetRef()->GetPos(), 0, " the definition of the edges in molecule " + id + " is not correct.");
		   return false;
	   }

      //check that some affinity is defined in the type file.. otherwise, what can we do?
      AffRuleBase* aff = st->GetEnv()->GetTypeAffinity()->GetAffinity(binder1->GetType(), binder2->GetType());
      if (aff == NULL || aff->GetCategory() != RULE_EXP_RATES)
      {
         Error_Manager::PrintError(this->GetRef()->GetPos(), 0, " missing affinity: you cannot create a complex between binders '" +
            edge_it->binder1 + "' and '" + edge_it->binder2 + "' if there is no affinity between '" + 
            binder1->GetType() + "' and '" + binder2->GetType() + "' in the types file.");
         return false;
      }


	   comp_graph->AddEdgeMaybeNode(binder1->GetSubject(), binder2->GetSubject(), node1, node2);
	   // aggiungo il nuovo nodo
	   string n_edge = node1->GetRef()->GetKey(node2->GetRef(), 
		   binder1->GetSubject(), binder2->GetSubject());
      st->GetEnv()->GetUIBind_map()->IncBindElem(node1->GetRef(), node2->GetRef(), 
		   binder1->GetSubject(), binder2->GetSubject(), n_edge, NULL);
	   st->GetEnv()->GetUIBind_map()->AddCounter(n_edge, counter-1);
   }

   // Se il grafo non e' connesso genero un errore
   if ( comp_graph->IsConnected() ) {
	   std::vector<string> edge_array;
	   string key;
	   comp_graph->Key(&edge_array);
	   comp_graph->Clean();
	   Utility::VKey(edge_array,&key);

	   int e_id = st->GetMolIDFromName(this->id);
	   Complex* new_comp = new Complex(comp_graph);
	   new_comp->AddCounter(counter-1);

	   if (st->GetEnv()->GetComplexMap()->Insert(new_comp,key,e_id) == NULL)
	   {
		   st->GetEnv()->GetComplexRel()->AddComplex(new_comp);
	   }

	   return true;
   }
   else {
	   Error_Manager::PrintError(0," the molecule " + id + " generate a not connected graph.");
	   return false;
   }
}

//
void ST_Mol_Def::AddCounter(int n) 
{
	counter+=n;
}

void ST_Mol_Def::AddEdge(const std::string& node1, const std::string& binder1, 
                         const std::string& node2, const std::string& binder2)
{
   defined_edges.push_back(ST_Mol_Edge(node1, binder1, node2, binder2));
}

bool ST_Mol_Def::AddNode(const std::string& node_name, ST_Beta_Proc* beta_type, 
                         set<string>*  bound_binders)
{   
   if (node_names.find(node_name) != node_names.end())
   {
      Error_Manager::PrintError(this->GetRef()->GetPos(), 3, "node \'" + node_name + "\' already defined");
      return false;
   }
   defined_nodes.push_back(triple<string, ST_Beta_Proc*, set<string>* >(node_name, 
      beta_type, bound_binders));
   node_names.insert(node_name);
   return true;
}

bool ST_Mol_Def::AddNode(const std::string& node_name, 
                         const std::string& ref_node_name)
{
   //It must be a new name (not already defined)
   if (node_names.find(node_name) != node_names.end())
   {
      Error_Manager::PrintError(this->GetRef()->GetPos(), 3, "node \'" + node_name + "\' already defined");
      return false;
   }

   //Reference must be already defined
   if (referenced_nodes_map.find(ref_node_name) == referenced_nodes_map.end())
   {
      Error_Manager::PrintError(this->GetRef()->GetPos(), 3, "node \'" + node_name + "\' refers to the undefined node '" + ref_node_name + "'");
      return false;
   }

   referenced_nodes_map[ref_node_name].push_back(node_name);
   node_names.insert(node_name);
   return true;
}

bool ST_Mol_Def::FindNode(const std::string& node_name)
{
   return (node_names.find(node_name) != node_names.end());
}

ST_Beta_Proc* ST_Mol_Def::GetNode(const std::string& node_name)
{
   if (node_names.find(node_name) == node_names.end())
      return NULL;

   string beta_id = NO_PROCESS;

   //search through the map if node_name is a reference to a real bproc
   hash_map< std::string, std::vector<std::string> >::iterator ref_it;
   for (ref_it = referenced_nodes_map.begin(); ref_it != referenced_nodes_map.end(); ++ref_it)
   {
      std::vector<std::string>::iterator vec_it;
      for (vec_it = ref_it->second.begin(); vec_it != ref_it->second.end(); ++vec_it)
         if (*vec_it == node_name)
         {
            beta_id = ref_it->first;
         }
   }   

   //if not, the name is directly a process
   if (beta_id == NO_PROCESS)
      beta_id = node_name;

   std::vector< triple<string, ST_Beta_Proc*, std::set<string>*> >::iterator def_it;
   for (def_it = defined_nodes.begin(); def_it != defined_nodes.end(); ++def_it)
   {
      //Get the corresponding ST for the beta processes involved
      if (def_it->first() == node_name)
         return def_it->second();
   }

   return NULL;
}

bool ST_Event::InitAM1(SymbolTable* st)
{
   std::list< std::pair<Entity*, int> > affected_entities;
   std::list<Entity*> originating_entities;
   std::vector<Entity*> targets;


   // First step: translate ST lists to Entity lists (InitAM1 for ST_Beta to Entity already run)
   std::list<std::pair<ST_Beta_Proc*, int> >::iterator aff_it;
   for (aff_it = AffectedProcesses.begin(); 
      aff_it != AffectedProcesses.end(); 
      ++aff_it)
   {
      // in the case of a NIL_BPROC (Nil), the ST_Beta_Proc* will be NULL.
      // Treat as a special case.
      ST_Beta_Proc* bproc = aff_it->first;
      if (bproc == NULL)
      {
		 Entity *tmp=NULL;
         affected_entities.push_back(pair<Entity*, int>(tmp, aff_it->second));
      }
      else
      {
         affected_entities.push_back(pair<Entity*, int>(bproc->GetEntity(), aff_it->second));
         targets.push_back(bproc->GetEntity());
      }
   }

   std::list<ST_Beta_Proc*>::iterator org_it;
   for (org_it = OriginatingProcesses.begin(); 
      org_it != OriginatingProcesses.end(); 
      ++org_it)
   {
      originating_entities.push_back((**org_it).GetEntity());
   }

   // Second step: Do some pre processing based on verbs
   // and create the right EventVerb for execution
   switch (verbType)
   {
   case VERB_SPLIT:
      {
         Entity* source_entity = originating_entities.front();
         // removed res = : a failure in IsSplitOk is not an error, but
         // a simple warning
         BBinders::IsSplitOk2(source_entity, targets);
      }
      verb = new EntityVerb(verbType);
      break;

   case VERB_JOIN:
      {
         Entity* target = targets[0];
         // removed res = : a failure in IsJoinOk is not an error, but
         // a simple warning
         BBinders::IsJoinOk2(originating_entities, target);
      }
      verb = new EntityVerb(verbType);
      break;

   case VERB_UPDATE:
      assert(this->verbUpdateFunc);
      assert(this->verbStateVar);
      verb = new StateVarVerb(verbStateVar->GetStateVar(), 
         verbUpdateFunc);
      break;

   default:
      // Generate an EventVerb for entities
      verb = new EntityVerb(verbType);
      break;
   }

   // Third step: create a FunBase for Time and ActualRate
   FunBase* fun = NULL;
   if (condType == COND_RATE_FUN)
   {
      //use the given function
      fun = st->FindFunction(GetFunctionRateId());
      if (fun == NULL)
      {
         Error_Manager::PrintError(GetRef()->GetPos(), 15, "function '" + GetFunctionRateId() + "' is not defined. Check your function file!");
         return false;
      }   
   }      
   else
   {
      //use standard functions
      double value = GetNumericRate();

      switch (originating_entities.size())
      {
      case 1:
         {
            // There are two special cases here: we can have "constant rates", zeroth-order reactions (new)
            // or monomolecular reactions. Distinguish between them.
            if (verbType == VERB_NEW)
            {
               // Always execute it 
               fun = new FunConstant(st->GetEnv(), value, true);
            }
            else if (cond != NULL)
            {
               // Execute it only if different from 0
               fun = new FunConstant(st->GetEnv(), value, false);
            }
            else
            {
               fun = new FunMono(st->GetEnv(), value, originating_entities.front());
            }
         }
         break;
      
      case 2:
         fun = new FunBim(st->GetEnv(), value, 1);
         break;

      case 0:
         //UPDATE, non origitating entity
         fun = NULL; //TODO!! ???
         break;

      default:
         assert(false && "Event: more than 2 originating entities are not allowed");
      }


      std::list<Entity*>::const_iterator oit;
      // Careful!!! The inizialization of funRate works only if the number of orginating
      // entities is 2, because we assume that this constructor is called only for conditions
      // with two entities. If the list of originating entities contains more than two
      // entities only the first two are used for creating the funRate of type FunBim.

      int position = 0;
      for (oit = originating_entities.begin(); oit != originating_entities.end(); ++oit)
      {
         fun->SetEntityVariable(*oit,position++);			   
      }
   }   
   //END FUNCTION  

   // values of the parameters for the general distributions
   double v_param1 = atof(this->param1.c_str());
   double v_param2 = atof(this->param2.c_str());

   EventReaction* new_event = new EventReaction(st->GetEnv(), 
      cond, condType, verb, fun, IsInheritable, 
      originating_entities, affected_entities);	

   return st->GetEnv()->AddEvent(new_event,v_param1,v_param2,this->parameters);
}


//////////////////////////////////////////////////////
//
// Symbol table state variable definition entry
//
//////////////////////////////////////////////////////


bool ST_Statevar::InitAM1(SymbolTable* st)
{
   // Time to build a StateVar
   int v_id = st->GetVarIDFromName(id);

   if (stateVarType == STATEVAR_CONTINUOUS)
      stateVar = new StateVar(v_id, delta_t, function, stateVarType, init_val);
   else
      stateVar = new StateVar(v_id, function, stateVarType, init_val);

   st->GetEnv()->AddStatevar(stateVar);
   return true;
}

bool ST_Statevar::InitAM2(SymbolTable* st)
{
   return stateVar->Init();
}


//////////////////////////////////////////////////////
// SYMBOL_TABLE METHODS DEFINITION
//////////////////////////////////////////////////////

//
SymbolTable::SymbolTable(Environment* p_env) 
 : func_map(p_env)
{
	current = NULL;
   env = p_env;
   Running = false;
}

//
SymbolTable::~SymbolTable() 
{
   std::list<ST_Node*>::iterator n_it;
   for (n_it = symbol_list.begin(); n_it != symbol_list.end(); ++n_it)
      delete (*n_it);

   std::list<ST_Event*>::iterator e_it;
   for (e_it = event_list.begin(); e_it != event_list.end(); ++e_it)
      delete (*e_it);
}

//
void SymbolTable::STInsert(ST_Node *element) {
	symbol_list.push_back(element);
	current = element;
}

bool SymbolTable::CheckRedefinition(const Pos& pos, const std::string& id)
{
   if (FindConstant(id))      
   {
      Error_Manager::PrintError(pos, 35, "'" + id + "' is already defined as a constant.");
      return false;
   }

   if (FindFunction(id))      
   {
      Error_Manager::PrintError(pos, 35, "'" + id + "' is already defined as a function.");
      return false;
   }

   if (Find(id, STATE_VAR))
   {
      Error_Manager::PrintError(pos, 35, "'" + id + "' is already defined as a state variable.");
      return false;
   }

   if (Find(id, BPROC))
   {
      Error_Manager::PrintError(pos, 35, "'" + id + "' is already defined as a bio-process.");
      return false;
   }

   if (Find(id, PPROC))
   {
      Error_Manager::PrintError(pos, 35, "'" + id + "' is already defined as a pi-process.");
      return false;
   }

   if (Find(id, MOL))
   {
      Error_Manager::PrintError(pos, 35, "'" + id + "' is already defined as a complex.");
      return false;
   }

   return true;
}


Entity* SymbolTable::AddEntity(list<BBinders *>* binder_list, PP_Node* pproc, int counter, int e_id)
{
   return env->AddEntity(binder_list, pproc, counter, e_id);
}

bool SymbolTable::AddEvent(bool inheritable, PTNode* ref)
{
   ST_Event* event_node = new ST_Event(inheritable, ref);
   event_list.push_back(event_node);  

   this->current = event_node;
   return true;
}

bool SymbolTable::AddStatevar(const Pos& pos, const std::string& id, Expr* function)
{
   return AddStatevar(pos, id, function, "1.0");
}

bool SymbolTable::AddStatevar(const Pos& pos, const std::string& id, Expr* function, const std::string& init)
{
   // check if already defined
   if (Find(id, STATE_VAR) != NULL)
   {
      Error_Manager::PrintError(pos, 24, "the state variable '" + id + "' is already defined.");
      return false;
   }

   double init_val = atof(init.c_str());

   ST_Statevar* st_node = new ST_Statevar(id, function, init_val);
   this->STInsert(st_node);
   return true;
}

bool SymbolTable::AddStatevar(const Pos& pos, const std::string& id, const std::string& val, Expr* function)
{
   return AddStatevar(pos, id, val, function, "1.0");
}
   
bool SymbolTable::AddStatevar(const Pos& pos, const std::string& id, const std::string& val, Expr* function, const std::string& init)
{
   // check if already defined
   if (!CheckRedefinition(pos, id))
   {
      Error_Manager::PrintError(pos, 24, "the symbol '" + id + "' is already used.");
      return false;
   }

   double value = atof(val.c_str());
   double init_val = atof(init.c_str());

   ST_Statevar* st_node = new ST_Statevar(id, value, function, init_val);
   this->STInsert(st_node);
   return true;
}

bool SymbolTable::AddFunction(const Pos& pos, const std::string& id, Expr* function)
{
   // check for errors, save position
   if (!CheckRedefinition(pos, id))
   {
      Error_Manager::PrintError(pos, 24, "the symbol '" + id + "' is already used.");
      return false;
   }

   func_map.Insert(pos, id, function);
   return true;
}

bool SymbolTable::AddClonedFunction(const std::string& id, FunBase* function)
{
   // check for errors, save position
   if (func_map.Find(id).empty())
      return false;

   func_map.InsertClone(id, function);
   if (this->Running)
   {
      //I'm in running phase, so init this myself
      function->Init();
   }

   return true;
}

//
bool SymbolTable::Is(const string& elem) {
	list<ST_Node *>::iterator i;
	for(i=symbol_list.begin(); i!=symbol_list.end(); i++)
		if ((*i)->GetIdentifier() == elem ) return(true);
	return(false);
}

bool SymbolTable::FindConstant(const std::string &id)
{
   ConstantMap::iterator it = constant_map.find(id);
   if (it != constant_map.end())
      return true;

	return false;
}

//
ST_Node *SymbolTable::Find(const string& elem, EntityType category) 
{
	list<ST_Node *>::iterator i;
	for(i=symbol_list.begin(); i!=symbol_list.end(); i++)
   {
		if (((*i)->GetIdentifier() == elem) && ((*i)->GetCategory() == category)) 
         return (*i);
   }
   // Nothing in global symbols;
   // try the stack of template ambients
   std::vector<ST_Temp_Ambient*>::reverse_iterator rit;

   //Use a reverse iterator: last ambient pushed searched first
   for (rit = TempAmbientStack.rbegin(); rit != TempAmbientStack.rend(); ++rit)
   {
      ST_Node* node = (**rit).Find(elem, category);
      if (node != NULL)
         return node;
   }
	return NULL;
}

bool SymbolTable::FindType(const string& key)
{
   if (this->GetEnv()->GetTypeAffinity()->Find(key))
      return true;
	
   // Nothing in global symbols;
   // try the stack of template ambients
   std::vector<ST_Temp_Ambient*>::reverse_iterator rit;

   //Use a reverse iterator: last ambient pushed searched first
   for (rit = TempAmbientStack.rbegin(); rit != TempAmbientStack.rend(); ++rit)
   {
      ST_Node* node = (**rit).Find(key, TYPE_TEMPLATE_REF);
      if (node != NULL)
         return true;
   }
	return false;
}

//
list<Free_Name> *SymbolTable::GetFnList(const string& elem) {
	list<ST_Node *>::iterator i;
	for(i=symbol_list.begin(); i!=symbol_list.end(); i++)
		if (((*i)->GetIdentifier() == elem) && (((*i)->GetCategory() == PPROC)) ) 
			return ((ST_Pi_Proc *)(*i))->GetFnList();
	return NULL;
}

//
void SymbolTable::SetCurrent(ST_Node *elem) {
	current = elem;
}

//
ST_Node *SymbolTable::GetCurrent() {
	return current;
}

//
bool SymbolTable::Validate() {
	// verify if the BASERATE is present;
	bool is_base = false;
   if (env->GetRateMap()->find("BASERATE") != env->GetRateMap()->end()) 
		is_base = true;
	bool res = true;
	bool find;
	string message;
	list<ST_Node *>::iterator i;
	for(i=symbol_list.begin(); i!=symbol_list.end(); i++)
		if ((*i)->GetCategory() == BPROC) {
			list<Free_Name> *fn_list = ((ST_Beta_Proc *)(*i))->GetFnList();
			list<Free_Name>::iterator j;
			for(j=fn_list->begin();j!=fn_list->end();j++)
				if ((*j).type == FN_GENERAL) {
					if (env->GetRateMap()->find((*j).name) == env->GetRateMap()->end() && !is_base) {
						message = "rate of name '" + (*j).name + "' in beta-process '" + (*i)->GetIdentifier() + "' not defined.";
						Error_Manager::PrintError(2, message);
						res = res && false;
					}
				}
				else if ((*j).type == FN_BINDER_SUB) {
					std::list<BBinders *> *binder_list = ((ST_Beta_Proc *)(*i))->GetBinderList();
					std::list<BBinders *>::iterator k;
					find = false;
					for(k=binder_list->begin();k!=binder_list->end() && !find;k++)
						if ( (*j).name == (*k)->GetSubject() )
							find = true;
					if (!find) {
						message = "In the bio-process " + ((ST_Beta_Proc *)(*i))->GetId() + 
							" an invalid hide/unhide/if/change actions is present. There is a free subject that do not refer to any binder.";
						Error_Manager::PrintWarning(1, message);
					}
				}
		}
	return res;
}

//
bool SymbolTable::GenerateIC() 
{
   bool res = true;
	list<ST_Node *>::iterator i;
	for(i=symbol_list.begin(); i!=symbol_list.end(); i++) 
		res &= (*i)->GenerateIC(this);
   return res;
}

//
bool SymbolTable::InitAM() 
{
	list<ST_Node *>::iterator i;

	bool res = true;

	// first phase: allocate entities, reduce trees, create molecules, allocate state variables
	for(i=symbol_list.begin(); i!=symbol_list.end(); i++) 
		res &= (*i)->InitAM1(this);

	// If res is false do not add the events and return false immediately
	if (!res) 
		return false;

   // after Entity creation: initialize functions 
   res = func_map.InitAM();
   if (!res) 
		return false;

	// build events
	std::list<ST_Event*>::iterator ev_it;
	for (ev_it = event_list.begin(); ev_it != event_list.end(); ++ev_it)
	{    
		ST_Event* current_event = *ev_it;
      if (!current_event->InitAM1(this))
         return false;
	}

   // second phase: initialize state variables and performs cleanup 
   for(i=symbol_list.begin(); i!=symbol_list.end(); i++) 
		res &= (*i)->InitAM2(this);

   Running = true;
   return res;
}

//
void SymbolTable::PrintIC() {
	list<ST_Node *>::iterator i;
	for(i=symbol_list.begin(); i!=symbol_list.end(); i++) {
		if ((*i)->GetCategory() == PPROC)
			((ST_Pi_Proc *)(*i))->GetIC()->Print(0);
		if ((*i)->GetCategory() == BPROC)
			((ST_Beta_Proc *)(*i))->GetIC()->Print(0);
	}
}

//New function to map string identifiers to integers
int SymbolTable::GetEntIDFromName(const std::string& s_id)
{
   string name_id = "S_" + s_id; 
   for (int i = 0; i < (int)entIdToString.size(); ++i)
   {
      if (name_id == entIdToString[i])
      {
         return i + 1;
      }
   }

   //else
   entIdToString.push_back(name_id);
   return (int)entIdToString.size();
}

//Function to map string identifiers to integers
int SymbolTable::GetVarIDFromName(const std::string& s_id)
{
   for (int i = 0; i < (int)varIdToString.size(); ++i)
   {
      if (s_id == varIdToString[i])
      {
         return i + 1;
      }
   }

   //else
   varIdToString.push_back("S_" + s_id);
   return (int)varIdToString.size();
}

const std::string& SymbolTable::GetEntNameFromID(int id)
{
	assert(id > 0);
   return entIdToString[id - 1];
}

const std::string& SymbolTable::GetVarNameFromID(int id)
{
   return varIdToString[id - 1];
}

int SymbolTable::NewEntID()
{
   int e_id = (int)entIdToString.size() + 1; 
   entIdToString.push_back("S_" + Utility::i2s(e_id));
   return e_id;
}

int SymbolTable::NewVarID()
{
   int e_id = (int)varIdToString.size() + 1; 
   varIdToString.push_back("S_" + Utility::i2s(e_id));
   return e_id;
}

//
int SymbolTable::GetMolIDFromName(const std::string& s_id)
{
   string name_id = "M_" + s_id;
   for (int i = 0; i < (int)molIdToString.size(); ++i)
   {
      if (name_id == molIdToString[i])
      {
         return i + 1;
      }
   }

   //else
   molIdToString.push_back(name_id);
   return (int)molIdToString.size();
}

//
const std::string& SymbolTable::GetMolNameFromID(int id)
{
	assert(id > 0);
   return molIdToString[id - 1];
}

//
int SymbolTable::NewMolID()
{
   int e_id = (int)molIdToString.size() + 1; 
   molIdToString.push_back("M_" + Utility::i2s(e_id));
   return e_id;
}

string SymbolTable::GetStringVerb(VerbType eventType)
{
   switch (eventType)
   {  
   case VERB_SPLIT:
      return "split";
      
   case VERB_NEW:
      return "new";
      
   case VERB_DELETE:
      return "delete";
      
   case VERB_JOIN:
      return "join";

   case VERB_UPDATE:
      return "update";

   default:      
      return "_";      
   }
}

/////////////////////////////////////////////////////////////////
// ST_Template_Name class
/////////////////////////////////////////////////////////////////

ST_Template_Name::ST_Template_Name(const string& p_id, TempType p_temp_type, PTNode* p_ref) 
   : ST_Node((EntityType)p_temp_type, p_ref)
{
   id = p_id;
   temp_type = p_temp_type;
}

/////////////////////////////////////////////////////////////////
// ST_Temp_Ambient class
/////////////////////////////////////////////////////////////////

//
ST_Node* ST_Temp_Ambient::Find(const string& elem, EntityType category) 
{
	vector<ST_Node *>::iterator i;
	for(i=symbol_list.begin(); i!=symbol_list.end(); i++)
		if (((*i)->GetIdentifier() == elem) && ((*i)->GetCategory() == category)) return (*i);
	return NULL;
}

//
int ST_Temp_Ambient::FindPosition(const string& elem, EntityType category) 
{
   for (size_t i = 0; i < symbol_list.size(); ++i)
   {

		if ((symbol_list[i]->GetIdentifier() == elem) && 
          (symbol_list[i]->GetCategory() == category)) 
      {
         return ((int)i);
      }
   }
	return -1;
}

//
void ST_Temp_Ambient::Add(ST_Node *node)
{
   this->symbol_list.push_back(node);
}

//////////////////////////////////////////////////////
// Symbol Table Bproc Template definition entry
//////////////////////////////////////////////////////

ST_Bproc_Template::~ST_Bproc_Template()
{
   BBinders *tmp_b;
   if(binder_list != NULL)
   {
      while(!binder_list->empty()) 
      {
         tmp_b = binder_list->front();
         binder_list->pop_front();
         delete tmp_b;
      }
      delete binder_list;
   }
   if (pproc != NULL) 
      delete pproc;
}

//
bool ST_Bproc_Template::InsertBinder(BBinders *elem) 
{
	list<BBinders *>::iterator i;
    for(i=binder_list->begin(); i!=binder_list->end(); i++)
		if ( ((*i)->GetSubject() == elem->GetSubject()) || ((*i)->GetType() == elem->GetType()) )
				return false;
	binder_list->push_front(elem);
	return true;
}

BBinders* ST_Bproc_Template::FindBinderByName(const std::string& binder_name)
{
   list<BBinders *>::iterator i;
    for(i=binder_list->begin(); i!=binder_list->end(); i++)
		if ((*i)->GetSubject() == binder_name)
         return *i;

    return NULL;
}

//
bool ST_Bproc_Template::GenerateIC(SymbolTable *st) 
{
	if (!generated)
	{
		generated = true;
		pproc = NULL;
      st->PushTempAmbient(this->GetTemplateAmbient());
		bool retval = this->GetRef()->GenerateIC(&pproc,st);
      st->PopTempAmbient();
      return retval;
	}
   else
      return true;
}

