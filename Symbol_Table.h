#ifndef SYMBOL_TABLE_H
#define SYMBOL_TABLE_H

#include <cstdlib>
#include <algorithm>
#include <string>
#include <list>
#include <set>
#include <sstream>

#include "hash.h"

#include "PT_General_Node.h"
#include "Pi_Process.h"
//#include "Environment.h"
#include "Define.h"
#include "FunGeneric.h"
#include "FuncMap.h"

#include "EventCond.h"
#include "EventVerb.h"

using namespace std;

class PTNode;
class SymbolTable;
class Environment;
class EventCond;
class EventVerb;
class ST_Node;
class PP_Node;

#define MAX_INT = std::numeric_limits<int>::max()

//////////////////////////////////////////////////////
// ST_Temp_Ambient CLASS
//////////////////////////////////////////////////////

class ST_Temp_Ambient
{
private:
   vector<ST_Node *> symbol_list;
public:
   ST_Node *Find(const string& elem, EntityType category);
   void Add(ST_Node* node);

   size_t Size() { return symbol_list.size(); }
   
   int FindPosition(const string& elem, EntityType category);

   std::vector<ST_Node *>::const_iterator GetIterator()
   {
      return symbol_list.begin();
   }
};




//////////////////////////////////////////////////////
// ST_NODE CLASS
//////////////////////////////////////////////////////

class ST_Node {
private:
	PTNode *ref;
	EntityType category;
public:
	ST_Node(EntityType p_category, PTNode *p_ref);
	virtual ~ST_Node() {};

	PTNode *GetRef();

	virtual const string& GetIdentifier() = 0;
	virtual bool GenerateIC(SymbolTable *st);

	//Two-phase ambient init
	//First pass is mandatory, second optional
	virtual bool InitAM1(SymbolTable* st) = 0;
	virtual bool InitAM2(SymbolTable* st) { return true; }

	EntityType GetCategory();
};



//////////////////////////////////////////////////////
// ST_PI_PROC CLASS
//////////////////////////////////////////////////////

class ST_Pi_Proc : public ST_Node {
private:
	string id;
	list<Free_Name> fn;
	PP_Node *pproc;
public:
	ST_Pi_Proc(const string& p_id, EntityType p_category, PTNode *p_ref);
	~ST_Pi_Proc();

	const string& GetIdentifier();

	bool GenerateIC(SymbolTable *st);
	void PrintFN();

	list<Free_Name> *GetFnList();

	PP_Node *GetIC();

	// Empty implementation (pi processes only inside boxes)
	virtual bool InitAM1(SymbolTable* st) { return true; }   
};

//////////////////////////////////////////////////////
// ST_Pi_Sequence CLASS
//////////////////////////////////////////////////////

class ST_Pi_Sequence : public ST_Node {
private:
	string id;
	list<Free_Name> fn;
	PP_Node *pproc;
public:
	ST_Pi_Sequence(const string& p_id, EntityType p_category, PTNode *p_ref);
	~ST_Pi_Sequence();

	const string& GetIdentifier();

	bool GenerateIC(SymbolTable *st);
	void PrintFN();

	list<Free_Name> *GetFnList();

	PP_Node *GetIC();

	// Empty implementation (sequence only inside templates)
	virtual bool InitAM1(SymbolTable* st) { return true; }   
};

//////////////////////////////////////////////////////
// ST_BETA_PROC CLASS
//////////////////////////////////////////////////////

class ST_Beta_Proc : public ST_Node {
private:
	string id;
	int counter;
	list<BBinders *> *binder_list;
	list<Free_Name> fn;
	PP_Node *pproc;

	bool initialized;
	bool generated;
	Entity* myEntity;
	std::list<Sub *> mySubs;

public:
	ST_Beta_Proc(const string& p_id, EntityType p_category, PTNode *p_ref);
	~ST_Beta_Proc();

	std::list<BBinders*>* GetBinderList();

	string GetSubOld(const string& subject) {
		std::list<Sub *>::iterator i;
		for( i = mySubs.begin() ; i != mySubs.end() ; i++ )
			if ( (*i)->new_v == subject ) return (*i)->old_v;
		return "";
	}

	string GetSubNew(const string& subject) {
		std::list<Sub *>::iterator i;
		for( i = mySubs.begin() ; i != mySubs.end() ; i++ )
			if ( (*i)->old_v == subject ) return (*i)->new_v;
		return "";
	}

	void AddCounter(int n);
	bool GenerateIC(SymbolTable *st);
	virtual bool InitAM1(SymbolTable* st);
	virtual bool InitAM2(SymbolTable* st);
	void PrintFN();
	string GetId() const { return id; }

	bool IsInitialized();
	bool IsGenerated();
	Entity* GetEntity() { assert(IsInitialized()); return myEntity; }

	const string& GetIdentifier();

	list<Free_Name> *GetFnList();

	/* if "the binder_list" union "elem" is Well Formed the the binder
	is inserted and the function return true. Otherwise the function
	returns false. */
	bool InsertBinder(BBinders *elem);	
	BBinders* FindBinderByName(const std::string& binder_name);

	PP_Node *GetIC();
   void SetIC(PP_Node* ppnode);
};


//////////////////////////////////////////////////////
// ST_Mol_Def CLASS
//////////////////////////////////////////////////////

struct ST_Mol_Edge
{
	std::string node1;
	std::string node2;
	std::string binder1;
	std::string binder2;

	ST_Mol_Edge() { }
	ST_Mol_Edge(const std::string& n1, const std::string& b1, 
		const std::string& n2, const std::string& b2)
		: node1(n1), node2(n2), binder1(b1), binder2(b2) { }
};

class ST_Mol_Def : public ST_Node {
private:
	string id;
	int counter;

	// list of directly defined nodes
	std::vector< triple<std::string, ST_Beta_Proc*, std::set<std::string>*> > defined_nodes;

	// map of referenced nodes
	hash_map< std::string, std::vector<std::string> > referenced_nodes_map;

	// list of edged ("signature")
	std::list<ST_Mol_Edge> defined_edges;

	// names used for nodes
	std::set<std::string> node_names;

public:
	ST_Mol_Def(const string& mol_id, EntityType p_category, PTNode *p_ref);
	~ST_Mol_Def();

	void AddEdge(const std::string& node1, const std::string& binder1,
		const std::string& node2, const std::string& binder2);

	bool AddNode(const string& node_name, ST_Beta_Proc* beta_type, set<string>* bound_binders);

	bool AddNode(const std::string& node_name, const std::string& ref_node_name);

	//return true if node is present
	bool FindNode(const std::string& node_name);
	ST_Beta_Proc* GetNode(const std::string& node_name);

	const string& GetIdentifier();

	virtual bool InitAM1(SymbolTable* st);
	void AddCounter(int n);

};

//////////////////////////////////////////////////////////
// ST_Template_Name class
//////////////////////////////////////////////////////////
class ST_Template_Name : public ST_Node
{
private:
	string id;
	TempType temp_type;

public:
	ST_Template_Name(const string& p_id, TempType p_temp_type, PTNode* ref);
	bool InitAM1(SymbolTable *) { return true; }

	const string& GetIdentifier() { return id; }
};


//////////////////////////////////////////////////////////
// ST_Pproc_Template class
//////////////////////////////////////////////////////////

class ST_Pproc_Template : public ST_Node {
private:
	string id;
	PP_Node *pproc;
	list<Free_Name> fn;

	//TemplateAmbient holds the translation of DecList
	ST_Temp_Ambient TemplateAmbient;

public:
	ST_Pproc_Template(const string& p_id, EntityType p_category, PTNode *p_ref);
	~ST_Pproc_Template();
	const string& GetIdentifier() { return id; }

	// This method generate template code that will contain
	// references to template names.
	// When istantiated, will be COPIED and template names
	// substituted.
	// Code will be generated by PTN_PiInv_Template::GenerateIC
	bool GenerateIC(SymbolTable *st);
	bool InitAM1(SymbolTable* st) { return true; }
	list<Free_Name>* GetFnList() { return &fn; }
	ST_Temp_Ambient* GetTemplateAmbient() { return &TemplateAmbient; }
	PP_Node* GetIC() { return pproc; }
};


//////////////////////////////////////////////////////////
// ST_Bproc_Template class
//////////////////////////////////////////////////////////

class ST_Bproc_Template : public ST_Node {
private:
	string id;
	list<BBinders*>* binder_list;
	list<Free_Name> fn;
	PP_Node* pproc;

	//TemplateAmbient holds the translation of DecList
	ST_Temp_Ambient TemplateAmbient;

	bool generated;

public:
	ST_Bproc_Template(const string& p_id, PTNode *p_ref)
		: ST_Node(BPROC_TEMPLATE, p_ref)
	{
		id = p_id;
		pproc = NULL;
		binder_list = new list<BBinders *>();
		generated = false;
	}

	~ST_Bproc_Template();

	std::list<BBinders*>* GetBinderList() { return binder_list; }
	const string& GetIdentifier() { return id; }

	bool InitAM1(SymbolTable* st) { return true; }
	list<Free_Name>* GetFnList() { return &fn; }
	ST_Temp_Ambient* GetTemplateAmbient() { return &TemplateAmbient; }

	bool InsertBinder(BBinders *elem);	
	BBinders* FindBinderByName(const std::string& binder_name);

	PP_Node *GetIC() { return pproc; }
	bool GenerateIC(SymbolTable *st);

};


//////////////////////////////////////////////////////
// ST_Statevar class
//////////////////////////////////////////////////////
class ST_Statevar : public ST_Node
{
private:
	// The end product: expression plus list of references
	StateVar* stateVar;
	StateVarType stateVarType;

	std::string id;   

	Expr* function;
	double delta_t;
	double init_val;

public:
	ST_Statevar(const std::string& p_id, Expr* p_function, double p_init_val)
		: ST_Node(STATE_VAR, NULL), id(p_id)
	{
		stateVar = NULL;
		function = p_function;
		stateVarType = STAREVAR_DISCRETE;
		init_val = p_init_val;
	}

	ST_Statevar(const std::string& p_id, double p_delta_t, Expr* p_function, double p_init_val)
		: ST_Node(STATE_VAR, NULL), id(p_id)
	{
		stateVar = NULL;
		function = p_function;
		stateVarType = STATEVAR_CONTINUOUS;
		delta_t = p_delta_t;
		init_val = p_init_val;
	}

	const string& GetIdentifier() { return id; }
	virtual bool InitAM1(SymbolTable* st);
	virtual bool InitAM2(SymbolTable* st);
	StateVar* GetStateVar() 
   { 
      //assert(stateVar != NULL); 
      return stateVar; 
   }

	void Print(ofstream *fstr) 
	{
		if ( stateVarType == STAREVAR_DISCRETE )
		{
			(*fstr) << "let " << id << " : var = ";
			function->Print(fstr, PRINT_PARAM);
			(*fstr) << " init " << init_val << ";\n";			 
		}
		else if ( stateVarType == STATEVAR_CONTINUOUS ) 
		{
			(*fstr) << "let (" << delta_t << ")" << id << " : var = ";
			function->Print(fstr, PRINT_PARAM);
			(*fstr) << " init " << init_val << ";\n";	
		}
	}
};

//////////////////////////////////////////////////////
// ST_Event class
//////////////////////////////////////////////////////

class ST_Event : public ST_Node
{
private:
	std::string RateConstantOrFunName;
	// Parameters for NORMAL and GAMMA distributions
	std::string param1;
	std::string param2;
	vector< pair<double,double> > parameters;

public:
	std::list<ST_Beta_Proc*> OriginatingProcesses;
	std::list<std::pair<ST_Beta_Proc*, int> > AffectedProcesses;

	EventCond* cond;
	EventVerb* verb;

	//Setted by PTN_Event*::GenerateST, must be public
	VerbType verbType;
	FunBase* verbUpdateFunc;
	ST_Statevar* verbStateVar;

	CondType condType;

	bool IsInheritable;

	ST_Event(bool inheritable, PTNode* ref)
		: ST_Node(EVENT_DEF, ref)
	{      
		IsInheritable = inheritable;
		verbStateVar = NULL;
		verbUpdateFunc = NULL;
	}

	void SetEventRate(const std::string& rate_or_fun)
	{      
		RateConstantOrFunName = rate_or_fun;
	}

	void SetEventCond(EventCond* cond)
	{
		this->cond = cond;
	}

	void SetParameters(const std::string& p_param1, const std::string& p_param2)
	{
		this->param1 = p_param1;
		this->param2 = p_param2;
	}

	void SetParameterVector(vector< pair<double,double> >& p_parameters)
	{
		parameters = p_parameters;
	}

	double GetNumericRate()
	{
		double field_value = 0.0;

		if (condType != COND_RATE_FUN)
		{
			if (RateConstantOrFunName == INF_RATE) 
				field_value = HUGE_VAL;
			else 
				field_value = atof(RateConstantOrFunName.c_str());
		}

		return field_value;  
	}

	std::string& GetFunctionRateId()
	{
		//assert (cond->condType == COND_RATE_FUN);
		return RateConstantOrFunName;
	}

	const std::string& GetIdentifier(void) { return EVENT_STRING; }
	bool InitAM1(SymbolTable* st);
};

//////////////////////////////////////////////////////
// SYMBOL_TABLE CLASS
//////////////////////////////////////////////////////

typedef hash_map<std::string, double> ConstantMap;

// Constants Iterator
class Constants_Iterator : public Iterator< ConstantMap::iterator >
{
public:
	Constants_Iterator(ConstantMap::iterator p_iterator_begin, size_t p_iterator_size)
		: Iterator< ConstantMap::iterator >(p_iterator_begin,p_iterator_size,IT_CONSTANTS,0) {}
	~Constants_Iterator() {}
};


class SymbolTable {
private:
	ST_Node *current;
	list<ST_Node *> symbol_list;
	list<ST_Event*> event_list;
	list<ST_Statevar> variables_list;

	std::vector<std::string> entIdToString; 
	std::vector<std::string> varIdToString; 
	std::vector<std::string> molIdToString; 

	FuncMap func_map;
	ConstantMap constant_map;   

	Environment* env;

	// Additional definitions to the symbols, by template definitions
	std::vector<ST_Temp_Ambient*> TempAmbientStack;

   bool Running;

public:
	SymbolTable(Environment* p_env);
	~SymbolTable();

	Environment* GetEnv() { return env; }

	void STInsert(ST_Node *element);
	void SetCurrent(ST_Node *);

	bool CheckRedefinition(const Pos& pos, const std::string& id);

	bool AddEvent(bool inheritable, PTNode* ref);
	Entity* AddEntity(list<BBinders *>* binder_list, PP_Node* pproc, int counter, int e_id);

	bool AddStatevar(const Pos& pos, const std::string& id, Expr* function);
	bool AddStatevar(const Pos& pos, const std::string& id, const std::string& val, Expr* function);
	bool AddStatevar(const Pos& pos, const std::string& id, Expr* function, const std::string& init);
	bool AddStatevar(const Pos& pos, const std::string& id, const std::string& val, Expr* function, const std::string& init);

	bool AddFunction(const Pos& pos, const std::string& id, Expr* function);
	bool AddClonedFunction(const std::string& id, FunBase* function);

	bool FindConstant(const std::string& id); 

	double GetConstant(const std::string& id)
	{
		double out_value = 0.0;
		ConstantMap::iterator it = constant_map.find(id);
		if (it != constant_map.end())
		{      
			out_value = it->second;
		}
		return out_value;
	}

	void SetConstant(const std::string& id, double val)
	{
		if ( constant_map.find(id) != constant_map.end())
		{      
			constant_map[id] = val;
		}
	}

	std::string GetConstantString(const std::string& id)
	{
		std::stringstream ss;
        ss.precision(20);
		ss << GetConstant(id);      
		return ss.str();
	}

	bool AddConstant(const Pos& pos, const std::string& id, double value)
	{
		if (!CheckRedefinition(pos, id))
			return false;

		constant_map[id] = value;
		return true;
	}

   FunBase* FindFunction(const std::string& id) 
   { 
      std::list<FunBase*>& fun_list = func_map.Find(id);
      if (fun_list.empty())
         return NULL;
      else
         return fun_list.front(); 
   }

	void ResetFuncMap() { func_map.Reset(); }
	FuncMap& GetFuncMap() { return func_map; }

	bool GenerateIC();
	bool InitAM();
	void PrintIC();

	bool Is(const string& elem);
	bool Validate();

	list<Free_Name> *GetFnList(const string& elem);

	ST_Node *Find(const string& elem, EntityType category);
   bool FindType(const string& key);

	ST_Node *GetCurrent();

	const std::string& GetEntNameFromID(int id);
	const std::string& GetVarNameFromID(int id);
	const std::string& GetMolNameFromID(int id);
	int GetEntIDFromName(const std::string& s_id);
	int GetVarIDFromName(const std::string& s_id);
	int GetMolIDFromName(const std::string& s_id);
	int NewEntID();
	int NewVarID();	
	int NewMolID();

	string GetStringVerb(VerbType eventType);

	/*const std::string& GetReactionName(int i1, int i2, int i3, int i4, ElementType type);
	const std::string& GetReactionName(int i1, int i2, int i3, int i4, ElementType, VerbType eventType);
	const std::string& GetReactionName(Entity* in1, Entity* in2, Entity* out1, Entity* out2);
	const std::string& GetReactionName(Entity* ent, Entity* n_ent);*/


	void PushTempAmbient(ST_Temp_Ambient* ambient) { TempAmbientStack.push_back(ambient); }
	void PopTempAmbient() { TempAmbientStack.pop_back(); }

	// Iteratore delle costanti
	Constants_Iterator *GetConstantsIterator()
	{
		return ( new Constants_Iterator(constant_map.begin(),constant_map.size()) );
	}

	void Print(ofstream *fstr)
	{
		(*fstr) << "/* Constants Definitions */" << endl << endl;
		ConstantMap::iterator i_const;
		for ( i_const = constant_map.begin() ; i_const != constant_map.end() ; i_const++ )
		{
			(*fstr) << "let " << (*i_const).first << " : const = " << (*i_const).second << ";" << endl;
		}

		(*fstr) << endl << endl;

		(*fstr) << "/* Variables Definitions */" << endl << endl;
		list<ST_Node *>::iterator i_var;
		for ( i_var = symbol_list.begin() ; i_var!= symbol_list.end() ; i_var++ )
		{
			if ( (*i_var)->GetCategory() == STATE_VAR )
			{
				((ST_Statevar *)(*i_var))->Print(fstr);
				(*fstr) << endl;
			}
		}

		(*fstr) << endl << endl;
		
		(*fstr) << "/* Functions Definitions */" << endl << endl;
		func_map.Print(fstr);
	}

};

#endif

