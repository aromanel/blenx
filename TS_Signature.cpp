#include "TS_Signature.h"

TS_Signature::TS_Signature()
{ }

TS_Signature::TS_Signature(const TS_Signature& right) 
{
	std::vector<TS_sig_elem>::const_iterator i;
	for( i = right.elements.begin() ; i != right.elements.end() ;i++)
	{
		TS_sig_elem elem;
		elem.id = (*i).id;
		elem.cardinality = (*i).cardinality;
		elements.push_back(elem);
	}
}
	
TS_Signature::~TS_Signature()
{}
	
bool TS_Signature::operator==(const TS_Signature& right)
{
	if ( this == &right ) return true;
	if ( elements.size() != right.elements.size() ) return false;
	std::vector<TS_sig_elem>::iterator i;
	std::vector<TS_sig_elem>::const_iterator j = right.elements.begin();
	for( i = elements.begin() ; i != elements.end() ; i++ )
	{
		if ( (*i).id != (*j).id ) return false;
		if ( (*i).cardinality != (*j++).cardinality ) return false;
	}
	return true;
}

TS_Signature& TS_Signature::operator=(const TS_Signature& right) 
{
	if ( this == &right ) return *this;
	std::vector<TS_sig_elem>::const_iterator i;
	// cancello il vecchio elements se c'era
	elements.clear();
	for( i = right.elements.begin() ; i != right.elements.end() ;i++)
	{
		TS_sig_elem elem;
		elem.id = (*i).id;
		elem.cardinality = (*i).cardinality;
		elements.push_back(elem);
	}
	return *this;
}

bool TS_Signature::AddEntity(const int& p_id, const int& p_cardinality) 
{
	TS_sig_elem elem;
	elem.id = p_id;
	elem.cardinality = p_cardinality;
	elements.push_back(elem);
	return true;
}

bool TS_Signature::CheckSpeciesComplexesLimit(int limit)
{
	std::vector<TS_sig_elem>::iterator iter;
	for(iter = elements.begin() ; iter != elements.end() ; iter++)
	{
		// Se vale questa condizione ho finito di controllare 
		// specie e complessi
		if ( (*iter).id == 0 )
			break;

		if ( (*iter).cardinality > limit )
		{
			return false;
		}
	}
	return true;
}




