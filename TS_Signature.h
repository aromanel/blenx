#ifndef TS_SIGNATURE_H_INCLUDED
#define TS_SIGNATURE_H_INCLUDED

#include <string>

#include "Define.h"
#include "Error_Manager.h"
#include "Symbol_Table.h"

struct TS_sig_elem {
	int id;						// IDs of Entities and Complex
	union {
		int cardinality;		// Cardinality of Entities and Complexes
		float value;			// Value of the StateVar
	};
};


class TS_Signature {
public:
	std::vector<TS_sig_elem> elements;
	TS_Signature();
	TS_Signature(const TS_Signature& right);
	~TS_Signature();
	bool operator==(const TS_Signature& right);
	TS_Signature& operator=(const TS_Signature& right);
	// For efficency, I do not control the presence of another element
	// with id equal to p_id. 
	// This control is left to the class user during the signature creation.
	bool AddEntity(const int& p_id, const int& p_cardinality);
	// bool AddBinding(const string& p_key, const int& p_cardinality);
	// Include metodo IsIdEntities e IsIdComplexes che cerca se un id c'e' gia
	bool CheckSpeciesComplexesLimit(int limit);
};

#endif

