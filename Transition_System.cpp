#include "Transition_System.h"

#include <limits>

////////////////////////////////////////////////////////////////
// TS_Edge class method definition

TS_Edge::TS_Edge(const string& p_label, double p_rate, TS_Node *p_source, TS_Node *p_destination)
{
	label = p_label;
	rate = p_rate;
	source = p_source;
	destination = p_destination;
	number = 1;
}

TS_Edge::TS_Edge(const TS_Edge& elem)
{
	label = elem.label;
	rate = elem.rate;
	source = elem.source;
	destination = elem.destination;
	number = elem.number;
}

TS_Edge::~TS_Edge() 
{
	// TODO
}


////////////////////////////////////////////////////////////////
// TS_Node class methods definition


TS_Node::TS_Node(TS_Signature* p_label, Transition_System *p_my_ts)
{
	label = p_label;
	mark = 0;
	node_name = EMPTY_STRING;
	mark_remove = false;
	init_prob = 0.0;
	my_ts = p_my_ts;
}

TS_Node::~TS_Node() 
{
}

bool TS_Node::AddOutEdge(TS_Edge *n_edge) 
{
	out_edges.push_back(n_edge);
	return true;
}

//
bool TS_Node::AddInEdge(TS_Edge *n_edge) 
{
	in_edges.push_back(n_edge);
	return true;
}

//
TS_Signature *TS_Node::GetLabel() 
{
	return label;
}

bool TS_Node::ImplodeImmediateActions()
{
	// Assumo che questa procedura venga chiamata
	// su un nodo che contine azioni immediate
	std::list<TS_Edge *>::iterator in_iterator;
	std::list<TS_Edge *>::iterator out_iterator;

   TS_Edge *current_edge, *c_edge;
	int edge_number;
	int edge_new_number;
	double edge_rate;

	// Totale delle azioni immediate in uscita
	edge_number = this->GetNumberActions();

	// Devo ripristinare le connessioni del grafo non considerando
	// piu il nodo corrente e rinormalizzando gli eventuali rates dei nodi entranti
	out_iterator = this->out_edges.begin();

	while ( out_iterator != this->out_edges.end() )
	{
		in_iterator = this->in_edges.begin();

		if ( (*out_iterator)->GetDestination() == this )
		{
			return false;
		}

		while ( in_iterator != this->in_edges.end() )
		{
			if ( (*in_iterator)->GetRate() != HUGE_VAL )
			{
				edge_rate = ( (*in_iterator)->GetRate() * (*out_iterator)->GetNumber() ) / edge_number;
				edge_new_number = 1;
			}
			else
			{
				edge_rate = HUGE_VAL;
				edge_new_number = (*in_iterator)->GetNumber() * (*out_iterator)->GetNumber();
			}

			current_edge = new TS_Edge( (*out_iterator)->GetLabel(), edge_rate , 
				(*in_iterator)->GetSource(), (*out_iterator)->GetDestination() ); 

			current_edge->SetNumber(edge_new_number);

			if ( ( c_edge = (*in_iterator)->GetSource()->FindOutEdge( (*out_iterator)->GetDestination()->GetLabel() ) ) 
					== NULL )
			{
				(*in_iterator)->GetSource()->AddOutEdge(current_edge);
				(*out_iterator)->GetDestination()->AddInEdge(current_edge);
				this->my_ts->IncEdges();
			}
			else
			{
				c_edge->AddInstance(edge_rate,edge_new_number);
				delete current_edge;
			}


			in_iterator++;

		}

		out_iterator++;

	}

	// Cancello gli archi in uscita
	out_iterator = this->out_edges.begin();
	while ( out_iterator != this->out_edges.end() )
	{
		(*out_iterator)->GetDestination()->DeleteInEdge( this );
		out_iterator = this->out_edges.erase(out_iterator);
		my_ts->DecEdges();
	}

	// Cancello gli archi in ingresso
	in_iterator = this->in_edges.begin();
	while ( in_iterator != this->in_edges.end() )
	{
		(*in_iterator)->GetSource()->DeleteOutEdge( this );
		in_iterator = this->in_edges.erase(in_iterator);
		my_ts->DecEdges();
	}

	return true;

}

int TS_Node::GetNumberActions()
{
	std::list<TS_Edge *>::iterator i;
	int res = 0;
	for ( i = this->out_edges.begin() ; i != this->out_edges.end() ;  i++ )
	{
		res += (*i)->GetNumber();
	}
	return res;
}

// Fa la delete anche dell'arco
void TS_Node::DeleteOutEdge(TS_Node *p_node)
{
	std::list<TS_Edge *>::iterator iter = out_edges.begin();
	TS_Edge *tmp;
	while ( iter != out_edges.end() )
	{
		if ( (*iter)->GetDestination() == p_node )
		{
			tmp = *iter;
			out_edges.erase(iter);
			delete(tmp);
			// Posso tornare subito perche' per costruzione del TS non ho mai piu di un arco 
			// (diretto nello stesso verso) che mi connette due nodi
			return;
		}
		else
		{
			iter++;
		}
	}
}

// Fa la delete anche dell'arco
void TS_Node::DeleteInEdge(TS_Node *p_node)
{
	std::list<TS_Edge *>::iterator iter = in_edges.begin();
	TS_Edge *tmp;
	while ( iter != in_edges.end() )
	{
		if ( (*iter)->GetSource() == p_node )
		{
			tmp = *iter;
			in_edges.erase(iter);
			delete(tmp);
			// Posso tornare subito perche' per costruzione del TS non ho mai piu di un arco 
			// (diretto nello stesso verso) che mi connette due nodi
			return;
		}
		else
		{
			iter++;
		}
	}
}

TS_Edge *TS_Node::FindOutEdge(TS_Signature *destination)
{
	std::list<TS_Edge *>::iterator i;
	
	for( i = this->out_edges.begin() ; i != this->out_edges.end() ; i++ )
	{
		if ( *(*i)->GetDestination()->GetLabel() == *destination )
			return (*i);
	}
	return NULL;
}

TS_Edge *TS_Node::FindInEdge(TS_Signature *source)
{
	std::list<TS_Edge *>::iterator i;
	
	for( i = this->in_edges.begin() ; i != this->in_edges.end() ; i++ )
	{
		if ( *(*i)->GetSource()->GetLabel() == *source )
			return (*i);
	}
	return NULL;
}


///////////////////////////////////////////////////////////////
// Transition_System class methods definition

//
Transition_System::Transition_System(Environment *p_env, unsigned long p_MAX_nodes, int p_MAX_species, int p_MAX_complexes) 
{
	env = p_env;
	node_iter = node_list.begin();
	nodes = edges = 0;
	MAX_nodes = p_MAX_nodes;
	MAX_species = p_MAX_species;
	MAX_complexes = p_MAX_complexes;
}

//
Transition_System::~Transition_System() 
{
	// TODO
}

//
bool Transition_System::AddNode(TS_Signature *node)
{
	NODE_MAP::iterator i;
	TS_Node *n_node;
	if ( ( i = node_hmap.find(*node) ) == node_hmap.end() )
	{
		n_node = new TS_Node(node,this);
		node_hmap[*node] = n_node;
		node_list.push_back(n_node);
		nodes++;
		return true;
	}
	return false;
}

//
bool Transition_System::AddEdge(TS_Signature *source, TS_Signature *destination, 
		const string& label, double rate) 
{
	TS_Node *n_source;
	TS_Node *n_destination;
	TS_Edge *n_edge;
	NODE_MAP::iterator i;
	bool new_node = false;
	
	// Create the source node if not present
	if ( ( i = node_hmap.find(*source) ) == node_hmap.end() )
	{
		// nessun controllo sul numero di nodi o sul numero di specie, perche', durante
		// l'esecuzione la source dovrebbe sempre esistere. Qui entro solo quando
		// aggiungo il nodo root al TS
		n_source = new TS_Node(source,this);
		node_hmap[*source] = n_source;
		node_list.push_back(n_source);
		nodes++;
		new_node = true;
	}
	else
	{
		n_source = (*i).second;
	}

	// Create the destination node if not present
	if ( ( i = node_hmap.find(*destination) ) == node_hmap.end() )
	{
		if ( !new_node && this->CheckLimits(destination) )
		{
			return false;
		}
		n_destination = new TS_Node(destination,this);
		node_hmap[*destination] = n_destination;
		node_list.push_back(n_destination);
		nodes++;
		new_node = true;
	}
	else
	{
		delete(destination);
		n_destination = (*i).second;
	}

	TS_Edge *current_edge;

	// qui aggiungo l'arco solo se non esiste gia' un arco che unisce i due nodi
	n_edge = new TS_Edge(label,rate,n_source,n_destination);	
	
	if ( ( current_edge = n_source->FindOutEdge( n_destination->GetLabel() ) ) == NULL )
	{
		n_destination->AddInEdge(n_edge);
		n_source->AddOutEdge(n_edge);
		edges++;
	}
	else
	{
		current_edge->AddInstance(rate,1);
		delete n_edge;
	}

	return true;
}


//
bool Transition_System::CheckLimits(TS_Signature *destination)
{
	bool res = false;
	if ( !destination->CheckSpeciesComplexesLimit(this->MAX_species) || node_list.size() == this->MAX_nodes )
	{
		res = true;
	}

	// Controllo la grandezza dei complessi
	std::vector<TS_sig_elem>::iterator i;

	// Controllo che non ci siano complessi che abbiano numero di nodi maggiore del limite
	for ( i = destination->elements.begin() ; i != destination->elements.end() ; i++ )
	{
		if ( (*i).id < 0 )
		{
			if ( (*this->env->GetComplexList())[ ( (*i).id * (-1) ) - 1 ]->GetComplex()->GetNodes() > this->MAX_complexes )
			{
				res = true;
				break;
			}
		}
	}

	return res;
}


bool Transition_System::IteratorNext() 
{
	if ( node_iter == node_list.end() )
		return false;
	node_iter++;
	if ( node_iter == node_list.end() )
		return false;
	return true;
}
	
//
bool Transition_System::IteratorIsEnd() 
{
	return ( node_iter == node_list.end() );
}

//
void Transition_System::IteratorReset() 
{
	node_iter = node_list.begin();
}

//
TS_Signature *Transition_System::IteratorGetActualSignature() 
{
	if ( node_iter != node_list.end() )
		return (*node_iter)->GetLabel();
	return NULL;
}

//
void Transition_System::PrintNodes(FILE *file)
{
	std::list<TS_Node *>::iterator index;
	std::vector<TS_sig_elem>::iterator i;
	TS_Signature *signature;
	for( index = node_list.begin() ; index != node_list.end() ; index++ )
	{
		fprintf(file, "%s\n", (*index)->node_name.c_str());

		signature = (*index)->GetLabel();
		for( i = signature->elements.begin() ; i != signature->elements.end() ; i++ )
		{
			fprintf(file, "%d,%d ", (*i).id, (*i).cardinality);
		}
		
		fprintf(file, "\n\n");
	}
}

//
void Transition_System::PrintGraphDOT(FILE *file)
{
	fprintf(file, "digraph TS {\n");

	int node_counter = 1;
	PrintEdgesDOT(node_list.front(),&node_counter,file);

	fprintf(file, "}\n");
}

//
void Transition_System::PrintEdgesDOT(TS_Node *node,int *counter,FILE *file) 
{
	std::list<TS_Edge *>::iterator i;
	node->mark = 1;
	
	if ( node->node_name == EMPTY_STRING )
	{
		node->node_name = "N_" + Utility::i2s((*counter)++);
	}
	
	fprintf(file, "%s\n", node->node_name.c_str());
	for( i = node->GetOutEdges()->begin() ; i != node->GetOutEdges()->end() ; i++ ) {

		if ( (*i)->GetDestination()->mark == 0 )
			PrintEdgesDOT((*i)->GetDestination(),counter,file);

		fprintf(file, "%s -> %s", node->node_name.c_str(),(*i)->GetDestination()->node_name.c_str());
		if ( (*i)->GetRate() != HUGE_VAL )
			fprintf(file, " [ label = \" %f \" ]\n", (*i)->GetRate() );
		else
			fprintf(file, " [ label = \" inf = %d \" ]\n", (*i)->GetNumber() );
	} 
}

//
void Transition_System::CleanMarks()
{
	std::list<TS_Node *>::iterator index;
	for( index = this->node_list.begin() ; index != this->node_list.end() ; index++ )
	{
		(*index)->mark = 0;
		(*index)->mark_remove = false;
	}
}

//
void Transition_System::Clean()
{
	std::list<TS_Node *>::iterator index;
	for( index = this->node_list.begin() ; index != this->node_list.end() ; index++ )
	{
		(*index)->mark = 0;
		(*index)->node_name = EMPTY_STRING;
		(*index)->mark_remove = false;
	}
}

//
bool Transition_System::ImmediateActionsImplosion()
{
	// Inserisco un nodo fittizio inziale con rate 1.0 collegato al nodo radice
	// del TS
	TS_Signature *root_sig = new TS_Signature();
	root_sig->AddEntity(0,0);
	TS_Node *root = new TS_Node(root_sig,this);
	root->node_name = "root";
	node_hmap[*root_sig] = root;
	nodes++;
	TS_Node *first = this->node_list.front();
	TS_Signature *first_sig = new TS_Signature(*(first->GetLabel()));
	node_list.push_front(root);
	this->AddEdge(root_sig,first_sig,EMPTY_STRING,1.0);

	// Per ogni nodo del TS faccio implosione dei nodi che contengono
	// azioni immediate

    TS_Node *tmp_node;
	std::list<TS_Node *>::iterator iter = node_list.begin();
	
	while( iter != node_list.end() )
	{
		// controllo che il nodo contenga azioni immediate. Mi basta controllare
		// il primo perche' tanto se contiene azioni immediate sono tutte immediate
		if ( !(*iter)->GetOutEdges()->empty() && (*iter)->GetOutEdges()->front()->GetRate() == HUGE_VAL )
		{
			if ( !(*iter)->ImplodeImmediateActions() )
			{
				return false;
			}

			tmp_node = (*iter);
			iter = node_list.erase(iter);
			node_hmap.erase( node_hmap.find( *(tmp_node->GetLabel()) ) );
			delete(tmp_node);
			nodes--;
		}
		else
		{
			iter++;
		}
	}

	return true;
}

