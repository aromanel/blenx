#ifndef TRANSITION_SYSTEM_H_INCLUDED
#define TRANSITION_SYSTEM_H_INCLUDED

////////////////////////////////////////////////////////////////////////
// This class implements a labelled Stochastic Transition System (STS).
// This class is also used for representing the CTMC of a STS.

#include <string>
#include <iostream>
#include "Define.h"
#include "Error_Manager.h"
#include "TS_Signature.h"
#include "Environment.h"



////////////////////////////////////////////////////////////////////////
// TS_Signature predicates and functions

#ifdef _MSC_VER

#include <hash_map>

using stdext::hash_map;
typedef unsigned __int16 uint16_t;
typedef unsigned __int32 uint32_t;

/*#elif defined(__GNUC__)

#include <ext/hash_map>

using __gnu_cxx::hash_map;
namespace __gnu_cxx
{
   template<> struct hash< TS_Signature >
   {
      size_t operator()(const TS_Signature& s1) const
      {
         return Hash((const char *)&(s1.elements[0]),(int)((2*s1.elements.size()) * sizeof(int)));
      }
   };
}*/

#endif

///////////////////////////////////////////////////////////////////////////////////////////
// Binary Predicate for the Transition System node hash map

inline bool operator==(const TS_Signature& s1, const TS_Signature& s2)
{
	if ( &s1 == &s2 ) return true;
	if ( s1.elements.size() != s2.elements.size() ) return false;
	std::vector<TS_sig_elem>::const_iterator i;
	std::vector<TS_sig_elem>::const_iterator j = s2.elements.begin();
	for( i = s1.elements.begin() ; i != s1.elements.end() ; i++ )
	{
		if ( (*i).id != (*j).id ) return false;
		if ( (*i).cardinality != (*j++).cardinality ) return false;
	}
	return true;
}

struct TS_Signature_hash_compare
{
	static const size_t bucket_size = 4;
	static const size_t min_buckets = 8;

	inline size_t operator()(const TS_Signature& s1) const;

	bool operator()(const TS_Signature& s1, const TS_Signature& s2) const
	{
		return !(s1==s2);
	}
};

///////////////////////////////////////////////////////////////////////////////////////////

class TS_Node;
class Transition_System;

//////////////////////////////////////////////////////////////////////////////////////////
// Implements the edge of the transition system. 
// Each node can be labelled with more than one 
// string.

class TS_Edge {
private:
	TS_Node *source;
	TS_Node *destination;
	string label;
	int number;
	double rate;
public:
	TS_Edge(const string& p_label, double p_rate, TS_Node *p_source, TS_Node *p_destination);
	TS_Edge(const TS_Edge& elem);
	~TS_Edge();
	TS_Node *GetDestination()
	{
		return destination;
	}
	TS_Node *GetSource()
	{
		return source;
	}
	double GetRate()
	{
		return rate;
	}
	string GetLabel()
	{
		return label;
	}
	void AddInstance(double p_rate,int p_number)
	{
		if ( rate != HUGE_VAL )
		{
			rate += p_rate;
		}
		else
		{
			number += p_number;
		}
	}
	int GetNumber() 
	{
		return number;
	}
	void SetNumber(int p_number) 
	{
		number = p_number;
	}
};

//////////////////////////////////////////////////////
// Implements the node of the Transition System.
// The node is labelled with a Signature implemented
// with the TS_Signature class.

class TS_Node {
private:
	// List of outgoing edges.
	std::list<TS_Edge *> out_edges;
	// List of incoming edges.
	std::list<TS_Edge *> in_edges;
	// Node signature
	TS_Signature *label;
	// TS pointer
	Transition_System *my_ts;
public:
	int mark;
	bool mark_remove;
	string node_name;
	double init_prob;
	TS_Node(TS_Signature* p_label, Transition_System *p_my_ts);
	~TS_Node();
	bool AddOutEdge(TS_Edge *n_edge);
	bool AddInEdge(TS_Edge *n_edge);
	TS_Signature *GetLabel();
	std::list<TS_Edge *> *GetOutEdges() { return &out_edges; }
	std::list<TS_Edge *> *GetInEdges() { return &in_edges; }

	TS_Edge *FindOutEdge(TS_Signature *destination);
	TS_Edge *FindInEdge(TS_Signature *source);
	int GetNumberActions();

	void DeleteOutEdge(TS_Node *p_node);
	void DeleteInEdge(TS_Node *p_node);

	bool ImplodeImmediateActions();
};


//////////////////////////////////////////////////////////////////////////////////////////////////
//

class Transition_System {

private:
	int nodes;												// number of nodes of the graph
	int edges;												// number of edges of the graph
	typedef hash_map< TS_Signature, TS_Node *, 
		TS_Signature_hash_compare > NODE_MAP;
	NODE_MAP node_hmap; 									// hash_map of nodes
	std::list<TS_Node *> node_list;							// list of nodes 
	std::list<TS_Node *>::iterator node_iter;				// iterator of the nodes list

	Environment *env;

	unsigned long MAX_nodes;
	int MAX_species;
	int MAX_complexes;

public:
	Transition_System(Environment *p_env, unsigned long p_MAX_nodes, int p_MAX_species, int p_MAX_complexes);
	~Transition_System();
	int GetNodes() { return nodes; }
	int GetEdges() { return edges; }
	bool AddNode(TS_Signature *node);
	// If the source and destination nodes are present the function adds
	// the edge and associate it the rate and the label, otherwise first
	// creates the absent nodes and then adds the edges.
	bool AddEdge(TS_Signature *source, TS_Signature *destination, 
		const string& label, double rate); 
	// Iterator methods
	void IteratorReset();
	bool IteratorNext();
	bool IteratorIsEnd();
	void PrintNodes(FILE *file);
	void PrintGraphDOT(FILE *file);
	void PrintEdgesDOT(TS_Node *node,int *counter,FILE *file);
	TS_Signature *IteratorGetActualSignature();

	// Clean node marking
	void Clean();
	void CleanMarks();
	void IncNodes() { nodes++; }
	void DecNodes() { nodes--; }
	void IncEdges() { edges++; }
	void DecEdges() { edges--; }

	// Generation of the Markov Chain
	bool ImmediateActionsImplosion();

	// Check the generation limits
	bool CheckLimits(TS_Signature *destination);

};


//////////////////////////////////////////////////////////////////////////
//

inline size_t TS_Signature_hash_compare::operator()(const TS_Signature& s1) const
{ 
	return Hash((const char *)&(s1.elements[0]),(int)((2*s1.elements.size()) * sizeof(int)));
}

#endif


