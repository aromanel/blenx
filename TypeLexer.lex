%{
 #include <iostream>
 #include <string>
 #include "TypeParser.h"
 #include "TypeParser.hpp"
 
 using namespace std;
 
#ifdef _MSC_VER 
#pragma warning (push,0)
#pragma warning( disable : 4505 )
#endif

#define YY_USER_ACTION { yylloc->first_line = yylloc->last_line = yylineno; yylloc->last_column=yycolumn+yyleng-1; yylloc->first_column=yycolumn;yycolumn+=yyleng; }

%}

%option noyywrap
%option never-interactive
%option stack
%option bison-bridge
%option bison-locations
%option prefix="type_"
%option nounistd
%option reentrant
%option yylineno

/*regular definitions*/

delim [ \t]
fline (\r\n?|\n)
NotEnd [^\r\n]
sb {delim}+

DLetter [a-z]
ULetter [A-Z]
Digit [0-9]
Exp [Ee][+\-]?{Digit}+

id ({DLetter}|{ULetter}|_)({ULetter}|{DLetter}|{Digit}|_)*

decimal {Digit}+
real1 {Digit}+{Exp}
real2 {Digit}*"."{Digit}+({Exp})?
real3 {Digit}+"."{Digit}*({Exp})?

CmntStrt     \/\*
CmntEnd      \*\/
CmntContent  [^\*\n\r]*

%x CMMNT

%%

{sb}    {  }
{fline} {  }

^{CmntStrt}{CmntContent}\**          { yy_push_state(CMMNT, yyscanner); }
^{CmntStrt}{CmntContent}\**{CmntEnd} { }

<CMMNT>{CmntContent}\**              { }
<CMMNT>{CmntContent}\**{CmntEnd}     { yy_pop_state(yyscanner); }
\/\/{NotEnd}*                        { }

"{"			{ return(LGOPEN); }
"}"			{ return(LGCLOSE); }
"("			{ return(LPOPEN); }
")"			{ return(LPCLOSE); }
","			{ return(LCOMMA); }
"%%"	   	{ return(LDELIM); }
"rate"      { return(LRATE); }
"normal"	{ return(LDIST_NORMAL); }
"gamma"		{ return(LDIST_GAMMA); }
"hypexp"	{ return(LDIST_HYPEREXP); }
"inf"		{ 
          
             string * new_string = new string(yytext);
			 yylval->ptr_string = new_string;
			 return(LINF); 
			}

{id}		{ 
			 string * new_string = new string(yytext);
			 yylval->ptr_string = new_string;
			 return(LID);
			}

{decimal}	{
			 string * new_string = new string(yytext);
			 yylval->ptr_string = new_string;
			 return(LDECIMAL);
			}

{real1} {
			 string * new_string = new string(yytext);
			 yylval->ptr_string = new_string;
			 return(LREAL);
        }
        
{real2} {
			 string * new_string = new string(yytext);
			 yylval->ptr_string = new_string;
			 return(LREAL);
        }
        
{real3} {
			 string * new_string = new string(yytext);
			 yylval->ptr_string = new_string;
			 return(LREAL);
        }

.       { 
			 Error_Manager::PrintError(Pos (yylloc->first_line, yylloc->first_column, ""), "(Failed recognition) token %s not admitted.", yytext);
			 yyterminate();
        }
%%


#ifdef _MSC_VER 
#pragma warning (pop)
#endif

