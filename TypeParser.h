
#ifndef TYPE_PARSER_H_INCLUDED
#define TYPE_PARSER_H_INCLUDED

#include "Type_Affinity.h"
#include "PT_General_Node.h"
#include "FuncMap.h"

class SymbolTable;

class TypeParser
{
public:
   bool control_flag;

   Type_Affinity& affinity;
   SymbolTable* symbolTable;
   std::string currentFilename;

private:
   //Disable copy
   TypeParser (const TypeParser& other);
   TypeParser& operator=(const TypeParser& other);

public:

   TypeParser (Type_Affinity& type_affinity, SymbolTable* symbol_table, const std::string& filename)
      : affinity(type_affinity), symbolTable(symbol_table), currentFilename(filename) { }

   int Parse();

   std::string& GetCurrentFilename() { return currentFilename; }
};


#endif //TYPE_PARSER_H_INCLUDED

