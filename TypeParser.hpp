/* A Bison parser, made by GNU Bison 2.1.  */

/* Skeleton parser for Yacc-like parsing with Bison,
   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     LID = 258,
     LDECIMAL = 259,
     LREAL = 260,
     LINF = 261,
     LGOPEN = 262,
     LGCLOSE = 263,
     LPOPEN = 264,
     LPCLOSE = 265,
     LCOMMA = 266,
     LDELIM = 267,
     LRATE = 268,
     LDIST_NORMAL = 269,
     LDIST_GAMMA = 270,
     LDIST_HYPEREXP = 271
   };
#endif
/* Tokens.  */
#define LID 258
#define LDECIMAL 259
#define LREAL 260
#define LINF 261
#define LGOPEN 262
#define LGCLOSE 263
#define LPOPEN 264
#define LPCLOSE 265
#define LCOMMA 266
#define LDELIM 267
#define LRATE 268
#define LDIST_NORMAL 269
#define LDIST_GAMMA 270
#define LDIST_HYPEREXP 271




#if ! defined (YYSTYPE) && ! defined (YYSTYPE_IS_DECLARED)
#line 38 "TypeParser.y"
typedef union YYSTYPE {
  PTNode *tree_node;
  string *ptr_string;
 } YYSTYPE;
/* Line 1447 of yacc.c.  */
#line 75 "TypeParser.hpp"
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif



#if ! defined (YYLTYPE) && ! defined (YYLTYPE_IS_DECLARED)
typedef struct YYLTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
} YYLTYPE;
# define yyltype YYLTYPE /* obsolescent; will be withdrawn */
# define YYLTYPE_IS_DECLARED 1
# define YYLTYPE_IS_TRIVIAL 1
#endif




