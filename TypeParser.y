//Includes and local variable 


%{
  #include <string>
  #include "Define.h"
  #include "PT_General_Node.h"
  #include "Type_Affinity.h"
  #include "TypeParser.h"
  #include "TypeParser.hpp"
  #include "Symbol_Table.h"
  #include "PT_GD_Node.h"
 
  using namespace std;
   
  //TypeLexer forward definitions
  typedef void* yyscan_t;
  int  type_lex(YYSTYPE *lvalp, YYLTYPE *llocp, yyscan_t scanner);
  void type_set_in(FILE * in_str, yyscan_t scanner);
  int  type_lex_init (yyscan_t* scanner);
  int  type_lex_destroy (yyscan_t yyscanner);
  
  void type_error(YYLTYPE *llocp, TypeParser* parser, yyscan_t scanner, char const* msg);
    
#ifdef _MSC_VER
#pragma warning(push,1)
#pragma warning(disable: 4706)
#pragma warning(disable: 4702)
#pragma warning(disable: 4505)

#endif
  
%}

/* Definizione della Sintassi e delle azioni semantiche */

%union
 {
  PTNode *tree_node;
  string *ptr_string;
 };

%error-verbose
%pure-parser
%locations
%name-prefix="type_"
%parse-param { TypeParser* parser }
%parse-param { yyscan_t scanner }
%lex-param { yyscan_t scanner }


%token <ptr_string> LID 
%token <ptr_string> LDECIMAL
%token <ptr_string> LREAL
%token <ptr_string> LINF

%token LGOPEN LGCLOSE LPOPEN LPCLOSE LCOMMA LDELIM LRATE   

%token LDIST_NORMAL LDIST_GAMMA LDIST_HYPEREXP          

%type <ptr_string> rate
%type <ptr_string> number
%type <ptr_string> id

%type <tree_node> hypexp_parameter_list

%start program

%%

number : 
     LREAL                                                           { $$ = $1; }
   | LDECIMAL                                                        { $$ = $1; }
   ;

rate :
     number                                                          { $$ = $1; }
   | LRATE LPOPEN LID LPCLOSE                                        {
                                                                        //a constant
                                                                        if (parser->symbolTable->FindConstant(*$3))
                                                                        {                                                                           
                                                                           $$ = new string(parser->symbolTable->GetConstantString(*$3));
                                                                           delete ($3);
                                                                        }
                                                                        else
                                                                        {
                                                                           $$ = $3;
                                                                           parser->control_flag = false;
                                                                           Error_Manager::PrintError(Pos(@3.first_line, @3.first_column, parser->GetCurrentFilename()), 23, "The constant '" + *$3 + "' is not defined. Check your function file.");                                                                               
                                                                        }                                                                                                                                       
                                                                     }  
   | LINF                                                            { $$ = $1; }
   ;

program : 
     LGOPEN type_list LGCLOSE                                        {  }
   | LGOPEN type_list LGCLOSE LDELIM LGOPEN affinity_list LGCLOSE    {  }
   ;

id :
     LID                                                             { $$ = $1; }
   ;

	
type_list:																
     id                                                              { parser->control_flag &= parser->affinity.AddType(*$1, Pos(@1.first_line, @1.first_column, parser->GetCurrentFilename())); delete($1); }
   | type_list LCOMMA id                                             { parser->control_flag &= parser->affinity.AddType(*$3, Pos(@3.first_line, @3.first_column, parser->GetCurrentFilename())); delete($3); }
   ;

affinity:
     LPOPEN LID LCOMMA LID LCOMMA rate LPCLOSE                           { parser->control_flag &= parser->affinity.AddAffinity(*$2,*$4,"0.","0.",*$6, Pos(@2.first_line, @2.first_column, parser->GetCurrentFilename())); delete($2); delete($4); delete($6); }
   | LPOPEN LID LCOMMA LID LCOMMA LID LPCLOSE                            {
                                                                            bool ret = true;
                                                                            Pos pos(@2.first_line, @2.first_column, parser->GetCurrentFilename());
                                                                            
                                                                            FunBase* fun = parser->symbolTable->FindFunction(*$6);
                                                                            if (fun == NULL)
                                                                            {
                                                                               Error_Manager::PrintError(pos, 23, "The function '" + *$6 + "' is not defined. Check your function file.");
                                                                               ret = false;
                                                                            }
                                                                            else
                                                                            {
                                                                               ret = parser->affinity.AddAffinity(*$2, *$4, fun, pos);
                                                                            }
                                                                            delete($2); 
                                                                            delete($4); 
                                                                            delete($6);
                                                                            parser->control_flag &= ret;
                                                                         }
   | LPOPEN LID LCOMMA LID LCOMMA rate LCOMMA rate LCOMMA rate LPCLOSE   { parser->control_flag &= parser->affinity.AddAffinity(*$2,*$4,*$6,*$8,*$10, Pos(@2.first_line, @2.first_column, parser->GetCurrentFilename())); delete($2); delete($4); delete($6); delete($8); delete($10);  }
//   | LPOPEN LID LCOMMA LID LCOMMA LDIST_NORMAL LPOPEN number LCOMMA number LPCLOSE LPCLOSE { parser->control_flag &= parser->affinity.AddNormalAffinity(*$2,*$4,*$8,*$10, Pos(@2.first_line, @2.first_column, parser->GetCurrentFilename())); delete($2); delete($4); delete($8); delete($10); }
   | LPOPEN LID LCOMMA LID LCOMMA LDIST_GAMMA LPOPEN number LCOMMA number LPCLOSE LPCLOSE { parser->control_flag &= parser->affinity.AddGammaAffinity(*$2,*$4,*$10,*$8, Pos(@2.first_line, @2.first_column, parser->GetCurrentFilename())); delete($2); delete($4); delete($8); delete($10);}
   | LPOPEN LID LCOMMA LID LCOMMA LDIST_HYPEREXP LPOPEN hypexp_parameter_list LPCLOSE LPCLOSE { parser->control_flag &= parser->affinity.AddHypExpAffinity(*$2,*$4,$8,Pos(@2.first_line, @2.first_column, parser->GetCurrentFilename())); delete($2); delete($4); delete($8); }
   ;
   

hypexp_parameter_list:
		LPOPEN number LCOMMA number LPCLOSE										{ $$ = new PTN_HypExp_Parameter(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),NULL,*$2,*$4); delete($2); delete($4);}
	|	LPOPEN number LCOMMA number LPCLOSE LCOMMA hypexp_parameter_list		{ $$ = new PTN_HypExp_Parameter(Pos(@$.first_line, @$.first_column, parser->GetCurrentFilename()),$7,*$2,*$4); delete($2); delete($4);}
	;


affinity_list:
     affinity                           
   | affinity LCOMMA affinity_list   
   ;
		
	
%%

// Syntax error message 
void type_error(YYLTYPE *llocp, TypeParser* parser, yyscan_t scanner, char const* msg)
{
   Pos pos(llocp->first_line, llocp->first_column, parser->currentFilename);
   Error_Manager::PrintError(pos, 0, msg);
}

// Parse the type file with name file_name
int TypeParser::Parse() 
{
   int res;
   control_flag = true;
      
   affinity.Reset();
   const char* file_name = currentFilename.c_str();
   
   FILE* type_file = fopen(file_name,"r+");
   if (type_file == NULL) 
   {
      Error_Manager::PrintError(0, "Cannot open '" + currentFilename + "'");
      return -1;   
   }
   
   // Initiliaze reentrant flex scanner   
   yyscan_t scanner;
   type_lex_init(&scanner);
   type_set_in(type_file, scanner);
   res = type_parse(this, scanner);
   
   if (control_flag == false)
      return -1;
 
   affinity.InitMatrix();
   
   type_lex_destroy(scanner);
   fclose(type_file);
   return res;
}

#ifdef _MSC_VER
#pragma warning(pop)
#pragma warning(default : 4706)
#pragma warning(default : 4702)
#pragma warning(default : 4505)
#endif

