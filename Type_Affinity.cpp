#include <iostream>
#include <math.h>
#include "Type_Affinity.h"
#include "PT_GD_Node.h"

//
Type_Affinity::Type_Affinity() 
{
	TYPE_COUNTER = 0;
	aff_matrix = NULL;
}

//
Type_Affinity::~Type_Affinity() 
{  
	Reset();
}

/*Type_Affinity::Type_Affinity(const Type_Affinity& other) 
{
this->type_map = other.type_map;
if (other.aff_matrix != NULL) 
{
this->TYPE_COUNTER = other.TYPE_COUNTER;
this->aff_matrix = new AffRuleBase*[TYPE_COUNTER];

for(int i=0; i<TYPE_COUNTER; i++) 
{
aff_matrix[i] = new AffRuleBase[TYPE_COUNTER];
for(int j=0; j<TYPE_COUNTER; j++) 
{
aff_matrix[i][j].bind = other.aff_matrix[i][j].bind;
aff_matrix[i][j].unbind = other.aff_matrix[i][j].unbind;
aff_matrix[i][j].inter = other.aff_matrix[i][j].inter;
}
}
}
}*/

/*Type_Affinity& Type_Affinity::operator=(const Type_Affinity& other)
{
if(this != &other)
{
Reset();

this->type_map = other.type_map;

if (other.aff_matrix != NULL) 
{
this->TYPE_COUNTER = other.TYPE_COUNTER;
this->aff_matrix = new aff_value*[TYPE_COUNTER];

for(int i=0; i<TYPE_COUNTER; i++) 
{
aff_matrix[i] = new aff_value[TYPE_COUNTER];
for(int j=0; j<TYPE_COUNTER; j++) 
{
aff_matrix[i][j].bind = other.aff_matrix[i][j].bind;
aff_matrix[i][j].unbind = other.aff_matrix[i][j].unbind;
aff_matrix[i][j].inter = other.aff_matrix[i][j].inter;
}
}
}
}
return *this;
}*/

bool Type_Affinity::Reset()
{
	if (aff_matrix != NULL)
	{
		for(int i=0; i<TYPE_COUNTER; i++) 
		{
			delete[] aff_matrix[i];
		}
		delete[] aff_matrix;

		TYPE_COUNTER = 0;
		aff_matrix = NULL;
	}
	type_map.clear();
	return true;
}


//
void Type_Affinity::InitMatrix() 
{
	if (aff_matrix != NULL) return;
	aff_matrix = new AffRuleBase**[TYPE_COUNTER];
	for(int i=0; i<TYPE_COUNTER; i++) 
	{
		aff_matrix[i] = new AffRuleBase*[TYPE_COUNTER];	
		for(int j=0; j<TYPE_COUNTER; j++) 
		{
			aff_matrix[i][j] = NULL;
		}
	}
}


bool Type_Affinity::HasInteraction(int i)
{
	for(int j=0; j<TYPE_COUNTER; j++) 
	{
		if (aff_matrix[i][j] != NULL )
			return true;
	}
	return false;
}

//
AffRuleBase *Type_Affinity::GetAffinity(const string& i, const string& j) {
	return aff_matrix[type_map[i]][type_map[j]];
}

//
AffRuleBase *Type_Affinity::GetAffinity(int i, int j) {
	return aff_matrix[i][j];
}


//
bool Type_Affinity::AddType(const string& id, const Pos& pos) {
	if (type_map.find(id) != type_map.end()) {
		string message = "(TYPE FILE): type " + id + " already defined.";
		Error_Manager::PrintError(pos, 10, message);
		return false;
	}
	type_map[id] = TYPE_COUNTER;
	++TYPE_COUNTER;
	return true;
}

//
bool Type_Affinity::Find(string key) {
	hash_map<string,int>::iterator i;
	return ( type_map.find(key) != type_map.end() );
}

//
bool Type_Affinity::AddAffinity(const string& id1, const string& id2, const string& bind, 
								const string& unbind, const string& inter, const Pos& pos) 
{
	bool res = true;
	double v_bind;
	double v_unbind;
	double v_inter;

	InitMatrix();

	if (type_map.find(id1) == type_map.end()) {
		string message = "(TYPE FILE): type " + id1 + " not defined.";
		Error_Manager::PrintError(pos, 11,message);
		res = false;
	}

	if (type_map.find(id2) == type_map.end()) {
		string message = "(TYPE FILE): type " + id2 + " not defined.";
		Error_Manager::PrintError(pos,11,message);
		res = false;
	}	

	if (bind == INF_RATE)
		v_bind = HUGE_VAL;
	else
		v_bind = atof(bind.c_str());

	if (unbind == INF_RATE)
		v_unbind = HUGE_VAL;
	else
		v_unbind = atof(unbind.c_str());

	if (inter == INF_RATE)
		v_inter = HUGE_VAL;
	else 
		v_inter = atof(inter.c_str());


	if (aff_matrix[type_map[id1]][type_map[id2]] != NULL) {
		string message = "(TYPE FILE): affinity values of types " + id1 + " and " + id2 + " already defined.";
		Error_Manager::PrintError(pos,11,message);
		res = false;
	}

	AffRuleComplex *new_affinity = new AffRuleComplex(v_bind,v_unbind,v_inter);

	if (res) {
		aff_matrix[type_map[id1]][type_map[id2]] = new_affinity;
		aff_matrix[type_map[id2]][type_map[id1]] = new_affinity;
	}

	return res;
}

bool Type_Affinity::AddNormalAffinity(const string& id1, const string& id2, const string& mean, const string& variance,
									  const Pos& pos)
{
	bool res = true;

	InitMatrix();

	double v_mean = atof(mean.c_str());
	double v_variance = atof(variance.c_str());

	if (type_map.find(id1) == type_map.end()) {
		string message = "(TYPE FILE): type " + id1 + " not defined.";
		Error_Manager::PrintError(pos, 11,message);
		res = false;
	}

	if (type_map.find(id2) == type_map.end()) {
		string message = "(TYPE FILE): type " + id2 + " not defined.";
		Error_Manager::PrintError(pos,11,message);
		res = false;
	}	

	if (aff_matrix[type_map[id1]][type_map[id2]] != NULL) {
		string message = "(TYPE FILE): affinity values of types " + id1 + " and " + id2 + " already defined.";
		Error_Manager::PrintError(pos,11,message);
		res = false;
	}

	AffRuleNormal *new_affinity = new AffRuleNormal(v_mean,v_variance);

	if (res) {
		aff_matrix[type_map[id1]][type_map[id2]] = new_affinity;
		aff_matrix[type_map[id2]][type_map[id1]] = new_affinity;
	}

	return res;
}
	
bool Type_Affinity::AddGammaAffinity(const string& id1, const string& id2, const string& scale, const string& shape,
									 const Pos& pos)
{
	bool res = true;

	InitMatrix();

	double v_scale = atof(scale.c_str());
	double v_shape = atof(shape.c_str());

	if (type_map.find(id1) == type_map.end()) {
		string message = "(TYPE FILE): type " + id1 + " not defined.";
		Error_Manager::PrintError(pos, 11,message);
		res = false;
	}

	if (type_map.find(id2) == type_map.end()) {
		string message = "(TYPE FILE): type " + id2 + " not defined.";
		Error_Manager::PrintError(pos,11,message);
		res = false;
	}	

	if (aff_matrix[type_map[id1]][type_map[id2]] != NULL) {
		string message = "(TYPE FILE): affinity values of types " + id1 + " and " + id2 + " already defined.";
		Error_Manager::PrintError(pos,11,message);
		res = false;
	}

	AffRuleGamma *new_affinity = new AffRuleGamma(v_shape,v_scale);

	if (res) {
		aff_matrix[type_map[id1]][type_map[id2]] = new_affinity;
		aff_matrix[type_map[id2]][type_map[id1]] = new_affinity;
	}	

	return res;
}

bool Type_Affinity::AddHypExpAffinity(const string& id1, const string& id2, PTNode *parameters, const Pos& pos)
{
	bool res = true;

	InitMatrix();

	if (type_map.find(id1) == type_map.end()) {
		string message = "(TYPE FILE): type " + id1 + " not defined.";
		Error_Manager::PrintError(pos, 11,message);
		res = false;
	}

	if (type_map.find(id2) == type_map.end()) {
		string message = "(TYPE FILE): type " + id2 + " not defined.";
		Error_Manager::PrintError(pos,11,message);
		res = false;
	}	

	if (aff_matrix[type_map[id1]][type_map[id2]] != NULL) {
		string message = "(TYPE FILE): affinity values of types " + id1 + " and " + id2 + " already defined.";
		Error_Manager::PrintError(pos,11,message);
		res = false;
	}

	std::vector< pair<double,double> > params = ((PTN_HypExp_Parameter *)parameters)->GetParameters(); 
	AffRuleHypExp *new_affinity = new AffRuleHypExp(params);

	if (res) {
		aff_matrix[type_map[id1]][type_map[id2]] = new_affinity;
		aff_matrix[type_map[id2]][type_map[id1]] = new_affinity;
	}	

	return res;
}

bool Type_Affinity::AddAffinity(const string& id1, const string& id2, 
								FunBase* rateFun, const Pos& pos)
{
	bool res = true;
	InitMatrix();

	if (type_map.find(id1) == type_map.end()) {
		string message = "(TYPE FILE): type " + id1 + " not defined.";
		Error_Manager::PrintError(pos, 11,message);
		res = false;
	}

	if (type_map.find(id2) == type_map.end()) {
		string message = "(TYPE FILE): type " + id2 + " not defined.";
		Error_Manager::PrintError(pos,11,message);
		res = false;
	}	

	if (aff_matrix[type_map[id1]][type_map[id2]] != NULL) {
		string message = "(TYPE FILE): affinity values of types " + id1 + " and " + id2 + " already defined.";
		Error_Manager::PrintError(pos,11,message);
		res = false;
	}

	AffRuleExpRate *new_affinity = new AffRuleExpRate(rateFun);

	if (res) 
	{
		aff_matrix[type_map[id1]][type_map[id2]] = new_affinity;
		aff_matrix[type_map[id2]][type_map[id1]] = new_affinity;
	}

	return res;
}

void Type_Affinity::Print() {
	if (aff_matrix == NULL) return;
	for(int i=0; i<TYPE_COUNTER; i++) {
		for(int j=0; j<TYPE_COUNTER; j++) {
			aff_matrix[i][j]->Print();
		}
		cout << endl;
	}
}

int Type_Affinity::GetID(const std::string& name)
{
	hash_map<std::string, int>::iterator it = type_map.find(name);
	if (it != type_map.end())
		return it->second;
	else
		return -1;
}

string Type_Affinity::GetKey(int ID) const {
	hash_map<string,int>::const_iterator i;
	for(i=type_map.begin();i!=type_map.end();i++) {
		if ((*i).second == ID) 
			return (*i).first;
	}
	return "";
}

void Type_Affinity::PrintFile(const char *file_name) const {
	FILE *out_file;
	out_file = fopen(file_name,"w+");
	bool control = true;

	// se non ci sono tipi non stampo nulla
	if (type_map.empty()) return;

	// stampo tutti i nomi dei tipi
	hash_map<string,int>::const_iterator i = type_map.begin();
	fprintf(out_file,"{\n%s\t",(*i++).first.c_str());
	while(i!=type_map.end()) {
		fprintf(out_file,",\n%s\t",(*i++).first.c_str());
	}

	fprintf(out_file,"\n}\n");
	if (aff_matrix == NULL) return;

	for(int i=0; i<TYPE_COUNTER; i++) {
		for(int j=i; j<TYPE_COUNTER; j++) {
			if (aff_matrix[i][j] != NULL) {
				if (control) {
					fprintf(out_file,"%%%%\n{\n");
					control = false;
				}
				else
					fprintf(out_file,",\n");

				fprintf(out_file,"( %s,%s,",GetKey(i).c_str(),GetKey(j).c_str());
				aff_matrix[i][j]->Print(out_file);
				fprintf(out_file,")");
			}
		}
	}

	if (!control) 
		fprintf(out_file,"\n}\n");

	fclose(out_file);
}
