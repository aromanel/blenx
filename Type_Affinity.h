#ifndef TYPE_AFFINITY_H_INCLUDED
#define TYPE_AFFINITY_H_INCLUDED

#include <string>
#include <fstream>

#include "Define.h"
#include "Error_Manager.h"
#include "FunBase.h"

class PTNode;

#include "hash.h"

using namespace std;

enum AffCategory {
	RULE_EXP_RATES,
	RULE_EXP_FUN,
	RULE_NORMAL,
	RULE_GAMMA,
	RULE_HYPEXP
};

class AffRuleBase
{
private:
	AffCategory category;
public:
	AffRuleBase(AffCategory p_category)
	{
		category = p_category;
	};
	virtual ~AffRuleBase(){};
	AffCategory GetCategory()
	{
		return category;
	};
	virtual void Print(){};
	virtual void Print(FILE *out_file){};
};

class AffRuleComplex : public AffRuleBase
{
public:
	double bind;
	double unbind;
	double inter;
	
	AffRuleComplex(double p_bind, double p_unbind, double p_inter)
		:AffRuleBase(RULE_EXP_RATES)
	{
		bind = p_bind;
		unbind = p_unbind;
		inter = p_inter;
	};

	~AffRuleComplex(){};
};

class AffRuleExpRate : public AffRuleBase
{
public:
	FunBase* rate_function;

	AffRuleExpRate(FunBase* p_rate_function)
		:AffRuleBase(RULE_EXP_FUN)
	{
		rate_function = p_rate_function;
	};

	~AffRuleExpRate() {};
};

class AffRuleNormal : public AffRuleBase
{
public:
	double mean;
	double variance;

	AffRuleNormal(double p_mean, double p_variance)
		:AffRuleBase(RULE_NORMAL)
	{
		mean = p_mean;
		variance = p_variance;
	};

	~AffRuleNormal() {};
};

class AffRuleGamma : public AffRuleBase
{
public:
	double shape;
	double scale;

	AffRuleGamma(double p_shape, double p_scale)
		:AffRuleBase(RULE_GAMMA)
	{
		shape = p_shape;
		scale = p_scale;
	};

	~AffRuleGamma(){};
};


class AffRuleHypExp : public AffRuleBase
{
public:
	std::vector< pair<double,double> > parameters; 

	AffRuleHypExp(std::vector< pair<double,double> >& p_parameters)
		:AffRuleBase(RULE_HYPEXP)
	{
		parameters = p_parameters;
	};

	~AffRuleHypExp(){};
};


class Type_Affinity 
{
private:
	int TYPE_COUNTER;
	hash_map<string,int> type_map;
	AffRuleBase*** aff_matrix;

public:
	Type_Affinity();
	~Type_Affinity();

	//Copy constructor
	//Type_Affinity(const Type_Affinity& other);
	//Copy operator
	//Type_Affinity& operator=(const Type_Affinity& other);

	bool Reset();

	hash_map<string,int>& GetTypeMap() {
		return type_map;
	}

	bool Find(string key);
	AffRuleBase* GetAffinity(const string& i, const string& j);
	AffRuleBase* GetAffinity(int i, int j);
	void InitMatrix();
	bool HasInteraction(int i);
	bool AddType(const string& id, const Pos& pos);

	bool AddAffinity(const string& id1, const string& id2, const string& bind, 
		const string& unbind, const string& inter, const Pos& pos);

	bool AddAffinity(const string& id1, const string& id2, 
		FunBase* rateFun, const Pos& pos);

	bool AddNormalAffinity(const string& id1, const string& id2, const string& mean, const string& variance, const Pos& pos);
	bool AddGammaAffinity(const string& id1, const string& id2, const string& scale, const string& shape, const Pos& pos);
	bool AddHypExpAffinity(const string& id1, const string& id2, PTNode *parameters, const Pos& pos);

	int GetID(const std::string& name);
	string GetKey(int ID) const;
	void Print();
	void PrintFile(const char *file_name) const;
};


#endif // TYPE_AFFINITY_H_INCLUDED

