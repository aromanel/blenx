#include <iostream>
#include "Unbind.h"

using namespace std;

//////////////////////////////////////////////////////
// CLASS UNBIND - METHODS DEFINITION
//////////////////////////////////////////////////////


//
Unbind::Unbind(Environment* p_env, Entity *p_beta1, Entity *p_beta2, double p_rate,BBinders *p_binder1, BBinders *p_binder2)
   : Element(p_env, UNBIND) 
{
	beta1 = p_beta1;
	beta2 = p_beta2;
	rate = p_rate;
	binder1 = p_binder1;
	binder2 = p_binder2;
}

//
double Unbind::GetTotal(){
	return  ((double)( (env->GetComplexRel()->GetActiveBindings(beta1,beta2,binder1->GetSubject(),binder2->GetSubject())) ));
	/*
	UIBind_elem *elem = env->GetUIBind_map()->Find(beta1,binder1->GetSubject(),beta2,binder2->GetSubject());
	if (elem == NULL) return 0;
	return elem->counter;
	*/
}

//
double Unbind::GetActualRate() 
{
	return ( ((double)env->GetComplexRel()->GetActiveBindings(beta1,beta2,binder1->GetSubject(),binder2->GetSubject())) 
		     * rate );
	/*
	UIBind_elem *elem = env->GetUIBind_map()->Find(beta1,binder1->GetSubject(),beta2,binder2->GetSubject());
	if (elem == NULL) return (0.0);
	else return ((double)(elem->counter));
	*/
}

//
double Unbind::Time()
{
	// double comb_entity = GetActualRate();
    double comb_entity = 
		((double)env->GetComplexRel()->GetActiveBindings(beta1,beta2,binder1->GetSubject(),binder2->GetSubject()));

	if ( comb_entity == 0.0 )
		return HUGE_VAL;

	if ( rate == HUGE_VAL ) 
		return -1.0;  

	double rn = env->Random0To1();
	if ( rn == 0. )
		return HUGE_VAL;

	return ( 1.0 / (comb_entity * rate) ) * log( 1.0 / rn ); 
}

//
void Unbind::Print(){
	cout << this->GetCategory() << " (" << beta1->GetId() << "," << binder1->GetSubject() << " - " 
		<< beta2->GetId() << "," << binder2->GetSubject() << " - " << rate << ")";
}


void Unbind::GetReactants(vector<Entity *> &reactants){
	reactants.push_back(beta1);
	reactants.push_back(beta2);
}


void Unbind::GetIterator(pair<Entity *, Entity *> *ent_pair, list<Complex *> *comp, pair<Iterator_Interface *, Iterator_Interface *> *it_pair){
	//This is necessary because it is possible that an elem of type Ubind
	//contains two entities that can be together in a graph but that aren't
	//together in this graph 
	if (ent_pair->second == NULL){
		//If beta1 and beta2 are equal it is possible that also if are present
		//two entities of the beta1 entity in the analysed graph, we have that
		//ent_pair->second is equal to NULL (because of the procedure followed to
		//build the it_pair object). For this reason in this case we create the
		//iterator
		if (beta1 != beta2){
			it_pair->first = it_pair->second = NULL;
			return;
		}
	}

	it_pair->first = new Simple_Iterator(NULL, new CR_Bind_Iterator(comp->begin(), comp->size(), beta1, beta2, binder1->GetSubject(), binder2->GetSubject(), env));
	it_pair->second = NULL;
}


//
double Unbind::GetRate(Iterator_Interface *c_iter)
{
	double c_rate = this->rate;

	if ( c_iter != NULL )
	{
		assert(c_iter->GetIterator(2)->GetCategory() == IT_COMPLEX_REL_BIND);
		Complex *c = (*((CR_Bind_Iterator *)(c_iter->GetIterator(2)))->GetCurrent());

		c_rate *= c->GetNumber();
	}

	return c_rate;
}


Expr* Unbind::GetExpr(Iterator_Interface *c_iter){
	if (rate == HUGE_VAL){
		return NULL;
	}

	Pos pos(0,0,"");
	Expr *expr;
	string name;
	int mol;

	assert(dynamic_cast<CR_Bind_Iterator *>(c_iter->GetIterator(2)));
	mol = ((CR_Bind_Iterator*)c_iter->GetIterator(2))->GetCurrentComplex()->GetIdN();

	name = env->GetST()->GetMolNameFromID(abs(mol));
	expr = new ExprCompConcentration(pos, env->GetST(), name.substr(2));

	return new ExprOp(pos, env->GetST(), expr, new ExprNumber(pos, env->GetST(), rate), EXPR_TIMES);
}


bool Unbind::IsOnlyMono(int reactants[2]){
	reactants[0] = this->beta1->GetId();
	reactants[1] = this->beta2->GetId();

	return true;
}

Iterator_Interface * Unbind::GetIterator(Entity *e1, Entity *e2, list<Complex *> *c1, list<Complex *> *c2, TwoIteratorKind iterator_kind){
	assert (!c1->empty());
	//If an Unbind can take place c1 and c2 must be equal, otherwise they will
	//be different and an unbind will not take place. For this reason we can
	//create the iterator choosing randomly between the two list of complexes
	return new Simple_Iterator(NULL, new CR_Bind_Iterator(c1->begin(), c1->size(), beta1, beta2, binder1->GetSubject(), binder2->GetSubject(), env));
}
