
#ifndef HASH_H_INCLUDED
#define HASH_H_INCLUDED

struct reaction_key
{
	int data[5];
};

#include <cstddef>
using std::size_t;

size_t Hash (int data[], int num);
size_t Hash (const char * data, int len);

#ifdef _MSC_VER

#include <hash_map>

using stdext::hash_map;
typedef unsigned __int16 uint16_t;
typedef unsigned __int32 uint32_t;

#elif defined(__GNUC__)

#include <stdint.h>
#include <assert.h>
#include <limits>
#include <stdlib.h>
#include <math.h>
#include <errno.h>
#include <ext/hash_map>
using __gnu_cxx::hash_map;
namespace __gnu_cxx
{  
   
   
    //template <class T> std::size_t hash_value(T* const& v)
    template <class T> std::size_t hash_value(T* v)
    {
        std::size_t x = static_cast<std::size_t>(
           reinterpret_cast<std::ptrdiff_t>(v));
        return x + (x >> 3);
    }
    
            inline void hash_float_combine(std::size_t& seed, std::size_t value)
        {
            seed ^= value + (seed<<6) + (seed>>2);
        }

        template <class T>
        inline std::size_t float_hash_impl(T v)
        {
            int exp = 0;
           errno = 0;
            v = frexp(v, &exp);
            if(errno) return 0;

            std::size_t seed = 0;

            std::size_t const length
                = (std::numeric_limits<T>::digits +
                        std::numeric_limits<int>::digits - 1)
                / std::numeric_limits<int>::digits;

            for(std::size_t i = 0; i < length; ++i)
            {
                v = ldexp(v, std::numeric_limits<int>::digits);
                int const part = static_cast<int>(v);
                v -= part;
                hash_float_combine(seed, part);
            }

            hash_float_combine(seed, exp);

            return seed;
        }

        template <class T>
        inline std::size_t float_hash_value(T v)
        {
            using namespace std;
            switch (fpclassify(v)) {
            case FP_ZERO:
                return 0;
            case FP_INFINITE:
                return (std::size_t)(v > 0 ? -1 : -2);
            case FP_NAN:
                return (std::size_t)(-3);
            case FP_NORMAL:
            case FP_SUBNORMAL:
                return float_hash_impl(v);
            default:
                assert(0);
                return 0;
            }
/*            switch(_fpclass(v)) {
            case _FPCLASS_NZ:
            case _FPCLASS_PZ:
                return 0;
            case _FPCLASS_PINF:
                return (std::size_t)(-1);
            case _FPCLASS_NINF:
                return (std::size_t)(-2);
            case _FPCLASS_SNAN:
            case _FPCLASS_QNAN:
                return (std::size_t)(-3);
            case _FPCLASS_NN:
            case _FPCLASS_ND:
                return float_hash_impl(v);
            case _FPCLASS_PD:
            case _FPCLASS_PN:
                return float_hash_impl(v);
            default:
                BOOST_ASSERT(0);
                return 0;
            }
#else
            return float_hash_impl(v);
#endif
*/
        }
    
    
    inline std::size_t hash_value(float v)
    {
        return float_hash_value(v);
    }

    inline std::size_t hash_value(double v)
    {
        return float_hash_value(v);
    }

    inline std::size_t hash_value(long double v)
    {
        return float_hash_value(v);
    }
    
    template <> struct hash<float>
    {
        std::size_t operator()(float val) const
        {
            return hash_value(val);
        }
     };
     
     template <> struct hash<double>
    {
        std::size_t operator()(double val) const
        {
            return hash_value(val);
        }
     };
     
   template <class T> struct hash<T*>
    {
        std::size_t operator()(T* v) const 
        {
            return hash_value(v);
        }
    };

   template<> struct hash< std::string >
   {
      size_t operator()( const std::string& x ) const
      {
         return hash< const char* >()( x.c_str() );
      }
   };
   
   template<> struct hash< reaction_key >
   {
      size_t operator()(const reaction_key& s1) const
      {
         return Hash((const char *)&s1, sizeof(reaction_key));
      }
   };
}


#endif

struct reaction_hash_compare
{
	static const size_t bucket_size = 4;
	static const size_t min_buckets = 8;

	inline size_t operator()(const reaction_key& s1) const;

	bool operator()(const reaction_key& s1, const reaction_key& s2) const
	{
		return !(s1.data[0] == s2.data[0] &&
			s1.data[1] == s2.data[1] &&
			s1.data[2] == s2.data[2] &&
			s1.data[3] == s2.data[3] &&
			s1.data[4] == s2.data[4]);
	}
};

inline bool operator==(const reaction_key& s1, const reaction_key& s2)
{
		return (s1.data[0] == s2.data[0] &&
			s1.data[1] == s2.data[1] &&
			s1.data[2] == s2.data[2] &&
			s1.data[3] == s2.data[3] &&
			s1.data[4] == s2.data[4]);
}


inline size_t get16bits(const char* d) 
{
	return (*((const uint16_t *) (d)));
}


inline size_t Hash (const char * data, int len) 
{
	size_t hash = len, tmp;
	int rem;

	if (len <= 0 || data == NULL) return 0;

	rem = len & 3;
	len >>= 2;

	/* Main loop */
	for (;len > 0; len--) 
	{
		hash  += get16bits (data);
		tmp    = (get16bits (data+2) << 11) ^ hash;
		hash   = (hash << 16) ^ tmp;
		data  += 2*sizeof (uint16_t);
		hash  += hash >> 11;
	}

	/* Handle end cases */
	switch (rem) {
case 3: hash += get16bits (data);
	hash ^= hash << 16;
	hash ^= data[sizeof (uint16_t)] << 18;
	hash += hash >> 11;
	break;
case 2: hash += get16bits (data);
	hash ^= hash << 11;
	hash += hash >> 17;
	break;
case 1: hash += *data;
	hash ^= hash << 10;
	hash += hash >> 1;
	}

	/* Force "avalanching" of final 127 bits */
	hash ^= hash << 3;
	hash += hash >> 5;
	hash ^= hash << 4;
	hash += hash >> 17;
	hash ^= hash << 25;
	hash += hash >> 6;

	return hash;
}

inline size_t reaction_hash_compare::operator()(const reaction_key& s1) const
{
	return Hash((const char *)&s1, sizeof(reaction_key));
}

inline size_t Hash (int data[], int num)
{
	return Hash ((const char *)&(data[0]), num * sizeof(int));
}



#endif //HASH_H_INCLUDED

